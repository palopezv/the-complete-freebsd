.\" This file is in -*- nroff-fill -*- mode
.\" $Id: 10-years.mm,v 1.1 2006/02/18 23:38:55 grog Exp grog $
.\" STATUS: external proofread 4th edition
.Chapter 0 "Preface to the free edition"
On Friday, 14 October 1995, I received a visit from a number of people from
Walnut Creek CDROM, with whom I had published a CD-ROM of ported free software.
This experience had also prompted me to write a book, \[lq]Porting UNIX
Software\[rq].  At the time of the visit, O'Reilly and Associates were in the
course of publishing the book, and I showed the final drafts to Jack Velte and
Greg Long.  They were very interested, and Jack said \[lq]Can't you write a book
about how to install FreeBSD?  Doesn't need to be long, just about 50 pages or
so.\[rq]
.P
At the time I had installed FreeBSD on a couple of machines, but my operating
system of choice was BSD/OS.  The idea sounded like fun, though, and so I
quickly hacked together a few pages.  Jack and Greg liked what they saw, and so
we went ahead.  I had gained some experience with using
.Command groff
while writing \[lq]Porting UNIX Software\[rq], and as the result of some
production issues that I had had with that book, we agreed that I would do the
entire formatting and supply the book in PostScript format.
.P
On 24 February 1996, little over 4 months later, I submitted the final draft of
a book which I called \[lq]Running FreeBSD 2.1\[rq], and which Walnut Creek
called \[lq]Installing And Running FreeBSD\[rq].  That's the way it was printed,
as those of you who have this book can confirm.
.P
The book was a great success.  Of course, it was a little more than 50 pages; in
fact, a total of 322 pages, including 54 pages of man pages from the FreeBSD
distribution.  Despite this, Jack wasn't happy: \[lq]Those Linux books have over
1000 pages!  This looks like nothing.  What can you do?  We need something
\fIyesterday\fP\/!\[rq].
.P
Well, I had started the trend with the first edition.  I added many more man
pages, a little more editorial content, and 5 months later, on 19 July, I
submitted the next version.  We had now agreed on the title: \[lq]The Complete
FreeBSD\[rq], with only 844 pages still shy of the 1000 page mark.  542 of those
pages were man pages.  Given the extreme time constraints, I'm still surprised
that I managed so much.
.P
From then on, I had more time to work on the book, and the material evolved in a
more orderly manner.  I formatted the second (or was that third?) edition on 29
November 1997, now with a whopping 1,766 pages (of which only 1,108 were man
pages; that still left 658 pages of real content).  Jack was finally pleased.
.P
That proved to be too much, though.  People complained that the book was
unwieldy, and after all, man pages are intended to be read online.  We agreed
that printing lots of man pages was also a waste of money.  When the third
edition appeared, on 17 May 1999, it was slimmer: only 808 pages, still
including 126 pages of man pages chosen because they could be of use when the
machine wasn't running.
.P
Changes were afoot in the industry, though.  Jack left Walnut Creek, and later
Bob Bruce merged Walnut Creek CDROM with BSDI.  Shortly after that, they were
taken over by Wind River Systems, who were ambivalent about the book.  Round
this time I convinced Andy Oram, my editor at O'Reilly, to publish the book.
The fourth edition finally appeared in O'Reilly's \[lq]Community Press\[rq]
series in May 2003.  It had 718 pages and\[em]oh wonder!\[em]none of them were
man pages.
.P
That's the current status.  Looking at those dates, you'll see that as time went
on, each edition was further from its predecessor.  There are several reasons
for this: the material is mainly there, there are now other books on FreeBSD out
there, and I don't have the time.
.P
Still, I think that \[lq]The Complete FreeBSD\[rq] is a unique book, and it's
probably worth maintaining.  But I can't do it myself.  So: I'm making the
sources available under the \[lq]Creative Commons\[rq] license (the book itself
has been under this license since the O'Reilly edition).
.P
You can find all about the Creative Commons license at their web site,
.URI http://creativecommons.org/ .
Creative Commons offer a number of alternative license forms.  This book is
licensed under the \[lq]Attribution-NonCommercial-ShareAlike 2.5\[rq] license.
The full text is located at
.URI http://creativecommons.org/licenses/by-nc-sa/2.5/legalcode
(and requires that you reproduce this URI if you redistribute the work).  The
summary at
.URI http://creativecommons.org/licenses/by-nc-sa/2.5/
may be more intelligible:
.P
You are free:
.Ls B
.LI
to copy, distribute, display, and perform the work
.LI
to make derivative works
.Le
under the following conditions:
.Ls B
.LI
Attribution. You must attribute the work in the manner specified by the author or licensor.
.LI
Noncommercial. You may not use this work for commercial purposes.
.LI
Share Alike. If you alter, transform, or build upon this work, you may
distribute the resulting work only under a license identical to this one.
.LI
For any reuse or distribution, you must make clear to others the license terms
of this work.
.LI
Any of these conditions can be waived if you get permission from the copyright
holder.
.Le
Your fair use and other rights are in no way affected by the above.
.P
I explicitly modify one aspect of this license:
.P
.blockquote
You may use this book for commercial purposes if you pay me the sum of USD 20
per copy printed (whether sold or not).  You must also agree to allow inspection
of printing records and other material necessary to confirm the royalty sums.
./blockquote
This is a ridiculously high royalty, of course; if you really want to
make commercial copies, please contact me and we'll come to a reasonable sum.
The purpose of this clause is simply to ensure that you \fIdo\fP\/ contact me
before printing.
.H2 "Building the book"
As it says on the flyleaf, this book was written in \fItroff\fP and formatted on
\*[formatdate] with GNU \fIgroff\fP Version \n(.x.\n(.y running under \*[os]
\*[osver].  It uses a set of macros which I derived from the
.Command groff
\fImm\fP\/ macros.  I've been working on them and the current version of the
build process for more than 10 years, and it's a mess.  I use the framework for
most of my document processing, and one of the things I need to do is to fix it;
I'm doing this in the background.  At some later date I will hopefully be able
to replace the current build mechanism.
.P
In the meantime, a few pointers:
.Ls B
.LI
There are two directories,
.Directory tools
and
.Directory Book .
Their purposes should be self-evident.
.LI
Currently you need to run
.Command make
in
.Directory tools
before building the book.  This should change.
.LI
The  default
.File Makefile
target builds the entire book.  The resulting PostScript file will be located in
.File complete/book.ps .
.LI
The build process uses
.Command Emacs .
.LI
The macros are in the file
.File tools/tmac.Mn .
The only documentation of the extended macros is in the comments in that file.
.P
Please don't bother to modify these macros, nor to document them.  If you feel
like doing that, please contact me and I'll give you the latest version, which
is noticeably different.
.LI
If you don't like the
.File Makefile
or some other aspect of the build, and you feel like fixing it, don't bother
either.  I have a vastly improved version which I can give you; it's just not
quite up to building the book yet.
.LI
The files are maintained with
.Command RCS .
If you like, you can check out any past version, even before the first edition
10 years ago.
.LI
The
.Directory RCS
directory also contains some chapters which were determined to be uninteresting
for the fourth edition, in particular chapters about SLIP, ISBN, UUCP and
faxes.  It should be relatively trivial to re-include them in the book, and I
may do so in the future.
.Le
.H2 "The status of this book"
This book currently has effectively the same content as the fourth edition.  I
have changed the formatting somewhat: O'Reilly asked for a layout which I found
ugly.  You can set this version by modifying the macro file
.File tmac.Mn
and reverting this change:
.Ds
--- tmac.Mn     2003/10/04 04:53:11     1.19
+++ tmac.Mn     2006/02/18 23:35:24
@@ -3185,7 +3185,7 @@
 \&.\e" Set style
 \&.\e" 1: O'Reilly's community press style (Helvetica headings, Times body, no
 italics)
 \&.\e" 2: Approximately O'Reilly's nutshell style (Garamond, italic headings)
-.nr *doc*style* 1
+.nr *doc*style* 2
 \&.\e" Chapter heading
 \&.de Chapter
 \&.ds Section*title
.De
The comments are no longer quite accurate: the book will format in the Garamond
font set if you have it; otherwise it will fall back to Times Roman: Garamond is
a commercial font, so I can't distribute it.
.P
I've made some changes, but many more are needed.  If you want to help, please
let me know.  I can't guarantee money (that would require another print run),
but I can guarantee recognition in the preface.
.P
.in +2i
.br
.nf
.na
Greg Lehey
Echunga, South Australia
24 February 2006
.ad
.fi
.in
