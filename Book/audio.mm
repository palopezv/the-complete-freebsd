.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: Unstarted draft	 4th edition
.\" $Id: audio.mm,v 1.2 2003/05/31 01:22:29 grog Exp $
.\"
.Chapter \*[nchaudio] "Multimedia"
<!-- Hey, emacs!  Edit this file in -*- html-fill -*- mode! -->
<!-- $Id: audio.mm,v 1.2 2003/05/31 01:22:29 grog Exp $ -->
.Ht "<head><title>Trawling the Ports Collection</title></head>"
In the first couple of articles in this series we looked at at how the Ports
Collection can give you a more comfortable environment in which to work.  This
month we'll go off in a different direction: using the sound hardware.
.P
BSD distributions supply hardware drivers for most sound cards, but that's about
all.  By contrast, most commercial systems offer all sorts of bells and
whistles.  You can't boot a Microsoft system without it generating all sorts of
strange noises (though, mercifully, you can mute them).  To do the same with BSD
systems, you need to install additional software from the Ports Collection.
.P
I'm probably not alone in disliking the funny noises that Microsoft generates.
So what applications of the audio hardware are interesting?  In fact, there are
quite a few:
.Ls B
.LI
The most obvious one is to use the CD-ROM drive as a CD player.  This is
particularly convenient when travelling with a laptop.
.LI
There are other sound sources on the net.  It would be nice to be able to play
them.
.LI
Once you have more than one sound source, you may want to mix them.  The base
system does supply a mixer, but it's a bare-bones command line thing.  Possibly
you'd like something more graphical.
.LI
A CD stores about 600 MB and runs for a little over an hour.  12 hours worth of
CDs are relatively bulky, but a modern laptop usually has plenty of space for
that much data on the disk.  We need programs to copy them, though.
.LI
How about writing CDs?  Yet another program..Le
This month we'll look at straightforward CD playing programs.
.H2 "Playing CDs"
A number of programs are available for playing CDs, ranging from the very simple
to the relatively sophisticated.  Most make some attempt to emulate the controls
of a real CD player.
.P
If you're not using X, the choice is a little simpler: use \fIcdplay\fP\[|/], which
displays a text-based minimal panel:
.Dx
cdplay
 ---------------------------------------
 | cdplay -                       Quit |
 | +----------+  Play      Stop        |
 | | 01 00:05 |  SPC pause Eject   +/- |
 | +----------+  Rew       Forward     |
 ---------------------------------------
.De
Use individual letters to control the program: \f(CWp\fP plays, (capital)
\f(CWP\fP pauses, \f(CWs\fP stops.
.P
Does this look too primitive?  I think so.  About the only reason you'd want to
use this kind of interface would be if you weren't running X.  With X you get
better choices.  One program with effectively the same functionality is
\fIxcdplayer\fP\[|/]:
.PIC Images/xcdplayer.ps 3i
Most of the buttons at the bottom are fairly recognizable; the rest (like the
bolt of lightning, which means ``exit program'') are not defined in the man
page.  You have to guess what they mean.  Note that exiting the program doesn't
stop the CD playing; you have to press the stop button for that.
.P
On FreeBSD at any rate, \fIxcdplayer\fP\[|/] currently has a silly problem: by
default it goes looking for a CD device called \fI/dev/rcd0c\fP\[|/], which is no
longer a valid name for a FreeBSD CD-ROM.  In addition, the error message it
produces is less than helpful:
.Dx
open: : No such file or directory.
.De
.P
To run it, you need to specify the device explicitly:
.Dx
$ \f(CBxcdplayer -device /dev/ad0c &\fP
.De
.P
You'll notice the text in red: <font color="red">No Title</font>.
\fIxcdplayer\fP\[|/] gives you the chance to put information about the current CD in
a file, though the man page is hazy about the details.
.P
I personally find \fIxcdplayer\fP\[|/] a little too colourful, and I've had problems
running it at all on certain machines.  Colour is definitely a matter of taste,
and I find \fIworkman\fP\[|/] a little too drab by comparison: it uses the
\fIxview\fP\[|/] library, which appears a little dated nowadays.  On the other hand,
it does offer a better means of categorizing your CDs as well.  Like
\fIxcdplayer\fP\[|/],
\fIworkman\fP\[|/] goes looking for the wrong CD-ROM device.  Run it with:
.Dx
$ \f(CBworkman -c /dev/ad0c &\fP
.De
.P
The display gives you more information:
.PIC Images/workman-main.ps 3i
.P
The buttons at the bottom give you additional menus: \f(CWCD Info\fP gives you
the ability to edit the list of titles and save it.  When you next play the same
CD, \fIworkman\fP\[|/] will find this information and display it:
.PIC Images/workman-cdinfo.ps 3i
.P
The button \f(CWGoodies\fP gives you some additional information:
.PIC Images/workman-goodies.ps 3i


.H2 "Ripping CDs"
The act of copying audio data from CD to disk is called \fIripping\fP.  A
number of programs are available; I use
.Command grip ,
which can also play CDs.


-rw-r--r--  1 grog  lemis     3022 May 15 09:55 /usr/ports/audio/cdplay/Make.log
-rw-r--r--  1 grog  lemis     2080 Jul 27  2002 /usr/ports/audio/dagrab/Make.log
-rw-r--r--  1 grog  lemis   173395 May 10 16:19 /usr/ports/audio/dap/Make.log
-rw-r--r--  1 grog  lemis  4068958 Jul 28  2002 /usr/ports/audio/gqmpeg/Make.log
-rw-r--r--  1 grog  lemis   854307 May 10 13:45 /usr/ports/audio/grip/Make.log
-rw-r--r--  1 grog  lemis    12437 Aug  9  2002 /usr/ports/audio/icecast/Make.log
-rw-r--r--  1 grog  lemis    32443 Oct 25  2002 /usr/ports/audio/id3lib/Make.log
-rw-r--r--  1 grog  lemis    59373 Jul 27  2002 /usr/ports/audio/lame/Make.log
-rw-r--r--  1 grog  lemis    12825 Sep 26  2002 /usr/ports/audio/linux-realplayer/Make.log
-rw-r--r--  1 grog  lemis    24907 Sep 26  2002 /usr/ports/audio/mpg123/Make.log
-rw-r--r--  1 grog  lemis    19329 May 10 17:03 /usr/ports/audio/sox/Make.log
-rw-r--r--  1 grog  lemis   377933 May 10 14:03 /usr/ports/audio/sweep/Make.log
-rw-r--r--  1 grog  lemis     2386 Jul 27  2002 /usr/ports/audio/tosha/Make.log
-rw-r--r--  1 grog  lemis     3249 May 10 15:24 /usr/ports/audio/wavplay/Make.log
-rw-r--r--  1 grog  lemis     5406 Jul 24  2002 /usr/ports/audio/workman/Make.log
-rw-r--r--  1 grog  lemis   299044 Sep 22  2002 /usr/ports/audio/xmms/Make.log


.H3 "dagrab"
DAGRAB is a program for reading audio tracks from a CD into wav sound
files.  An IDE CD-rom drive that supports digital audio is required.

WWW: http://web.tiscalinet.it/marcellou/dagrab.html
.H3 "icecast"
Icecast is a streaming mp3 audio server.

Icecast provides nearly all the functionality of the Shoutcast server.
It will accept encoding streams from encoders like winamp, shout and ices.
It can also add itself to a directory server such as our own
icecast.linuxpower.org or Nullsoft's yp.shoutcast.com.

WWW: http://www.icecast.org/

.H3 "dap"
DAP is a comprehensive audio sample editing and processing suite. DAP
currently supports AIFF and AIFF-C audio files, 8 or 16 bit resolution
and 1, 2 or 4 channels of audio data. Note however that on Linux and
Solaris, compressed AIFF-C files are not currently supported, only
non-compressed AIFF and AIFF-C files.

The package itself offers comprehensive editing, playback and recording
facilities including full time stretch resampling, manual data editing
and a reasonably complete DSP processing suite. 

WWW: http://www.cee.hw.ac.uk/~richardk/
.H3 "lame"
LAME stands for LAME Ain't an Mp3 Encoder. 

This is a patch agains the ISO MPEG1 demonstration source.  The
modifications are distributed under the GNU GENERAL PUBLIC LICENSE
(see the file COPYING for details).

The graphical frame analyzer uses the MPGLIB decoding engine, written by:
Michael Hipp (email: Michael.Hipp@student.uni-tuebingen.de) 
and released under a more restrictive agreement.  

WWW: http://lame.sourceforge.net/
.H3 "mpg123"
The mpg123 reads one or more files (or standard input if ``-'' is
specified) or URLs and plays them on the audio device (default) or
outputs them to stdout.  file/URL is assumed to be an MPEG-1/2 audio
bit stream.

WWW: http://www.mpg123.de/
.H3 "sox"
SoX (also known as Sound eXchange) translates sound samples between
different file formats, and optionally applies various sound effects.
SoX is intended as the Swiss Army knife of sound processing tools.
It doesn't do anything very well, but sooner or later it comes in
very handy.

WWW: http://sox.sourceforge.net/
.H3 "sweep"
Sweep is an audio editor and live playback tool for GNU/Linux, BSD and
compatible systems. It supports many music and voice formats including
WAV, AIFF, Ogg Vorbis, Speex and MP3, with multichannel editing and
LADSPA effects plugins.

WWW: http://sweep.sourceforge.net/
.H3 "tosha"
tosha reads CD-DA (digital audio) and CD-XA (digital video)
tracks and writes them to the harddisk.  Several audio formats
are supported:  raw PCM (little-endian and big-endian byte
order), WAV / RIFF, AIFF and Sun AU.

You can also pipe the data directly into an audio or video
player.  A simple audio player is included ("pcmplay").  To
playback VideoCD data, you need a third-party product, for
example MpegTV (see http://www.mpegtv.com/).

tosha reads the digital audio / video data through the SCSI
bus; therefore it does not work with IDE/ATAPI CD-ROM drives
nor with proprietary interfaces.

Oliver Fromme <oliver.fromme@heim3.tu-clausthal.de>
.H3 "wavplay"
This is a port of wavplay from Linux.  It can record from your sound
card and play recorded sound.

- greg
greg@rosevale.com.au
.H3 "cdparanoia"
Cdparanoia  is a  Compact  Disc Digital  Audio  (CDDA) extraction  tool,
commonly known on the net as a 'ripper'.
Cdparanoia is a  bit different than most other CDDA  extration tools. It
contains few-to-no  'extra' features, concentrating only  on the ripping
process and  knowing as much  as possible about the  hardware performing
it. Cdparanoia will read correct, rock-solid audio data from inexpensive
drives prone to misalignment, frame  jitter and loss of streaming during
atomic reads.  Cdparanoia will also read  and repair data from  CDs that
have been damaged in some way.

WWW: http://www.xiph.org/paranoia/

- Simon 'corecode' Schubert
.H3 "cdrdao"
Not there any more?
