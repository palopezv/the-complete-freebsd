.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: biblio.mm,v 4.8 2003/06/29 06:27:59 grog Exp grog $
.\"
.Appendix \*[nbiblio] "Bibliography"
While the manual pages provide the definitive reference for individual pieces of
the FreeBSD operating system, they are notorious for not illustrating how to put
the pieces together to make the whole operating system run smoothly.
.P
Since the last edition of this book, a number of other books on FreeBSD have
appeared.  We'll look at them first, though you can consider most of them to be
an alternative to this book.
.H2 "Books on BSD"
The following books relate specifically to BSD, most of them to FreeBSD.
.P
.X "Anderson, Annelise"
\fIFreeBSD: An Open-Source Operating System For Your Personal Computer\fP.
Annelise Anderson, The Bit Tree Press, 2001.  An introductory book, particularly
suitable for Microsoft users.
.P
.X "Gay, Warren W."
\fIAdvanced UNIX Programming\fP, by Warren W. Gay.  Sams Publishing, 2000.  This
book uses FreeBSD as the basis for an in-depth programming course.
.P
.X "Horspool, R. Nigel"
\fIThe Berkeley UNIX Environment\fP, by R. Nigel Horspool.  Prentice-Hall Canada
Inc, 1992.  This book predates FreeBSD, but it includes a lot of information for
the advanced user.
.P
.X "Lucas, Michael"
\fIAbsolute BSD\fP, by Michael Lucas.  No Starch Press, 2002.
.P
.X "Mittelstaedt, Ted"
\fIThe FreeBSD Corporate Networker's Guide\fP, by Ted Mittelstaedt.
Addison-Wesley, 2001.  An introduction to FreeBSD for Microsoft system
administrators.
.P
.X "Smith, Roderick W."
\fIFreeBSD: The Complete Reference\fP, by Roderick W. Smith.
McGraw-Hill/Osborne, 2003.
.P
.X "Stokely, Murray"
.X "Clayton, Nik"
\fIThe FreeBSD Handbook\fP, edited by Murray Stokely and Nik Clayton.  Wind
River systems, 2001.  A print version of the online handbook.
.P
.X "Urban, Michael"
.X "Tiemann, Brian"
\fIFreeBSD Unleashed\fP, by Michael Urban and Brian Tiemann.  Sams Publishing,
2002.  An introduction to FreeBSD with detailed descriptions of shell
programming, Gnome and Perl programming.
.H2 "Users' guides"
These books are good general texts.  They have no particular emphasis on BSD.
.P
.X "Abrahams, Paul W."
.X "Larson, Bruce R."
\fIUNIX for the Impatient\fP\|, by Paul W. Abrahams and Bruce R. Larson.  Second
Edition, Addison-Wesley, 1996.  An excellent not-too-technical introduction to
UNIX in general.  Includes a section on X11.
.P
.X "Peek, Jerry"
.X "Todino-Gonguet, Grace"
.X "Strang, John"
\fILearning the Unix Operating System: A Concise Guide for the New User\fP, by
Jerry Peek, Grace Todino-Gonguet, John Strang.  5th Edition, O'Reilly &
Associates, Inc., 2001.  A good introduction for beginners.
.P
.X "O'Reilly, Tim"
.X "Loukides, Mike"
\fIUNIX Power Tools\fR, by Shelley Powers, Jerry Peek, Tim O'Reilly, Mike
Loukides, O'Reilly & Associates, Inc., 3rd Edition October 2002.  A superb
collection of interesting information.  Recommended for everybody, from
beginners to experts.
.H2 "Administrators' guides"
\fIBuilding Internet Firewalls\fP, by D. Brent Chapman and Elizabeth Zwicky.
O'Reilly & Associates, Inc., 1995.
.P
.X "Albitz, Paul"
.X "Liu, Cricket"
\fIDNS and BIND\fP\/, by Paul Albitz, Cricket Liu.  4th Edition, O'Reilly &
Associates, Inc., 2001
.P
.X "Cheswick, William R."
.X "Bellovin, Steven M."
\fIFirewalls and Internet Security: Repelling the Wily Hacker\fP, by William
R. Cheswick and Steven M. Bellovin.  Second edition, Addison-Wesley, 2003.
.P
.X "Frisch, \(AEleen"
\fIEssential System Administration\fP, by \(AEleen Frisch.  Third edition,
O'Reilly & Associates, Inc., 2003.  Includes coverage of FreeBSD 4.7.
.P
.X "Hunt, Craig"
\fITCP/IP Network Administration\fP, by Craig Hunt.  Third Edition.  O'Reilly &
Associates, 2002
.P
.X "Nemeth, Evi"
.X "Snyder, Garth"
.X "Seebass, Scott"
.X "Hein, Trent R."
\fIUNIX System Administration Handbook\fP, by Evi Nemeth, Garth Snyder, Scott
Seebass, and Trent R. Hein.  3nd edition, Prentice Hall, 2001.  An excellent
coverage of four real-life systems, including FreeBSD 3.4.
.P
.X "Stern, Hal"
.X "Eisler, Mike"
.X "Labiaga, Ricardo"
\fIManaging NFS and NIS\fP, by Hal Stern, Mike Eisler and Ricardo Labiaga.  2nd
Edition, O'Reilly & Associates, Inc., 2001
.P
.X "Ts, Jay"
.X "Eckstein, Robert"
.X "Collier-Brown, David"
\fIUsing Samba\fP, by Jay Ts, Robert Eckstein and David Collier-Brown.  2nd
Edition, O'Reilly & Associates, Inc., 2003.
.H2 "Programmers' guides"
.X "Asente, Paul"
\fIX Window System Toolkit\fP, by Paul Asente. Digital Press.
.P
.X "Ellis, Margaret A."
.X "Stroustrup, Bjarne"
\fIThe Annotated C++ Reference Manual\fP, by Margaret A. Ellis and Bjarne
Stroustrup.  Addison-Wesley, 1990.
.P
.X "Harbison, Samuel P."
.X "Steele, Guy L."
\fIC: A Reference Manual\fP, by Samuel P. Harbison and Guy L. Steele, Jr.  3rd
edition, Prentice Hall, 1991.
.P
.X "Jolitz, William F."
\fI``Porting UNIX to the 386''\fP in \fIDr. Dobb's Journal\fP, William Jolitz.
January 1991\(enJuly 1992.
.P
.X "Lehey, Greg"
\fIPorting UNIX Software\fP, by Greg Lehey.  O'Reilly & Associates, 1995.
.P
.X "McKusick, Kirk"
.X "Karels, Michael J."
.X "Quarterman, John S."
.X "Bostic, Keith"
\fIThe Design and the Implementation of the 4.4BSD Operating System\fR.
Marshall Kirk McKusick, Keith Bostic, Michael J.  Karels, John S.  Quarterman.
Addison-Wesley, 1996.  The definitive description of the 4.4BSD kernel and
communications.
.P
.X "Plauger, P. J."
\fIThe Standard C Library\fP, by P. J. Plauger. Prentice Hall, 1992.
.P
.X "Stevens, W. Richard"
.X "Wright, Gary R."
\fITCP/IP illustrated\fP, by W. Richard Stevens and Gary R. Wright (Volume 2
only).  Prentice-Hall, 1994\(en1996.  A three-volume work describing the
Internet Protocols.  Volume 2 includes code from the 4.4BSD-Lite implementation,
most of which is very similar to the FreeBSD implementation.
.P
.X "Stevens, W. Richard"
\fIUNIX Network Programming\fP, by W. Richard Stevens.  Prentice-Hall, 1998.  A
two-volume introduction to network programming.
.P
.X "Wells, Bill"
\fIWriting Serial Drivers for UNIX\fP, by Bill Wells.  \fIDr. Dobb's Journal\fP,
19(15), December 1994. pp 68-71, 97-99.
.H2 "Hardware reference"
.X "Seyer, Marty"
\fIRS-232 made easy\fR, second edition.  Martin D.  Seyer, Prentice-Hall 1991.
A discussion of the RS-232 standard.
.P
.X "Stanley, Tom"
\fIISA System Architecture\fP, by Tom Stanley.  3rd edition, Addison-Wesley,
1995.
.P
\fIPCI System Architecture\fP, by Tom Stanley.  3rd edition, Addison-Wesley,
1995.
.P
.P
.X "Van Gilluwe, Frank"
\fIThe Undocumented PC\fP, by Frank Van Gilluwe.  Addison-Wesley, 1994.
.H2 "The 4.4BSD manuals"
The original 4.4BSD manual set includes the man pages and a number of documents
on various aspects of programming, user programs and system administration.
With a few minor exceptions, you can find the latest versions in
\fI/usr/share/man\fP\| (the man pages) and \fI/usr/share/doc\fP\| (the other
documents).  If you want the original 4.4BSD versions, you can check them out of
the repository.
.P
If you prefer a bound version, O'Reilly and Associates published the original
five-volume set of documentation for 4.4BSD as released by the CSRG in 1994,
including the AT&T historical documents.  Compared to FreeBSD, much of this
documentation is severely out of date, and it's also out of print, though you
should still be able to find it second-hand.  It comprises the following
volumes:
.Ls B
.LI
\fI4.4BSD Programmer's Reference Manual\fR.  These are sections 2, 3, 4 and 5 of
the man pages for 4.4BSD.
.LI
\fI4.4BSD Programmer's Supplementary Documents\fR.  You can find the latest
versions of most of these documents in \fI/usr/share/doc/psd\fP.
.LI
\fI4.4BSD User's Reference Manual\fR.  This book contains sections 1, 6 and 7 of
the 4.4BSD man pages.
.LI
\fI4.4BSD User's Supplementary Documents\fR.  You can find the latest versions
of most of these documents in \fI/usr/share/doc/usd\fP.
.LI
\fI4.4BSD System Manager's Manual\fR.  Contains section 8 of the manual and a
number of other documents.  You can find the latest versions of most of these
documents in \fI/usr/share/doc/smm\fP.
.Le
.H2 "Getting FreeBSD on CD-ROM"
FreeBSD is available on CD-ROM from a number of suppliers:
.nf
.na
.P
.ne 9v
\fBDaemon News Mall\fP
560 South State Street, Suite A2
Orem, UT 84058
USA
Phone:\h'|.5i' +1 800 407-5170
Fax:\h'|.5i' +1 801 765-0877
Email:\h'|.5i'\fIsales@bsdmall.com\fP\/
WWW:\h'|.5i'\fIhttp://www.bsdmall.com/\fP\/
.P
.ne 9v
\fBEverything Linux\fP
PO Box 243
Croydon NSW 2132
Australia
Phone:\h'|.5i'0500 500 368
\h'|.5i'02 8752 6666
Fax:\h'|.5i'02 9712 3977
Email:\h'|.5i'\fIsales@everythinglinux.com.au\fP
WWW:\h'|.5i'\fIhttp://www.everythinglinux.com.au/\fP
.P
.ne 9v
\fBFreeBSD Mall, Inc.\fP
3623 Sanford Street
Concord, CA  94520-1405
USA
Phone:\h'|.5i'+1 925 674-0783
Fax:\h'|.5i'+1 925 674-0821
Email:\h'|.5i'\fI<info@freebsdmall.com>\fP\/
WWW:\h'|.5i'\fIhttp://www.freebsdmall.com/\fP
.P
.ne 9v
\fBFreeBSD Services Ltd\fP
11 Lapwing Close
Bicester
OX26 6XR
United Kingdom
WWW:\h'|.5i'\fIhttp://www.freebsd-services.com/\fP
.P
.ne 9v
\fBHinner EDV\fP
St. Augustinus-Stra�e 10
D-81825 M�nchen
Germany
Phone:\h'|.5i'(089) 428 419
WWW:\h'|.5i'\fIhttp://www.hinner.de/linux/freebsd.html\fP
.P
.ne 9v
\fBIngram Micro\fP
1600 E. St. Andrew Place
Santa Ana, CA  92705-4926
USA
Phone:\h'|.5i'1 (800) 456-8000
WWW:\h'|.5i'\fIhttp://www.ingrammicro.com/\fP
.P
.ne 9v
\fBThe Linux Emporium\fP
Hilliard House, Lester Way
Wallingford
OX10 9TA
United Kingdom
Phone:\h'|.5i'+44 1491 837010
Fax:\h'|.5i'+44 1491 837016
WWW:\h'|.5i'\fIhttp://www.linuxemporium.co.uk/bsd.html\fP
.P
.ne 9v
\fBUNIXDVD.COM LTD\fP
57 Primrose Avenue
Sheffield
S5 6FS
United Kingdom
WWW:\h'|.5i'\fIhttp://www.unixdvd.com/\fP
.ad
.fi
.P
In addition, in the USA Frys Electronics and CompUSA carry boxed sets of FreeBSD
and documentation.
