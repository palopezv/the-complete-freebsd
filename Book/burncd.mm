.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: burncd.mm,v 4.14 2003/06/29 04:33:03 grog Exp grog $
.Chapter \*[ncdburn] "Writing CD-Rs"
.X "UNIX File System"
.X "UFS"
.X "ISO 9660"
Under FreeBSD, data on conventional hard disks is stored in the \fIUNIX File
System\fP\/ or \fIUFS\fP\/ format.  CD-ROMs and CD-Rs use a different file
system, the \fIISO 9660\fP\/ format, which is compatible with other systems.
This is not a problem when you mount a CD-ROM: FreeBSD includes a read-only ISO
9660 file system.  When you want to write a CD-R, however, things are a little
more complicated: the medium requires you to write the entire file system at
once, and since the file system is stored in a different format, you can't just
copy the UFS file system.  Instead, you must first create an \fIimage\fP\/ of
the file system that you want to put on the CD-R, and then you copy it.  We'll
look at these steps in more detail below.
.H2 "Creating an ISO-9660 image"
.Pn install-cdrtools
.X "ISO 9660"
.X "ISO image"
.X "image, ISO"
The first step is to create the \fIISO 9660\fP\/ file system image, frequently
simply called an \fIISO image\fP.  There are a number of ports available in the
Ports Collection; here we'll look at
.Command mkisofs ,
which is part of the
.Command cdrtools
port.  Installation isn't quite as straightforward as you might expect: you need
a special flag to install
.Command mkisofs \/:
.Dx
# \f(CBcd /usr/ports/sysutils/cdrtools\fP
# \f(CBmake install -DMKISOFS\fP
.De
.Command mkisofs
has a bewildering number of parameters.  Here are the important ones:
.Ls B
.LI
The \f(CW-A\fP option specifies the \fIapplication ID\fP, a text string written
to the header of the file system to describe the ``application'' on the image.
It can be up to 128 characters long.
.LI
Use \f(CW-b\fP if you want to be able to boot from the CD, such as a FreeBSD
bootable CD.  In the case of FreeBSD, use the 2.88 MB image
.File floppies/boot.flp
which is built as part of the release process.  Note that this file \fImust\fP\/
be in one of the directories included in the image, and the name is relative to
the root directory of the CD.
.LI
The \f(CW-f\fP option tells
.Command mkisofs
to follow symbolic links.  If you don't specify this option and you have
symbolic links in the directory from which you create the image, the resultant
CD-ROM image will contain only a symbolic link instead of the file itself.  If
the file to which the symbolic link points is below the top-level (root)
directory, this is the preferred way to do things, because it saves space, but
if it points outside the CD-ROM, the file will not appear on the CD-ROM at all.
Use this option if you have symbolic links to files outside the directory that
you're using for the root of the CD-ROM file system.
.LI
.X "Joliet file system"
.X "file system, Joliet"
The \f(CW-J\fP option makes the CD compatible with Microsoft's \fIJoliet\fP\/
format.  You don't need it for FreeBSD, but it doesn't cost much, so it's a good
idea to include it if there's a chance the CD will be used in a Microsoft
environment.
.LI
Use the \f(CW-o\fP option to specify the name of the resultant ISO image.
This image is the size of the resultant CD, so it could be up to 700 MB.
.LI
The \f(CW-p\fP option specifies the \fIpreparer ID\fP, another ISO 9660 header
field to specify who made the CD-ROM.
.LI
.X "Rock Ridge Extensions"
The \f(CW-r\fP option specifies the \fIRock Ridge Extensions\fP\/ that are used
to store UNIX file names.  It makes a number of assumptions about permissions
and owners; see the man page for details.  It takes no parameters.
.LI
The \f(CW-T\fP option tells
.Command mkisofs
to include a translation file
.File TRANS.TBL
in each directory for use by systems that don't support the Rock Ridge
extensions.  For each file name in the directory,
.File TRANS.TBL
contains a Microsoft-compatible name (up to eight characters, a period
(\f(CW.\fP) and up to three more characters).  The names bear a slight
resemblance to the original names.
.LI
If you don't like the name
.File TRANS.TBL ,
you can specify a different name with the \f(CW-table-name\fP option, which
implies \f(CW-T\fP.  For example, if you write \f(CW-table-name .MAP\fP you will
generate names that won't show up with a normal
.Command ls
command.
.LI
The \f(CW-V\fP option specifies the \fIvolume ID\fP\/ for the file system.  This
will normally be more specific than the application ID; for example, each CD in
a set of CDs might have the same application ID and a different volume ID.
.LI
The final parameters are the names of the directories that will be included in
the image.  You can specify multiple directories.  In each case, the entire
directory hierarchy will be included.
.Le
.ne 10v
This is a lot of stuff to type in every time.  It's easier to write a
.File Makefile
and use
.Command make \/:
.Dx
APPLID  =       "Dummy application"
BOOT    =
# To make it bootable, put in something like this:
# Note that the -b option is there as well
# BOOT  =       "-b floppies/boot.flp"
ISO     =       /var/tmp/isoimage
PREPARER  =     "me"
VOLID   =       "Volume 0000"
DIR     =       .

cdrom:
        mkisofs -A ${APPLID} ${BOOT} -J -o ${ISO} -f \e
        -p ${PREPARER} -r -T -V ${VOLID} ${DIR}
.De
For example, to make a bootable CD-R of the FreeBSD release, you would first
perform the \fImake world\fP\/ and \fImake release\fP.  Assuming that the
release directory is
.Directory /home/release ,
you will find the directory trees for the first two CD-ROMs in
.Directory -n /home/release/R/cdrom/disc1
and
.Directory -n /home/release/R/cdrom/disc2 .
You could do this:
.Dx
# \f(CBmake cdrom DIR=/home/release/R/cdrom/disc1\fP
mkisofs -A "Dummy application"  -J -o ../iso -table-name .MAP  -p "Greg Lehey" -r -T
-V "Volume 0000" .
  6.40% done, estimate finish Sun Aug 27 13:34:54 2000
 12.79% done, estimate finish Sun Aug 27 13:35:02 2000
 19.19% done, estimate finish Sun Aug 27 13:35:05 2000
 25.57% done, estimate finish Sun Aug 27 13:35:10 2000
 31.97% done, estimate finish Sun Aug 27 13:35:10 2000
 38.36% done, estimate finish Sun Aug 27 13:35:10 2000
 44.75% done, estimate finish Sun Aug 27 13:35:10 2000
 51.15% done, estimate finish Sun Aug 27 13:35:12 2000
 57.54% done, estimate finish Sun Aug 27 13:35:12 2000
 63.94% done, estimate finish Sun Aug 27 13:35:12 2000
 70.34% done, estimate finish Sun Aug 27 13:35:11 2000
 76.72% done, estimate finish Sun Aug 27 13:35:13 2000
 83.12% done, estimate finish Sun Aug 27 13:35:12 2000
 89.52% done, estimate finish Sun Aug 27 13:35:13 2000
 95.90% done, estimate finish Sun Aug 27 13:35:13 2000
Total translation table size: 35119
Total rockridge attributes bytes: 59724
Total directory bytes: 104448
Path table size(bytes): 256
Max brk space used 86224
78211 extents written (152 Mb)
.De
The progress reports are rather boring nowadays, considering that the whole
process only takes a couple of minutes, but the summary information at the
bottom can be of interest.
.H3 "Testing the CD-R"
.Pn md-driver
So now you have an ISO image.  How do you know it's correct?  It's just a single
file, and it could have just about anything on it.  You can burn a CD, of
course, but if it's junk, you have another coaster.  If you're not sure, it's
better to look inside first.  You can do that by using it as the basis for an
\fImd vnode\fP\/ device.
.P
.X "md, driver"
.X "driver, md"
.X "vnode, driver"
.X "driver, vnode"
The \fImd\fP\/ driver creates a number of different kinds of pseudo-device.  See
the man page \fImd(4)\fP\/ for more details.  We use the \fIvnode\fP\/ device, a
special file that refers to file system files.  Support for \fImd\fP\/ is
included in the GENERIC kernel, but if you've built a kernel without the
\fImd\fP\/ driver, you can load it as a \fIkld\fP.  If you're not sure, try
loading the kld anyway.  Then you associate a vnode device with the ISO image
\fIiso-image\fP\/ using the program
.Command mdconfig \/:
.Dx
# \f(CBkldload md\fP                            \fIload the kld module if necessary\fP\/
kldload: can't load md: File exists     \fIalready loaded or in the kernel\fP\/
# \f(CBmdconfig -a -t vnode -f iso-image\fP     \fIconfigure the device\fP\/
md0                                     \fIthis is the name assigned\fP\/
# \f(CBmount -t cd9660 /dev/md0 /mnt\fP         \fImount it\fP\/
.De
After this, you will be able to access the image at
.Directory /mnt
as a normal file system.  Don't forget to unmount and unconfigure the file when
you're finished:
.Dx
# \f(CBumount /mnt\fP
# \f(CBmdconfig -d -u 0\fP
.De
.X "vn, driver"
.X "driver, vn"
Older releases of FreeBSD used the \fIvn\fP\/ driver, which used different syntax.
.H2 "Burning the CD-R"
Once you have created and tested an ISO image, you can copy it to CD-R.  For
SCSI burners, you use
.Command cdrecord \/;
ATA (IDE) CD-R burners you use
.Command burncd .
In the following sections we'll look at both programs.
.H3 "Burning a CD-R on an ATA burner"
To burn a CD-R in an ATA (or IDE) burner, use
.Command burncd ,
which is part of the base system.  Typically you'll only have one CD-R burner on
the system, so it will be called
.Device acd0 .
You'll have something like this in your
.Command dmesg
output:
.Dx
acd0: CD-RW <RWD RW4224> at ata1-slave BIOSPIO
.De
.Command burncd
has both flags and commands.  For our purposes, the most important flags are:
.Ls B
.LI
The \f(CW-f \fIdevice\fR\/ option specifies the device to use for the burning process.
.LI
The \f(CW-m\fP option tells
.Command burncd
to close the disk in multisession mode.
.LI
The \f(CW-q\fP option tells
.Command burncd
to be quiet and not to print progress messages.  In fact, it's not very verbose
anyway.
.LI
The \f(CW-s \fIspeed\fR\/ option specifies the speed of the burner device.  It
defaults to 1, so you'll save a lot of time using this.
.LI
The \f(CW-t\fP option specifies a test write:
.Command burncd
does not actually write on the medium.
.LI
The \f(CW-v\fP (verbose) option prints a few extra progress messages.
.Le
.ne 10v
The most important commands for writing ISO 9660 CD-ROMs are:
.Ls B
.LI
.X "mode1 data"
.X "CD-R, mode1 data"
\f(CWdata\fP or \f(CWmode1\fP write data tracks, also known as \fImode1\fP\/
tracks, for the image files named on the command line.
.LI
\f(CWfixate\fP fixates the medium by generating the table of contents.  This
should be the last command to
.Command burncd .
.Le
If
.Command burncd
doesn't recognize a command, it assumes it's a file name.  If it does, it
assumes it isn't a file name.  This can be confusing: there are other commands
not mentioned in the list above, for example \f(CWraw\fP.  If you have an ISO
file called \fIraw\fP, you'll have to rename it before you can burn it with
\fIburncd\fP.
.P
.X "CR-R, coaster"
Before you start, you should decide on the recording speed.  If your machine is
fast enough, use the rated recording speed.  In the case of the example machine,
that's an 8x speed (i.e. it records at eight times the speed at which a normal
audio CD is played).  Before you do this, though, you should make sure that your
system can keep a sufficient data rate so that there is always data available to
transfer to the CD-R.  If it can't keep up, you'll get an \fIunderrun\fP, a gap
in the data, and your CD-R is worthless (a \fIcoaster\fP\/).
.P
To make sure you don't make coasters, you should do a test run.  The system goes
through all the motions, but it doesn't actually write anything to the CD-R
blank.  Nevertheless, it tests all aspects of the burn, so you must have a valid
CD-R blank in the drive, otherwise the attempt will fail.  To test burn an image
called \fIiso\fP, enter:
.Dx
# \f(CBburncd -f /dev/acd0c -t -v -s 8 data iso fixate\fP
adding type 0x08 file iso size 184576 KB 92288 blocks
next writeable LBA 0
addr = 0 size = 189005824 blocks = 92288
writing from file iso size 184576 KB
written this track 6880 KB (3%) total 6880 KB
.De
At this point,
.Command burncd
overwrites the line with progress indications until it is finished.  Finally,
you see:
.Dx
written this track 184576 KB (100%) total 184576 KB
fixating CD, please wait..
burncd: ioctl(CDRIOCFIXATE): Input/output error
.De
This last line appears a little alarming.  It's not really serious, though: the
CD has not really been written, so it's not possible to read from it.  A number
of CD-R drives return error conditions under these circumstances.
.P
If everything was OK in the test run, you can repeat the command without the
\f(CW-t\fP flag:
.Dx
# \f(CBburncd -f /dev/acd0c -v -s 8 data iso fixate\fP
.De
The output is identical, but this time you should not get the error message.
.H3 "Burning a CD-R on a SCSI burner"
If you have a SCSI burner, use
.Command cdrecord ,
which is part of the
.Command cdrtools
port we installed on page
.Sref \*[install-cdrtools] .
.Command cdrecord
has a rather strange habit of not using device names: instead, it accesses the
device directly by its SCSI parameters (bus, unit and LUN).  You can get these
parameters from the
.Command dmesg
output in
.File /var/run/dmesg.boot ,
but there's an easier way:
.Dx
# \f(CBcdrecord -scanbus\fP
Cdrecord 1.9 (i386-unknown-freebsd4.1) Copyright (C) 1995-2000 J�rg Schilling
Using libscg version 'schily-0.1'
scsibus0:
        0,0,0     0) 'MATSHITA' 'CD-R   CW-7503  ' '1.06' Removable CD-ROM
cdrecord: Warning: controller returns zero sized CD capabilities page.
cdrecord: Warning: controller returns wrong size for CD capabilities page.
cdrecord: Warning: controller returns wrong page 0 for CD capabilities page (2A).
        0,1,0     1) 'TEAC    ' 'CD-ROM CD-532S  ' '1.0A' Removable CD-ROM
        0,2,0     2) *
        0,3,0     3) *
        0,4,0     4) 'SONY    ' 'SDT-10000       ' '0101' Removable Tape
        0,5,0     5) *
        0,6,0     6) *
        0,7,0     7) *
        0,8,0     8) 'QUANTUM ' 'QM318000TD-SW   ' 'N491' Disk
.De
This output doesn't tell you exactly which devices are CD-Rs, and it also
doesn't look at any except the first SCSI bus.  Alternatively, you can use the
standard system utility
.Command camcontrol \/:
.Dx
# \f(CBcamcontrol devlist\fP
<MATSHITA CD-R   CW-7503 1.06>     at scbus0 target 0 lun 0 (pass0,cd0)
<TEAC CD-ROM CD-532S 1.0A>         at scbus0 target 1 lun 0 (pass1,cd1)
<SONY SDT-10000 0101>              at scbus0 target 4 lun 0 (sa0,pass2)
<QUANTUM QM318000TD-SW N491>       at scbus0 target 8 lun 0 (pass3,da0)
<EXABYTE EXB-8505SMBANSH2 0793>    at scbus1 target 1 lun 0 (sa1,pass4)
<Quantum DLT4000 CC1E>             at scbus1 target 3 lun 0 (sa2,pass5)
<AIWA GD-8000 0119>                at scbus1 target 4 lun 0 (sa3,pass6)
<NRC MBR-7 110>                    at scbus1 target 6 lun 0 (pass7,cd2)
<NRC MBR-7 110>                    at scbus1 target 6 lun 1 (pass8,cd3)
<NRC MBR-7 110>                    at scbus1 target 6 lun 2 (pass9,cd4)
<NRC MBR-7 110>                    at scbus1 target 6 lun 3 (pass10,cd5)
<NRC MBR-7 110>                    at scbus1 target 6 lun 4 (pass11,cd6)
<NRC MBR-7 110>                    at scbus1 target 6 lun 5 (pass12,cd7)
<NRC MBR-7 110>                    at scbus1 target 6 lun 6 (pass13,cd8)
.De
Either way, you need to recognize the CD-R device, which in this case is
relatively easy: it's the Matsushita CW-7503 (``\f(CWMATSHITA\fP'') at the very
beginning of each list.
.Command cdrecord
refers to this device as \f(CW0,0,0\fP (bus 0, target 0, LUN 0).
.P
.X "CD-R, coaster"
The next thing to look at is the recording speed.  If your machine is fast
enough, use the rated recording speed.  In the case of the example machine,
that's an 8x speed (i.e. it records at 8 times the speed at which a normal audio
CD is played).  Before you do this, though, you should make sure that your
system can keep a sufficient data rate so that there is always data available
when to go on the CD.  If you can't keep up, you'll get an \fIunderrun\fP, a gap
in the data, and your CD-R is worthless (a \fIcoaster\fP\/).
.P
.ne 3v
To make sure you don't make coasters, you should do a dummy run.  The system
goes through all the motions, but it doesn't actually write anything to the CD-R
blank.  Nevertheless, it tests all aspects of the burn, so you must have a valid
CD-R blank in the drive, otherwise the attempt will fail.  To burn an image
called \fIiso\fP, enter:
.Dx
# \f(CBcdrecord -dummy -v dev=0,0,0 -speed=8 iso\fP
Cdrecord 1.9 (i386-unknown-freebsd5.0) Copyright (C) 1995-2000 J�rg Schilling
TOC Type: 1 = CD-ROM
scsidev: '0,0,0'
scsibus: 0 target: 0 lun: 0
Using libscg version 'schily-0.1'
atapi: 0
Device type    : Removable CD-ROM
Version        : 2
Response Format: 2
Capabilities   : SYNC LINKED
Vendor_info    : 'MATSHITA'
Identifikation : 'CD-R   CW-7503  '
Revision       : '1.06'
Device seems to be: Generic mmc CD-R.
Using generic SCSI-3/mmc CD-R driver (mmc_cdr).
Driver flags   : SWABAUDIO
FIFO size      : 4194304 = 4096 KB
Track 01: data  152 MB
Total size:     175 MB (17:22.84) = 78213 sectors
Lout start:     175 MB (17:24/63) = 78213 sectors
Current Secsize: 2048
ATIP info from disk:
  Indicated writing power: 5
  Is not unrestricted
  Is not erasable
  ATIP start of lead in:  -11080 (97:34/20)
  ATIP start of lead out: 335100 (74:30/00)
Disk type:    Long strategy type (Cyanine, AZO or similar)
Manuf. index: 11
Manufacturer: Mitsubishi Chemical Corporation
Blocks total: 335100 Blocks current: 335100 Blocks remaining: 256887
RBlocks total: 342460 RBlocks current: 342460 RBlocks remaining: 264247
Starting to write CD/DVD at speed 8 in dummy mode for single session.
Last chance to quit, starting dummy write in 1 seconds.
Waiting for reader process to fill input buffer ... input buffer ready.
Starting new track at sector: 0
Track 01: 0 of 152 MB written (fifo 100%).
.De
At this point,
.Command cdrecord
overwrites the last line with progress indications until it is finished.  If
you're watching, keep an eye on the \fIfifo\fP\/ information at the end of the
line.  This gives you an idea how well the system is keeping up with the burner.
If the utilization drops to 0, you will get an underrun, and the blank would
have become a coaster if this were for real.
.P
Finally, you see:
.Dx
Track 01: 152 of 152 MB written (fifo 100%).
Track 01: Total bytes read/written: 160176128/160176128 (78211 sectors).
Writing  time:  136.918s
Fixating...
WARNING: Some drives don't like fixation in dummy mode.
Fixating time:   35.963s
cdrecord: fifo had 2523 puts and 2523 gets.
cdrecord: fifo was 0 times empty and 2451 times full, min fill was 96%.
.De
.ne 3v
The summary information at the end shows that at some point the fifo dropped
below 100% full, but this is far from being a problem.  If, on the other hand,
there was a lot of disk activity at the same time, you might find the fifo level
dropping much lower.
.P
When you're sure that you won't have any problems, you can do the real thing:
just repeat the command without the \f(CW-dummy\fP option.  The output looks
almost identical.
.H2 "Copying CD-ROMs"
Frequently you'll want to make a verbatim copy of another CD.  There are
copyright implications here, of course, but many CD-ROMs are not restricted.  In
particular, you may make copies of FreeBSD CD-ROMs for your personal use.
.P
CD-ROMs are already in ISO format, of course, so to get a file \fIiso\fP, as in
the examples above, you could just perform a literal copy with
.Command dd \/:
.Dx
# \f(CBdd if=/dev/cd0c of=iso bs=128k\fP
.De
The \f(CWbs=128k\fP tells
.Command dd
to copy in blocks of 128 kB.  It's not strictly necessary, but if you omit it,
it will perform a separate transfer for every sector, and on a slow machine it
can be much less efficient.
.P
There's an even easier way, though, if you have two CD-ROM drives: you can
frequently copy directly from one drive to the other, without storing on disk at
all.  To do this, of course, you need to be very sure that your CD-ROM drive is
fast enough.  In particular, if it spins down during the copy, you will almost
certainly have underruns and a useless copy.  Be very sure to do a dummy run
first.  Let's assume that your second CD-ROM drive is
.Device cd1c
(a SCSI drive).  For IDE drives, write:
.Dx
# \f(CBburncd -f /dev/acd0c -t -v -s 8 data /dev/cd1c fixate\fP
.De
In this example, the \f(CW-f\fP option indicates that
.Device acd0c
is the (IDE) CD-R burner. 
.Device cd1c
is the (SCSI) CD-ROM drive with the original CD-ROM.  You don't need to mount
.Device cd1c ,
since it's being accessed as raw data, not a file system.
.P
When you're sure this will work, remove the \f(CW-t\fP flag and repeat.
.P
For SCSI, enter
.Dx
# \f(CBcdrecord -dummy -v dev=0,0,0 -speed=8 /dev/cd1c\fP
.De
When it completes satisfactorily, remove the \f(CW-dummy\fP and repeat.
.ig
 
Date: Thu, 1 May 2003 15:41:10 -0700
From: Joshua Oreman <oremanj@webserver.get-linux.org>
To: JR Richards <jr@ebcrp.org>
Cc: questions@freebsd.org
Subject: Re: DVD+RW and FreeBSD 4.7

On Thu, May 01, 2003 at 10:09:55AM -0700 or thereabouts, JR Richards seemed to write:
> Hello Folks,
> 
>  
> 
> I'm running FreeBSD 4.7 with a Sony DVD+RW drive and am trying to get
> this darn thing to work with burncd.
> 
> These are the packages that I added recently:
> Cdrtools-2.00
> Mkisofs-2.00
 
Not needed for +RW.
 
> 
> This is what happens when I try to run burncd by hand:
> 
> backup# burncd -q -f /dev/acd0c -s 10 data filename
> 
> burncd: ioctl(CDRIOCNEXTWRITEABLEADDR): Input/output error
> 
> Can anyone help me get this thing up?

Sure.
To format a DVD+RW (needs to be done only once):
# burncd -f /dev/acd0c format dvd+rw
To write a file:
# burncd -f /dev/acd0c -s max dvdrw filename
It will erase the DVD if there's already stuff on it.


Date: Thu, 01 May 2003 13:42:21 -0400
From: Bill Moran <wmoran@potentialtech.com>
To: JR Richards <jr@ebcrp.org>
Cc: FreeBSD Forum <freebsd-questions@FreeBSD.ORG>
Subject: Re: DVD+RW and FreeBSD 4.7
 
I don't specifically know about burncd, but reading this allowed me
to get cdrecord burning DVDs:
http://www.whiterose.net/~mrpink/freebsd/dvdr.php
Date: Thu, 1 May 2003 17:45:16 -0500 (CDT)
From: Eduardo Viruena Silva <mrspock@esfm.ipn.mx>
To: JR Richards <jr@ebcrp.org>
Cc: FreeBSD Forum <freebsd-questions@freebsd.org>
Subject: Re: DVD+RW and FreeBSD 4.7
 
On Thu, 1 May 2003, JR Richards wrote:

> Hello Folks,
>
>  
> 
> I'm running FreeBSD 4.7 with a Sony DVD+RW drive and am trying to get
> this darn thing to work with burncd.
> 
>
>
> These are the packages that I added recently:
>
> Cdrtools-2.00
>
> Mkisofs-2.00
>
>
> 
> This is what happens when I try to run burncd by hand:
> 
> backup# burncd -q -f /dev/acd0c -s 10 data filename
> 
> burncd: ioctl(CDRIOCNEXTWRITEABLEADDR): Input/output error
> 
>

It is  problem I had while using burncd in 5.0 and a DVD-RW also.

>
>   
> Can anyone help me get this thing up?
>
  
You may want to reconfigure your kernel.  Delete the atapi cdrom
entries and add

           device atapicam
           device scbus
           device cd
           device pass

rebuild your kernel and install it.

Now your DVD-WR will be seen as a SCSI burner and you will be
able to burn your cd using xcdroast, cdrecord, etc.


Date: Thu, 15 May 2003 09:54:46 +0100
From: Vince Hoffman <Vince.Hoffman@uk.circle.com>
To: "'Eduardo Viruena Silva'" <mrspock@esfm.ipn.mx>,
        "Greg 'groggy' Lehey" <grog@freebsd.org>
Cc: questions@freebsd.org,
        How Can ThisBe <howcanthisbe300@hotmail.com>
Subject: RE: How can I mount a cdrom .bin file?   
X-Mailer: Internet Mail Service (5.5.2653.19)
 

> > On Wednesday, 14 May 2003 at 21:17:22 +0000, How Can ThisBe wrote:
> > > Hello. I have a number of .bin CDROM images and I was 
> wondering how I
> > > can mount them? I know how to mount .iso CDROM images but 
> .bin is new
> > > to me :]
> >
>
<SNIP>
> .bin & .cue files can be read by daemontools [www.daemontools.org] for
> windows.  These guys took BSD cd9660 mounting programs and adapt them
> for windows.  I don't remember, but I believe they have
> legal problems for this.
> 
> You can "mount" that image in a virtual CD and then, with a 
> burner program
> you can build the iso image, just as "mdconfig -a -t vnode..." does.
> 
> Now, in FreeBSD, /usr/ports/sysutils/bchunk that help you
> to build the iso image:
> 
> bchunk is a Unix/C rewrite of the fine BinChunker software for some
> non-Unix systems.
<SNIP>
> ========================
 
Another way is described here.
<http://www.freebsdforums.org/forums/showthread.php?threadid=6816>
(first hit on google using "bin cue burn bsd") the cue file is a text
file that describes the layout of the bin file. Any program that
creates a .bin CD image should create a cue file also.


Date: Thu, 15 May 2003 16:04:01 -0700
From: Joshua Oreman <oremanj@webserver.get-linux.org>
To: Jim Sills <jsills@g3llc.com> 
Cc: questions@freebsd.org
Subject: Re: Writing DVD+RW

On Thu, May 15, 2003 at 02:45:39PM -0500 or thereabouts, Jim Sills seemed to write:
> Anybody riting DVD+RW disks??  Anybody ported growisofs??

As of 5.0 (and maybe 4.8), DVD+RW is supported in burncd.

To get you started (these commands all as root):
# Insert a DVD+RW
burncd -f /dev/acd0c format dvd+rw # <-- You only have to do this once
burncd -f /dev/acd0c -s max dvdrw imagefile.iso # <-- Burn a file
# The disk will be erased automatically if you burn something to
# it and there's already something on it
#
# Replace /dev/acd0c above with your burner /dev node

HTH,
-- Josh
..
