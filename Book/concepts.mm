.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: concepts.mm,v 4.21 2003/04/02 06:37:12 grog Exp grog $
.\"
.Chapter \*[nchconcepts] "Before you install"
FreeBSD runs on just about any modern PC, Alpha or 64 bit SPARC machine.  You
can skip this chapter and the next and move to chapter
.Sref "\*[nchquickinstall]" ,
and you'll have a very good chance of success.  Nevertheless, it makes things
easier to know the contents of this chapter before you start.  If you do run
into trouble, it will give you the background information you need to solve the
trouble quickly and simply.
.P
.X "laptop"
FreeBSD also runs on most Intel-based laptops; in general the considerations
above apply for laptops as well.  In the course of the book we'll see examples
of where laptops require special treatment.
.P
Most of the information here applies primarily to Intel platforms.  We'll look
at the Compaq Alpha architecture on page
.Sref \*[alpha-concepts] .
The first release of FreeBSD to support the SPARC 64 architecture is 5.0, and
support is still a little patchy.  At the time of going to press, it's not worth
describing, since it will change rapidly.  The instructions on the CD-ROM
distribution are currently the best source of information on running FreeBSD on
SPARC 64.
.\" XXX  point to .Sref \*[sparc-concepts]  when it's worthwhile.
.H2 "Using old hardware"
.Pn mouldy
FreeBSD runs on all relatively recent machines.  In addition, a lot of older
hardware that is available for a nominal sum, or even for free, runs FreeBSD
quite happily, though you may need to take more care in the installation.
.P
.ne 2v
FreeBSD does not support all PC hardware: the PC has been on the market for over
20 years, and it has changed a lot in that time.  In particular:
.Ls B
.LI
FreeBSD does not support 8 bit and 16 bit processors.  These include the 8086
and 8088, which were used in the IBM PC and PC-XT and clones, and the 80286,
used in the IBM PC-AT and clones.
.LI
The FreeBSD kernel no longer supports ST-506 and ESDI drives.  You're unlikely
to have any of these: they're now so old that most of them have failed.  The
\fIwd\fP\/ driver still includes support for them, but it hasn't been tested,
and if you want to use this kind of drive you might find it better to use
FreeBSD Release 3.  See page
.Sref \*[disk-hardware] \&
to find out how to identify these drives.  You can get Release 3 of FreeBSD from
\fIftp://ftp.FreeBSD.org/pub/FreeBSD/releases/i386/3.x-STABLE\fP.  You'll have
to perform a network installation.
.LI
Memory requirements for FreeBSD have increased significantly in the last few
years, and you should consider 16 MB a minimum size, though nobody has recently
checked whether it wouldn't install in, say, 12 MB.  FreeBSD Release 3 still
runs in 4 MB, though you need 5 MB for installation.
.Le
If you're planning to install FreeBSD on an old machine, consider the following
to be an absolute minimum:
.Ls B
.LI
PC with 80386 CPU, Alpha-based machine with SRM firmware.
.LI
16 MB memory (Intel) or 24 MB (Alpha).
.LI
80 MB free disk space (Intel).  Nobody has tried an installation on an Alpha or
SPARC machine with less than 500 MB, though you can probably reduce this value
significantly.
.Le
You don't absolutely need a keyboard and display board: many FreeBSD machines
run server tasks with neither keyboard nor display.  Even then, though, you may
find it convenient to put a display board in the machine to help in case you run
into trouble.
.P
When I say \fIabsolute\fP\/ minimum, I mean it.  You can't do very much with
such a minimal system, but for some purposes it might be adequate.  You can
improve the performance of such a minimal system significantly by adding memory.
Before you go to the trouble to even try such a minimal installation, consider
the cost of another 16 MB of memory.  And you can pick up better machines than
this second-hand for $50.  Is the hassle worth it?
.P
To get full benefits from a desktop or laptop FreeBSD system (but not from a
machine used primarily as a server), you should be running the X Window system.
This uses more memory.  Consider 32 MB a usable minimum here, though thanks to
FreeBSD's virtual memory system, this is not such a hard limit as it is with
some other systems.
.Aside
The speed of a virtual memory-based system such as FreeBSD depends at least as
much on memory performance as on processor performance.  If you have, say, a
486DX-33 and 16 MB of memory, upgrading memory to 32 MB will probably buy you
more performance than upgrading the motherboard to a Pentium 100 and keeping the
16 MB memory.  This applies for a usual mix of programs, in particular, programs
that don't perform number crunching.
.End-aside
Any SPARC 64 machine runs FreeBSD acceptably, as the machines are relatively
new.  If you're running Intel or Alpha, consider the following the minimum for
getting useful work done with FreeBSD and X:
.Ls B
.LI
PC with 80486DX/2-66, or Alpha-based machine
.LI
32 MB memory (i386) or 64 MB (Alpha)
.LI
SVGA display board with 2 MB memory, 1024x768
.LI
Mouse
.LI
200 MB free disk space
.Le
.Aside
Your mileage may vary.  During the review phase of an earlier edition of this
book, one of the reviewers stated that he was very happy with his machine, which
has a 486-33 processor, 16 MB main memory, and 1 MB memory on his display board.
He said that it ran a lot faster than his Pentium 100 at work, which ran
Microsoft.  The moral: if your hardware doesn't measure up to the recommended
specification, don't be discouraged.  Try it out anyway.
.End-aside
Beyond this minimum, FreeBSD supports a large number of other hardware
components.
.H3 "Device drivers"
.X kernel
.X "driver"
.X "device driver"
The FreeBSD kernel is the only part of the system that can access the hardware.
It includes \fIdevice drivers\fP, which control the function of peripheral
devices such as disks, displays and network boards.  When you install new
hardware, you need a driver for it.
.P
.X "kernel loadable module"
.X "kld"
There are two ways to get a driver into the kernel: you can build a kernel that
includes the driver code, or you can load a driver module (\fIKernel Loadable
Module\fP\/ or \fIkld\fP\/) into the kernel at run time.  Not all drivers are
available as klds.  If you need one of these drivers, and it's not included in
the standard kernel, you have to build a new kernel.  We look at building
kernels in Chapter
.Sref "\*[nchbuild]" .
.P
The kernel configuration supplied with FreeBSD distributions is called
\f(CWGENERIC\fP after the name of the configuration file that describes it.  It
contains support for most common devices, though support for some older hardware
is missing, usually because it conflicts with more modern drivers.  For a full
list of currently supported hardware, read the web page
\fIhttp://www.FreeBSD.org/releases/\fP\/ and select the link \fIHardware
Notes\fP\/ for the release you're interested in.  This file is also available on
installed FreeBSD systems as
.URI /usr/share/doc/en_US.ISO_8859-1/books/faq/hardware.html .
It is also available in other languages; see the subdirectories of
\fI/usr/share/doc\fP.
.H2 "PC Hardware"
This section looks at the information you need to understand to install FreeBSD
on the i386 architecture.  In particular, in the next section we'll look at how
FreeBSD detects hardware, and what to do if your hardware doesn't correspond to
the system's expectations.  On page
.Sref \*[disk-hardware] \&
we'll see how FreeBSD and other PC operating systems handle disk space, and how
to set up your disk for FreeBSD.
.P
Some of this information also applies to the Alpha and SPARC 64 architectures.
We'll look at the differences for the Alpha architecture on page
.Sref \*[alpha-concepts] .
Currently the SPARC 64 implementation is changing too fast to describe it in a
meaningful manner.
.P
.X "IBM"
Since the original PC, a number of hardware standards have come, and some have
gone:
.Ls B
.LI
The original PC had an 8 bit bus.  Very few of these cards are still available,
but they are compatible with the ISA bus (see the next item).
.LI
.X "Industry Standard Architecture"
.X "ISA"
The PC AT, introduced in 1984, had a 16 bit 80286 processor.  To support this
processor, the bus was widened to 16 bits.  This bus came to be known as the
\fIIndustry Standard Architecture\fP, or \fIISA\fP.  This standard is still not
completely dead, and many new motherboards support it.  Most older motherboards
have a number of ISA slots.
.P
.LI
.X "Microchannel Architecture"
.X "MCA"
The ISA bus has a number of severe limitations, notably poor performance.  This
became a problem very early.  In 1985, IBM introduced the PS/2 system, which
addressed this issue with a new bus, the so-called \fIMicrochannel
Architecture\fP\/ or \fIMCA\fP.  Although successful for IBM, MCA was not
adopted by other manufacturers, and FreeBSD does not support it at all.  IBM no
longer produces products based on MCA.
.LI
.X "EISA"
In parallel to MCA, other manufacturers introduced a bus called the \fIExtended
Industry Standard Architecture\fP, or \fIEISA\fP.  As the name suggests, it is a
higher-performance extension of ISA, and FreeBSD supports it.  Like MCA, it is
obsolete.
.LI
.X "local bus"
EISA still proved to be not fast enough for good graphics performance.  In the
late 80s, a number of \fIlocal bus\fP\/ solutions appeared.  They had better
performance, but some were very unreliable.  FreeBSD supported most of them, but
you can't rely on it.  It's best to steer clear of them.
.LI
.X "Peripheral Component Interconnect"
.X "PCI"
Finally, in the early 1990s, Intel brought out a new bus called \fIPeripheral
Component Interconnect\fP, or \fIPCI\fP.  PCI is now the dominant bus on a
number of architectures.  Most modern PC add-on boards are PCI.
.P
Compared to earlier buses, PCI is much faster.  Most boards have a 32 bit wide
data bus, but there is also a 64 bit PCI standard.  PCI boards also contain
enough intelligence to enable the system to configure them, which greatly
simplifies installation of the system or of new boards.
.LI
.X "AGP"
.X "Accelerated Graphics Port"
Modern motherboards also have an \fIAGP\fP\/ (\fIAccelerated Graphics Port\fP\/)
slot specifically designed to support exactly one graphic card.  As the name
implies, it's faster even than PCI, but it's optimized for graphics only.
FreeBSD supports it, of course; otherwise it couldn't run on modern hardware.
.LI
.X "PC Card"
.X "PCMCIA"
.X "CardBus"
Most laptops have provision for external plug-in cards that conform to the
\fIPC Card\fP\/ (formerly called \fIPCMCIA\fP\/) or \fICardBus\fP\/ standards.
These cards are designed to be inserted into and removed from a running system.
FreeBSD has support for these cards; we'll look at them in more detail on page
.\" XXX check
.Sref "\*[pcmcia]" .
.LI
.X "Universal Serial Bus"
.X "USB"
More and more, the basic serial and parallel ports installed on early PCs are
being replaced by a \fIUniversal Serial Bus\fP\/ or \fIUSB\fP.  We'll look at it
on page
.Sref "\*[usb]" .
.Le
.sp -1v
.H2 "How the system detects hardware"
.X "probing"
When the system starts, each driver in the kernel examines the system to find
any hardware that it might be able to control.  This examination is called
\fIprobing\fP.  Depending on the driver and the nature of the hardware it
supports, the probe may be clever enough to set up the hardware itself, or to
recognize its hardware no matter how it has been set up, or it may expect the
hardware to be set up in a specific manner in order to find it.  In general, you
can expect PCI drivers to be able to set up the card to work correctly.  In the
case of ISA or EISA cards, you may not be as lucky.
.H2 "Configuring ISA cards"
ISA cards are rapidly becoming obsolete, but sometimes they're still useful:
.Ls B
.LI
ISA graphics cards are very slow in comparison with modern graphic cards, but if
you just want a card for maintenance on a server machine that normally doesn't
display anything, this is an economical alternative.
.LI
Some ISA disk controllers can be useful, but they are sharply limited in
performance.
.LI
ISA Ethernet cards may be a choice for low-volume networking.
.LI
Many ISA serial cards and built-in modems are still available.
.Le
Most ISA cards require some configuration.  There are four main parameters that
you may need to set for PC controller boards:
.Ls N
.LI
.X "port address"
The \fIport address\fP\/ is the address of the first of possibly several control
registers that the driver uses to communicate with the board.  It is normally
specified in hexadecimal, for example \f(CW0x320\fP.
.Aside
If you come from a Microsoft background, you might be more used to the notation
\f(CW320H\fP.  The notation \f(CW0x320\fP comes from the C programming language.
You'll see a lot of it in UNIX.
.End-aside
Each board needs its own address or range of addresses.  The ISA architecture
has a sharply limited address range, and one of the most frequent causes of
problems when installing a board is that the port addresses overlap with those
of another board.
.P
Beware of boards with a large number of registers.  Typical port addresses end
in (hexadecimal) \f(CW0\fP.  Don't rely on being able to take any unoccupied
address ending in \f(CW0\fP, though: some boards, such as Novell NE2000
compatible Ethernet boards, occupy up to 32 registers\(emfor example, from
\f(CW0x320\fP to \f(CW0x33f\fP.  Note also that a number of addresses, such as
the serial and parallel ports, often end in \f(CW8\fP.
.LI
.Pn IRQ
.X "interrupt requests, available"
.X "IRQ"
Boards use an \fIInterrupt Request\fP, also referred to as \fIIRQ\fP, to get the
attention of the driver when a specific event happens.  For example, when a
serial interface reads a character, it generates an interrupt to tell the driver
to collect the character.  Interrupt requests can sometimes be shared, depending
on the driver and the hardware.  There are even fewer interrupt requests than
port addresses: a total of 15, of which a number are reserved by the
motherboard.  You can usually expect to be able to use IRQs 3, 4, 5, 7, 9, 10,
11 and 12.  IRQ 2 is special: due to the design of the original IBM PC/AT, it is
the same thing as IRQ 9.  FreeBSD refers to this interrupt as IRQ 9.
.P
As if the available interrupts weren't already restricted enough, ISA and PCI
boards use the same set of interrupt lines.  PCI cards can share interrupt lines
between multiple boards, and in fact the PCI standard only supports four
interrupts, called INTA, INTB, INTC and INTD.  In the PC architecture they map
to four of the 15 ISA interrupts.  PCI cards are self-configuring, so all you
need to do is to ensure that PCI and ISA interrupts don't conflict.  You
normally set this up in a BIOS setup menu.
.LI
.X "Direct Memory Access"
.X "DMA"
.X "DMA, Request"
.X "DMA, Acknowledge"
.X "DMA, Channel"
Some high-speed devices perform \fIDirect Memory Access\fP, also known as
\fIDMA\fP, to transfer data to or from memory without CPU intervention.  To
transfer data, they assert a \fIDMA Request\fP\/ (DRQ) and wait for the bus to
reply with a \fIDMA Acknowledge\fP\/ (DACK).  The combination of DRQ and DACK is
sometimes called a \fIDMA Channel\fP.  The ISA architecture supplies 7 DMA
channels, numbered 0 to 3 (8 bit) and 5 to 7 (16 bit).  The floppy driver uses
DMA channel 2.  DMA channels may not be shared.
.LI
.X "memory, I/O"
.X "I/O memory"
.X "IOmem"
Finally, controllers may have on-board memory, sometimes referred to as \fII/O
memory\fP\/ or \fIIOmem\fP.  It is usually located at addresses between
\f(CW0xa0000\fP and \f(CW0xeffff\fP.
.Le
If the driver only looks at specific board configurations, you can set the board
to match what the driver expects, typically by setting jumpers or using a
vendor-supplied diagnostic program to set on-board configuration memory, or you
can build a kernel to match the board settings.
.H2 "PCMCIA, PC Card and CardBus"
.Pn pcmcia
.X "PC Card"
.X "CardBus"
.X "PCMCIA"
Laptops don't have enough space for normal PCI expansion slots, though many use
a smaller PCI card format.  It's more common to see \fIPC Card\fP\/ or
\fICardBus\fP\/ cards, though.  PC Card was originally called \fIPCMCIA\fP,
which stands for \fIPersonal Computer Memory Card International Association\fP:
the first purpose of the bus was to expand memory.  Nowadays memory expansion is
handled by other means, and PC Card cards are usually peripherals such as
network cards, modems or disks.  It's true that you can insert compact flash
memory for digital cameras into a PC Card adapter and access it from FreeBSD,
but even in this case, the card looks like a disk, not a memory card.
.P
The original PC Card standard already has one foot in the grave: it's a 16 bit
bus that doesn't work well with modern laptops.  The replacement standard has a
32 bit wide bus and is called \fICardBus\fP.  The cards look almost identical,
and most modern laptops support both standards.  In this book I'll use use the
term \fIPC Card\fP\/ to include CardBus unless otherwise stated.  FreeBSD
Release 5 includes completely new PC Card code.  It now supports both 16 bit PC
Card and 32 bit CardBus cards.
.P
PC Card offers one concept that conventional cards don't: the cards are \fIhot
swappable\fP.  You can insert them and remove them in a running system.  This
poses a number of potential problems, some of which are only partially solved.
.H3 "PC Card and CardBus cards"
PC Card and CardBus both use the same form factor cards: they are 54 mm wide and
at least 85 mm long, though some cards, noticeably wireless networking cards,
are up to 120 mm long and project beyond the casing of the laptop.  The wireless
cards contain an antenna in the part of the card that projects from the
machine.
.P
PC Card cards can have one of three standard thicknesses:
.Ls B
.LI
\fIType 1\fP\/ cards are 3.3 mm thick. They're very uncommon.
.LI
\fIType 2\fP\/ cards are 5 mm thick.  These are the most common type, and most
laptops take two of them.
.LI
\fIType 3\fP\/ cards are 10.5 mm thick.  In most laptops you can normally insert
either one type 3 card or two type 2 cards.
.Le
The \f(CWGENERIC\fP FreeBSD kernel contains support for PC Card, so you don't
need to build a new kernel.
.H2 "Universal Serial Bus"
.Pn usb
.\" See http://www.usbdeveloper.com/UnderstandUSB/understandusb.htm
.X "Universal Serial Bus"
.X "USB"
The \fIUniversal Serial Bus\fP\/ (\fIUSB\fP\/) is a new way of connecting
external peripherals, typically those that used to be connected by serial or
parallel ports.  It's much faster than the old components: the old serial
interface had a maximum speed of 115,200 bps, and the maximum you can expect to
transfer over the parallel port is about 1 MB/s.  By comparison, current USB
implementations transfer data at up to 12 Mb/s, and a version with 480 Mb/s is
in development.
.P
As the name states, USB is a \fIbus\fP\/: you can connect multiple devices to a
bus.  Currently the most common devices are mid-speed devices such as printers
and scanners, but you can connect just about anything, including keyboards,
mice, Ethernet cards and mass storage devices.
.H2 "Disks"
.Pn disk-hardware
.X "IDE"
A number of different disks have been used on PCs:
.Ls B
.LI
.X "ST-506"
.X "cable, control"
.X "cable, data"
.X "control cable"
.X "data cable"
\fIST-506\fP\/ disks are the oldest.  You can recognize them by the fact that
they have two cables: a \fIcontrol cable\fP\/ that usually has connections for
two disks, and a thinner \fIdata cable\fP\/ that is not shared with any other
disk.  They're just about completely obsolete by now, but FreeBSD Release 3
still supports them with the \fIwd\fP\/ driver.  These disks are sometimes
called by their modulation format, \fIModified Frequency Modulation\fP\/ or
\fIMFM\fP.  A variant of MFM that offers about 50% more storage is \fIRLL\fP\/
or \fIRun Length Limited\fP modulation.  From the operating system point of
view, there is no difference between MFM and RLL.
.LI
.X "ESDI"
.X "Enhanced Small Device Interface"
\fIESDI\fP\/ (\fIEnhanced Small Device Interface\fP\/) disks were designed to
work around some of the limitations of ST-506 drives.  They also use the same
cabling as ST-506, but they are not hardware compatible, though most ESDI
controllers understand ST-506 commands.  They are also obsolete, but the
\fIwd\fP\/ driver in FreeBSD Release 3 supports them, too.
.LI
.Pn 504m-limit
.X "IDE"
.X "Integrated Device Electronics"
.Pn EIDE
.X "EIDE"
.X "enhanced IDE"
.X "ATA"
.X "AT Attachment"
\fIIDE\fP\/ (\fIIntegrated Device Electronics\fP\/), now frequently called
\fIATA\fP\/ (\fIAT Attachment\fP\/), is the current low-cost PC disk interface.
It supports two disks connected by a single 40 or 80 conductor flat cable.  The
connectors for both cables are the same, but the 80 conductor cable is needed
for the 66 MHz, 100 MHz and 133 MHz transfer rates supported by recent disk
drives.
.P
All modern IDE disks are so-called \fIEIDE\fP\/ (\fIEnhanced IDE\fP\/) drives.
The original IDE disks were limited by the PC BIOS standard to a size of 504 MB
(1024 * 16 * 63 * 512, or 528,482,304 bytes).  EIDE drives exceed this limit by
several orders of magnitude.
.P
.X "programmed I/O"
.X "PIO"
.Pn use-DMA
A problem with older IDE controllers was that they used \fIprogrammed I/O\fP\/
or \fIPIO\fP\/ to perform the transfer.  In this mode, the CPU is directly
involved in the transfer to or from the disk.  Older controllers transferred a
byte at a time, but more modern controllers can transfer in units of 32 bits.
Either way, disk transfers use a large amount of CPU time with programmed I/O,
and it's difficult to achieve the transfer rates of modern IDE drives, which can
be as high as 100 MB/s.  During such transfers, the system appears to be
unbearably slow: it ``grinds to a halt.''
.P
.X "UDMA"
.X "Ultra DMA"
To solve this problem, modern chipsets offer DMA transfers, which almost
completely eliminate CPU overhead.  There are two kinds of DMA, each with
multiple possible transfer modes.  The older \fIDMA\fP\/ mode is no longer in
use.  It handled transfer rates between 2.1 MB/s and 16.7 MB/s.  The newer
\fIUDMA\fP\/ (\fIUltra DMA\fP\/) mode supports transfer rates between 16.7 MB/s
and 133 MB/s.  Current disks use UDMA33 (33 MHz transfer rate), which is the
fastest rate you can use with a 40 conductor cable, and UDMA66 (66 MHz), UDMA100
(100 MHz) and UDMA-133 (133 MHz) with an 80 conductor cable.  To get this
transfer rate, both the disk and the disk controller must support the rate.
FreeBSD supports all UDMA modes.
.P
.X "tagged queueing"
Another factor influencing IDE performance is the fact that most IDE controllers
and disks can only perform one transfer at a time.  If you have two disks on a
controller, and you want to access both, the controller serializes the requests
so that a request to one drive completes before the other starts.  This results
in worse performance than on a SCSI chain, which does not have this restriction.
If you have two disks and two controllers, it's better to put one disk on each
controller.  This situation is gradually changing, so when choosing hardware
it's worth checking on current support for \fItagged queueing\fP, which allows
concurrent transfers.
.LI
.X "SCSI"
.X "Small Computer Systems Interface"
.X "host adapter"
.X "wide SCSI"
.X "scuzzy"
\fISCSI\fP\/ is the \fISmall Computer Systems Interface\fP.  It's usually
pronounced ``scuzzy.''  It is used for disks, tapes, CD-ROMs and also other
devices such as scanners and printers.  The SCSI controller is more correctly
called a \fIhost adapter\fP.  Like IDE, SCSI has evolved significantly over
time.  SCSI devices are connected by a single flat cable, with 50 conductors
(``narrow SCSI,'' which connects a total of 8 devices) or 68 conductors (``wide
SCSI,'' which also connects up to 16 devices).  Some SCSI devices have
subdevices, for example CD-ROM changers.
.P
SCSI drives have a reputation for much higher performance than IDE.  This is
mainly because nearly all SCSI host adapters support DMA, whereas in the past
IDE controllers usually used programmed I/O.  In addition, SCSI host adapters
can perform transfers from multiple units at the same time, whereas IDE
controllers can only perform one transfer at a time.  Typical SCSI drives are
still faster than IDE drives, but the difference is nowhere near as large as it
used to be.  Narrow SCSI can support transfer rates of up to 40 MB/s (Ultra 2),
and wide SCSI can support rates of up to 320 MB/s (Ultra 320).  These speeds are
not necessarily faster than IDE: you can connect more than seven times as many
devices to a wide SCSI chain.
.Le
.SPUP
.H2 "Disk data layout"
Before you install FreeBSD, you need to decide how you want to use the disk
space available to you.  If desired, FreeBSD can coexist with other operating
systems on the Intel platform.  In this section, we'll look at the way data is
laid out on disk, and what we need to do to create FreeBSD file systems on disk.
.H3 "PC BIOS and disks"
.X "read/write head"
.X "head, read/write"
The basics of disk drives are relatively straightforward: data is stored on one
or more rotating disks with a magnetic coating similar in function to the
coating on an audio tape.  Unlike a tape, however, disk heads do not touch the
surface: the rotating disk produces an air pressure against the head, which
keeps it floating very close to the surface.  The disk has (usually) one
\fIread/write head\fP\/ for each surface to transfer data to and from the
system.  People frequently talk about the number of heads, not the number of
surfaces, though strictly speaking this is incorrect: if there are two heads per
surface (to speed up access), you're still interested in the number of surfaces,
not the number of heads.
.P
.X "track"
.X "sectors"
.X "cylinder"
While transferring data, the heads are stationary, so data is written on disks
in a number of concentric circular \fItracks\fP.  Logically, each track is
divided into a number of \fIsectors\fP, which nowadays almost invariably contain
512 bytes.  A single positioning mechanism moves the heads from one track to
another, so at any one time all the tracks under the current head position can
be accessed without repositioning.  This group of tracks is called a
\fIcylinder\fP.
.P
.X "Logical Block Addressing"
.Pn LBA
.X "LBA"
.X "CHS"
Since the diameter of the track differs from one track to the other, so does the
storage capacity per track.  Nevertheless, for the sake of simplicity, older
drives, such as ST-506 (MFM and RLL) drives, had a fixed number of sectors per
track.  To perform a data transfer, you needed to tell the drive which cylinder,
head and sector to address.  This mode of addressing is thus called \fICHS\fP\/
addressing.
.P
Modern disks have a varying number of sectors per track on different parts of
the disk to optimize the storage space, and for the same reason they normally
store data on the disk in much larger units than sectors.  Externally, they
translate the data into units of sectors, and they also optionally maintain the
illusion of ``tracks'' and ``heads,'' though the values have nothing to do with
the internal organization of the disk.  Nevertheless, BIOS setup routines still
give you the option of specifying information about disk drives in terms of the
numbers of cylinders, heads and sectors, and some insist on it.  In reality,
modern disk drives address sectors sequentially, so-called \fILogical Block
Addressing\fP\/ or \fILBA\fP.  CHS addressing has an additional problem: various
standards have limited the size of disks to 504 MB or 8 GB.  We'll look at that
in more detail on page
.Sref \*[sizelimit] .
.P
SCSI drives are a different matter: the system BIOS normally doesn't know
anything about them.  They are always addressed in LBA mode.  It's up to the
host adapter to interrogate the drive and find out how much space is on it.
Typically, the host adapter has a BIOS that interrogates the drive and finds
its dimensions.  The values it determines may not be correct: the PC BIOS 1 GB
address limit (see page
.Sref \*[1GB-limit] )
might bite you.  Check your host adapter documentation for details.
.H3 "Disk partitioning"
.Pn partitions
.X "partition"
.X "partition table"
.X "partition, logical"
.X "logical partition"
The PC BIOS divides the space on a disk into up to four \fIpartitions\fP, headed
by a \fIpartition table\fP.  For Microsoft systems, each partition may be
either a \fIprimary partition\fP that contains a file system (a ``drive'' in
Microsoft terminology), or an \fIextended partition\fP that contains multiple
file systems (or ``logical partitions'').
.P
.X "slice, defined"
FreeBSD does not use the PC BIOS partition table directly.  It maintains its own
partitioning scheme with its own partition table.  On the PC platform, it places
this partition table in a single PC BIOS partition, rather in the same way that
a PC BIOS extended partition contains multiple ``logical partitions.''  It
refers to PC BIOS partitions as ``slices.''
.Pn define-slice
.Aside
This double usage of the word \fIpartition\fP\/ is really confusing.  In this
book, I follow BSD usage, but I continue to refer to the PC BIOS partition table
by that name.
.End-aside
Partitioning offers the flexibility that other operating systems need, so it has
been adopted by all operating systems that run on the PC platform.  Figure
\*[MS-partition-table] shows a disk with all four slices allocated.
.X "partition table"
.X "partition, active"
The \fIPartition Table\fP\/ is the most important data structure.  It contains
information about the size, location and type of the slices (PC partitions).
The PC BIOS allows one of these slices to be designated as \fIactive\fP\/: at
system startup time, its bootstrap record is used to start the system.
.P
The partition table of a boot disk also contains a \fIMaster Boot Record\fP\/
(\fIMBR\fP\/), which is responsible for finding the correct slice and booting it.
The MBR and the partition table take up the first sector on disk, and many
people consider them to be the same thing.  You only need an MBR on disks from
which you boot the system.
.P
.PS
h = .2i
dh = .02i
dw = 2i
boxht = .4i
move right 2.35i
down
[
        [
                boxwid = dw
          M:    box ht .15i "Master Boot Record"
          P:    box ht .15i "Partition Table"
          A:    box ht boxht
          B:    box ht boxht
          C:    box ht boxht
          D:    box ht boxht
                "Partition (slice) 1" at A above
                "\fI/dev/da0s1\fP\/" at A below
                "Partition (slice) 2" at B above
                "\fI/dev/da0s2\fP\/" at B below
                "Partition (slice) 3" at C above
                "\fI/dev/da0s3\fP\/" at C below
                "Partition (slice) 4" at D above
                "\fI/dev/da0s4\fP\/" at D below
                ]
]
.PE
.Figure-heading "Partition table"
.Fn MS-partition-table
.X "master boot record"
.X "MBR"
.X "primary partition"
.X "extended partition"
PC usage designates at least one slice as the \fIprimary partition\fP, the
\f(CWC:\fP drive.  Another slice may be designated as an \fIextended
partition\fP\/ that contains the other ``drives'' (all together in one slice).
.P
.X "file system"
.X "swap, partition"
.X "UNIX partitions"
.X "partition, UNIX"
.X "da0, device"
.X "device, da0"
UNIX systems have their own form of partitioning which predates the PC and is
not compatible with the PC method.  As a result, all versions of UNIX that can
coexist with Microsoft implement their own partitioning within a single slice
(PC BIOS partition).  This is conceptually similar to an extended partition.
FreeBSD systems define up to eight partitions per slice.  They can be used for
the following purposes:
.Ls B
.LI
.X "file system"
A partition can be a \fIfile system\fP, a structure in which UNIX stores files.
.LI
.X "swap, partition"
It can be used as a \fIswap partition\fP.  FreeBSD uses virtual memory: the
total addressed memory in the system can exceed the size of physical memory, so
we need space on disk to store memory pages that don't fit into physical
memory.  Swap is a separate partition for performance reasons: you can use files
for swap, like Microsoft does, but it is much less efficient.
.LI
.X "vinum"
The partition may be used by other system components.  For example, the
\fIVinum\fP\/ volume manager uses special partitions as building blocks for
volumes.  We'll look at Vinum on page
.Sref \*[vinum] .
.LI
The partition may not be a real partition at all.  For example, partition
\f(CWc\fP refers to the entire slice, so it overlaps all the rest.  For obvious
reasons, the partitions that represent file systems and swap space (\f(CWa\fP,
\f(CWb\fP, and \f(CWd\fP through \f(CWh\fP) should not overlap.
.Le
.X "character device"
.X "device, character"
.H3 "Block and character devices"
.Pn block-device
Traditional UNIX treats disk devices in two different ways.  As we have seen,
you can think of a disk as a large number of sequential blocks of data.  Looking
at it like this doesn't give you a file system\(emit's more like treating it as
a tape.  UNIX calls this kind of access \fIraw\fP\/ access.  You'll also hear
the term \fIcharacter device\fP.
.P
.X "buffer cache"
Normally, of course, you want files on your disk: you don't care where they are,
you just want to be able to open them and manipulate them.  In addition, for
performance reasons the system keeps recently accessed data in a \fIbuffer
cache\fP.  This involves a whole lot more work than raw devices.
.X "block device"
.X "device, block"
These devices are called \fIblock devices\fP.
.P
By contrast with UNIX, Linux originally did not have character disk devices.
Starting with Release 4.0, FreeBSD has taken the opposite approach: there are
now no user-accessible block devices any more.  There are a number of reasons
for this:
.Ls B
.LI
Having two different names for devices is confusing.  In older releases of
FreeBSD, you could recognize block and character devices in an \f(CWls -l\fP
listing by the letters \f(CWb\fP and \f(CWc\fP at the beginning of the
permissions.  For example, in FreeBSD 3.1 you might have seen:
.Dx
$ \f(CBls -l /dev/rwd0s1a   /dev/wd0s1a\fP
crw-r-----  1 root  operator    3, 131072 Oct 31 19:59 /dev/rwd0s1a
brw-r-----  1 root  operator    0, 131072 Oct 31 19:59 /dev/wd0s1a
.De
\fIwd\fP\/ is the old name for the current \fIad\fP\/ disks.  The question is:
when do you use which one?  Even compared to UNIX System V, the rules were
different.
.LI
Nearly all access to disk goes via the file system, and user-accessible block
devices add complication.
.LI
If you write to a block device, you don't automatically write to the disk, only
into buffer cache.  The system decides when to write to disk.  If there's a
problem writing to disk, there's no way to notify the program that performed the
write: it might even already have finished.  You can demonstrate this very
effectively by comparing the way FreeBSD and Linux write to a floppy disk.  It
takes 50 seconds to write a complete floppy disk\(emthe speed is determined by
the hardware, so the FreeBSD copy program finishes after 50 seconds.  With
Linux, though, the program runs only for a second or two, after which it
finishes and you get your prompt back.  In the meantime, the system flushes the
data to floppy: you still need to wait a total of 50 seconds.  If you remove the
floppy in this time, you obviously lose data.
.Le
The removal of block devices caused significant changes to device naming.  In
older releases of FreeBSD, the device name was the name of the block device, and
the raw (character) device had the letter \f(CWr\fP at the beginning of the
name, as shown in the example above.
.P
Let's look more carefully at how BSD names its partitions:
.Ls B
.LI
.X "device node"
.X "device file system"
.X "devfs"
Like all other devices, the \fIdevice nodes\fP, the entries that describe the
devices, are stored in the directory \fI/dev\fP.  Unlike traditional UNIX and
older releases of FreeBSD, FreeBSD Release 5 includes the \fIdevice file
system\fP\/ or \fIdevfs\fP, which creates the device nodes automatically, so you
don't need to worry about creating them yourself.
.LI
Next comes the name of the driver.  As we have seen, FreeBSD has drivers for IDE
and friends (\f(CWad\fP), SCSI disks (\f(CWda\fP) and floppy disks (\f(CWfd\fP).
For SCSI disks, we now have the name \fI/dev/da\fP.
.br
.ne 3v
.Aside
The original releases of FreeBSD had the abbreviation \f(CWwd\fP for IDE drives.
This abbreviation arose because the most popular of the original MFM controllers
were made by Western Digital.  Others claim, however, that it's an abbreviation
for ``Winchester Disk.''  SCSI disks were originally abbreviated \f(CWsd\fP.
The name \f(CWda\fP comes from the CAM standard and is short for \fIdirect
access\fP.  BSD/OS, NetBSD and OpenBSD still use the old names.
.End-aside
.sp -.5v
.LI
Next comes the unit number, generally a single digit.  For example, the first
SCSI disk on the system would normally be called \fI/dev/da0\fP.
.Aside
Generally, the numbers are assigned during the boot probes, but you can reserve
numbers for SCSI disks if you want.  This prevents the removal of a single disk
from changing the numbers of all subsequent drives.  See page
.Sref \*[device-hints] \&
for more details.
.End-aside
.sp -.5v
.LI
.X "slice name, strict"
.X "strict slice name"
Next comes the partition information.  The so-called \fIstrict slice name\fP\/
is specified by adding the letter \f(CWs\fP (for \fIslice\fP\/) and the slice
number (1 to 4) to the disk name.  BSD systems name partitions by appending the
letters \f(CWa\fP to \f(CWh\fP to the disk name.  Thus, the first partition of
the first slice of our disk above (which would typically be a root file system)
would be called \fI/dev/da0s1a\fP.
.P
.X "compatibility slice name"
.X "slice name, compatibility"
Some other versions of BSD do not have the same support for slices, so they use
a simpler terminology for the partition name.  Instead of calling the root file
system \fI/dev/da0s1a\fP, they refer to it as \fI/dev/da0a\fP.  FreeBSD supports
this method as well\(emit's called \fIcompatibility slice naming\fP.  The
compatibility slice is simply the first FreeBSD slice found on the disk, and
the partitions in this slice have two different names, for example
.Device ad0s1a
and
.Device ad0a .
.LI
Partition \f(CWc\fP is an exception: by convention, it represents the whole BSD
disk (in this case, the slice in which FreeBSD resides).
.LI
In addition, NetBSD reserves partition \f(CWd\fP for the entire disk, including
other partitions.  FreeBSD no longer assigns any special significance to
partition \f(CWd\fP.
.Le
Figure
.Sref \*[shared-partitions] \&
shows a typical layout on a system with a single SCSI disk, shared between
Microsoft and FreeBSD.  You'll note that partition \fI/dev/da0s3c\fP\/ is
missing from the FreeBSD slice, since it isn't a real partition.  Like the PC
BIOS partition table, the disk label contains information necessary for FreeBSD
to manage the FreeBSD slice, such as the location and the lengths of the
individual partitions.  The bootstrap is used to load the kernel into memory.
We'll look at the boot process in more detail in Chapter
.Sref \*[nchstarting] .
.PS
h = .2i
dh = .02i
dw = 1.7i
move right .5i
down
[
        boxht = h; boxwid = 1.8i
        box ht .15i "Master Boot Record"
        box ht .15i "Partition Table"
        box ht .2i "Bootstrap"
    P1: box ht .4i
    P2: box ht .4i
    P3: box ht .4i

        move right 1.6i from P1.ne
        boxwid = 2i
        down
     C: box ht .25i "PC BIOS \f(CWC:\fP drive"
        move .1i
     D: box ht .25i "PC BIOS \f(CWD:\fP drive \fI/dev/da0s5\fP\/"
     E: box ht .25i "PC BIOS \f(CWE:\fP drive \fI/dev/da0s6\fP\/"

        move .25i
    FA: box ht .2i "\fI/dev/da0s3a\fP\/: \fI/\fP\/ file system"
        box ht .2i "\fI/dev/da0s3b\fR: swap"
        box ht .2i "\fI/dev/da0s3d\fR: unused"
        box ht .2i "\fI/dev/da0s3e\fR: \fI/usr\fP\/ file system"
        box ht .2i "\fI/dev/da0s3f\fR: unused"
        box ht .2i "\fI/dev/da0s3g\fR: unused"
        box ht .2i "\fI/dev/da0s3h\fR: unused"

        arrow from P1.e to C.w
        arrow from P2.e to D.w
        arrow from P3.e to FA.w
        move up .08i from P1.c  "Slice 1 - PC BIOS primary"
        move down .08i from P1.c  "\fI/dev/da0s1\fP\/"
        move up .08i from P2.c  "Slice 2 - PC BIOS extended"
        move down .08i from P2.c  "\fI/dev/da0s2\fP\/"
        move up .08i from P3.c  "Slice 3 - FreeBSD"
        move down .08i from P3.c  "\fI/dev/da0s3\fP\/"
        ]
.PE
.Figure-heading "Partition table with FreeBSD file system"
.Fn shared-partitions
.P
Table
.Sref \*[partition-table] \&
gives you an overview of the devices that FreeBSD defines for this disk.
.ne 2i
.Pn disk-partitions
.Table-heading "Disk partition terminology"
.nf
.Tn partition-table
.TS H
tab(#) ;
lfCWp9 | lw51 .
\s10\fBSlice name#\fBUsage
_
.TH
/dev/da0s1#T{
First slice (PC BIOS \f(CWC:\fP partition)
T}
/dev/da0s2#T{
Second slice (PC BIOS extended partition)
T}
/dev/da0s3#T{
Third slice (PC BIOS partition), FreeBSD
T}
/dev/da0s5#T{
First drive in extended PC BIOS partition (\f(CWD:\fP)
T}
/dev/da0s6#T{
Second drive in extended PC BIOS partition (\f(CWE:\fP)
T}
/dev/da0s3a#T{
Third slice (PC BIOS partition), partition \f(CWa\fP (root file system)
T}
/dev/da0s3b#T{
Third slice (PC BIOS partition), partition \f(CWb\fP (swap space)
T}
/dev/da0s3c#T{
Third slice (PC BIOS partition), entire partition
T}
/dev/da0s3e#T{
Third slice (PC BIOS partition), partition \f(CWe\fP (\fI/usr\fP\/ file system)
T}
/dev/da0a#T{
Compatibility partition, root file system, same as
.Device da0s1a
T}
/dev/da0b#T{
Compatibility partition, swap partition, same as
.Device da0s1b
T}
/dev/da0c#T{
Whole BSD slice, same as \fI/dev/da0s1c\fP\/
T}
/dev/da0e#T{
Compatibility partition, \fIusr\fP\/ file system, same as
.Device da0s1e
T}
_
.TE
.fi
.H2 "Making the file systems"
.Pn make-fs
Armed with this knowledge, we can now proceed to make some decisions about how
to install our systems.  First, we need to answer some questions:
.Ls B
.LI
Do we want to share this disk with any other operating system?
.LI
If so, do we have data on this disk that we want to keep?
.Le
..if needed
We won't consider the question of an upgrade FreeBSD installation here.  There
is some material on the subject in the online handbook, but this is still an
area that needs more work.
..endif
.P
.X "FDISK, MS-DOS program"
.X "MS-DOS program, FDISK"
If you already have another system installed on the disk, it is best to use that
system's tools for manipulating the partition table.  FreeBSD does not normally
have difficulty with partition tables created by other systems, so you can be
reasonably sure that the other system will understand what it has left.  If the
other system is Microsoft, and you have a slice that you don't need, use the
MS-DOS \fIFDISK\fP\/ program to free up enough space to install FreeBSD.  If you
don't have a slice to delete, you can use the \fIFIPS\fP\/ program to create
one\(emsee
.Sref "\*[chinstall]" ,
page
.Sref \*[FIPS] .
.P
.Pn fdisk
.X "sysinstall, command"
.X "command, sysinstall"
If for some reason you can't use MS-DOS \fIFDISK\fP, for example because you're
installing FreeBSD by itself, FreeBSD also supplies a program called
.Command fdisk
that manipulates the partition table.  Normally you invoke it indirectly via the
\fIsysinstall\fP\/ program\(emsee page \*[building-partition-table].
.H2 "Disk size limitations"
.Pn sizelimit
Disk storage capacity has grown by several orders of magnitude since FreeBSD was
first released.  As it did so, a number of limits became apparent:
.Ls B
.LI
The first was the BIOS \fI504 MB limit\fP\/ on IDE disks, imposed by their
similarity with ST-506 disks.  We discussed this on page
.Sref \*[504m-limit] .
FreeBSD works around this issue by using a loader that understands large disks,
so this limit is a thing of the past.
.LI
.Pn 1GB-limit
The next limit was the 1 GB limit, which affected some older SCSI host adapters.
Although SCSI drives always use LBA addressing internally, the BIOS needed to
simulate CHS addressing for Microsoft.  Early BIOSes were limited to 64 heads,
32 sectors and 1024 tracks (64 \(mu 32 \(mu 1024 \(mu 512 = 1 GB).  This
wouldn't be such a problem, except that some old Adaptec controllers offer a 1
GB compatibility option.  Don't use it: it's only needed for systems that were
installed with the old mapping.
.LI
After that, it's logical that the next limit should come at 2 GB.  There are
several different problems here.  The only one that affects FreeBSD appears to
be a bug in some IDE controllers, which don't work beyond this limit.  All of
them are old, and IDE controllers don't cost anything, so if you are sure you
have this problem, you can solve it by replacing the controller.  Make sure you
get one that supports DMA.
.P
Other systems, including many versions of UNIX System V, have problems with this
limit because 2\u\s-431\s0\d is the largest number that can be represented in a
32 bit signed integer.  FreeBSD does not have this limitation, as file sizes are
represented in 64 bit quantities.
.LI
At 4 GB, some IDE controllers have problems because they convert this to a CHS
mapping with 256 heads, which doesn't work: the largest number is 255.  Again,
if you're sure this is the cause of problems you may be having, a new controller
can help.
.LI
At 8 GB the CHS system runs out of steam.  It can't describe more than 1024
cylinders, 255 heads or 63 sectors.  Beyond this size, you must use LBA
addressing\(emif your BIOS supports it.
.LI
You'd expect more problems at 16 GB, but in fact the next limitation doesn't
come until 128 GB.  It's due to the limitations in the original LBA scheme,
which had only 28 bits of sector address.  The new standard extends this to 48
bits, which should be sufficient for the next few years.  FreeBSD already uses
the new standard, so this limitation has never been an issue.
.Le
None of these problems affect FreeBSD directly.  The FreeBSD bootstrap no longer
uses the system BIOS, so it is not bound by the restrictions of the BIOS and the
controller.  If you use another operating system's loader, however, you could
have problems.  If you have the choice, use LBA addressing.  Unfortunately, you
can't do so if the disk already contains software that uses CHS addressing.
.P
.ne 3v
Other things to consider are:
.Ls B
.LI
If you have other software already installed on the disk, and you want to keep
it, \f(BIdo not change the drive geometry\fP.  If you do so, you will no longer
be able to run the other software.
.LI
Use LBA addressing if your hardware supports it.
.LI
If you have to use CHS, and you don't have any other software on the drive, use
the drive geometry specified on the disk itself or in the manual, if you're
lucky enough to get a manual with the disk.  Many BIOSes remap the drive
geometry in order to get Microsoft to agree to work with the disk, but this can
break FreeBSD disk mapping.  Check that the partition editor has these values,
and change them if necessary.
.LI
If all else fails, install Microsoft in a small slice at the start of the disk.
This creates a valid partition table for the drive, and the installation
software understands it.  Once you have started the installation process, the
Microsoft partition has fulfilled its purpose, and you can delete it again.
.Le
.sp -1v
.H2 "Display hardware"
.X "desktop"
For years, UNIX users have worked with a single 80x25 character mode display.
Many people consider this extremely old-fashioned, but in fact the flexibility
of the UNIX system made this quite a good way to work.  Still, there's no doubt
of the advantage of a system that offers the possibility of performing multiple
operations at once, and this is one of the particular advantages of UNIX.  But
you normally need a terminal to interact with each task.  The best way to do
this is with the X Window System.  You might also want to use a \fIdesktop\fP, a
set of programs that offer commonly used functionality.
.P
In many other environments, the GUI and the graphical display are the same
thing, and in some systems, notably Microsoft, there is no clear distinction
between the operating system and the GUI.  In UNIX, there are at least four
levels of abstraction:
.Ls B
.LI
The kernel runs the computer.
.LI
X interfaces with the kernel and runs the display.  It doesn't display anything
itself except possibly a display background, by default a grey cross-hatch
pattern.
.LI
.X "desktop"
.X "window manager"
The \fIwindow manager\fP\/ gives you control over the windows, such as moving,
resizing and \fIiconification\fP\/ (often called \fIminimizing\fP\/ in other
systems).  It provides the windows with \fIdecorations\fP\/ like frames,
buttons and menus.
.LI
The \fIdesktop\fP\/ provides commonly used applications and ways of starting
them.  Many people get by without a desktop by using window manager
functionality.
.Le
Why do it this way?  Because it gives you more choice.  There are dozens of
window managers available, and also several desktops.  You're not locked in to a
single product.  This has its down side, though: you must make the choice, and
so setting up X requires a little more thought than installing Microsoft.
.H2 "The hardware"
.X "hardware for X"
.X "X, hardware"
X runs on almost any hardware.  That doesn't mean that all hardware is equal, of
course.  Here are some considerations:
.H3 "The keyboard"
X uses the keyboard a lot more than Microsoft.  Make sure you get a good one.
.H3 "The mouse"
.X "mouse, double-click"
.X "double-click, mouse"
.X "CTS"
X prefers a three-button mouse, though it has provisions for up to five buttons.
It can support newer mice with rollers and side buttons, but most software does
not use them.  Some mice, such as the Logitech wireless mouse, require
undocumented sequences to enable some buttons (the thumb button in the case of
Logitech).  X does not support this button.
.P
Get the best mouse you can.  Prefer a short, light switch.  It \fImust\fP\/ have
at least three buttons.  Accept no substitutes.  Look for one with an
easy-to-use middle button.  Frequently mice with both a middle button and a
roller make it difficult to use the middle button: it's either misplaced, too
heavy in action, or requires pressing on the roller (and thus possibly turning
it).  All of these prove to be a nuisance over time.
.P
.X "PS/2 mouse"
.X "bus mouse"
.X "USB, mouse"
Older mice connected via the serial port or a special card (``bus mouse'').
Nowadays most mice are so-called PS/2 mice, and USB mice are becoming more
popular.
.H3 "The display board and monitor"
X enables you to do a lot more in parallel than other windowing environments.
As a result, screen real estate is at a premium.  Use as big a monitor as you
can afford, and as high a resolution as your monitor can handle.  You should be
able to display a resolution of 1600x1200 on a 21" monitor, 1280x1024 on a 17"
monitor, and 1024x768 on a 14" monitor.  Premium quality 21" monitors can
display 2048x1536.  If that's not enough, we'll look at multiple monitor
configurations on page
.Sref \*[multihead] .
.H3 "Laptop hardware"
If you have a laptop, you don't get any choice.  The display has a native
resolution which you can't change.  Most laptops display lower resolutions by
interpolation, but the result looks much worse than the native resolution.  LCD
screens look crisper than CRT monitors, so you can choose higher
resolutions\(emmodern laptops have display resolutions of up to 1600x1200.
.P
If you're going to use your laptop for presentations with overhead projectors,
make sure you find one that can display both on the internal screen and also on
the external output at the same time, while maintaining a display resolution of
1024x768: not many overhead projectors can display at a higher resolution.
.H2 "Compaq/Digital Alpha machines"
.X "Processor, AXP"
.X "AXP, processor"
.X "processor, Alpha"
.X "Alpha, architecture"
.X "console firmware, SRM"
.X "SRM, console firmware"
.X "console firmware, ARC"
.X "ARC, console firmware"
.Pn alpha-concepts
FreeBSD also supports computers based on the Compaq (previously Digital)
\fIAXP\fP\/ processor, commonly called \fIAlpha\fP.  Much of the information
above also applies to the Alpha; notable exceptions are:
.Ls B
.LI
Much of the PC hardware mentioned above was never supplied with the Alpha.  This
applies particularly to older hardware.
.LI
The PC BIOS is very different from the Alpha console firmware.  We'll look at
that below.
.LI
Disk partitioning is different.  FreeBSD does not support multiple operating
systems on the Alpha platform.
.Le
In this section we'll look at some additional topics that only apply to the
Alpha.
.P
FreeBSD requires the \fISRM\fP\/ console firmware, which is used by
\fITru64\fP\/ (formerly known as \fIDigital UNIX\fP\/).  It does not work with
the ARC firmware (sometimes called AlphaBIOS) used with Microsoft NT.  The SRM
firmware runs the machine in 64 bit mode, which is required to run FreeBSD,
while the \fIARC\fP\/ firmware sets 32 bit mode.  If your system is currently
running Tru64, you should be able to use the existing SRM console.
.P
The SRM console commands differ from one version to another.  The commands
supported by your version are described in the hardware manual that was shipped
with your system.  The console \f(CWhelp\fP command lists all supported console
commands.  If your system has been set to boot automatically, you must type
\fBCtrl-C\fP to interrupt the boot process and get to the SRM console prompt
(\f(CW>>>\fP).  If the system is not set to boot automatically, it displays the
SRM console prompt after performing system checks.
.P
All SRM console versions support the \f(CWset\fP and \f(CWshow\fP commands,
which operate on environment variables that are stored in non-volatile memory.
The \f(CWshow\fP command lists all environment variables, including those that
are read-only.
.P
Alpha's SRM is picky about which hardware it supports.  For example, it
recognizes NCR SCSI boards, but it doesn't recognize Adaptec boards.  There are
reports of some Alphas not booting with particular video boards.  The
\f(CWGENERIC\fP kernel configuration (\fI/usr/src/sys/alpha/conf/GENERIC\fP\/)
shows what the kernel supports, but that doesn't mean that the SRM supports all
the devices.  In addition, the SRM support varies from one machine to the next,
so there's a danger that what's described here won't work for you.
.P
Other differences for Alpha include:
.Ls B
.LI
The disk layout for SRM is different from the layout for Microsoft NT.  SRM
looks for its bootstrap where Microsoft keeps its partition table.  This means
that you cannot share a disk between FreeBSD and Microsoft on an Alpha.
.LI
Most SRM-based Alpha machines don't support IDE drives: you're limited to SCSI.
.Le
.\" XXX XXX There's no point discussing this here.  It will change so rapidly that it
.\" XXX won't be worth it.
.\" XXX .H2 "SPARC 64"
.\" XXX .Pn sparc-concepts
.\" XXX The most recent platform to run FreeBSD is Sun Microsystems' SPARC 64, a 64 bit
.\" XXX implementation of the SPARC architecture, which was released with FreeBSD 5.0-RELEASE.
.\" XXX At present support is still a little
.\" XXX patchy,
.H2 "The CD-ROM distribution"
.Pn cdrom-distribution
.X "installation CD-ROM"
.X "CD-ROM, Installation"
.X "Live File System CD-ROM"
.X "CD-ROM, Live File System"
The easiest way to install FreeBSD is from CD-ROM.  You can buy them at a
discount with the order form at the back of the book, or you can download an
\fIISO image\fP\/ from \fIftp.FreeBSD.org\fP\/ and create your own CD-ROM.
There are a number of CD-ROMs in a FreeBSD distribution, but the only essential
one is the first one, the \fIInstallation\fP\/ CD-ROM.  It contains everything
you need to install the system itself.  The other CD-ROMs contain mainly
installable packages.  Individual releases may contain other data, such as a
copy of the source code repository.  We'll take a more detailed look at the
installation CD-ROM here.
.H3 "Installation CD-ROM"
The Installation CD-ROM contains everything you need to install FreeBSD on your
system.  It supplies two categories of installable software:
.Ls B
.LI
The base operating system is stored as \fIgzip\fP\/ped \fItar\fP\/ archives in
the directories \fIbase\fP, \fIboot\fP, \fIcatpages\fP, \fIcompat1x\fP,
\fIcompat20\fP, \fIcompat21\fP, \fIcompat3x\fP, \fIcompat4x\fP, \fIdes\fP,
\fIdict\fP, \fIdoc\fP, \fIgames\fP, \fIinfo\fP, \fImanpages\fP and
\fIproflibs\fP.  To facilitate transport to and installation from floppy, the
archives have been divided into chunks of 1.44 MB.  For example, the only
required set is in the files \fIbase/base.??\fP, in other words, all files whose
names start with \fIbase.\fP\/ and contain two additional characters.  This
specifically excludes the files
.File base.inf
and
.File base.mtree ,
which are not part of the archive.
.LI
.X "packages/All"
The directory \fIpackages/All\fP\/ contains ported, installable software
packages as \fIgzip\fP\/ped \fItar\fP\/ archives.  They are designed to be
installed directly on a running system, so they have not been divided into
chunks.  Due to size restrictions on the CD-ROM, this directory does not contain
all the packages: others are on additional CD-ROMs.
.P
\fIpackages/Latest\fP contains the latest versions of the packages.
.P
\fIpackages/All\fP contains a large subset of the Ports Collection.  To make it
easier for you to find your way around them, symbolic links to appropriate
packages have been placed in the directories \fIarchivers\fP, \fIastro\fP,
\fIaudio\fP, \fIbenchmarks\fP, \fIbiology\fP, \fIcad\fP, \fIchinese\fP,
\fIcomms\fP, \fIconverters\fP, \fIdatabases\fP, \fIdeskutils\fP, \fIdevel\fP,
\fIeditors\fP, \fIemulators\fP, \fIfrench\fP, \fIftp\fP, \fIgames\fP,
\fIgerman\fP, \fIgraphics\fP, \fIhebrew\fP, \fIirc\fP, \fIjapanese\fP,
\fIjava\fP, \fIkorean\fP, \fIlang\fP, \fImail\fP, \fImath\fP, \fImbone\fP,
\fImisc\fP, \fInet\fP, \fInews\fP, \fIpalm\fP, \fIpicobsd\fP, \fIplan9\fP,
\fIprint\fP, \fIrussian\fP, \fIscience\fP, \fIsecurity\fP, \fIshells\fP,
\fIsysutils\fP, \fItemplates\fP, \fItextproc\fP, \fIukrainian\fP,
\fIvietnamese\fP, \fIwww\fP, \fIx11\fP, \fIx11-clocks\fP, \fIx11-fm\fP,
\fIx11-fonts\fP, \fIx11-servers\fP, \fIx11-toolkits\fP and \fIx11-wm\fP.  Don't
get the impression that these are different packages\(emthey are really pointers
to the packages in \fIAll\fP.  You will find a list of the currently available
packages in the file \fIpackages/INDEX\fP\/.
.P
We'll look at the Ports Collection in more detail in Chapter
.Sref \*[nchports] .
.Le
.ne 2i
Table
.Sref \*[installation-cd] \&
lists typical files in the main directory of the installation CD-ROM.
.Table-heading "The installation CD-ROM"
.Tn installation-cd
.TS H
tab(#) ;
 lw14fI  | lw48  .
\fBFile#\fBContents
_
.TH N
ERRATA.TXT#T{
A list of last-minute changes.  \f(BIRead this file.
It can save you a lot of headaches\fP.
T}
.sp .4v
HARDWARE.TXT#T{
A list of supported hardware.
T}
.sp .4v
INSTALL.TXT#T{
Information about installing FreeBSD.
T}
.sp .4v
README.TXT#T{
The traditional first file to read.  It describes how to use the other files.
T}
.sp .4v
RELNOTES.TXT#T{
Release notes.
T}
.sp .4v
base#T{
Installation directory: the base distribution of the system.  This is the only
required directory for installation.  See
.Sref "\*[chinstall]" ,
for more detail.
T}
.sp .4v
boot#T{
Files related to booting, including the installation kernel.
T}
.sp .4v
catpages#T{
Pre-formatted man pages.  See page
.Sref \*[man] \&
for more detail.
T}
.sp .4v
cdrom.inf#T{
Machine-readable file describing the CD-ROM contents for the benefit of
.Command sysinstall .
T}
.sp .4v
compat1x#T{
Directory containing libraries to maintain compatibility with Release 1.X of
FreeBSD.
T}
.sp .4v
compat20#T{
Directory containing libraries to maintain compatibility with Release 2.0 of
FreeBSD.
T}
.sp .4v
compat21#T{
Directory containing libraries to maintain compatibility with Release 2.1 of
FreeBSD.
T}
.sp .4v
compat22#T{
Directory containing libraries to maintain compatibility with Release 2.2 of
FreeBSD.
T}
.sp .4v
compat3x#T{
Directory containing libraries to maintain compatibility with Release 3 of
FreeBSD.
T}
.sp .4v
compat4x#T{
Directory containing libraries to maintain compatibility with Release 4 of
FreeBSD.
T}
.sp .4v
crypto#T{
Installation directory: cryptographic software.
T}
.sp .4v
dict#T{
Installation directory: dictionaries.
T}
.sp .4v
doc#T{
Installation directory: documentation.
T}
.sp .4v
docbook.css#T{
Style sheet for documentation.
T}
.sp .4v
filename.txt#T{
A list of all the files on this CD-ROM.
T}
.sp .4v
floppies#T{
A directory containing installation floppy disk images.
T}
.sp .4v
games#T{
Installation directory: games.
T}
.sp .4v
info#T{
Installation directory: GNU info documents.
T}
.sp .4v
kernel#T{
The boot kernel.
T}
.sp .4v
manpages#T{
A directory containing the man pages for installation.
T}
.sp .4v
packages#T{
A directory containing installable versions of the Ports Collection.  See page
\*[install-package].
T}
.sp .4v
ports#T{
The sources for the  Ports Collection.  See
.Sref "\*[chports]" ,
page
.Sref \*[ports-collection] .
T}
.sp .4v
proflibs#T{
A directory containing profiled libraries, useful for identifying performance
problems when programming.
T}
.sp .4v
src#T{
A directory containing the system source files.
T}
.sp .4v
tools#T{
A directory containing tools to prepare for installation from another operating
system.
T}
_
.TE
.P
The \fI.TXT\fP\/ files are also supplied in HTML format with a \fI.HTM\fP\/
suffix.
.P
The contents of the CD-ROM will almost certainly change from one release to
another.  Read
.File README.TXT
for details of the changes.
.H3 "Live File System CD-ROM"
.X "Live File System CD-ROM"
.X "CD-ROM, Live File System"
Although the installation CD-ROM contains everything you need to install
FreeBSD, the format isn't what you'd like to handle every day.  The distribution
may include a \fILive File System\fP\/ CD-ROM, which solves this problem: it
contains substantially the same data stored in file system format in much the
same way as you would install it on a hard disk.  You can access the files
directly from this CD-ROM.
.H3 "CVS Repository CD-ROM"
.X "CD-ROM, CVS Repository"
.P
One of the disks may also contain the ``CVS Repository.''  The repository is the
master source tree of all source code, including all update information.  We'll
look at it in more detail in
.Sref "\*[chcurrent]" ,
page
.Sref \*[repository] .
.H3 "The Ports Collection CD-ROMs"
.X "Ports Collection"
An important part of FreeBSD is the \fIPorts Collection\fP, which comprises many
thousand popular programs.  The Ports Collection automates the process of
porting software to FreeBSD.  A combination of various programming tools already
available in the base FreeBSD installation allows you to simply type
\fImake\fP\/ to install a given package.  The ports mechanism does the rest, so
you need only enough disk space to build the ports you want.
.X "Ports Collection"
We'll look at the Ports Collection in more detail in Chapter
.Sref \*[nchports] .
The files are spread over a number of CD-ROMs:
.Ls B
.LI
You'll find the \fIports\fP, the instructions for building the packages, on the
installation CD-ROM.
.LI
The base sources for the Ports Collection fill more than one CD-ROM, even though
copyright restrictions mean that not all sources may be included: some source
files are freely distributable on the Net, but may not be distributed on CD-ROM.
.P
Don't worry about the missing sources: if you're connected to the Internet, the
Ports Collection automatically retrieves the sources from an Internet server
when you type \fImake\fP.
.LI
You'll find the most popular \fIpackages\fP, the precompiled binaries of the
ports, on the Installation CD-ROM.  A full distribution contains a number of
other CD-ROMs with most of the remaining packages.
.Le
