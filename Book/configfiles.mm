.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: configfiles.mm,v 4.19 2003/06/29 04:32:34 grog Exp grog $
.\"
.Chapter \*[nchconfigfiles] "FreeBSD configuration files"
.Pn configfiles
One of the outstanding things about UNIX is that all system configuration
information is stored in text files, usually in the directory
.Directory /etc
or its subdirectories.  Some people consider this method primitive by comparison
with a flashy GUI configuration editor or a ``registry,'' but it has significant
advantages.  In particular, you see the \fIexact\fP\/ system configuration.
With a GUI editor, the real configuration is usually stored in a format that
you can't read, and even when you can, it's undocumented.  Also, you can see
more of the configuration at a time: a GUI editor usually presents you with only
small parts of the configuration, and it's difficult to see the relationships
(``standing outside and looking in through a window'').
.P
In the Microsoft world, one of the most common methods of problem resolution is
to reinstall the system.  This is a declaration of bankruptcy: it's very slow,
you're liable to cause other problems on the way, and you never find out what
the problem was.  If you have problems with your FreeBSD system configuration,
\f(BIdon't reinstall the system\fP.  Take a look at the configuration files, and
there's a good chance that you'll find the problem there.
.P
Many configuration files are the same across all versions of UNIX.  This chapter
touches on them briefly, but in many case you can get additional information in
books such as the \fIUNIX System Administration Handbook\fP, by Evi Nemeth,
Garth Snyder, Scott Seebass, and Trent R. Hein.  In all cases, you can get more
information from section 5 of the man pages.
.P
In the following section, we'll first look at
.File /etc/rc.conf ,
the main configuration file.  We'll look at the remaining configuration files on
page
.Sref "\*[changeable-config-files]" .
.H2 "/etc/rc.conf"
.Pn rc.conf
.File /etc/rc.conf
is the main system configuration file.  In older releases of FreeBSD, this file
was called
.File /etc/sysconfig .
.P
.File /etc/rc.conf
is a shell script that is intended to be the one file that defines the
configuration of your system\(emthat is to say, what the system needs to do when
it starts up.  It's not quite that simple, but nearly all site-dependent
information is stored here.  We'll walk through the version that was current at
the time of writing.  The files will change as time goes on, but most of the
information will remain relevant.
.P
.File /etc/rc.conf
is completely your work.  When you install the system, there is no such file:
you create it, usually implicitly with the aid of
.Command sysinstall .
The system supplies a script
.File /etc/defaults/rc.conf
that contains default values for everything you might put in
.File /etc/rc.conf ,
and which the other configuration files read to get their definitions.  When the
system starts, it first reads
.File /etc/defaults/rc.conf .
Commands at the end of this file check for the existence of the file
.File /etc/rc.conf
and read it in if they find it, so that the definitions in
.File /etc/rc.conf
override the defaults in
.File /etc/defaults/rc.conf .
This makes it easier to upgrade: just change the file with the defaults, and
leave the site-specific configuration alone.  You may still need to change some
things, but it'll be a lot easier.
.P
In this section we'll walk through
.File /etc/defaults/rc.conf .
As we do, we'll build up two different
.File /etc/rc.conf
files, one for a server and one for a laptop connected with an 802.11b wireless
card.  To avoid too much confusion, I show the text that goes into
.File /etc/rc.conf
in \f(CBconstant width bold\fP font, whereas the text in
.File /etc/defaults/rc.conf
is in \f(CWconstant width\fP font.
.Dx
#!/bin/sh
#
# This is rc.conf - a file full of useful variables that you can set
# to change the default startup behavior of your system.  You should
# not edit this file!  Put any overrides into one of the ${rc_conf_files}
# instead and you will be able to update these defaults later without
# spamming your local configuration information.
#
# The ${rc_conf_files} files should only contain values which override
# values set in this file.  This eases the upgrade path when defaults
# are changed and new features are added.
#
# All arguments must be in double or single quotes.
#
# $FreeBSD: src/etc/defaults/rc.conf,v 1.159 2002/09/05 20:14:40 gordon Exp $
.De
The claim that all arguments must be in double or single quotes is incorrect.
Both this file and
.File /etc/rc.conf
are Bourne shell scripts, and you only need quotes if the values you include
contain spaces.  It's a good idea to stick to this convention, though, in case
the representation changes.
.P
Note the version information on the previous line (1.159).  Your
.File /etc/defaults/rc.conf
will almost certainly have a different revision.  If you have a CVS repository
on line (see Chapter
.Sref \*[nchcurrent] ),
you can see what is changed with the following commands:
.Dx
$ \f(CBcd /usr/src/etc/defaults\fP
$ \f(CBcvs diff -wu -r1.159 rc.conf\fP
.De
Continuing,
.Dx
##############################################################
###  Important initial Boot-time options  ####################
##############################################################
rc_ng="YES"                     # Set to NO to disable new-style rc scripts.
rc_info="YES"                   # Enables display of informational messages at boot.
rcshutdown_timeout="30"         # Seconds to wait before terminating rc.shutdown
.De
FreeBSD Release 5 has a new method of system startup, called \fIRCng\fP\/ (run
commands, next generation).  This method was originally introduced in NetBSD.
.\" XXX We'll look at it on page
.\" XXX .Sref \*[rcng] .
Don't change these values unless you know \fIexactly\fP\/ what you are doing.
If you make a mistake, you may find it impossible to start the system.
.Dx
swapfile="NO"                   # Set to name of swapfile if aux swapfile desired.
.De
Normally you set up entries for swap partitions in
.File /etc/fstab .
This entry refers only to swapping on files, not for partitions.  It requires
the \fImd\fP\/ driver, which we looked at on page
.Sref \*[md-driver] .
.Dx
apm_enable="NO"                 # Set to YES to enable APM BIOS functions (or NO).
apmd_enable="NO"                # Run apmd to handle APM event from userland.
apmd_flags=""                   # Flags to apmd (if enabled).
.De
.X "APM"
.X "Advanced Power Management"
These parameters cover \fIAPM\fP, \fIAdvanced Power Management\fP.
.\" XXX We looked at this topic in
.\" XXX .Sref "\*[chextras]" .
.Dx
devd_enable="NO"        # Run devd, to trigger programs on device tree changes.
pccard_enable="NO"      # Set to YES if you want to configure PCCARD devices.
pccard_mem="DEFAULT"    # If pccard_enable=YES, this is card memory address.
pccard_beep="2"         # pccard beep type.
pccard_ifconfig="NO"    # Specialized pccard ethernet configuration (or NO).
pccardd_flags="-z"      # Additional flags for pccardd.
pccard_conf="/etc/defaults/pccard.conf" # pccardd(8) config file
pccard_ether_delay="5"  # Delay before trying to start dhclient in pccard_ether
.De
These parameters control
.Daemon devd ,
.X "device daemon"
the \fIdevice daemon\fP used primarily for hot-pluggable devices such as USB and
PC Card, and
.Daemon pccardd ,
the daemon for the old PC Card code.
See page
.Sref \*[devd] \&
for more details of
.Daemon devd ,
and page
.Sref \*[OLDCARD] \&
for a brief description of
.Daemon pccardd
and the old PC Card code.
.P
If you're running PC Card devices, you would start
.Daemon devd .
That's what we put in the
.File /etc/rc.conf
for \fIandante\fP\/:
.Dx
\f(CBdevd_enable="YES"\fP
.De
Next comes a list of directories that are searched for startup scripts:
.Dx 2
local_startup="/usr/local/etc/rc.d /usr/X11R6/etc/rc.d" # startup script dirs.
script_name_sep=" "             # Change if startup scripts' names contain spaces
.De
.ne 2v
If you come from a System V background, you would expect to find these scripts
in the directories such as
.Directory -n /etc/rc2.d .
.Dx
rc_conf_files="/etc/rc.conf /etc/rc.conf.local"
.De
\f(CWrc_conf_files\fP is a list of files to read after this file.  You'll
recognize
.File /etc/rc.conf ,
which we discussed above.
.File /etc/rc.conf.local
is an idea that hasn't completely died, but there's a good chance that it will.
You'd be best off not to use it until you're sure it's going to stay.
.P
For obvious reasons, this is one entry in
.File /etc/defaults/rc.conf
that you can't override in
.File /etc/rc.conf .
If you really want to search other files, you'll have to modify
.File /etc/defaults/rc.conf .
It's still not a good idea.
.Pn fsck-options
.Dx
fsck_y_enable="NO"              # Set to YES to fsck -y if the initial preen fails.
background_fsck="YES"           # Attempt to run fsck in the background
extra_netfs_types="NO"          # List of network extra filesystem types for delayed
                                # mount at startup (or NO).
.De
On system startup, the system checks the integrity of all file systems.  It does
this in a number of steps:
.Ls B
.LI
First, it checks the \fIsuperblock\fP, the key to the file system, to see
whether it was unmounted before the system stopped.  If so, it assumes that the
file systems are consistent and continues with the startup.
.LI
If any file system was not unmounted, the system probably crashed or was turned
off without proper shutdown.  The file system could contain inconsistent data,
so the startup scripts run
.Command fsck
against the file system.
.LI
If you're running with soft updates and checkpointing, you may be able to
perform the
.Command fsck
in the \fIbackground\fP, in other words in parallel with other activities.  If
you have a good reason, you can inhibit this behaviour by setting
\f(CWbackground_fsck\fP to \f(CWNO\fP.
.LI
If the file system is badly damaged, the ``standard strength'' fsck may not be
able to recover the file system.  In this case, the normal action is to drop
into single-user mode and let a human take a look at it.
.Le
The usual first action of the human is to run
.Command fsck
with the \f(CW-y\fP
option, meaning ``answer \fIyes\fP\/ to all questions from
.Command fsck ''.
If
you set \f(CWfsck_y_enable\fP to \f(CWYES\fP, the startup scripts will perform
this task for you.  It's still possible that the check will fail, so this is not
enough to ensure that you will always pass
.Command fsck ,
but it helps.
.Dx
##############################################################
###  Network configuration sub-section  ######################
##############################################################
### Basic network and firewall/security options: ###
hostname=""                     # Set this!
.De
.ne 3v
\f(CWhostname\fP is the fully qualified name of the host.  Always set it in
.File /etc/rc.conf .
See page
.Sref \*[set-hostname] \&
for more details.  In our
.File /etc/rc.conf
we'll put:
.Dx 2
\f(CBhostname="gw.example.org"
\f(CBhostname="andante.example.org"
.De
.ne 2
Continuing in
.File /etc/defaults/rc.conf ,
.Dx 1
nisdomainname="NO"              # Set to NIS domain if using NIS (or NO).
.De
If you're using Sun's NIS, set this.  We don't discuss NIS in this book.
.Dx
dhcp_program="/sbin/dhclient"   # Path to dhcp client program.
dhcp_flags=""                   # Additional flags to pass to dhcp client.
.De
The settings for the \fIDHCP\fP\/ client,
.Command dhclient .
Normally you won't need to change them.  We talked about \fIDHCP\fP\/ on page
.Sref \*[DHCP] .
.Dx
firewall_enable="NO"            # Set to YES to enable firewall functionality
firewall_script="/etc/rc.firewall" # Which script to run to set up the firewall
firewall_type="UNKNOWN"         # Firewall type (see /etc/rc.firewall)
firewall_quiet="NO"             # Set to YES to suppress rule display
firewall_logging="NO"           # Set to YES to enable events logging
.De
.X "firewall, ipfw"
.X "ipfw, firewall"
Parameters for the \fIipfw\fP\/ firewall.  See page
.Sref \*[firewall-configuration] ,
where we set the following flags in the
.File /etc/rc.conf
for \fIgw\fP\/:
.Dx
.ft CB
firewall_enable="YES"           # Set to YES to enable firewall functionality
firewall_type="client"          # Firewall type (see /etc/rc.firewall)\fP
.De
You don't normally run firewalls on laptops, though there's no technical reason
why not.  The problem with firewalls on laptops is that the configuration files
are dependent on where the system is located, which makes it a pain for systems
that frequently change locations.  As a result, we won't add any firewall
parameters to the
.File /etc/rc.conf
for \fIandante\fP.
.Dx
ip_portrange_first="NO"         # Set first dynamically allocated port
ip_portrange_last="NO"          # Set last dynamically allocated port
.De
These values are used to set the numbers of ports that are dynamically
allocated.  Normally they won't need changing.
.Dx
ipsec_enable="NO"               # Set to YES to run setkey on ipsec_file
ipsec_file="/etc/ipsec.conf"    # Name of config file for setkey
.De
.\" XXX should we change this?
Parameters for IPSec.  We don't discuss IPSec in this book.
.Dx
natd_program="/sbin/natd"       # path to natd, if you want a different one.
natd_enable="NO"                # Enable natd (if firewall_enable == YES).
natd_interface=""               # Public interface or IPaddress to use.
natd_flags=""                   # Additional flags for natd.
.De
Parameters for
.Daemon natd .
See page
.Sref \*[natd-rc.conf] \&
for more details.  In the example there, we'll add these lines to \fIgw\fP\/'s
.File /etc/rc.conf \/:
.Dx
.ft CB
firewall_enable=YES
gateway_enable="YES"            # Set to YES if this host is a gateway.
natd_enable="YES"
natd_interface="dc0"
firewall_script="/etc/rc.nat"   # script for NAT only
firewall_type="client"          # firewall type if running a firewall
.De
Continuing with
.File /etc/defaults/rc.conf ,
.Dx
ipfilter_enable="NO"            # Set to YES to enable ipfilter functionality
ipfilter_program="/sbin/ipf"    # where the ipfilter program lives
ipfilter_rules="/etc/ipf.rules" # rules definition file for ipfilter, see
                                # /usr/src/contrib/ipfilter/rules for examples
ipfilter_flags=""               # additional flags for ipfilter
ipnat_enable="NO"               # Set to YES to enable ipnat functionality
ipnat_program="/sbin/ipnat"     # where the ipnat program lives
ipnat_rules="/etc/ipnat.rules"  # rules definition file for ipnat
ipnat_flags=""                  # additional flags for ipnat
ipmon_enable="NO"               # Set to YES for ipmon; needs ipfilter or ipnat
ipmon_program="/sbin/ipmon"     # where the ipfilter monitor program lives
ipmon_flags="-Ds"               # typically "-Ds" or "-D /var/log/ipflog"
ipfs_enable="NO"                # Set to YES to enable saving and restoring
                                # of state tables at shutdown and boot
ipfs_program="/sbin/ipfs"       # where the ipfs program lives
ipfs_flags=""                   # additional flags for ipfs
.De
.X "ipmon"
.X "ipfs"
.X "ipnat"
.X "ipfilter, firewall"
.X "firewall, ipfilter"
These entries define defaults for \fIipfilter\fP, another firewall package,
\fIipnat\fP, another NAT package, \fIipmon\fP, an IP monitor package, and
\fIipfs\fP, a utility for saving the state tables of \fIipfilter\fP,
\fIipnat\fP\/ and \fIipfilter\fP.  We don't discuss any of them in this book.
.Dx
tcp_extensions="NO"             # Disallow RFC1323 extensions (or YES).
log_in_vain="0"                 # >=1 to log connects to ports w/o listeners.
tcp_keepalive="YES"             # Enable stale TCP connection timeout (or NO).
# For the following option you need to have TCP_DROP_SYNFIN set in your
# kernel.  Please refer to LINT and NOTES for details.
tcp_drop_synfin="NO"            # Set to YES to drop TCP packets with SYN+FIN
                                # NOTE: this violates the TCP specification
icmp_drop_redirect="NO"         # Set to YES to ignore ICMP REDIRECT packets
icmp_log_redirect="NO"          # Set to YES to log ICMP REDIRECT packets
.De
These are some of the more obscure IP configuration variables.  You can
.\" Hopefully!
find more about them in \fItcp(4)\fP\/ and \fIicmp(4)\fP.
.\" XXX This stuff isn't all documented.  Consider removing.
.Dx
network_interfaces="auto"       # List of network interfaces (or "auto").
cloned_interfaces=""            # List of cloned network interfaces to create.
#cloned_interfaces="gif0 gif1 gif2 gif3" # Pre-cloning GENERIC config.
ifconfig_lo0="inet 127.0.0.1"   # default loopback device configuration.
#ifconfig_lo0_alias0="inet 127.0.0.254 netmask 0xffffffff" # Sample alias entry.
#ifconfig_ed0_ipx="ipx 0x00010010"  # Sample IPX address family entry.
.De
In previous releases of FreeBSD, you had to set \f(CWnetwork_interfaces\fP to a
list of the interfaces on the machine.  Nowadays the value \f(CWauto\fP enables
the startup scripts to find them by themselves, so you don't need to change this
variable.  You still need to set the interface addresses, of course.  For
\fIgw\fP, we add the following entry to
.File /etc/rc.conf \/:
.Dx 1
.ft CB
ifconfig_ed0="inet 223.147.37.5     netmask 255.255.255.0"\fP
.De
We don't need to do anything here for \fIandante\fP: its Ethernet interface is a
PC Card card.  We looked at that on page
.Sref \*[PCMCIA-net] .
.P
.ne 5v
If you're using DHCP, you don't have an address to specify, of course.  You
still need to tell the startup scripts to use DHCP, however.  Do it like this:
.Dx
.ft CB
ifconfig_ed0="DHCP"
.De
.ne 8v
Continuing,
.Dx
# If you have any sppp(4) interfaces above, you might also want to set
# the following parameters.  Refer to spppcontrol(8) for their meaning.
sppp_interfaces=""              # List of sppp interfaces.
#sppp_interfaces="isp0"         # example: sppp over ISDN
#spppconfig_isp0="authproto=chap myauthname=foo myauthsecret='top secret' hisauthnam
e=some-gw hisauthsecret='another secret'"
gif_interfaces="NO"             # List of GIF tunnels (or "NO").
#gif_interfaces="gif0 gif1"     # Examples typically for a router.
                                # Choose correct tunnel addrs.
#gifconfig_gif0="10.1.1.1 10.1.2.1"     # Examples typically for a router.
#gifconfig_gif1="10.1.1.2 10.1.2.2"     # Examples typically for a router.
.De
.X "sppp"
.X "isdn4bsd"
.X "Generic Tunnel Interface"
These are parameters for the \fIsppp\fP\/ implementation for \fIisdn4bsd\fP and
the \fIGeneric Tunnel Interface\fP, both of which we won't discuss here.  See
the man pages \fIspp(4)\fP\/ and \fIgif(4)\fP\/ for more details.
.Dx
# User ppp configuration.
ppp_enable="NO"         # Start user-ppp (or NO).
ppp_mode="auto"         # Choice of "auto", "ddial", "direct" or "dedicated".
                        # For details see man page for ppp(8). Default is auto.
ppp_nat="YES"           # Use PPP's internal network address translation or NO.
ppp_profile="papchap"   # Which profile to use from /etc/ppp/ppp.conf.
ppp_user="root"         # Which user to run ppp as
.De
These parameters relate to running user PPP, which we discussed in Chapter
.Sref \*[nchppp] ,
on page
.Sref \*[user-ppp] .
.Dx
### Network daemon (miscellaneous) ###
syslogd_enable="YES"    # Run syslog daemon (or NO).
syslogd_program="/usr/sbin/syslogd" # path to syslogd, if you want a different one.
syslogd_flags="-s"      # Flags to syslogd (if enabled).
#syslogd_flags="-ss"    # Syslogd flags to not bind an inet socket
.De
You should always run syslogd unless you have a very good reason not to.  In
previous releases of FreeBSD, \f(CWsyslogd_flags\fP was empty, but security
concerns have changed that, and now by default
.Daemon syslogd
is started with
the \f(CW-s\fP flag, which stops
.Daemon syslogd
from accepting remote messages.
If you specify the \f(CW-ss\fP flag, as suggested in the comment, you will also
not be able to log to remote systems.
.P
Sometimes it's very useful to log to a remote system.  For example, you might
want all systems in \fIexample.org\fP to log to \fIgw\fP.  That way you get one
set of log files for the entire network.  To do this, you would add the
following line at the beginning of
.File /etc/syslog.conf
on each machine:
.Dx
.ft CB
*.*                                                @gw\fP
.De
For this to work, add the following to
.File /etc/rc.conf
on \fIgw\fP\/:
.Dx
.ft CB
syslogd_flags=""\fP
.De
.ne 8v
Next come some parameters relating to
.Daemon inetd ,
the \fIInternet Daemon\fP, sometimes called the \fIsuper-server\fP.  It's
responsible for starting services on behalf of remote clients.
.Dx
inetd_enable="NO"               # Run the network daemon dispatcher (YES/NO).
inetd_program="/usr/sbin/inetd" # path to inetd, if you want a different one.
inetd_flags="-wW"               # Optional flags to inetd
.De
We looked at
.Daemon inetd
on page
.Sref \*[inetd] .
Normally you will want to have it enabled, but you won't need to change the
flags.  Add this line to the
.File /etc/rc.conf
for both \fIgw\fP\/ and \fIandante\fP\/:
.Dx
.ft CB
inetd_enable="YES"\fP
.De
Continuing, we see:
.Dx
named_enable="NO"               # Run named, the DNS server (or NO).
named_program="/usr/sbin/named" # path to named, if you want a different one.
#named_flags="-u bind -g bind"  # Flags for named
.De
These parameters specify whether we should run the name server, and what flags
we should use if we do.  See page
.Sref \*[running-named] \&
for more details.  Previous versions of
.Daemon named
required a flag to specify the location of the configuration file, but the
location FreeBSD uses has now become the standard, so we no longer need to
specify any flags.  All we put in
.File /etc/rc.conf
for \fIgw\fP\/ is:
.Dx
.ft CB
named_enable="YES"                 # Run named, the DNS server (or NO).\fP
.De
Continuing with
.File /etc/defaults/rc.conf ,
.Dx
kerberos4_server_enable="NO"            # Run a kerberos IV master server (or NO).
kerberos4_server="/usr/sbin/kerberos"   # path to kerberos IV KDC
kadmind4_server_enable="NO"             # Run kadmind (or NO)
kadmind4_server="/usr/sbin/kadmind"     # path to kerberos IV admin daemon
kerberos5_server_enable="NO"            # Run a kerberos 5 master server (or NO).
kerberos5_server="/usr/libexec/kdc"     # path to kerberos 5 KDC
kadmind5_server_enable="NO"             # Run kadmind (or NO)
kadmind5_server="/usr/libexec/k5admind" # path to kerberos 5 admin daemon
kerberos_stash="NO"                     # Is the kerberos master key stashed?
.De
Set these if you want to run Kerberos.  We don't discuss Kerberos in this book.
.Dx
rwhod_enable="NO"               # Run the rwho daemon (or NO).
rwhod_flags=""                  # Flags for rwhod
.De
Set this if you want to run the
.Daemon rwhod
daemon, which broadcasts
information about the system load.
.Dx
rarpd_enable="NO"               # Run rarpd (or NO).
rarpd_flags=""                  # Flags to rarpd.
bootparamd_enable="NO"          # Run bootparamd (or NO).
bootparamd_flags=""             # Flags to bootparamd
xtend_enable="NO"               # Run the X-10 power controller daemon.
xtend_flags=""                  # Flags to xtend (if enabled).
.De
These entries relate to the
.Daemon rarpd ,
.Daemon bootparamd
and the X-10 daemons, which we don't discuss in this book.  See the respective
man pages.
.Dx
pppoed_enable="NO"              # Run the PPP over Ethernet daemon.
pppoed_provider="*"             # Provider and ppp(8) config file entry.
pppoed_flags="-P /var/run/pppoed.pid"   # Flags to pppoed (if enabled).
pppoed_interface="fxp0"         # The interface that pppoed runs on.
.De
.Daemon pppoed
.X "PPP, over Ethernet"
is the \fIPPP Over Ethernet\fP\/ daemon.  We discussed it briefly on page
.Sref \*[PPPoE] .
.Dx
sshd_enable="NO"                # Enable sshd
sshd_program="/usr/sbin/sshd"   # path to sshd, if you want a different one.
sshd_flags=""                   # Additional flags for sshd.
.De
.Daemon sshd
is the \fISecure Shell Daemon\fP\/ which we talked about on page
.Sref \*[sshd] .
You don't need to change anything here to run
.Command ssh ,
but if you want to connect to this system with
.Command ssh ,
you'll need to run
.Daemon sshd .
In \fIgw\fP\/'s
.File /etc/rc.conf
we put:
.Dx
.ft CB
sshd_enable="YES"\fP
.De
Next, we see:
.Dx
amd_enable="NO"                 # Run amd service with $amd_flags (or NO).
amd_flags="-a /.amd_mnt -l syslog /host /etc/amd.map /net /etc/amd.map"
amd_map_program="NO"            # Can be set to "ypcat -k amd.master"
.De
These entries relate to the automounter, which we don't discuss in this book.
See \fIamd(8)\fP\/ for details.
.Pn nfs-setup
.Dx
nfs_client_enable="NO"          # This host is an NFS client (or NO).
nfs_access_cache="2"            # Client cache timeout in seconds
nfs_server_enable="NO"          # This host is an NFS server (or NO).
nfs_server_flags="-u -t -n 4"   # Flags to nfsd (if enabled).
mountd_enable="NO"              # Run mountd (or NO).
mountd_flags="-r"               # Flags to mountd (if NFS server enabled).
weak_mountd_authentication="NO" # Allow non-root mount requests to be served.
nfs_reserved_port_only="NO"     # Provide NFS only on secure port (or NO).
nfs_bufpackets="DEFAULT"        # bufspace (in packets) for client (or DEFAULT)
rpc_lockd_enable="NO"           # Run NFS rpc.lockd needed for client/server.
rpc_statd_enable="NO"           # Run NFS rpc.statd needed for client/server.
rpcbind_enable="NO"             # Run the portmapper service (YES/NO).
rpcbind_program="/usr/sbin/rpcbind" # path to rpcbind, if you want a different one.
rpcbind_flags=""                # Flags to rpcbind (if enabled).
rpc_ypupdated_enable="NO"       # Run if NIS master and SecureRPC (or NO).
.De
These are flags for NFS.  Some of them have changed from previous releases of
FreeBSD.  In particular, \f(CWsingle_mountd_enable\fP is now called
\f(CWmountd_enable\fP, and
.Daemon portmap
has been replaced by
.Daemon rpcbind ,
so
\f(CWportmap_enable\fP is now called \f(CWrpcbind_enable\fP,
\f(CWportmap_program\fP is now called \f(CWrpcbind_program\fP and
\f(CWportmap_flag\fP is now called \f(CWrpcbind_flags\fP.  See page
\*[NFS-setup].  We set the following values in
.File /etc/rc.conf
for \fIgw\fP\/:
.Dx
.ft CB
nfs_client_enable="YES"         # This host is an NFS client (or NO).
nfs_server_enable="YES"         # This host is an NFS server (or NO).
.ft P
.De
For \fIandante\fP\/, we enable only the client (the first line).  Next, we see:
.Dx
keyserv_enable="NO"             # Run the SecureRPC keyserver (or NO).
keyserv_flags=""                # Flags to keyserv (if enabled).
.De
These entries refer to the Secure RPC key server, which we don't discuss in this
book.  See the man pages \fIkeyserv(8)\fP\/ for more details.
.Dx
### Network Time Services options: ###
timed_enable="NO"               # Run the time daemon (or NO).
timed_flags=""                  # Flags to timed (if enabled).
ntpdate_enable="NO"             # Run ntpdate to sync time on boot (or NO).
ntpdate_program="/usr/sbin/ntpdate"  # path to ntpdate, if you want a different one.
ntpdate_flags="-b"              # Flags to ntpdate (if enabled).
ntpd_enable="NO"                # Run ntpd Network Time Protocol (or NO).
ntpd_program="/usr/sbin/ntpd"   # path to ntpd, if you want a different one.
ntpd_flags="-p /var/run/ntpd.pid"    # Flags to ntpd (if enabled).
.De
.\" XXX
.Daemon timed ,
.Command ntpdate
and
.Daemon ntpd
are three different ways of synchronizing your machine with the current date and
time.  As we saw on page
.Sref \*[timekeeping] ,
we'll use
.Daemon ntpd .
We add the following line to
.File /etc/rc.conf
for each system:
.Dx
.ft CB
ntpd_enable="YES"               # Run ntpd Network Time Protocol (or NO).
.De
Continuing with
.File /etc/defaults/rc.conf ,
.Dx
# Network Information Services (NIS) options: All need rpcbind_enable="YES" ###
nis_client_enable="NO"          # We're an NIS client (or NO).
nis_client_flags=""             # Flags to ypbind (if enabled).
nis_ypset_enable="NO"           # Run ypset at boot time (or NO).
nis_ypset_flags=""              # Flags to ypset (if enabled).
nis_server_enable="NO"          # We're an NIS server (or NO).
nis_server_flags=""             # Flags to ypserv (if enabled).
nis_ypxfrd_enable="NO"          # Run rpc.ypxfrd at boot time (or NO).
nis_ypxfrd_flags=""             # Flags to rpc.ypxfrd (if enabled).
nis_yppasswdd_enable="NO"       # Run rpc.yppasswdd at boot time (or NO).
nis_yppasswdd_flags=""          # Flags to rpc.yppasswdd (if enabled).
.De
More parameters for configuring NIS.  As mentioned above, this book does not
deal with NIS.
.Dx
### Network routing options: ###
defaultrouter="NO"              # Set to default gateway (or NO).
static_routes=""                # Set to static route list (or leave empty).
gateway_enable="NO"             # Set to YES if this host will be a gateway.
.De
See page
.Sref \*[route] \&
for more information on routing.  On \fIgw\fP\/ we add the following line to
.File /etc/rc.conf \/:
.Dx
.ft CB
defaultrouter="139.130.136.133" # Set to default gateway (or NO).
gateway_enable="YES"            # Set to YES if this host will be a gateway.\fP
.De
\fIandante\fP\/ gets its routing information from \fIDHCP\fP, so we don't need
to do anything here.
.Dx
router_enable="NO"              # Set to YES to enable a routing daemon.
router="/sbin/routed"           # Name of routing daemon to use if enabled.
router_flags="-q"               # Flags for routing daemon.
mrouted_enable="NO"             # Do multicast routing (see /etc/mrouted.conf).
mrouted_flags=""                # Flags for multicast routing daemon.
.De
These parameters relate to the routing daemons
.Daemon routed
and
.Daemon mrouted .
In the configurations we considered, you don't need them.
.Dx
ipxgateway_enable="NO"          # Set to YES to enable IPX routing.
ipxrouted_enable="NO"           # Set to YES to run the IPX routing daemon.
ipxrouted_flags=""              # Flags for IPX routing daemon.
.De
.ne 3v
IPX is a Novell proprietary networking protocol designed to be similar to IP.
FreeBSD supplies the daemon
.Daemon IPXrouted
(note the capitalization) which handles IPX routing tables.  See the man page
\fIIPXrouted(8)\fP for further details.
.Dx
arpproxy_all="NO"               # replaces obsolete kernel option ARP_PROXYALL.
forward_sourceroute="NO"        # do source routing
accept_sourceroute="NO"         # accept source routed packets to us
### ATM interface options: ###
atm_enable="NO"                 # Configure ATM interfaces (or NO).
#atm_netif_hea0="atm 1"         # Network interfaces for physical interface.
#atm_sigmgr_hea0="uni31"        # Signalling manager for physical interface.
#atm_prefix_hea0="ILMI"         # NSAP prefix (UNI interfaces only) (or ILMI).
#atm_macaddr_hea0="NO"          # Override physical MAC address (or NO).
#atm_arpserver_atm0="0x47.0005.80.999999.9999.9999.9999.999999999999.00"
#atm_scsparp_atm0="NO"          # Run SCSP/ATMARP on network interface (or NO).
atm_pvcs=""                     # Set to PVC list (or leave empty).
atm_arps=""                     # Set to permanent ARP list (or leave empty).
### ISDN interface options: (see also: /usr/share/examples/isdn) ###
isdn_enable="NO"                # Enable the ISDN subsystem (or NO).
isdn_fsdev="NO"                 # Output device for fullscreen mode
isdn_flags="-dn -d0x1f9"        # Flags for isdnd
isdn_ttype="cons25"             # terminal type for fullscreen mode
isdn_screenflags="NO"           # screenflags for ${isdn_fsdev}
isdn_trace="NO"                 # Enable the ISDN trace subsystem (or NO).
isdn_traceflags="-f /var/tmp/isdntrace0"  # Flags for isdntrace
.De
A few miscellaneous IP options and parameters for ATM and ISDN.  This book
doesn't discuss any of them.
.Dx
### Miscellaneous network options: ###
icmp_bmcastecho="NO"            # respond to broadcast ping packets
.De
.X "smurf"
This parameter relates to the so-called \fIsmurf\fP\/ ``denial of service''
attack: according to the RFCs, a machine should respond to a ping to its
broadcast address.  But what happens if somebody pings a remote network's
broadcast address across the Internet, as fast as he can?  Each system on the
remote network will reply, completely overloading the outgoing Internet
interface.  Yes, this is silly, but there are silly people out there.  If you
leave this parameter as it is, your system will not be vulnerable.  See
\fIhttp://www.cert.org/advisories/CA-98.01.smurf.html\fP\/ for more details.
.P
Next come a large number of options for \fIIPv6\fP, the new Internet protocol
standard.  This book doesn't deal with IPv6, and they're liable to change, so
they're not printed here.  Next, we find:
.Dx
##############################################################
###  System console options  #################################
##############################################################
keymap="NO"                     # keymap in /usr/share/syscons/keymaps/*
keyrate="NO"                    # keyboard rate to: slow, normal, fast
keybell="NO"                    # bell to duration.pitch or normal or visual
keychange="NO"                  # function keys default values
cursor="NO"                     # cursor type {normal|blink|destructive}
scrnmap="NO"                    # screen map in /usr/share/syscons/scrnmaps/*
font8x16="NO"                   # font 8x16 from /usr/share/syscons/fonts/*
font8x14="NO"                   # font 8x14 from /usr/share/syscons/fonts/*
font8x8="NO"                    # font 8x8 from /usr/share/syscons/fonts/*
blanktime="300"                 # blank time (in seconds) or "NO" to turn it off.
saver="NO"                      # screen saver: Uses /boot/kernel/${saver}_saver.ko
.De
These parameters describe the use of alternate keyboard mappings when using the
standard character-based terminals only.  See the files in
.Directory /usr/share/syscons/keymaps
for key map files, and
.Directory /usr/share/syscons/fonts
for alternate fonts.  These parameters have no effect on the X-based displays
that this book assumes.  You can enable a screen saver by setting the variable
\f(CWsaver\fP to \f(CWYES\fP.
.Dx
moused_enable="NO"              # Run the mouse daemon.
moused_type="auto"              # See man page for available settings.
moused_port="/dev/psm0"         # Set to your mouse port.
moused_flags=""                 # Any additional flags to moused.
mousechar_start="NO"            # if 0xd0-0xd3 default range is occuped in your
                                # language code table, specify alternative range
allscreens_flags=""             # Set this vidcontrol mode for all virtual screens
allscreens_kbdflags=""          # Set this kbdcontrol mode for all virtual screens
.De
Parameters for
.Daemon moused ,
a mouse driver for the character-based terminals,
and global flags for virtual screens.  If you're using an X server, you should
run
.Daemon moused .
On \fIandante\fP, we add this line to
.File /etc/rc.conf \/:
.Dx
.ft CB
moused_enable="YES"\fP
.De
Next follow some definitions for the alternative console driver \fIpcvt\fP,
which we don't look at here, followed by a section describing the mail
configuration:
.Dx
##############################################################
###  Mail Transfer Agent (MTA) options  ######################
##############################################################
mta_start_script="/etc/rc.sendmail"
                                # Script to start your chosen MTA
# Settings for /etc/rc.sendmail:
sendmail_enable="NO"            # Run the sendmail inbound daemon (YES/NO).
sendmail_flags="-L sm-mta -bd -q30m" # Flags to sendmail (as a server)
sendmail_submit_enable="YES"    # Start a localhost-only MTA for mail submission
sendmail_submit_flags="-L sm-mta -bd -q30m -ODaemonPortOptions=Addr=localhost"
                                # Flags for localhost-only MTA
sendmail_outbound_enable="YES"  # Dequeue stuck mail (YES/NO).
sendmail_outbound_flags="-L sm-queue -q30m" # Flags to sendmail (outbound only)
sendmail_msp_queue_enable="YES" # Dequeue stuck clientmqueue mail (YES/NO).
sendmail_msp_queue_flags="-L sm-msp-queue -Ac -q30m"
.De
Since FreeBSD Release 5, the
.Daemon sendmail
MTA is no longer enabled by default.  If you have been running
.Daemon sendmail
on an older release of FreeBSD, add an entry to
.File /etc/rc.conf
to keep it running.
.Dx
##############################################################
###  Miscellaneous administrative options  ###################
##############################################################
cron_enable="YES"               # Run the periodic job daemon.
cron_program="/usr/sbin/cron"   # Which cron executable to run (if enabled).
cron_flags=""                   # Which options to pass to the cron daemon.
.De
Run
.Daemon cron ,
the daemon responsible for running things at specific times.  See
page
.Sref \*[cron] \&
for a description of
.Daemon cron .
Leave this enabled unless you have a good reason not to.
.Dx
lpd_enable="NO"                 # Run the line printer daemon.
lpd_program="/usr/sbin/lpd"     # path to lpd, if you want a different one.
lpd_flags=""                    # Flags to lpd (if enabled).
.De
See page
.Sref \*[printers] \&
for a discussion of printing.  In older releases of FreeBSD, \f(CWlpd_enable\fP
was set to \f(CWYES\fP.  Now, to run
.Daemon lpd ,
we need to put the following
line in
.File /etc/rc.conf
for both \fIgw\fP\/ and \fIadagio\fP\/:
.Dx
.ft CB
lpd_enable="YES"                # Run the line printer daemon.\fP
.De
Next, we see:
.Dx
usbd_enable="NO"                # Run the usbd daemon.
usbd_flags=""                   # Flags to usbd (if enabled).
.De
Run
.Daemon usbd ,
.X "Universal Serial Bus"
.X "USB"
the \fIUniversal Serial Bus\fP\/ or \fIUSB\fP\/ daemon.  See the man pages
\fIusbd(8)\fP\/ and \fIusb(4)\fP\/ for more information.
.Dx
dumpdev="NO"                    # Device name to crashdump to (if enabled).
dumpdir="/var/crash"            # Directory where crash dumps are to be stored
savecore_flags=""               # Used if dumpdev is enabled above, and present.
.De
These parameters specify how to take dumps when the system panics.  See page
.Sref \*[panic] \&
for details.  As mentioned there, it is preferable to set this value in
.File /boot/loader.conf \/:
that way you can still get a dump if your system panics before reading
.File /etc/rc.conf ,
so we don't change anything here.
.\" XXX check we don't have to do it again.
.P
Continuing with
.File /etc/defaults/rc.conf ,
.Dx
enable_quotas="NO"              # turn on quotas on startup
check_quotas="YES"              # Check quotas on startup
accounting_enable="NO"          # Turn on process accounting
ibcs2_enable="NO"               # Ibcs2 (SCO) emulation loaded at startup
ibcs2_loaders="coff"            # List of additional Ibcs2 loaders
sysvipc_enable="NO"             # Load System V IPC primitives at startup
linux_enable="NO"               # Linux binary compatibility loaded at startup
svr4_enable="NO"                # SysVR4 emulation loaded at startup
osf1_enable="NO"                # Alpha OSF/1 emulation loaded at startup
.De
.ne 3v
We don't discuss quotas or accounting in this book.  We looked at the parameters
\f(CWibcs2_enable\fP on page
.Sref \*[SCO-emulation] \&
and \f(CWlinux_enable\fP on page
.Sref \*[Linux-emulation] .
We also don't discuss System V and OSF-1 emulation.
.Dx
clear_tmp_enable="NO"           # Clear /tmp at startup.
.De
In the old days, the startup sequence automatically deleted everything in the
file system
.Directory /tmp .
Sometimes this wasn't desirable, so now it's your choice.  Change this value to
\f(CWYES\fP if you want the old behaviour.
.P
Note that if you use a
.Directory /tmp
based on MFS (memory file system), this variable has no effect.  The contents of
MFS file systems disappear on reboot.
.Dx
ldconfig_insecure="NO"          # Set to YES to disable ldconfig security checks
ldconfig_paths="/usr/lib/compat /usr/X11R6/lib /usr/local/lib"
                                # shared library search paths
ldconfig_paths_aout="/usr/lib/compat/aout /usr/X11R6/lib/aout /usr/local/lib/aout"
                                # a.out shared library search paths
.De
.Command ldconfig
maintains the dynamic library cache required for finding libraries when starting
most processes.  Potentially this can be a security issue, and
.Command ldconfig
makes a number of security checks before accepting
libraries.  If you really want to, you can disable these checks by setting
\f(CWldconfig_insecure\fP.  The two other variables are lists of the directories
that are searched to find \fIELF\fP\/ and \fIa.out\fP\/ dynamic libraries,
respectively.  See page
.Sref \*[dynamic-libraries] \&
for more details.  You would normally not remove anything from these lists, but
you might want to add something.
.Dx
kern_securelevel_enable="NO"    # kernel security level (see init(8)),
kern_securelevel="-1"           # range: -1..3 ; `-1' is the most insecure
update_motd="YES"               # update version info in /etc/motd (or NO)
.De
The kernel runs with five different levels of security.  Any super-user process
can raise the security level, but only
.Daemon init
can lower it.  The security levels are:
.LB 16p 0 0 1 1
.\" Kludge: set the first count to -1.  There must be an easier way to do this.
.nr li*cnt!\n[li*lvl] -2
.LI
Permanently insecure mode: always run the system in level 0 mode.  This is the
default initial value.
.LI
Insecure mode: the immutable and append-only flags may be turned off.  All
devices may be read or written subject to their permissions.
.LI
Secure mode: the immutable and append-only flags may not be turned off.  Disks
for mounted filesystems,
.Device mem
and
.Device kmem
may not be opened for writing.
.LI
Highly secure mode.  This is the same as secure mode with the addition that
disks may not be opened for writing (except by \fImount(2)\fP\/), whether or not
they are mounted.  This level precludes tampering with filesystems by unmounting
them, but it also prevents running \fInewfs(8)\fP\/ while the system is
multi-user.
.LI
Network secure mode.  This is the same as highly secure mode with the addition
that IP packet filter rules (see page
.Sref \*[firewall-configuration] )
can not be changed and dummynet configuration can not be adjusted.  We don't
discuss dummynet in this book.
.Le
To set the secure level to anything except -1, set the variable
\f(CWkern_securelevel\fP to the value you want, and set
\f(CWkern_securelevel_enable\fP to \f(CWYES\fP.
.Dx
start_vinum="NO"                # set to YES to start vinum
.De
.X "vinum"
We looked at \fIVinum\fP\/ on page
.Sref \*[vinum] .
There we put the following text into
.File /etc/rc.conf
to start it on booting:
.Dx
.ft CB
start_vinum="YES"               # set to YES to start vinum\fP
.De
Finally we have a few miscellaneous entries:
.Dx
unaligned_print="YES"           # print unaligned access warnings on the alpha
entropy_file="/entropy"         # Set to NO disables caching entropy through reboots
entropy_dir="/var/db/entropy"   # Set to NO to disable caching entropy via cron.
entropy_save_sz="2048"          # Size of the entropy cache files.
entropy_save_num="8"            # Number of entropy cache files to save.
harvest_interrupt="YES"         # Entropy device harvests interrupt randomness
harvest_ethernet="YES"          # Entropy device harvests ethernet randomness
harvest_p_to_p="YES"            # Entropy device harvests point-to-point randomness
dmesg_enable="YES"              # Save dmesg(8) to /var/run/dmesg.boot
.De
\f(CWunaligned_print\fP is a diagnostic tool for the Alpha processor; there's a
good chance it will go away.  \f(CWdmesg_enable\fP saves the boot messages to
the file
.File /var/run/dmesg.boot .
Leave it this way; the messages are often useful for reference, and after a
certain number of messages, they get flushed from the kernel internal message
buffer.
.P
The other messages are used for configuring entropy harvesting for the \fIrandom
number devices\fP,
.Device random
and
.Device urandom .
See \fIrandom(4)\fP\/ for further details.  Under normal circumstances you
shouldn't change them.
.H3 "Our /etc/rc.conf"
To summarize the changes from the defaults,
.File /etc/rc.conf
for \fIgw\fP\/ should now contain the following entries:
.Dx
hostname="gw.example.org"
firewall_enable="YES"           # Set to YES to enable firewall functionality
firewall_type="client"          # Firewall type (see /etc/rc.firewall)
natd_enable="YES"               # Enable natd (if firewall_enable == YES).
natd_interface="tun0"           # Public interface or IPaddress to use.
syslogd_flags=""                # Allow logging from other systems
inetd_enable="YES"              # Run \fIinetd\fP\/
named_enable="YES"              # Run named, the DNS server (or NO).
sshd_enable="YES"               # Enable sshd
nfs_client_enable="YES"         # This host is an NFS client (or NO).
nfs_server_enable="YES"         # This host is an NFS server (or NO).
ntpd_enable="YES"               # Run ntpd Network Time Protocol (or NO).
defaultrouter="139.130.136.133" # Set to default gateway (or NO).
gateway_enable="YES"            # Set to YES if this host will be a gateway.
lpd_enable="YES"                # Run the line printer daemon.
start_vinum="YES"               # set to YES to start vinum
.De
.ne 5v
The corresponding
.File /etc/rc.conf
for \fIandante\fP\/ should now contain the following entries:
.Dx
hostname="andante.example.org"
inetd_enable="YES"              # Run \fIinetd\fP\/
nfs_client_enable="YES"         # This host is an NFS client (or NO).
ntpd_enable="YES"               # Run ntpd Network Time Protocol (or NO).
moused_enable="YES"             # Run the mouse daemon
lpd_enable="YES"                # Run the line printer daemon.
start_vinum="YES"               # set to YES to start vinum
.De
.SPUP
.H2 "Files you need to change"
.Pn changeable-config-files
\fIrc.conf\fP\/ is only part of the story, of course.  The
.Directory /etc
directory contains a large number of other files, nearly all of them relating to
the configuration.  Some of them, like
.File /etc/amd.map
and
.File /etc/dm.conf ,
are intended for specific subsystems that we don't discuss here.  In general,
they have comments in them to explain their purpose, and they have a man page in
section 5 of the manual.
.P
Most of the files in
.Directory /etc
are intended to be left the way they are.  Some, however, will definitely need
changing, and there are others that you may need to change.  In this section
we'll look at the ones you almost certainly need to change, and on page
.Sref \*[maybe-config-files] \&
we'll look at the ones you may have to change.  On page
.Sref \*[ro-config-files] \&
we'll look at the some of the more interesting ones you should normally leave
alone.
.H3 "/etc/exports"
.Pn etc-exports
.File /etc/exports
is a list of file systems that should be NFS exported.  We looked at it on page
.Sref \*[/etc/exports] .
See also the man page \fIexports(5)\fP.
.H3 "/etc/fstab"
.Pn /etc/fstab
.File /etc/fstab
contains a list of file systems known to the system.  The script
.File /etc/rc
starts
.Command mount
twice during system startup first to mount the local file
systems, and later to mount the NFS file system.
.Command mount
will mount all
file systems in
.File /etc/fstab
unless they are explicitly excluded.
.P
.X "freebie.example.org"
Here's a typical
.File /etc/fstab ,
from host \fIfreebie.example.org\fP\/:
.Dx
# File system      Mount point             fstype  flags
/dev/ad0s1a        /                       ufs     rw                      1 1
/dev/ad0s1b        none                    swap    sw                      0 0
/dev/ad0s1e        /usr                    ufs     rw                      2 2
/dev/da0b          none                    swap    sw                      0 0
/dev/da0h          /src                    ufs     rw                      2 2
/dev/da1h          /home                   ufs     rw                      2 2
/dev/da2b          none                    swap    sw                      0 0
/dev/da2e          /S                      ufs     rw,noauto               2 2
/dev/da3a          /mod                    ufs     rw,noauto               0 0
proc               /proc                   procfs  rw                      0 0
/dev/cd0a          /cdrom/1                cd9660  ro,noauto               0 0
/dev/cd1a          /cdrom/2                cd9660  ro,noauto               0 0
/dev/cd2a          /cdrom/3                cd9660  ro,noauto               0 0
/dev/cd3a          /cdrom/4                cd9660  ro,noauto               0 0
/dev/cd4a          /cdrom/5                cd9660  ro,noauto               0 0
/dev/cd5a          /cdrom/6                cd9660  ro,noauto               0 0
/dev/cd6a          /cdrom/7                cd9660  ro,noauto               0 0
/dev/cd7a          /cdrom/8                cd9660  ro,noauto               0 0
presto:/           /presto                 nfs     soft,rw,noauto          0 0
presto:/usr        /presto/usr             nfs     soft,rw,noauto          0 0
presto:/home       /presto/home            nfs     soft,rw,noauto          0 0
bumble:/           /bumble                 nfs     soft,rw,noauto          0 0
bumble:/usr        /bumble/usr             nfs     soft,rw,noauto          0 0
wait:/C            /C                      nfs     soft,rw,noauto          0 0
wait:/             /wait                   nfs     soft,rw,noauto,tcp      0 0
.De
This information has the following meaning:
.Ls B
.LI
.X "UFS"
.X "cd9660, file system"
.X "proc"
The first column contains either the name of a device (for swap, \fIufs\fP\/ and
\fIcd9660\fP\/ file systems), the name of a file system (for NFS file systems),
or \f(CWproc\fP for the \fIproc\fP\/ file system.
.LI
The lines beginning with \f(CW#\fP are \fIcommented out\fP\/:
.Command mount
ignores them completely.
.LI
The second column is either a mount point or the keyword \f(CWnone\fP in the
case of a partition that is not mounted, such as swap.
.LI
The third column is the kind of file system (or \f(CWswap\fP).
.LI
The fourth column are flags relating to the particular file system being
mounted.  We'll look at them below.
.LI
The fifth column is used by \fIdump(8)\fP\/ to determine how often to dump the
file system.  0 means not to dump the file system.
.Command dump
only understands ufs file systems, so this field should be 0 for all other file
systems.
.LI
The sixth column is the \fIpass number\fP\/ for running
.Command fsck
at boot
time.  Again, 0 means ``don't run
.Command fsck .''
Normally you set pass 1 only for the root file system, and pass 2 for other file
systems.
.Le
The flags are worth a closer look.  Some of the more common are:
.br
.ne 1i
.Table-heading "Mount flags"
.TS
tab(#) ;
 lfCWp9 | lw56 .
\fRFlag#Purpose
_
ro#Mount read-only.
rw#Mount read/write.
sw#Mount as swap.
noauto#Don't mount automatically.
soft#T{
For an NFS mount, fail if the request times out.  If you don't specify this
option, NFS will keep retrying for ever.
T}
tcp#T{
For NFS only, mount with TCP transport rather than the standard UDP transport.
This feature is supported almost only by BSD systems\(emcheck whether the other
end offers TCP transport.
T}
.TE
.sp 1.5v
For NFS mount flags, see Chapter
.Sref \*[nchclient] ,
page
.Sref \*[NFS-mount-flags] .
.P
Why are there so many entries with the \f(CWnoauto\fP keyword?  If you don't
bother to mount them, why bother to mention them?
.P
If file system has an entry in
.File /etc/fstab ,
.Command mount
is clever enough to get all the information it needs from this file.  You just
need to specify the name of the mount point or the name of the special device
(for \fIufs\fP\/ and \fIcd9660\fP\/) or the remote file system (for NFS).  This
is particularly useful for \fIcd9660\fP, the CD file system type.  Without an
entry in
.File /etc/fstab ,
you would have to write something like the following to mount a CD-ROM.
.Dx
# \f(CBmount -t cd9660 -o ro /dev/cd0a /cdrom\fP
.De
With the entry, you can simplify this to:
.Dx
# \f(CBmount /cdrom\fP
.De
.SPUP
.H3 "/etc/group"
.File /etc/group
defines the groups known to the system.  You normally update it when adding
users, for example with
.Command vipw
or
.Command adduser ,
though you can also edit
it directly.  See page
.Sref \*[group] \&
for more details.
.H3 "/etc/namedb/named.conf"
.X "named, daemon"
.X "daemon, named"
.File /etc/named/named.conf
is the main configuration file for
.Daemon named ,
the Domain Name Service daemon.
We looked at it in Chapter
.Sref \*[nchdns] .
Previous versions of
.Daemon named
used a different form of configuration file stored in
.File /etc/named.boot .
.H3 "/etc/mail"
The directory
.Directory /etc/mail
contains configuration information for some MTAs, including
.Daemon sendmail .
.H3 "/etc/master.passwd"
.File /etc/master.passwd
is the real password file.  Like
.File /etc/group ,
you update with
.Command vipw
or
.Command adduser .
See page
.Sref \*[master-passwd] \&
for more details.
.H2 "Files you might need to change"
.Pn maybe-config-files
You don't need to customize any of the following files to get the system up and
running.  You may find it necessary to change them to do specific things,
however.
.H3 "/etc/crontab"
.X "cron, daemon"
.X "daemon, cron"
.File /etc/crontab
describes the jobs to be performed by
.Daemon cron
on behalf of the system.  You
don't have to use this file at all; you can use each user's
.File crontab
files instead.  Note that this file has a slightly different format from the
other
.File crontab
files.  A user's
.File crontab
contains entries like this:
.Dx
0       0       *       *       *       /home/grog/Scripts/rotate-log
.De
This line runs the script
.File /home/grog/Scripts/rotate-log
at midnight every day.  If you put this entry into
.File /etc/crontab ,
you need to tell
.Daemon cron
which user to run it as.  Do this by putting the
name of the user before the command:
.Dx
0       0       *       *       *       grog    /home/grog/Scripts/rotate-log
.De
See page
.Sref \*[cron] \&
for more details about
.Daemon cron .
.H3 "/etc/csh.cshrc, /etc/csh.login, /etc/csh.logout"
These are default initialization files for
.Command csh .
See the man page
\fIcsh(1)\fP\/ for more details.
.H3 "/etc/dhclient.conf"
.File /etc/dhclient.conf
describes the client side of DHCP services.  Normally it's empty.  We discussed
\fIdhcp\fP\/ on
.Sref \*[DHCP] .
.H3 "/etc/disktab"
.File /etc/disktab
contains descriptions of disk geometries for
.Command disklabel .
This is almost
obsolete.
.H3 "/etc/ftpusers"
.File /etc/ftpusers
is a list of users who are \fInot\fP\/ allowed to connect to this system using
.Command ftp .
It's a strong contender for the prize for the worst-named file in the system.
.H3 "/etc/hosts"
.Pn etc-hosts
For a small network, especially if you're not permanently connected to the
Internet, you have the option of placing the addresses of the systems you want
to talk to in a file called
.File /etc/hosts .
This file is simply a list of IP addresses and host names, for example:
.Dx
# Local network host addresses
#
# loopback address for all systems
127.1 loopback local localhost
###### domain example.com.
#
223.147.37.1   freebie freebie.example.org        # FreeBSD 3.0
223.147.37.2   presto.example.org presto          # 66 MHz 486 (BSD UNIX)
.De
.ne 3v
Before the days of DNS, this was the way to resolve IP addresses.  It only works
locally, and even there it's a pain to maintain: you need to propagate every
update to every machine on the network.  As we saw in Chapter
.Sref \*[nchdns] ,
it's far preferable to run
.Daemon named ,
even if you're not connected to the Internet.
.H3 "/etc/hosts.equiv"
.File /etc/hosts.equiv
is a list of hosts whose users may use
.Command rsh
to access this system without supplying a password.
.Command rsh
is now obsolete, so it's unlikely you'll need to change this file.  See the
description of
.Command ssh
on page
.Sref "\*[ssh]" \&
for a replacement.
.H3 "/etc/hosts.lpd"
.X "lpd, daemon"
.X "daemon, lpd"
.File /etc/hosts.lpd
is a list of hosts that can use the
.Daemon lpd
spooler on this system.
.H3 "/etc/inetd.conf"
.X "inetd, daemon"
.X "daemon, inetd"
.File /etc/inetd.conf
is the configuration file for
.Daemon inetd ,
the Internet daemon.  It dates back
to the original implementation of TCP/IP in 4.2BSD, and the format is the same
for all versions of UNIX.  We have looked at various modifications to this file
throughout the network part of the book.  See the index (\fIinetd.conf\fP\/) and
the man page \fIinetd.conf(5)\fP\/ for further details.  FreeBSD now disables
all services by default to limit security exposures, so there's a good chance
you'll have to edit this file.
.H3 "/etc/login.access"
.File /etc/login.access
is a file that limits remote access by individual users.  We don't look at it
in more detail here.
.H3 "/etc/login.conf"
.Pn login-class
.File /etc/login.conf
describes user parameters set at login time.
.P
.X "login class"
In UNIX tradition, \f(CWroot\fP has been the owner of the universe.  This is
rather primitive, and the 4.3BSD Net/2 relase introduced \fIlogin classes\fP,
which determine session accounting, resource limits and user environment
settings.  Many programs use the database described in
.File /etc/login.conf
to set up a user's login environment and to enforce policy, accounting and
administrative restrictions.  The login class database also provides the means
to authenticate users to the system and to choose the type of authentication.
.P
When creating a user, you may optionally enter a class name, which should match
an entry in
.File /etc/login.conf \(emsee
page
.Sref \*[user-class] \&
for more details.  If you don't, the system uses the entry \f(CWdefault\fP for a
non-root user.  For the root user, the system uses the entry \f(CWroot\fP if it
is present, and \f(CWdefault\fP otherwise.
.P
The structure of the login configuration database is relatively extensive.  It
describes a number of parameters, many of which can have two values: a
\fIcurrent\fP\/ value and a \fImaximum\fP\/ value.  On login, the system sets
the values to the \f(CW-cur\fP (current) value, but the user may, at his option,
increase the value to the \f(CW-max\fP (maximum) value.  We'll look at the
\f(CWdefault\fP entry for an example.
.Dx
default:\e
        :passwd_format=md5:\e
        :copyright=/etc/COPYRIGHT:\e
        :welcome=/etc/motd:\e
        :setenv=MAIL=/var/mail/$,BLOCKSIZE=K,FTP_PASSIVE_MODE=YES:\e
        :path=/sbin /bin /usr/sbin /usr/bin /usr/games /usr/local/sbin /usr/local/bi
n /usr/X11R6/bin ~/bin:\e
        :nologin=/var/run/nologin:\e
        :cputime=unlimited:\e
        :datasize=unlimited:\e
        :stacksize=unlimited:\e
        :memorylocked=unlimited:\e
        :memoryuse=unlimited:\e
        :filesize=unlimited:\e
        :coredumpsize=unlimited:\e
        :openfiles=unlimited:\e
        :maxproc=unlimited:\e
        :sbsize=unlimited:\e
        :vmemoryuse=unlimited:\e
        :priority=0:\e
        :ignoretime@:\e
        :umask=022:
.De
.\"XXX check this.
.X backslash
As in the password file, the fields are delimited by colons (\f(CW:\fP).  In
this example, though, lines are \fIcontinued\fP\/ by placing a backslash
(\f(CW\e\fP) at the end of each line except the last.  This usage is common in
UNIX.  Unlike Microsoft usage, a backslash is never used to represent a
directory.
.P
.ne 5v
This entry defines the following parameters:
.Ls B
.LI
\f(CWpasswd_format\fP controls the password format used for new passwords.  It
takes the values \f(CWdes\fP, \f(CWmd5\fP or \f(CWblf\fP.  See the
\fIlogin.conf(5)\fP manual page for more information about login capabilities.
.LI
Processes may use as much CPU time as they want.  If you change this, you can
stop processes that use more than a specific amount of CPU time.
.LI
The current maximum sizes of the user data segment and the stack are set to 64
MB.  The entry doesn't define maximum values for these parameters.
.LI
The user may lock a maximum of 10 MB of memory per process.
.LI
The total memory use per process may not exceed 100 MB.
.LI
There is no limit on the size of data files or core dump files that the user may
create.
.LI
The user may have up to 64 processes.
.LI
Each process may have up to 64 open files.  For some programs, this could be a
limitation.
.LI
The user \fIneed not\fP\/ have a home directory to log in.  The \f(CW@\fP symbol
specifies that the preceding symbol (\f(CWrequirehome\fP) should be undefined.
As a result, the system does not require the home directory.
.LI
.X "umask"
By default, the
.Command umask
is set to \f(CW022\fP.  See page
.Sref \*[umask] \&
for more details of
.Command umask .
.LI
The system uses the default authentication scheme for this user.
.Le
See the man page \fIlogin.conf(5)\fP\/ for further details.
.H3 "/etc/motd"
.X "message of the day"
.File /etc/motd
(\fImessage of the day\fP\/) is a file whose contents are printed out at login.
You can put any message you like in it.  See page
.Sref \*[motd] \&
for an example.
.H3 "/etc/newsyslog.conf"
.Pn newsyslog.conf
.File /etc/newsyslog.conf
contains configuration information for the
.Command newsyslog
command: which log files to archive, how many copies, and whether to compress.
See \fInewsyslog(8)\fP\/ for further details.  If you generate a lot of logging
information, you may need to modify this file to avoid overflowing the file
system with your
.Directory /var/log
directory.
.H3 "/etc/nsswitch.conf"
.File /etc/nsswitch.conf
tells the resolver how to perform name resolution.  This file format comes from
Solaris and replaces the older
.File /etc/host.conf .
It gives you the flexibility to use both
.File /etc/hosts
and DNS lookups, for
example.  You specify the lookup sequence for hostnames with a line like this:
.Dx 1
hosts:   files dns
.De
The word \f(CWhosts\fP here specifies the type of lookup (for host names, not
NIS, password entries or something else).  The keyword \f(CWfile\fP represents
the
.File /etc/hosts
file in this case.  This file is not installed by
default; see the man page \fInsswitch.conf(8)\fP\/ if you need to use it.
.H3 "/etc/pccardd.conf"
.File /etc/pccardd.conf
and its companion
.File /etc/defaults/pccardd.conf
are the configuration files for
.Daemon pccardd .
We looked at them in detail in
Chapter
.Sref \*[nchnetsetup] ,
on page
.Sref \*[PCMCIA-net] .
.H3 "/etc/periodic.conf"
.File /etc/periodic.conf
controls how to perform the maintenance jobs that
.Daemon cron
runs during the night:
.Dx
# Perform daily/weekly/monthly maintenance.
1       3       *       *       *       root    periodic daily
15      4       *       *       6       root    periodic weekly
30      5       1       *       *       root    periodic monthly
.De
Like
.File /etc/rc.conf ,
.File /etc/periodic.conf
is an optional file which overrides the default file
.File /etc/defaults/periodic.conf .
You don't need to change it at all, but you may find it worthwhile.  Read the
man page \fIperiodic.conf(5)\fP\/ or the file
.File /etc/defaults/periodic.conf
for more details.
.H3 "/etc/printcap"
.File /etc/printcap
describes the printers connected to a system.  See page
.Sref \*[printcap] \&
for more details.
.H3 "/etc/profile"
.File /etc/profile
is a default startup file for Bourne-style shells.  See page
.Sref \*[profile] \&
for more details.
.H3 "/etc/rc.firewall"
.File /etc/rc.firewall
.X "ipfw, firewall"
.X "firewall, ipfw"
is used to initialize the packet filtering firewall \fIipfw\fP.  See page
.Sref \*[firewall-configuration] \&
for further details.
.H3 "/etc/resolv.conf"
.File /etc/resolv.conf
is used by the \fIresolver\fP\/ library to locate name servers to perform DNS
lookups.  See
.Sref \*[resolv.conf] \&
for more details.
.H3 "/etc/syslog.conf"
.\" XXX more description?
.X "syslogd, daemon"
.X "daemon, syslogd"
.File /etc/syslog.conf
is the configuration file for
.Daemon syslogd .
See \fIsyslogd.conf(5)\fP\/ for
further details.
.H3 "/etc/ttys"
.X "init, daemon"
.X "daemon, init"
.File /etc/ttys
is a file that describes terminals and pseudo-terminals to
.Daemon init .
We've
looked at it in a number of places: check the index.
.P
Here's an excerpt from the default
.File /etc/ttys \/:
.Dx
# This entry needed for asking password when init goes to single-user mode
# If you want to be asked for password, change "secure" to "insecure" here
console none                                    unknown off secure
.De
The system console.  This is not a real terminal: it can be moved from one
device to another.  By default, it corresponds to
.Device ttyv0
(the next entry).
.Dx
ttyv0   "/usr/libexec/getty Pc"         cons25  on  secure
.De
This is the first virtual terminal, the one that you get automatically at boot
time.  To change to the others, press \fBAlt\fP-\fBF\f(BIx\fR, where \f(BIx\fP\/
is between 1 and 16.  This will give you one of the others:
.Dx
# Virtual terminals
ttyv1   "/usr/libexec/getty Pc"         cons25  on secure
ttyv2   "/usr/libexec/getty Pc"         cons25  on secure
ttyv3   "/usr/libexec/getty Pc"         cons25  on secure
\fI(etc)\fP\/
ttyv8   "/usr/X11R6/bin/xdm -nodaemon"  xterm   off secure
.De
.P
.ne 3v
Each virtual terminal can support either a login or an X server.  The default
.File /etc/ttys
enables
.Command getty
on the first eight virtual terminals and reserves
.Device -n ttyv8
for X.  If you don't enable
.Daemon xdm
on this terminal, you start X with
.Command startx .
.P
The default kernel supports sixteen virtual terminals, the maximum possible
value.  The kernel configuration parameter \f(CWMAXCONS\fP (defined in
.File /usr/src/sys/conf/NOTES )
allows you to reduce this number.
.Dx
# Serial terminals
ttyd0   "/usr/libexec/getty std.9600"   unknown off secure
ttyd1   "/usr/libexec/getty std.9600"   unknown off secure
ttyd2   "/usr/libexec/getty std.9600"   unknown off secure
ttyd3   "/usr/libexec/getty std.9600"   unknown off secure
.De
These are the serial ports on your machine.  It doesn't matter if it contains
names that correspond to non-existent hardware, such as
.Device ttyd3 ,
as long as you don't try to enable them.
.Dx
# Pseudo terminals
ttyp0           none                    network
ttyp1           none                    network
.De
There's a whole list of these.  The purpose here is to tell network programs
the properties of the terminal: in particular, they're not \f(CWsecure\fP, which
means that you're not allowed to log in on them as \f(CWroot\fP.
.H3 "/boot/device.hints"
.Pn device-hints
The file
.File /boot/device.hints
contains configuration information that was previously stored in the kernel
configuration file, and which could not be changed except by building and
running a new kernel.  Now it can be set at boot time.  For these changes to
apply, you may still need to reboot, but you no longer need a new kernel.
.P
The information in
.File /boot/device.hints
consists of structured variables.  For example, consider a third ISA serial I/O
port, called
.Device -n sio2
in FreeBSD.  By default, this port gets IRQ 4, the same as
.Device sio0 .
This just plain doesn't work, so you will have to find a different IRQ.  The
hints for this device start with the text \f(CWhint.sio.2\fP.  The default
.File /boot/device.hints
contains the following parameters for this device:
.Dx
hint.sio.2.at="isa"
hint.sio.2.disabled="1"
hint.sio.2.port="0x3E8"
hint.sio.2.irq="5"
.De
In sequence, these ``hints'' state that the device is connected to the ISA bus,
that it is disabled (in other words, the kernel does not probe for it), that its
device registers are at address \f(CW0x3e8\fP, and that it interrupts at IRQ 5.
Modern motherboards configure this device via the BIOS.  You may find that on
your system it's more appropriate to run at IRQ 11.  In this case, in
.File /boot/device.hints ,
you would remove the ``disabled'' line and change the \f(CWirq\fP line:
.Dx
hint.sio.2.at="isa"
hint.sio.2.port="0x3E8"
hint.sio.2.irq="\f(CB11\fP"
.De
In some cases, you can change these flags at run time with the
.Command kenv
command.  For example, to change the flags of the first serial port to
\f(CW0x90\fP to run a serial debugger, you might enter:
.Dx
# \f(CBkenv hint.sio.0.flags\fP
0x0
# \f(CBkenv hint.sio.0.flags=0x90\fP
hint.sio.0.flags="0x90"
.De
.SPUP
.H4 "Wiring down SCSI devices"
.X "SCSI, target names"
.X "target names, SCSI"
.X "wiring down SCSI devices"
.X "SCSI, wiring down devices"
.X "LUN"
Another example is so-called \fIwiring down\fP\/ SCSI and ATA devices.  We
looked at this issue on page
.Sref \*[installing-scsi] .
By default, FreeBSD assigns SCSI unit numbers in the order in which it finds the
devices on the SCSI bus.  If you remove or add a disk drive, the numbers change,
and you may have to modify your
.File /etc/fstab
file.  To avoid this problem, you can \fIwire down\fP your SCSI devices so that
a given bus, target, and unit (\fILUN\fP\/) always come on line as the same
device unit.
.P
The unit assignment begins with the first non-wired down unit for a device type.
Units that are not specified are treated as if specified as LUN 0.  For example,
if you wire a disk as \f(CWsd3\fP, the first non-wired disk is assigned
\f(CWsd4\fP.
.P
.ne 3v
The following example shows configuration file entries for a system with three
Adaptec 2940 class host adapters, one of them with two buses.  The first has a
disk on ID 0 that we want to call
.Device -n da0 ,
the second has a tape on ID 6 that we want to call
.Device -n sa0 ,
the first bus of the third adapter has a disk on ID 3 that we want to call
.Device -n da2 ,
and the second bus of the third adapter has a disk on ID 1 that we want to call
.Device -n da1 .
.Dx
hint.scbus.0.at="ahc0"                          \fIscbus0 is ahc0\fP\/
hint.scbus.1.at="ahc1"                          \fIscbus1 is ahc1 bus 0 (only one)\fP\/
hint.scbus.1.bus="0"
hint.scbus.3.at="ahc2"                          \fIscbus3 is ahc2 bus 0\fP\/
hint.scbus.3.bus="0"
hint.scbus.2.at="ahc2"                          \fIscbus2 is ahc2 bus 1\fP\/
hint.scbus.2.bus="1"
hint.da.0.at="scbus0"                           \fIda0 is on scsbus0, target 0, unit 0\fP\/
hint.da.0.target="0"
hint.da.0.unit="0"
hint.da.1.at="scbus3"                           \fIda1 is on scsbus3, target 1\fP\/
hint.da.1.target="1"
hint.da.2.at="scbus2"                           \fIda2 is on scsbus2, target 3\fP\/
hint.da.2.target="3"
hint.sa.1.at="scbus1"                           \fIsa1 is on scsbus1, target 6\fP\/
hint.sa.1.target="6"
.De
If there are any other devices connected to the host adapters, they are assigned
other names in sequence, starting at
.Device -n da3
and
.Device -n sa2 .
.P
.ne 5v
This arrangement corresponds to the earlier syntax in the kernel configuration
file:
.Dx
controller      scbus0 at ahc0          # Single bus device
controller      scbus1 at ahc1 bus 0    # Single bus device
controller      scbus3 at ahc2 bus 0    # Twin bus device
controller      scbus2 at ahc2 bus 1    # Twin bus device
disk            da0 at scbus0 target 0 unit 0
disk            da1 at scbus3 target 1
disk            da2 at scbus2 target 3
tape            sa1 at scbus1 target 6
device  cd0 at scbus?
.De
.SPUP
.H2 "Files you should not change"
.Pn ro-config-files
The files in the following section are there as a kind of reference information.
Normally you should not change them, though there might be exceptional
circumstances where it makes sense to change them.
.H3 "/etc/gettytab"
.File /etc/gettytab
describes profiles for
.Command getty .
You probably don't need it; check the man
page if you're interested.
.H3 "/etc/manpath.config"
.File /etc/manpath.config
is a configuration file for
.Command man .
You don't usually need to change this
file.
.H3 "/etc/netconfig"
.File /etc/netconfig
is new in FreeBSD Release 5.  It is similar to the file of the same name in UNIX
System V, but it's only used for C library RPC code.  In general, you don't need
to worry about this file unless you're upgrading from an older release of
FreeBSD.  If it's not here, a number of network functions, including NFS, will
not work.
.H3 "/etc/networks"
.File /etc/networks
was once a list of networks in the Internet.  Although this sounds like a good
idea, it is almost useless: if you connect to the Internet, you should use a
name server, which supplants this file.
.H3 "/etc/passwd"
.File /etc/passwd
is the old-style password file.  It is now present only for programs that expect
to read user information from it, and it no longer contains passwords.  Don't
change this file; the programs
.Command vipw ,
.Command adduser
and
.Command pwd_mkdb
do it automatically.  See page
.Sref \*[master-passwd] \&
for more details.
.H3 "/etc/protocols"
.File /etc/protocols
is a list of known protocols that run on the IP layer.  Consider this file to
be read-only.
.H3 "/etc/pwd.db"
.File /etc/pwd.db
is a machine-readable form of the user database with the passwords removed.  We
looked at it on page
.Sref \*[master-passwd] .
Like
.File /etc/passwd ,
it is generated automatically.
.H3 "/etc/rc"
.File /etc/rc
is the main script that starts up the system.  It uses the other files whose
names start with
.File /etc/rc
to perform specific initialization.  See page
.Sref \*[init] \&
for more details.
.H3 "/etc/rc.i386"
.File /etc/rc.i386
is used to initialize features specific to the Intel 386 architecture, such as
SCO and Linux emulation.  You don't normally need to look at or change this
file.
.H3 "/etc/rc.network and /etc/rc.network6"
The main scripts for starting the network are
.File /etc/rc.network ,
which in earlier FreeBSD releases was called
.File /etc/network ,
and
.File /etc/rc.network6 ,
which starts IPv6 services.  You normally don't change these files: they read
all the necessary definitions from
.File /etc/rc.conf ,
and that's the file you should change.
.H3 "/etc/rc.pccard"
.File /etc/rc.pccard
sets up laptops using the PC Card bus.
.H3 "/etc/rc.serial"
.File /etc/rc.serial
sets default values for serial devices.
.H3 "/etc/shells"
.File /etc/shells
is a list of valid shells, used by
.Command ftp
and some other programs.
.Command ftpd
refuses to open a session for a user whose shell is not mentioned in this file.
This prevents people from starting an
.Command ftp
session as a daemon, which frequently have no passwords.
.Command chpass
will not let you change your shell to a shell not included in this file.  See
page
.Sref \*[/etc/shells] \&
for more details.  It is usually updated when you install a
new shell from the Ports Collection.
.H3 "/etc/services"
.File /etc/services
contains a list of the IP services that this system supports.
.H3 "/etc/spwd.db"
.File /etc/spwd.db
is a machine-readable form of the user database with the passwords intact.  We
looked at it on page
.Sref \*[master-passwd] .
.H3 "/etc/termcap"
.X "terminal capability"
.X "capability, terminal"
.File /etc/termcap
(\fIterminal capabilities\fP\/) describes terminal control sequences.  By
default, programs use the value of the \f(CWTERM\fP environment variable to look
up the terminal capabilities in this database.  See page
.Sref \*[environment-variables] \&
for more details.
.H3 "/etc/periodic"
The directory
.Directory /etc/periodic
contains three directories used by
.Daemon cron
at regular intervals: \fIdaily\fP\/, \fIweekly\fP\/ and \fImonthly\fP.  The
directories contain a number of files for performing specific tasks.  For
example,
.File /etc/periodic/daily
contains the following files:
.Dx
  -rwxr-xr-x  5 grog  lemis  1269 Apr 26  2001 100.clean-disks
  -rwxr-xr-x  4 grog  lemis  1449 Nov 21 13:55 110.clean-tmps
  -rwxr-xr-x  5 grog  lemis  1092 Sep 15  2000 120.clean-preserve
  -rwxr-xr-x  5 grog  lemis   695 Sep 15  2000 130.clean-msgs
  -rwxr-xr-x  5 grog  lemis  1056 Sep 15  2000 140.clean-rwho
  -rwxr-xr-x  1 grog  lemis   595 Jan  9 07:11 150.clean-hoststat
  -rwxr-xr-x  5 grog  lemis  1742 Nov 15  2001 200.backup-passwd
  -rwxr-xr-x  5 grog  lemis   996 Sep 15  2000 210.backup-aliases
  -rwxr-xr-x  5 grog  lemis   679 Sep 15  2000 300.calendar
  -rwxr-xr-x  5 grog  lemis  1211 May 31  2001 310.accounting
  -rwxr-xr-x  5 grog  lemis   710 Sep 15  2000 330.news
  -rwxr-xr-x  5 grog  lemis   516 Jul 26  2002 400.status-disks
  -rwxr-xr-x  5 grog  lemis   548 Sep 15  2000 420.status-network
  -rwxr-xr-x  5 grog  lemis   687 Sep 15  2000 430.status-rwho
  -rwxr-xr-x  3 grog  lemis  1362 Dec  9 07:15 440.status-mailq
  -rwxr-xr-x  5 grog  lemis   768 Jul 26  2002 450.status-security
  -rwxr-xr-x  3 grog  lemis  1633 Dec  9 07:15 460.status-mail-rejects
  -rwxr-xr-x  1 grog  lemis  1489 Jan  7 07:10 470.status-named
  -rwxr-xr-x  5 grog  lemis   723 Jul 26  2002 500.queuerun
  -rwxr-xr-x  5 grog  lemis   712 Jun  2  2001 999.local
.De
The files are executed in the order of their names, so the names consist of two
parts: a number indicating the sequence, and a name indicating the function.
This method is new with FreeBSD Release 3.  In older releases of FreeBSD, these
functions were performed by files with the names
.File /etc/daily ,
.File /etc/weekly
and
.File /etc/monthly .
See page
.Sref \*[cron] \&
for more details of
.Daemon cron .
.H2 "Obsolete configuration files"
In the course of time, a number of configuration files have come and gone.  This
can be tricky if you're updating a system: some old configuration files could
remain and either confuse you by not working the way you expect, or cause
incorrect operation by some side effect of the presence of the file.
.H3 "/etc/host.conf"
.File /etc/host.conf
described the order in which to perform name resolution.  It has been replaced
by
.File /etc/nsswitch.conf ,
which has a different syntax.
.H3 "/etc/named.boot"
Previous versions of
.Daemon named ,
the DNS daemon, used
.File /etc/named.boot
as the main configuration file.  Newer versions use
.File /etc/namedb/named.conf ,
and the format is very different.
.H3 "/etc/netstart"
.File /etc/netstart
was a script called by
.File /etc/rc
to start up the network.  Its name has now been changed to
.File /etc/rc.network .
FreeBSD still includes a file
.File /etc/netstart ,
but its only purpose is to start the network in single-user mode.
.H3 "/etc/sysconfig"
.File /etc/sysconfig
was a file that contained all the site-specific configuration definitions.  Its
name has been changed to
.File /etc/rc.conf .
