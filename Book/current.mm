.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: current.mm,v 4.18 2003/06/29 04:29:20 grog Exp grog $
.\"
.Chapter \*[nchcurrent] "Keeping up to date"
FreeBSD is constantly changing.  The average time that elapses between changes
to the source tree is in the order of a few minutes.  Obviously you can't keep
up to date with that pace of change.
.P
In the following three chapters we'll look at how to keep up to date.  In this
chapter, we'll look at:
.Ls B
.LI
FreeBSD releases: how the FreeBSD project comes to terms with the rapid rate of
change, and how it keeps the system stable despite the changes.
.LI
How the system sources are stored, and how you can update them.
.Le
In
.Sref "\*[chupgrading]" ,
we'll look at how to upgrade FreeBSD to a new release, with particular reference
to upgrades to FreeBSD Release 5, and in
.Sref "\*[chbuild]" ,
we'll look at building special kernels.
.SPUP
.H2 "FreeBSD releases and CVS"
.Pn repository
.X "repository, CVS"
.X "CVS, repository"
.X "Concurrent Versions System"
.X "CVS"
.X "branch, CVS"
.X "CVS, branch"
.X "branch, CVS"
.X "FreeBSD, releases"
.X "Revision Control System"
.X "RCS"
.X "trunk, CVS"
.X "CVS, trunk"
The FreeBSD project keeps the entire operating system sources in a single master
source tree, called a \fIrepository\fP, which is maintained by the \fIConcurrent
Versions System\fP, or \fICVS\fP.  It's included in most multi\(enCD-ROM
distributions of FreeBSD.  The repository contains all versions of FreeBSD back
to Release 2.0 and the last release from the Computer Sciences Research Group of
the University of California at Berkeley, 4.4BSD-Lite, upon which it was based.
For copyright reasons FreeBSD Release 1 was not included, because at the time,
as the result of the lawsuits described on page
.Sref \*[unixwars] ,
it was not permitted to distribute it freely.  That situation changed in early
2002, but it's now too late to include FreeBSD Release 1 in the repository.
.P
\fICVS\fP\/ is built on top of the \fIRevision Control System\fP, or \fIRCS\fP.
\fIRCS\fP\/ keeps multiple versions of files, called \fIrevisions\fP, in a
single RCS file.  Each revision has a number indicating its relationship to the
other revisions.  The oldest revision has the number 1.1, the next oldest has
the number 1.2, and so on.  The RCS file contains the most recent revision of
the file along with instructions for creating any other revision.
.P
In addition to this linear sequence, it's possible to update a specific revision
in more than one way.  The obvious way to update revision 1.2 would create
revision 1.3; but it's also possible to create \fIbranches\fP, which get numbers
like 1.2.1.1.  Updating revision 1.2.1.1 would create revision 1.2.1.2, and so
on.  By contrast, the revisions with a two-part number are collectively called
the \fItrunk\fP\/ of the tree.
.H3 "Symbolic names or tags"
.X "symbolic names, CVS"
.X "tag, CVS"
.X "CVS, symbolic names"
.X "CVS, tag"
In addition to the numeric identifiers, each of which relates only to a single
file, RCS allows you to attach \fIsymbolic names\fP\/ to specific revisions.
CVS generally calls these names \fItags\fP, and that's the term you'll see most
often.  FreeBSD uses tags to indicate the revisions corresponding to a
particular release.  For example, in the directory
.Directory /usr/src/sys/kern ,
revision 1.13 of
.File kern_clock.c ,
revision 1.12 of \fIkern_fork.c\fP\/ and revision 1.21.4.1 of \fIkern_exec.c\fP\/
participate in \f(CWRELENG_2_1_0_RELEASE\fP.  We'll look at tags in more detail
on page
.Sref \*[release-tags] .
.P
\fIRCS\fP\/ stores its files either in the same directory as the working files
it is tracking, or in a subdirectory
.Directory RCS
if it exists.  To avoid file name conflicts, \fIRCS\fP\/ appends the characters
\fI,v\fP\/ to the RCS file, so the working file
.File -n main.c \&
would correspond to the RCS file
.File -n main.c,v .
For more details of \fIRCS\fP, see the man page.
.P
\fICVS\fP\/ is an extension to \fIRCS\fP\/ that allows concurrent access,
making it more suitable for projects involving more than one person.  Unlike
\fIRCS\fP, it stores its RCS files in a separate directory hierarchy, called a
\fIrepository\fP.  Each directory in the working tree contains a subdirectory
.Directory CVS
with information on the location of the repository, the revisions of the working
files and a tag if the revision isn't on the trunk.
.P
If you're a serious developer, there are a number of advantages to keeping a
copy of the repository.  If you're a casual user, it's probably overkill.
.H2 "FreeBSD releases"
.Pn release-names
There are four main versions of FreeBSD, each intended for use by different
people:
.H3 "FreeBSD-RELEASE"
.X "FreeBSD-RELEASE"
\fIFreeBSD-RELEASE\fP\/ is the latest version of FreeBSD that has been released
for general use.  It contains those new features that are stable, and it has
been through extensive testing.  You can get it on CD-ROM.  FreeBSD-RELEASEs are
given a release number that uniquely identifies them, such as 5.0.  There are
three or four releases a year.  A new branch is made for each release of
FreeBSD.
.H3 "FreeBSD-STABLE"
.X "FreeBSD-STABLE"
\fIFreeBSD-STABLE\fP\/ is an updated version of FreeBSD-RELEASE to which all
possible bug fixes have been applied, to make it as stable as possible.  Fixes
are made on a daily basis.  It is based on the same source branch as
FreeBSD-RELEASE, so it has all the features and fewer bugs.  It may contain
additional features, but new features are tested in the \f(CW-CURRENT\fP branch
first.
.P
Due to the frequent updates, FreeBSD-STABLE is not available on CD-ROM.
.H3 "Security fix releases"
Despite the name, FreeBSD-STABLE is subject to some problems.  Every change to a
source tree has the potential to go wrong.  In many cases, you're more
interested in keeping your system running than you are in getting minor bug
fixes.  FreeBSD also maintains a second ``stable'' branch consisting of the
release and only very important bug fixes, including security updates.  This
branch does not have a well-defined name, but it's generally referred to as the
\fIsecurity branch\fP\/.
.H3 "FreeBSD-CURRENT"
.X "FreeBSD-CURRENT"
.X "/usr/src/Makefile"
\fIFreeBSD-CURRENT\fP\/ is the very latest version of FreeBSD, located on the
trunk of the tree.  All new development work is done on this branch of the tree.
FreeBSD-CURRENT is an ever-changing snapshot of the working sources for FreeBSD,
including work in progress, experimental changes and transitional mechanisms
that may or may not be present in the next official release of the software.
Many users compile almost daily from FreeBSD-CURRENT sources, but there are
times when the sources are uncompilable, or when the system crashes frequently.
The problems are always resolved, but others can take their place.  On occasion,
keeping up with FreeBSD-CURRENT can be a full-time business.  If you use
\f(CW-CURRENT\fP, you should be prepared to spend a lot of time keeping the
system running.  The following extract from the RCS log file for
.File /usr/src/Makefile
should give you a feel for the situation:
.Pn hooboy .
.Dx
$ \f(CBcvs log Makefile\fP
\&\fI...\fP\/
revision 1.152
date: 1997/10/06 09:58:11;  author: jkh;  state: Exp;  lines: +41 -13
Hooboy!

Did I ever spam this file good with that last commit.  Despite 3
reviewers, we still managed to revoke the eBones fixes, TCL 8.0 support,
libvgl and a host of other new things from this file in the process of
parallelizing the Makefile.  DOH!   I think we need more pointy hats - this
particular incident is worthy of a small children's birthday party's worth of
pointy hats. ;-)

I certainly intend to take more care with the processing of aged diffs
in the future, even if it does mean reading through 20K's worth of them.
I might also be a bit more anal about asking for more up-to-date changes
before looking at them. ;)
.De
This example also shows the list of the symbolic names for this file, and their
corresponding revision numbers.  There is no symbolic name for \f(CW-CURRENT\fP,
because it is located on the trunk.  That's the purpose of the line
\f(CWhead:\fP, which shows that at the time of this example, the
\f(CW-CURRENT\fP revision of this file was 1.270.
.P
So why use \f(CW-CURRENT\fP?  The main reasons are:
.Ls B
.LI
You might be doing development work on some part of the source tree.  Keeping
``current'' is an absolute requirement.
.LI
You may be an active tester, which implies that you're willing to spend time
working through problems to ensure that FreeBSD-CURRENT remains as sane as
possible.  You may also wish to make topical suggestions on changes and the
general direction of FreeBSD.
.LI
You may just want to keep an eye on things and use the current sources for
reference purposes.
.Le
People occasionally have other reasons for wanting to use FreeBSD-CURRENT.  The
following are \fInot\fP\/ good reasons:
.Ls B
.LI
They see it as a way to be the first on the block with great new FreeBSD
features.  This is not a good reason, because there's no reason to believe that
the features will stay, and there is good reason to believe that they will be
unstable.
.LI
They see it as a quick way of getting bug fixes.  In fact, it's a way of
\fItesting\fP\/ bug fixes.  Bug fixes will be retrofitted into the -STABLE
branch as soon as they have been properly tested.
.LI
They see it as the newest officially supported release of FreeBSD.  This is
incorrect: FreeBSD-CURRENT is \fInot\fP\/ officially supported.  The support is
provided by the users.
.Le
If you do decide to use \f(CW-CURRENT\fP, read the suggestions on page
.Sref \*[current] .
.H4 "Snapshots"
.X "snapshot"
FreeBSD-CURRENT \fIis\fP\/ available in the form of ISO (CD-ROM) images.  From
time to time, at irregular intervals when the tree is relatively stable, the
release team makes a \fIsnapshot\fP\/ release from the \f(CW-CURRENT\fP source
tree.  They are also available on CD-ROM from some vendors; check the online
handbook for details.  This is a possible alternative to online updates if you
don't want the absolute latest version of the system.
.H2 "Getting updates from the Net"
There are a number of possibilities to keep up with the daily modifications to
the source tree.  The first question is: how much space do you want to invest in
keeping the sources?  Table \*[source-tree-space]
.\" XXX.pageref \*[source-tree-space-page] "on page \*[source-tree-space-page]"
shows the approximate space required by different parts of the sources.  Note
that the repository keeps growing faster than the source tree, because it
includes all old revisions as well.
.br
.Table-heading "Approximate source tree sizes"
.P
.fi
.TS
tab(#) ;
 l | r  .
Component#Size (MB)
_
Repository \fIsrc/sys\fP\/#250
Repository \fIsrc\fP\/#1000
Repository \fIports\fP\/#300
Source tree \fI/usr/src/sys\fP\/#110
Source tree \fI/usr/src\fP\/#450
Source tree \fI/usr/ports\fP\/#200
Object tree \fI/usr/obj\fP\/#160
.TE
.Tn source-tree-space
.sp 1.5v
The size of
.Directory /usr/src/sys
includes the files involved in a single kernel build.  You can remove the entire
kernel build directory, but if you want to be able to analyze a panic dump, you
should keep the
.File kernel.debug
file in the kernel build directory.  This changes the size of
.Directory /usr/src
as well, of course.
The other object files get built in the directory
.Directory /usr/obj .
Again, you can remove this directory tree entirely if you want, either with the
.Command rm
command or with \f(CWmake clean\fP.  Similarly, the size of
.Directory /usr/ports
includes a few ports.  It will, of course, grow extremely large (many gigabytes)
if you start porting all available packages.
.P
If you're maintaining multiple source trees (say, for different versions), you
still only need one copy of the repository.
.P
.H3 "CVSup"
.Pn CVSup
.X "cvsup, command"
\fICVSup\fP\/ is a software package that distributes updates to the repository.
You can run the client at regular intervals\(emfor example, with
.Daemon cron
(see page \*[cron]) to update your repository.
.P
To get started with \fICVSup\fP\/, you need the following:
.Ls B
.LI
A source tree or repository, which doesn't have to be up to date.  This is not
absolutely necessary, but the initial setup will be faster if you do it this
way.
.LI
A copy of the
.Command cvsup
program.  Install it with
.Command pkg_add
from the CD-ROM
.File -n ( /cdrom/packages/Latest/cvsup.tbz ).
.LI
.X "cvsupfile"
A \fIcvsupfile\fP, a command file for
.Command cvsup .
We'll look at this below.
.LI
.X "mirror site"
A \fImirror site\fP\/ from which you can load or update the repository.  We'll
discuss this below as well.
.Le
The \fIcvsupfile\fP\/ contains a description of the packages you want to
download.  You can find all the details in the online handbook, but the
following example shows a reasonably normal file:
.Dx
*default release=cvs
*default host=cvsup9.freebsd.org
*default base=/src/cvsup
*default prefix=/home/ncvs
*default delete
*default use-rel-suffix
*default compress
src-all
ports-all
doc-all
.De
The lines starting with \f(CW*default\fP specify default values; the lines that
do not are collections that you want to track.  This file answers these implicit
questions:
.Ls B
.LI
.X "collection, CVSup"
.X "cvsup, collection"
Which files do you want to receive?  These are the names of the
\fIcollections\fP\/ in the last three lines: all of the sources, ports and
documentation.
.LI
Which versions of them do you want?  By default, you get updates to the
repository.  If you want a specific version, you can write:
.Dx
*default tag=\fIversion\fP\/
.De
\fIversion\fP\/ is a \fIrelease tag\fP\/ that identifies the version you want,
or \f(CW.\fP (a period) to represent the \f(CW-CURRENT\fP version.  We'll
discuss release tags on page
.Sref \*[release-tags] .
.P
Alternatively, you might ask for a version as of a specific date.  For example:
.Dx
*default date=97.09.13.12.20
.De
This would specify that you want \fIthe version\fP\/ as it was on 13 September
1997 at 12:20.  In this case, \fIversion\fP\/ defaults to \f(CW.\fP (a period).
.LI
Where do you want to get them from?  Two parameters answer this question:
\f(CWhost=cvsup9.freebsd.org\fP specifies the name of the host from which to
load the files, and \f(CWrelease=cvs\fP specifies to use the \fIcvs\fP\/
release.  The \fIrelease\fP\/ option is obsolescent, but it's a good idea to
leave it in there until it is officially removed.
.LI
Where do you want to put them on your own machine?  This question is answered by
the line \f(CW*default prefix=/home/ncvs\fP.  We're tracking the repository in
this example, so this is the name of the repository.  If we were tracking a
particular release, we would use \f(CW*default prefix=/usr\fP.  The collections
are called \fIdoc\fP, \fIports\fP\/ and \fIsrc\fP, so we refer to the parent
directory in each case.
.LI
Where do you want to put your status files?  This question is answered by the
line \f(CW*default base=/src/cvsup\fP.
.Le
In addition, the file contains three other lines.  \f(CW*default delete\fP means
that
.Command cvsup
may delete files where necessary.  Otherwise you run the risk of accumulating
obsolete files.  \f(CW*default compress\fP enables compression of the data
transmitted, and \f(CW*default use-rel-suffix\fP specifies how
.Command cvsup
should handle list files.  It's not well-documented, but it's necessary.  Don't
worry about it.
.H3 "Which CVSup server?"
.X "which cvsup server?"
.X "cvsup, which server?"
In this example, we've chosen one of the backup US servers,
\fIcvsup9.FreeBSD.org\fP.  In practice, this may not be the best choice.  A
large number of servers are spread around the world, and you should choose the
one topographically closest to you.  This isn't the same thing as being
geographically closest\(emI live in Adelaide, South Australia, and some ISPs in
the same city are further away on the Net than many systems in California.  Look
on the web site
.URI http://www.FreeBSD.org
for an up-to-date list.
.H3 "Running \fIcvsup\fP\/"
.X "cvsup, running"
.X "cvsupfile, file"
.X "file, cvsupfile"
.Command cvsup
is a typical candidate for a \fIcron\fP\/ job.  I rebuild the \f(CW-CURRENT\fP
tree every morning at 3 am.  To do so, I have the following entry in
.File /root/crontab \/:
.Dx
# Get the latest and greatest FreeBSD stuff.
0 3 * * * ./extract-updates
.De
.ne 6v
The file
.File /root/extract-updates
contains, amongst other things,
.Dx
cvsup -g -L2 /src/cvsup/cvs-cvsupfile
.De
.File -n /src/cvsup/cvs-cvsupfile
is the name of the \fIcvsupfile\fP\/ we looked at above.  The other parameters
to
.Command cvsup
specify \fInot\fP\/ to use the GUI (\f(CW-g\fP), and \f(CW-L2\fP specifies to
produce moderate detail about the actions being performed.
.H3 "Getting individual releases"
The example \fIcvsupfile\fP\/ above is useful if you're maintaining a copy of
the repository.  If you just want to maintain a copy of the sources of one
version, say Release 5.0, use the following file instead:
.Dx
*default tag=RELENG_5_0_0_RELEASE
*default release=cvs
*default host=cvsup9.freebsd.org
*default base=/usr                              \fIfor /usr/doc, /usr/ports, /usr/src\fP\/
*default prefix=/home/ncvs
*default delete
*default use-rel-suffix
*default compress
src-all
.De
Be careful with tags.  They must exist in the repository, or
.Command cvsup
will replace what you have with nothing: it will delete all the files.  In our
original
.File -n cvsupfile ,
we had two additional sets, \f(CWports-all\fP and \fIdoc-all\fP.  These sets
don't have the same release tags, so if you left them in this file, you would
lose all the files in the
.Directory /usr/doc
and
.Directory /usr/ports
directory hierarchies.
.H2 "Creating the source tree"
.X "creating the source tree"
.X "source tree, creating"
If you're tracking the repository, you're not finished yet.  Once you have an
up-to-date repository, the next step is to create a source tree.  By default,
the source tree is called
.Directory /usr/src ,
though it's very common for
.Directory /usr/src
to be a symbolic link to a source tree on a different file system.  You create
the tree with
.Command cvs .
.P
.ne 5v
Before you check anything out with
.Command cvs ,
you need to know:
.Ls N
.LI
What do you want to check out?  You specify this with a module name, which
usually corresponds with a directory name (for example, \fIsrc\fP\/).  There are
a number of top-level modules, including \fIdoc\fP, \fIports\fP, \fIsrc\fP\/ and
\fIwww\fP.
.LI
Which version do you want to check out?  By default, you get the latest version,
which is FreeBSD-CURRENT.  If you want a different version, you need to specify
its \fItag\fP.
.LI
Possibly, the date of the last update that you want to be included in the
checkout.  If you specify this date,
.Command cvs
ignores any more recent updates.  This option is often useful when somebody
discovers a recently introduced bug in \f(CW-CURRENT\fP: you check out the
modules as they were before the bug was introduced.  You specify the date with
the \f(CW-D\fP option, as we'll see below.
.Le
.SPUP
.H3 "Release tags"
.Pn release-tags
.X "CVS, tag"
.X "tag, CVS"
FreeBSD identifies releases with two or more numbers separated by periods.  Each
number represents a progressively smaller increment in the functionality of the
release.  The first number is the base release of FreeBSD.  The number is
incremented only when significant functionality is added to the system.  The
second number represents a less significant, but still important difference in
the functionality, and the third number is only used when a significant bug
requires rerelease of an otherwise unchanged release.  Before Release 3 of
FreeBSD, a fourth number was sometimes also used.
.P
Tags for released versions of FreeBSD follow the release numbers.  For release
\fIx\fP.\fIy\fP.\fIz\fP\/ you would look for the tag
\f(CWRELENG_\|\fIx\fP\/_\|\fIy\fP\/_\|\fIz\fP\/_RELEASE\fR.  For example, to get
the current state of the FreeBSD 5.0 source tree, you would look for the tag
\f(CWRELENG_5_0_0_RELEASE\fP.
.P
Tags for the -STABLE branch are simpler: they just have the release number, for
example \f(CWRELENG_4\fP.  The security branch has an additional number, for
example \f(CWRELENG_4_7\fP.
.P
Some tags diverge from this scheme.  In particular, \f(CWCSRG\fP and
\f(CWbsd_44_lite\fP both refer to the original 4.4BSD sources from Berkeley.  If
you feel like it, you can extract this source tree as well.
.P
.ne 5v
To find out what tags are available, do:
.Dx
# \f(CBcd $CVSROOT/src\fP
# \f(CBrlog Makefile,v | less\fP
RCS file: /home/ncvs/src/Makefile,v
RCS file: /home/ncvs/src/Makefile,v
Working file: Makefile
head: 1.270
branch:
locks: strict
access list:
symbolic names:
        RELENG_5_0_0_RELEASE: 1.271             \fI5.0-RELEASE\fP\/
\&...
        RELENG_4_7_0_RELEASE: 1.234.2.18        \fI4.7-RELEASE\fP\/
        RELENG_4_7: 1.234.2.18.0.2              \fI4.7 security fixes only\fP\/
        RELENG_4_7_BP: 1.234.2.18               \fIbranch point for 4.7\fP\/
        RELENG_4_6_2_RELEASE: 1.234.2.12        \fI4.6.2-RELEASE\fP\/
        RELENG_4_6_1_RELEASE: 1.234.2.12        \fI4.6.1-RELEASE\fP\/
        RELENG_4_6_0_RELEASE: 1.234.2.12        \fI4.6-RELEASE\fP\/
\&...
        RELENG_4: 1.234.0.2                     \fI4-STABLE\fP\/
\&...
        RELEASE_2_0: 1.30                       \fI2.0-RELEASE\fP\/
        BETA_2_0: 1.30
        ALPHA_2_0: 1.29.0.2
        bsd_44_lite: 1.1.1.1                    \fI4.4BSD-Lite\fP\/
        CSRG: 1.1.1
keyword substitution: kv
total revisions: 179;   selected revisions: 179
description:
.De
This example shows the same file we saw on page
.Sref \*[hooboy] .
This time we use the
.Command rlog
command, which is part of \fIRCS\fP\/, to look at the revision log.  Normally
you'd use
.Command "cvs log" ,
but that only works in a checked out source tree.
.P
There are a number of ways to tell
.Command cvs
the name of its repository:  if you already have a
.Directory CVS
subdirectory, it will contain files
.File Root
and
.File Repository .
The name of the repository is in
.File Root ,
not in
.File Repository .
When you first check out files, you won't have this directory, so you specify
it, either with the \f(CW-d\fP option to
.Command cvs
or by setting the \f(CWCVSROOT\fP environment variable.  As you can see in the
example above, it's convenient to set the environment variable, since you can
use it for other purposes as well.
.P
The repository contains a number of directories, usually one for each collection
you track.  In our case, we're tracking the source tree and the Ports
Collection, so:
.Ls B
.LI
\fICVSROOT\fP\/ contains files used by CVS.  It is not part of the source tree.
.LI
\fIports\fP\/ contains the Ports Collection.
.LI
\fIsrc\fP\/ contains the system sources.
.Le
The directories \fIports\fP\/ and \fIsrc\fP\/ correspond to the directories
.Directory /usr/ports
and
.Directory /usr/src
for a particular release.  To extract the \fIsrc\fP\/ tree of the most
up-to-date version of FreeBSD-CURRENT, do the following:
.Dx
# \f(CBcd /usr\fP
# \f(CBcvs co src 2>&1 | tee /var/tmp/co.log\fP
.De
.ne 5v
To check out any other version, say, everything for Release 4.6, you would
enter:
.Dx
# \f(CBcd /usr\fP
# \f(CBcvs co -r RELENG_4_6_RELEASE src 2>&1 | tee /var/tmp/co.log\fP
.De
If you need to check out an older version, for example if there are problems
with the most recent version of \f(CW-CURRENT\fP, you could enter:
.Dx 1
# \f(CBcvs co  -D "10 December 2002" src/sys\fP
.De
This command checks out the kernel sources as of 10 December 2002.  During
checkout,
.Command cvs
creates a subdirectory \fICVS\fP\/ in each directory.  \fICVS\fP\/ contains four
files.  We'll look at typical values when checking out the version of the
directory
.Directory -n /usr/src/usr.bin/du
for Release 4.6, from the repository at
.Directory /home/ncvs \/:
.Ls B
.LI
.X "CVS, file, Entries"
\fIEntries\fP\/ contains a list of the files being maintained in the parent
directory, along with their current versions.  In our example, it would contain:
.Dx
/Makefile/1.4.2.1/Sun Jul  2 10:45:29 2000//TRELENG_4_6_0_RELEASE
/du.1/1.15.2.7/Thu Aug 16 13:16:47 2001//TRELENG_4_6_0_RELEASE
/du.c/1.17.2.3/Thu Jul 12 08:46:53 2001//TRELENG_4_6_0_RELEASE
D
.De
Note that \fIcvs\fP\/ prepends a \f(CWT\fP to the version name.
.LI
.X "CVS, file, Repository"
\fIRepository\fP\/ contains the name of the directory in the repository that
corresponds to the current directory.  This corresponds to
\fI$CVSROOT/directory\fP.  In our example, it would contain
\f(CWsrc/usr.bin/du\fP.
.LI
.X "CVS, file, Root"
\fIRoot\fP\/ contains the name of the root of the repository.  In our example,
it would contain \f(CW/home/ncvs\fP.
.LI
.X "CVS, file, Tag"
\fITag\fP\/ contains the \fIversion tag\fP\/ of the source tree.  This is the
RCS tag prefixed by a \f(CWT\fP.  In this case, it is
\f(CWTRELENG_4_6_0_RELEASE\fP.
.Le
\fIcvs co\fP\/ produces a lot of output\(emat least one line for each directory,
and one line for each file it checks out.  Here's part of a typical output:
.Dx
U src/usr.sbin/mrouted/rsrr_var.h
U src/usr.sbin/mrouted/vif.c
U src/usr.sbin/mrouted/vif.h
cvs checkout: Updating src/usr.sbin/mrouted/common
U src/usr.sbin/mrouted/common/Makefile
cvs checkout: Updating src/usr.sbin/mrouted/map-mbone
U src/usr.sbin/mrouted/map-mbone/Makefile
cvs checkout: Updating src/usr.sbin/mrouted/mrinfo
U src/usr.sbin/mrouted/mrinfo/Makefile
cvs checkout: Updating src/usr.sbin/mrouted/mrouted
U src/usr.sbin/mrouted/mrouted/Makefile
cvs checkout: Updating src/usr.sbin/mrouted/mtrace
U src/usr.sbin/mrouted/mtrace/Makefile
cvs checkout: Updating src/usr.sbin/mrouted/testrsrr
U src/usr.sbin/mrouted/testrsrr/Makefile
.De
.X "CVS, conflict"
.X "conflicts, CVS"
.ne 5v
The flag at the beginning of the line indicates what action
.Command cvs
took for the file.  The meanings are:
.Ls B
.LI
\f(CWU\fP means that
.Command cvs
updated this file.  Either it didn't exist previously, or it was an older
version.
.LI
You won't normally see \f(CWP\fP on a local update.  It implies that
.Command cvs
patched the file to update it.  Otherwise it has the same meaning as \f(CWU\fP.
.LI
\f(CW?\fP means that
.Command cvs
found the file in the directory, but it doesn't exist in the repository.
.LI
\f(CWM\fP means that
.Command cvs
found that the file in your working directory has been modified since checkout,
but it either didn't need to change it, or it was able to apply the changes
cleanly.
.LI
\f(CWC\fP found that the file in your working directory has been modified since
checkout, and it needed to change it, but it was not able to apply the changes
cleanly.  You will have to resolve the conflicts manually.
.Le
After checkout, check the log file for conflicts.  For each conflict, you must
check the files manually and possibly recover the contents.  See the man page
\fIcvs(1)\fP\/ for more details.
.H3 "Updating an existing tree"
.X "updating an existing source tree"
.X "source tree, updating an existing"
Once you have checked out a tree, the ground rules change a little.  Next time
you do a checkout, files may also need to be deleted.  Apart from that, there
isn't much difference between checkout and updating.  To update the
.Directory /usr/src
directory after updating the repository, do:
.Dx
# \f(CBcd /usr/src\fP
# \f(CBcvs update -Pd\fP
.De
Note that this time we can start in
.Directory /usr/src \/:
we now have the \fICVS/\fP\/ subdirectories in place, so \fIcvs\fP\/ knows what
to do without being given any more information.
.H3 "Using a remote CVS tree"
A CVS tree takes up a lot of space, and it's getting bigger all the time.  If
you don't check out very often, you may find it easier to use \fIanonymous
CVS\fP, where the tree is on a different system.  FreeBSD provides the server
\fIanoncvs.FreeBSD.org\fP\/ for this purpose.
.P
For example, to check out the \f(CW-CURRENT\fP source tree, perform the
following steps:
.nr firstpage 2
.Dx
$ \f(CBcd /usr\fP                                                   \fIgo to the parent directory\fP\/
$ \f(CBCVSROOT=:pserver:anoncvs@anoncvs.FreeBSD.org:/home/ncvs\fP   \fIset the server path\fP\/
$ \f(CBcvs login\fP                                                 \fIYou only need to do this once\f(CW
Logging in to :pserver:anoncvs@anoncvs.freebsd.org:2401/home/ncvs
CVS password:                                               \fIenter \f(CBanoncvs\fI; it doesn't echo\f(CW
$ \f(CBcvs co src\fP
cvs server: Updating src
U src/COPYRIGHT
U src/Makefile
U src/Makefile.inc1
\&\fI(etc)\fP\/
.De
.bp
\&
