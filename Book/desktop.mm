.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: draft 4th edition
.\" $Id: desktop.mm,v 4.6 2003/01/04 05:58:26 grog Exp $
.\"
.\" XXX multiple X servers per machine.
.H2 "Overview of KDE"
A typical 
.Command kde ,
display looks like this:
.PIC images/kde-main.ps 5i
.Figure-heading "KDE display"
There are a number of things to look at on this display:
.Ls B
.LI
At the top right is a web browser,
.Command konqueror ,
displaying the FreeBSD home page.
.LI
Below the
.Command Konqueror
window is part of an
.Command Emacs
window, almost completely obscured.
.LI
To the left you can see part of a terminal window.
.LI
At the top left are two icons: a \fITrash Can\fP, in fact a folder where files
are placed for deletion, and one marked \f(CWHome\fP.  We'll look at the
\f(CWHome\fP icon below. XXX
.LI
At the bottom of the display is the \fItask bar\fP, which we'll also look at
below.
.Le



.H2 "The shell"
One of the big differences between X and Microsoft environments is that X has
not gone overboard and eliminated typing.  You will find that you still spend a
considerable amount of time with the old-style shell.  You don't have to: UNIX
always gives you the choice, but most users find that once you have relatively
complicated things to to, it is easier to tell the system what they want rather
than to wade through lots of menus.
.P
When you start up an \fIxterm\fP, you will automatically have a shell running.
We looked at the
.Command bash
shell in \*[chunixref], page \*[shell].
.H2 "More than one X server"
.Pn multihead
XXX
