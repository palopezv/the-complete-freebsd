.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: disks.mm,v 4.19 2003/06/29 02:54:00 grog Exp grog $
.\"
.\" XXX Add section on snapshots
.Chapter \*[nchdisks] "Disks"
One of the most important parts of running any computer system is handling data
on disk.  We have already looked at UNIX file handling in Chapter
.Sref "\*[nchfilesys]" .
In this chapter, we'll look at two ways to add another disk to your system, and
what you should put on them.  In addition, we'll discuss disk error recovery on
page
.Sref \*[disk-error-recovery] .
.H2 "Adding a hard disk"
.X "adding hard disk"
.X "disk, adding"
.Pn second-disk
When you installed FreeBSD, you created file systems on at least one hard disk.
At a later point, you may want to install additional drives.  There are two ways
to do this: with
.Command sysinstall
and with the traditional UNIX command-line utilities.
.P
There was a time when it was dangerous to use
.Command sysinstall
after the system had been installed: there was a significant chance of shooting
yourself in the foot.  There's always a chance of doing something wrong when
initializing disks, but
.Command sysinstall
has become a lot better, and now it's the tool of choice.  It's good to know the
alternatives, though.  In this section we'll look at
.Command sysinstall ,
and on page
.Sref \*[the-hard-way] \&
we'll see how to do it manually if
.Command sysinstall
won't cooperate.
.P
We've been through all the details of disk layout and slices and partitions in
Chapter
.Sref "\*[nchconcepts]" ,
so I won't repeat them here.  Basically, to add a new disk to the system, you
need to:
.Ls B
.LI
Install the disk physically.  This usually involves power cycling the machine.
.LI
Barely possibly, format the disk.  Without exception, modern disks come
pre-formatted, and you only need to format a disk if it has defects or if it's
ancient.  In many cases the so-called ``format'' program doesn't really format
at all.
.LI
If you want to share with other operating systems, create a PC style
partition table on the disk.  We looked at the concepts on page
.Sref \*[building-partition-table] .
.LI
Define a FreeBSD slice (which the PC BIOS calls a ``partition'').
.LI
Define the partitions in the FreeBSD slice.
.LI
Tell the system about the file systems and where to mount them.
.LI
Create the file systems.
.Le
These are the same operations that we performed in Chapter
.Sref "\*[nchinstall]" .
.H3 "Disk hardware installation"
.X "installing, disk hardware"
.X "disks, installing hardware"
Before you can do anything with the disk, you have to install it in the system.
To do this, you must normally shut down the system and turn the power off,
though high-end SCSI enclosures allow \fIhot-swapping\fP, changing disks in a
running system.  If the disk is IDE, and you already have an IDE disk on the
controller, you need to set the second disk as ``slave'' drive.  \fIAnd\fP\/ you
may have to set the first disk as ``master'' drive: if you only have one drive,
you don't set any jumpers, but if you have two drives, some disks require you to
set jumpers on both disks.  If you don't do this, the system will appear to hang
during the power-on self test, and will finally report some kind of disk error.
.P
Adding a SCSI disk is more complicated.  You can connect up to 15 SCSI devices
to a host adapter, depending on the interface.  Many systems restrict the number
to 7 for compatibility with older SCSI interfaces.  Typically, your first SCSI
disk will have the SCSI ID 0, and the host adapter will have the SCSI ID 7.
Traditionally, the IDs 4, 5, and 6 are reserved for tape and CD-ROM drives, and
the IDs 0 to 3 are reserved for disks, though FreeBSD doesn't impose any
restrictions on what goes where.
.P
Whatever kind of disk you're adding, look at the boot messages, which you can
retrieve with the
.Command dmesg
command.  For example, if you're planning to add a SCSI device, you might see:
.Dx
sym0: <875> port 0xc400-0xc4ff mem 0xec002000-0xec002fff,0xec003000-0xec0030ff irq 10
 at device 9.0 on pci0
sym0: Symbios NVRAM, ID 7, Fast-20, SE, NO parity
sym0: open drain IRQ line driver, using on-chip SRAM
sym0: using LOAD/STORE-based firmware.
sym0: SCAN FOR LUNS disabled for targets 0.
sym1: <875> port 0xc800-0xc8ff mem 0xec001000-0xec001fff,0xec000000-0xec0000ff irq 9
at device 13.0 on pci0
sym1: No NVRAM, ID 7, Fast-20, SE, parity checking
\&\fIfurther down...\fP\/
Waiting 3 seconds for SCSI devices to settle
sa0 at sym0 bus 0 target 3 lun 0
sa0: <EXABYTE EXB-8505SMBANSH2 0793> Removable Sequential Access SCSI-2 device
sa0: 5.000MB/s transfers (5.000MHz, offset 11)
sa1 at sym0 bus 0 target 4 lun 0
sa1: <ARCHIVE Python 28849-XXX 4.CM> Removable Sequential Access SCSI-2 device
sa1: 5.000MB/s transfers (5.000MHz, offset 15)
sa2 at sym0 bus 0 target 5 lun 0
sa2: <TANDBERG TDC 3800 -03:> Removable Sequential Access SCSI-CCS device
sa2: 3.300MB/s transfers
pass4 at sym0 bus 0 target 4 lun 1
pass4: <ARCHIVE Python 28849-XXX 4.CM> Removable Changer SCSI-2 device
pass4: 5.000MB/s transfers (5.000MHz, offset 15)
cd0 at sym0 bus 0 target 6 lun 0
cd0: <NRC MBR-7 110> Removable CD-ROM SCSI-2 device
cd0: 3.300MB/s transfers
cd0: cd present [322265 x 2048 byte records]
da0 at sym1 bus 0 target 3 lun 0
da0: <SEAGATE ST15230W SUN4.2G 0738> Fixed Direct Access SCSI-2 device
da0: 20.000MB/s transfers (10.000MHz, offset 15, 16bit), Tagged Queueing Enabled
da0: 4095MB (8386733 512 byte sectors: 255H 63S/T 522C)
.De
.X "SCSI, target"
.X "target, SCSI"
.X "SCSI, logical unit"
.X "logical unit, SCSI"
.X "LUN"
This output shows two Symbios SCSI host adapters
.Device -n ( sym0 \&
and
.Device -n sym1 ),
three tape drives
.Device -n ( sa0 ,
.Device -n sa1
and
.Device -n sa2 ),
a CD-ROM drive
.Device -n ( cd0 ),
a tape changer
.Device -n ( pass4 ),
and also a disk drive
.Device -n da0
on ID 3, which is called a \fItarget\fP\/ in these messages.  The disk is
connected to the second host adapter, and the other devices are connected to the
first host adapter.
.H4 "Installing an external SCSI device"
.X "installing, external SCSI device"
.X "SCSI, installing external device"
.X "daisy chaining"
.X "SCSI, terminator"
.X "terminator, SCSI"
External SCSI devices have two cable connectors: one goes towards the host
adapter, and the other towards the next device.  The order of the devices in the
chain does not have to have anything to do with the SCSI ID.  This method is
called \fIdaisy chaining\fP.  At the end of the chain, the spare connector may
be plugged with a \fIterminator\fP, a set of resistors designed to keep noise
off the bus.  Some devices have internal terminators, however.  When installing
an external device, you will have to do one of the following:
.Ls B
.LI
If you are installing a first external device (one connected directly to the
cable connector on the backplane of the host adapter), you will have to ensure
that the device provides termination.  If you already have at least one internal
device, the host adapter will no longer be at one end of the chain, so you will
also have to stop it from providing termination.  Modern SCSI host adapters can
decide whether they need to terminate, but older host adapters have resistor
packs.  In the latter case, remove these resistor packs.
.LI
If you are adding an additional external device, you have two choices: you can
remove a cable in the middle of the daisy chain and plug it into your new
device.  You then connect a new cable from your device to the device from which
you removed the original cable.
.P
Alternatively, you can add the device at the end of the chain.  Remove the
terminator or turn off the termination, and plug your cable into the spare
socket.  Insert the terminator in your device (or turn termination on).
.Le
.X "camcontrol, command"
.X "command, camcontrol"
You can add external SCSI devices to a running system if they're hot-pluggable.
It might even work if they're not hot-pluggable, but it's not strictly the
correct thing to do, and there's the risk that you might damage something,
possibly irreparably.  After connecting the devices, powering them up and
waiting for them to come ready, run
\fIcamcontrol rescan\fP\/.  For example, if you added a second disk drive to the
second host adapter in the example above, you might see:
.Dx
# \f(CBcamcontrol rescan 1\fP
da1 at sym1 bus 0 target 0 lun 0
da1: <SEAGATE ST15230W SUN4.2G 0738> Fixed Direct Access SCSI-2 device
da1: 20.000MB/s transfers (10.000MHz, offset 15, 16bit), Tagged Queueing Enabled
da1: 4095MB (8386733 512 byte sectors: 255H 63S/T 522C)
Re-scan of bus 1 was successful
.De
There's a problem with this approach: note that
.Device -n da1
has ID 0, and the already present
.Device -n da0
has ID 3.  If you now reboot the system, they will come up with the device names
the other way round.  We'll look at this issue in more detail in the next
section.
.H4 "Installing an internal SCSI device"
.X "installing, internal SCSI device"
.X "SCSI, installing internal device"
.Pn installing-scsi
Installing an internal SCSI device is much the same as installing an external
device.  Instead of daisy chains, you have a flat band cable with a number of
connectors.  Find one that suits you, and plug it into the device.  Again, you
need to think about termination:
.Ls B
.LI
If you are installing the device at the end of the chain, it should have
termination enabled.  You should also disable termination for the device that
was previously at the end of the chain.  Depending on the device, this may
involve removing the physical terminators or setting a jumper.
.LI
If you are installing the device in the middle of the chain, make sure it does
not have termination enabled.
.Le
In this chapter, we'll look at two ways of installing a drive in an existing
SCSI chain.  We could be in for a surprise: the device ID we get for the new
drive depends on what is currently on the chain.  For example, consider our
example above, where we have a chain with a single drive on it:
.Dx
da0 at sym1 bus 0 target 3 lun 0
da0: <SEAGATE ST15230W SUN4.2G 0738> Fixed Direct Access SCSI-2 device
da0: 20.000MB/s transfers (10.000MHz, offset 15, 16bit), Tagged Queueing Enabled
da0: 4095MB (8386733 512 byte sectors: 255H 63S/T 522C)
.De
This drive on target (ID) 2.  If we put our new drive on target 0 and reboot, we
see:
.Dx
da0 at sym1 bus 0 target 0 lun 0
da0: <SEAGATE ST15230W SUN4.2G 0738> Fixed Direct Access SCSI-2 device
da0: 20.000MB/s transfers (10.000MHz, offset 15, 16bit), Tagged Queueing Enabled
da0: 4095MB (8386733 512 byte sectors: 255H 63S/T 522C)
da1 at sym1 bus 0 target 3 lun 0
da1: <SEAGATE ST15230W SUN4.2G 0738> Fixed Direct Access SCSI-2 device
da1: 20.000MB/s transfers (10.000MHz, offset 15, 16bit), Tagged Queueing Enabled
da1: 4095MB (8386733 512 byte sectors: 255H 63S/T 522C)
.De
At first glance, this looks reasonable, but that's only because both disks are
of the same type.  If you look at the target numbers, you'll notice that the new
disk is
.Device -n da0 ,
not
.Device -n da1 .
The target ID of the new disk is lower than the target ID of the old disk, so
the system recognizes the new disk as
.Device -n da0 ,
and our previous
.Device -n da0
has become
.Device -n da1 .
.P
.ne 3v
This change of disk ID can be a problem.  One of the first things you do with a
new disk is to create new disk labels and file systems.  Both offer excellent
opportunities to shoot yourself in the foot if you choose the wrong disk: the
result would almost certainly be the complete loss of data on that disk.  Even
apart from such catastrophes, you'll have to edit
.File /etc/fstab
before you can mount any file systems that are on the disk.  The alternatives
are to wire down the device names, or to change the SCSI IDs.  In FreeBSD 5.0,
you wire down device names and busses by adding entries to the boot
configuration file
.File /boot/device.hints .
We'll look at that on page
.Sref \*[device-hints] .
.H3 "Formatting the disk"
.X "formatting, disks"
.X "disks, formatting"
.X "low-level format"
.X "format, low-level"
.Pn format-disk
Formatting is the process of rewriting every sector on the disk with a specific
data pattern, one that the electronics find most difficult to reproduce: if they
can read this pattern, they can read anything.  Microsoft calls this a
\fIlow-level format\fP.\*F
.FS
Microsoft also uses the term \fIhigh-level format\fP\/ for what we call creating
a file system.
.FE
Obviously it destroys any existing data, so
.Highlight
If you have anything you want to keep, back it up before formatting.
.End-highlight
Most modern disks don't need formatting unless they're damaged.  In particular,
formatting will not help if you're having configuration problems, if you can't
get PPP to work, or you're running out of disk space.  Well, it \fIwill\fP\/
solve the disk space problem, but not in the manner you probably desire.
.P
If you do need to format a SCSI disk, use
.Command camcontrol .
.Command camcontrol
is a control program for SCSI devices, and it includes a lot of useful functions
that you can read about in the man page.  To format a disk, use the following
syntax:
.Dx
# \f(CBcamcontrol format da1\fP
.De
.SPUP
.Highlight
Remember that formatting a disk destroys all data on the disk.  Before using the
command, make sure that you need to do so: there are relatively few cases that
call for formatting a disk.  About the only reasons are if you want to change
the physical sector size of the disk, or if you are getting ``medium format
corrupted'' errors from the disk in response to read and write requests.
.End-highlight
FreeBSD can format only floppies and SCSI disks.  In general it is no longer
possible to reformat ATA (IDE) disks, though some manufacturers have programs
that can recover from some data problems.  In most cases, though, it's
sufficient to write zeros to the entire disk:
.Dx
# \f(CBdd if=/dev/zero of=/dev/ad1 bs=128k\fP
.De
If this doesn't work, you may find formatting programs on the manufacturer's web
site.  You'll probably need to run them under a Microsoft platform.
.br
.ne 15v
.H2 "Using sysinstall"
If you can, use
.Command sysinstall
to partition your disk.  Looking at the
.Command dmesg
output for our new disk, we see:
.Dx
da1 at sym1 bus 0 target 0 lun 0
da1: <SEAGATE ST15230W SUN4.2G 0738> Fixed Direct Access SCSI-2 device
da1: 20.000MB/s transfers (10.000MHz, offset 15, 16bit), Tagged Queueing Enabled
da1: 4095MB (8386733 512 byte sectors: 255H 63S/T 522C)
.De
You see the standard installation screen (see Chapter
.Sref "\*[nchinstall]" ,
page
.Sref \*[sysinstall-main] ).
Select \f(CWIndex\fP, then \f(CWPartition\fP, and you see the following screen:
.PIC  "images/disk-selection.ps" 4i
.Figure-heading "Disk selection menu"
.P
In this case, we want to partition
.Device da1 ,
so we position the cursor on \f(CWda1\fP (as shown) and press \fBEnter\fP.
.DF
.PIC "images/disk-partition.ps" 4i
.Pn disk-partition-menu
.Figure-heading "Disk partition menu"
.P
.DE
We see the disk partition menu, which shows that the disk currently contains
three partitions:
.Ls B
.LI
The first starts at offset 0, and has a length of 63.  This is \fInot\fP\/
unused, no matter what the description says.  It's the partition table, padded
to the length of a ``track.''
.LI
The next partition takes up the bulk of the drive and is a Microsoft partition.
.LI
Finally, we have 803 sectors left over as a result of the partitioning scheme.
Sometimes this can be much larger\(emI have seen values as high as 35 MB.  This
is the price we pay for compatibility with PC BIOS partitioning.
.Le
.Pn dangerously-dedicated
We want a FreeBSD partition, not a Microsoft partition.  At this point, we have
a number of choices:
.Ls B
.LI
We can change the partition type (called ``Subtype'' in the menu).  It's
currently 6, and we would need to change it to 165.  Do this with the \f(CWt\fP
command.
.LI
We could delete the partition by positioning the cursor on the partition
information and pressing \f(CWd\fP, then create a new partition, either with
\f(CWa\fP if we want a single partition, or with \f(CWc\fP if we want more than
one partition.
.LI
If we're using this disk for FreeBSD only, we don't have to waste even this much
space.  There is an option ``use whole disk for FreeBSD,'' the so-called
``dangerously dedicated'' mode.  This term comes partially from superstition and
partially because some BIOSes expect to find a partition table on the first
sector of a disk, and they can't access the disk if they don't find one.  If
your BIOS has this bug, you'll find this one out pretty quickly when you try to
boot.  If it doesn't fail on the first boot, it won't fail, though it's barely
possible that you might have trouble if you move it to a system with a different
BIOS.  If you want to use this method, use the undocumented \f(CWf\fP command.
.Le
To use the whole disk, we first delete the current partition: we press the
cursor down key until it highlights the FreeBSD partition.  Then we press
\fBd\fP, and the three partitions are joined into one, marked \f(CWunused\fP.
.P
.ne 12v
The next step is to create a new partition using the entire disk.  If we press
\f(CWf\fP, we get the following message:
.PIC "images/disk-partition-2.ps" 4i
.sp
We don't get this message if we use the \f(CWa\fP command: it just automatically
assumes \f(CWYes\fP.  In this case we've decided to use the whole disk, so we
move the cursor right to \f(CWNo\fP and press \fBEnter\fP.  That gives us a boot
manager selection screen:
.PIC "images/mbr-choice.ps" 4i
.P
.ne 12v
This isn't a boot disk, so we don't need any boot record, and it doesn't make
any difference what we select.  It's tidier, though, to select \f(CWNone\fP as
indicated.  Then we press \f(CWq\fP to exit the partition editor, get back to
the function index, and select \f(CWLabel\fP.  We see:
.PIC "images/disk-label.ps" 4i
.sp
The important information on this rather empty looking menu is the information
at the top about the free space available.  We want to create two partitions:
first, a swap partition of 512 MB, and then a file system taking up the rest of
the disk.  We press C, and are shown a submenu offering us all 8386733 blocks on
the disk.  We erase that and enter \f(CW512m\fP, which represents 512 MB.  Then
we press \fBEnter\fP, and another submenu appears, asking us what kind of slice
it is.  We move the cursor down to select \f(CWA swap partition\fP:
.\" XXX one sub menu only .PIC "images/disk-label-1.ps" 4i
.PIC "images/disk-label-2.ps" 4i
.sp
.ne 4v
Next, we press \f(CWc\fP again to create a new partition.  This time, we accept
the offer of the rest of the space on the disk, 7338157 sectors, we select
\f(CWA file system\fP, and we are presented with yet another menu asking for the
name of the file system.  We enter the name, in this case
.Directory -n /S \/:
.PIC "images/disk-label-3.ps" 4i
.sp
After pressing \fBEnter\fP, we see:
.PIC "images/disk-label-4.ps" 4i
.sp
.ne 12v
Finally, we press \f(CWW\fP to tell the disk label editor to perform the
function.  We get an additional warning screen:
.PIC "images/disk-label-5.ps" 4i
.sp
We're doing this online, so that's OK.  We select \f(CWYes\fP, and
.Command sysinstall
creates the file system and mounts both it and the swap partition.  This can
take quite a while.  Don't try to do anything with the drive until it's
finished.
.H2 "Doing it the hard way"
.Pn the-hard-way
Unfortunately, sometimes you may not be able to use the
.Command sysinstall
method.  You may not have access to
.Command sysinstall ,
or you may want to use options that
.Command sysinstall
doesn't offer.  That leaves us with the old way to add disks. The only
difference is that this time we need to use different tools.  In the following
sections, we'll look at what we have to do to install this same 4 GB Seagate
drive manually.  This time we'll change the partitioning to contain the
following partitions:
.Ls B
.LI
A Microsoft file system.
.LI
The
.Directory /newhome
file system for our FreeBSD system.
.LI
Additional swap for the FreeBSD system.
.Le
We've called this file system
.Directory /newhome
to use it as an example of moving file systems to new disks.  On page
.Sref \*[move-fs] \&
we'll see how to move the data across.
.H3 "Creating a partition table"
.X "creating a partition table"
.X "partition table, creating"
The first step is to create a PC BIOS style partition table on the disk.  As in
Microsoft, the partitioning program is called
.Command fdisk .
In the following discussion, you'll find a pocket calculator indispensable.
.P
If the disk is not brand new, it will have existing data of some kind on it.
Depending on the nature of that data,
.Command fdisk
could get sufficiently confused to not work correctly.  If you don't format the
disk, it's a good idea to overwrite the beginning of the disk with
.Command dd \/:
.Dx
# \f(CBdd if=/dev/zero of=/dev/da1 count=100\fP
100+0 records in
100+0 records out
51200 bytes transferred in 1 secs (51200 bytes/sec)
.De
We'll assign 1 GB for Microsoft and use the remaining approximately 3 GB for
FreeBSD.  Our resulting partition table should look like:
.PS
h = .2i
dh = .02i
dw = 1.7i
move right .5i
down
[
        boxht = h; boxwid = 1.7i
        box ht .15i "Master Boot Record"
        box ht .15i "Partition Table"
    P1: box ht .4i
    P2: box ht .4i
    P3: box ht .4i
    P4: box ht .4i

        move right 1.65i from P1.ne
        boxwid = 2.5i
        down
     C: box ht .25i "Microsoft primary partition"

        move .25i
    FA: box ht .2i "\fI/dev/da1s2b\fR: FreeBSD swap"
        box ht .2i "\fI/dev/da1s2h\fR\/: \fI/newhome\fP\/ file system"

        arrow from P1.e to C.w
        arrow from P2.e to FA.w
        move up .08i from P1.c  "Slice 1 - Microsoft primary"
        move down .08i from P1.c  "\fI/dev/da1s1\fP, 1 GB"
        move up .08i from P2.c  "Slice 2 - FreeBSD"
        move down .08i from P2.c  "\fI/dev/da1s2\fP, 3 GB"
        "Slice 3 (unused)" at P3.c
        "Slice 4 (unused)" at P4.c
        ]
.PE
.Figure-heading "Partition table on second FreeBSD disk"
.Fn second-disk-partitions
.ps \n(PS                       \"  point size of standard text
.vs \n(LS                       \" Standard vertical spacing
The Master Boot Record and the Partition Table take up the first sector of the
disk, but many of the allocations are track oriented, so the entire first track
of the disk is not available for allocation.  The rest, up to the end of the
last entire cylinder, can be divided between the partitions.  It's easy to make
a mistake in specifying the parameters, and
.Command fdisk
performs as good as no checking.  You can easily create a partition table that
has absolutely no relationship with reality, so it's a good idea to calculate
them in advance.  For each partition, we need to know three things:
.Ls B
.LI
.X "partition, type"
The \fIpartition type\fP, which
.Command fdisk
calls \fIsysid\fP.  This is a number describing what the partition is used for.
FreeBSD partitions have partition type 165, and modern (MS-DOS Release 4 and
later) Microsoft partitions have type 6.
.LI
The \fIstart sector\fP, the first sector in the partition.
.LI
The \fIend sector\fP\/ for the partition.
.Le
.X "active partition"
.X "partition, active"
.ne 3v
In addition, we need to decide which partition is the \fIactive\fP\/ partition,
the partition from which we want to boot.  In this case, it doesn't make any
difference, because we won't be booting from the disk, but it's always a good
idea to set it anyway.
.P
We specify the partitions we don't want by giving them a type, start sector and
end sector of 0.  Our disk has 8386733 sectors, numbered 0 to 8386732.
Partitions should start and end on a cylinder boundary, and we want the
Microsoft partition to be about 1 GB.  1 GB is 1024 MB, and 1 MB is 2048 sectors
of 512 bytes each, so theoretically we want 1024 \(mu 2048, or 2197152 sectors.
Because of the requirement that partitions begin and end on a ``cylinder''
boundary, we need to find the closest number of ``cylinders'' to this value.
First we need to find out how big a ``cylinder'' is.  We can do this by running
.Command fdisk
without any options:
.Dx
# \f(CBfdisk da1\fP
******* Working on device /dev/da1 *******
parameters extracted from in-core disklabel are:
cylinders=13726 heads=13 sectors/track=47 (611 blks/cyl)

Figures below won't work with BIOS for partitions not in cyl 1
parameters to be used for BIOS calculations are:
cylinders=13726 heads=13 sectors/track=47 (611 blks/cyl)

fdisk: invalid fdisk partition table found
Media sector size is 512
Warning: BIOS sector numbering starts with sector 1
Information from DOS bootblock is:
The data for partition 1 is:
<UNUSED>
The data for partition 2 is:
<UNUSED>
The data for partition 3 is:
<UNUSED>
The data for partition 4 is:
sysid 165,(FreeBSD/NetBSD/386BSD)
    start 47, size 8386539 (4094 Meg), flag 80 (active)
        beg: cyl 0/ head 1/ sector 1;
        end: cyl 413/ head 12/ sector 47
.De
You'll notice that
.Command fdisk
has decided that there is a FreeBSD partition in partition 4.  That happens even
if the disk is brand new.  In fact, this is a less desirable feature of
.Command fdisk  \/:
it ``suggests'' this partition, it's not really there, which can be really
confusing.  This printout does, however, tell us that
.Command fdisk
thinks there are 611 sectors per cylinder, so we divide 2197152 by 611 and get
3423.327 cylinders.  We round down to 3423 cylinders, which proves to be 2091453
sectors.  This is the length we give to the first partition.
.P
We use the remaining space for the FreeBSD partition.  How much?  Well,
.Command dmesg
tells us that there are 8386733 sectors, but if you look at the geometry that
.Command fdisk
outputs, there are 13726 cylinders with 13 heads (tracks) per cylinder and 47
sectors per track.  13726 \(mu 13 \(mu 47 is 8386586.  This rounding down is the
explanation for the missing data at the end of the disk that we saw on page
.Sref \*[disk-partition-menu] .
The best way to calculate the size of the FreeBSD partition is to take the
number of cylinders and multiply by the number of tracks per cylinder.  The
FreeBSD partition starts behind the Microsoft partition, so it goes from
cylinder 3423 to cylinder 13725 inclusive, or 10303 cylinders.  At 611 sectors
per cylinder, we have a total of 6295133 sectors in the partition.  Our
resulting information is:
.br
.ne 10v
.Table-heading "sample \fIfdisk\fP\/ parameters"
.TS H
tab(#) ;
 rfCWp9| rfCWp9| rfCWp9| rfCWp9 .
\fRPartition#\fRPartition#\fRStart#\fRSize
\fRnumber#\fRtype#\fRsector
_
.TH N
1#6#1#2091453
2#165#2091453#6295133
3#0#0
4#0#0
.TE
.sp 2v
Next we run
.Command fdisk
in earnest by specifying the \f(CW-i\fP option.  During this time, you may see
messages on the console:
.Dx
da1: invalid primary partition table: no magic
.De
The message \fIno magic\fP\/ doesn't mean that
.Command fdisk
is out of purple smoke.  It refers to the fact that it didn't find the so-called
\fImagic number\fP, which identifies the partition table.  We don't have a
partition table yet, so this message isn't surprising.  It's also completely
harmless.
.P
.Command fdisk
prompts interactively when you specify the \f(CW-i\fP flag:
.Dx
# \f(CBfdisk -i da1\fP
******* Working on device /dev/da1 *******
parameters extracted from in-core disklabel are:
cylinders=13726 heads=13 sectors/track=47 (611 blks/cyl)

Figures below won't work with BIOS for partitions not in cyl 1
parameters to be used for BIOS calculations are:
cylinders=13726 heads=13 sectors/track=47 (611 blks/cyl)

Do you want to change our idea of what BIOS thinks ? [n]  \f(BIEnter \fIpressed\f(CW
Media sector size is 512
Warning: BIOS sector numbering starts with sector 1
Information from DOS bootblock is:
The data for partition 1 is:
sysid 165,(FreeBSD/NetBSD/386BSD)
    start 0, size 8386733 (4095 Meg), flag 80 (active)
        beg: cyl 0/ head 0/ sector 1;
        end: cyl 522/ head 12/ sector 47
Do you want to change it? [n] \f(CBy\fP
Supply a decimal value for "sysid (165=FreeBSD)" [0] \f(CB6\fP
Supply a decimal value for "start" [0]  \f(BIEnter \fIpressed\f(CW
Supply a decimal value for "size" [0] \f(CB2091453\fP
Explicitly specify beg/end address ? [n]  \f(BIEnter \fIpressed\f(CW
sysid 6,(Primary 'big' DOS (> 32MB))
    start 0, size 2091453 (1021 Meg), flag 0
        beg: cyl 0/ head 0/ sector 1;
        end: cyl 350/ head 12/ sector 47
Are we happy with this entry? [n] \f(CBy\fP
The data for partition 2 is:
<UNUSED>
Do you want to change it? [n] \f(CBy\fP
Supply a decimal value for "sysid (165=FreeBSD)" [0] \f(CB165\fP
Supply a decimal value for "start" [0] \f(CB2091453\fP
Supply a decimal value for "size" [0] \f(CB6295133\fP
Explicitly specify beg/end address ? [n]  \f(BIEnter \fIpressed\f(CW
sysid 165,(FreeBSD/NetBSD/386BSD)
    start 2091453, size 6295133 (3073 Meg), flag 0
        beg: cyl 351/ head 0/ sector 1;
        end: cyl 413/ head 12/ sector 47
Are we happy with this entry? [n] \f(CBy\fP
The data for partition 3 is:
<UNUSED>
Do you want to change it? [n] \f(BIEnter \fIpressed\f(CW
The data for partition 4 is:
sysid 165,(FreeBSD/NetBSD/386BSD)
    start 47, size 8386539 (4094 Meg), flag 80 (active)
        beg: cyl 0/ head 1/ sector 1;
        end: cyl 413/ head 12/ sector 47
Do you want to change it? [n] \f(CBy\fP

The static data for the DOS partition 4 has been reinitialized to:
sysid 165,(FreeBSD/NetBSD/386BSD)
    start 47, size 8386539 (4094 Meg), flag 80 (active)
        beg: cyl 0/ head 1/ sector 1;
        end: cyl 413/ head 12/ sector 47
Supply a decimal value for "sysid (165=FreeBSD)" [165] \f(CB0\fP
Supply a decimal value for "start" [47] \f(CB0\fP
Supply a decimal value for "size" [8386539] \f(CB0\fP
Explicitly specify beg/end address ? [n] \f(BIEnter \fIpressed\f(CW
<UNUSED>
Are we happy with this entry? [n] \f(CBy\fP
Do you want to change the active partition? [n] \f(CBy\fP
Supply a decimal value for "active partition" [1] \f(CB2\fP
Are you happy with this choice [n] \f(CBy\fP

We haven't changed the partition table yet.  This is your last chance.
parameters extracted from in-core disklabel are:
cylinders=13726 heads=13 sectors/track=47 (611 blks/cyl)

Figures below won't work with BIOS for partitions not in cyl 1
parameters to be used for BIOS calculations are:
cylinders=13726 heads=13 sectors/track=47 (611 blks/cyl)

Information from DOS bootblock is:
1: sysid 6,(Primary 'big' DOS (> 32MB))
    start 0, size 2091453 (1021 Meg), flag 0
        beg: cyl 0/ head 0/ sector 1;
        end: cyl 350/ head 12/ sector 47
2: sysid 165,(FreeBSD/NetBSD/386BSD)
    start 2091453, size 6295133 (3073 Meg), flag 80 (active)
        beg: cyl 351/ head 0/ sector 1;
        end: cyl 413/ head 12/ sector 47
3: <UNUSED>
4: <UNUSED>
Should we write new partition table? [n] \f(CBy\fP
.De
You'll notice a couple of things here:
.Ls B
.LI
Even though we created valid partitions 1 and 2, which cover the entire drive,
.Command fdisk
gave us the phantom partition 4 which covered the whole disk, and we had to
remove it.
.LI
The cylinder numbers in the summary at the end don't make any sense.  We've
already calculated that the Microsoft partition goes from cylinder 0 to cylinder
3422 inclusive, and the FreeBSD partition goes from cylinder 3423 to cylinder
13725.  But
.Command fdisk
says that the Microsoft partition goes from cylinder 0 to cylinder 350
inclusive, and the FreeBSD partition goes from cylinder 351 to cylinder 413.
What's that all about?
.P
.ne 2v
The problem here is overflow: once upon a time, the maximum cylinder value was
1023, and
.Command fdisk
still thinks this is the case.  The numbers we're seeing here are the remainder
left by dividing the real cylinder numbers by 1024.
.\" 0 d5f  359d
.\"   15f   19d
.Le
.SPUP
.H3 "Labelling the disk"
.X "labelling disks"
.X "disks, labelling"
.X "disk slice"
.X "slice, disk"
Once we have a valid PC BIOS partition table, we need to create the file
systems.  We won't look at the Microsoft partition in any more detail, but we
still need to do some more work on our FreeBSD slice (slice or PC BIOS partition
2).  It'll make life easier here to remember a couple of things:
.Ls B
.LI
From now on, we're just looking at the slice, which we can think of as a logical
disk.  Names like \fIdisk label\fP\/ really refer to the slice, but many
standard terms use the word \fIdisk\fP, so we'll continue to use them.
.LI
All offsets are relative to the beginning of the slice, not the beginning of the
disk.  Sizes also refer to the slice and not the disk.
.Le
The first thing we need is the disk (slice) label, which supplies general
information about the slice:
.Ls B
.LI
The fact that it's a FreeBSD slice.
.LI
The size of the slice.
.LI
The sizes, types and layout of the file systems.
.LI
Some obsolete information about details like rotational speed of the disk and
the track-to-track switching time.  This is still here for historical reasons
only.  It may go away soon.
.Le
The only information we need to input is the kind, size and locations of the
partitions.  In this case, we have decided to create a file system on partition
\f(CWh\fP 
.File -n ( /dev/da1s2h \/)
and swap space on partition \f(CWb\fP
.File -n ( /dev/da1s1b ).
The swap space will be 512 MB, and the file system will take up the rest of the
slice.  This is mainly tradition: traditionally data disks use the \f(CWh\fP
partition and not the \f(CWa\fP partition, so we'll stick to that tradition,
though there's nothing to stop you from using the \f(CWa\fP partition if you
prefer.  In addition, we need to define the \f(CWc\fP partition, which
represents the whole slice.  In summary, the FreeBSD slice we want to create
looks like:
.PS
h = .2i
dh = .02i
dw = 1.7i
boxwid = 3i
down
[    S: box invis
    FA: box ht .2i "\fI/dev/da1s2b\fR: FreeBSD swap, 512 MB" at S+(1,0)
        box ht .2i "\fI/dev/da1s2h\fR\/: \fI/newhome\fP\/ file system, 2.5 GB"
        ]
.PE
.Figure-heading "FreeBSD slice on second disk"
.Fn second-disk-freebsd-slice
.H3 "bsdlabel"
.Pn disklabel
The program that writes the disk label used to be called
.Command disklabel .
As FreeBSD migrated to multiple platforms, this proved to be too generic: many
hardware platforms have their own disk label formats.  For example, FreeBSD on
SPARC64 uses the Sun standard labels.  On platforms which use the old BSD
labels, such as the PC, the name was changed to
.Command bsdlabel .
On SPARC64 it is called
.Command sunlabel .
On each platform, the appropriate file is linked to the name
.Command disklabel ,
but some of the options have changed.  In addition, the output format now
normally ignores a number of historical relics.  It's not as warty as
.Command fdisk ,
but it can still give you a run for your money.  You can usually ignore most of
the complexity, though.  You can normally create a disk label with the single
command:
.Dx
# \f(CBbsdlabel -w /dev/da1s2 auto\fP
.De
This creates the label with a single partition, \f(CWc\fP.  You can look at the
label with
.Command bsdlabel
without options:
.Dx
# \f(CBbsdlabel /dev/da1s2\fP
# /dev/da0s2:
8 partitions:
#        size   offset    fstype   [fsize bsize bps/cpg]
  c:  6295133        0    unused        0     0         # "raw" part, don't edit
.De
At this point, the only partition you have is the ``whole disk'' partition
\f(CWc\fP.  You still need to create partitions \f(CWb\fP and \f(CWh\fP and
specify their location and size.  Do this with \fIbsdlabel -e\fP, which starts
an editor with the output you see above.  Simply add additional partitions:
.Dx
8 partitions:
#        size   offset    fstype   [fsize bsize bps/cpg]
  c:  6295133        0    unused        0     0         # "raw" part, don't edit
.ft CB
  b:  1048576        0    swap          0     0
  h:  5246557  1048576    unused        0     0
.ft
.De
You don't need to maintain any particular order, and you don't need to specify
that partition \f(CWh\fP will be a file system.  In the next step,
.Command newfs
does that for you automatically.
.\" Because of the list at the beginning of the section
.br
.ne 12v
.H3 "Problems running bsdlabel"
Using the old
.Command disklabel
program used to be like walking through a minefield.  Things have got a lot
better, but it's possible that some problems are still hiding.  Here are some of
the problems that have been encountered in the past, along with some suggestions
about what to do if you experience them:
.Ls B
.LI
When writing a label (the \f(CW-w\fP option), you may find:
.Dx
# \f(CBbsdlabel -w da1s2\fP
bsdlabel: /dev/da1s2c: Undefined error: 0
.De
This message may be the result of the kernel having out-of-date information
about the slice in memory.  If this is the case, a reboot may help.
.LI
\f(CWNo disk label on disk\fP is straightforward enough.  You tried to use
.Command bsdlabel
to look at the label before you had a label to look at.
.LI
\f(CWLabel magic number or checksum is wrong!\fP tells you that
.Command bsdlabel
thinks it has a label, but it's invalid.  This could be the result of an
incorrect previous attempt to label the disk.  It can be difficult to get rid of
an incorrect label.  The best thing to do is to repartition the disk with the
label in a different position, and then copy
.Device zero
to where the label used to be:
.Dx
# \f(CBdd if=/dev/zero of=/dev/da1 bs=128k count=1\fP
.De
Then you can repartition again the way you want to have it.
.LI
\f(CWOpen partition would move or shrink\fP probably means that you have
specified incorrect values in your slice definitions.  Check particularly that
the \f(CWc\fP partition corresponds with the definition in the partition table.
.LI
\f(CWwrite: Read-only file system\fP means that you are trying to do something
invalid with a valid disk label.  FreeBSD write protects the disk label, which
is why you get this message.
.LI
In addition, you might get kernel messages like:
.Dx
fixlabel: raw partition size > slice size
\fIor\fP\/
fixlabel: raw partitions offset != slice offset
.De
The meanings of these messages should be obvious.
.Le
.SPUP
.H2 "Creating file systems"
.X "creating file systems"
.X "file system, creating"
Once we have a valid label, we need to create the file systems.  In this case,
there's only one file system, on
.Device da1s2h .
Mercifully, this is easier:
.Dx
# \f(CBnewfs -U /dev/da1s2h\fP
/dev/vinum/da1s2h: 2561.8MB (5246556 sectors) block size 16384, fragment size 2048
        using 14 cylinder groups of 183.77MB, 11761 blks, 23552 inodes.
        with soft updates
super-block backups (for fsck -b #) at:
 160, 376512, 752864, 1129216, 1505568, 1881920, 2258272, 2634624, 3010976, 3387328,
 3763680, 4140032, 4516384, 4892736
.De
The \f(CW-U\fP flag tells
.Command newfs
to enable soft updates, which we looked at on page
.Sref \*[soft-updates] .
.br
.ne 10v
.H3 "Mounting the file systems"
.X "mounting, file systems"
.X "file system, mounting"
Finally the job is done.  Well, almost.  You still need to mount the file
system, and to tell the system that it has more swap.  But that's not much of a
problem:
.Dx
# \f(CBmkdir /newhome\fP                                \fImake sure we have a directory to mount on\fP\/
# \f(CBmount /dev/da1s2h /newhome\fP                    \fIand mount it\fP\/
.Pn swapon
# \f(CBswapon /dev/da1s2b\fP
# \f(CBdf\fP                                            \fIshow free capacity and mounted file systems\fP\/
Filesystem    1024-blocks     Used    Avail Capacity  Mounted on
/dev/ad0s1a         19966    17426      944    95%    /
/dev/ad0s1e       1162062   955758   113340    89%    /usr
procfs                  4        4        0   100%    /proc
presto:/            15823     6734     8297    45%    /presto/root
presto:/usr        912271   824927    41730    95%    /presto/usr
presto:/home      1905583  1193721   521303    70%    /presto/home
presto:/S         4065286  3339635   563039    86%    /S
/dev/da1s2h       2540316        2  2337090     0%    /newhome
# \f(CBpstat -s\fP                                      \fIshow swap usage\fP\/
Device          1K-blocks     Used    Avail Capacity  Type
/dev/ad0s4b       524160        0   524160     0%    Interleaved
/dev/da1s2b       524160        0   524160     0%    Interleaved
Total            1048320        0  1048320     0%
.De
This looks fine, but when you reboot the system,
.Directory /newhome
and the additional swap will be gone.  To ensure that they get mounted after
booting, you need to add the following lines to
.File /etc/fstab \/:
.Dx
/dev/da1s2b             none            swap    sw      0       0
/dev/da1s2h             /newhome        ufs     rw      0       0
.De
.SPUP
.H2 "Moving file systems"
.Pn move-fs
Very frequently, you add a new disk to a system because existing disks have run
out of space.  Let's consider the disk we have just added and assume that
currently the files in
.Directory /home
are physically located on the
.Directory /usr
file system, and that
.Directory /home
is a symbolic link to
.Directory /usr/home .
We want to move them to the new file system and then rename it to
.Directory /home .
Here's what to do:
.Ls B
.LI
Copy the files:
.Dx
# \f(CBcd /home\fP
# \f(CBtar cf - . | (cd /newhome; tar xvf - 2>/var/tmp/tarerrors)\fP
.De
This writes any error messages to the file
.File -n /var/tmp/tarerrors .
If you don't do this, any errors will get lost.
.LI
\f(BICheck /var/tmp/tarerrors and make sure that the files really made it to the
right place!\fP\/
.LI
.ne 3v
Remove the old files:
.Dx
# \f(CBrm -rf /usr/home\fP
.De
.SPUP
.LI
In this case,
.Directory /home
was a symbolic link, so we need to remove it and create a directory called
.Directory /home \/:
.Dx
# \f(CBrm /home\fP
# \f(CBmkdir /home\fP
.De
You don't need to do this if
.Directory /home
was already a directory (for example, if you're moving a complete file system).
.LI
Modify
.File /etc/fstab
to contain a line like:
.Dx
/dev/da1s2h             /home           ufs     rw      0       0
.De
.SPUP
.LI
Unmount the
.Directory /newhome
directory and mount it as
.Directory /home \/:
.Dx
# \f(CBumount /newhome\fP
# \f(CBmount /home\fP
.De
.Le
.\" XXX
.sp -2v
.H2 "Recovering from disk data errors"
.Pn disk-error-recovery
.X "recovering from disk data errors"
.X "disk, recovering from data errors"
Modern hard disks are a miracle in evolution.  Today you can buy a 200 GB hard
disk for under $200, and it will fit in your shirt pocket.  Thirty years ago, a
typical disk drive was the size of a washing machine and stored 20 MB.  You
would need 10,000 of them to store 200 GB.
.P
.ne 3v
At the same time, reliability has gone up, but disks are still relatively
unreliable devices.  You can achieve maximum reliability by keeping them cool,
but sooner or later you are going to run into some kind of problem.  One kind is
due to surface irregularities: the disk can't read a specific part of the
surface.
.P
Modern disks make provisions for recovering from such errors by allocating an
alternate sector for the data.  IDE drives do this automatically, but with SCSI
drives you have the option of enabling or disabling reallocation.  Usually
reallocation is enabled when you buy the disk, but occasionally it is not.  When
installing a new disk, you should check that the parameters \fIARRE\fP\/
(\fIAuto Read Reallocation Enable\fP\/) and \fIAWRE\fP\/ (\fIAuto Write
Reallocation Enable\fP\/) are turned on.  For example, to check and set the
values for disk
.Device -n da1 ,
you would enter:
.Dx
# \f(CBcamcontrol modepage da1 -m 1 -e\fP
.De
This command will start up your favourite editor (either the one specified in
the \f(CWEDITOR\fP environment variable, or
.Command vi
by default) with the following data:
.Dx
AWRE (Auto Write Reallocation Enbld):  0
ARRE (Auto Read Reallocation Enbld):  0
TB (Transfer Block):  1
EER (Enable Early Recovery):  0
PER (Post Error):  1
DTE (Disable Transfer on Error):  0
DCR (Disable Correction):  0
Read Retry Count:  41
Write Retry Count:  24
.De
The values for \f(CWAWRE\fP and \f(CWARRE\fP should both be 1.  If they aren't,
as in this case, where \f(CWAWRE\fP is 0, change the data with the editor, write
it back, and exit.
.Command camcontrol
writes the data back to the disk and enables the option.
.P
Note the last two lines in this example.  They give the number of actual retries
that this drive has performed.  You can reset these values too if you want; they
will be updated if the drive performs any additional retries.
