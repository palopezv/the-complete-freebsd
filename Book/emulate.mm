.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: --- draft 4th edition
.\" $Id: emulate.mm,v 4.6 2003/01/15 08:27:03 grog Exp $
.\"
.Chapter \*[nchemulate] "Emulating other systems"
.X "emulate"
.X "/usr/ports/emulators"
A large number of operating systems run on Intel hardware, and there is a lot of
software which is available for these other operating systems, but not for
FreeBSD.
.H2 "Emulators and simulators"
There are a number of ways to execute software written for a different
platform.  The most popular are:
.Ls B
.LI
\fISimulation\fP\| is a process where a program executes the functions which are
normally performed by the native instruction set of another machine.  Since they
simulate the low-level instructions of the target machine, simulators don't have
to run on the same kind of machine as the code which they execute.  A good
example is the port \fIemulators/p11\fP, which simulates a PDP-11 minicomputer,
the machine for which most early versions of UNIX were written.
.P
Simulators run much more slowly than the native instruction set: for each
simulated instruction, the simulator may execute hundreds of machine
instructions.  Amusingly, on most modern machines, the \fIp11\fP\| emulator
still runs faster than the original PDP-11: modern machines are over 1,000 times
faster than the PDP-11.
.LI
In general, \fIemulators\fP\| execute the program instructions directly and only
simulate the operating system environment.  As a result, they have to run on the
same kind of hardware, but they're not noticeably slower than the original.  If
there is any difference in performance, it's because of differences between the
host operating system and the emulated operating system.
.LI
Another use for the term \fIemulator\fP\| is where the hardware understands a
different instruction set than the native one.  Obviously this is not the kind
of emulator we're talking about here.
.Le
FreeBSD can \fIemulate\fP\| many other systems to a point where applications
written for these systems will run under FreeBSD.  You'll find all the emulators
in the Ports Collection in the directory 
.File /usr/ports/emulators .
This directory also contains a number of emulators for less well known systems.
.P
In a number of cases, the emulation support is in an experimental stage.  Here's
an overview:
.Ls B
.LI
FreeBSD will run most BSD/OS programs with no problems.
.LI
FreeBSD will also run most NetBSD and OpenBSD executables, though not many
people do this: it's safer to recompile them under FreeBSD.
.LI
.X "Linux"
.X "a.out, object format"
.X "object format, a.out"
.X "ELF, object format"
.X "object format, ELF"
.X "kld"
.X "loadable kernel module"
FreeBSD runs Linux executables with the aid of the 
.Command linux
\fIkld\fP\| (\fIloadable kernel module\fP\|).  We'll look at how to use it in
the next section.
.LI
.X "ibcs2"
FreeBSD can run SCO COFF executables with the aid of the 
.Command ibcs2
\fIkld\fP.  This support is a little patchy: although the executables will run,
you may run into problems caused by differences in the directory structure
between SCO and FreeBSD.  We'll look at it on page
.Sref \*[SCO-emulation] .
.LI
.X "Microsoft Windows"
A \fIMicrosoft Windows\fP\| emulator is available.  We'll look at it on page
.Sref \*[wine] .
.LI
Two MS-DOS emulators are available.  Consider them both experimental.
.Le
.SPUP
.H2 "Emulating Linux"
.X "Torvalds, Linus"
Linux is a UNIX-like operating system which in many ways is very similar to
FreeBSD.  It is not based on UNIX, however: it was written from scratch.  As a
result, many of the internal kernel interfaces are different from those of
FreeBSD or other UNIX-based systems.  The Linux compatibility package handles
these differences, and most Linux software will run on FreeBSD.  Most of the
exceptions use specific drivers which don't run on FreeBSD, though there is a
considerable effort to minimize even this category.
.P
To install the Linux emulator, you must:
.Ls B
.LI
.X "linux, kld"
.X "kld, linux"
Run the Linux emulator kld, 
.Command linux .
.LI
Install the compatibility libraries.
.Le
.H3 "Running the Linux emulator"
.Pn Linux-emulation
Normally you load the Linux emulator when you boot the system.  Put the
following line in your 
.File /etc/rc.conf \|:
.Dx
linux_enable="YES"
.De
If you don't want to do this for some reason, you can start it from the command line:
.Dx
# \f(CBlinux\fP
.De
You don't interact directly with the emulator module: it's just there to supply
kernel functionality, so you get a new prompt immediately when you start it.
.P
.X "kldstat, command"
.X "command, kldstat"
Since 
.Command linux
is an kld, it won't show up in a
.Command ps
listing.  To check whether it is loaded, use
.Command kldstat \|:
.Dx
$ \f(CBkldstat\fP
Id Refs Address    Size     Name
 1    5 0xc0100000 1d08b0   kernel
 2    2 0xc120d000 a000     ibcs2.ko
 3    1 0xc121b000 3000     ibcs2_coff.ko
 5    1 0xc1771000 e000     linux.ko
.De
This listing shows that the SCO UNIX emulation (\fIibcs2\fP\|) has also been
loaded.
.P
The Linux emulator and programs are located in the directory hierarchy
.File /usr/compat/linux/ .
You won't normally need to access them directly, but if you get a Linux program
which includes libraries destined for
.File /lib ,
you will need to manually place them in
.File /usr/compat/linux/lib .
Be \f(BIvery\fP\| careful not to replace any files in the
.File /usr/lib/
hierarchy with Linux libraries; this would make it impossible to run programs
which depend on them, and it's frequently very difficult to recover from such
problems.  Note that FreeBSD does not have a directory
.File /lib ,
so the danger is relatively minor.
.H3 "Problems executing Linux binaries"
.X "branding ELF binaries"
.X "brandelf, command"
.X "command, brandelf"
.X "StarOffice, package"
One of the problems with the ELF format used by more recent Linux binaries is
that they usually contain no information to identify them as Linux binaries.
They might equally well be BSD/OS or UnixWare binaries.  That's normally not a
problem, unless there are library conflicts: the system can't decide which
shared library to use.  If you have this kind of binary, you must \fIbrand\fP\|
the executable using the program 
.Command brandelf .
For example, to brand the \fIStarOffice\fP\| program
.Command swriter3 ,
you would enter:
.Dx
# \f(CBbrandelf -t Linux /usr/local/StarOffice-3.1/linux-x86/bin/swriter3\fP
.De
This example deliberately shows a very old version of StarOffice: it's not clear
that there are any modern binaries which cause such problems.
.H2 "SCO UNIX emulation"
.Pn SCO-emulation
.X "SCO UNIX"
.X "SCO OpenDesktop"
.X "COFF, object format"
.X "object format, COFF"
.X "Common Object File Format"
\fISCO UNIX\fP\|, also known as \fISCO OpenDesktop\fP\| and \fISCO Open
Server\fP, is based on UNIX version System V.3.2.  This particular version of
UNIX was current in the late 1980s.  It uses a binary format called \fICOFF\fP\|
(\fICommon Object File Format\fP\|).  COFF is now considered obsolescent.
.P
.X "ibcs2, kld"
.X "kld, ibcs2"
Like Linux support, SCO support for FreeBSD is supplied as a loadable kernel
module.  It's not called 
.Command sco ,
though, since a number of older System V.3.2 systems, including Interactive
UNIX, also support the \fIibcs2\fP\|\*F
.FS
.X "Intel Binary Compatibility System 2"
\fIibcs2\fP\| stands for \fIIntel Binary Compatibility System 2\fP.
.FE
standard.
As a result, the kld is called \fIibcs2\fP.
.P
Run ibcs2 support like Linux support: start it manually, or modify
.File /etc/rc.conf
to start it automatically at bootup:
.Dx
ibcs2_enable="YES"	# Ibcs2 (SCO) emulation loaded at startup (or NO).
.De
One problem with SCO emulation are the SCO shared libraries.  These are required
to execute many SCO executables, and they're not supplied with the emulator.
They \fIare\fP\| supplied with SCO's operating systems.  Check the SCO license
to determine whether you are allowed to use them on FreeBSD.  You may also be
eligible for a free SCO license\(emsee
\fIhttp://www.sco.com/offers/index.htm\fP\| for further details.
.\"XXX.H2 "UNIX System V.4 emulation"
.\"XXX.Pn svr4-emulation
.\"XXXThe UNIX System V.4 emulation package specifically emulates SCO UnixWare. XXX
.H2 "Emulating Microsoft Windows"
.Pn wine
.X "wine, command"
.X "command, wine"
The \fIwine\fP\| project has been working for some time to provide an emulation
of Microsoft's \fIWindows\fP\| range of execution environments.  It's part of
the Ports Collection.  It's changing continually, so there's little point
describing it here.  You can find up-to-date information at
.URI http://www.winehq.com/about/ .
