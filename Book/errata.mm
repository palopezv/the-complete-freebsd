.\" This file is in -*- nroff-fill -*- mode
.so global.mm
.Chapter 0 "Errata and addenda for the Complete FreeBSD, second edition"
.ce
\fILast revision: 21 June 1999\fR\|
.P
The trouble with books is that you can't update them the way you can a web page
or any other online documentation.  The result is that most leading edge
computer books are out of date almost before they are printed.  Unfortunately,
``The Complete FreeBSD'', published by Walnut Creek, is no exception.
Inevitably, a number of bugs and changes have surfaced.
.P
The following is a list of modifications which go beyond simple typos.  They
relate to the second edition, formatted on 16 December 1997.  If you have this
book, please check this list.  If you have the first edition of 19 July 1996,
please check
.URL ftp://ftp.lemis.com/pub/cfbsd/errata-1 .  
This same file is also available via the web link
.URL http://www.lemis.com/ .
.P
This list is available in four forms:
.Ls B
.LI
A PostScript version, suitable for printing out, at
.URL ftp://ftp.lemis.com/pub/cfbsd/errata-2.ps .
See page 222 of the book to find out how to print out PostScript.  If at all
possible, please take this document: it's closest to the original text.
.P
Be careful selecting this file with a web browser: it is often impossible to
reload the document, and you may see a previously cached version.
.LI
An enhanced ASCII version at
.URL ftp://ftp.lemis.com/pub/cfbsd/errata-2.txt .
When viewed with \fImore\fR\| or \fIless\fR, this version will show some
highlighting and underlining.  It's not suitable for direct viewing.
.LI
An ASCII-only version at
.URL ftp://ftp.lemis.com/pub/cfbsd/errata-2.ascii .
This version is posted every week to the \f(CWFreeBSD-questions\fR mailing list.
Only take this version if you have real problems with PostScript: I can't be
sure that the lack of different fonts won't confuse the meaning.
.LI
A web version at 
.URL http://www.lemis.com/errata-2.html .
.Le
All these modifications have been applied to the ongoing source text of the
book, so if you buy a later edition, they will be in it as well.  If you find a
bug or a suspected bug in the book, please contact me at
.Mailid \f(CWgrog@FreeBSD.org\fR.
.H2 "General changes"
.Ls B
.LI
In a number of places, I suggest the use of the following command to find
process information:
.Dx
$ ps aux | grep \fIfoo\fR\|
.De
Unfortunately, \fIps\fR\| is sensitive to the column width of the terminal
emulator upon which it is working.  This command usually works fine on a
relatively wide \fIxterm\fR, but if you're running on an 80-column terminal, it
may truncate exactly the information you're looking for, so you end up with no
output.  You can fix that with the \f(CWw\fR option:
.Dx
$ ps waux | grep \fIfoo\fR\|
.De
Thanks to Sue Blake 
.Mailid sue@welearn.com.au
for this information
.Le
.H3 "Location of the sample files"
On the 2.2.5 CD-ROM only, the location of the sample files does not match the
specifications in the book (\fI/book\fP\| on the first CD-ROM).  The 2.2.5
CD-ROM came out before the book, and it contains the files on the third
(repository) CD-ROM as a single gzipped tar file
\fI/xperimnt/cfbsd/cfbsd.tar.gz\fR\|.  It contains the following files:
.Dx
drwxr-xr-x jkh/jkh           0 Oct 17 13:01 1997 cfbsd/
drwxr-xr-x jkh/jkh           0 Oct 17 13:01 1997 cfbsd/mutt/
-rw-r--r-- jkh/jkh         352 Oct 15 15:21 1997 cfbsd/mutt/.mail_aliases
-rw-r--r-- jkh/jkh        9394 Oct 15 15:22 1997 cfbsd/mutt/.muttrc
drwxr-xr-x jkh/jkh           0 Oct 17 14:02 1997 cfbsd/scripts/
-rw-r--r-- jkh/jkh       18281 Oct 16 16:52 1997 cfbsd/scripts/.fvwm2rc
-rwxr-xr-x jkh/jkh        1392 Oct 17 12:54 1997 cfbsd/scripts/install-desktop
-rw-r--r-- jkh/jkh         296 Oct 17 12:35 1997 cfbsd/scripts/.xinitrc
-rwxr-xr-x jkh/jkh         622 Oct 17 13:51 1997 cfbsd/scripts/install-rcfiles
-rw-r--r-- jkh/jkh        1133 Oct 17 13:00 1997 cfbsd/scripts/Uutry
-rw-r--r-- jkh/jkh        1028 Oct 17 14:02 1997 cfbsd/scripts/README
drwxr-xr-x jkh/jkh           0 Oct 18 19:32 1997 cfbsd/docs/
-rw-r--r-- jkh/jkh      199111 Oct 16 14:29 1997 \f(CBcfbsd/docs/packages.txt\f(CW
-rw-r--r-- jkh/jkh      189333 Oct 16 14:28 1997 cfbsd/docs/packages-by-category.txt
-rw-r--r-- jkh/jkh      188108 Oct 16 14:29 1997 cfbsd/docs/packages.ps
-rw-r--r-- jkh/jkh      226439 Oct 16 14:27 1997 cfbsd/docs/packages-by-category.ps
-rw-r--r-- jkh/jkh         788 Oct 16 15:01 1997 cfbsd/README
-rw-r--r-- jkh/jkh         248 Oct 17 11:52 1997 cfbsd/errata
.De
To extract one of these files, say \fIcfbsd/docs/packages.txt\fR, and assuming
you have the CD-ROM mounted as \fI/cdrom\fR, enter:
.Dx
# cd /usr/share/doc
# tar xvzf /cdrom/xperimnt/cfbsd/cfbsd.tar.gz cfbsd/docs/packages.txt
.De
See page 209 for more information on using \fItar\fR\|.
.P
These files are an early version of what is described in the book.  I'll put up
some updated versions on
.URL ftp://ftp.lemis.com/
in the near future.
.P
Thanks to Frank McCormick 
.Mailid gfm@readybox.com
for drawing this to my attention.
.H3 "Chapter 8: Setting up X11"
For FreeBSD 2.2.7, this chapter has changed sufficiently to make it impractical
to distribute errata.  You can download the PostScript version from
\fIftp://www.lemis.com/pub/cfbsd/xsetup.ps\fP, or the ASCII version from
\fIftp://www.lemis.com/pub/cfbsd/xsetup.txt\fP.  No HTML version is available.
.H3 "Page xxxiv"
\fIBefore the discussion of the shell prompts in the middle of the page,
add\fP\|:
.P
In this book, I recommend the use of the Bourne shell or one of its descendents
(\fIsh\fP, \fIbash\fP, \fIpdksh\fP, \fIksh\fP\| or \fIzsh\fP\|).  With the
exception of \fIsh\fP, they are all in the Ports Collection.  I personally use
the \fIbash\fP\| shell.
.P
This is a personal preference, and a recommendation, but it's not the standard
shell.  The standard BSD shell is the C shell (\fIcsh\fP\|), which has a
fuller-featured descendent \fItcsh\fP.  In particular, the standard installation
sets the \fIroot\fP\| user up with a \fIcsh\fP.  See page 152 (\fIin this
errata\fP\|) for details of how to change the shell.
.H3 "Page 11: Reading the handbook"
\fIThe CD-ROM now includes Netscape.  Replace the last paragraph on the page and
the example on the following page with:\fP\|
.P
If you're running X, you can use a browser like \fInetscape\fP\| to read the
handbook.  If you don't have X running yet, use \fIlynx\fP\|.  Both of these
programs are included on the CD-ROM.  To install them, enter:
.Dx
# \f(CBpkg_add /cdrom/packages/All/netscape-communicator-4.5.tgz\fP
\fIor\fP\|
# \f(CBpkg_add /cdrom/packages/All/lynx-2.8.1.1.tgz\fP\fP
.De
The numbers after the name (\f(CW4.5\fP and \f(CW2.8.1.1\fP) may change after
this book has been printed.  Use \fIls\fP\| to list the names if you can't find
these particular versions.
.P
Note that \fIlynx\fP\| is not a complete substitute for \fInetscape\fP\|: since
it is text-only, it is not capable of displaying the large majority of web pages
correctly.  It will suffice for reading most of the handbook, however.
.P
Thanks to Stuart Henderson \f(CW<stuart@internationalschool.co.uk>\fP and
\f(CW<gkaplan@castle.net>\fP for drawing this to my attention.
.H3 "Page 12: Printing the handbook"
\fIThe instructions for formatting the handbook are obsolete.  Replace the
section starting \f(BIAlternatively, you can print out the handbook\fI with the
following text:\fR
.P
Alternatively, you can print out the handbook.  You need to have the
documentation sources (\fI/usr/doc\fP\|) installed on your system.  You can find
them on the second CD-ROM in the directory of the same name.  To install them,
first mount your CD-ROM (see page 175).  Then enter:
.Dx
$ \f(CBcd /cdrom/usr/doc/handbook\fP
$ \f(CBmkdir -p /usr/doc/handbook\fP			\fIyou may need to be \f(CWroot\fP for this operation\f(CW
$ \f(CBcp -pr * /usr/doc/handbook\fP 
.De
You have a choice of formats for the output:
.Ls B
.LI
\f(CWascii\fP will give you plain 7-bit ASCII output, suitable for reading on a
character-mode terminal.
.LI
\f(CWhtml\fP will give you HTML output, suitable for browsing with a web
browser.
.LI
\f(CWlatex\fP will give you \fI\*[LaTeX]\fP format, suitable for further
processing with \fI\*[TeX]\fP and \fI\*[LaTeX]\fP.
.LI
\f(CWps\fP will give you PostScript output, probably the best choice for
printing.
.LI
\f(CWroff\fP will give you output in \fItroff\fP\| source.  You can process this
output with \fInroff\fP\| or \fItroff\fP, but it's currently not very polished.
\fI\*[LaTeX]\fP\| output is a better choice if you want to process it further.
.Le
Once you have decided your format, use \fImake\fP\| to create the document.  For
example, if you decide on PostScript format, you would enter:
.Dx
$ \f(CBmake FORMATS=ps\fP
.De
This creates a file \fIhandbook.ps\fP\| which you can then print to a PostScript
printer or with the aid of \fIghostscript\fP\| (see page 222).
.P
Thanks to Bob Beer \f(CW<r-beer@onu.edu>\fP for drawing this to my attention.
.H3 "Page 45:  Preparing floppies for installation"
\fIReplace the paragraph below the list of file names (in the middle of the
page) with\fR\|:
.P
The floppy set should contain the file \fIbin.inf\fR\| and the ones whose names
start with \fIbin.\fR followed by two letters.  These other files are all 240640
bytes long, except for the final one which is usually shorter.  Use the MS-DOS
\fICOPY\fR\| program to copy as many files as will fit onto each disk (5 or 6)
until you've got all the distributions you want packed up in this fashion.  Copy
each distribution into subdirectory corresponding to the base name\(emfor
example, copy the \fIbin\fR\| distribution to the files \fIA:\eBIN\eBIN.INF\fR,
\fIA:\eBIN\eBIN.AA\fR and so on.
.H3 "Page 80 and 81"
In a couple of examples, the FreeBSD partition is shown as type 164.  It should
be 165.  Thanks to an unknown contributer for this correction (sorry, I lost
your name).
.H3 "Page 88: setting up for dumping"
The example mentions a variable \f(CWsavecore\fP in \fI/etc/rc.conf\fP.  This
variable is no longer used\(emit's enough to set the variable \f(CWdumpdev\fP.
.H3 "Page 92"
At the end of the section \f(BIHow to install a package\fI add the text\fR\|:
.P
Alternatively, you can install packages from the \fI/stand/sysinstall\fP\| Final
Configuration Menu.  We saw this menu on page in figure 4-14 on page 71.  When
you start \fIsysinstall\fP\| from the command line, you get to this menu by
selecting \f(CWIndex\fP, and then selecting \f(CWConfigure\fP.
.P
.H3 "Page 93"
\fIBefore the heading \f(BIInstall ports from the first CD-ROM\fP\| add\fR\|:
.P
.H2 "Install ports when installing the system"
The file \fIports/ports.tgz\fP\| on the first CD-ROM is a \fItar\fP\| archive
containing all the ports.  You can install it with the base system if you select
the \f(CWCustom\fP distribution and include the \f(CWports collection\fP.  If
you didn't install them at the time, use the following method to install them
all (about 40 MB).  Make sure your CD-ROM is mounted (in this example on
\fI/cdrom\fP\|), and enter:
.H3 "Page 96"
\fIReplace the example at the top of the page with\fP\|:
.P
Instead, do:
.Dx
# \f(CBcd /cd4/ports/distfiles\fP
# \f(CBmkdir -p /usr/ports/distfiles\fP			\fImake sure you have a distfiles directory\fP\|
# \f(CBfor i in *; do\fP
> \f(CB  ln -s /cd4/ports/distfiles/$i /usr/ports/distfiles/$i\fP
> \f(CBdone\fP
.De
If you're using \fIcsh\fP\| or \fItcsh\fP, enter:
.Dx
# \f(CBcd /cd4/ports/distfiles\fP
# \f(CBmkdir -p /usr/ports/distfiles\fP			\fImake sure you have a distfiles directory\fP\|
# \f(CBforeach i (*)\fP
? \f(CB  ln -s /cd4/ports/distfiles/$i /usr/ports/distfiles/$i\fP
? \f(CBend\fP
.De
Thanks to Christopher Raven \f(CW<gurab@lineone.net>\fP and Fran�ois Jacques
\f(CW<francois.jacques@callisto.si.usherb.ca>\fP for drawing this to my
attention.
.H3 "Page 104"
\fIThe examples at the bottom of the page and the top of the next page specify the
wrong directory (\f(BI/usr\fI\|).  It should be \f(BI/usr/X11R6\fI\|.  Replace the
examples with:\fR
.P
For a full install, choose \fI/cdrom/dists/XF86331/X331*.tgz\fP.  If you are
using sh, enter:
.Dx
# \f(CBcd /usr/X11R6\fP
# \f(CBfor i in /cdrom/dists/XF86331/X331*.tgz; do\fP
# \f(CB  tar xzf $i\fP
# \f(CBdone\fP
.De
If you are using csh, enter:
.Dx
% \f(CBcd /usr/X11R6\fP
% \f(CBforeach i (/cdrom/dists/XF86331/X331*.tgz)\fP
% \f(CB  tar xzf $i\fP
% \f(CBend\fP
.De
For a minimal installation, first choose a server archive corresponding to your
VGA board.  If table 8-2 on page 103 doesn't give you enough information, check
the server man pages, starting on page 1545, which list the VGA chip sets
supported by each server.  For example, if you have an ET4000 based board you
will use the XF86_SVGA server.  In this case you would enter:
.Dx
# \f(CBcd /usr/X11R6\fP
# \f(CBtar xzf /cdrom/dists/XF86331/X331SVGA.tgz\fP	\fIsubstitute your server name here\fP\|
# \f(CBfor i in bin fnts lib xicf; do\fP
# \f(CB  tar xzf /cdrom/dists/XF86331/X331$i.tgz\fP
# \f(CBdone\fP
.De
If you are using csh, enter:
.Dx
% \f(CBcd /usr/X11R6\fP
% \f(CBtar xzf /cdrom/dists/XF86331/X331SVGA.tgz\fP	\fIsubstitute your server name here\fP\|
% \f(CBforeach i (bin fnts lib xicf)\fP
% \f(CB  tar xzf /cdrom/dists/XF86331/$i\fP
% \f(CBend\fP
.De
.P
Thanks to Manuel Enrique Garcia Cuesta \f(CW<megarcia@intercom.es>\fP for
pointing out this one.
.H3 "Page 128"
\fIReplace the complete text below the example with the following\fP\|:
.P
These values are defaults, and many are either incorrect for FreeBSD (for
example the device name \fI/dev/com1\fP\|) or do not apply at all (for example
\f(CWXqueue\fP).  If you are configuring manually, select one \f(CWProtocol\fP
and one \f(CWDevice\fP entry from the following selection.  If you must use a
two-button mouse, uncomment the keyword \f(CWEmulate3Buttons\fP\(emin this mode,
pressing both mouse buttons simultaneously within \f(CWEmulate3Timeout\fP
milliseconds causes the server to report a middle button press.
.Dx
Section "Pointer"

    Protocol	"Microsoft"		\fIfor Microsoft protocol mice\fP\|
    Protocol    "MouseMan"		\fIfor Logitech mice\fP\|
    Protocol    "PS/2"			\fIfor a PS/2 mouse\fP\|
    Protocol    "Busmouse"		\fIfor a bus mouse\fP\|

    Device	"/dev/ttyd0"		\fIfor a mouse on the first serial port\fP\|
    Device	"/dev/ttyd1"		\fIfor a mouse on the second serial port\fP\|
    Device	"/dev/ttyd2"		\fIfor a mouse on the third serial port\fP\|
    Device	"/dev/ttyd3"		\fIfor a mouse on the fourth serial port\fP\|
    Device	"/dev/psm0"		\fIfor a PS/2 mouse\fP\|
    Device	"/dev/mse0"		\fIfor a bus mouse\fP\|

    Emulate3Buttons			\fIonly for a two-button mouse\fP\|

EndSection
.De
You'll notice that the protocol name does not always match the manufacturer's
name.  In particular, the \fILogitech\fP\| protocol only applies to older
Logitech mice.  The newer ones use either the MouseMan or Microsoft protocols.
Nearly all modern serial mice run one of these two protocols, and most run both.
.P
If you are using a bus mouse or a PS/2 mouse, make sure that the device driver
is included in the kernel.  The \f(CWGENERIC\fP kernel contains drivers for both
mice, but the PS/2 driver is disabled.  Use UserConfig (see page 50) to enable
it.
.H3 "Page 140"
\fIJust before the paragraph \f(BIThe super user\fI\ add the following
paragraph:\fR
.P
If you \fIdo\fR\| manage to lose the \f(CWroot\fR password, all may not be lost.
Reboot the machine to single user mode (see page 157), and enter:
.Dx
# \f(CBmount -u /\f(CW			\fImount root file system read/write\f(CW
# \f(CBmount /usr\f(CW			\fImount /usr file system (if separate)\f(CW
# \f(CBpasswd root\f(CW			\fIchange the password for \f(CWroot
Enter new password:
Enter password again:
# \f(CB^D\f(CW				\fIenter ctrl-D to continue with startup\fR
.De
If you have a separate \fI/usr\fP\| file system (the normal case), you need to
mount it as well, since the \fIpasswd\fP\| program is in the directory
\fI/usr/bin\fP.  Note that you should explicitly state the name \f(CWroot\fR: in
single user mode, the system doesn't have the concept of user IDs.
.H3 "Page 148"
\fIReplace the text at the top of the page with\fP\|:
.P
Modern shells supply command line editing which resembles the editors \fIvi\fP\|
or \fIEmacs\fP.  In \fIbash\fP, \fIsh\fP, \fIksh\fP, and \fIzsh\fP\| you can
make the choice by entering
.H3 "Page 152"
\fIAfter figure 10-8, add the following text\fP\|:
.P
It would be tedious for every user to put settings in their private
initialization files, so the shells also read a system-wide default file.  For
the Bourne shell family, it is \fI/etc/profile\fP, while the C shell family has
three files: \fI/etc/csh.login\fP to be executed on login, \fI/etc/csh.cshrc\fP
to be executed when a new shell is started after you log in, and
\fI/etc/csh.logout\fP to be executed when you stop a shell.  The start files are
executed before the corresponding individual files.
.P
In addition, login classes (page 141) offer another method of setting
environment variables at a global level.
.H3 "Changing your shell"
.Pn chsh
.X "command, chsh"
.X "chsh, command"
The FreeBSD installation gives \f(CWroot\fP a C shell, \fIcsh\fP.  This is the
traditional Berkeley shell, but it has a number of disadvantages: command line
editing is very primitive, and the script language is significantly different
from that of the Bourne shell, which is the \fIde facto\fP\| standard for shell
scripts: if you stay with the C shell, you may still need to understand the
Bourne shell.  The latest version of the Bourne shell \fIsh\fP\| also includes
some command line editing.  See page 148 for details of how to enable it.
.P
You can get better command line editing with \fItcsh\fP, in the Ports
Collection.  You can get both better command line editing and Bourne shell
syntax with \fIbash\fP, also in the Ports Collection.
.P
If you have \f(CWroot\fP access, you can use \fIvipw\fP\| to change your shell,
but there's a more general way: use \fIchsh\fP\| (\fIChange Shell\fP\|).  Simply
run the program.  It starts your favourite editor (as defined by the
\f(CWEDITOR\fP environment variable).  Here's an example before:
.Dx
.X "Velte, Jack"
#Changing user database information for velte.
Shell: /bin/csh
Full Name: Jack Velte
Location: 
Office Phone: 
Home Phone: 
.De
You can change anything after the colons.  For example, you might change this
to:
.Dx
#Changing user database information for velte.
Shell: \f(CB/usr/local/bin/bash\fP
Full Name: Jack Velte
Location: \f(CBOn the road\fP
Office Phone: \f(CB+1-408-555-1999\fP
Home Phone: 
.De
\fIchsh\fP\| checks and updates the password files when you save the
modifications and exit the editor.  The next time you log in, you get the new
shell.  \fIchsh\fP\| tries to ensure you don't make any mistakes\(emfor example,
it won't let you enter the name of a shell which isn't mentioned in the file
\fI/etc/shells\fP\(embut it's a \fIvery\fP\| good idea to check the shell before
logging out.  You can try this with \fIsu\fP, which you normally use to become
super user:
.Dx
bumble# \f(CBsu velte\fP
Password:
su-2.00$				\fInote the new prompt\fP\|
.De
There are a couple of problems in using \fItcsh\fP\| or \fIbash\fP\| as a root
shell:
.Ls B
.LI
The shell for \f(CWroot\fP \fImust\fP\| be on the root file system, otherwise it
will not work in single user mode.  Unfortunately, most ports of shells put the
shell in the directory \fI/usr/local/bin\fP, which is almost never on the root
file system.
.LI
Most shells are \fIdynamically linked\fP\|: they rely on library routines in
files such as \fI/usr/lib/libc.a\fP.  These files are not available in single
user mode, so the shells won't work.  You can solve this problem by creating
\fIstatically linked\fP\| versions of the shell, but this requires programming
experience beyond the scope of this book.
.Le
If you can get hold of a statically linked version, perform the following steps
to install it:
.Ls B
.LI
Copy the shell to \fI/bin\fP, for example:
.Dx
# \f(CBcp /usr/local/bin/bash /bin\fP
.De
.sp -1v
.LI
Add the name of the shell to \fI/etc/shells\fP,  in this example the line in
\fBbold print\fP:
.Dx
# List of acceptable shells for chpass(1).
# Ftpd will not allow users to connect who are not using
# one of these shells.
/bin/sh
/bin/csh
\f(CB/bin/bash\fP
.De
.Le
You can then change the shell for \f(CWroot\fP as described above.
.P
Thanks to Lars K�ller \f(CW <Lars.Koeller@Uni-Bielefeld.DE>\fP
.\" <a href=mailto:Lars.Koeller@Uni-Bielefeld.DE>Lars K�ller</a> 
for drawing this to my attention.
.H3 "Page 160"
\fIReplace the text at the fourth bullet with the augmented text:\fR\|
.P
The second-level boot locates the kernel, by default the file \fI/kernel\fR\| on
the root file system, and loads it into memory.  It prints the \f(CWBoot:\fR
prompt at this point so that you can influence this choice\(emsee the man page
on page 579 for more details of what you can enter at this prompt.
.H3 "Page 169"
\fIReplace the last paragraph on the page with\fP\|:
.P
The standard solution for these problems is to relocate the \fI/tmp\fP\| file
system to a different directory, say \fI/usr/tmp\fP, and create a symbolic link
from \fI/usr/tmp\fP\| to \fI/tmp\fP\|\(emsee Chapter 4, \fIInstalling
FreeBSD\fP, page 72, for more details.
.P
Thanks to Charlie Sorsby \f(CW<crs@hgo.net>\fP for drawing this to my attention.
.H3 "Page 176"
\fIAdd the following paragraph\fP\|
.P
\s+3\fBUnmounting file systems\fR\s0
.P
When you mount a file system, the system assumes it is going to stay there, and
in the interests of efficiency it delays writing data back to the file system.
This is the same effect we discussed on page 158.  As a result, if you want to
stop using a file system, you need to tell the system about it.  You do this
with the \fIumount\fP\| command.  Note the spelling\(emthere's no \fBn\fP in the
command name.
.P
You need to do this even with read-only media such as CD-ROMs: the system
assumes it can access the data from a mounted file system, and it gets quite
unhappy if it can't.  Where possible, it locks removable media so that you can't
remove them from the device until you unmount them.
.P
Using \fIumount\fP\| is straightforward: just tell it what to unmount, either
the device name or the directory name.  For example, to unmount the CD-ROM we
mounted in the example above, you could enter one of these commands:
.Dx
# \f(CBumount /dev/cd1a\fP
# \f(CBumount /cd1\fP
.De
Before unmounting a file system, \fIumount\fP\| checks that nobody is using it.
If somebody is using it, it will refuse to unmount it with a message like
\f(CWumount: /cd1: Device busy\fP.  This message often occurs because you have
changed your directory to a directory on the file system you want to remove.
For example (which also shows the usefulness of having directory names in the
prompt):
.Dx
=== root@freebie (/dev/ttyp2) /cd1 16 -> \f(CBumount /cd1\fP
umount: /cd1: Device busy
=== root@freebie (/dev/ttyp2) /cd1 17 -> \f(CBcd\fP
=== root@freebie (/dev/ttyp2) ~ 18 -> \f(CBumount /cd1\fP
=== root@freebie (/dev/ttyp2) ~ 19 -> 
.De
\fIThanks to Ken Deboy
.Mailid glockr@locked_and_loaded.reno.nv.us
for pointing out this omission.\fR
.H3 "Page 180"
\fIThe example in the middle of the page should read:\fP\|
.P
For example, to generate a second set of 32 pseudo-terminals, enter:
.Dx
# \f(CBcd /dev\fP
# \f(CB./MAKEDEV pty1\fP
.De
You can generate up to 256 pseudo-terminals.  They are named \fIttyp0\fP\|
through \fIttypv\fP, \fIttyq0\fP\| through \fIttyqv\fP, \fIttyr0\fP\| through
\fIttyrv\fP, \fIttys0\fP\| through \fIttysv\fP, \fIttyP0\fP\| through
\fIttyPv\fP, \fIttyQ0\fP\| through \fIttyQv\fP, \fIttyR0\fP\| through
\fIttyRv\fP and \fIttyS0\fP\| through \fIttySv\fP.  To create each set of 32
terminals, use the number of the set: the first set is \fIpty0\fP, and the
eighth set is \fIpty7\fP.  Note that some processes, such as \fIxterm\fP, only
look at \fIttyp0\fP\| through \fIttysv\fP.
.P
\fIThanks to Karl Wagner \f(CW<karl@softronex.dynip.com>\fP for
pointing out this error.\fP

.H3 "Page 197, first line"
\fIThe text of the first full sentence reads\fR\|:
.P
The first name, up the the \f(CW\fP symbol, is the \fIlabel\fR\|.
.P
\fIIn fact, it should read\fR\|:
.P
The first name, up to the \f(CW|\fR symbol, is the \fIlabel\fR.
.P
.H3 "Page 208, middle of page"
The example shows the file name \fI/dev/rst0\fP\| when using the Bourne shell,
and \fI/dev/nrst0\fP\| when using C shell and friends.  This is inconsistent;
use \fI/dev/nrst0\fP\| with any shell if you want a non-rewinding tape, or
\fI/dev/rst0\fP\| if you want a rewinding tape.
.P
Thanks to Norman C Rice 
.Mailid nrice@emu.sourcee.com
for pointing out this one.
.H3 "Page 219"
\fIBefore the section \f(BITesting the spooler\fP add the following
section\fR\|:
.P
As we saw above, the line printer d�mon \fIlpd\fP\| is responsible for printing
spooled jobs.  By default it isn't started at boot time.  If you're
\f(CWroot\fP, you can start it by name:
.Dx
# \f(CBlpd\fP
.De
Normally, however, you will want it to be started automatically when the system
starts up.  You do this by setting the variable \f(CWlpd_enable\fP in
\fI/etc/rc.conf\fP\|:
.Dx
lpd_enable="YES"			# Run the line printer daemon
.De
See page \*[rc.conf] for more details of \fI/etc/rc.conf\fP.
.P
Another line in \fI/etc/rc.conf\fP\| refers to the line printer d�mon:
.Dx
lpd_flags=""		# Flags to lpd (if enabled).
.De
You don't normally need to change this line.  See the man page for \fIlpd\fP\|
for details of the flags.
.P
Thanks to Tommy G. James \f(CW<tgj@worldnet.att.net>\fP for bringing this to my
attention.
.H3 "Page 231"
\fIReplace the first line of the example with\fP\|:
.Dx
xhost presto bumble gw
.De
The original version allowed anybody on the Internet to access your system.
.P
Thanks to Jerry Dunham \f(CW<dunham@dunham.org>\fP for drawing this one to my
attention.

.H3 "Page 237"
\fIIn the section\fP\| Installing the sample desktop\fI, replace the first
paragraph with:\fP\|
.P
You'll find all the files described in this chapter on the first CD-ROM
(Installation CD-ROM) in the directory \fI\*[skelc]\fP.  Remember that you must
mount the CD-ROM before you can access the files\(emsee page 175 for further
details.  The individual scripts are in the directory \fI\*[skelc]/scripts\fP,
but you'll probably find it easier to install them with the script
\fIinstall-desktop\fP:
.P
Thanks to Chris Kaiser \f(CW<kaiserc@fltg.net>\fP for drawing this to my
attention.
.H3 "Page 242"
The instructions for extracting the source files from CD-ROM in the middle of
page 242 are incorrect.  You'll find the kernel sources on the first CD-ROM in
the directory \fI/src\fP.  Replace the example with:
.Dx
# \f(CBmkdir -p /usr/src/sys\f(CW
# \f(CBln -s /usr/src/sys /sys\f(CW
# \f(CBcd /\f(CW
# \f(CBcat /cdrom/src/ssys.[a-d]* | tar xzvf -\f(CW
.De
Thanks to Raymond Noel \f(CW<raynoel@videotron.ca>\fP, Suttipan Limanond
\f(CW<b0l6604@unix.tamu.edu>\fP and Satwant <wizkid11@xnet.com> for finding this
one in several small slices.
.H3 "Page 257"
\fIReplace the paragraph\fP\| Berkeley Packet Filter \fIwith\fP\|:
.P
.H4 "pseudo-device bpfilter"
.X "pseudo-device, bpfilter"
.X "bpf"
.X "Berkeley Packet Filter"
.X "command, tcpdump"
.X "tcpdump, command"
The \fIBerkeley Packet Filter\fP\| (\fIbpf\fP\|) allows you to capture packets
crossing a network interface to disk or to examine them with the \fItcpdump\fP
program.  Note that this capability represents a significant compromise of
network security.  The \fInumber\fP after bpfilter is the number of concurrent
processes that can use the facility.  Not all network interfaces support bpf.
.P
In order to use the Berkeley Packet Filter, you must also create the device
nodes \fI/dev/bpf0\fP\| to \fI/dev/bpf3\fP\| (if you're using the default number
4).  Currently, \fIMAKEDEV\fP\| doesn't help much\(emyou need to create each
device separately:
.Dx
# \f(CBcd /dev\fP
# \f(CB./MAKEDEV bpf0\fP
# \f(CB./MAKEDEV bpf1\fP
# \f(CB./MAKEDEV bpf2\fP
# \f(CB./MAKEDEV bpf3\fP
.De
Thanks to Christopher Raven \f(CW<c.raven@ukonline.co.uk>\fP for drawing this to
my attention.
.H3 "Page 264"
\fIIn the list of disk driver flags, add:\fP\|
.P
.Ls B
.LI
Bit 12 (\f(CW0x1000\fP) enables LBA (logical block addressing mode).  If this
bit is not set, the driver accesses the disk in CHS (cylinder/head/sector) mode.
.LI
In CHS mode, if bits 11 to 8 are not equal to 0, they specify the number of
heads to assume (between 1 and 15).  The driver recalculates the number of
cylinders to make up the total size of the disk.
.Le
.H3 "Page 273, ``Building the kernel''"
\fIReplace the example with:\fP\|
.P
Next, change to the build directory and build the kernel:
.Pn make-kernel
.Dx
# \f(CBcd ../../compile/FREEBIE\fP
# \f(CBmake depend\fP
# \f(CBmake\fP
.De
.sp -1v
.Aside
The \f(CWmake depend\fP is needed even if the directory has just been created:
apart from creating dependency information, it also creates some files needed
for the build.
.End-aside
Thanks to Mark Ovens \f(CW<marcov@globalnet.co.uk>\fP for drawing this to my
attention.
.H3 "Page 283, ``Creating the source tree''"
Add a third point to what you need to know:
.LB \\n(Pi 0 0 0 3.
.LI
Possibly, the date of the last update that you want to be included in the
checkout.  If you specify this date, \fIcvs\fR\| ignores any more recent
updates.  This option is often useful when somebody discovers a recently
introduced bug in -CURRENT: you check out the modules as they were before the
bug was introduced.  You specify the date with the \f(CW-D\fR option, for
example \f(CW-D "10 December 1997"\fR.
.Le
.H3 "Page 285, after the second example.
\fIAdd the text:\fR\|
.P
If you need to check out an older version, for example if there are problems
with the most recent version of -CURRENT, you could enter:
.Dx
# \f(CBcvs co  -D "10 December 1997" src/sys\fR
.De
This command checks out the kernel sources as of 10 December 1997.
.H3 "Page 294"
\fIAdd the following section\fP\|:
.H3 "Problems executing Linux binaries"
.X "branding ELF binarues"
.X "brandelf, command"
.X "command, brandelf"
.X "StarOffice, package"
One of the problems with the ELF format used by more recent Linux binaries is
that they usually contain no information to identify them as Linux binaries.
They might equally well be BSD/OS or UnixWare binaries.  That's not really a
problem at this point, since the only ELF format that FreeBSD \*[Fver]
understands is Linux, but FreeBSD-CURRENT recognizes a native FreeBSD ELF format
as well, and of course that's the default.  If you want to run a Linux ELF
binary on such a system, you must \fIbrand\fP\| the executable using the program
\fIbrandelf\fP.  For example, to brand the \fIStarOffice\fP\| program
\fIswriter3\fP, you would enter:
.Dx
# \f(CBbrandelf -t linux /usr/local/StarOffice-3.1/linux-x86/bin/swriter3\fP
.De
Thanks to Dan Busarow \f(CW<dan@dpcsys.com>\fP for bringing this to my
attention.
.H3 "Page 364, middle of page"
\fIChange the text from:\fR\|
.P
The names \f(CWMYADDR\fR and \f(CWHISADDR\fR are keywords which represent the
addresses at each end of the link.  They must be written as shown, though they
may be in lower case.
.P
\fIto\fR\|
.P
The names \f(CWMYADDR\fR and \f(CWHISADDR\fR are keywords which represent the
addresses at each end of the link.  They must be written as shown, though newer
versions of \fIppp\fR\| allow you to write them in lower case.  
.P
Thanks to Mark S. Reichman
.Mailid mark@fang.cs.sunyit.edu
for this correction.
.H3 "Page 368"
\fIReplace the paragraph after the second example with\fP\|:
.P
In FreeBSD version 3.0 and later, specify the options \f(CWPPP_BSDCOMP\fP and
\f(CWPPP_DEFLATE\fP to enable two kinds of compression.  You'll also need to
specify the corresponding option in Kernel PPP's configuration file.  These
options are not available in FreeBSD version 2.
.P
Thanks to Brian Somers \f(CW<brian@Awfulhak.org>\fP for this information.
.H3 "Page 397"
In the section ``Nicknames'', the example should read:
.Dx
www		IN	CNAME		freebie
ftp		IN	CNAME		presto
.De
In other words, there should be a space between \f(CWCNAME\fP and the system
name.
.H3 "Page 422"
\fIReplace the text above the example with\fP\|:
.P
\fItcpdump\fP\| is a program which monitors a network interface and displays
selected information which passes through it.  It uses the \fIBerkeley Packet
Filter\fP\| (\fIbpf\fP\|), an optional component of the kernel.  It is not
included in the \f(CWGENERIC\fP kernel: see page 257 for information on how to
configure it.
.P
If you don't configure the Berkeley Packet Filter, you will get a message like
.Dx
tcpdump: /dev/bpf0:  device not configured
.De
If you forget to create the devices for bpf, you will get a message like:
.Dx
tcpdump: /dev/bpf0: No such file or directory
.De
Since \fItcpdump\fP\| poses a potential security problem, you must be
\f(CWroot\fP in order to run it.  The simplest way to run it is without any
parameters.  This will cause \fItcpdump\fP\| to monitor and display all traffic
on the first active network interface, normally Ethernet:
.P
Thanks to Christopher Raven \f(CW<c.raven@ukonline.co.uk>\fP for drawing this to
my attention.
.H3 "Page 423"
\fIThe description at the top of the page incorrectly uses the term\fP\| IP
address\fI instead of\fP\| Ethernet address.  \fIIn addition, a page number
reference is incorrect.  Replace the paragraph with:\fP\|
.Ls B
.LI
Line 1 shows an \fIARP\fP\| request: system \fIpresto\fP\| is looking for the
Ethernet address of \fIwait\fP.  It would appear that \fIwait\fP\| is currently
not responding, since there is no reply.
.LI
Line 2 is not an IP message at all.  \fItcpdump\fP\| shows the Ethernet
addresses and the beginning of the packet.  We don't consider this kind of
request in this book.
.LI
.X "ntp"
Line 3 is a broadcast \fIntp\fP\| message.  We looked at \fIntp\fP\| on page
160.
.LI
Line 4 is another attempt by \fIpresto\fP\| to find the IP address of
\fIwait\fP.
.LI
.X "rwho, command"
.X "command, rwho"
Line 5 is a broadcast message from \fIbumble\fP\| on the \f(CWrwho\fP port,
giving information about its current load averages and how long it has been up.
See the man page for \fIrwho\fP\| on page 1167 for more information.
.LI
Line 6 is from a TCP connection between port 6000 on \fIfreebie\fP\| and port
1089 on \fIpresto\fP.  It is sending 384 bytes (with the sequence numbers
\f(CW536925467\fP to \f(CW536925851\fP; see page 305), and is
acknowledging that the last byte it received from \fIpresto\fP\| had the
sequence number \f(CW325114346\fP.  The window size is \f(CW17280\fP.
.LI
Line 7 is another ARP request.  \fIpresto\fP\| is looking for the Ethernet
address of \fIfreebie\fP.  How can that happen?  We've just seen that they have
a TCP connection.  In fact, ARP information expires after 20 minutes.  It's
quite possible that all connections between \fIpresto\fP\| and \fIfreebie\fP\|
have been dormant for this period, so \fIpresto\fP\| needs to find
\fIfreebie\fP\|'s IP address again.
.LI
Line 8 is the ARP reply from \fIfreebie\fP\| to \fIpresto\fP\| giving its
Ethernet address.
.LI
Line 9 shows a reply from \fIpresto\fP\| on the connection to \fIfreebie\fP\|
that we saw on line 6.  It acknowledges the data up to sequence number
\f(CW536925851\fP, but doesn't send any itself.
.LI
Line 10 shows another 448 bytes of data from \fIfreebie\fP\| to \fIpresto\fP,
and acknowledging the same sequence number from \fIpresto\fP\| as in line 6.
.Le
Thanks to \f(CWSergei S. Laskavy <laskavy@hedgehog.cs.msu.su>\fP for drawing
this to my attention.
.H3 "Page 450: anonymous \fIftp\fP"
\fIReplace the paragraph starting with \f(BICreate a user ftp\fR\|:
.P
Create a user \fIftp\fP, with the anonymous \fIftp\fP\| directory as the home
directory and the shell \fI/dev/null\fP.  Using \fI/dev/null\fP\| as the shell
makes it impossible to log in as user \fIftp\fP, but does not interfere with the
use of anonymous \fIftp\fP.  \fIftp\fP\| can be a member of group \fIbin\fP, or
you can create a new group \fIftp\fP\| by adding the group to \fI/etc/group\fP.
See page 138 for more details of adding users, and the man page on page 805 for
adding groups.
.P
Thanks to Mark S. Reichman \f(CW<mark@borg.com>\fP for drawing this to my
attention.
.H3 "Page 466, before the \fIps\fR\| example"
\fIAdd another bullet:\fR\|
.Ls B
.LI
Finally, you may find it convenient to let some other system handle all your
mail delivery for you: you just send anything you can't deliver locally to this
other host, which \fIsendmail\fR\| calls a \fIsmart host\fR.  This is
particularly convenient if you send your mail with \fIUUCP\fR.
.P
To tell \fIsendmail\fR\| to use a smart host (in our case,
\fImail.example.net\fR\|), find the following line in \fIsendmail.cf\fR\|:
.Dx
# "Smart" relay host (may be null)
DS
.De
\fIChange it to:\fR\|
.Dx
# "Smart" relay host (may be null)
DS\f(CBmail.example.net\fR
.De
.Le
.sp -1v
.H3 "Page 478, ``Running Apache''"
The text describes the location of the server as
\fI/usr/local/www/server/httpd\fR.  This appears to depend on where you get the
port from.  Some people report the file being at the more likely location
\fI/usr/local/sbin/httpd\fR\| (though note the directory \fIsbin\fR, not
\fIbin\fR\|).  Check both locations if you run into trouble.  Thanks to Sue
Blake 
.Mailid sue@welearn.com.au
for this information.
.H3 "Page 492"
Replace references to \fInmdb\fP\| with \fInmbd\fP.
.H3 "Page 493"
\fIReplace the last paragraph on the page with:\fP\|
.P
\f(CWsocket options\fP is hardly mentioned in the documentation, but it's very
important: many Microsoft implementations of TCP/IP are inefficient and
establish a new TCP more often than necessary.  Select the socket options
\f(CWTCP_NODELAY\fP and \f(CWIPTOS_LOWDELAY\fP, which can speed up the response
time of such applications by over 95%.
