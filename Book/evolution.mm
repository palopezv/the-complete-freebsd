.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: evolution.mm,v 4.13 2003/04/02 04:59:47 grog Exp grog $
.\"
.Appendix \*[nappthirded] "The evolution of FreeBSD"
FreeBSD has been around for ten years.  During this time, it has evolved
significantly, and it continues to evolve.  In this chapter we'll look at what
has changed, particularly in more recent times.  If you're planning to install
one of the older releases of FreeBSD, for example on old hardware that is too
small for modern releases, refer to \*[biblio], for copies of older editions of
this book.
.H2 "FreeBSD Releases 1 and 2"
Release 1.0 of FreeBSD appeared in December 1993.  It was substantially an
improved and debugged version of 386/BSD, based on the 4.3BSD Net/2 tape.
.Pn version2
FreeBSD Release 2 was released in January 1995.  The big difference from Release
1 was that it was based on 4.4BSD Lite, one of the results of the lawsuit we
discussed on page \*[unixwars].  There were no major differences from Release 1.
.H2 "FreeBSD Release 3"
.Pn version3
FreeBSD Release 3.0 was released in September 1998.  It represented the biggest
change in FreeBSD since the code base was moved to 4.4BSD.  A number of new
features were introduced, which made upgrading a little more complicated than is
normally the case.  In particular, the following new features were of note:
.Ls B
.LI
.X "AXP, processor"
.X "Alpha, architecture"
It introduced support for the Compaq/Digital Equipment \fIAXP\fP\/ (also known
as \fIALPHA\fP\/) processor.
.LI
On the Intel architecture, FreeBSD supported multiple processors.
.LI
.X "CAM driver"
A new SCSI driver, \fICAM\fP, was introduced.  This required some modifications
to the kernel configuration, and the device names changed.  At present this
means that FreeBSD device names are different from the NetBSD or OpenBSD names
for the same devices.  We'll look at CAM in more detail below.
.LI
The IDE driver first supported DMA.  We discussed DMA on page \*[use-DMA].  The
entire IDE driver was replaced in a later release.
.LI
A new console driver was introduced.
.LI
.X "loadable kernel module"
.X "kernel loadable module"
.X "kld"
This release of FreeBSD started phasing out \fIloadable kernel modules\fP,
described on page \*[Linux-emulation].  Since then, they have been replaced by
\fIkernel loadable modules\fP\/ (\fIkld\fP\/s).  Does this sound like word play?
Well, there's a solid technical background: you can tell the bootstrap to load
\fIkld\fP\/s along with the kernel.  We'll look at them below.
.LI
A new, more flexible bootstrap (the program that loads the kernel) was
introduced.
.LI
.X "a.out, object format"
.X "ELF, object format"
The default object file format changed from \fIa.out\fP\/ to \fIELF\fP.  FreeBSD
supported the ELF format for some time previously, initially to emulate Linux,
but now it is the native format as well.  FreeBSD still supports \fIa.out\fP\/
binaries.
.Le
.H2 "The CAM SCSI driver"
.Pn cam-driver
.X "common Access Method"
.X "CAM driver"
.X "driver, CAM"
FreeBSD Release 3.0 included a new SCSI driver, based on the ANSI ratified
\fICommon Access Method\fP\/ or \fICAM\fP\/ specification, which defines a
software interface for talking to SCSI and ATAPI devices.  The FreeBSD driver is
not completely CAM compliant, but it follows many of the precepts of CAM.  More
importantly, it addresses many of the shortcomings of the previous SCSI layer
and provides better performance and reliability, and eases the task of adding
support for new controllers.
.P
.X "SCSI, direct access device"
.X "SCSI, serial access"
.X "direct access device"
.X "serial access"
For most users, the most obvious difference between the old SCSI driver and CAM
is the way they named SCSI devices.  In the old driver, disks were called
\fIsd\s+2\f(BIn\s0\fR, and tapes were called \fIst\s+2\f(BIn\s0\fR, where
\s+2\f(BIn\s0\fP\/ was a small positive number.  The CAM driver calls disks
\fIda\s+2\f(BIn\s0\fR\/ (for \fIdirect access\fP\/), and tapes are called
\fIsa\s+2\f(BIn\s0\fR\/ (for \fIserial access\fP\/).
.P
In addition, a new program,
.Command camcontrol ,
enables you to administrate the SCSI chain at a more detailed level then
previously: for example, it is now possible to add devices to a chain after the
system has started.  See the man page for more details.
.H2 "Kernel loadable modules"
.Pn kld-changes
.X "loadable kernel module"
.X "LKM"
Older releases of FreeBSD supplied \fILoadable Kernel Modules\fP\/ or
\fILKM\fP\/s, object files that could be loaded and executed in the kernel
while the kernel was running.
.P
.X "kernel loadable module"
.X "kld"
The ELF kernel and the new bootstrap introduced with FreeBSD Release 3 allow you
to load additional modules at boot time.  To do so, however, the format of the
modules needed to be changed.  To avoid (too much) confusion, the name changed
from \fIloadable kernel module\fP\/ to \fIkernel loadable module\fP\/
(\fIkld\fP\/).
.br
.ne 1i
.Table-heading "Differences between LKMs and klds"
.TS H
tab(#) ;
 l | l | l .
Parameter#LKM#kld
_
.TH
Directory#\fI/lkm\fP\/#\fI/boot/kernel\fP\/
Load program#\fImodload\fP\/#\fIkldload\fP\/
Unload program#\fImodunload\fP\/#\fIkldunload\fP\/
List program#\fImodstat\fP\/#\fIkldstat\fP\/
_
.TE
.Tn kld-diffs
.sp 1.5v
Some other details have changed as well.
.Command kldload
.X "kld"
knows an internal path for finding \fIkld\fP\/s, so you don't need to specify
the path unless it's in a non-standard location.  It also assumes that the name
of the \fIkld\fP\/ ends in \fI.ko\fP, and you don't need to specify that either.
For example, to load the Linux emulator as an LKM, you entered:
.Dx
# \f(CBmodload /lkm/linux_mod.o\fP
.De
To load the \fIkld\fP, you enter:
.Dx
# \f(CBkldload linux\fP
.De
.Command kldload
searches for klds in a number of places.  Table
.Sref \*[kld-diffs] \&
shows the default path,
.Directory /boot/kernel .
If you boot from a different kernel, for example
.File /boot/kernel.old/kernel ,
the path will change to
.Directory /boot/kernel.old .
Up to Release 4 of FreeBSD, it searched
.Directory /modules
as well.  At the time of writing, this directory is still in the search path,
but it may be phased out.  It's a bad idea to store kernel code where it might
be loaded by different kernels.
.H2 "The ELF object format"
.Pn elf-binaries
.X "a.out, object format"
When UNIX was written, the world was simple.  The kernel of the Third Edition of
UNIX, in January 1973, had a little over 7,000 lines of code in total.  The
FreeBSD 5.0 kernel has approximately 300 times as much code.  The original UNIX
object format was correspondingly simple: it had provision for only three data
segments.  It was named after the name of the output from the assembler,
\fIa.out\fP.
.P
.X "COFF, object format"
.X "a.out, object format"
.X "ELF, object format"
.X "object format, COFF"
.X "object format, a.out"
.X "object format, ELF"
.ne 3v
In the course of time, binaries required additional features, in particular the
ability to link to dynamic libraries.  UNIX System V introduced a new object
file format, \fICOFF\fP, but BSD objected to some of the details of COFF and
remained with \fIa.out\fP\/ and used some rather dirty tricks to link to dynamic
libraries.  The change to \fIELF\fP\/ enabled a much cleaner interface.
.P
Since Release 3, FreeBSD uses \fIELF\fP\/ as the default executable format, but
the Intel port supported execution of \fIa.out\fP\/ binaries until Release 5.
The Alpha port was created after the change to \fIELF\fP\/ and does not support
\fIa.out\fP\/ at all.
.H3 "What happened to my libraries?"
.Pn dynamic-libraries
One detail of the change from \fIa.out\fP\/ to \fIELF\fP\/ can make life
difficult: ELF and \fIa.out\fP\/ executables need different libraries, each with
their own format, but frequently with the same name.  For example, the system
now knows the following versions of the standard C library, which is required by
every program:
.X "library files"
.Ls B
.LI
.File libc.a
is a static library used for including the library routines into the program at
link time.
.LI
.File -n libc_p.a
is a static library containing profiled versions of the library routines for
inclusion into the program at link time.
.LI
.File -n libc_pic.a
is a static library containing position-independent versions of the library
routines for inclusion into the program at link time.
.LI
.File -n libc_r.a
is a static library containing reentrant versions of the library routines for
inclusion into the program at link time.
.LI
.File -n libc.so
is a symbolic link to the current version of a dynamic library for linking at
run time.  This link is only used for ELF programs.
.LI
.File -n libc.so.3
is a version of an ELF dynamic library for linking at run time.  The number
\fI3\fP\/ changes with the release.
.LI
\fIlibc.so.3.1\fP\/ is a version of an \fIa.out\fP\/ dynamic library for linking
at run time.  The number \fI3.1\fP\/ changes with the release.
.Le
Don't worry if these names don't make much sense to you; unless you're writing
programs, all you need to know is that an ELF system uses
.File -n /usr/lib/libc.so
at run time.
.P
.Directory /usr/lib
contains a large number of libraries.  It would be possible, but messy, to find
an alternative arrangement for the name conflicts, and leave the rest of the
names unchanged.  Instead, the conversion process moves all \fIa.out\fP\/
libraries to a subdirectory \fIaout\fP, so an \fIa.out\fP\/ executable now looks
for
.File -n /usr/lib/aout/libc.so.3.0 .
An ELF executable looks for
.File -n /usr/lib/libc.so.3 .
.P
.X "hints file"
But how does the system know to look in a different place?  It uses a \fIhints
file\fP\/ generated by the
.Command ldconfig
program.  When the system starts, it takes a list of directory names from
.File /etc/rc.conf
and runs
.Command ldconfig
to search the directories for \fIa.out\fP\/ libraries and to generate the hints
file.  In Release 2 of FreeBSD, the standard
.File /etc/rc.conf
contained the following definition:
.Dx
ldconfig_paths="/usr/lib/compat /usr/X11R6/lib /usr/local/lib" # search paths
.De
In Release 3.0, this changed to:
.Dx
ldconfig_paths="/usr/lib/compat /usr/X11R6/lib /usr/local/lib"
                        # shared library search paths
ldconfig_paths_aout="/usr/lib/compat/aout /usr/X11R6/lib/aout /usr/local/lib/aout"
                        # a.out shared library search paths
.De
.H4 "Upgrading from Release 2 of FreeBSD"
If you're still using Release 2, you might run into some minor problems.  The
following discussion applies when upgrading to Release 3 or any later release:
part of the upgrade process from Release 2 to Release 3 changes this entry in
.File /etc/rc.conf ,
so there should be no problem with normal libraries.  A couple of problems may
still occur, however:
.Ls B
.LI
Some programs refer to library names that are symbolic links.  The upgrade
process doesn't always handle symbolic links correctly, so you may find that the
link points to the wrong place.  For example, you might have this in a 2.2.7
system
.Directory /usr/lib/compat \/:
.Dx
/usr/lib/compat:
total 1
-r--r--r--  1 root  wheel  8417 Jan 21 18:37 libgnumalloc.so.2.0
-r--r--r--  1 root  wheel  8398 Jan 21 18:37 libresolv.so.2.0
lrwxr-xr-x  1 root  wheel    31 Jan 21 18:36 libtermcap.so.3.0 -> /usr/lib/libte
rmcap.so.2.1
lrwxr-xr-x  1 root  wheel    31 Jan 21 18:36 libtermlib.so.3.0 -> /usr/lib/libte
rmlib.so.2.1
-r--r--r--  1 root  wheel  8437 Jan 21 18:37 liby.so.2.0
.De
After updating, you could end up with this:
.Dx
/usr/lib/compat/aout:
total 1
-r--r--r--  1 root  wheel  8417 Jan 21 18:37 libgnumalloc.so.2.0
-r--r--r--  1 root  wheel  8398 Jan 21 18:37 libresolv.so.2.0
lrwxr-xr-x  1 root  wheel    31 Jan 21 18:36 libtermcap.so.3.0 -> /usr/lib/libte
rmcap.so.2.1
lrwxr-xr-x  1 root  wheel    31 Jan 21 18:36 libtermlib.so.3.0 -> /usr/lib/libte
rmlib.so.2.1
-r--r--r--  1 root  wheel  8437 Jan 21 18:37 liby.so.2.0
.De
In other words, the libraries have been moved, but the symbolic links are
absolute and still point to the old place.  The system doesn't install absolute
symbolic links, so it doesn't make any attempt to correct them.  You need to fix
the problem manually.  In this example, we replace the symbolic links with
relative symbolic links:
.Dx
# \f(CBcd /usr/lib/compat/aout\fP
# \f(CBrm libtermcap.so.3.0\fP
# \f(CBln -s libtermcap.so.2.1 libtermcap.so.3.0\fP
# \f(CBrm libtermlib.so.3.0\fP
# \f(CBln -s libtermlib.so.2.1 libtermlib.so.3.0\fP
.De
.sp -1v
.LI
.ne 5v
If you have modified your
.File /etc/rc.conf
.X "a.out, object format"
significantly, the update may
fail, and your \fIa.out\fP\/ hints file will still point to the old locations.
In this case edit
.File /etc/rc.conf
as shown above.
.Dx
# \f(CBcd /usr/X11R6/lib\fP
# \f(CBmkdir aout\fP
# \f(CBcp -p lib* aout\fP
.De
.Le
.sp -1v
.H2 "FreeBSD Version 4"
FreeBSD Release 4.0 appeared in March 2000.  It included a number of significant
changes from Release 3.  At the time of writing, FreeBSD Release 4 is still a
current release, in parallel with Release 5.
.P
First, the good news: the differences between Release 3 and Release 4 aren't as
far-reaching or as complicated as the differences between Release 2 and Release
3.  Still, there are a couple of things that you need to know.  There are also
a few things that make installation easier.  You can get a blow-by-blow
description of the changes from the file
.File /usr/src/UPDATING .
This document
discusses the following more important new features:
.Ls B
.LI
From Release 4, FreeBSD no longer has block devices.  See page
.Sref \*[no-block-devices] \&
for more details.
.LI
.X "OpenSSH"
The base operating system now includes \fIOpenSSH\fP.  This may conflict with
the \fIports/security/ssh\fP\/ port: the base \fIOpenSSH\fP\/ is installed in
.Directory /usr/bin
and the port goes into
.Directory /usr/local/bin .
Most paths have
.Directory /usr/bin
in the path before
.Directory /usr/local/bin ,
so problems may arise.  If you don't want OpenSSH, add the following line to
.File /etc/make.conf \/:
.Dx
NO_OPENSSH=yes
.De
You will also need to enable OpenSSH in
.File /etc/rc.conf
if you want to run the new servers.  You may need to move your host key and
other config files from
.Directory /usr/local/etc
to
.Directory /etc/ssh .
.P
.X "OpenSSH"
\fIOpenSSH\fP\/ has different command line parsing, available options and
default settings from
.Command ssh ,
so you should take some care in its operation.  Perform a full audit of all
configuration settings.
.LI
\fIsendmail.cf\fP\/ has moved from
.File /etc/sendmail.cf
to
.File /etc/mail/sendmail.cf .
In addition to moving this file, you may need to
adjust
.File /etc/rc.conf .
.LI
.Daemon xntpd
has been updated to Revision 4.  The name of the daemon has changed from
.Daemon xntpd
to
.Daemon ntpd ,
so you may need to update your
.File /etc/rc.conf
file.  The
.File ntp.conf
files are compatible with the old release, unless you are using a local
reference clock.  You can find more details about \fIntp4\fP\/ at
\fIhttp://www.ntp.org/\fP.
.LI
There is a new driver for ATA (IDE) drives.  See page
.Sref \*[ata-page] \&
for more details.
.LI
Release 3 supported both the old and the new names for SCSI devices, for example
.Device sd0
and
.Device da0 .
The old names are no longer there in Release 4, so if you're upgrading you
should check your
.File /etc/fstab
and
.File /etc/rc.conf
and change the names where necessary.
.LI
.Command bad144
support for old WD and ESDI drives has been removed.
.LI
The \fImfs\fP\/ driver has been replaced with the \fImd\fP\/ driver.
Accordingly the \f(CWMFS_ROOT\fP and \f(CWMFS_ROOT_SIZE\fP kernel configuration
options have been replaced by \f(CWMD_ROOT\fP and \f(CWMD_ROOT_SIZE\fP.  See the
\fIGENERIC\fP\/ or \fILINT\fP\/ configuration files for more details.
.LI
Some Ethernet drivers no longer supports hard wired addresses in the config
file.  This is part of an on-going process to remove static hardware information
from the kernel and to enable learning it at boot time.
.LI
.File /var/cron/log
has been moved to
.File /var/log/cron
to get all the log files in one place.
.LI
User-visible TCP timers are now expressed in units of 1ms, instead of 500ms, so
if you've customized any timer values under \f(CWnet.inet.tcp\fP, multiply them
by 500 to preserve TCP's behavior.
.LI
The \fIbpfilter\fP\/ device has been renamed to \fIbpf\fP.
.LI
.X "vinum"
\fIVinum\fP\/ now supports a simplified interface.  See the man page
\fIvinum(8)\fP\/ for details.
.LI
A new driver, \fIida\fP, was introduced for the Compaq Smart Raid array.
.LI
The \fIlpt\fP\/ driver has been rewritten using \fIppbus\fP.  See
\fIppbus(4)\fP\/ for details.
.LI
Linux threads options has gone away (they are now standard in
the FreeBSD kernel).
.LI
.X "Pluggable Authentication Modules"
.X "PAM"
From Release 4, FreeBSD supports \fIPAM\fP\/ (\fIPluggable Authentication
Modules\fP\/).  This requires a new file
.File /etc/pam.conf .
If you don't have this (for
example, if you're upgrading from an older release of FreeBSD, and you don't
install the file), you'll get relatively harmless error messages.
.LI
For improved security, FreeBSD Release 4 runs
.Daemon named
as a new user and group, both called \f(CWbind\fP.
.LI
The floppy tape driver \fIft\fP\/ has been removed from the kernel.  There is no
replacement: this driver was always very non-standard, and the hardware that it
supports is unreliable and obsolete.
.LI
There are new keyboard and video card drivers.  We'll look at them in more
detail on page \*[new-console].
.Le
.sp -1v
.H2 "No more block devices"
.Pn no-block-devices
.X "block device"
.X "character device"
.X "raw disk"
From the beginnings of UNIX, users were confused by the fact that a disk drive
could appear in two different ways, either a \fIblock device\fP\/ or a
\fIcharacter device\fP, also called a \fIraw disk\fP.  For example, your root
partition might have been one of these:
.Dx
$ \f(CBls -l /dev/wd0a /dev/rwd0a\fP
crw-r-----  1 root  operator    3,   0 Oct 19  1997 /dev/rwd0a
brw-r-----  1 root  operator    0,   0 Oct 19  1997 /dev/wd0a
.De
A raw device always accesses the drive directly.  As a result, you're limited to
the way the drive is organized: the transfer must start on a sector\*F
.FS
Data on disk used to be stored in units called \fIsectors\fP\/.  Modern disks
store data in a number of different ways, but this is not visible outside the
drive.  The externally visible unit of data is still a sector of 512 bytes.
.FE
.X "buffer cache"
boundary and must be an integral number of sectors long.  By contrast, block
devices are \fIbuffered\fP\/: instead of accessing the disk directly, the system
transfers data via an area of memory called \fIbuffer cache\fP.  You access the
copy of the data in buffer cache.  This has the advantages that you can access
it much more quickly if it is in cache, and you don't have to pay any attention
to sector boundaries.  Still, having two different kinds of device is confusing,
and it's obvious why we should want to simplify things.
.P
But why are block devices going away, and not the raw disks?  Until recently,
for example, Linux didn't have any raw disk access, only block devices.  There
are a number of reasons to prefer to keep the raw disks:
.Ls B
.LI
If you want to access disks in an aligned fashion, it's faster: you don't have
to go via buffer cache.  This also saves memory.
.LI
If you have an error on a write to a raw device, you get an error indication
immediately.  On a block device, the error may not occur until after the process
has terminated, too late to try to recover.
.LI
The buffer cache isn't going away, only the device interface.  It's very seldom
that you'll find a need to access disk devices directly from user context.  The
most common access is via a file system or as swap.  In the former case, the
file system provides the buffering, and in the latter case it's
counterproductive, since swap always writes entire pages.  The only other access
to disk devices is from system programs like
.Command disklabel ,
.Command newfs
and
.Command mount ,
all of which have always accessed the raw device.
.Le
For most users, the biggest difference is that you will never use a name like
.Device rda0a
again; instead, it will become
.Device da0a .
If you are upgrading, you must run
.File /dev/MAKEDEV
to recreate the device nodes.
.P
Note that in Release 5 of FreeBSD,
.File /dev/MAKEDEV
is no longer needed.
.H2 "New ATA (IDE) disk driver"
.Pn ata-page
There is a new driver, \fIata\fP, for \fIATA\fP\/ (\fIAT attachment\fP\/)
drives, which were formerly called IDE.  It supports not only disks but also
ATAPI CD-ROM and DVD drives, ZIP drives and tape streamers.
.P
In the process, the name of the devices has changed: disk drives are now called
\fIad\fP, CD-ROM drives are called \fIacd\fP, LS-120 floppies are called
\fIafd\fP, and tapes are called \fIast\fP.
.P
For a transition period, the \fIwd\fP\/ driver remains available, but you
shouldn't use it unless you have very good reasons, for example if you have old
or unusual hardware that has trouble with the \fIad\fP\/ driver.
.H2 "New console driver"
.Pn new-console
FreeBSD Release 4 includes a new console driver.  The configuration file entries
have changed.  See the \f(CWGENERIC\fP configuration file for more details.
.H2 "FreeBSD Release 5"
FreeBSD Release 5 is the latest release of FreeBSD.  It has a number of new
features, most of which are transparent to the user.  There's a complete list in
the release notes, which you should certainly read if you're upgrading the
system, but here are some highlights:
.Ls B
.LI
SMP (symmetric multiprocessor) support has been rewritten from scratch.  This
will ultimately give much better performance and scalability, though currently
the performance potential has not been fully realized.  We looked at some of the
visible differences on page
.Sref \*[ps-command] .
.LI
.X "kqueue"
The \fIkqueue\fP\/ event notification facility is a new interface that is able
to replace \fIpoll\fP\/ and \fIselect\fP.  It offers improved performance as
well as the ability to report many different types of events.  Support for
monitoring changes in sockets, pipes, fifos, and files are present, as well as
for signals and processes.
.LI
A large number of kernel configuration options have been turned into boot-time
tunable variables, and the need to build specific kernels has become much more
seldom.
.LI
.X "Kernel-Scheduled Entity"
.X "KSE"
The \fIKernel-Scheduled Entity\fP\/ (\fIKSE\fP\/) project offers multi-threading
in the kernel.
.LI
Support for the 80386 processor has been removed from the \f(CWGENERIC\fP
kernel, as this code seriously pessimizes performance on other IA32 processors.
.P
The \f(CWI386_CPU\fP kernel option to support the 80386 processor is now
mutually exclusive with support for other IA32 processors; this should slightly
improve performance on the 80386 due to the elimination of run time processor
type checks.  Custom kernels that will run on the 80386 can still be built by
changing the cpu options in the kernel configuration file to only include
\f(CWI386_CPU\fP.
.P
.LI
Support has been added for 64 bit SPARC and IA 64 (Itanium) processors.
.LI
.X "device file system"
.X "devfs"
The system includes the \fIdevice file system\fP, or \fIdevfs\fP.  In older
releases of FreeBSD, as in other versions of UNIX, the directory
.Directory /dev
.X "device node"
contained \fIdevice nodes\fP, entries that looked like files but which in fact
described a possible device on the system.  The problem was that there was no
good way to keep the device nodes in sync with the kernel, and problems occurred
where the hardware corresponding to a device node didn't exist (a ``Device not
configured'' error), or where the device node corresponding to the hardware did
not exist (a ``no such file or directory'' error).  \fIdevfs\fP\/ solves this
problem by creating at boot time the device nodes for the hardware the system
finds.
.LI
.X "GEOM"
The disk I/O access system has been rearranged and made more flexible with the
\fIGEOM\fP\/ framework.
.LI
A number of file system enhancements have been made.  The standard UFS file
system now supports snapshots and background file system checking after a crash,
significantly reducing reboot time after a crash.
.LI
.X "UFS2"
UFS has been significantly enhanced as \fIUFS2\fP.  It supports files larger
than 1 TB and extended file attributes.
.LI
The PCMCIA code has been rewritten and now supports CardBus devices.
.LI
.X "a.out, object format"
The default kernel no longer supports \fIa.out\fP\/ file format.  You can still
execute these files by loading the \fIaout.ko\fP\/ KLD.
.LI
.X "Advanced Configuration and Power Interface"
.X "ACPI"
.X "APM"
FreeBSD now supports the \fIAdvanced Configuration and Power Interface\fP
(\fIACPI\fP\/), the replacement for \fIAPM\fP.
.LI
It is now possible to increase the size of ufs file systems with the
.Command growfs
command.
.LI
.X "vinum"
\fIVinum\fP\/ now supports the root file system.  See Chapter
.Sref \*[nchvinum] \&
for details.
.Le
