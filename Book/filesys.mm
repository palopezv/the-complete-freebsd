.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: filesys.mm,v 4.17 2003/04/02 06:43:57 grog Exp grog $
.\"
.Chapter \*[nchfilesys] "File systems and devices"
.X "file system"
.X "files"
.X "directories"
One of the most revolutionary concepts of the UNIX operating system was its
\fIfile system\fP, the way in which it stores data.  Although most other
operating systems have copied it since then, including Microsoft's platforms,
none have come close to the elegance with which it is implemented.  Many aspects
of the file system are not immediately obvious, some of them not even to
seasoned UNIX users.
.P
We've already looked at file naming conventions on page
.Sref \*[file-names] .
In the next section, we'll look at the file system access, structure and
hierarchy, and on page
.Sref \*[devices] \&
we'll look at how the file system treats hardware devices as files.
.H2 "File permissions"
.X "file permissions"
.X "permissions, file"
.Pn permissions
A UNIX system may potentially be used by many people, so UNIX includes a method
of protecting data from access by unauthorized persons.  Every file has three
items of information associated with it that describe who can access it in
what manner:
.Ls B
.LI
.X "file owner"
The \fIfile owner\fP, the user ID of the person who owns the file.
.LI
.X "file group"
The \fIfile group\fP, the group ID of the group that ``owns'' the file.
.LI
A list of what the owner, the group and other people can do with the file.  The
possible actions are reading, writing or executing.
.Le
.ne 2v
For example, you might have a program that accesses private data, and you want
to be sure that only you can execute it.  You do this by setting the permissions
so that only the owner can execute it.  Or you might have a text document in
development, and you want to be sure that you are the only person who can change
it.  On the other hand, the people who work with you have a need to be able to
refer to the document.  You set the permissions so that only the owner can write
it, that the owner and group can read it, and, because it's not ready for
publication yet, you don't allow anybody else to access it.
.P
Traditionally, the permissions are represented by three groups of \f(CWrwx\fP:
\f(CWr\fP stands for \fIread\fP\/ permission, \f(CWw\fP stands for \fIwrite\fP\/
permission, and \f(CWx\fP stands for \fIexecute\fP\/ permission.  The three
groups represent the permissions for the owner, the group and others
respectively.  If the permission is not granted, it is represented by a hyphen
(\f(CW-\fP).
.\" Hi, Jack :-)
.nh
Thus, the permissions for the program I discussed above would be
\f(CWr-x------\fP (I can read and execute the program, and nobody else can do
anything with it).  The permissions for the draft document would be
\f(CWrw-r-----\fP (I can read and write, the group can read, and others can't
access it).
.hy
.P
.X "~/.rhosts"
Typical FreeBSD file access permissions are \f(CWrwxr-xr-x\fP for programs and
\f(CWrw-r--r--\fP for other system files.  In some cases, however, you'll find
that other permissions are \fIrequired\fP.  For example, the file
.File ~/.rhosts ,
which is used by some network programs for user validation, may contain the
user's password in legible form.  To help ensure that other people don't read
it, the network programs refuse to read it unless its permissions are
\f(CWrw-------\fP.  The vast majority of system problems in UNIX can be traced
to incorrect permissions, so you should pay particular attention to them.
.P
.X "permission, setuid"
.X "permission, setgid"
.X "setuid"
.X "set user ID"
.X "setgid"
.X "set group"
Apart from these access permissions, executables can also have two bits set to
specify the access permissions of the process when it is run.  If the
\fIsetuid\fP\/ (\fIset user ID\fP\/) bit is set, the process always runs as if
it had been started by its owner.  If the \fIsetgid\fP\/ (\fIset group ID\fP\/)
bit is set, it runs as if it had been started by its group.  This is frequently
used to start system programs that need to access resources that the user may
not access directly.  We'll see an example of this with the
.Command ps
command on page
.Sref \*[ps-permissions] .
.Command ls
represents the \fIsetuid\fP\/ bit by setting the third letter of the permissions
string to \f(CWs\fP instead of \f(CWx\fP; similarly, it represents the
\fIsetgid\fP\/ bit by setting the sixth letter of the permissions string to
\f(CWs\fP instead of \f(CWx\fP.
.P
In addition to this access information, the permissions contain a character
that describes what kind of file it represents.  The first letter may be a
\f(CW-\fP (hyphen), which designates a regular file, the letter \f(CWd\fP for
directory, or the letters \f(CWb\fP or \f(CWc\fP for a device node.  We'll look
at device nodes in Chapter
.Sref "\*[nchdisks]" ,
page
.Sref \*[devices] .
There are also a number of other letters that are less used.  See the man page
\fIls(1)\fP\/ for a full list.
.P
To list files and show the permissions, use the
.Command ls
command with the \f(CW-l\fP option:
.Dx
$ \f(CBls -l\fP
total 2429
-rw-rw-r--   1 grog     wheel       28204 Jan  4 14:17 %backup%~
drwxrwxr-x   3 grog     wheel         512 Oct 11 15:26 2.1.0-951005-SNAP
drwx------   4 grog     wheel         512 Nov 25 17:23 Mail
-rw-rw-r--   1 grog     wheel         149 Dec  4 14:18 Makefile
-rw-rw-r--   1 grog     wheel         108 Dec  4 12:36 Makefile.bak
-rw-rw-r--   1 grog     wheel         108 Dec  4 12:36 Makefile~
-rw-rw-r--   1 grog     wheel           0 Dec  4 12:36 depend
-rw-rw-r--   1 daemon   wheel     1474560 Dec 14 17:03 deppert.floppy
-rwxr-xr-x   1 grog     wheel         100 Dec 19 15:24 doio
-rwxrwxr-x   1 grog     wheel         204 Dec 19 15:25 doiovm
-rwxrwxr-x   1 grog     wheel         204 Dec 19 15:16 doiovm~
-rwxr-xr-x   1 grog     wheel         115 Dec 26 08:42 dovm
-rwxr-xr-x   1 grog     wheel         114 Dec 19 15:30 dovm~
drwxr-xr-x   2 grog     wheel         512 Oct 16  1994 emacs
drwxrwxrwx   2 grog     wheel         512 Jan  3 14:07 letters
.De
This format shows the following information:
.Ls B
.LI
First, the permissions, which we've already discussed.
.LI
.X "link, count"
Then, the \fIlink count\fP.  This is the number of hard links to the file.  For
a regular file, this is normally 1, but directories have at least 2.  We look at
links on page
.Sref \*[links] .
.LI
.X "image, floppy"
.X "floppy image"
.X "Lincke, Roland"
Next come the names of the owner and the group, and the size of the file in
bytes.  You'll notice that the file
.File -n deppert.floppy
belongs to \f(CWdaemon\fP.  This was probably an accident, and it could lead to
problems.  Incidentally, looking at the name of the file and its size, it's
fairly obvious that this is an \fIimage\fP\/ of a 3\(12\f(CW"\fP floppy, that is
to say, a literal copy of the data on the complete floppy.
.LI
The date is normally the date that the file was last modified.  With the
\f(CW-u\fP option to
.Command ls ,
you can list the last time the file was accessed.
.LI
Finally comes the name of the file.  As you can see from this example, the names
can be quite varied.
.Le
A couple of the permissions are of interest.  The directories all have the
\f(CWx\fP (execute) permission bit set.  This enables accessing (i.e.  opening)
files in the directory\(emthat's the way the term \fIexecute\fP\/ is defined for
a directory.  If I reset the execute permission, I can still list the names of
the files, but I can't access them.
.P
I am the only person who can access the directory \fIMail\fP.  This is the
normal permission for a mail directory.
.H4 "Changing file permissions and owners"
Often enough, you may want to change file permissions or owners.  UNIX supplies
three programs to do this:
.Ls B
.LI
.X "deppert.floppy"
To change the file owner, use
.Command chown .
For example, to change the ownership of the file
.File -n deppert.floppy ,
which in the list above belongs to \f(CWdaemon\fP, \f(CWroot\fP would enter:
.Dx
# \f(CBchown grog deppert.floppy\fP
.De
Note that only \f(CWroot\fP may perform this operation.
.LI
To change the file group, use
.Command chgrp ,
which works in the same way as
.Command chown .
To change the group ownership to \fIlemis\fP, you would enter:
.Dx
# \f(CBchgrp lemis deppert.floppy\fP
.De
.Command chown
can also change both the owner and the group.  Instead of the two previous
examples, you could enter:
.Dx
# \f(CBchown grog:lemis deppert.floppy\fP
.De
This changes the owner to \f(CWgrog\fP, as before, and also changes the group to
\f(CWlemis\fP.
.LI
To change the permissions, use the
.Command chmod
program.
.Command chmod
has a number of different formats, but unfortunately the nine-character
representation isn't one of them.  Read the man page \fIchmod(1)\fP\/ for the
full story, but you can achieve just about anything you want with one of the
formats shown in table
.Sref "\*[chmod-codes]" :
.Table-heading "chmod permission codes"
.Tn chmod-codes
.TS H
tab(#) ;
lfCWp9 | lw55 .
\s10\fBSpecification#\fBEffect
_
.TH N
go-w#T{
Deny write permission to group and others
T}
.sp .4v
=rw,+X#T{
Set the read and write permissions to the usual defaults,
but retain any execute permissions that are currently set
T}
.sp .4v
+X#T{
Make a directory or file searchable/executable by everyone
if it is already searchable/executable by anyone
T}
.sp .4v
u=rwx,go=rx#T{
Make a file readable/executable by everyone and writable by
the owner only
T}
.sp .4v
go=#T{
Clear all mode bits for group and others
T}
.sp .4v
g=u-w#T{
Set the group bits equal to the user bits, but clear the group write bit
T}
.sp .4v
.\" .TH N
_
.TE
.Le
.H4 "Permissions for new files"
.Pn umask
.X "umask, command"
.X "command, umask"
.X "user, mask"
None of this tells us what the permissions for new files are going to be.  The
wrong choice could be disastrous.  For example, if files were automatically
created with the permissions \f(CWrwxrwxrwx\fP, anybody could access them in any
way.  On the other hand, creating them with \f(CWr--------\fP could result in a
lot of work setting them to what you really want them to be.  UNIX solves this
problem with a thing called \fIumask\fP\/ (\fIUser mask\fP\/).  This is a
default non-permission: it specifies which permission bits \fInot\fP\/ to allow.
.P
As if this weren't confusing enough, it's specified in the octal number system,
in which the valid digits are \f(CW0\fP to \f(CW7\fP.  Each octal digit
represents 3 bits.  By contrast, the more common hexadecimal system uses 16
digits, \f(CW0\fP to \f(CW9\fP and \f(CWa\fP to \f(CWf\fP.  The original
versions of UNIX ran on machines that used the octal number system, and since
the permissions come in threes, it made sense to leave the \fIumask\fP\/ value
in octal.
.P
An example: by default, you want to create files that anybody can read, but
only you can write.  You set the mask to \f(CW022\fP.  This corresponds to the
binary bit pattern \f(CW000010010\fP.
.Aside
The leading \f(CW0\fP is needed to specify that the number is in octal, not to
make up three digits.  If you want to set the permissions so that by default
nobody can read, you'd set it to \f(CW0222\fP.  Some shells automatically assume
that the number is octal, so you \fImay\fP\/ be able to omit the \f(CW0\fP, but
it's not good practice.
.End-aside
.ne 5v
The permissions are allowed where the corresponding bit is \f(CW0\fP:
.Dx
rwxrwxrwx               \fIPossible permissions\fP\/
000010010               \fIumask\fP\/
rwxr-xr-x               \fIresultant permissions\fP\/
.De
By default, files are created without the \f(CWx\fP bits, whereas directories
are created with the allowed \f(CWx\fP bits, so with this \fIumask\fP\/, a file
would be created with the permissions \f(CWrw-r--r--\fP.
.P
.Command umask
is a shell command.  To set it, just enter:
.Dx
$ \f(CBumask 022\fP
.De
It's preferable to set this in your shell initialization file\(emsee page
.Sref \*[bashrc] \&
for further details.
.P
Beware of creating a too restrictive umask.  For example, you will get into a
lot of trouble with a umask like \f(CW377\fP, which creates files that you can
only read, and that nobody else can access at all.  If you disallow the
\f(CWx\fP (executable) bit, you will not be able to access directories you
create, and you won't be able to run programs you compile.
.H4 "Making a program executable"
.Pn ps-permissions
File permissions enable one problem that occurs so often that it's worth drawing
attention to it.  Many operating systems require that an executable program have
a special naming convention, such as
.File -n COMMAND.COM
or
.File -n FOO.BAT ,
which in MS-DOS denotes a specific kind of binary executable and a script file,
respectively.  In UNIX, executable programs don't need a special suffix, but
they must have the \f(CWx\fP bit set.  Sometimes this bit gets reset (turned
off), for example if you copy it across the Net with
.Command ftp .
The result looks like this:
.Dx
$ \f(CBps\fP
bash: ps: Permission denied
$ \f(CBls -l /bin/ps\fP
-r--r--r--  1 bin  kmem  163840 May  6 06:02 /bin/ps
$ \f(CBsu\fP                                             \fIyou need to be super user to set ps permission\fP\/
Password:                                        \fIpassword doesn't echo\fP\/
# \f(CBchmod +x /bin/ps\fP                               \fImake it executable\fP\/
# \f(CBps\fP                                             \fInow it works\fP\/
  PID  TT  STAT      TIME COMMAND
  226  p2  S      0:00.56 su (bash)
  239  p2  R+     0:00.02 ps
  146  v1  Is+    0:00.06 /usr/libexec/getty Pc ttyv1
  147  v2  Is+    0:00.05 /usr/libexec/getty Pc ttyv2
# \f(CB^D\fP                                             \fIexit su\fP\/
$ \f(CBps\fP
ps: /dev/mem: Permission denied                  \fIhey! it's stopped working\fP\/
.De
Huh?  It only worked under
.Command su ,
and stopped working when I became a mere mortal again?  What's going on here?
.P
.X "setuid"
There's a second problem with programs like
.Command ps \/:
some versions need to be able to access special files, in this case
.Device mem ,
a special file that addresses the system memory.  To do this, we need to set
the \fIsetgid\fP\/ bit, \f(CWs\fP, which requires becoming superuser again:
.Dx
$ \f(CBsu\fP                                             \fIyou need to be super user to set ps permission\fP\/
Password:                                        \fIpassword doesn't echo\fP\/
# \f(CBchmod g+s /bin/ps\fP                              \fIset the setgid bit\fP\/
# \f(CBls -l /bin/ps\fP                                  \fIsee what it looks like\fP\/
-r-xr-sr-x  1 bin  kmem  163840 May  6 06:02 /bin/ps
# \f(CB^D\fP                                             \fIexit su\fP\/
$ \f(CBps\fP                                             \fInow it still works\fP\/
  PID  TT  STAT      TIME COMMAND
  226  p2  S      0:00.56 su (bash)
  239  p2  R+     0:00.02 ps
  146  v1  Is+    0:00.06 /usr/libexec/getty Pc ttyv1
  147  v2  Is+    0:00.05 /usr/libexec/getty Pc ttyv2
.De
In this example, the permissions in the final result really are the correct
permissions for
.Command ps .
It's impossible to go through the permissions for every standard program.  If
you suspect that you have the permissions set incorrectly, use the permissions
of the files on the Live Filesystem CD-ROM as a guideline.
.P
\fIsetuid\fP\/ and \fIsetgid\fP\/ programs can be a security issue.  What
happens if the program called
.Command ps
is really something else, a Trojan Horse?  We set the permissions to allow it to
break into the system.  As a result, FreeBSD has found an alternative method for
.Command ps
to do its work, and it no longer needs to be set
.Command setgid .
.H2 "Mandatory Access Control"
For some purposes, traditional UNIX permissions are insufficient.  Release 5.0
of FreeBSD introduces \fIMandatory Access Control\fP, or \fIMAC\fP, which
permits loadable kernel modules to augment the system security policy.  MAC is
intended as a toolkit for developing local and vendor security extensions, and
it includes a number of sample policy modules, including Multi-Level Security
(MLS) with compartments, and a number of augmented UNIX security models
including a file system firewall.  At the time of writing it is still considered
experimental software, so this book doesn't discuss it further.  See the man
pages for more details.
.H2 "Links"
.Pn links
.X "hard link"
.X "link, hard"
In UNIX, files are defined by \fIinodes\fP, structures on disk that you can't
access directly.  They contain the \fImetadata\fP, all the information about the
file, such as owner, permissions and timestamps.  What they don't contain are
the things you think of as making up a file: they don't have any data, and they
don't have names.  Instead, the inode contains information about where the data
blocks are located on the disk.  It doesn't know anything about the name: that's
the job of the directories.
.P
.ne 3v
A directory is simply a special kind of file that contains a list of names and
inode numbers: in other words, they assign a name to an inode, and thus to a
file.  More than one name can point to the same inode, so files can have more
than one name.  This connection between a name and an inode is called a
\fIlink\fP, sometimes confusingly \fIhard link\fP.  The inode numbers relate to
the file system, so files must be in the same file system as the directory that
refers to them.
.P
Directory entries are independent of each other: each points to the inode, so
they're completely equivalent.  The inode contains a \fIlink count\fP that keeps
track of how many directory entries point to it: when you remove the last entry,
the system deletes the file data and metadata.
.P
.X "symbolic link"
.X "soft link"
.X "link, symbolic"
.X "link, soft"
Alternatively, \fIsymbolic links\fP, sometimes called \fIsoft links\fP, are not
restricted to the same file system (not even to the same system!), and they
refer to another file name, not to the file itself.  The difference is most
evident if you delete a file: if the file has been hard linked, the other names
still exist and you can access the file by them.  If you delete a file name
that has a symbolic link pointing to it, the file goes away and the symbolic
link can't find it any more.
.P
It's not easy to decide which kind of link to use\(emsee \fIUNIX Power Tools\fP
(O'Reilly) for more details.
.H2 "Directory hierarchy"
.Pn directory-structure
Although Microsoft platforms have a hierarchical directory structure, there is
little standardization of the directory names: it's difficult to know where a
particular program or data file might be.  UNIX systems have a standard
directory hierarchy, though every vendor loves to change it just a little bit to
ensure that they're not absolutely compatible.  In the course of its evolution,
UNIX has changed its directory hierarchy several times.  It's still better than
the situation in the Microsoft world.  The most recent, and probably most
far-reaching changes, occurred over ten years ago with System V.4 and 4.4BSD,
both of which made almost identical changes.
.P
.X "root file system"
.X "file system, root"
.X "directory, /usr"
.X "directory, /"
Nearly every version of UNIX prefers to have at least two file systems,
.Directory -n /
(the \fIroot file system\fP\/) and
.Directory /usr ,
even if they only have a single disk.  This arrangement is considered more
reliable than a single file system: it's possible for a file system to crash so
badly that it can't be mounted any more, and you need to read in a tape backup,
or use programs like
.Command fsck
or
.Command fsdb
to piece them together.  We have already discussed this issue on page
.Sref \*[partition-size] ,
where I recommend having
.Directory /usr
on the same file system as
.Directory -n / .
.H3 "Standard directories"
The physical layout of the file systems does not affect the names or contents of
the directories, which are standardized.  Table
.Sref \*[hierarchy] \&
gives an overview
of the standard FreeBSD directories; see the man page \f(CWhier(7)\fP for more
details.
.P
.ne 10
.ds Section*title FreeBSD directory hierarchy
.Table-heading "FreeBSD directory hierarchy"
.Tn hierarchy
.TS H
tab(#) ;
 lfI | lw53  .
\fBDirectory
\fBname#\fBUsage
_
.TH N
.X "directory, /"
/#T{
Root file system.  Contains a couple of system directories and mount points for
other file systems.  It should not contain anything else.
T}
.sp .4v
.X "directory, /bin"
/bin#T{
Executable programs of general use needed at system startup time.  The
name was originally an abbreviation for \fIbinary\fP, but many of the files in
here are shell scripts.
T}
.sp .4v
.X "directory, /boot"
/boot#T{
Files used when booting the system, including the kernel and its associated
\fIklds\fP.
T}
.sp .4v
.X "directory, /cdrom"
/cdrom#T{
A mount point for CD-ROM drives.
T}
.sp .4v
.X "directory, /compat"
/compat#T{
A link to
.Directory /usr/compat \/:
see below.
T}
.sp .4v
.X "directory, /dev"
/dev#T{
Directory of device nodes.  The name is an abbreviation for \fIdevices\fP.  From
FreeBSD 5.0 onward, this is normally a mount point for the \fIdevice file
system\fP, \fIdevfs\fP.  We'll look at the contents of this directory in more
detail on page
.Sref \*[FreeBSD-devices] .
.X "directory, /etc"
.X "directory, /sbin"
T}
.sp .4v
.X "directory, /etc"
/etc#T{
Configuration files used at system startup.  Unlike System V,
.Directory /etc
does not contain kernel build files, which are not needed at system startup.
Unlike earlier UNIX versions, it also does not contain executables\(emthey have
been moved to
.Directory /sbin .
T}
.sp .4v
.X "directory, /home"
/home#T{
By convention, put user files here.  Despite the name,
.Directory /usr
is for system files.
T}
.sp .4v
.X "directory, /proc"
.X "process, directory"
.X "directory, process"
.X "directory, /lib"
.X "directory, /var"
.X "directory, /mnt"
/mnt#T{
A mount point for floppies and other temporary file systems.
T}
.sp .4v
.X "directory, /proc"
/proc#T{
.X "process, file system"
.X "file system, process"
The \fIprocess file system\fP.  This directory contains pseudo-files that refer
to the virtual memory of currently active processes.
T}
.sp .4v
.X "directory, /root"
/root#T{
The home directory of the user \f(CWroot\fP.  In traditional UNIX file
systems, \f(CWroot\fP's home directory was
.Directory -n / ,
but this is messy.
T}
.sp .4v
.X "directory, /sbin"
\fI/sbin#T{
System executables needed at system startup time.  These are typically system
administration files
that used to be stored in
.Directory /etc .
T}
.sp .4v
/sys#T{
If present, this is usually a symbolic link to
.Directory /usr/src/sys ,
the kernel sources.  This is a tradition derived from 4.3BSD.
T}
.sp .4v
/tmp#T{
A place for temporary files.  This directory is an anachronism: normally it is
on the root file system, though it is possible to mount it as a separate file
system or make it a symbolic link to
.Directory /var/tmp .
It is maintained mainly for programs that expect to find it.
T}
.sp .4v
.X "directory, /usr"
/usr#T{
The ``second file system.''  See the discussion above.
T}
.sp .4v
.X "directory, /usr/X11R6"
/usr/X11R6#T{
The X Window System.
T}
.sp .4v
.X "directory, /usr/X11R6/bin"
/usr/X11R6/bin#T{
Executable X11 programs.
T}
.sp .4v
.X "directory, /usr/X11R6/include"
/usr/X11R6/include#T{
Header files for X11 programming.
T}
.sp .4v
.X "directory, /usr/X11R6/lib"
/usr/X11R6/lib#T{
Library files for X11.
T}
.sp .4v
.X "directory, /usr/X11R6/man"
/usr/X11R6/man#T{
Man pages for X11.
T}
.sp .4v
.X "directory, /usr/bin"
/usr/bin#T{
Standard executable programs that are not needed at system start.  Most standard
programs you use are stored here.
T}
.sp .4v
.X "directory, /usr/compat"
/usr/compat#T{
A directory containing code for emulated systems, such as Linux.
T}
.sp .4v
.X "directory, /usr/games"
/usr/games#T{
Games.
T}
.sp .4v
.X "directory, /usr/include"
/usr/include#T{
Header files for programmers.
T}
.sp .4v
.X "directory, /usr/lib"
.X "directory, /lib"
/usr/lib#T{
Library files.  FreeBSD does not have a directory
.Directory /lib .
T}
.sp .4v
.X "directory, /usr/libexec"
/usr/libexec#T{
Executable files that are not started directly by the user, for example the
phases of the C compiler (which are started by
.File /usr/bin/gcc )
or the
.Command getty
program, which is started by
.Daemon init .
T}
.sp .4v
.X "directory, /usr/libdata"
/usr/libdata#T{
Miscellaneous files used by system utilities.
T}
.sp .4v
.X "directory, /usr/local"
/usr/local#T{
Additional programs that are not part of the operating system.  It parallels
the
.Directory /usr
directory in having subdirectories
.Directory -n bin ,
.Directory -n include ,
.Directory -n lib ,
.Directory -n man ,
.Directory -n sbin ,
and
.Directory -n share .
This is where you can put programs that you get from other sources.
T}
.sp .4v
.X "directory, /usr/obj"
/usr/obj#T{
Object files created when building the system.  See Chapter
.Sref "\*[nchbuild]".
T}
.sp .4v
.X "directory, /usr/ports"
/usr/ports#T{
The Ports Collection.
T}
.sp .4v
.X "directory, /usr/sbin"
/usr/sbin#T{
System administration programs that are not needed at system startup.
T}
.sp .4v
.X "directory, /usr/share"
/usr/share#T{
Miscellaneous read-only files, mainly informative.  Subdirectories include
.Directory -n doc ,
the FreeBSD documentation,
.Directory -n games ,
.Directory -n info ,
the GNU \fIinfo\fP\/ documentation,
.Directory -n locale ,
internationization information, and
.Directory -n man ,
the man pages.
T}
.sp .4v
.X "directory, /usr/src"
/usr/src#T{
System source files.
T}
.sp .4v
.X "directory, /var"
/var#T{
A file system for data that changes frequently, such as mail, news, and log
files.  If
.Directory /var
is not a separate file system, you should create a directory on another file
system and symlink
.Directory /var
to it.
T}
.sp .4v
.X "directory, /var/log"
/var/log#T{
Directory with system log files
T}
.sp .4v
.X "directory, /var/mail"
/var/mail#T{
Incoming mail for users on this system
T}
.sp .4v
.X "directory, /var/spool"
/var/spool#T{
Transient data, such as outgoing mail, print data and anonymous ftp.
T}
.sp .4v
.X "directory, /var/tmp"
/var/tmp#T{
Temporary files.
T}
_
.TE
.H2 "File system types"
.Pn fs-types
FreeBSD supports a number of file system types.  The most important are:
.Ls B
.LI
.X "UFS"
.X "ffs"
.X "UNIX File System"
.X "Fast File System"
\fIUFS\fP\/ is the \fIUNIX File System\fP.\*F
.FS
Paradoxically, although BSD may not be called UNIX, its file system \fIis\fP\/
called the UNIX File System.  The UNIX System Group, the developers of UNIX
System V.4, adopted UFS as the standard file system for System V and gave it
this name.  Previously it was called the Berkeley \fIFast File System\fP, or
\fIffs\fP.
.FE
All native disk file systems are of this type.  Since FreeBSD 5.0, you have a
choice of two different versions, \fIUFS 1\fP\/ and \fIUFS 2\fP.  As the names
suggest, UFS 2 is a successor to UFS 1.  Unlike UFS 1, UFS 2 file systems are
not limited to 1 TB (1024 GB) in size.  UFS 2 is relatively new, so unless you
require very large file systems, you should stick to UFS 1.
.LI
.Pn RockRidge
.X "cd9660, file system"
.X "Rock Ridge Extensions"
\fIcd9660\fP\/ is the ISO 9660 CD-ROM format with the so-called \fIRock Ridge
Extensions\fP\/ that enable UNIX-like file names to be used.  Use this file
system type for all CD-ROMs, even if they don't have the Rock Ridge Extensions.
.LI
.X "nfs"
.X "Network File System"
\fInfs\fP\/ is the \fINetwork File System\fP, a means of sharing file systems
across a network.  We'll look at it in Chapter
.Sref "\*[nchserver]" .
.LI
.Pn msdosfs
FreeBSD supports a number of file systems from other popular operating systems.
You mount the file systems with the
.Command mount
command and the \f(CW-t\fP option to specify the file system type.  For example:
.Dx
# \f(CBmount -t ext2fs /dev/da1s1 /linux\fP     \fImount a Linux ext2 file system\fP\/
# \f(CBmount -t msdos /dev/da2s1 /C:\fP         \fImount a Microsoft FAT file system\fP\/
.De
Here's a list of currently supported file systems:
.br
.ne 1i
.Table-heading "File system support"
.TS
tab(#) ;
l | lfCWp9 .
\fBFile system#\fBmount option
_
CD-ROM#cd9660
DVD#udf
Linux ext2#ext2fs
Microsoft MS-DOS#msdosfs
Microsoft NT#ntfs
Novell Netware#nwfs
Microsoft CIFS#smbfs
.TE
.Le
.SPUP
.H3 "Soft updates"
.Pn soft-updates
Soft updates change the way the file system performs I/O.  They enable
\fImetadata\fP\/ to be written less frequently.  This can give rise to dramatic
performance improvements under certain circumstances, such as file deletion.
Specify soft updates with the \f(CW-U\fP option when creating the file system.
For example:
.Dx
# \f(CBnewfs -U /dev/da1s2h\fP
.De
If you forget this flag, you can enable them later with
.Command tunefs \/:
.Dx
# \f(CBtunefs -n enable /dev/da1s2h\fP
.De
You can't perform this operation on a mounted file system.
.H3 "Snapshots"
One of the problems with backing up file systems is that you don't get a
consistent view of the file system: while you copy a file, other programs may be
modifying it, so what you get on the tape is not an accurate view of the file at
any time.  \fISnapshots\fP\/ are a method to create a unified view of a file
system.  They maintain a relatively small file in the file system itself
containing information on what has changed since the snapshot was taken.  When
you access the snapshot, you get this data rather than the current data for the
parts of the disk which have changed, so you get a view of the file system as it
was at the time of the snapshot.
.H4 "Creating snapshots"
You create  snapshots with the
.Command mount
command and the \f(CW-o snapshot\fP option.  For example, you could enter
.Dx
# \f(CBmount -u -o snapshot /var/snapshot/snap1 /var\fP
.De
This command creates a snapshot of the
.Directory /var
file system called
.File /var/snapshot/snap1 .
.P
.ne 4v
Snapshot files have some interesting properties:
.Ls B
.LI
You can have multiple snapshots on a file system, up to the current limit of 20.
.LI
Snapshots have the \f(CWschg\fP flag set, which prevents anybody writing to them.
.LI
Despite the \f(CWschg\fP flag, you can still remove them.
.LI
They are automatically updated when anything is written to the file system.  The
view of the file system doesn't change, but this update is necessary in order to
maintain the ``old'' view of the file system.
.LI
They look like normal file systems.  You can mount them with the \fImd\fP\/
driver.  We'll look at that on page
.Sref \*[vnode-device] .
.Le
Probably the most useful thing you can do with a snapshot is to take a backup of
it.  We'll look at backups on page
.Sref \*[backups] .
.P
At the time of writing, snapshots are still under development.  It's possible
that you might still have trouble with them, in particular with deadlocks that
can only be cleared by rebooting.
.P
It takes about 30 seconds to create a snapshot of an 8 GB file system.  During
the last five seconds, file system activity is suspended.  If there's a lot of
soft update activity going on in the file system (for example, when deleting a
lot of files), this suspension time can become much longer, up to several
minutes.  To remove the same snapshot takes about two minutes, but it doesn't
suspend file system activity at all.
.SPUP
.H2 "Mounting file systems"
.Pn mount
.X "file system, root"
.X "root file system"
.X "mounting"
.X "CD-ROM, mounting"
Microsoft platforms identify partitions by letters that are assigned at boot
time.  There is no obvious relation between the partitions, and you have little
control over the way the system assigns them.  By contrast, all UNIX partitions
have a specific relation to the \fIroot file system\fP, which is called simply
.Directory -n / .
This flexibility has one problem: you have the choice of where in the overall
file system structure you put your individual file systems.  You specify the
location with the
.Command mount
command.  For example, you would typically mount a CD-ROM in the directory
.Directory /cdrom ,
but if you have three CD-ROM drives attached to your SCSI controller, you might
prefer to mount them in the directories
.Directory -n /cd0 ,
.Directory -n /cd1 ,
and
.Directory -n /cd2 .
To mount a file system, you need to specify the device to be mounted, where it
is to be mounted, and the type of file system (unless it is ufs).  The \fImount
point\fP, (the directory where it is to be mounted) must already exist.  To
mount your second CD-ROM on
.Directory -n /cd1 ,
you enter:
.Dx
# \f(CBmkdir /cd1\fP                                    \fIonly if it doesn't exist\fP\/
# \f(CBmount -t cd9660 -o ro /dev/cd1a /cd1\fP
.De
When the system boots, it calls the startup script
.File /etc/rc ,
which among other things automatically mounts the file systems.  All you need to
do is to supply the information: what is to be mounted, and where?  This is in
the file
.File /etc/fstab .
If you come from a System V environment, you'll notice significant difference in
format\(emsee the man page \fIfstab(5)\fP\/ for the full story.  A typical
.File /etc/fstab
might look like:
.Dx
/dev/ad0s1a  /                   ufs         rw  1 1  \fIroot file system\fP\/
/dev/ad0s1b  none                swap        sw  0 0  \fIswap\fP\/
/dev/ad0s1e  /usr                ufs         rw  2 2  \fI/usr file system\fP\/
/dev/da1s1e  /src                ufs         rw  2 2  \fIadditional file system\fP\/
/dev/da2s1   /linux              ext2fs      rw  2 2  \fILinux file system\fP\/
/dev/ad1s1   /C:                 msdos       rw  2 2  \fIMicrosoft file system\fP\/
proc         /proc               procfs      rw  0 0  \fIproc pseudo-file system\fP\/
linproc      /compat/linux/proc  linprocfs   rw  0 0
/dev/cd0a    /cdrom              cd9660      ro  0 0  \fICD-ROM\fP\/
presto:/     /presto/root        nfs         rw  0 0  \fINFS file systems on other systems\fP\/
presto:/usr  /presto/usr         nfs         rw  0 0
presto:/home /presto/home        nfs         rw  0 0
presto:/S    /S                  nfs         rw  0 0
//guest@samba/public  /smb       smbfs       rw,noauto  0 0  \fISMB file system\fP\/
.De
.ne 5v
The format of the file is reasonably straightforward:
.Ls B
.LI
The first column gives the name of the device (if it's a real file system), a
keyword for some file systems, like \f(CWproc\fP, or the name of the remote file
system for NFS mounts.
.LI
The second column specifies the mount point.  Swap partitions don't have a mount
point, so the mount point for the swap partition is specified as \f(CWnone\fP.
.LI
The third column specifies the type of file system.  Local file systems on hard
disk are always \fIufs\fP, and file systems on CD-ROM are \fIcd9660\fP.  Remote
file systems are always \fInfs\fP\/.  Specify swap partitions with \fIswap\fP,
and the \fIproc\fP\/ file system with \fIproc\fP\/.
.LI
The fourth column contains \f(CWrw\fP for file systems that can be read or
written, \f(CWro\fP for file systems (like CD-ROM) that can only be read, and
\f(CWsw\fP for swap partitions.  It can also contain options like the
\f(CWnoauto\fP in the bottom line, which tells the system startup scripts to
ignore the line.  It's there so that you can use the shorthand notation
\f(CWmount /smb\fP when you want to mount the file system.
.LI
The fifth and sixth columns are used by the \fIdump\fP\/ and \f(CWfsck\fP
programs.  You won't normally need to change them.  Enter \f(CW1\fP for a root
file system, \f(CW2\fP for other UFS file systems, and 0 for everything else.
.Le
.H3 "Mounting files as file systems"
.X "loop mount"
.X "mount, loop"
.Pn vnode-device
So far, our files have all been on devices, also called \fIspecial files\fP.
Sometimes, though, you may want to access the contents of a file as a file
system:
.Ls B
.LI
It's sometimes of interest to access the contents of a snapshot, for example to
check the contents.
.LI
After creating an ISO image to burn on CD-R, you should check that it's valid.
.LI
Also, after downloading an ISO image from the Net, you may just want to access
the contents, and not create a CD-R at all.
.Le
In each case, the solution is the same: you mount the files as a \fIvnode\fP\/
device with the \fImd\fP\/ driver.
.P
The \fImd\fP\/ driver creates a number of different kinds of pseudo-device.  See
the man page \fImd(4)\fP.  We use the \fIvnode\fP\/ device, a special file that
refers to file system files.  Support for \fImd\fP\/ is included in the GENERIC
kernel, but if you've built a kernel without the \fImd\fP\/ drive, you can load
it as a \fIkld\fP.  If you're not sure, try loading the kld anyway.
.P
In the following example, we associate a vnode device with the ISO image
.File -n iso-image
using the program
.Command mdconfig \/:
.Dx
# \f(CBkldload md\fP                            \fIload the kld module if necessary\fP\/
kldload: can't load md: File exists     \fIalready loaded or in the kernel\fP\/
# \f(CBmdconfig -a -t vnode -f iso-image\fP     \fIand configure the device\fP\/
md0                                     \fIthis is the name assigned in directory /dev\fP\/
# \f(CBmount -t cd9660 /dev/md0 /mnt\fP         \fIthen mount it\fP\/
.De
.ne 5v
After this, you can access the image at
.Directory /mnt
as a normal file system.  You specify \f(CW-t cd9660\fP in this case because the
file system on the image is a CD9660 file system.  You don't specify this if
you're mounting a UFS file system, for example a snapshot image.
.P
Older versions of FreeBSD used the \fIvn\fP\/ driver, which used different
syntax.  Linux uses \fIloop mounts\fP, which FreeBSD doesn't support.
.H3 "Unmounting file systems"
When you mount a file system, the system assumes it is going to stay there, and
in the interests of efficiency it delays writing data back to the file system.
This is also the reason why you can't just turn the power off when you shut down
the system.  If you want to stop using a file system, you must tell the system
about it so that it can flush any remaining data.  You do this with the
.Command umount
command.  Note the spelling of this command\(emthere's no \fBn\fP in the command
name.
.P
You need to do this even with read-only media such as CD-ROMs: the system
assumes it can access the data from a mounted file system, and it gets quite
unhappy if it can't.  Where possible, it locks removable media so that you can't
remove them from the device until you unmount them.
.P
Using
.Command umount
is straightforward: just tell it what to unmount, either the device name or the
directory name.  For example, to unmount the CD-ROM we mounted in the example
above, you could enter one of these commands:
.Dx
# \f(CBumount /dev/cd1a\fP
# \f(CBumount /cd1\fP
.De
Before unmounting a file system,
.Command umount
checks that nobody is using it.  If somebody is using it, it refuses to unmount
it with a message like \f(CWumount: /cd1: Device busy\fP.  This message often
occurs because you have changed your directory to a directory on the file system
you want to remove.  For example (which also shows the usefulness of having
directory names in the prompt):
.Dx
=== root@freebie (/dev/ttyp2) /cd1 16 -> \f(CBumount /cd1\fP
umount: /cd1: Device busy
=== root@freebie (/dev/ttyp2) /cd1 17 -> \f(CBcd\fP
=== root@freebie (/dev/ttyp2) ~ 18 -> \f(CBumount /cd1\fP
=== root@freebie (/dev/ttyp2) ~ 19 ->
.De
.ne 3v
After unmounting a vnode file system, don't forget to unconfigure the file:
.Dx
# \f(CBumount /mnt\fP
# \f(CBmdconfig -d -u 0\fP
.De
The parameter \f(CW0\fP refers to \fImd\fP\/ device 0, in other words
.Device md0 .
.H2 "FreeBSD devices"
.Pn devices
UNIX refers to devices in the same manner as it refers to normal files.  By
contrast to normal (``regular'') files, they are called \fIspecial files\fP.
They're not really files at all: they're information about device support in the
kernel, and the term \fIdevice node\fP\/ is more accurate.  Conventionally, they
are stored in the directory
.Directory /dev .
Some devices don't have device nodes, for example Ethernet interfaces: they are
treated differently by the
.Command ifconfig
program.
.P
Traditional UNIX systems distinguish two types of device, \fIblock devices\fP\/
and \fIcharacter devices\fP.  FreeBSD no longer has block devices; we discussed
the reasons for this on page
.Sref \*[block-device] .
.P
In traditional UNIX systems, including FreeBSD up to Release 4, it was necessary
to create device nodes manually.  This caused a number of problems when they
didn't match what was in the system.  Release 5 of FreeBSD has solved this
problem with the \fIdevice file system\fP, also known as \fIdevfs\fP.
\fIdevfs\fP\/ is a pseudo-file system that dynamically creates device nodes for
exactly those devices that are in the kernel, which makes it unnecessary to
manually create devices.
.H3 "Overview of FreeBSD devices"
.Pn FreeBSD-devices
Every UNIX system has its own peculiarities when it comes to device names and
usage.  Even if you're used to UNIX, you'll find the following table useful.
.ne 1i
.X "device, overview"
.Table-heading "FreeBSD device names"
.Tn FreeBSD-devices-table
.TS H
tab(#) ;
lfCWp9 | lw60 .
\s10\fBDevice#\fBDescription
_
.TH N
.\" XXX redo this if there's time
.sp .3v
acd0#T{
First ata (IDE) CD-ROM drive.
T}
.sp .3v
ad0#T{
First ata (IDE or similar) disk drive.  See Chapter
.Sref "\*[nchconcepts]" ,
page
.Sref \*[disk-partitions] ,
for a complete list of disk drive names.
T}
.X "fd/0, device"
.X "device, fd/0"
.X "fd0a, device"
.X "device, fd0a"
.X "fd0c, device"
.X "device, fd0c"
.X "bpf0, device"
.X "device, bpf0"
.sp .3v
bpf0#T{
Berkeley packet filter.
T}
.sp .3v
.X "cd0, device"
.X "device, cd0a"
cd0#T{
First SCSI CD-ROM drive.
T}
.sp .3v
.X "ch0, device"
.X "device, ch0"
ch0#T{
SCSI CD-ROM changer (juke box)
T}
.sp .3v
.X "console, device"
.X "device, console"
console#T{
System console, the device that receives console messages.  Initially it is
.Device ttyv0 ,
but it can be changed.
T}
.sp .3v
.X "cuaa0, device"
.X "device, cuaa0"
cuaa0#T{
First serial port in callout mode.
T}
.sp .3v
.X "cuaia0, device"
.X "device, cuaia0"
cuaia0#T{
First serial port in callout mode, initial state.  Note the letter \f(CWi\fP for
\fIinitial\fP.
T}
.sp .3v
.X "cuala0, device"
.X "device, cuala0"
cuala0#T{
First serial port in callout mode, lock state.  Note the letter \f(CWl\fP for
\fIlock\fP.
T}
.sp .3v
.X "da0, device"
.X "device, da0"
da0#T{
First SCSI disk drive.  See Chapter
.Sref "\*[nchconcepts]" ,
page
.Sref \*[disk-partitions] ,
for a complete list of disk drive names.
T}
.sp .3v
.X "esa0, device"
.X "device, esa0"
esa0#T{
First SCSI tape drive, eject on close mode.
T}
.sp .3v
.X "fd, device"
.X "device, fd"
fd#T{
File descriptor pseudo-devices: a directory containing pseudo-devices that,
when opened, return a duplicate of the file descriptor with the same number.
For example, if you open
.Device fd/0 ,
you get another handle on your
\fIstdin\fP\/ stream (file descriptor 0).
T}
.sp .3v
.X "fd0, device"
.X "device, fd0"
fd0#T{
The first floppy disk drive, accessed as a file system.
T}
.sp .3v
.\" io
.\" klog
.X "kmem, device"
.X "device, kmem"
kmem#T{
Kernel virtual memory pseudo-device.
T}
.sp .3v
.X "lpt0, device"
.X "device, lpt0"
lpt0#T{
First parallel printer.
T}
.sp .3v
.X "mem, device"
.X "device, mem"
mem#T{
Physical virtual memory pseudo-device.
T}
.sp .3v
.X "nsa0, device"
.X "device, nsa0"
nsa0#T{
First SCSI tape drive, no-rewind mode.
T}
.sp .3v
.X "null, device"
.X "device, null"
null#T{
The ``bit bucket.''  Send data to this device if you never want to see it
again.
T}
.sp .3v
.X "psm0, device"
.X "device, psm0"
psm0#T{
PS/2 mouse.
T}
.sp .3v
.X "ptyp0, device"
.X "device, ptyp0"
ptyp0#T{
First master pseudo-terminal.  Master pseudo-terminals are named
.Device -n ptyp0
through
.Device -n ptypv ,
.Device -n ptyq0
through
.Device -n ptyqv ,
.Device -n ptyr0
through
.Device -n ptyrv ,
.Device -n ptys0
through
.Device -n ptysv ,
.Device -n ptyP0
through
.Device -n ptyPv ,
.Device -n ptyQ0
through
.Device -n ptyQv ,
.Device -n ptyR0
through
.Device -n ptyRv
and
.Device -n ptyS0
through
.Device -n ptySv .
T}
.sp .3v
.X "random, device"
.X "device, random"
random#T{
Random number generator.
T}
.sp .3v
.Pn sa0
.X "sa0, device"
.X "device, sa0"
sa0#T{
First SCSI tape drive, rewind on close mode.
T}
.sp .3v
.X "sysmouse, device"
.X "device, sysmouse"
sysmouse#T{
System mouse, controlled by
.Daemon moused .
We'll look at this again on page
.Sref \*[XF86Config-InputDevice-section] .
T}
.sp .3v
.X "tty, device"
.X "device, tty"
tty#T{
Current controlling terminal.
T}
.sp .3v
.X "ttyd0, device"
.X "device, ttyd0"
ttyd0#T{
First serial port in callin mode.
T}
.sp .3v
.X "ttyid0, device"
.X "device, ttyid0"
ttyid0#T{
First serial port in callin mode, initial state.
T}
.sp .3v
.X "ttyld0, device"
.X "device, ttyld0"
ttyld0#T{
First serial port in callin mode, lock state.
T}
.sp .3v
.Pn pty-devname
.X "ttyp0, device"
.X "device, ttyp0"
ttyp0#T{
First slave pseudo-terminal.  Slave pseudo-terminals are named
.Device -n ttyp0
through
.Device -n ttypv ,
.Device -n ttyq0
through
.Device -n ttyqv ,
.Device -n ttyr0
through
.Device -n ttyrv ,
.Device -n ttys0
through
.Device -n ttysv ,
.Device -n ttyP0
through
.Device -n ttyPv ,
.Device -n ttyQ0
through
.Device -n ttyQv ,
.Device -n ttyR0
through
.Device -n ttyRv
and
.Device -n ttyS0
through
.Device -n ttySv .
Some processes, such as
.Command -n xterm ,
only look at
.Device -n ttyp0
through
.Device -n ttysv .
T}
.sp .3v
.X "ttyv0, device"
.X "device, ttyv0"
ttyv0#T{
First virtual tty.  This is the display with which the system starts.  Up to 10
virtual ttys can be activated by adding the appropriate
.Command getty
information in the file
.File /etc/ttys .
See Chapter
.Sref "\*[nchmodems]" ,
page
.Sref \*[dialin] ,
for
further details.
T}
.sp .3v
ugen0#T{
First generic USB device.
T}
.sp .3v
ukbd0#T{
First USB keyboard.
T}
.sp .3v
ulpt0#T{
First USB printer.
T}
.sp .3v
umass0#T{
First USB mass storage device.
T}
.sp .3v
ums0#T{
First USB mouse.
T}
.sp .3v
uscanner0#T{
First USB scanner.
T}
.sp .3v
vinum#T{
Directory for Vinum device nodes.  See Chapter
.Sref "\*[nchvinum]" ,
for further details.
T}
.sp .3v
.X "zero, device"
.X "device, zero"
zero#T{
Dummy device that always returns the value (binary) \f(CW0\fP when read.
T}
_
.TE
.sp 1.5v
You'll note a number of different modes associated with the serial ports.  We'll
look at them again in Chapter
.Sref "\*[nchmodems]" .
.H2 "Virtual terminals"
.Pn vt
.X "virtual terminal"
.X "terminal, virtual"
As we have seen, UNIX is a multitasking operating system, but a PC generally
only has one screen.  FreeBSD solves this problem with \fIvirtual
terminals\fP.  When in text mode, you can change between up to 16 different
screens with the combination of the \fBAlt\fP key and a function key.  The
devices are named
.Device ttyv0
through
.Device ttyv15 ,
and correspond to the keystrokes \fBAlt-F1\fP through \fBAlt-F16\fP.  By
default, three virtual terminals are active:
.Device ttyv0
through
.Device ttyv2 .
The system console is the virtual terminal
.Device ttyv0 ,
and that's what you see when you boot the machine.  To
activate additional virtual terminals, edit the file
.File /etc/ttys .
There you find:
.Dx
ttyv0   "/usr/libexec/getty Pc"         cons25  on  secure
.\" # Virtual terminals
ttyv1   "/usr/libexec/getty Pc"         cons25  on  secure
ttyv2   "/usr/libexec/getty Pc"         cons25  on  secure
ttyv3   "/usr/libexec/getty Pc"         cons25  off secure
.De
The keywords \f(CWon\fP and \f(CWoff\fP refer to the state of the terminal: to
enable one, set its state to \f(CWon\fP.  To enable extra virtual terminals, add
a line with the corresponding terminal name, in the range
.Device ttyv4
to
.Device ttyv15 .
After you have edited
.File /etc/ttys ,
you need to tell the system to re-read it in order to start the terminals.  Do
this as \f(CWroot\fP with this command:
.Dx
# \f(CBkill -1 1\fP
.De
Process 1 is
.Daemon init
\(emsee page
.Sref \*[init] \&
for more details.
.H3 "Pseudo-terminals"
.Pn ptys
.X "pty"
.X "pity"
.X "pseudo-terminal"
.X "master device"
.X "slave device"
In addition to virtual terminals, FreeBSD offers an additional class of
terminals called \fIpseudo-terminals\fP.  They come in pairs: a \fImaster
device\fP, also called a \fIpty\fP\/ (pronounced \fIpity\fP\/) is used only by
processes that use the interface, and has a name like
.Device ptyp0 .
The \fIslave device\fP\/ looks like a terminal, and has a name like
.Device ttyp0 .
Any process can open it without any special knowledge of the interface.  These
terminals are used for network connections such as
.Command xterm ,
.Command telnet
and
.Command rlogin .
You don't need a
.Command getty
for pseudo-terminals.  Since FreeBSD Release 5.0, pseudo-terminals are created
as required.
