.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: firewall.mm,v 4.13 2003/06/29 04:25:08 grog Exp grog $
.\"
.Chapter \*[nchfirewall] "Firewalls, IP aliasing and proxies"
The Internet was developed by a relatively small community of computer
scientists, who were for the most part responsible people who often did not take
security issues very seriously.  Since the Internet has been opened to the
general public, three problems have become evident:
.Ls B
.LI
A large number of people have sought to abuse its relatively lax security.
.LI
The address space is no longer adequate for the number of machines connecting to
the network.
.LI
Much bandwidth is used by people downloading the same web pages multiple times.
.Le
What do these problems have to do with each other?  Nothing much, but the
solutions are related, so we'll look at them together.  More specifically, we'll
consider:
.Ls B
.LI
.X "Internet firewall"
.X "firewall, Internet"
How to set up an \fIInternet firewall\fP\/ to keep intruders out of your
network.
.LI
Security tools that ensure that nobody can steal your password from a node
through which it passes.
.LI
.X "IP aliasing"
.X "aliasing, IP"
Tools for \fIIP aliasing\fP, which translate IP addresses to make them appear to
come from the gateway machine.  The way this is done makes it impossible to set
up connections from outside, so they also represent a kind of security device.
.LI
\fICaching proxy servers\fP, which both address the multiple download issues and
provide some additional security.
.Le
.H2 "Security and firewalls"
.X "inetd, daemon"
.X "daemon, inetd"
.X "telnet, command"
.X "command, telnet"
.X "rlogin, command"
.X "command, rlogin"
Recall from Chapter
.Sref "\*[nchnetintro]" \&
that incoming packets need to connect to an IP port, and that some process on
the machine must accept them.  By default, this process is
.Daemon inetd .
You can limit the vulnerability of your machine by limiting the number of
services it supports.  Do you need to supply
.Command telnet
and
.Command rlogin
services?  If not, don't enable the service.  By default,
.File /etc/inetd.conf
no longer enables any services, so this should not be a problem.  Obviously,
careful system configuration can minimize your vulnerability, but it also
reduces your accessibility: intruders can't get in, but neither can the people
who need to access the machine.
.P
.X "packet filtering firewall"
.X "firewall, packet filtering"
.X "proxy firewall"
.X "firewall, proxy"
A better solution is a tool that passes authorized data and refuses to pass
unauthorized data.  Such a tool is called a \fIfirewall\fP.  In this section,
we'll look at \fIpacket filtering firewalls\fP\/: the firewall examines each
incoming packet and uses a set of predefined walls to decide whether to pass it
unchanged, change it, or simply discard it.  An alternative approach is a
\fIproxy firewall\fP, which analyzes each packet and creates new requests based
on its content.  On page
.Sref \*[proxy-server] \&
we'll look at
.Daemon squid ,
a caching proxy server that provides some of this functionality.
.P
.X "ipf, firewall"
.X "ipfilter, firewall"
.X "firewall, ipf"
.X "ipfw, firewall"
.X "firewall, ipfw"
.X "firewall, ipfilter"
FreeBSD supports three different firewalls, \fIipf\fP, \fIipfilter\fP\/ and
\fIipfw\fP.  We consider \fIipfw\fP\/ here; you can find details about
\fIipf\fP\/ and \fIipfilter\fP\/ and in the respective man pages.
.P
The \f(CWDEFAULT\fP FreeBSD kernel does not include firewall support.  If you
wish, you can build a kernel with firewall support\(emsee the file
.File /usr/src/sys/conf/NOTES
for a list of parameters\(embut you don't need to build a new kernel.  You can
load the KLD
.File /boot/kernel/ipfw.ko
instead:
.Dx
# \f(CBkldload ipfw\fP
ipfw2 initialized, divert disabled, rule-based forwarding enabled, default to deny
, logging disabled
.De
Before you do so, make sure you have direct local connection to the system.  If
you start this command remotely, you will instantly lose access to the system.
Read the following section before loading the firewall.
.H3 "ipfw: defining access rules"
The program
.Command ipfw
.X "ipfw, firewall"
.X "firewall, ipfw"
processes access rules for the firewall.  Each rule relates to specific kinds of
packet and describes what to do with them.  On receiving a packet, \fIipfw\fP\/
examines each rule in a predetermined order until it finds one which matches the
packet.  It then performs the action that the rule specifies.  In most cases,
the rule accepts or denies the packet, so \fIipfw\fP\/ does not need to continue
processing the remaining rules, though sometimes processing can continue after a
match.  If no rule matches, the default is to allow no traffic.
.P
.ne 5v
Table
.Sref \*[firewall-keywords] \&
shows the keywords you can use to define the packets and the forms that the
\fIIP address range\fP\/ can take:
.X "ipfw packet types"
.X "packet types, ipfw"
.Table-heading "\fIipfw\fP\/ packet types"
.X "ipfw, firewall"
.X "firewall, ipfw"
.TS H
tab(#) ;
 lfCWp9w14 | lw51  .
\fBKeyword#\fBDescription
_
.TH
ip#T{
All IP packets.
T}
.sp .4v
tcp#T{
TCP packets.
T}
.sp .4v
udp#T{
UDP packets.
T}
.sp .4v
icmp#T{
ICMP packets.
T}
.sp .4v
T{
.nf
.sp .4v
\fIservice name or number\fP\/
T}#T{
.fi
A packet destined for one of the services described in
.File /etc/services .
T}
.sp .4v
T{
.nf
src \fIIP address range\fP\/
T}#T{
.fi
A packet with a source address that matches \fIIP address\fP.  See below for
the interpretation of \fIIP address range\fP.
T}
.sp .4v
T{
.nf
dst \fIIP address range\fP\/
T}#T{
.fi
A packet with a destination address that matches \fIIP address\fP.
T}
.sp .4v
T{
.nf
via \fIinterface\fP\/
T}#T{
.fi
All packets going by the specified interface.  \fIinterface\fP\/ may be an
interface name or an IP address associated with only one interface.
T}
.sp .4v
T{
.nf
recv \fIinterface\fP\/
T}#T{
.fi
All packets arriving by the specified interface.  \fIinterface\fP\/ may be an
interface name or an IP address associated with only one interface.
T}
.sp .4v
T{
.nf
xmit \fIinterface\fP\/
T}#T{
.fi
All packets going out by the specified interface.  \fIinterface\fP\/ may be an
interface name or an IP address associated with only one interface.
T}
.sp .4v
\fIIP address\fP\/#T{
.fi
This is an IP address.  It specifies a match for exactly this
address.
T}
.sp .4v
\fIIP address\fP/\fIbits\fP\/#T{
.fi
\fIbits\fP\/ is a value between 0 and 32.  This form matches the first
\fIbits\fP\/ bits of \fIIP address\fP.
T}
.sp .4v
\fIIP address\fP\/:\fImask\fP\/#T{
.fi
\fImask\fP\/ is a 32-bit value.  This form matches those bits of \fIIP
address\fP specified in \fImask\fP.  This is the same concept as a net
mask\(emsee Chapter
.Sref "\*[nchnetintro]" ,
page
.Sref \*[netmask] ,
for a description of net masks.
T}
.TE
.Tn firewall-keywords
.sp 1.5v
These options can be combined with a few restrictions:
.Ls B
.LI
The \f(CWrecv\fP interface can be tested on either incoming or outgoing packets,
while the \f(CWxmit\fP interface can be tested only on outgoing packets.  This
means that you must specify the keyword \f(CWout\fP (and you may not specify
\f(CWin\fP) when you use \f(CWxmit\fP.  You can't specify \f(CWvia\fP together
with \f(CWxmit\fP or \f(CWrecv\fP.
.LI
A packet that originates from the local host does not have a receive interface.
A packet destined for the local host has no transmit interface.
.Le
.SPUP
.H3 "Actions"
.X "ipfw, firewall"
.X "firewall, ipfw"
When \fIipfw\fP\/ finds a rule which matches a packet, it performs the specified
action.  Table
.Sref \*[firewall-actions] \&
shows the possibilities.
.Table-heading "Actions on packets"
.TS H
tab(#) ;
 lfCWp9w14 | lw59  .
\fBKeyword#\fBDescription
_
.TH
allow#T{
.fi
Allow a packet to pass.  Stop processing the rules.
T}
deny#T{
.fi
Discard the packet.  Stop processing the rules.
T}
unreach#T{
.fi
Discard the packet and send an ICMP \fIhost unreachable\fP\/ message to the
sender.  Stop processing the rules.
T}
reset#T{
.fi
Discard the packet and send a TCP reset message.  This can apply only to TCP
packets.  Stop processing the rules.
T}
count#T{
.fi
Count the packet and continue processing the rules.
T}
T{
.nf
divert \fIport\fP\/
T}#T{
.fi
.X "divert socket"
.X "socket, divert"
Divert the packet to the \fIdivert socket\fP\/ bound to port \fIport\fP.  See
the man page \fIipfw(8)\fP\/ for more details.  Stop processing the rules.
T}
tee \fIport\fP\/#T{
.fi
Send a copy of the packet to the \fIdivert socket\fP\/ bound to port \fIport\fP.
Continue processing the rules.
T}
skipto \fIrule\fP\/#T{
.fi
Continue processing the rules at rule number \fIrule\fP.
T}
.TE
.Tn firewall-actions
.H3 "Writing rules"
The sequence in which rules are applied is not necessarily the sequence in which
they are read.  Instead, each rule can have a \fIline number\fP\/ between 1 and
65534.  Rules are applied from the lowest to the highest line number.  If you
enter a rule without a line number, however, it receives a number 100 higher
than the previous rule.
.P
The highest-numbered rule is number 65535, which is always present.  Normally it
has the form:
.Dx
65535 deny all from any to any
.De
.X "ipfw, firewall"
.X "firewall, ipfw"
In other words, if no other rules are present, or they don't match the packet,
\fIipfw\fP\/ drops the packet.  If you build a kernel with the option
\f(CWIPFIREWALL_DEFAULT_TO_ACCEPT\fP, this rule changes to its opposite:
.Dx
65535 allow all from any to any
.De
These two rulesets  implicitly illustrate two basic security strategies.  You
may note parallels to certain political systems:
.Ls B
.LI
The first takes the attitude ``everything is forbidden unless explicitly
allowed.''
.LI
The second takes the attitude ``everything is allowed unless explicitly
forbidden.''
.Le
It goes without saying that the first policy is safer.  If you make a mistake
with the first (more restrictive) ruleset, you're more likely to lock people out
of your system accidentally than you are to let them in when you don't want
them.
.H3 "Configuration files"
.Pn firewall-configuration
The main configuration file is
.File /etc/rc.firewall .
It's unlikely to match your needs exactly.  There are two possibilities:
.Ls B
.LI
You can edit it to match your requirements.
.LI
You can create your own configuration file with your rules.
.Le
Which do you choose?  If you're making only minor modifications, it's easier to
edit it.  If you're taking things seriously, though, you'll end up with
something that no longer bears much of a relationship with the original file.
Upgrades are easier if you have your own file.
.P
If you create your own file, you can tell
.File /etc/rc.conf
its name, and
.File /etc/rc.firewall
will read it.  Either way, the rules are the same.  In the following section
we'll go through the default
.File /etc/rc.firewall
file.  There's nothing stopping you from copying them to another file and
editing them to match your requirements.
.P
Reading the file is somewhat complicated by a number of environment variables
that are set in the system startup scripts.  We'll see the important ones below.
It's also helpful to know that \f(CW${fwcmd}\fP gets replaced by the name of the
firewall program,
.File -n /sbin/ipfw .
The other ones are described in
.File /etc/default/rc.conf \/:
.Dx
firewall_enable="NO"               # Set to YES to enable firewall functionality
firewall_script="/etc/rc.firewall" # Which script to run to set up the firewall
firewall_type="UNKNOWN"            # Firewall type (see /etc/rc.firewall)
firewall_quiet="NO"                # Set to YES to suppress rule display
firewall_logging="NO"              # Set to YES to enable events logging
firewall_flags=""                  # Flags passed to ipfw when type is a file
.De
To set up the firewall, first decide the kind of profile you need and set the
variable \f(CWfirewall_type\fP accordingly.  The current version of
.File /etc/rc.firewall
defines four kinds of usage profile:
.Ls B
.LI
The \fIopen\fP\/ profile is effectively a disabled firewall.  It allows all
traffic.  You might use this if you're having trouble setting up the firewall
and need to disable it temporarily.
.LI
The \fIclient\fP\/ profile is a good starting point for a system that does not
provide many publicly accessible services to the Net.  We'll look at it in the
next section.
.LI
The \fIsimple\fP\/ profile, despite its name, is intended for a system that does
provide a number of publicly accessible services to the Net.  We'll look at it
on page
.Sref \*[simple-firewall] .
.LI
The \fIclosed\fP\/ profile allows only local traffic via the loopback interface.
.Le
In addition, you can set \f(CWfirewall_type\fP to the name of a file describing
the firewall configuration.
.P
.ne 5v
All configurations start with a call to \f(CWsetup_loopback\fP, which adds the
following rules:
.Dx
${fwcmd} add 100 pass all from any to any via lo0
${fwcmd} add 200 deny all from any to 127.0.0.0/8
${fwcmd} add 300 deny ip from 127.0.0.0/8 to any
.De
These rules allow all local traffic and stop traffic coming in with a fake local
address.
.H4 "The client profile"
At the beginning of the client profile you'll find a number of variables you
need to set.  In the following example they're set to match
\fIfreebie.example.org\fP\/ and our example network:
.Dx
[Cc][Ll][Ii][Ee][Nn][Tt])
    ############
    # This is a prototype setup that will protect your system somewhat against
    # people from outside your own network.
    ############

    # set these to your network and netmask and ip
    net="\f(CB223.147.37.0\fP"
    mask="255.255.255.0"
    ip="\f(CB223.147.37.1\fP"           \fIfreebie.example.org\fP\/
.De
.Figure-heading "Client profile in /etc/rc.firewall"
.Fn client-firewall
The first line matches the text \f(CWclient\fP whether written in upper or lower
case.  Then we have:
.Dx
    setup_loopback

    # Allow any traffic to or from my own net.
    ${fwcmd} add pass all from ${ip} to ${net}:${mask}
    ${fwcmd} add pass all from ${net}:${mask} to ${ip}
.De
These rules allow any traffic in the local network.
.Dx
    # Allow TCP through if setup succeeded
    ${fwcmd} add pass tcp from any to any established
.De
If a TCP connection has already been established, allow it to continue.
Establishing a TCP connection requires other rules, which we shall see below.
.Dx
    # Allow IP fragments to pass through
    ${fwcmd} add pass all from any to any frag
.De
Fragmented packets are difficult to recognize, and if we block them, strange
things might happen.  They're usually not a significant security risk.
.Dx
    # Allow setup of incoming email
    ${fwcmd} add pass tcp from any to ${ip} 25 setup

    # Allow setup of outgoing TCP connections only
    ${fwcmd} add pass tcp from ${ip} to any setup

    # Disallow setup of all other TCP connections
    ${fwcmd} add deny tcp from any to any setup
.De
The preceding three rules allow external systems to establish a TCP connection
for delivering mail (first rule), but nothing else (third rule).  The second
rule allows setup of TCP connections to the outside world.
.Dx
    # Allow DNS queries out in the world
    ${fwcmd} add pass udp from ${ip} to any 53 keep-state

    # Allow NTP queries out in the world
    ${fwcmd} add pass udp from ${ip} to any 123 keep-state

    # Everything else is denied as default.
.De
.X "ipfw, firewall"
.X "firewall, ipfw"
These two rules allow DNS and NTP queries.  The keyword \f(CWkeep-state\fP
causes \fIipfw\fP\/ to build a short-lived dynamic rule matching this particular
combination of end points and protocol.  This means that we don't need to open
traffic in the other direction.  Previously, the rule set for DNS queries
consisted of these two rules:
.Dx
    $fwcmd add pass udp from any 53 to ${ip}
    $fwcmd add pass udp from ${ip} to any 53
.De
This allows all DNS traffic in both directions.  By contrast, \f(CWkeep-state\fP
allows only the reply traffic for specific queries to pass the firewall.  You
don't need this for TCP\(emthe \f(CWestablished\fP keyword does the same
thing\(em but UDP doesn't have the concept of a connection, so the firewall
needs to keep track of the traffic.
.P
There are no more rules, so the default \fIdeny\fP\/ rule prevents any other
kind of traffic.
.H4 "The simple profile"
.Pn simple-firewall
Despite the name, the \fIsimple\fP\/ profile is really a (simple) server
profile.  It assumes that the machine is a gateway, and that it supplies DNS and
NTP services to the outside world (for example, to the \fIclient\fP\/ machine we
just looked at).  This profile is more appropriate for the system
\fIgw.example.org\fP, so we'll use its addresses.
.Dx
    # set these to your outside interface network and netmask and ip
    oif="tun0"
    onet="\f(CB139.130.136.133\fP"
    omask="255.255.255.255"
    oip="\f(CB139.130.136.133\fP"

    # set these to your inside interface network and netmask and ip
    iif="ep0"
    inet="\f(CB223.147.37.0\fP"
    imask="255.255.255.0"
    iip="\f(CB223.147.37.0\fP"
.De
These addresses and networks correspond to the PPP link and the local ethernet,
respectively.
.Dx
    # Stop spoofing
    ${fwcmd} add deny all from ${inet}:${imask} to any in via ${oif}
    ${fwcmd} add deny all from ${onet}:${omask} to any in via ${iif}
.De
.X "spoofing"
These two rules stop any packets purporting to come from the local network that
arrive via the external network, and any packets purporting to come from the
remote network that arrive via the local interface.  These packets would have
been faked, an action known as \fIspoofing\fP.
.Dx
    # Stop RFC1918 nets on the outside interface
    ${fwcmd} add deny all from any to 10.0.0.0/8 via ${oif}
    ${fwcmd} add deny all from any to 172.16.0.0/12 via ${oif}
    ${fwcmd} add deny all from any to 192.168.0.0/16 via ${oif}
.De
RFC 1918 defines networks that should not be routed.  These rules enforce that
requirement.
.P
At this point in the file there are also some other addresses that should not be
routed.  A check is made for address translation, because non-routed addresses
are typically used by NAT environments.
.Dx
    # Allow TCP through if setup succeeded
    ${fwcmd} add pass tcp from any to any established

    # Allow IP fragments to pass through
    ${fwcmd} add pass all from any to any frag

    # Allow setup of incoming email
    ${fwcmd} add pass tcp from any to ${oip} 25 setup

    # Allow access to our DNS
    ${fwcmd} add pass tcp from any to ${oip} 53 setup
    ${fwcmd} add pass udp from any to ${oip} 53
    ${fwcmd} add pass udp from ${oip} 53 to any

    # Allow access to our WWW
    ${fwcmd} add pass tcp from any to ${oip} 80 setup
.De
These rules add to what we saw for the \fIclient\fP\/ profile: in addition to
email, we allow incoming DNS and WWW connections.
.Dx
    # Reject&Log all setup of incoming connections from the outside
    ${fwcmd} add deny log tcp from any to any in via ${oif} setup

    # Allow setup of any other TCP connection
    ${fwcmd} add pass tcp from any to any setup
.De
Here, we don't just reject TCP setup requests from the outside world, we log
them as well.
.Dx
    # Allow DNS queries out in the world
    ${fwcmd} add pass udp from ${oip} to any 53 keep-state

    # Allow NTP queries out in the world
    ${fwcmd} add pass udp from ${oip} to any 123 keep-state

    # Everything else is denied as default.
.De
Finally, we allow DNS and NTP queries via UDP, and deny everything else from the
outside world.
.H4 "user-defined profiles"
.X "ipfw, firewall"
.X "firewall, ipfw"
If the profile isn't one of the recognized keywords,
.File /etc/rc.firewall
checks if there's a file with that name.  If so, it uses it as a command file to
pass to
.Command ipfw \/:
.Dx
elif [ "${firewall}" != "NONE" -a -r "${firewall}" ]; then
    ${fwcmd} ${firewall_flags} ${firewall_type}
.De
Note that you can't put comment lines in the file defined by
\f(CW${firewall}\fP.
.H4 "Entries in /etc/rc.conf"
When you have decided what kind of firewall configuration best suits your
network, note that fact in
.File /etc/rc.conf .
Set the value of \f(CWfirewall_enable\fP to \f(CBYES\fP to enable the firewall,
and the value of \f(CWfirewall_type\fP to indicate the type of firewall.  For
our example network, \f(CWclient\fP is probably the most appropriate type:
.Dx
firewall_enable="\f(CBYES\fP"               # Set to YES to enable firewall functionality
firewall_script="/etc/rc.firewall"  # Which script to set up the firewall
firewall_type="\f(CBclient\fP"              # Firewall type (see /etc/rc.firewall)
.De
If you have decided to write your own file rather than modify
.File /etc/rc.firewall ,
set \f(CWfirewall_type\fP to the name of the file.
.H3 "Trying it out"
You'll probably find that your first attempt at firewall configuration won't be
the optimum.  You'll probably discover requirements that you hadn't thought of
and that are now being denied by the default rule.  Be prepared to spend some
time getting everything to work, and \f(BIdo this at the system console\fP.
There's no good alternative.
.H2 "IP aliasing"
.Pn ip-aliasing
.X "IP aliasing"
.X "aliasing, IP"
In our reference network on page
.Sref \*[ref-net] ,
we assumed that our local network had a valid assigned IP address.  Sometimes,
this isn't possible.  In fact, in the Real World it's pretty well impossible to
get a complete class C network for a system with only five systems on it.  You
have the alternative of getting a subset of a class C network (in this case,
eight addresses would do) from your ISP, or using just one address and running
software that makes all traffic from the network to the outside world look as if
it's coming from that system.  The latter approach, called \fInetwork address
translation\fP\/ (\fINAT\fP\/) or \fIIP aliasing\fP, can be significantly
cheaper: ISPs often charge good money for additional addresses.  On the down
side, NAT restricts you in some ways.  Any connection between a machine on a NAT
network and the global Internet must start from the machine on the NAT network,
because the translation doesn't exist until the connection is set up.  This also
means that you can't connect two machines on different NAT networks.
.P
Network address translation involves three machines: one on the global Internet
with real Internet addresses, one on a private subnet with unroutable addresses,
and one in the middle that performs the translation.  In our reference network
(see page
.Sref \*[ref-net] ),
let's consider connecting \fIandante\fP, the laptop, to the Internet with
\fIpresto\fP\/ acting as address translator.  \fIandante\fP\/ is not part of the
local network, so it gets an address in one of the address spaces specified by
RFC 1918 (\f(CW192.168.0.0\fP to \f(CW192.168.255.255\fP, \f(CW172.16.0.0\fP to
\f(CW172.31.255.255\fP, or \f(CW10.0.0.0\fP to \f(CW10.255.255.255\fP).  In this
example, it has the address \f(CW192.168.27.17\fP, and we can assume that it got
this address from a DHCP server on \fIpresto\fP.  A connection to a remote web
site
.URI http://www.FreeBSD.org
might look like this:
.PS
.ps 9
        boxht = .4i
        boxwid = .6i
        move right .2
ANDANTE:        box "\fIandante\fP"

PRESTO: box at ANDANTE+(2,0) "\fIpresto\fP\/"

WWW:    box at ANDANTE+(4,0)    "\fIFreeBSD\fP"
        "\fIwww.\fP\/" at WWW.c+(0,.13)
        "\fI.org\fP\/" at WWW.c+(0,-.13)

L:      line from ANDANTE.e to PRESTO.w

LB:     line from PRESTO.e to WWW.w
        "\f(CW\s7192.168.27.17\s0\fP"  at ANDANTE.n+(0,.1)
        "\f(CW\s72731\s0\fP" ljust at L.w+(.1,-.1)

        "\f(CI\s7192.168.27.1\s0\fP" at PRESTO.n+(-.1,.1) rjust
        "\f(CI\s780\s0\fP" at PRESTO.w+(-.1,-.1) rjust

        "\f(CI\s7223.147.37.2\s0\fP" at PRESTO.n+(.1,.1) ljust
        "\f(CI\s73312\s0\fP" at PRESTO.e+(.1,-.1) ljust

        "\f(CI\s7216.136.204.117\s0\fP" at WWW.n+(0,.1)
        "\f(CI\s780\s0\fP" at WWW.w+(-.1,-.1) rjust
.\" Interface names

        "\fIwi0\fP\/" ljust at ANDANTE.e+(.1,.1)
        "\fIxl0\fP\/" rjust at PRESTO.w+(-.1,.1)
        "\fIdc0\fP\/" ljust at PRESTO.e+(.1,.1)

.PE
.Fn NAT-access
.Figure-heading "Accessing the Web via NAT"
In this diagram, the IP addresses are above the boxes, the interface names above
the connection lines, and the port numbers below the connection lines.  The
connection must be started by \fIandante\fP, because there is no way to route to
it directly from outside the local link.  It finds the address of
\fIwww.FreeBSD.org\fP\/ (216.136.204.117) and sends it out its default route, in
this case the only interface, \fIwi0\fP.  \fIpresto\fP\/ gets the packet on
interface \fIxl0\fP\/ and routes it out through interface \fIdc0\fP.
.P
So far, that's nothing special\(emit's what any router does.  The difference is
that \fIpresto\fP\/ changes the source address and port number.  On the wireless
link, \fIandante\fP\/'s address is \f(CW192.168.27.17\fP, and the port number
for this connection is 2731.  On the remote link, the IP address becomes
\f(CW223.147.37.2\fP, \fIpresto\fP\/'s own address, and the port number in this
case is 3312.  Theoretically, the ``changed'' port address could be the same as
the original, since there is no relationship.  The destination IP address and
port number can't change, of course, or the packet would never get to its
destination.
.P
On return, the reverse happens: \fIwww.FreeBSD.org\fP\/ replies to \fIpresto\fP,
which recognizes the port number, converts it to \fIandante\fP\/'s IP address
and source port, and sends it to \fIandante\fP\/ on the local network.
.H3 "IP aliasing software"
There are a number of ways to perform IP aliasing with FreeBSD.  If you're
connecting to the outside world via User PPP (see Chapter
.Sref "\*[nchppp]" ,
page
.Sref \*[user-ppp] ),
you can use the \f(CW-alias\fP keyword to tell PPP to alias \fIall\fP\/ packets
coming from the network to the address of the tunnel interface.  In our
reference network, this would be the address \f(CW139.130.136.133\fP.
.P
.X "natd, daemon"
.X "daemon, natd"
This particular form of IP aliasing has some limitations: it works only for a
single User PPP connection to the outside world, and it's global in its
functionality.  One alternative is the \fINetwork Address Translation Daemon\fP,
or
.Daemon natd ,
which uses divert sockets to translate addresses.  It works well in conjunction
with the firewall software we looked at above.
.H3 "natd"
.Pn natd-rc.conf
To set up
.Daemon natd
for the example above, perform the following steps:
.Ls B
.LI
Even if you don't plan to run an IP firewall, build and boot a custom kernel
with the following options:
.Dx
options IPFIREWALL
options IPDIVERT
.De
If you are running a firewall, configure the firewall normally, but be sure to
include the \f(CWIPDIVERT\fP option.
.LI
Make sure your interfaces are running.  For example, if you're running Kernel
PPP and you want to specify \fIppp0\fP\/ as your interface, start \fIpppd\fP\/
before starting
.Daemon natd .
.LI
What you do next differs a little depending on whether you are also running a
firewall or not.  If you're not, you're better off with a separate script, which
you might call
.File /etc/rc.nat ,
with the following content:
.Dx
/sbin/ipfw -f flush
/sbin/ipfw add divert natd all from any to any via dc0
/sbin/ipfw add pass all from any to any
.De
.SPUP
.LI
If you want to combine NAT with real firewall rules, you need only the second
line of the previous example.  Set up the firewall as described above, and put
the NAT line at the start of the section of
.File /etc/rc.firewall
that you have chosen, so that
.Daemon natd
sees all packets before they are dropped by the firewall.  After
.Daemon natd
translates the IP addresses, the firewall rules are run again on the translated
packet, with the exception of the divert rules.  The \fIclient\fP\/
configuration is the most likely one to suit your needs if you're using NAT.
After the example in figure
.Sref \*[client-firewall] \&
on
.Sref \*[client-firewall-page] ,
you might add:
.Dx
# set these to your network and netmask and ip
net="192.0.2.0"
mask="255.255.255.0"
ip="192.0.2.1"

setup_loopback

.ft CB
/sbin/ipfw add divert natd all from any to any via dc0
.ft

# Allow any traffic to or from my own net.
${fwcmd} add pass all from ${ip} to ${net}:${mask}
${fwcmd} add pass all from ${net}:${mask} to ${ip}
.De
.SPUP
.LI
.ne 5v
Add the following to
.File /etc/rc.conf \/:
.Dx
firewall_enable=YES
gateway_enable="YES"            # Set to YES if this host is a gateway.
natd_enable="YES"
natd_interface="dc0"
firewall_script="/etc/rc.nat"   # script for NAT only
firewall_type="client"          # firewall type if running a firewall
.De
.ne 2v
The interface name in the second line, \fIdc0\fP, is the name of the external
interface (the one with the real IP addresses).
.P
If you're using NAT but not a firewall, you don't need to specify a
\f(CWfirewall_type\fP, because that relates to
.File /etc/rc.firewall .
You do need to specify the name of the script to run, however.
.LI
Enable your firewall as shown above in the firewall section.  If you don't
intend to reboot now, just run
.File /etc/rc.firewall
(or
.File -n /etc/rc.natd )
by hand from the console, and then start
.Daemon natd \/:
.Dx
# \f(CBsh /etc/rc.nat\fP                              \fIfor NAT only\fP\/
# \f(CBfirewall_type=client sh /etc/rc.firewall\fP    \fIfor NAT and firewall \fP\/
# \f(CBnatd dc0\fP
.De
The expression \f(CWfirewall_type=client\fP tells the Bourne shell to set the
value of the variable \f(CWfirewall\fP just for this command.  If you're using
.Command csh
or
.Command tcsh ,
use the following sequence:
.Dx
(setenv firewall_type=client; sh /etc/rc.firewall)
.De
\fINever\fP\/ start this script from an X terminal or across the network.  If
you do, you can lock yourself out of the session in the middle of the script,
causing
.File /etc/rc.firewall
to stop at this point, blocking all accesses to the system.
.Le
.SPUP
.H2 "Proxy servers"
.Pn proxy-server
For some purposes, a good alternative or adjunct to a packet filtering firewall
and NAT is a \fIproxy server\fP that converts requests for specific protocols.
In the example in the previous section, which was accessing a web server, we
could also have run a proxy server on \fIpresto\fP.  Particularly in conjunction
with web servers, a proxy server has the advantage that it can cache data
locally, thus reducing network load.
.P
There are a couple of other differences between NAT and proxy servers:
.Daemon natd
does not know much about the data it passes.  Proxy servers know a lot about it.
This makes proxy servers less suitable as a general security or address
translation mechanism.  In addition, the client must know about the proxy
server, whereas it does not need to know anything about NAT and firewalls.  A
typical connection looks like this:
.PS
.ps 9
        boxht = .4i
        boxwid = .6i
        move right .2
ANDANTE:        box "\fIandante\fP"

PRESTO: box at ANDANTE+(2,0) "\fIpresto\fP\/"

WWW:    box at ANDANTE+(4,0)    "\fIFreeBSD\fP"
        "\fIwww.\fP\/" at WWW.c+(0,.13)
        "\fI.org\fP\/" at WWW.c+(0,-.13)

L:      line from ANDANTE.e to PRESTO.w

LB:     line from PRESTO.e to WWW.w
        "\f(CW\s7192.168.27.17\s0\fP"  at ANDANTE.n+(0,.1)
        "\f(CW\s72731\s0\fP" ljust at L.w+(.1,-.1)

        "\f(CI\s7192.168.27.1\s0\fP" at PRESTO.n+(-.1,.1) rjust
        "\f(CI\s78080\s0\fP" at PRESTO.w+(-.1,-.1) rjust

        "\f(CI\s7223.147.37.2\s0\fP" at PRESTO.n+(.1,.1) ljust
        "\f(CI\s73312\s0\fP" at PRESTO.e+(.1,-.1) ljust

        "\f(CI\s7216.136.204.117\s0\fP" at WWW.n+(0,.1)
        "\f(CI\s780\s0\fP" at WWW.w+(-.1,-.1) rjust
.\" Interface names

        "\fIwi0\fP\/" ljust at ANDANTE.e+(.1,.1)
        "\fIxl0\fP\/" rjust at PRESTO.w+(-.1,.1)
        "\fIdc0\fP\/" ljust at PRESTO.e+(.1,.1)
.PE
.\" Grrr
.ps \n(PS
.Fn proxy-access
.Figure-heading "Accessing the Web via a proxy server"
This looks very similar to Figure
.Sref \*[NAT-access] .
The only thing that appears to have changed is the port number on
\fIpresto\fP\/'s \fIxl0\fP\/ interface.  In fact, there's more than that: in
Figure
.Sref \*[NAT-access] ,
\fIandante\fP\/ establishes a connection with
.URI www.FreeBSD.org .
Here it establishes a connection with
.URI presto.example.org .
.H2 "Installing squid"
A good choice of web proxy server is
.Daemon squid ,
which is available in the Ports Collection.  Install it in the normal manner:
.Dx
# \f(CBcd /usr/ports/www/squid\fP
# \f(CBmake install\fP
.De
.Daemon squid
is not the easiest thing in the world to set up, and it's hampered by
sub-standard documentation.  The man page is \fIsquid(8)\fP, but most of the
information is in the configuration file
.File /usr/local/etc/squid/squid.conf .
By default, it is set up to do nothing.  It has over 3,000 lines of mostly
comments.  I suggest the following changes:
.Ls B
.LI
Set the value \f(CWhttp_proxy\fP to the number of the port you want to use.  By
default,
.Daemon squid
uses port 3128, but many proxies use port 8080, and that's the port that most
web browsers expect too.  If you are not running a web server on the machine,
you can also use the \fIhttp\fP\/ port, 80.  Add:
.Dx
 http_port 8080 80
.De
.SPUP
.LI
The variable \f(CWhttp_access\fP defines who can access the web server.  By
default, it denies all requests except from the local manager, so you must set
it if you expect to get any results from the server.  An appropriate setting
might be:
.Dx
acl local src 192.168.27.0/255.255.255.0
acl exampleorg src 223.147.37.0/24

http_access allow local
http_access allow exampleorg
.De
This defines two access control lists, one for the NAT network we looked at in
the previous section (\f(CWlocal\fP), and one for the globally visible network
223.147.37.0 (\f(CWexampleorg\fP).  The first \f(CWacl\fP statement specifies
the network in the form \fIaddress\fP\//\fInetmask\fP, while the second
specifies it with the number of significant bits in the net mask.  The
\f(CWhttp_access\fP statements then allow access for each of them.
.LI
If you're using the \fIftp\fP\/ proxy, it's probably a good idea to change the
default name with which
.Daemon squid
performs anonymous \fIftp\fP.  By default it's \f(CWSquid@\fP, but that looks
silly.  Change it by setting:
.Dx
ftp_user squid@example.org
.De
.SPUP
.LI
.ne 4v
.Daemon squid
doesn't expect any line of the \fIftp\fP\/ file listing to be more than 32
characters long.  That's pretty conservative.  You can make it larger like this:
.Dx 1
ftp_list_width 120
.De
.SPUP
.LI
By default,
.Daemon squid
caches any object less than 4 MB in size on disk.  If you're doing a lot of
\fIftp\fP\/ work, this can seriously degrade the cache performance for
\fIhttp\fP.  You can reduce it to, say, 256 kB with:
.Dx
maximum_object_size 256 KB
.De
.SPUP
.LI
The system starts
.Daemon squid
as user \f(CWroot\fP, which is not the best for security: proxy servers are a
popular target for intruders on the Internet.  You should change it to run as
user and group \f(CWwww\fP:
.Dx
cache_effective_user www
cache_effective_group www
.De
.SPUP
.Le
.H3 "Starting squid"
Before you can start
.Daemon squid ,
you must first create the cache directories.  If not, you can start it, and it
doesn't complain, but it doesn't run either.  Later you might find something
like this in the log file
.File /var/log/messages \/:
.Dx
Dec 21 15:26:51 presto squid[23800]: Squid Parent: child process 23802 started
Dec 21 15:26:53 presto (squid):         Failed to verify one of the swap directories
, Check cache.log   for details.  Run 'squid -z' to create swap directories
if needed, or if running Squid for the first time.
Dec 21 15:26:53 presto kernel: pid 23802 (squid), uid 65534: exited on signal 6
Dec 21 15:26:53 presto squid[23800]: Squid Parent: child process 23802 exited due to
 signal 6
Dec 21 15:26:56 presto squid[23800]: Squid Parent: child process 23805 started
.De
The log files are in
.File /usr/local/squid/log ,
and the cache files should be in
.File /usr/local/squid/cache .
To create them, enter:
.Dx
# \f(CBsquid -z\fP
2002/12/21 15:30:35| Creating Swap Directories
.De
Finally, you can start
.Daemon squid \/:
.Dx 1
# \f(CBsquid
.De
On system restart,
.Daemon squid
will be started automatically from the script in
.File /usr/local/etc/rc.d/squid.sh .
.H2 "Browser proxy configuration"
As mentioned earlier, proxies aren't transparent to the application.  You have
to set up your software to talk to the proxy.  To do that, you need to configure
the web browser accordingly.  For example, with
.Command galeon
you select Settings\(->Preferences\(->Advanced\(->Network and get the following
screen:
.PIC images/galeon-proxy-settings.ps 4i
.Figure-heading "Galeon proxy settings"
.Daemon squid
understands the individual protocols that it supports, so it can tell the
difference between, say, an \fIhttp\fP\/ request on port 8080 and an \fIftp\fP\/
request on the same port.  Nevertheless, consider whether it's a good idea to
use
.Daemon squid
for \fIftp\fP.  It doesn't speed up access the first time you fetch the file,
and if you access each file only once, you don't have any gain through using
.Daemon squid .
On the other hand, the \fIftp\fP\/ data can pollute the cache.
.H3 "Setting proxy information for ftp"
.Command ftp
understands proxies, and uses them for non-interactive connections only.  Put
the following statement in your
.File .profile
file:
.Dx
export http_proxy=presto.example.org:8080
export ftp_proxy=presto.example.org:8080
.De
.ig
For
.File csh -like
shells, put this in your
.File .login
file:
.Dx 2
setenv http_proxy presto.example.org:8080
setenv ftp_proxy presto.example.org:8080
.De
From: "Sean J. Countryman" <sean@rackoperations.com>
To: "FreeBSD Questions" <freebsd-questions@FreeBSD.ORG>,
        "admin" <admin2@enabled.com>
Subject: RE: port scanning detection
X-Mailer: Microsoft Outlook IMO, Build 9.0.2416 (9.0.2910.0)

snort
portsentry
 

-----Original Message-----
From: owner-freebsd-questions@freebsd.org
[mailto:owner-freebsd-questions@freebsd.org]On Behalf Of admin
Sent: Sunday, May 04, 2003 10:51 PM
To: freebsd-questions@freebsd.org
Subject: port scanning detection

  

 
Hey,
 
Is there a good Program out there that can assist me with identifying when I
am getting portscanned and possible origination?

- Noah

..
