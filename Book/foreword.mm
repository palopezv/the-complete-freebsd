.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\"
.\" $Id: foreword.mm,v 4.6 2003/04/08 00:12:00 grog Exp grog $
.\"
.Chapter 0 Foreword
.X "McKusick, Kirk"
I have been a long time developer of the Berkeley Software Distributions
(BSD). My involvement started in 1976, at the University of California at
Berkeley. I got drawn in as an office-mate of Bill Joy, who single-handedly
wrote the code for BSD and then started handling its release. Bill went on to
run the Computer Systems Research Group (CSRG) which developed and released the
first fully complete BSD distributions. After Bill's departure to become a
founder of Sun Microsystems, I eventually rose to head the CSRG and oversee the
release of the freely redistributable 4.4BSD-Lite. The 4.4BSD-Lite distribution
forms the basis for all the freely distributable variants of BSD today as well
as providing many of the utilities found in Linux and commercial UNIX
distributions.
.P 
With the release of 4.4BSD-Lite, the University of California at Berkeley ceased
further BSD development. After considering the strengths and weaknesses of
different BSD development groups, I decided to do my continued development in
FreeBSD because it had the largest user community. For the past ten years,
therefore, I have been a member of the FreeBSD developer team.
.P 
I have always felt that it is important to use your own product. For this
reason, I have always run BSD everywhere: on my workstation, on my
Web/file/mail/backup server, on my laptop, and on my firewall. By necessity, I
have to find tools to do my job that will run on my BSD systems.  It may be
easier to just run Windows and PowerPoint to do your presentations, but there
are an ever increasing number of fine alternatives out there that run on FreeBSD
such as the open source OpenOffice.org suite or MagicPoint.
.P
In the old days, there were not very many people working on the BSD
software. This constraint on BSD development made it easy to keep up with what
BSD could do and how to manage your system. But the last decade has seen an
exponential growth in the open source movement. The result has been a huge
increase in the number of people working on FreeBSD and an even larger increase
in the number of applications and tools that have been ported to run on
FreeBSD. It has become a more than full time job just to keep track of all the
system capabilities, let alone to figure out how to use them all.
.P
Greg Lehey has done a wonderful job with this book of helping those of us that
want to fully utilize the FreeBSD system to do so without having to devote our
entire lives figuring how. He has gone through and figured out each of the
different tasks that you might ask your system to do.  He has identified the
software that you need to do the task.  He explains how to configure it for your
operational needs.  He tells you how to monitor the resulting subsystem to make
sure it is working as desired. And, he helps you to identify and fix problems
that arise.
.P
The book starts with the basics of getting the FreeBSD system up and running on
your hardware, including laptops, workstations, and servers. It then explains
how to customize an installation for your personal needs. This personalization
includes downloading and operating the most important of the more than 8000
software packages in the FreeBSD ports collection. The book also includes a very
comprehensive set of systems administration information, including the setup and
operation of printers, local and external networking, the domain name system,
the NFS and Samba remote filesystems, electronic mail, web surfing and hosting,
and dial-up for FAX, remote login, and point-to-point network connections.
.P
In short, this book provides everything you need to know about the FreeBSD
system from the day you first pick up the software through the day you have a
full suite of machines.  It covers your complete range of computing needs. There
is a reason that this book is so popular: as its title says, it is The Complete
FreeBSD. I am very happy to see this revision which once again fulfills that
mandate.
.P
.in +2i
.br
.nf
.na
Marshall Kirk McKusick
Berkeley, California
February 2003
.ad
.fi
.in
