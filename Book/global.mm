.\" This file is in -*- nroff-fill -*- mode
.\" Global string definitions
.\" $Id: global.mm,v 4.6 2003/04/09 19:31:26 grog Exp grog $
.\" Basic page format
.ds Fver 5.0\" FreeBSD version number
.ds Xver 4.2.0\" XFree86 version number
.ds Xupdate 1\" XFree86 update number
.\"
.\" XXX we don't really need this.  Only referred to in one place.
.nr cwfontsize 8
.cs CW 19
.cs CB 19
.cs CBI 19
.cs CI 19
.nr pg*footer-size 5
.ps \n(PS			\"  point size of standard text
.vs \n(LS			\" Standard vertical spacing
.nr Pi 3                        \" indent paragraphs 3 characters
.FD 1                           \" footnotes adjusted, hyphenated, indented
.nr Fs .6                       \" additional .6 line gap between footnotes
.nr appendix 0			\" not an appendix until proven otherwise
.nr ft*clear-at-header 1	\" Clear footnotes on every page
.hw Free-BSD
.\" Macros
.de EOP
.br
.ps 8
.ie (\\n[firstpage]=1) .tl '\\*[RCS-ID]''%'
.el \{\
.  ie e .tl '''\\*[RCS-ID]'
.  el .tl '\\*[RCS-ID]'''
.\}
.if \\n[firstpage]>0 .nr firstpage \\n[firstpage]-1
.ps
..
.if '\\n[html'1' \{\
.nf
.na
.nh
.\}
.rm pg@header
.de pg@header
.\" XXX cut marks? .CM pagetop
.\" XXX.if \\n[D]>1 .tm Page# \\n[%] (\\n[.F]:\\n[c.]) firstpage:\\n[firstpage]
.if \\n[D]>1 .tm Page# \\n[%] (\\n[.F]:\\n[c.])
.if \\n[Idxf] \{\
.tl '<pagenr\ \\n[%]>'''
.\}
.\" assign current page-number to P
.hd@set-page
.\"
.\" suppress pageheader if pagenumber == 1 and N == [124]
.if \\n[pg*top-enabled] \{\
.\"	must be fixed!!
.\".	pg@disable-top-trap
.	if \\n[pg*extra-header-size] 'sp \\n[pg*extra-header-size]u
.	if \\n[pg*top-margin] 'sp \\n[pg*top-margin]u
.	ev pg*tl-ev
.	pg@set-env
'	sp 3
.fam H
.br
.ps 8
.\" No page number on first page of chapter
.	ie (\\n[firstpage]=0) \{\
.		ie e \{\
.			ie '\\*[Chapter*number]'0' .tl  '%''\\*[Chapter*title]'
.			el .tl '%''Chapter \\*[Chapter*number]: \\*[Chapter*title]'
.			\}
.		el \{\
.		        ie '\\*[Section*title]'' .tl 'The Complete FreeBSD''%'
.			el .tl '\\*[Section*title]''%'
.		\}
.		fam T
\v'-.7v'\l'5i'
.if !'\\*[Book*title]'' \h'-5i'\v'-\\n[title*offset]u'\s6\\*[Format*date]  \\*[Book*title] (\\n[.F]), page \\n%\s0\v'\\n[title*offset]u'
.\}
.\" XXX get this right.  Should be related to title*offset less 1v.
.el .if e .if !'\\*[Book*title]'' \v'-.94i'\s6\\*[Format*date]  \\*[Book*title] (\\n[.F]), page \\n%\s0\v'\\n[title*offset]u'
.br
.	\}
.	ev
.	\" why no-space??
.	if d PX \{\
.		ns
.		PX
.		rs
.	\}
.	\" check for pending footnotes
.	ft@check-old
.	\"
.	\" back to normal text processing
.	pg@enable-trap
.	\" mark for multicolumn
.	nr pg*head-mark \\n[nl]u
.	\" reset NCOL pointer at each new page.
.	nr pg*last-ncol 0
.	\" set multicolumn
.	\"
.	pg@set-po
.	\" print floating displays
.	df@print-float 4
.	tbl@top-hook
.	ns
.\}
.if \\n[pg*top-enabled]<0 .nr pg*top-enabled 1
.nr hd*cur-bline \\n[nl]	\" .H needs to know if output has occured
.nr ft*nr 0 1                  \" Reset footnote number
..
.\" Just for Jack
.\" See GNU troff man page: hyphenation space
.hys 5p
.ie n \{\
.de Df
.br
.DS
.fi
..
\}
.el \{\
.de Df
.br
.DF
.fi
..
.\}
