.\" This file is in -*- nroff-fill -*- mode
.\" $Id: grog.mm,v 2.8 2003/04/02 05:06:48 grog Exp grog $
.\" STATUS: 4th edition
.\"
.1C
.fi
.ce
\f(BI\s14About the author\fP\s0
.sp
.ad
.br
.X "Lehey, Greg"
Greg Lehey was born in Australia and went to school in Malaysia and England
before studying Chemistry in Germany and Chemical Engineering in England.  He
spent most of his professional career in Germany, where he worked for computer
manufacturers such as Univac, Tandem, and Siemens-Nixdorf, the German space
research agency, nameless software houses and a large user.  Finally he worked
for himself as a consultant.  He returned to Australia in 1997.
.P
In the course of 30 years in the industry he has performed most jobs, ranging
from kernel development to product management, from systems programming to
systems administration, from processing satellite data to programming petrol
pumps, from the production of CD-ROMs of ported free software to DSP instruction
set design.  Apart from this book, he is also the author of ``Porting UNIX
Software'' (O'Reilly and Associates, 1995).  About the only thing he hasn't done
is writing commercial applications software.  He is available for short-term
contracts and can be reached by mail at \f(CWgrog@FreeBSD.org\fP or
\f(CWgrog@lemis.com\fP.  Alternatively, browse his home page at
\fIhttp://www.lemis.com/grog/\fP.
.P
When he can drag himself away from his collection of UNIX workstations, he is
involved in performing baroque and classical woodwind music on his collection of
original instruments, exploring the Australian countryside with his family on
their Arabian and Peruvian horses, or exploring new cookery techniques or
ancient and obscure European languages.
