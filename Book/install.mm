.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: install.mm,v 4.24 2004/03/09 01:31:12 grog Exp grog $
.\"
.\" This file contains conditional code for the following sizes:
.\" complete  (The Complete FreeBSD)
.\" workshop ("FreeBSD: Instant Server")
.\" long, verylong, itworks -- experimental stuff, probably obsolete
.\" XXX driver floopy
.Chapter \*[nchinstall] "Installing FreeBSD"
..if workshop
The first step in setting up FreeBSD is to install the base system.  This
chapter looks at how to install FreeBSD from CD-ROM onto an Intel ia32 (80386 or
later) system.  For alternative installation methods and processors, refer to
the book ``The Complete FreeBSD.''
.H2 "Booting from CD-ROM"
..else
.Pn install-start
In the previous chapters, we've looked at preparing to install FreeBSD.  In this
chapter, we'll finally do it.  If you run into trouble, I'll refer you back to
the page of Chapter
.Sref \*[nchconcepts] \&
which discusses this topic.  If you want to install FreeBSD on the same disk as
Microsoft or another operating system, you should have already read
.Sref "\*[chshareinstall]" .
.P
The following discussion relates primarily to installation on the i386
architecture.  See page
.Sref \*[alpha-install] \&
for differences when installing on the AXP (``Alpha'') processor.
.H2 "Installing on the Intel i386 architecture"
.Pn sw-prepare
To install FreeBSD you need the software in a form that the installation
software understands.  You may also need a boot diskette.  Nowadays you will
almost invariably install from CD-ROM, so we'll assume that medium.  On page
.Sref "\*[alternative-install]" ,
we'll look at some alternatives: installation from floppy disk or via the
network.
.P
The first step in installing FreeBSD is to start a minimal version of the
operating system.  The simplest way is to boot directly from the installation
CD-ROM.  If your system doesn't support this kind of boot, boot from floppy.
See page
.Sref "\*[make-floppy]" \&
for more details.
.P
.Pn boot-install-kernel
The description in this chapter is based on a real-life installation on a real
machine.  When you install FreeBSD on your machine, a number of things will be
different, depending on the hardware you're running, the way you're installing
the software and the release of FreeBSD you're installing.  Nevertheless, you
should be able to recognize what is going on.
.P
..endif
Booting from CD-ROM is mainly a matter of setting up your system BIOS and
possibly your SCSI BIOS.  Typically, you perform one of the following
procedures:
.Ls B
.LI
.X "boot sequence"
If you're booting from an IDE CD-ROM, you enter your system BIOS setup routines
and set the \fIBoot sequence\fP\/ parameter to select CD-ROM booting ahead of
hard disk booting, and possibly also ahead of floppy disk booting.  A typical
sequence might be \f(CWCDROM,C,A\fP.
.LI
On most machines, if you're booting from a SCSI CD-ROM, you also need a host
adapter that supports CD-ROM boot.  Set up the system BIOS to boot in the
sequence, say, \f(CWSCSI,A,C\fP.  On typical host adapters (such as the Adaptec
2940 series), you set the adapter to enable CD-ROM booting, and set the ID of
the boot device to the ID of the CD-ROM drive.
.Le
These settings are probably not what you want to use for normal operation.  If
you leave the settings like this, and there is a bootable CD-ROM in your CD-ROM
drive, it always boots from that CD-ROM rather than from the hard disk.  After
installation, change the parameters back again to boot from hard disk before
CD-ROM.  See your system documentation for further details.
..if complete
.H2 "Booting to sysinstall"
.Pn sysinstall-main
The boot process itself is very similar to the normal boot process described on
page
.Sref \*[bootup] .
After it completes, though, you are put into the
.Command sysinstall
main menu.
.X "sysinstall, using"
.X "FAQ/Text"
.PIC images/main-install.ps 4i
.Figure-heading "Main installation menu"
.Fn main-installation-menu
.\" XXX .pageref \*[main-installation-menu] "on page \*[main-installation-menu]"
Figure
.Sref \*[main-installation-menu] \&
shows the main
.Command sysinstall
menu.
.Command sysinstall
includes online help at all stages.  Simply press \fBF1\fP and you will get
appropriate help.  Also, if you haven't been here before, the \f(CWDoc\fP menu
gives you a large part of the appropriate information from the handbook.
.H3 "Kinds of installation"
.X "standard installation"
.X "express installation"
.X "custom installation"
.X "installation, standard"
.X "installation, express"
.X "installation, custom"
To get started, select one of \fIStandard\fP, \fIExpress\fP\/ or \fICustom\fP.
The names imply that the \fIStandard\fP\/ installation is the best way to go,
the \fIExpress\fP\/ installation is for people in a hurry, and \fICustom\fP\/
installation is for when you want to specify exactly what is to be done.
.P
In fact, the names are somewhat misleading.  There isn't really that much
difference between the three forms of installation.  They all perform the same
steps:
.Ls B
.LI
Possibly set up options.
..if verylong
We'll look at this on page
.Sref \*[install-options] .
..endif
.LI
Set up disk partitions, which we'll discuss in the next section.
.LI
Set up file systems and swap space within a FreeBSD slice, which we start on
page
.Sref \*[disklabel-editor] .
.LI
Choose what you want to install, which we discuss on page
.Sref \*[select-distribution] .
.LI
Choose where you want to install it from.  We'll look at this on page
.Sref \*[select-medium] .
.LI
Actually install the software.  We'll treat this on page
.Sref \*[commit] .
.Le
..if complete
We looked at disk partitions and file systems on page
.Sref \*[partitions] .
We'll look at the other points when we get to them.
.P
..endif
So what's the difference between the kinds of installation?
.Ls B
.LI
The Standard installation takes you through these steps in sequence.  Between
each step, you get a pop-up window that tells you what is going to happen next.
.LI
The Express installation also takes you through these steps in sequence.  The
main difference is that you don't get the pop-up window telling you what is
going to happen next.  This can save a little time.  If you do want the
information, similar information is available with the F1 key.
.LI
The Custom installation returns you to its main menu after each step.  It's up
to you to select the next step.  You can also select another step, or go back to
a previous one.  Like the Express installation, you don't get the pop-up
information window, but you can get more information with the F1 key.
.Le
The big problem with Standard and Express installations is that they don't let
you back up: if you pass a specific step and discover you want to change
something, you have to abort the installation and start again.  With the Custom
installation, you can simply go back and change it.  As a result, I recommend
the Custom installation.  In the following discussion, you won't see too much
difference: the menus are the same for all three installation forms.
.br
.PIC images/custom-main.ps 4i
.Figure-heading "Custom Installation options"
.Fn custom-main-menu
.H2 "Setting installation options"
The first item on the menu is to set installation options.  There's probably not
too much you'll want to change.  About the only thing of interest might be the
editor
.Command ec ,
which is a compromise between a simple editor for beginners and more complicated
editors like
.Command vi .
If you're planning to edit anything during the installation, for example the
file
.File /etc/exports ,
which we'll look at on page
.Sref \*[etc-exports] ,
you may prefer to set an editor with which you are familiar.  Select the fields
by moving the cursor to the line and pressing the space bar.
.PIC images/custom-options.ps 4i
.Figure-heading "Installation options"
.H2 "Partitioning the disk"
.X "partition table, creating"
.X "disks, creating file systems"
.X "disks, creating space"
.Pn building-partition-table
The first installation step is to set up space for FreeBSD on the disk.  We
looked at the technical background in Chapter
.Sref "\*[nchconcepts]" ,
on page
.Sref \*[make-fs] .
In this section only, we'll use the term \fIpartition\fP\/ to refer to a slice
or BIOS partition, because that's the usual terminology.
.P
.X "partition"
Even if your disk is correctly partitioned, select the \fIPartition\fP\/ menu:
the installation routines need to enter this screen in order to read the
partition information from the disk.  If you like what you see, you can leave
again immediately with \f(CWq\fP (quit), but you must first enter this menu.  If
you have more than one disk connected to your machine, you will next be asked to
choose the drives that you want to use for FreeBSD.
.PIC images/select-disk.ps 4i
.Figure-heading "Disk selection menu"
.Fn disk-selection-menu
.X "ad0"
This screen shows entries for each drive that
.Command sysinstall
has detected; in this example, the system has one ATA (IDE) drive,
.Device ad0 ,
and one SCSI drive, \fIda0\fP.  You only get this screen if you have at least
two drives connected to your machine; otherwise
.Command sysinstall
automatically goes to the next screen.
.P
If you intend to use more than one disk for FreeBSD, you have the choice of
setting up all disks now, or setting the others up after the system is up and
running.
..if complete
We'll look at the latter option in Chapter
.Sref "\*[nchdisks]" ,
on page
.Sref \*[second-disk] .
..endif
.P
To select the disk on which you want to install FreeBSD, move the cursor to the
appropriate line and press the space bar.  The screen you get will probably look
like Figure
.Sref \*[partition-editor-menu] .
Table
.Sref \*[fdisk-tab] \&
explains the meanings of the columns in this display.  The first partition
contains the Master Boot Record, which is exactly one sector long, and the
bootstrap, which can be up to 15 sectors long.  The partitioning tools use the
complete first track: in this case, the geometry information from BIOS says that
it has 63 sectors per track.
.P
.Pn death-to-Microsoft
In this case, the Microsoft file system uses up the whole disk except for the
last track, 1008 sectors (504 kB) at the end of the disk.  Clearly there's not
much left to share.  We have the option of removing the Microsoft partition,
which we'll look at here, or we can shorten it with
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
.Command -n FIPS .
We looked at
.Command -n FIPS
in Chapter
.Sref "\*[nchshareinstall]" ,
page
.Sref \*[FIPS] ,
and we'll look at what to do with the resultant layout on page
.Sref \*[2-partitions] .
.P
.PIC images/partition-editor.1.ps 4i
.Figure-heading "Partition editor menu"
.Fn partition-editor-menu
.Highlight
Don't forget that if you remove a partition, you lose all the data in it.  If
the partition contains anything you want to keep, make sure you have a readable
backup.
.End-highlight
You remove the partition with the \f(CWd\fP command.  After this, your display
looks like:
.PIC images/partition-editor.2.ps 4i
.sp 1.5v
The next step is to allocate a FreeBSD partition.  There are two ways to do
this: if you want to have more than one partition on the drive (for example, if
you share the disk with another operating system), you use the \f(CWc\fP
(create) command.  We'll look at that on page
.Sref \*[2-partitions] .
In this case, though, you want to use the entire disk for FreeBSD, so you choose
the \f(CWa\fP option.  The resultant display is effectively the same as in
Figure
.Sref \*[partition-editor-menu] \/:
the only difference is that the \fIDesc\fP field now shows \f(CWfreebsd\fP
instead of \f(CWfat\fP.
.P
.X "boot selector"
.X "MBR"
.X "master boot record"
.Pn bootmgr
That's all you need to do here: leave
.Command fdisk
by pressing the \f(CWq\fP key.
.Highlight
Don't use the \f(CWW\fP (Write Changes) command here.  It's intended for use
only once the system is up and running.
.End-highlight
.Table-heading "fdisk information"
.Tn fdisk-tab
.TS H
tab(#) ;
lfCWp9w8 | lw62  .
.sp .4v
\s10\fRColumn#Description
_
.TH N
.X "fat"
.X "file allocation table"
Offset#T{
The number of the first sector in the partition.
T}
.sp .4v
Size#T{
The length of the partition in sectors.
T}
.sp .4v
End#T{
The number of the last sector in the partition.
T}
.sp .4v
Name#T{
Where present, this is the device name that FreeBSD assigns to the partition.  In this
example, only the second entry has a name.
T}
.sp .4v
Ptype#T{
The partition type.  Partition type 6 is the Master Boot Record,
which is exactly one track long (note that the header says that this drive has
63 sectors per track).  Type 2 is a regular partition.
T}
.sp .4v
.X "fat"
T{
Desc
T}#T{
A textual description of the kind of partition.  \fIfat\fP\/ stands for \fIFile
Allocation Table\fP, a central part of the Microsoft disk space
allocation strategy.
T}
.sp .4v
T{
Subtype
T}#T{
The partition subtype.  This corresponds to the descriptive text.
T}
.sp .4v
Flags#Can be one or more of the following characters:
.sp .5v
#\f(CW=\fP  The partition is correctly aligned.
#T{
\f(CW>\fP  The partition finishes after cylinder 1024, which used to cause problems for Microsoft.
T}
#\f(CWA\fP  This is the active (bootable) partition.
#\f(CWB\fP  The partition employs BAD144 bad-spot handling.
#\f(CWC\fP  This is a FreeBSD compatibility partition.
#\f(CWR\fP  This partition contains a root file system.
_
.TE
.sp 1.5v
.X "boot selector"
.X "MBR"
On a PC, the next screen asks what kind of \fIboot selector\fP\/ (in other
words, \fIMBR\fP\/) you want.  You don't get this on an Alpha.
.PIC "images/bootmgr.ps" 4i
.Figure-heading "Boot selector menu"
.Fn boot-selector--menu
.X "BootMgr"
.X "BootMgr"
If you plan to have only one operating system on this disk, select
\f(CWStandard\fP.  If you are sharing with another operating system, you should
choose \fIBootMgr\fP\/ instead.  We'll look at this in more detail in the
section on booting the system on page
.Sref "\*[booting]" .
Exit by pressing the tab key until the \f(CWOK\fP tab is highlighted, then press
\fBEnter\fP.
.ne 2i
.Pn select-mbr
.Table-heading "MBR choices"
.TS H
tab(#) ;
lfCWp9 | lw61 .
\fR\s10Choice#Description
_
.TH N
.X "booteasy"
BootMgr#T{
Install the FreeBSD boot manager in the MBR.  This will enable you choose which
partition to boot every time you start the system.
T}
.sp .4v
Standard#T{
Use a standard MBR.  You will be able to boot only from the active partition.
T}
.sp .4v
None#T{
Don't change the MBR.  This is useful if you already have another boot manager
installed.  If no MBR is installed, though, you won't be able to boot from this
disk.
T}
_
.TE
.sp 1.5v
.Tn MBR-choices
.H3 "Shared partitions"
.Pn 2-partitions
If you are installing on a disk shared with another operating system, things are
a little different.  The section continues the example started in Chapter
.Sref "\*[nchshareinstall]" .
When you enter the partition editor, you will see something like:
.XPSPIC -W 4i images/part2.2.ps
.Figure-heading "Shared partitions"
.P
This display shows the two Microsoft partitions, \fIad0s1\fP\/ and \fIad0s2\fP,
which is what you see after using
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
.Command -n FIPS \/;
if you have just installed Microsoft on one partition, the partition
\fIad0s2\fP\/ will not be present.  If it is, you first need to remove it.
\f(BIBe very careful to remove the correct partition\fP.  It's always the second
of the two partitions, in this case \fIad0s2\fP.  Remove the partition by moving
the highlight to the second partition and pressing \f(CWd\fP.  After this, the
display looks like:
.XPSPIC -W 4i images/part2-2.ps
The next step is to allocate a FreeBSD partition with the \f(CWc\fP command.
The menu asks for the size of the partition, and suggests a value of 35899920
sectors, the size of the unused area at the end.  You can edit this value if you
wish, but in this case it's what you want, so just press \fBENTER\fP.  You get
another window asking you for the partition type, and suggesting type 165, the
FreeBSD partition table.  When you accept that, you get:
.XPSPIC -W 4i images/part2-3.ps
The new partition now has a partition type 8 and subtype 165 (\f(CW0xa5\fP),
which identifies it as a FreeBSD partition.
.P
After this, select a boot method as described on page
.Sref \*[select-mbr] \&
and exit the menu with the \f(CWq\fP command.  There are two operating systems
on the disk, so select the \f(CWBootMgr\fP option.
.H2 "Defining file systems"
.X "disk label, specifying"
.Pn disklabel-editor
The next step is to tell the installation program what to put in your FreeBSD
partition.  First, we'll look at the simple case of installing FreeBSD by
itself.  On page
.Sref \*[2-filesystems] \&
we'll look at what differences there are when installing alongside another
operating system on the same disk.
.P
When you select \fILabel\fP, you get the  screen shown in Figure
.Sref \*[label-editor-menu] .
.PIC "images/disk-label-0-a.ps" 4i
.Figure-heading "Label editor menu"
.Fn label-editor-menu
.H3 "What partitions?"
.Pn partition-size
In this example, you have 20 GB of space to divide up.  How should you do it?
You don't have to worry about this issue, since
.Command sysinstall
can do it for you, but we'll see below why this might not be the best choice.
In this section we'll consider how UNIX file systems have changed over the
years, and we'll look at the issues in file system layout nowadays.
.P
When UNIX was young, disks were tiny.  At the time of the third edition of UNIX,
in 1972, the root file system was on a Digital RF-11, a fixed head disk with 512
kB.  The system was growing, and it was no longer possible to keep the entire
system on this disk, so a second file system became essential.  It was mounted
on a Digital RK03 with 2 MB of storage.  To quote from a paper published in the
\fICommunications of the ACM\fP\/ in July 1974:
.Indent
In our installation, for example, the root directory resides on the fixed-head
disk, and the large disk drive, which contains user's files, is mounted by the
system initialization program...
.End-indent
.P
As time went on, UNIX got bigger, but so did the disks.  By the early 80s, disks
were large enough to put
.Directory /
and
.Directory /usr
on the same disk, and it would have been possible to merge
.Directory /
and
.Directory /usr ,
but they didn't, mainly because of reliability concerns.  Since that time, an
additional file system,
.Directory /var ,
has come into common use for frequently changed data, and just recently
.Command sysinstall
has been changed to create a
.Directory /tmp
file system by default.  This is what
.Command sysinstall
does if you ask it to partition automatically:
.PIC "images/disk-label-default.1.ps" 4i
.Figure-heading "Default file system sizes"
It's relatively simple to estimate the size of the root file system, and
.Command sysinstall 's
value of 128 MB is reasonable.  But what about
.Directory /var
and
.Directory /tmp \/?
Is 256 MB too much or too little?  In fact, both file systems put together would
be lost in the 18.7 GB of
.Directory /usr
file system.  Why are things still this way?  Let's look at the advantages and
disadvantages:
.Ls B
.LI
If you write to a file system and the system crashes before all the data can be
written to disk, the data integrity of that file system can be severely
compromised.  For performance reasons, the system doesn't write everything to
disk immediately, so there's quite a reasonable chance of this happening.
.LI
If you have a crash and lose the root file system, recovery can be difficult.
.LI
If a file system fills up, it can cause lots of trouble.  Most messages about
file systems on the \f(CWFreeBSD-questions\fP mailing list are complaining about
file systems filling up.  If you have a large number of small file systems, the
chances are higher that one will fill up while space remains on another.
.LI
On the other hand, some file systems are more important than others.  If the
.Directory /var
file system fills up (due to overly active logging, for example), you may not
worry too much.  If your root file system fills up, you could have serious
problems.
.LI
In single-user mode, only the root file system is mounted.  With the classical
layout, this means that the only programs you can run are those in
.Directory /bin
and
.Directory /sbin .
To run other programs, you must first mount the file system on which they are
located.
.LI
It's nice to keep your personal files separate from the system files.  That way
you can upgrade a system much more easily.
.LI
It's very difficult to estimate in advance the size needs of some file systems.
For example, on some systems
.Directory /var
can be very small, maybe only 2 or 3 MB.  It's hardly worth making a separate
file system for that much data.  On the other hand, other systems, such as ftp
or web servers, may have a
.Directory /var
system of 50 or 100 GB.  How do you choose the correct size for your system?
.LI
When doing backups, it's a good idea to be able to get a file system on a single
tape.
.Le
In the early days of UNIX, system crashes were relatively common, and the damage
they did to the file systems was relatively serious.  Times have changed, and
nowadays file system damage is relatively seldom, particularly on file systems
that have little activity.  On the other hand, disk drives have grown beyond
most peoples' wildest expectations.  The first edition of this book, only six
years ago, showed how to install on a 200 MB drive.  The smallest disk drives in
current production are 20 GB in size, more than will fit on many tapes.
.P
As a result of these considerations, I have changed my recommendations.  In
earlier editions of this book, I recommended putting a small root file system
and a
.Directory /usr
file system on the first (or only) disk on the system.
.Directory /var
was to be a symbolic link to
.Directory /usr/var .
.P
This is still a valid layout, but it has a couple of problems:
.Ls B
.LI
In the example we're looking at,
.Directory /usr
is about 19 GB in size.  Not many people have backup devices that can write
this much data on a single medium.
.LI
Many people had difficulty with the symbolic link to
.Directory /usr/var .
.Le
.ne 10v
As a result, I now recommend:
.Ls B
.LI
Make a single root file system of between 4 and 6 GB.
.LI
Do not have a separate
.Directory /usr
file system.
.LI
Do not have a separate
.Directory /var
file system unless you have a good idea how big it should be.  A good example
might be a web server, where (contrary to FreeBSD's recommendations) it's a good
idea to put the web pages on the
.Directory /var
file system.
.LI
Use the rest of the space on disk for a
.Directory /home
file system, as long as it's possible to back it up on a single tape.  Otherwise
make multiple file systems.
.Directory /home
is the normal directory for user files.
.Le
This layout allows for easy backup of the file systems, and it also allows for
easy upgrading to a new system version: you just need to replace the root file
system.  It's not a perfect fit for all applications, though.  Ultimately you
need to make your own decisions.
.H3 "How much swap space?"
.X "swap, space"
.Pn how-much-swap
Apart from files, you should also have at least one swap partition on your disk.
It's very difficult to predict how much swap space you need.  The
\fIautomatic\fP\/ option gave you 522 MB, slightly more than twice the size of
physical memory.  Maybe you can get by with 64 MB.  Maybe you'll need 2 GB.  How
do you decide?
.P
It's almost impossible to know in advance what your system will require.  Here
are some considerations:
.Ls B
.LI
Swap space is needed for all pages of virtual memory that contain data that is
not locked in memory and that can't be recreated automatically.  This is the
majority of virtual memory in the system.
.LI
Some people use rules of thumb like ``2.5 times the size of physical memory, or
64 MB, whichever is bigger.''  These rules work only by making assumptions about
your workload.  If you're using more than 2.5 times as much swap space as
physical memory, performance will suffer.
.LI
Known memory hogs are X11 and integrated graphical programs such as Netscape and
StarOffice.  If you use these, you will probably need more swap space.  Older
UNIX-based hogs such as Emacs and the GNU C compiler
.Command ( gcc )
are not in the same league.
.LI
You can add additional swap partitions on other disks.  This has the additional
advantage of balancing the disk load if your machine swaps a lot.
.LI
About the only ways to change the size of a swap partition are to add another
partition or to reinstall the system, so if you're not sure, a little bit more
won't do any harm, but too little can really be a problem.
.LI
If your system panics, and memory dumping is enabled, it will write the contents
of memory to the swap partition.  This will obviously not work if your swap
partition is smaller than main memory.  Under these circumstances, the system
refuses to dump, so you will not be able to find the cause of the problems.
.P
The dump routines can only dump to a single partition, so you need one that is
big enough.  If you have 512 MB of memory and two swap partitions of 384 MB
each, you still will not be able to dump.
.LI
Even with light memory loads, the virtual memory system slowly pages out data in
preparation for a possible sudden demand for memory.  This means that it can be
more responsive to such requests.  As a result, you should have at least as much
swap as memory.
.Le
A couple of examples might make this clearer:
.Ls N
.LI
Some years ago I used to run \fIX\fP\/, \fIStarOffice\fP\/, \fINetscape\fP\/ and
a whole lot of other memory-hungry applications on an old 486 with 16 MB.  Sure,
it was really slow, especially when changing from one application to another,
but it worked.  There was not much memory, so it used a lot of swap.
.P
To view the current swap usage, use
.Command pstat .
Here's a typical view of this machine's swap space:
.Dx
$ \f(CBpstat -s\fP
Device          1024-blocks     Used    Avail Capacity  Type
/dev/da0s1b          122880    65148    57668    53%    Interleaved
.De
.LI
At the time of writing I run much more stuff on an AMD Athlon with 512 MB of
memory.  It has lots of swap space, but what I see is:
.Dx
$ \fB pstat -s\fP
Device          1024-blocks     Used    Avail Capacity  Type
/dev/ad0s1b         1048576    14644  1033932     1%    Interleaved
.De
.Le
It's not so important that the Athlon is using less swap: it's using less than
3% of its memory in swap, whereas the 486 used 4 times its memory.  In a
previous edition of this book, I had the example of a Pentium with 96 MB of
memory, which used 43 MB of swap.  Look at it from a different point of view,
and it makes more sense: swap makes up for the lack of real memory, so the 486
was using a total of 80 MB of memory, the Pentium was using 140 MB, and the
Athlon is using 526 MB.  In other words, there is a tendency to be able to say
``the more main memory you have, the less swap you need.''
.P
If, however, you look at it from the point of view of acceptable performance,
you will hear things like ``you need at least one-third of your virtual memory
in real memory.''  That makes sense from a performance point of view, assuming
all processes are relatively active.  And, of course, it's another way of saying
``take twice as much swap as real memory.''
.P
.ne 3v
In summary: be generous in allocating swap space.  If you have the choice, use
more.  If you really can't make up your mind, take 512 MB of swap space or 1 MB
more than the maximum memory size you are likely to install.
.P
.X "UNIX File System"
.X "UFS1"
For the file systems, the column \fIMount\fP\/ now shows the mount points, and
the \fINewfs\fP\/ column contains the letters \f(CWUFS1\fP\/ for \fIUNIX File
System\fP, Version 1, and the letter \f(CWY\fP, indicating that you need to
create a new file system before you can use it.  At this point, you have two
choices: decide for yourself what you want, or let the disk label editor do it
for you.  Let's look at both ways:
.H4 "Creating the file systems"
.Pn fs-create
With these considerations in mind, we'll divide up the disk in the following
manner:
.Ls B
.LI
4 GB for the root file system, which includes
.Directory /usr
and
.Directory /var
.LI
512 MB swap space
.LI
The rest of the disk for the
.Directory /home
file system
.Le
To create a file system, you press \f(CWc\fP.  You get a prompt window asking
for the size of the file system, and offering the entire space.  Enter the size
of the root file system:
.PIC "images/disk-label-1.ps" 4i
.Figure-heading "Specifying partition size"
When you press \fBENTER\fP, you see another prompt asking for the kind of
partition.  Select \f(CWA File System\fP:
.PIC "images/disk-label-2.ps" 4i
.Figure-heading "Selecting partition type"
When you press \fBENTER\fP, you see another prompt asking for the mount point for the
file system.  Enter \f(CW/\fP for the root file system, after which the
display looks like:
.PIC "images/disk-label-3-a.ps" 4i
.Figure-heading "Allocated root file system"
It's not immediately obvious at this point that soft updates are not enabled for
this file system.  Press \f(CWs\fP to enable them, after which the entry in the
\f(CWNewfs\fP column changes from \f(CWUFS1\fP to \f(CWUFS1+S\fP.  See page
.Sref \*[soft-updates] \&
for reasons why you want to use soft updates.
.P
Next, repeat the operation for the swap partition and the
.Directory /home
file system, entering the appropriate values each time.  Don't change the value
offered for the length of
.Directory /home \/:
just use all the remaining space.  At the end, you have:
.PIC "images/disk-label-5-a.ps" 4i
.Figure-heading "Completed partition allocation"
You don't need to enable soft updates for
.Directory /home \/;
that happens automatically.
.P
That's all you need to do.  Exit the menu by pressing \f(CWq\fP.
.H4 "Where you are now"
At this point in the installation, you have told
.Command sysinstall
the overall layout of the disk or disks you intend to use for FreeBSD, and
whether or how you intend to share them with other operating systems.  The next
step is to specify how you want to use the FreeBSD partitions.  First, though,
we'll consider some alternative scenarios.
.H4 "Second time through"
If you have already started an installation and aborted it for some reason after
creating the file systems, things will look a little different when you get to
the label editor.  It will find the partitions, but it won't know the name of
the mount points, so the text under \f(CWMount\fP will be \f(CW<none>\fP.  Under
\f(CWNewfs\fP, you will find an asterisk (\f(CW*\fP) instead of the text
\f(CWUFS1\ Y\fP.  The label editor has found the partitions, but it doesn't know
where to mount the file systems.  Before you can use them, you \fImust\fP\/ tell
the label editor the types and mount points of the UFS partitions.  To do this:
.Ls B
.LI
Position the cursor on each partition in turn.
.LI
Press \f(CWm\fP (Mount).  A window pops up asking for the mount point.  Enter
the name, in this example, first \f(CW/\fP, then press \fBEnter\fP.  The label
editor enters the name of the mount point under \fIMount\fP, and under
\fINewfs\fP\/ it enters \f(CWUFS1\ N\fP\(emit knows that this is a UFS file
system, so it just checks its consistency and doesn't overwrite it.  Repeat this
procedure for
.Directory /home ,
and you're done.
.Pn dos-mount-point
If you are sharing your disk with another system, you can also use this method
to specify mount points for your Microsoft file systems.  Select the Microsoft
partition and specify the name of a mount point.
.LI
Unless you are very sure that the file system is valid, and you really want to
keep the data in the partitions, press \f(CWt\fP to specify that the file system
should be created.  The text \f(CWUFS1\ N\fP changes to \f(CWUFS1\ Y\fP.  If you
leave the \f(CWN\fP there, the commit phase will check the integrity of the file
system with
.Command fsck
rather than creating a new one.
.Le
.SPUP
.H3 "File systems on shared disks"
.Pn 2-filesystems
.X "slice, disk"
.X "partition, disk"
.\" XXX this is tiny!
If you have another operating system on the disk, you'll notice a couple of
differences.  In particular, the label editor menu of Figure
.Sref \*[label-editor-menu] \&
(on page
.Sref \*[label-editor-menu-page] )
will not be empty: instead, you'll see something like this:
.XPSPIC -W 4i images/label2-1.ps
.P
\f(BIBe careful\fP at this point.  The file system shown in the list is the
active Microsoft partition, \fInot\fP\/ a FreeBSD file system.  The important
piece of information here is the fact that we have 17529 MB of free space on the
disk.  We'll create the file systems in that free space in the same way we saw
on page
.Sref \*[fs-create] .
.H2 "Selecting distributions"
.X "distributions, selecting"
.Pn select-distribution
The next step is to decide what to install.  Figure
.Sref \*[select-distribution-menu] \&
.\" XXX fix refs .pageref \*[select-distribution-menu-page]  "on page \*[select-distribution-menu-page] "
shows you the menu you get when you enter \fIDistributions\fP.  A complete
installation of FreeBSD uses about 1 GB of space, so there's little reason to
choose anything else.  Position the cursor on the line \f(CWAll\fP, as shown,
and press the space bar.
.br
.DF
.br
.fam T
.PIC images/select-dist.ps  4i
.Figure-heading "Distribution selection menu"
.Fn select-distribution-menu
.DE
.Aside
Why press the space bar when so far you have been pressing \fBENTER\fP?  Because in
this particular menu, \fBENTER\fP will return you to the upper level menu or simply
continue to the media selection menu, depending on the type of installation
you're doing.  It's one of the strangenesses of
.Command sysinstall .
.End-aside
Next,
.Command sysinstall
asks you if you want to install the Ports Collection.
..if complete
We'll look at the Ports Collection in Chapter
.Sref "\*[nchports]" .
..else
The Ports Collection contains the information necessary for installing over
7,000 applications.
..endif
You don't have to install it now, and it takes much more time than you would
expect from the amount of space that it takes: the Ports Collection consists of
over 150,000 very small files, and copying them to disk can take as long as the
rest of the installation put together.  On the other hand, it's a lot easier to
do now, so if you have the time, you should install them.
.P
Whatever you answer to this question, you are returned to the distribution menu
of Figure
.Sref \*[select-distribution-menu] .
Select \f(CWExit\fP, and you're done selecting your distributions.
.Aside
Earlier versions of
.Command sysinstall
asked you questions about XFree86 at this point.  Nowadays you do that after
completing the installation.
.End-aside
.SPUP
.H4 "Where you are now"
Now
.Command sysinstall
knows the layout of the disk or disks you intend to use for FreeBSD, and what to
put on them.  Next, you specify where to get the data from.
.H2 "Selecting the installation medium"
.X "installation medium, selecting"
.Pn select-medium
.X "menu, media"
The next thing you need to specify is where you will get the data from.  Where
you go now depends on your installation medium.  Figure
.Sref \*[medium-menu] \&
shows the \fIMedia\fP\/ menu.
If you're installing from anything except an ftp server or NFS, you just need to
select your medium and then commit the installation, which we look at on page
.Sref \*[commit] .
If you're installing from media other than CD-ROM, see page
.Sref "\*[alternative-install]" .
.P
At this point,
.Command sysinstall
knows everything it needs to install the software.  It's just waiting for you to
tell it to go ahead.
.P
.PIC images/select-medium.ps 4i
.Figure-heading "Installation medium menu"
.Fn medium-menu
.H2 "Performing the installation"
.Pn commit
So far, everything you have done has had no effect on the disk drives.  If you
change your mind, you can just abort the installation, and the data on your
disks will be unchanged.  That changes completely in the next step, which you
call \fIcommitting\fP\/ the installation.  Now is the big moment.  You've set up
your partitions, decided what you want to install and from where.  Now you do
it.
.P
If you are installing with the Custom installation, you need to select
\fICommit\fP explicitly.  The Standard installation asks you if you want to
proceed:
.Dx
Last Chance!  Are you SURE you want continue the installation?

If you're running this on an existing system, we STRONGLY
encourage you to make proper backups before proceeding.
We take no responsibility for lost disk contents!
.De
When you answer \f(CWyes\fP,
.Command sysinstall
does what we've been preparing for:
.Blist
.LI
It creates the partitions and disk partitions.
.LI
It creates the file system structures in the file system partitions, or it
checks them, depending on what you chose in the label editor.
.LI
It mounts the file systems and swap space.
.LI
It installs the software on the system.
.Le
.ne 2v
After the file systems are mounted, and before installing the software,
.Command sysinstall
starts processes on two other virtual terminals.\*F
.FS
See page
.Sref \*[vt] \&
for an explanation of virtual terminals.
.FE
On
.Device ttyv1
you get log output showing you what's going on behind the scenes.  You can
switch to it with \fBALT-F2\fP.  Right at the beginning you'll see a whole lot
of error messages as
.Command sysinstall
tries to initialize every device it can think of.  Don't worry about them,
they're normal.  To get back to the install screen, press \fBALT-F1\fP.
.P
In addition, after
.Command sysinstall
mounts the root file system, it starts an interactive shell on
.Device ttyv3 .
You can use it if something goes wrong, or simply to watch what's going on while
you're installing.  You switch to it with \fBALT-F4\fP.
.P
After installing all the files,
.Command sysinstall
asks:
.Dx
Visit the general configuration menu for a chance to set
any last options?
.De
You really have the choice here.  You can answer \f(CWYes\fP and continue, or
you can reboot: the system is now runnable.  In all probability, though, you
will have additional installation work to do, so it's worth continuing.  We'll
look at that in the following chapter.
..if complete
.H2 "Installing on an Alpha system"
.X "installing on Alpha"
.X "Alpha, installing on"
.Pn alpha-install
Installing FreeBSD on an Alpha (officially Compaq AXP) has a few minor
differences due to the hardware itself.  In principle, you perform the same
steps to install FreeBSD on the Alpha architecture that you perform for the
Intel architecture.  See page
.Sref \*[alpha-concepts] \&
for some differences.
.P
The easiest type of installation is from CD-ROM.  If you have a supported CD-ROM
drive and a FreeBSD installation CD for Alpha, you can start the installation by
building a set of FreeBSD boot floppies from the files
.File floppies/kern.flp
and
.File floppies/mfsroot.flp
as described for the Intel architecture on page
.Sref \*[make-floppy] .
Use the CD-ROM marked ``Alpha installation.''  From the SRM console prompt,
insert the
.File kern.flp
floppy and type the following command to start the
installation:
.Dx
>>>\f(CBboot dva0\fP
.De
Insert the
.File mfsroot.flp
floppy when prompted and you will end up at the
first screen of the install program.  You can then continue as for the Intel
architecture on page
.Sref \*[boot-install-kernel] .
.P
To install over the Net, fetch the floppy images from the ftp site, boot as
above, then proceed as for the Intel architecture.
.P
.ne 4v
Once the install procedure has finished, you will be able to start FreeBSD/Alpha
by typing something like this to the SRM prompt:
.Dx
>>>\f(CBboot dkc0\fP
.De
This instructs the firmware to boot the specified disk. To find the SRM names of
disks in your machine, use the \f(CWshow device\fP command:
.Dx
>>>\f(CBshow device\fP
dka0.0.0.4.0               DKA0           TOSHIBA CD-ROM XM-57  3476
dkc0.0.0.1009.0            DKC0                       RZ1BB-BS  0658
dkc100.1.0.1009.0          DKC100             SEAGATE ST34501W  0015
dva0.0.0.0.1               DVA0
ewa0.0.0.3.0               EWA0              00-00-F8-75-6D-01
pkc0.7.0.1009.0            PKC0                  SCSI Bus ID 7  5.27
pqa0.0.0.4.0               PQA0                       PCI EIDE
.De
This example comes from a Digital Personal Workstation 433au and shows three
disks attached to the machine. The first is a CD-ROM called \fIdka0\fP\/ and the
other two are disks and are called \fIdkc0\fP\/ and \fIdkc100\fP\/ respectively.
.P
You can specify which kernel file to load and what boot options to use
with the \f(CW-file\fP and \f(CW-flags\fP options to boot:
.Dx
>>>\f(CBboot -file kernel.old -flags s\fP
.De
To make FreeBSD/Alpha boot automatically, use these commands:
.Dx
>>>\f(CBset boot_osflags a\fP
>>>\f(CBset bootdef_dev dkc0\fP
>>>\f(CBset auto_action BOOT\fP
.De
.SPUP
.H2 "Upgrading an old version of FreeBSD"
.Pn upgrade-install
Paradoxically, upgrading an old version of FreeBSD is more complicated than
installing from scratch.  The reason is that you almost certainly want to keep
your old configuration.  There's enough material in this topic to fill a
chapter, so that's what I've done: see Chapter
.Sref "\*[nchcurrent]" ,
for more details on how to upgrade a system.
.H2 "How to uninstall FreeBSD"
.X "uninstall"
.X "removing FreeBSD from disk"
What, you want to remove FreeBSD?  Why would you want to do that?
.P
Seriously, if you decide you want to completely remove FreeBSD from the system,
this is no longer a FreeBSD issue, it's an issue of whatever system you use to
replace it.  For example, on page
.Sref \*[death-to-Microsoft] \&
we saw how to remove a Microsoft partition and replace it with FreeBSD; no
Microsoft software was needed to remove it.  In the same way, you don't need any
help from FreeBSD if you want to replace it with a different operating system.
.H2 "If things go wrong"
In this section, we'll look at the most common installation problems.  Many of
these are things that once used to happen and haven't been seen for some time:
.Command sysinstall
has improved considerably, and modern hardware is much more reliable and easy to
configure.  You can find additional information on this topic in the section
\fIKnown Hardware Problems\fP\/ in the file
.File INSTALL.TXT
on the first CD-ROM.
.H3 "Problems with sysinstall"
.Command sysinstall
is intended to be easy to use, but it is not very tolerant of errors.  You may
well find that you enter something by mistake and can't get back to where you
want to be.  In case of doubt, if you haven't yet committed to the install, you
can always just reboot.
.P
.H3 "Problems with CD-ROM installation"
If you select to install from CD-ROM, you may get the message:
.Dx
No CD-ROM device found
.De
This might even happen if you have booted from CD-ROM!  The most common reasons
for this problem are:
.Ls B
.LI
You booted from floppy and forgot to put the CD-ROM in the drive before you
booted.  Sorry, this is a current limitation of the boot process.  Restart the
installation (press \fBCtrl-Alt-DEL\fP or the reset button, or power cycle the
computer).
.LI
You are using an ATAPI CD-ROM drive that doesn't quite fit the specification.
In this case you need help from the FreeBSD developers.  Send a message to
\f(CWFreeBSD-questions@FreeBSD.org\fP and describe your CD-ROM as accurately as
you can.
.Le
.H3 "Can't boot"
One of the most terrifying things after installing FreeBSD is if you find that
the machine just won't boot.  This is particularly bad if you have important
data on the disk (either another operating system, or data from a previous
installation of FreeBSD).
.P
At this point, seasoned hackers tend to shrug their shoulders and point out that
you still have the backup you made before you did do the installation.  If you
tell them you didn't do a backup, they tend to shrug again and move on to
something else.
.P
Still, all is probably not lost.  The most frequent causes of boot failure are
an incorrect boot installation or geometry problems.  In addition, it's possible
that the system might hang and never complete the boot process.  All of these
problems are much less common than they used to be, and a lot of the information
about how to address them is a few years old, as they haven't been seen since.
.H3 "Incorrect boot installation"
It's possible to forget to install the bootstrap, or even to wipe it the
existing bootstrap.  That sounds like a big problem, but in fact it's easy
enough to recover from.  Refer to the description of the boot process on page
.Sref "\*[booting]" ,
and boot from floppy disk or CD-ROM.  Interrupt the boot process with the space
bar.  You might see:
.Dx
BTX loader 1.00  BTX version is 1.01
BIOS drive A: is disk0
BIOS drive C: is disk1
BIOS drive D: is disk1
BIOS 639kB/130048kB available memory

FreeBSD/i386 bootstrap loader, Revision 0.8
(grog@freebie.example.com, Thu Jun 13 13:06:03 CST 2002)
Loading /boot/defaults/loader.conf

Hit [Enter] to boot immediately, or any other key for command prompt.
Booting [kernel] in 6 seconds...        \fIpress space bar here\fP\/
ok \f(CBunload\fP                               \fIunload the current kernel\fP\/
ok \f(CBset currdev=disk1s1a\fP                 \fIand set the location of the new one\fP\/
ok \f(CBload /boot/kernel/kernel\fP             \fIload the kernel\fP
ok \f(CBboot\fP                                 \fIthen start it\fP\/
.De
This boots from the drive
.Device ad0s1a ,
assuming that you are using IDE drives.  The correspondence between the name
.Device ad0s1a
and \fIdisk1s1a\fP\/ goes via the information at the top of the example: BTX
only knows the BIOS names, so you'd normally be looking for the first partition
on drive
.Directory C: .
After booting, install the correct bootstrap with \f(CWbsdlabel -B\fP\/ or
.Command boot0cfg ,
and you should be able to boot from hard disk again.
.H3 "Geometry problems"
.X "booteasy"
Things might continue a bit further: you elect to install \fIbooteasy\fP, and
when you boot, you get the Boot Manager prompt, but it just prints \f(CWF?\fP at
the boot menu and won't accept any input.  In this case, you may have set the
hard disk geometry incorrectly in the partition editor when you installed
FreeBSD.  Go back into the partition editor and specify the correct geometry for
your hard disk.  You may need to reinstall FreeBSD from the beginning if this
happens.
.P
It used to be relatively common that
.Command sysinstall
couldn't calculate the correct geometry for a disk, and that as a result you
could install a system, but it wouldn't boot.  Since those days,
.Command sysinstall
has become a lot smarter, but it's still barely possible that you'll run into
this problem.
.P
If you can't figure out the correct geometry for your machine, and even if you
don't want to run Microsoft on your machine, try installing a small Microsoft
partition at the beginning of the disk and install FreeBSD after that.  The
install program sees the Microsoft partition and tries to infer the correct
geometry from it, which usually works.  After the partition editor has accepted
the geometry, you can remove the Microsoft partition again.  If you are sharing
your machine with Microsoft, make sure that the Microsoft partition is before
the FreeBSD partition.
.P
Alternatively, if you don't want to share your disk with any other operating
system, select the option to use the entire disk (\f(CWa\fP in the partition
editor).  You're less likely to have problems with this option.
.H3 "System hangs during boot"
A number of problems may lead to the system hanging during the boot process.
All the known problems have been eliminated, but there's always the chance that
something new will crop up.  In general, the problems are related to hardware
probes, and the most important indication is the point at which the boot failed.
It's worth repeating the boot with the verbose flag: again,
refer
to the description of the boot process on page
.Sref "\*[booting]" .
Interrupt the boot process with the space bar and enter:
.Dx
Hit [Enter] to boot immediately, or any other key for command prompt.
Booting [kernel] in 6 seconds...        \fIpress space bar here\fP\/
ok \f(CBset boot_verbose\fP                     \fIset a verbose boot\fP\/
ok \f(CBboot\fP                                 \fIthen continue\fP\/
.De
This flag gives you additional information that might help diagnose the
problem.  See Chapter
.Sref "\*[nchstarting]" \&
for more details of what the output means.
.P
If you're using ISA cards, you may need to reconfigure the card to match the
kernel, or change the file
.File /boot/device.hints
to match the card settings.  See the example on page
.Sref "\*[IO-config]" .
Older versions of FreeBSD used to have a program called
.Command UserConfig
to perform this function, but it is no longer supported.
.H3 "System boots, but doesn't run correctly"
If you get the system installed to the point where you can start it, but it
doesn't run quite the way you want, \f(BIdon't reinstall\fP.  In most cases,
reinstallation won't help.  Instead, try to find the cause of the
problem\(emwith the aid of the \f(CWFreeBSD-questions\fP mailing list if
necessary\(emand fix the problem.
.H3 "Root file system fills up"
You might find that the installation completes successfully, and you get your
system up and running, but almost before you know it, the root file system fills
up.  This is relatively unlikely if you follow my recommendation to have one
file system for
.Directory / ,
.Directory /usr
and
.Directory /var ,
but if you follow the default recommendations, it's a possibility.  It could be,
of course, that you just haven't made it big enough\(emFreeBSD root file systems
have got bigger over the years.  In the first edition of this book I recommended
32 MB ``to be on the safe side.''  Nowadays the default is 128 MB.
.P
On the other hand, maybe you already have an 128 MB root file system, and it
still fills up.  In this case, check where you have put your
.Directory /tmp
and
.Directory /var
file systems.  There's a good chance that they're on the root file system, and
that's why it's filling up.
.H3 "Panic"
.X "panic"
.Pn panic
Sometimes the system gets into so much trouble that it can't continue.  It
should notice this situation and stop more or less gracefully.  You might see a
message like:
.Dx
panic: free vnode isn't

Syncing disks 14 13 9 5 5 5 5 5 5 5 giving up

dumping to dev 20001 offset 0
dump 16 32 48 64 80 96 112 128 succeeded
Automatic reboot in 15 seconds - press a key on the console to abort
Reboooting...
.De
Just because the system has panicked doesn't mean that you should panic too.
It's a sorry fact of life that software contains bugs.  Many commercial systems
just crash when they hit a bug, and you never know why, or they print a message
like \f(CWGeneral protection fault\fP, which doesn't tell you very much either.
When a UNIX system panics, it usually gives you more detailed information\(emin
this example, the reason is \fIfree vnode isn't\fP.  You may not be any the
wiser for a message like this (it tells you that the file system handling has
got confused about the current state of storage on a disk), but other people
might.  In particular, if you \fIdo\fP\/ get a panic and you ask for help on
\f(CWFreeBSD-questions\fP, please don't just say ``My system panicked, what do I
do?''  The first answer\(emif you get one\(emwill be ``What was the panic
string?''  The second will be ``Where's the dump?''
.P
After panicking, the system tries to write file system buffers back to disk so
that they don't get lost.  This is not always possible, as we see on the second
line of this example.  It started off with 14 buffers to write, but it only
managed to write 9 of them, possibly because it was confused about the state of
the disk.  This can mean that you will have difficulties after rebooting, but it
might also mean that the system was wrong in its assumptions about the number of
buffers needed to be written.
.P
.X "dumping the system"
In addition to telling you the cause of the panic, FreeBSD will optionally copy
the current contents of memory to the swap file for post-mortem analysis.  This
is called \fIdumping\fP\/ the system, and is shown on the next two lines.  To
enable dumping, you need to specify where the dump should be written.  In
.File /etc/defaults/rc.conf ,
you will find:
.Dx
dumpdev="NO"            # Device name to crashdump to (if enabled).
.De
To enable dumping, put something like this in
.File /boot/loader.conf \/:
.Dx
dumpdev="/dev/ad0s1b"
.De
This enables the dumps to be taken even if a panic occurs before the system
reads the
.File /etc/rc.conf
file.  Make sure that the name of the \f(CWdumpdev\fP corresponds to a swap
partition with at least as much space as your total memory.  You can use
.Command pstat
to check this:
.Dx
# \f(CBpstat -s\fP
Device      1024-blocks     Used    Avail Capacity  Type
/dev/ad0s1b       51200    50108     1028    98%    Interleaved
/dev/da0s1b       66036    51356    14616    78%    Interleaved
/dev/da2s1b      204800    51220   153516    25%    Interleaved
Total            321844   152684   169160    47%
.De
As long as this machine doesn't have more than about 192 MB of memory, it will
be possible to take a dump on
.Device da2s1b .
.P
In addition, ensure that you have a directory called
.Directory /var/crash .
After rebooting, the system first checks the integrity of the file systems, then
it checks for the presence of a dump.  If it finds one, it copies the dump and
the current kernel to
.Directory /var/crash .
.P
It's always worth enabling dumping, assuming your swap space is at least as
large as your memory.  You can analyze the dumps with
.Command gdb \(emsee
page
.Sref \*[dumpanal] \&
for more details.
.P
.X "debug kernel"
To get the best results from a dump analysis, you need a \fIdebug kernel\fP.
This kernel is identical to a normal kernel, but it includes a lot of
information that can be used for dump analysis.  See page
.Sref \*[debug-kernel] \&
for details of how to build a debug kernel.  You never know when you might run
into a problem, so I highly recommend that you use a debug kernel at all times.
It doesn't have any effect on the performance of the system.
.H3 "Fixing a broken installation"
.Pn recovery
.X "fixit floppy"
A really massive crash may damage your system to such an extent that you need to
reinstall the whole system.  For example, if you overwrite your hard disk from
start to finish, you don't have any other choice.  In many cases, though, the
damage is repairable.  Sometimes, though, you can't start the system to fix the
problems.  In this case, you have two possibilities:
.Ls B
.LI
.X "CD-ROM, Live File System"
Boot from the second CD-ROM (\fILive Filesystem\fP\/).  It will be mounted as
the root file system.
.LI
.X "fixit floppy"
Boot from the \fIFixit\fP\/ floppy.  The Fixit floppy is in the distribution in
the same directory as the boot diskette, \fIfloppies\fP\/.  Just copy
.File floppies/fixit.flp
to a disk in the same way as described for boot
diskettes on page
.Sref \*[make-floppy] .
To use the fixit floppy, first boot with the boot diskette and select ``Fixit
floppy'' from the main menu.  The Fixit floppy will be mounted under the root
MFS as
.Directory /mnt2 .
.Le
In either case, the hard disks aren't mounted; you might want to do repair work
on them before any other access.
.P
Use this option only if you have a good understanding of the system installation
process.  Depending on the damage, you may or may not be successful.  If you
have a recent backup of your system, it might be faster to perform a complete
installation than to try to fix what's left, and after a reinstallation you can
be more confident that the system is correctly installed.
.H2 "Alternative installation methods"
.Pn alternative-install
The description at the beginning of this chapter applied to the most common
installation method, from CD-ROM.  In the following sections we'll look at the
relatively minor differences needed to install from other media.  The choices
you have are, in order of decreasing attractiveness:
.Ls B
.LI
Over the network.  You have the choice of \fIftp\fP\/ or NFS connection.
If you're connected to the Internet and you're not in a hurry, you can load
directly from one of the distribution sites described in the FreeBSD handbook.
.LI
From a locally mounted disk partition, either FreeBSD (if you have already
installed it) or Microsoft.
.LI
From floppy disk.  This is only for masochists or people who really have almost
no hardware: depending on the extent of the installation, you will need up to
250 disks, and at least one of them is bound to have an I/O error.  And don't
forget that a CD-ROM drive costs a lot less than 250 floppies.
.Le
.ig
There are two steps involved here: preparing the data for installation, which
we'll look at in the following sections, and performing the installation, which
we'll look at starting on page
.Sref "\*[install-alternates]" .
..
.SPUP
.H3 "Preparing boot floppies"
.Pn make-floppy
.X "boot floppy, preparing"
.X "boot disk"
If your machine is no longer the youngest, you may be able to read the CD-ROM
drive, but not boot from it.  In this case, you'll need to boot from floppy.  If
you are using 1.44 MB floppies, you will need two or three of them, the
\fIKernel Disk\fP\/ and the \fIMFS Root Disk\fP\/ and possibly the \fIDrivers
Disk\fP\/ to boot the installation programs.  If you are using 2.88 MB floppies
or a LS-120 disk, you can copy the single \fIBoot Disk\fP, which is 2.88 MB
long, instead of the kernel and MFS root disks.  The images of these floppies
are on the CD-ROM distribution in the files
.File floppies/kern.flp ,
.File floppies/mfsroot.flp ,
.File floppies/drivers.flp
and
.File floppies/boot.flp
respectively.  If you have your CD-ROM mounted on a Microsoft system, they may
be called
.File -n FLOPPIES\eKERN.FLP ,
.File -n FLOPPIES\eMFSROOT.FLP ,
.File -n FLOPPIES\eDRIVERS.FLP
and
.File -n FLOPPIES\eBOOT.FLP
respectively.  The bootstrap does not recover bad blocks, so the floppy must be
100% readable.
.P
The way you get the boot disk image onto a real floppy depends on the operating
system you use.  If you are using any flavour of UNIX, just perform something
like:
.Dx
# \f(CBdd if=/cdrom/floppies/kern.flp of=/dev/fd0 bs=36b\fP
.sp .5v
\fIchange the floppy\fP\/
.sp .5v
# \f(CBdd if=/cdrom/floppies/mfsroot.flp of=/dev/fd0 bs=36b\fP
\fIchange the floppy\fP\/
.sp .5v
# \f(CBdd if=/cdrom/floppies/drivers.flp of=/dev/fd0 bs=36b\fP
.De
.X "fd0, device"
.X "device, fd0"
This assumes that your software is on CD-ROM, and that it is mounted on the
directory
.Directory /cdrom .
It also assumes that your floppy drive is called
.Device fd0 .
This is the FreeBSD name as of Release 5.0, and it's also the name that Linux
uses.  Older FreeBSD and other BSD systems refer to it as
.Device fd0c .
.P
The
.Command dd
implementation of some versions of UNIX, particularly older System V variants,
may complain about the option \f(CWbs=36b\fP.  If this happens, just leave it
out.  It might take up to 10 minutes to write the floppy, but it will work, and
it will make you appreciate FreeBSD all the more.
.P
If you have to create the boot floppy from Microsoft, use the program
.Command FDIMAGE.EXE ,
which is in the
.Directory tools
directory of the first CD-ROM.
.H3 "Booting from floppy"
.X "booting, from floppy"
.Pn floppy-boot
In almost all cases where you don't boot from CD-ROM, you'll boot from floppy,
no matter what medium you are installing from.  If you are installing from
CD-ROM, \fIput the CD-ROM in the drive\fP\/ before booting.  The installation
may fail if you boot before inserting the CD-ROM.
.P
.X "A: drive"
Boot the system in the normal manner from the first floppy (the one containing
the
.File kern.flp
image).  After loading the kernel, the system will print the message:
.Dx
Please insert MFS root floppy and press enter:
.De
After you replace the floppy and press enter, the boot procedure carries on as
before.
.P
If you're using the 2.88 MB image on a 2.88 MB floppy or an LS-120 drive, you
have everything you need on the one disk, so you don't get the prompt to change
the disk.  Depending on your hardware, you may later get a prompt to install
additional drivers from the driver floppy.
.H3 "Installing via ftp"
.X "installing, from Internet"
.Pn install-alternates
.Pn ftp-install
The fun way to install FreeBSD is via the Internet, but it's not always the best
choice.  There's a lot of data to transfer, and unless you have a really
high-speed, non-overloaded connection to the server, it could take forever.  On
the other hand, of course, if you have the software on another machine on the
same LAN, and the system on which you want to install FreeBSD doesn't have a
CD-ROM drive, these conditions are fulfilled, and this could be for you.  Before
you decide, though, read about the alternative of NFS installation below: if you
don't have an ftp server with the files already installed, it's a lot easier to
set up an NFS installation.
.P
There are two ftp installation modes you can use:
.Ls B
.LI
Regular \fIftp\fP\/ mode does not work through most firewalls but will often
work best with older ftp servers that do not support passive mode.  Use this
mode if your connection hangs with passive mode.
.LI
.X "ftp, passive"
If you need to pass through firewalls that do not allow incoming connections,
try \fIpassive ftp\fP.
.Le
Whichever mode of installation and whichever remote machine you choose, you need
to have access to the remote machine.  The easiest and most common way to ensure
access is to use anonymous ftp.  If you're installing from another FreeBSD
machine, read how to install anonymous ftp on page
.Sref \*[anonymous-ftp] .
This information is also generally correct for other UNIX systems.
.H4 "Setting up the ftp server"
.X "\*[Fver]-RELEASE"
Put the FreeBSD distribution in the public ftp directory of the ftp server.  On
BSD systems, this will be the home directory of user \f(CWftp\fP, which in
FreeBSD defaults to
.Directory /var/spool/ftp .
The name of the directory is the name of the release, which in this example
we'll assume to be \fI\*[Fver]-RELEASE\fP.
.\" .ad l
.\" .nh
You can put this directory in a subdirectory of
.Directory /var/spool/ftp ,
for example
\fI/var/spool/ftp/FreeBSD/\*[Fver]-RELEASE\fP,
.\" .hy
.\" .ad
but the only optional part in this example is the parent directory
\fIFreeBSD\fP.
.P
This directory has a slightly different structure from the CD-ROM distribution.
To set it up, assuming you have your distribution CD-ROM mounted on
.Directory /cdrom ,
and that you are installing in the directory
\fI/var/spool/ftp/FreeBSD/\*[Fver]-RELEASE\fP, perform the following steps:
.Dx
# \f(CBcd /var/spool/ftp/FreeBSD/\*[Fver]-RELEASE\fP
# \f(CBmkdir floppies\fP
# \f(CBcd floppies\fP
# \f(CBcp /cdrom/floppies/* .\fP                        \fIdon't omit the \f(CB.\fI at the end\f(CW
# \f(CBcd /cdrom\fP                                     \fIthe distribution directory on CD-ROM\f(CW
# \f(CBtar cf - . | (cd /var/spool/ftp/FreeBSD/\*[Fver]-RELEASE; tar xvf -)\fP
.De
This copies all the directories of
.Directory /cdrom
into \fI/var/spool/ftp/FreeBSD/\*[Fver]-RELEASE\fP.  For a minimal installation,
you need only the directory
.Directory base .
To just install \fIbase\fP\/ rather
than all of the distribution, change the last line of the example above to:
.Dx
# \f(CBmkdir base\fP
# \f(CBcp /cdrom/base/* base\fP
.De
.SPUP
.H3 "Installing via ftp"
On page
.Sref "\*[medium-menu-page]" \&
we saw the media select menu.  Figure
.Sref \*[ftp-server-menu] \&
.\"XXX .pageref \*[ftp-server-menu-page] "on page \*[ftp-server-menu-page]"
shows the menu you get when you select \fIFTP\fP or \fIFTP Passive\fP.
.DF
.fam T
.PIC images/select-server.ps 4i
.Figure-heading "Selecting ftp server"
.Fn ftp-server-menu
.DE
.Pn install-set-hostname
To see the remainder of the sites, use the \fBPageDown\fP key.  Let's assume you
want to install from \fIpresto\fP, a system on the local network.
\fIpresto\fP\/ isn't on this list, of course, so you select \f(CWURL\fP.
Another menu appears, asking for an ftp pathname in the URL form
\f(CWftp://\fIhostname\f(CW/\fIpathname\fR.  \fIhostname\fP\/ is the name of the
system, in this case \fIpresto.example.org\fP, and \fIpathname\fP\/ is the path
relative to the anonymous ftp directory, which on FreeBSD systems is usually
.Directory /var/spool/ftp .
The install program knows its version number, and it attaches it to the name you
supply.
.Aside
You can change the version number from the options menu, for example to install
a snapshot of a newer release of FreeBSD.
.End-aside
In this case, we're installing Release
.Sref \*[Fver] \&
of FreeBSD, and it's in the directory
.Directory -n /var/spool/ftp/pub/FreeBSD/\*[Fver]-RELEASE .
.Command sysinstall
knows the \fI\*[Fver]-RELEASE\fP, so you enter only
\fIftp://presto.example.org/pub/FreeBSD\fP.  The next menu asks you to configure
your network.  This is the same menu that you would normally fill out at the
end of the installation\(emsee page
.Sref \*[net-setup] \&
for details.
.hy
.P
This information is used to set up the machine after installation, so it pays to
fill out this information correctly.  After entering this information, continue
with \fICommit\fP\/ (on page
.Sref "\*[commit]" \/).
.H3 "Installing via NFS"
.Pn nfs-install-setup
If you're installing from a CD-ROM drive on another system in the local network,
you might find an installation via ftp too complicated for your liking.
Installation is a lot easier if the other system supports NFS.  Before you
start, make sure you have the CD-ROM mounted on the remote machine, and that the
remote machine is exporting the file system (in System V terminology, exporting
is called \fIsharing\fP\/).  When prompted for the name of the directory,
specify the name of the directory on which the CD-ROM is mounted.  For example,
if the CD-ROM is mounted on directory
.Directory /cdrom
on the system \fIpresto.example.org\fP, enter \f(CBpresto.example.org:/cdrom\fP\|.
That's all there is to it!
.DF
.fam T
.PIC images/nfs-select.ps 4i
.Figure-heading "Specifying NFS file system"
.Fn nfs-install-menu
.DE
.Aside
Older versions of FreeBSD stored the distribution on a subdirectory \fIdists\fP.
Newer versions store it in the root directory of the CD-ROM.
.End-aside
Next, you give this information to
.Command sysinstall ,
as shown in Figure
.Sref \*[nfs-install-menu] .
After entering this information,
.Command sysinstall
asks you to configure an interface.  This is the same procedure that you would
otherwise do after installation\(emsee page
.Sref \*[net-setup] .
After performing this configuration, you continue with \fICommit\fP\/ (on page
.Sref "\*[commit]" \/).
.H3 "Installing from a Microsoft partition"
On the Intel architecture you can also install from a primary Microsoft
partition on the first disk.  To prepare for installation from a Microsoft
partition, copy the files from the distribution into a directory called
\fIC:\eFREEBSD\fP.  For example, to do a minimal installation of FreeBSD from
Microsoft using files copied from a CD-ROM, copy the directories
.Directory floppies
and
.Directory base
to the Microsoft directories
\fIC:\eFREEBSD\eFLOPPIES\fP\/ and \fIC:\eFREEBSD\eBIN\fP\/ respectively.
You need the directory
.Directory -n FLOPPIES
because that's where
.Command sysinstall
looks for the
.File boot.flp ,
the first image in every installation.
.P
The only required directory is
.Directory base .
You can include as many other
directories as you want, but be sure to maintain the directory structure.  In
other words, if you also wanted to install \fIXF86336\fP\/ and
.Directory manpages ,
you would copy them to \fIC:\eFREEBSD\eXF86336\fP and
\fIC:\eFREEBSD\eMANPAGES\fP.
.H3 "Creating floppies for a floppy installation"
.Pn create-floppies
Installation from floppy disk is definitely the worst choice you have.  You will
need nearly 50 floppies for the minimum installation, and about 250 for the
complete installation.  The chance of one of them being bad is high.  Most
problems on a floppy install can be traced to bad media, or differences in
alignment between the media and the drive in which they are used, so:
.Highlight
Before starting, format all floppies in the drive you intend to use, even if
they are preformatted.
.End-highlight
.P
.X "RAWRITE.EXE, MS-DOS command"
The first two floppies you'll need are the Kernel floppy and the MFS Root
floppy, which were described earlier.
.P
In addition, you need at minimum as many floppies as it takes to hold all files
in the
.Directory base
directory, which contains the binary distribution.  Read the file
.File LAYOUT.TXT
paying special attention to the ``Distribution format'' section, which describes
which files you need.
.P
If you're creating the floppies on a FreeBSD machine, you can put  \fIufs\fP\/
file systems on the floppies instead:
.Dx
# \f(CBfdformat -f 1440 fd0.1440\fP
# \f(CBbsdlabel -w fd0.1440 floppy3\fP
# \f(CBnewfs -t 2 -u 18 -l 1 -i 65536 /dev/fd0\fP
.De
Next, copy the files to the floppies.  The distribution files are split into
chunks that will fit exactly on a conventional 1.44MB floppy.  Copy one file to
each floppy.  Make very sure to put the file
.File base.inf
on the first floppy; it is needed to find out how many floppies to read.
.P
The installation itself is straightforward enough: follow the instructions
starting on page
.Sref \*[building-partition-table] ,
select \fIFloppy\fP\/ in the installation medium menu on page
.Sref \*[select-medium] ,
then follow the prompts.
