.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: introduction.mm,v 4.26 2003/06/30 06:47:54 grog Exp grog $
.Chapter \*[nchintro] "Introduction"
FreeBSD is a free operating system derived from AT&T's \fIUNIX\fP\/ operating
system.\*F
.FS
FreeBSD no longer contains any AT&T proprietary code, so it may be distributed
freely.  See page
.Sref \*[history] \&
for more details.
.FE
It runs on the following platforms:
.Ls B
.LI
Computers based on the Intel i386 CPU architecture, including the 386, 486 and
Pentium families of processors, and compatible CPUs from AMD and Cyrix.
.LI
The Compaq/Digital Alpha processor.
.LI
64 bit SPARC machines from Sun Microsystems.
.LI
In addition, significant development efforts are going towards porting FreeBSD
to other hardware, notably the Intel 64 bit architecture and the IBM/Motorola
PowerPC architecture.
.Le
This book describes the released versions of FreeBSD for Intel and Alpha
processors.  Current support for SPARC 64 processors is changing too fast for it
to be practical to give details specific to this processor, but nearly
everything in this book also applies to SPARC 64.
.H2 "How to use this book"
This book is intended for a number of different audiences.  It attempts to
present the material without too many forward references.  It contains the
following parts:
.Ls
.LI
The first part, Chapters
.Sref \*[nchintro] \&
to
.Sref \*[nchpostinstall] ,
tells you how to install FreeBSD and what to do if things go wrong.
.LI
Chapters
.Sref \*[nchunixref] \&
to
.Sref \*[nchprinters] \&
introduce you to life with FreeBSD, including setting up optional features and
system administration.
.LI
Chapters
.Sref \*[nchnetintro] \&
to
.\" XXX .Sref \*[nchmicronet] \&
.Sref \*[nchmta] \&
introduce you to FreeBSD's rich network support.
.LI
Finally, Chapters
.Sref "\*[nchxtheory]" \&
to
.Sref "\*[nchbuild]" \&
look at system administration topics that build on all the preceding material.
.Le
In more detail, we'll discuss the following subjects:
.Ls B
.LI
In the rest of this chapter, we'll look at what FreeBSD is, what you need to run
it, and what resources are available, including FreeBSD's features and history,
how it compares to other free UNIX-like operating systems, other sources of
information about FreeBSD, the world-wide FreeBSD community, and support for
FreeBSD.  In addition, we'll look at the BSD's daemon emblem.
.LI
.Sref "\*[chconcepts]" ,
discusses the installation requirements and theoretical background of installing
FreeBSD.
.LI
.Sref "\*[chquickinstall]" ,
presents a quick overview of the installation process.  If you're reasonably
experienced, this may be all you need to install FreeBSD.
.LI
In
.Sref "\*[chshareinstall]" ,
we'll look at preparing to install FreeBSD on a system that already contains
another operating system.
.LI
In
.Sref "\*[chinstall]" ,
we'll walk through a typical installation in detail.
.LI
.Sref "\*[chpostinstall]" ,
explains the configuration you need to do after installation to get a complete
functional system.
.LI
.Sref "\*[chunixref]" ,
presents a number of aspects of FreeBSD that are of interest to newcomers
(particularly from a Microsoft environment).  We'll look at setting up a
``desktop,'' the concept of \fIusers\fP and file naming.  We'll also consider
the basics of using the \fIshell\fP and editor, and how to shut down the
machine.
.LI
.Sref "\*[chunixadmin]" ,
goes into more detail about the specifics of working with UNIX, such as
processes, daemons, timekeeping and log files.  We'll also look at features
unique to FreeBSD, including multiple processor support, removable I/O devices
and emulating other systems.
.LI
.Sref "\*[chports]" ,
describes the thousands of free software packages that you can optionally
install on a FreeBSD system.
.LI
.Sref "\*[chfilesys]" ,
contains information about the FreeBSD directory structure and device names.
You'll find the section on device names (starting on page
.Sref \*[devices] \/)
interesting even if you're an experienced UNIX hacker.
.LI
.Sref "\*[chdisks]" ,
describes how to format and integrate hard disks, and how to handle disk errors.
.LI
Managing disks can be a complicated affair.
.Sref "\*[chvinum]" ,
describes a way of managing disk storage.
.LI
In
.Sref "\*[cdburn]" ,
we'll look at how to use FreeBSD to write CD-Rs.
.LI
FreeBSD provides professional, reliable data backup services as part of the base
system.  Don't ever let yourself lose data because of inadequate backup
provisions.  Read all about it in
.Sref "\*[chtapes]" .
.LI
.Sref "\*[chprinters]" ,
describes the BSD spooling system and how to use it both on local and networked
systems.
.LI
Starting at
.Sref "\*[chnetintro]" ,
we'll look at the Internet and the more important services.
.LI
.Sref "\*[chnetsetup]" ,
describes how to set up local networking.
.LI
.Sref "\*[chisp]" ,
discusses the issues in selecting an Internet Service Provider (ISP) and
establishing a presence on the Internet.
.LI
.Sref "\*[chmodems]" ,
discusses serial hardware and the prerequisites for PPP and SLIP communications.
.LI
In
.Sref "\*[chppp]" ,
we look at FreeBSD's two PPP implementations and what it takes to set them up.
.LI
In
.Sref "\*[chdns]" ,
we'll consider the use of names on the Internet.
.LI
Security is an increasing problem on the Internet.  In
.Sref "\*[chfirewall]" ,
.X "IP aliasing"
.X "proxy server"
we'll look at some things we can do to improve it.  We'll also look at \fIIP
aliasing\fP, since it goes hand-in-hand with firewalls, and \fIproxy servers\fP.
.LI
Networks sometimes become \fInotwork\fP\/s.  In
.Sref "\*[chnetdebug]" ,
we'll see what we can do to solve network problems.
.LI
.Sref "\*[chclient]" ,
describes the client viewpoint of network access, including Web browsers,
.Command ssh ,
.Command ftp ,
.Command rsync
.X "nfs"
and \fInfs\fP clients for sharing file systems between networked computers.
.LI
Network clients talk to network servers.  We'll look at the corresponding server
viewpoint in
.Sref "\*[chserver]" .
.LI
Despite the World Wide Web, traditional two-way personal communication is still
very popular.  We'll look at how to use mail clients in
.Sref "\*[chmua]" .
.LI
Mail servers are an important enough topic that there's a separate
.Sref "\*[chmta]" .
.\" XXX .LI
.\" XXX Before Microsoft and Novell discovered the Internet, they created a number of
.\" XXX less powerful networking systems, some of which are still in use.  We'll look at
.\" XXX them in
.\" XXX .Sref "\*[chmicronet]" .
.LI
In
.Sref "\*[chxtheory]" ,
we'll look at the theory behind getting X11 working.
.LI
.Sref "\*[chstarting]" ,
describes how to start and stop a FreeBSD system and all the things you can do
to customize it.
.LI
In
.Sref "\*[chconfigfiles]" ,
we'll look at the more common configuration files and what they should contain.
.LI
In
.Sref "\*[chcurrent]" ,
we'll discuss how to ensure that your system is always running the most
appropriate version of FreeBSD.
.LI
FreeBSD keeps changing.  We'll look at some aspects of what that means to you in
.Sref "\*[chupgrading]" .
.LI
.Sref "\*[chbuild]" ,
discusses optional kernel features.
.LI
.Sref "\*[biblio]" ,
suggests some books for further reading.
.LI
.Sref "\*[appthirded]" ,
describes the changes that have taken place in FreeBSD since it was introduced
nearly ten years ago.
.Le
.H2 "FreeBSD features"
.Pn features
.X "Berkeley Software Distribution"
FreeBSD is derived from \fIBerkeley UNIX\fP\/, the flavour of UNIX developed by
the Computer Systems Research Group at the University of California at Berkeley
and previously released as the \fIBerkeley Software Distribution\fP\/ (BSD) of
UNIX.
.Aside
UNIX is a registered trademark of the Open Group, so legally, FreeBSD may not be
called UNIX.  The technical issues are different, of course; make up your own
mind as to how much difference this makes.
.End-aside
Like commercial UNIX, FreeBSD provides you with many advanced features,
including:
.Ls B
.LI
FreeBSD uses \fIpreemptive multitasking\fP\/ with dynamic priority adjustment to
ensure smooth and fair sharing of the computer between applications and users.
.LI
FreeBSD is a \fImulti-user system\fP\/: many people can use a FreeBSD system
simultaneously for unrelated purposes.  The system shares peripherals such as
printers and tape drives properly between all users on the system.
.P
Don't get this confused with the ``multitasking'' offered by some commercial
systems.  FreeBSD is a true multi-user system that protects users from each
other.
.LI
FreeBSD is secure.  Its track record is borne out by the reports of the
\fICERT\fP, the leading organization dealing with computer security.  See
.URI http://www.cert.org
for more information.  The FreeBSD project has a team of security officers
concerned with maintaining this lead.
.LI
FreeBSD is reliable.  It is used by ISPs around the world.  FreeBSD systems
regularly go several years without rebooting.  FreeBSD can fail, of course, but
the main causes of outages are power failures and catastrophic hardware
failures.
.LI
FreeBSD provides a complete \fITCP/IP networking\fP\/ implementation.  This
means that your FreeBSD machine can interoperate easily with other systems and
also act as an enterprise server, providing vital functions such as NFS (remote
file access) and electronic mail services, or putting your organization on the
Internet with WWW, FTP, routing and firewall services.  In addition, the Ports
Collection includes software for communicating with proprietary protocols.
.\" XXX \(emsee
.\" .Sref "\*[chmicronet]"
.\" for more details.
.LI
\fIMemory protection\fP\/ ensures that neither applications nor users can
interfere with each other.  If an application crashes, it cannot affect other
running applications.
.LI
FreeBSD includes the \fIXFree86\fP\/ implementation of the \fIX11\fP\/ graphical user
interface.
.LI
FreeBSD can run most programs built for versions of SCO UNIX and UnixWare,
Solaris, BSD/OS, NetBSD, 386BSD and Linux on the same hardware platform.
.LI
The FreeBSD Ports Collection includes thousands of ready-to-run applications.
.LI
Thousands of additional and easy-to-port applications are available on the
Internet.  FreeBSD is source code compatible with most popular commercial UNIX
systems and thus most applications require few, if any, changes to compile.
Most freely available software was developed on BSD-like systems.  As a result,
FreeBSD is one of the easiest platforms you can port to.
.LI
Demand paged \fIvirtual memory\fP\/ (\fIVM\fP\/) and ``merged VM/buffer cache''
design efficiently satisfies applications with large appetites for memory while
still maintaining interactive response to other users.
.LI
The base system contains a full complement of C, C++ and FORTRAN development
tools.  All commonly available programming languages, such as
.Command perl ,
.Command python
and
.Command ruby ,
are available.  Many additional languages for advanced research and development
are also available in the Ports Collection.
.LI
FreeBSD provides the complete \fIsource code\fP\/ for the entire system, so you
have the greatest degree of control over your environment.  The licensing terms
are the freest that you will find anywhere (``Hey, use it, don't pretend you
wrote it, don't complain to us if you have problems'').  Those are just the
licensing conditions, of course.  As we'll see later in the chapter, there are
plenty of people prepared to help if you run into trouble.
.LI
Extensive \fIonline documentation\fP, including traditional \fIman pages\fP\/ and
a hypertext-based \fIonline handbook\fP.
.LE
.P
FreeBSD is based on the 4.4BSD UNIX released by the Computer Systems Research
Group (CSRG) at the University of California at Berkeley.  The FreeBSD Project
has spent many thousands of hours fine-tuning the system for maximum performance
and reliability.  FreeBSD's features, performance and reliability compare very
favourably with those of commercial operating systems.
.P
Since the source code is available, you can easily customize it for special
applications or projects, in ways not generally possible with operating systems
from commercial vendors.  You can easily start out small with an inexpensive 386
class PC and upgrade as your needs grow.  Here are a few of the applications in
which people currently use FreeBSD:
.P
.Ls B
.LI
\fIInternet Services\fP\/: the Internet grew up around Berkeley UNIX.  The
original TCP/IP implementation, released in 1982, was based on 4.2BSD, and
nearly every current TCP/IP implementation has borrowed from it.  FreeBSD is a
descendent of this implementation, which has been maintained and polished for
decades.  It is the most mature and reliable TCP/IP available at any price.
This makes it an ideal platform for a variety of Internet services such as FTP
servers, World Wide Web servers, electronic mail servers, USENET news servers,
DNS name servers and firewalls.  With the \fISamba\fP\/ suite, you can replace a
Microsoft file server.
.LI
\fIEducation:\fP\/ FreeBSD is an ideal way to learn about operating systems,
computer architecture and networking.  A number of freely available CAD,
mathematical and graphic design packages also make it highly useful to those
whose primary interest in a computer is to get \fIother\fP\/ work done.
.LI
\fIResearch:\fP\/ FreeBSD is an excellent platform for research in operating
systems as well as other branches of computer science, since the source code for
the entire system is available.  FreeBSD's free availability also makes it
possible for remote groups to collaborate on ideas or shared development without
having to worry about special licensing agreements or limitations on what may be
discussed in open forums.
.LI
\fIX Window workstation:\fP\/ FreeBSD makes an excellent choice for an
inexpensive graphical desktop solution.  Unlike an X terminal, FreeBSD allows
many applications to be run locally, if desired, thus relieving the burden on a
central server.  FreeBSD can even boot ``diskless,'' making individual
workstations even cheaper and easier to administer.
.LI
\fISoftware Development:\fP\/ The basic FreeBSD system comes with a full
complement of development tools including the renowned GNU C/C++ compiler and
debugger.
.nr ll -1
.P
.LE
.SPUP
.H2 "Licensing conditions"
.Pn BSD-license
As the name suggests, FreeBSD is free.  You don't have to pay for the code, you
can use it on as many computers as you want, and you can give away copies to
your friends.  There are some restrictions, however.  Here's the BSD license as
used for all new FreeBSD code:
.P
.in .1i
.ll -.1i
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
.Ls
.LI
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
.LI
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
.Le
This software is provided by the FreeBSD project ``as is'' and any express or
implied warranties, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose are disclaimed.  In no
event shall the FreeBSD project or contributors be liable for any direct,
indirect, incidental, special, exemplary, or consequential damages (including,
but not limited to, procurement of substitute goods or services; loss of use,
data, or profits; or business interruption) however caused and on any theory of
liability, whether in contract, strict liability, or tort (including negligence
or otherwise) arising in any way out of the use of this software, even if
advised of the possibility of such damage.
.ll +.1i
.in -.1i
.P
The last paragraph is traditionally written in ALL CAPS, for reasons which don't
seem to have anything to do with the meaning.  Older versions of the license
also contained additional clauses relating to advertising.
.H2 "A little history"
.Pn history
FreeBSD is a labour of love: big commercial companies produce operating systems
and charge lots of money for them; the FreeBSD project produces a
professional-quality operating system and gives it away.  That's not the only
difference.
.P
.X "QDOS"
.X "Quick and Dirty Operating System"
.X "86/DOS"
.X "Paterson, Tim"
In 1981, when IBM introduced their Personal Computer, the microprocessor
industry was still in its infancy.  They entrusted Microsoft to supply the
operating system.  Microsoft already had their own version of UNIX, called
XENIX, but the PC had a minimum of 16 kB and no disk. UNIX was not an
appropriate match for this hardware.  Microsoft went looking for something
simpler.  The ``operating system'' they chose was correspondingly primitive:
86/DOS, a clone of Digital Research's successful CP/M operating system, written
by Tim Paterson of Seattle Computer Products and originally called \fIQDOS\fP\/
(\fIQuick and Dirty Operating System\fP\/).  At the time, it seemed just the
thing: it ran fine without a hard disk (in fact, the original PC didn't
\fIhave\fP\/ a hard disk, not even as an option), and it didn't use up too much
memory.  The only thing that they really had to do was to change the name.  IBM
called its version PC-DOS, while Microsoft marketed its version under the name
MS-DOS.
.P
.X "CSRG"
.X "Berkeley UNIX"
.X "Internet Protocol"
By this time, a little further down the US West Coast, the Computer Systems
Research Group (CSRG) of the University of California at Berkeley had just
modified AT&T's UNIX operating system to run on the new DEC VAX 11/780 machine,
which sported virtual memory, and had turned their attention to implementing
some new protocols for the ARPANET: the so-called \fIInternet Protocols\fP.  The
version of UNIX that they had developed was now sufficiently different from
AT&T's system that it had been dubbed \fIBerkeley UNIX\fP\/.
.P
.X "Fast File System"
.X "UNIX File System"
As time went on, both MS-DOS and UNIX evolved.  Before long, MS-DOS was modified
to handle hard disks\(emnot well, but it handled them, and for the PC users, it
was so much better than what they had before that they ignored the
inefficiencies.  After all, the PC gave you your own hard disk on your desk, and
you didn't have to share it with all the other people in the department.
Microsoft even tried to emulate the UNIX directory structure, but succeeded only
in implementing the concept of nested directories.  At Berkeley, they were
developing a higher performance disk subsystem, the \fIFast File System\fP\/,
now known as the \fIUNIX File System\fP\/.
.P
By the late 80s, it was evident that Microsoft no longer intended to
substantially enhance MS-DOS.  New processors with support for multitasking and
virtual memory had replaced the old Intel 8088 processor of the IBM PC, but they
still ran MS-DOS by emulating the 8088 processor, which was now completely
obsolete.  The 640 kB memory limit of the original PC, which once appeared
bigger than anybody would ever need, became a serious problem.  In addition,
people wanted to do more than one thing at a time with their computers.
.P
A solution to both problems was obvious: move to the 32 bit address mode of the
new Intel 80386 processor and introduce real multitasking, which operating
systems on larger machines had had for decades.  Of course, these larger
machines were only physically larger.  The average PC of 1990 had more memory,
more disk and more processing power than just about any of the large computers
of the 70s.  Nevertheless, Microsoft didn't solve these problems for its
``Windows'' platform until much later, and the solutions still leave a lot to be
desired.
.P
.X "Research UNIX"
.X "Berkeley Software Distribution"
.X "System V"
.X "Santa Cruz Operation"
UNIX, on the other hand, was a relatively mature operating system at the time
when the PC was introduced.  As a result, Microsoft-based environments have had
little influence on the development of UNIX.  UNIX development was determined by
other factors: changes in legal regulations in the USA between 1977 and 1984
enabled AT&T first to license UNIX to other vendors, noticeably Microsoft, who
announced XENIX in 1981, and then to market its own version of UNIX.  AT&T
developed System III in 1982, and System V in 1983.  The differences between
XENIX and System V were initially small, but they grew: by the mid-80s, there
were four different versions of UNIX: the \fIResearch Version\fP, used almost
only inside AT&T, which from the eighth edition on derived from 4.1cBSD, the
\fIBerkeley Software Distribution\fP (BSD) from Berkeley, the commercial
\fISystem V\fP\/ from AT&T, and XENIX, which no longer interested Microsoft, and
was marketed by the company that had developed it, the \fISanta Cruz
Operation\fP, or \fISCO\fP.
.P
.X "USL"
.X "Berkeley Software Design
.X "BSDI"
One casualty of UNIX's maturity was the CSRG in Berkeley.  UNIX was too mature
to be considered an object of research, and the writing was on the wall: the
CSRG would close down.  Some people decided to port Berkeley UNIX to the
PC\(emafter all, SCO had ported its version of UNIX to the PC years earlier.  In
the Berkeley tradition, however, they wanted to give it away.
.Pn unixwars
.X "UNIX Systems Laboratories"
The industry's reaction was not friendly.  In 1992, AT&T's subsidiary
\fIUSL\fP\/ (\fIUNIX Systems Laboratories\fP\/) filed a lawsuit against
\fIBerkeley Software Design, Inc.\fP\/ (\fIBSDI\fP\/), the manufacturer of the
BSD/386 operating system, later called BSD/OS, a system very similar to FreeBSD.
They alleged distribution of AT&T source code in violation of licence
agreements.  They subsequently extended the case to the University of California
at Berkeley.  The suit was settled out of court, and the exact conditions were
not all disclosed.  The only one that became public was that BSDI would migrate
their source base to the newer 4.4BSD-Lite sources, a thing that they were
preparing to do in any case.  Although not involved in the litigation, it was
suggested to FreeBSD that they should also move to 4.4BSD-Lite, which was done
with the release of FreeBSD release 2.0 in late 1994.
.P
Now, in the early 21st century, FreeBSD is the best known of the BSD operating
systems, one that many consider to follow in the tradition of the CSRG.  I can
think of no greater honour for the development team.  It was developed on a
shoestring budget, yet it manages to outperform commercial operating systems by
an order of magnitude.
.H3 "The end of the UNIX wars"
In the course of the FreeBSD project, a number of things have changed about
UNIX.  Sun Microsystems moved from a BSD base to a System V base in the late
80s, a move that convinced many people that BSD was dead and that System V was
the future.  Things turned out differently: in 1992, AT&T sold USL to Novell,
Inc., who had introduced a product based on System V.4 called UnixWare.
Although UnixWare has much better specifications than SCO's old System V.3 UNIX,
it was never a success, and Novell finally sold their UNIX operation to SCO.
SCO itself was then bought out by Caldera (which recently
.\" XXX September 2002
changed its name back to SCO), while the ownership of the UNIX trade mark has
passed to the Open Group.  System V UNIX is essentially dead: current commercial
versions of UNIX have evolved so far since System V that they can't be
considered the same system.  By contrast, BSD is alive and healthy, and lives on
in FreeBSD, NetBSD, OpenBSD and Apple's Mac OS X.
.P
The importance of the AT&T code in the earlier versions of FreeBSD was certainly
overemphasized in the lawsuit.  All of the disputed code was over 10 years old
at the time, and none of it was of great importance.  In January 2002, Caldera
released all ``ancient'' versions of UNIX under a BSD license.  These
specifically included all versions of UNIX from which BSD was derived: the first
to seventh editions of Research UNIX and 32V, the predecessor to 3BSD.  As a
result, all versions of BSD, including those over which the lawsuit was
conducted, are now freely available.
.H2 "Other free UNIX-like operating systems"
.Pn unixclones
FreeBSD isn't the only free UNIX-like operating system available\(emit's not
even the best-known one.  The best-known free UNIX-like operating system is
undoubtedly Linux, but there are also a number of other BSD-derived operating
systems.  We'll look at them first:
.Ls B
.LI
.X "386/BSD"
\fI386/BSD\fP\/ was the original free BSD operating system, introduced by
William F. Jolitz in 1992.  It never progressed beyond a test stage: instead,
two derivative operating systems arose, FreeBSD and NetBSD.  386/BSD has been
obsolete for years.
.LI
.X "NetBSD"
\fINetBSD\fP\/ is an operating system which, to the casual observer, is almost
identical to FreeBSD.  The main differences are that NetBSD concentrates on
hardware independence, whereas FreeBSD concentrates on performance.  FreeBSD
also tries harder to be easy to understand for a beginner.  You can find more
information about NetBSD at
.URI http://www.NetBSD.org .
.LI
.X "OpenBSD"
\fIOpenBSD\fP\/ is a spin-off of NetBSD that focuses on security.  It's also
very similar to FreeBSD.  You can find more information at
.URI http://www.OpenBSD.org .
.LI
.X "Mac OS X"
Apple computer introduced Version 10 (X) of its \fIMac OS\fP\/ in early 2001.
It is a big deviation from previous versions of Mac OS: it is based on a Mach
microkernel with a BSD environment.  The base system (Darwin) is also free.
FreeBSD and Darwin are compatible at the user source code level.
.Le
You could get the impression that there are lots of different, incompatible BSD
versions.  In fact, from a user viewpoint they're all very similar to each
other, much more than the individual distributions of Linux, which we'll look at
next.
.H3 "FreeBSD and Linux"
.X "Linux"
.Pn Linux
In 1991, Linus Torvalds, then a student in Helsinki, Finland, decided he wanted
to run UNIX on his home computer.  At that time the BSD sources were not freely
available, and so Linus wrote his own version of UNIX, which he called Linux.
.P
Linux is a superb example of how a few dedicated, clever people can produce an
operating system that is better than well-known commercial systems developed by
a large number of trained software engineers.  It is better even than a number
of commercial UNIX systems.
.P
Obviously, I prefer FreeBSD over Linux, or I wouldn't be writing this book, but
the differences between FreeBSD and Linux are more a matter of philosophy rather
than of concept.  Here are a few contrasts:
.br
.ne 1i
.Table-heading "Differences between FreeBSD and Linux"
.TS H
tab(#) ;
 lw34  lw35 .
.TH
T{
FreeBSD is a direct descendent of the original UNIX, though it contains no
residual AT&T code.
T}#T{
Linux is a clone and never contained any AT&T code.
T}
.sp .9v
T{
FreeBSD is a complete operating system, maintained by a central group of
software developers under the Concurrent Versions System which maintains a
complete history of the project development.  There is only one distribution of
FreeBSD.
T}#T{
Linux is a kernel, personally maintained by Linus Torvalds and a few trusted
companions.  The non-kernel programs supplied with Linux are part of a
\fIdistribution\fP, of which there are several.  Distributions are not
completely compatible with each other.
T}
.sp .9v
T{
The FreeBSD development style emphasizes accountability and documentation of
changes.
T}#T{
The Linux kernel is maintained by a small number of people who keep track of all
changes.  Unofficial patches abound.
T}
.sp .9v
T{
The kernel supplied with a specific release of FreeBSD is clearly defined.
T}#T{
Linux distributions often have subtly different kernels.  The differences are
not always documented.
T}
.sp .9v
T{
FreeBSD aims to be a stable production environment.
T}#T{
Many versions of Linux are still ``bleeding edge'' development environments.
This is changing rapidly, however.
T}
.sp .9v
T{
As a result of the centralized development style, FreeBSD is straightforward and
easy to install.
T}#T{
The ease of installation of Linux depends on the \fIdistribution\fP.  If you
switch from one distribution of Linux to another, you'll have to learn a new set
of installation tools.
T}
.sp .9v
T{
FreeBSD is still relatively unknown, since its distribution was initially
restricted due to the AT&T lawsuits.
T}#T{
Linux did not have any lawsuits to contend with, so for some time it was
thought to be the only free UNIX-type system available.
T}
.sp .9v
T{
As a result of the lack of knowledge of FreeBSD, relatively little commercial
software is available for it.
T}#T{
A growing amount of commercial software is becoming available for Linux.
T}
.sp .9v
T{
As a result of the smaller user base, FreeBSD is less likely to have drivers for
brand-new boards than Linux.
T}#T{
Just about any new board will soon have a driver for Linux.
T}
.sp .9v
T{
Because of the lack of commercial applications and drivers for FreeBSD, FreeBSD
runs most Linux programs, whether commercial or not.
T}#T{
Linux appears not to need to be able to run FreeBSD programs.
T}
.sp .9v
T{
FreeBSD is licensed under the BSD license\(emsee page
.Sref \*[BSD-license] .
There are very few restrictions on its use.
T}#T{
Linux is licensed under the GNU General Public License.  Further details are at
.URI http://www.gnu.org/licenses/gpl.html .
By comparison with the BSD license, it imposes significant
restrictions on what you can do with the source code.
T}
.sp .9v
T{
FreeBSD has aficionados who are prepared to flame anybody who
dares suggest that it's not better than Linux.
T}#T{
Linux has aficionados who are prepared to flame anybody who
dares suggest that it's not better than FreeBSD.
T}
.TE
.P
.ne 2v
In summary, Linux is also a very good operating system.  For many, it's better
than FreeBSD.
.\" XXX http://keystone.westminster.edu/~fullermd/bsdvlin.htm
.H2 "FreeBSD system documentation"
.X "online handbook"
.X "handbook, online"
.X "lynx, command"
FreeBSD comes with a considerable quantity of documentation which we'll look at
in the following few pages:
.Ls B
.LI
The FreeBSD Documentation Project maintains a collection of ``books,''
documents in HTML or PDF format which can also be accessed online.  They're
installed in the directory hierarchy
.Directory /usr/share/doc .
.LI
The traditional UNIX document format is \fIman pages\fP, individual documents
describing specific functionality.  They're short and to the point of being
cryptic, but if you know what you're looking for, they have just the right
amount of detail.  They're not a good introduction.
.LI
The GNU project introduced their own document format, \fIGNU info\fP.  Some GNU
programs have no other form of documentation.
.Le
.H3 "Reading online documentation"
You'll find a number of HTML documents in the directory
.Directory /usr/share/doc/en/books \/:
.Ls B
.LI
.X "FAQ"
.X "Frequently Asked Questions"
.File /usr/share/doc/en/books/faq/index.html
contains the FreeBSD \fIFAQ\fP\/ (\fIFrequently Asked Questions\fP\/).  It's
just what it says it is: a list of questions that people frequently ask about
FreeBSD, with answers of course.
.LI
.File /usr/share/doc/en/books/fdp-primer/index.html
is a primer for the \fIFreeBSD Documentation Project\fP,
.LI
.X "online handbook"
.X "handbook, online"
.Pn handbook
.X "Live Filesystem"
.File /usr/share/doc/en/books/handbook/index.html
is the FreeBSD \fIonline handbook\fP.  It contains a lot of information
specifically about FreeBSD, including a deeper discussion of many topics in this
book.
.LI
.File /usr/share/doc/en/books/porters-handbook/index.html
is a handbook for contributors to the FreeBSD Ports Collection, which we'll
discuss in
.Sref "\*[chports]" .
.LI
.File /usr/share/doc/en/books/ppp-primer/index.html
contains a somewhat dated document about setting up PPP.  If you have trouble
with
.Sref "\*[chppp]" ,
you may find it useful.
.Le
In addition to the directory
.Directory /usr/share/doc/en/books ,
there's also a directory
.Directory /usr/share/doc/en/articles
with a number of shorter items of documentation.
.P
Note the component \fIen\fP\/ in the pathnames above.  That stands for
\fIEnglish\fP.  A number of these books are also installed in other languages:
change \fIen\fP\/ to \fIde\fP\/ for a German version, to \fIes\fP\/ for Spanish,
to \fIfr\fP\/ for French, to \fIja\fP\/ for Japanese, to \fIru\fP\/ for Russian,
or to \fIzh\fP\/ for Chinese.  Translation efforts are continuing, so you may
find documentation in other languages as well.
.P
.ne 3v
If you're running X, you can use a browser like
.Command mozilla
to read the documents.  If you don't have X running yet, use
.Command lynx .
Both of these programs are included in the CD-ROM distribution.  To install
them, use
.Command sysinstall ,
which is described on page
.Sref \*[sysinstall] .
.P
.Command lynx
is not a complete substitute for complete web browsers such as
.Command mozilla :
since it is text-only, it is not capable of displaying the large majority of web
pages correctly.  It's good enough for reading most of the FreeBSD online
documentation, however.
.P
In each case, you start the browser with the name of the document, for example:
.Dx
$ \f(CBlynx /usr/share/doc/en/books/handbook/index.html\fP
$ \f(CBmozilla /usr/share/doc/en/books/handbook/index.html &\fP
.De
Enter the \f(CW&\fP after the invocation of
.Command mozilla
to free up the window in which you invoke it:
.Command mozilla
opens its own window.
.P
If you haven't installed the documentation, you can still access it from the
Live Filesystem CD-ROM.  Assuming the CD-ROM is mounted on
.Directory /cdrom ,
choose the file
.File -n /cdrom/usr/share/doc/en/books/handbook/index.html .
.P
Alternatively, you can print out the handbook.  This is a little more difficult,
and of course you'll lose the hypertext references, but you may prefer it in
this form.  To format the handbook for printing, you'll need a PostScript
printer or
.Command ghostscript .
See page
.Sref \*[PostScript] \&
for more details of how to print PostScript.
.P
The printable version of the documentation doesn't usually come with the CD-ROM
distribution.  You can pick it up with
.Command ftp
(see page
.Sref \*[ftp] )
from
.URI ftp://ftp.FreeBSD.ORG/pub/FreeBSD/doc/ ,
which has the same directory structure as described above.  For example, you
would download the handbook in PostScript form from
.URI ftp://ftp.FreeBSD.ORG/pub/FreeBSD/doc/en/books/handbook/book.ps.bz2 .
.H3 "The online manual"
.Pn man
.X "man page"
The most comprehensive documentation on FreeBSD is the online manual, usually
referred to as the \fIman pages\fP.  Nearly every program, file, library
function, device or interface on the system comes with a short reference manual
explaining the basic operation and various arguments.  If you were to print it
out, it would run to well over 8,000 pages.
.P
When online, you view the man pages with the command
.Command man .
For example, to learn more about the command
.Command ls ,
type:
.Dx
$ \f(CBman ls \fP
LS(1)                      FreeBSD Reference Manual                      LS(1)

\f(CBNAME\fP
     ls - list directory contents

\f(CBSYNOPSIS\fP
     ls [-\f(CBACFLRTacdfiloqrstu1\fP] [ file ... ]

\f(CBDESCRIPTION\fP
     For each operand that names a file of a type other than directory, \f(CBls\fP
     displays its name as well as any requested, associated information.  For
     each operand that names a file of type directory, ls displays the names.
\fI(etc)\fP\/
.De
In this particular example, with the exception of the first line, the text in
\f(CBconstant width bold\fP is not input, it's the way it appears on the screen.
.P
The online manual is divided up into sections numbered:
.Ls
.LI
User commands
.LI
System calls and error numbers
.LI
Functions in the C libraries
.LI
Device drivers
.LI
File formats
.LI
Games and other diversions
.LI
Miscellaneous information
.LI
System maintenance and operation commands
.LI
Kernel interface documentation
.Le
In some cases, the same topic may appear in more than one section of the online
manual.  For example, there is a user command
.Command chmod
and a system call \f(CWchmod()\fP.  In this case, you can tell the
.Command man
command which you want by specifying the section number:
.Dx
$ \f(CBman 1 chmod \fP
.De
This command displays the manual page for the user command chmod.  References to
a particular section of the online manual are traditionally placed in
parentheses in written documentation.  For example, \fIchmod(1)\fP\/ refers to
the user command
.Command chmod ,
and \fIchmod(2)\fP\/ means the system call.
.P
.X "apropos, command"
.X "command, apropos"
This is fine if you know the name of the command and forgot how to use it, but
what if you can't recall the command name?  You can use
.Command man
to search for keywords in the command descriptions by using the \f(CW-k\fP
option, or by starting the program
.Command apropos \/:
.Dx
$ \f(CBman -k mail \fP
$ \f(CBapropos mail \fP
.De
Both of these commands do the same thing: they show the names of the man pages
that have the keyword \fImail\fP\/ in their descriptions.
.P
Alternatively, you may browse through the
.Directory /usr/bin
directory, which contains most of the system executables.  You'll see lots of
file names, but you don't have any idea what they do.  To find out, enter one of
the lines:
.Dx
$ \f(CBcd /usr/bin; man -f * \fP
$ \f(CBcd /usr/bin; whatis * \fP
.De
.ne 2v
Both of these commands do the same thing: they print out a one-line summary of
the purpose of the program:
.Dx
$ \f(CBcd /usr/bin; man -f * \fP
a2p(1)           - Awk to Perl translator
addftinfo(1)     - add information to troff font files for use with groff
apply(1)         - apply a command to a set of arguments
apropos(1)       - search the whatis database
\fI\&...etc\fP
.De
.SPUP
.H4 "Printing man pages"
.Pn howto-print-manpages
If you prefer to have man pages in print, rather than on the screen, you can do
this in two different ways:
.Ls B
.LI
The simpler way is to redirect the output to the spooler:
.Dx
$ \f(CBman ls | lpr\fP
.De
This gives you a printed version that looks pretty much like the original on
the screen, except that you may not get bold or underlined text.
.LI
You can get typeset output with
.Command troff \/:
.Dx
$ \f(CBman -t ls | lpr\fP
.De
This gives you a properly typeset version of the man page, but it requires that
your spooling system understand PostScript\(emsee page
.Sref \*[PostScript] \&
for more details of printing PostScript, even on printers that don't understand
PostScript.
.Le
.H3 "GNU info"
.X "emacs, command"
.X "command, emacs"
The Free Software Foundation has its own online hypertext browser called
.Command info .
Many FSF programs come with either no man page at all, or with an excuse for a
man page
.Command ( gcc ,
for example).  To read the online documentation, you need to browse the
.Command info
files with the
.Command info
program, or from
.X "emacs, command"
.X "command, emacs"
.Command -n Emacs
with the
.Command info
mode.  To start
.Command info ,
simply type:
.Dx
$ \f(CBinfo\fP
.De
In
.Command -n Emacs ,
enter \fBCTRL-h i\fP or \fBALT-x\fP \f(CWinfo\fP.  Whichever way you start
.Command info ,
you can get brief introduction by typing \f(CBh\fP, and a quick command
reference by typing \f(CB?\fP.
.H2 "Other documentation on FreeBSD"
.X "documentation, online"
.Pn docco
FreeBSD users have access to probably more top-quality documentation than just
about any other operating system.  Remember that word UNIX is trademarked.
Sure, the lawyers tell us that we can't refer to FreeBSD as UNIX, because UNIX
belongs to the Open Group.  That doesn't make the slightest difference to the
fact that nearly every book on UNIX applies more directly to FreeBSD than any
other flavour of UNIX.  Why?
.P
.ne 3v
Commercial UNIX vendors have a problem, and FreeBSD doesn't help them: why
should people buy their products when you can get it free from the FreeBSD
Project (or, for that matter, from other free UNIX-like operating systems such
as NetBSD, OpenBSD and Linux)?  One obvious reason would be ``value-added
features.''  So they add features or fix weak points in the system, put a
copyright on the changes, and help lock their customers in to their particular
implementation.  As long as the changes are really useful, this is legitimate,
but it does make the operating system less compatible with ``standard UNIX,''
and the books about standard UNIX are less applicable.
.P
In addition, many books are written by people with an academic background.  In
the UNIX world, this means that they are more likely than the average user to
have been exposed to BSD.  Many general UNIX books handle primarily BSD,
possibly with an additional chapter on the commercial System V version.
.P
In
.Sref "\*[biblio]" ,
you'll find a list of books that I find worthwhile.  I'd like to single out some
that I find particularly good, and that I frequently use myself:
.Ls B
.LI
.X "Peek, Jerry"
.X "O'Reilly, Tim"
.X "Loukides, Mike"
\fIUNIX Power Tools\fR, by Jerry Peek, Tim O'Reilly, and Mike Loukides, is a
superb collection of interesting information, including a CD-ROM.  Recommended
for everybody, from beginners to experts.
.LI
.X "Abrahams, Paul W."
.X "Larson, Bruce R."
\fIUNIX for the Impatient\fP\/, by Paul W. Abrahams and Bruce R. Larson, is more
similar to this book, but it includes a lot more material on specific products,
such as shells and the
.X "command, emacs"
.X "emacs, command"
.Command -n Emacs
editor.
.LI
.X "Nemeth, Evi"
.X "Snyder, Garth"
.X "Seebass, Scott"
.X "Hein, Trent R."
The \fIUNIX System Administration Handbook\fP, by Evi Nemeth, Garth Snyder,
Scott Seebass, and Trent R. Hein, is one of the best books on systems
administration I have seen.  It covers a number different UNIX systems,
including an older version of FreeBSD.
.Le
There are also many active Internet groups that deal with FreeBSD.
..if verylong
We'll look at them in the rest of this chapter, starting on page.
.Sref \*[fdafdsa] XXX
..else
Read about them in the online handbook.
..endif
.H2 "The FreeBSD community"
.X "community, FreeBSD"
.X "FreeBSD, community"
.Pn FreeBSD-community
FreeBSD was developed by a world-wide group of developers.  It could not have
happened without the Internet.  Many of the key players have never even met each
other in person; the main means of communication is via the Internet.  If you
have any kind of Internet connection, you can participate as well.  If you don't
have an Internet connection, it's about time you got one.  The connection
doesn't have to be complete: if you can receive email, you can participate.  On
the other hand, FreeBSD includes all the software you need for a complete
Internet connection, not the very limited subset that most PC-based ``Internet''
packages offer you.
.H2 "Mailing lists"
.X "mailing lists"
.Pn support
.X "support"
.X "installation support"
As it says in the copyright, FreeBSD is supplied as-is, without any support
liability.  If you're on the Internet, you're not alone, however.  Liability is
one thing, but there are plenty of people prepared to help you, most for free,
some for fee.  A good place to start is with the mailing lists.  There are a
number of mailing lists that you can join.  Some of the more interesting ones
are:
.Ls B
.LI
\f(CWFreeBSD-questions@FreeBSD.org\fP is the list to which you may send general
questions, in particular on how to use FreeBSD.  If you have difficulty
understanding anything in this book, for example, this is the right place to
ask.  It's also the list to use if you're not sure which is the most
appropriate.
.LI
\f(CWFreeBSD-newbies@FreeBSD.org\fP is a list for newcomers to FreeBSD.  It's
intended for people who feel a little daunted by the system and need a bit of
reassurance.  It's not the right place to ask any kind of technical question.
.LI
\f(CWFreeBSD-hackers@FreeBSD.org\fP is a technical discussion list.
.LI
\f(CWFreeBSD-current@FreeBSD.org\fP is an obligatory list for people who run the
development version of FreeBSD, called \f(CWFreeBSD-CURRENT\fP.
.LI
\f(CWFreeBSD-stable@FreeBSD.org\fP is a similar list for people who run the more
recent stable version of FreeBSD, called \f(CWFreeBSD-STABLE\fP.  We'll talk
about these versions on page
.Sref \*[release-names] .
Unlike the case for \f(CWFreeBSD-CURRENT\fP users, it's not obligatory for
\f(CWFreeBSD-STABLE\fP users to subscribe to \f(CWFreeBSD-stable\fP.
.Le
You can find a complete list of FreeBSD mailing lists on the web site, currently
at
.URI http://www.FreeBSD.org/doc/en_US.ISO8859-1/books/handbook/eresources.html .
This address is part of the online handbook and may change when the handbook is
modified; follow the link \fIMailing Lists\fP\/ from
.URI http://www.FreeBSD.org/
if it is no longer valid, or if you can't be bothered typing in the URI.
.P
The mailing lists are run by
.Command mailman
(in the Ports Collection).  Join them via the web interface mentioned above.
You will receive a mail message from \fImailman\fP\/ asking you to confirm your
subscription by replying to the message.  You don't need to put anything in the
reply: the reply address is used once only, and you're the only person who will
ever see it, so the system knows that it's you by the fact that you replied at
all.  You also have the option of confirming via a web interface with a
specially generated URI.  Similar considerations apply in this case.
.P
FreeBSD mailing lists can have a very high volume of traffic.  The
FreeBSD-questions mailing list, for example, has thousands of subscribers, and
many of them are themselves mailing lists.  It receives over a hundred messages
every day.  That's about a million messages a day in total for just one mailing
list, so when you sign up for a mailing list, be sure to read the charter.  You
can find the URI from the \fImailman\fP\/ confirmation message.  It's also a
good idea to ``lurk'' (listen, but not say anything) on the mailing list a while
before posting anything: each list has its own traditions.
.P
.ne 2v
When submitting a question to \f(CWFreeBSD-questions\fP, consider the following
points:
.Ls
.LI
Remember that nobody gets paid for answering a FreeBSD question.  They do it of
their own free will.  You can influence this free will positively by submitting
a well-formulated question supplying as much relevant information as possible.
You can influence this free will negatively by submitting an incomplete,
illegible, or rude question.  It's perfectly possible to send a message to
FreeBSD-questions and not get an answer even if you follow these rules.  It's
much more possible to not get an answer if you don't.
.LI
Not everybody who answers FreeBSD questions reads every message: they look at
the subject line and decide whether it interests them.  Clearly, it's in your
interest to specify a subject.  ``FreeBSD problem'' or ``Help'' aren't enough.
If you provide no subject at all, many people won't bother reading it.  If your
subject isn't specific enough, the people who can answer it may not read it.
.LI
When sending a new message, well, send a new message.  Don't just reply to some
other message, erase the old content and change the subject line.  That leaves
an \f(CWIn-Reply-To:\fP header which many mail readers use to thread messages,
so your message shows up as a reply to some other message.  People often delete
messages a whole thread at a time, so apart from irritating people, you also run
a chance of having the message deleted unread.
.LI
Format your message so that it is legible, and PLEASE DON'T SHOUT!!!!!.  It's
really painful to try to read a message written full of typos or without any
line breaks.  A lot of badly formatted messages come from bad mailers or badly
configured mailers.  The following mailers are known to send out badly formatted
messages without you finding out about them:
.P
.nf
Eudora
exmh
Microsoft Exchange
Microsoft Internet Mail
Microsoft Outlook
Netscape
.fi
.P
.ne 2v
As you can see, the mailers in the Microsoft world are frequent offenders.  If
at all possible, use a UNIX mailer.  If you must use a mailer under Microsoft
environments, make sure it is set up correctly.  Try not to use MIME: a lot of
people use mailers which don't get on very well with MIME.
.P
For further information on this subject, check out
.URI http://www.lemis.com/email.html .
.LI
Make sure your time and time zone are set correctly.  This may seem a little
silly, since your message still gets there, but many of the people you are
trying to reach get several hundred messages a day.  They frequently sort the
incoming messages by subject and by date, and if your message doesn't come
before the first answer, they may assume they missed it and not bother to look.
.LI
Don't include unrelated questions in the same message.  Firstly, a long message
tends to scare people off, and secondly, it's more difficult to get all the
people who can answer all the questions to read the message.
.LI
Specify as much information as possible.  This is a difficult area: the
information you need to submit depends on the problem.  Here's a start:
.Ls B
.LI
If you get error messages, don't say ``I get error messages'', say (for example)
``I get the error message \fINo route to host\fP''.
.LI
If your system panics, don't say ``My system panicked'', say (for example) ``my
system panicked with the message \fIfree vnode isn't\fP''.
.LI
If you have difficulty installing FreeBSD, please tell us what hardware you
have, particularly if you have something unusual.
.LI
If, for example, you have difficulty getting PPP to run, describe the
configuration.  Which version of PPP do you use? What kind of authentication do
you have? Do you have a static or dynamic IP address? What kind of messages do
you get in the log file?  See
.Sref "\*[chppp]" ,
for more details in this particular case.
.Le
.LI
If you don't get an answer immediately, or if you don't even see your own
message appear on the list immediately, don't resend the message.  Wait at least
24 hours.  The FreeBSD mailer offloads messages to a number of subordinate
mailers around the world.  Usually the messages come through in a matter of
seconds, but sometimes it can take several hours for the mail to get through.
.LI
If you do all this, and you still don't get an answer, there could be other
reasons.  For example, the problem is so complicated that nobody knows the
answer, or the person who does know the answer was offline.  If you don't get an
answer after, say, a week, it might help to re-send the message.  If you don't
get an answer to your second message, though, you're probably not going to get
one from this forum.  Resending the same message again and again will only make
you unpopular.
.Le
.H3 "How to follow up to a question"
Often you will want to send in additional information to a question you have
already sent.  The best way to do this is to reply to your original message.
This has three advantages:
.Ls
.LI
You include the original message text, so people will know what you're talking
about.  Don't forget to trim unnecessary text, though.
.LI
The text in the subject line stays the same (you did remember to put one in,
didn't you?).  Many mailers will sort messages by subject.  This helps group
messages together.
.LI
The message reference numbers in the header will refer to the previous message.
Some mailers, such as mutt, can thread messages, showing the exact relationships
between the messages.
.Le
There are more suggestions, in particular for answering questions, at
.URI http://www.lemis.com/questions.html .
See also 
.Sref "\*[chmua]" \&
for more information about sending mail messages.  You may also like to check
out the FreeBSD web site at
.URI http://www.FreeBSD.org/ 
and the support page at
.URI http://www.FreeBSD.org/support.html .
.P
In addition, a number of companies offer support for FreeBSD.  See the web page
.URI http://www.FreeBSD.org/commercial/consulting_bycat.html
for some possibilities.
.H3 "Unsubscribing from the mailing lists"
There's a lot of traffic on the mailing lists, particularly on
\f(CWFreeBSD-questions\fP.  You may find you can't take it and want to get out
again.  Again, you unsubscribe from the list either via the web or via a special
mail address, \f(BInot\fP\/ by sending mail to the the list.  Each message you
get from the mailing lists finishes with the following text:
.Dx
freebsd-questions@freebsd.org mailing list
http://lists.freebsd.org/mailman/listinfo/freebsd-questions
To unsubscribe, send any mail to "freebsd-questions-unsubscribe@freebsd.org"
.De
Don't be one of those people who send the unsubscribe request to the mailing
list instead.
.H3 "User groups"
.X "user, groups"
But how about meeting FreeBSD users face to face?  There are a number of user
groups around the world.  If you live in a big city, chances are that there's
one near you.  Check
.URI http://www.FreeBSD.org/support.html#user
for a list.  If you don't find one, consider taking the initiative and starting
one.
.P
.X "BSDCon"
In addition, USENIX holds an annual conference, the \fIBSDCon\fP, which deals
with technical aspects of the BSD operating systems.  It's also a great
opportunity to get to know other users from around the world.  If you're in
Europe, there is also a BSDCon Europe, which at the time of writing was not run
by USENIX.  See
.URI http://www.eurobsdcon.org
for more details.
.H3 "Reporting bugs"
.X "reporting bugs"
.X "bugs, reporting"
If you find something wrong with FreeBSD, we want to know about it, so that we
can fix it.  To report a bug, use the
.Command send-pr
program to send it as a mail message.
.P
There used to be a web form at
.URI http://www.FreeBSD.org/send-pr.html ,
but it has been closed down due to abuse.
.H2 "The Berkeley daemon"
.Pn bsdd
.X "Branagan, Linda"
.X "Berkeley daemon"
.X "daemon, Berkeley"
.X "daemon, Berkeley"
.X "Kolstad, Rob"
.X "McKusick, Kirk"
.XPSPIC -R -W 1.4i -0 images/daemon.ps
.ll -1.6i
The little daemon at the right symbolizes BSD.  It is included with kind
permission of Marshall Kirk McKusick, one of the leading members of the former
Computer Sciences Research Group at the University of California at Berkeley,
and owner of the daemon's copyright.  Kirk also wrote the foreword to this book.
.P
The daemon has occasionally given rise to a certain amount of confusion.  In
fact, it's a joking reference to processes that run in the background\(emsee
.Sref "\*[chunixadmin]" ,
page
.Sref \*[daemon] ,
for a description.  The outside world occasionally sees things differently, as
the following story indicates:
.br
.ne 5v
.ll
.in 1m
.\" XXX.ft CW
.nf
.na
.sp
.ft C
Newsgroups: alt.humor.best-of-usenet
Subject: [comp.org.usenix] A Great Daemon Story
.sp .4v
From: Rob Kolstad <kolstad@bsdi.com>
Newsgroups: comp.org.usenix
Subject: A Great Daemon Story
.ft R
.fi
.ad
.sp .3v
Linda Branagan is an expert on daemons.  She has a T-shirt that sports the
daemon in tennis shoes that appears on the cover of the 4.3BSD manuals and
\fIThe Design and Implementation of the 4.3BSD UNIX Operating System\fP\/ by
S. Leffler, M. McKusick, M. Karels, J. Quarterman, Addison Wesley Publishing
Company, Reading, MA 1989.
.sp .3v
She tells the following story about wearing the 4.3BSD daemon T-shirt:
.sp .3v
Last week I walked into a local ``home style cookin' restaurant/watering hole''
in Texas to pick up a take-out order.  I spoke briefly to the waitress behind
the counter, who told me my order would be done in a few minutes.
.sp .3v
So, while I was busy gazing at the farm implements hanging on the walls, I was
approached by two ``natives.''  These guys might just be the original Texas
rednecks.
.sp .3v
``Pardon us, ma'am.  Mind if we ask you a question?''
.sp .3v
Well, people keep telling me that Texans are real friendly, so I nodded.
.sp .3v
``Are you a Satanist?''
.sp .3v
Well, at least they didn't ask me if I liked to party.
.sp .3v
``Uh, no, I can't say that I am.''
.sp .3v
``Gee, ma'am.  Are you sure about that?'' they asked.
.sp .3v
I put on my biggest, brightest Dallas Cowboys cheerleader smile and said, ``No,
I'm positive.  The closest I've ever come to Satanism is watching Geraldo.''
.sp .3v
``Hmmm.  Interesting.  See, we was just wondering why it is you have the lord of
darkness on your chest there.''
.sp .3v
I was this close to slapping one of them and causing a scene\(emthen I stopped
and noticed the shirt I happened to be wearing that day.  Sure enough, it had a
picture of a small, devilish-looking creature that has for some time now been
associated with a certain operating system.  In this particular representation,
the creature was wearing sneakers.
.sp .3v
They continued: ``See, ma'am, we don't exactly appreciate it when people show
off pictures of the devil.  Especially when he's lookin' so friendly.''
.sp .3v
These idiots sounded terrifyingly serious.
.sp .3v
Me: ``Oh, well, see, this isn't really the devil, it's just, well, it's
sort of a mascot.
.sp .3v
Native: ``And what kind of football team has the devil as a mascot?''
.sp .3v
Me: ``Oh, it's not a team.  It's an operating\(emuh, a kind of computer.''
.sp .3v
I figured that an ATM machine was about as much technology as these guys could
handle, and I knew that if I so much as uttered the word ``UNIX'' I would only
make things worse.
.sp .3v
Native: ``Where does this satanical computer come from?''
.sp .3v
Me: ``California.  And there's nothing satanical about it really.''
.sp .3v
Somewhere along the line here, the waitress noticed my predicament\(embut these
guys probably outweighed her by 600 pounds, so all she did was look at me
sympathetically and run off into the kitchen.
.sp .3v
Native: ``Ma'am, I think you're lying.  And we'd appreciate it if you'd
leave the premises now.''
.sp .3v
Fortunately, the waitress returned that very instant with my order, and they
agreed that it would be okay for me to actually pay for my food before I left.
While I was at the cash register, they amused themselves by talking to each
other.
.sp .3v
Native #1: ``Do you think the police know about these devil computers?''
.sp .3v
Native #2: ``If they come from California, then the FBI oughta know
about 'em.''
.sp .3v
They escorted me to the door.  I tried one last time: ``You're really blowing
this all out of proportion.  A lot of people use this `kind of computers.'
Universities, researchers, businesses.  They're actually very useful.''
.sp .3v
Big, big, \fIbig\fP\/ mistake.  I should have guessed at what came next.
.sp .3v
Native: ``Does the government use these devil computers?''
.sp .3v
Me: ``Yes.''
.sp .3v
Another \fIbig\fP\/ boo-boo.
.sp .3v
Native: ``And does the government pay for 'em?  With our tax dollars?''
.sp .3v
I decided that it was time to jump ship.
.sp .3v
Me: ``No.  Nope.  Not at all.  Your tax dollars never entered the picture at
all.  I promise.  No sir, not a penny.  Our good Christian congressmen would
never let something like that happen.  Nope.  Never.  Bye.''
.sp .3v
Texas.  What a country.
.in
.P
.ne 3v
The daemon tradition goes back quite a way.  As recently as 1996, after the
publication of the first edition of this book, the following message went
through the \f(CWFreeBSD-chat\fP mailing list:
.in 1m
.\"XXX.ft CW
.nf
.na
.sp
To: "Jonathan M. Bresler" <jmb@freefall.freebsd.org>
Cc: obrien@antares.aero.org (Mike O'Brien),
        joerg_wunsch@uriah.heep.sax.de,
        chat@FreeBSD.org, juphoff@tarsier.cv.nrao.edu
Date: Tue, 07 May 1996 16:27:20 -0700
Sender: owner-chat@FreeBSD.org
.ft R
.fi
.ad
.sp .3v
>         details and gifs PLEASE!
.sp .3v
        If you insist. \f(CW:-)\fP
.sp .3v
.X "Salus, Peter"
.X "Ferentz, Mel"
        Sherman, set the Wayback Machine for around 1976 or so (see Peter Salus'
\fIA Quarter Century of UNIX\fP\/ for details), when the first really national
UNIX meeting was held in Urbana, Illinois.  This would be after the ``forty
people in a Brooklyn classroom'' meeting held by Mel Ferentz (yeah I was at that
too) and the more-or-less simultaneous West Coast meeting(s) hosted by SRI, but
before the UNIX Users Group was really incorporated as a going concern.
.sp .3v
.X "Thompson, Ken"
.X "Ritchie, Dennis"
.X "Foglio, Phil"
.X "O'Brien, Mike"
        I knew Ken Thompson and Dennis Ritchie would be there.  I was living in
Chicago at the time, and so was comic artist Phil Foglio, whose star was just
beginning to rise.  At that time I was a bonded locksmith.  Phil's roommate had
unexpectedly split town, and he was the only one who knew the combination to the
wall safe in their apartment.  This is the only apartment I've ever seen that
had a wall safe, but it sure did have one, and Phil had some stuff locked in
there.  I didn't hold out much hope, since safes are far beyond where I was (and
am) in my locksmithing sphere of competence, but I figured ``no guts no glory''
and told him I'd give it a whack.  In return, I told him, he could do some
T-shirt art for me.  He readily agreed.
.sp .3v
        Wonder of wonders, this safe was vulnerable to the same algorithm that
Master locks used to be susceptible to.  I opened it in about 15 minutes of
manipulation.  It was my greatest moment as a locksmith and Phil was overjoyed.
I went down to my lab and shot some Polaroid snaps of the PDP-11 system I was
running UNIX on at the time, and gave it to Phil with some descriptions of the
visual puns I wanted: pipes, demons with forks running along the pipes, a ``bit
bucket'' named \fI/dev/null\fP, all that.
.sp .3v
        What Phil came up with is the artwork that graced the first decade's
worth of ``UNIX T-shirts,'' which were made by a Ma and Pa operation in a
Chicago suburb.  They turned out transfer art using a 3M color copier in their
basement.  Hence, the PDP-11 is reversed (the tape drives are backwards) but
since Phil left off the front panel, this was hard to tell.  His trademark
signature was photo-reversed, but was recopied by the T-shirt people and
``re-forwardized,'' which is why it looks a little funny compared to his real
signature.
.sp .3v
        Dozens and dozens of these shirts were produced.  Bell Labs alone
accounted for an order of something like 200 for a big picnic.  However, only
four (4) REAL originals were produced: these have a distinctive red collar and
sleeve cuff.  One went to Ken, one to Dennis, one to me, and one to my
then-wife.  I now possess the latter two shirts.  Ken and Dennis were presented
with their shirts at the Urbana conference.
.sp .3v
.X "Stettner, Armando"
        People ordered these shirts direct from the Chicago couple.  Many years
later, when I was living in LA, I got a call from Armando Stettner, then at DEC,
asking about that now-famous artwork.  I told him I hadn't talked to the
Illinois T-shirt makers in years.  At his request I called them up.  They'd
folded the operation years ago and were within days of discarding all the old
artwork.  I requested its return, and duly received it back in the mail.  It
looked strange, seeing it again in its original form, a mirror image of the
shirts with which I and everyone else were now familiar.
.sp .3v
        I sent the artwork to Armando, who wanted to give it to the Ultrix
marketing people.  They came out with the Ultrix poster that showed a nice shiny
Ultrix machine contrasted with the chewing-gum-and-string PDP-11 UNIX people
were familiar with.  They still have the artwork, so far as I know.
.sp .3v
        I no longer recall the exact contents of the letter I sent along with
the artwork. I did say that as far as I knew, Phil had no residual rights to the
art, since it was a `work made for hire', though nothing was in writing (and
note this was decades before the new copyright law).  I do not now recall if I
explicitly assigned all rights to DEC.  What is certain is that John Lassiter's
daemon, whether knowingly borrowed from the original, or created by parallel
evolution, postdates the first horde of UNIX daemons by at least a decade and
probably more.  And if Lassiter's daemon looks a lot like a Phil Foglio
creation, there's a reason.
.sp .3v
        I have never scanned in Phil's artwork; I've hardly ever scanned in
anything, so I have no GIFs to show.  But I have some very very old UNIX
T-shirts in startlingly good condition.  Better condition than I am at any rate:
I no longer fit into either of them.
.sp .3v
Mike O'Brien
.br
creaky antique
.P
.in
Note the date of this message: it appeared since the first edition of this book.
Since then, the daemon image has been scanned in, and you can find a version at
.URI http://www.mckusick.com/beastie/shirts/usenix.html .
.\" Here's a scan of the teeshirt.
.\" .PIC images/olddaemon.ps 4i
