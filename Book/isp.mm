.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: isp.mm,v 4.10 2003/04/02 03:09:55 grog Exp grog $
.\"
.Chapter \*[nchisp] "Connecting to the Internet"
To implement the reference network shown in the previous chapter, we need to do
a lot of things that interface with the outside world.  They can take some
time, so we should look at them first:
.Ls B
.LI
What kind of physical connection should we use?  We'll consider that in the next
section.
.LI
We may want to \fIregister a domain\fP.  Many people don't, but I strongly
recommend it.  Find out about that on page \*[getting-on-internet].
.LI
We may also want to \fIregister a network\fP.  In our example, we have used the
network \f(CW223.147.37.0\fP.  In real life, we can't choose our own network: we
take what is given to us.  We'll look at this on page \*[netreg].
.LI
We need to find an \fIInternet Service Provider\fP.  We'll look at what that
entails on page \*[select-isp].
.LE
.H2 "The physical connection"
.Pn connect-types
Just two or three years ago, the way to connect to the outside world was simple:
a phone line.  Since then, things have changed quite a bit, and you may have
quite a choice:
.Ls B
.LI
Analogue telephone line connections are still the most common way of connecting
small networks in most countries, but their bandwidth is limited to about 7 kB/s
at best.  You can run PPP or SLIP over this kind of line, though nowadays most
ISPs support only PPP.
.LI
.X "ISDN"
.X "Integrated Services Digital Network"
.X "POTS"
\fIISDN\fP\/ stands for \fIIntegrated Systems Digital Network\fP.  It's the new,
better, washes-whiter telephone system that is replacing POTS (\fIPlain Old
Telephone Service\fP\/) in some countries, notably in Europe.  FreeBSD supports
ISDN with the \fIisdn4bsd\fP\/ driver.  We won't look at ISDN further in this
book.
.LI
.X "leased line"
.X "T1 line"
.X "E1 line"
.X "line, leased"
.X "line, T1"
.X "line, E1"
\fILeased lines\fP\/ form the backbone of the Internet.  They're invariably more
expensive than dialup lines, but they can provide quite high speeds\(emin the
USA, a \fIT1\fP\/ line will give you 1,536 kbps, and in the rest of the world an
\fIE1\fP\/ will give you 2,048 kbps.  Leased lines are becoming less
interesting, and we won't look at them in more detail in this book.
.LI
.X "cable modem"
.X "modem, cable"
\fICable modems\fP\/ use existing cable TV networks to deliver a high speed
connection, up to several megabits per second.  They use the cable as a
broadcast medium, rather like an Ethernet, and suffer from the same load
problems: you share the speed with the other users of the cable. There are also
some security issues to consider, but if you have a cable service in your area,
you'll probably find it superior to telephones.  The cable modem is effectively
a bridge between the cable and an Ethernet.  From the FreeBSD point of view, the
cable modem looks like just another Ethernet device.
.LI
.X "ADSL"
.X "Asynchronous Digital Subscriber Line"
.X "DSL"
.X "Digital Subscriber Line"
.X "HDSL"
.X "High-speed Digital Subscriber Line"
.X "SDSL"
.X "Synchronous Digital Subscriber Line"
\fIDSL\fP\/ (\fIDigital Subscriber Line\fP\/) is the telephone companies'
reaction to cable modems.
.Aside
Until recently, the \fIL\fP\/ stood for \fILoop\fP, not \fILine\fP.  A loop is
the telco term for the pair of wires between the exchange (or \fICentral
Office\fP\/) and the subscriber premises.
.End-aside
There are a number of variants on DSL: \fIADSL\fP\/ (\fIAsynchronous Digital
Subscriber Line\fP\/) has different speeds for the uplink and the downlink,
while \fISDSL\fP\/ (\fISymmetric Digital Subscriber Line\fP\/) and \fIHDSL\fP\/
(\fIHigh-speed Digital Subscriber Line\fP\/) have the same speed in each
direction.  Speeds and capabilities differ widely from one location to another.
By modifying the way they transmit data over normal phone wires, including the
use of special modems, ADSL can get speeds of up to 6 Mb/s downstream (towards
the end user), and about 640 kbps upstream.  HDSL has similar speeds, but the
speed is the same in each direction.  In contrast to cable modems, you don't
have to share this bandwidth with anybody.  Technical considerations limit the
loop length to about four miles, so even in big cities you may not be able to
get it.  Many DSL services are plagued by technical problems.  There are a
number of different ways to connect to a DSL service, but most of them involve a
conversion to Ethernet.
.LI
.X "Internet, satellite connection to"
.X "satellite, connection to Internet"
In some parts of the world, \fIsatellite connections\fP\/ are a viable
alternative.  These usually use a telephone line for outgoing data and a
satellite receiver for incoming data.  Pricing varies from very cheap to quite
expensive, but if you can't get cable or DSL, this might be your only choice.
.Le
.sp -1v
.H2 "Establishing yourself on the Internet"
.Pn getting-on-internet
The first thing you need to decide is the extent of your presence on the Net.
There are various possibilities:
.Ls B
.LI
.X "shell, account"
You could get a dialup service where you use your computer just to connect to
the ISP, and perform network functions such as reading mail and news on the
ISP's machine (a \fIshell account\fP\/).  It's a lot faster to perform these
functions on your own machine, and you have all the software you need to do so,
so this option isn't very desirable.  This option is becoming increasingly
uncommon.
.LI
You could perform all the functions on your machine, but using names and
addresses assigned to you by the ISP.
.LI
You could perform all the functions on your machine, using addresses assigned to
you by the ISP, but you would use your own domain name.
.LI
You get your own address space and use your own domain name.
.Le
.X "Dunham, Jerry"
Does it matter?  That's for you to decide.  It's certainly a very good idea to
have your own domain name.  As time goes on, your email address will become more
and more important.  If you get a mail address like \f(CW4711@flybynight.net\fP,
and Flybynight goes broke, or you decide to change to a different ISP, your mail
address is gone, and you have to explain that to everybody who might want to
contact you.  If, on the other hand, your name is Jerry Dunham, and you register
a domain \f(CWdunham.org\fP, you can assign yourself any mail address in that
domain.
.P
But how do you go about it?  One way would be to pay your ISP to do it for you.
You don't need to do that: it's easy enough to do yourself on the World-Wide
Web.  You must be connected to the Internet to perform these steps.  This
implies that you should first connect using your ISP's domain name, then
establish your domain name, and change to that domain.
.H3 "Which domain name?"
.X "domain name, registering"
We'll continue to assume that your name is Jerry Dunham.  If you live in, say,
Austin, Texas, you have a number of domain names you can choose from:
\f(CWdunham.org\fP, \f(CWdunham.com\fP, \f(CWdunham.net\fP, or even
\f(CWdunham.tx.us\fP if you want to use the geographical domain.
.P
If you live in, say, Capetown, people will probably suggest that you get the
domain \f(CWdunham.za\fP, the geographical domain for South Africa.  The problem
with that is that you are limiting yourself to that country.  If you move to,
say, Holland, you would have to change to \f(CWdunham.nl\fP\(ema situation only
fractionally better than being bound to an ISP.  The same considerations apply
to \f(CWdunham.tx.us\fP, of course.
.P
Your choice of domain name also affects the way you apply.  In the following
sections, I assume you take my advice and apply for an organizational rather
than a geographical domain.
.H3 "Preparing for registration"
.X "NIC handle"
Once upon a time, registration was handled by InterNIC, a professional body.
Since then it has been delegated to commercial companies, and the quality of
service has suffered correspondingly: they don't even appear to know the
technical terms.  For example, you may find them referring to a domain name as a
``Web Address.''  Things are still deteriorating at the time of writing:
additional companies are being allowed to register domain names, and the field
seems to attract a lot of cowboys.
.H3 "Registering a domain name"
.Pn domainreg
The only prerequisites for registering a domain name are:
.Ls B
.LI
.X "microsoft.edu"
The name must be available, though there are some legal implications that
suggest that, though you might be able to register a domain such as
\fImicrosoft.edu\fP, it might not be good for you if you do.  In fact,
\fImicrosoft.edu\fP\/ was once registered to the BISPL business school in
Hyderabad, India, presumably not in agreement with Microsoft.
.LI
You must be able to specify two name servers for it\(emsee Chapter
.Sref \*[nchdns] \&
for further details about name servers.
.Le
.P
First, check that the name is available:
.Dx
$ \f(CBwhois dunham.org\fP
No match for "DUNHAM.ORG".

The InterNIC Registration Services Host contains ONLY Internet Information
(Networks, ASN's, Domains, and POC's).
Please use the whois server at nic.ddn.mil for MILNET Information.
.De
Next, try to find a reputable registrar.  Immediately after the transfer of
registrars from InterNIC, the only company to offer this service was Network
Solutions, but now there are many.  I do not recommend Network Solutions:
they're expensive and incompetent.  If, as I recommend, you set up your mail
server to refuse mail from servers without reverse mapping, you will not be able
to communicate with them, since they do not have reverse DNS on their mail
servers, and they use unregistered names for them.  Judge for yourself what this
says about their technical competence.
.P
One registrar that many FreeBSD people use is Gandi
(\fIhttp://www.gandi.net/\fP\/), which is slightly associated with the FreeBSD
project.  So far nobody has found anything negative to say about them.  Unlike
Network Solutions, their web pages are also relatively simple to understand.
.H3 "Getting IP addresses"
.X "IP addresses, getting"
.Pn netreg
Once upon a time, it was possible to get IP addresses from InterNIC, but this
practice is now restricted to large allocations for ISPs.  Instead, get the
addresses from your ISP.  Routing considerations make it impractical to move IP
addresses from one place to another.  If you move a long distance, you should
expect to change your IP addresses in the same way as you would change your
telephone number.
.H2 "Choosing an Internet Service Provider"
.Pn select-isp
.X "Internet Service Provider, choosing"
.X "ISP"
In most cases, you will get your connection to the Internet from an \fIInternet
Service Provider\fP, or \fIISP\fP.  As the name suggests, an ISP will supply the
means for you to connect your system or your local network to the Internet.
They will probably also supply other services: most ISPs can't live on Internet
connections alone.
.P
In this chapter we'll look at the things you need to know about ISPs, and how to
get the best deal.  We'll concentrate on what is still the most common setup,
PPP over a dialup line with a V.90 modem (56 kbps), which will give you a peak
data transfer rate of about 7 kB/s.
.H2 "Who's that ISP?"
As the Internet, and in particular the number of dialup connections, explodes,
a large number of people have had the idea to become involved.  In the early
days of public Internet access, many ISPs were small companies run by very
technical people who have seen a market opportunity and have grabbed it.  Other
ISPs were small companies run by not-so technical people who have jumped on the
bandwagon.  Still other ISPs are run by large companies, in particular the cable
TV companies and the telephone companies.  Which is for you?  How can you tell
to which category an ISP belongs?  Do you care?
.P
You \fIshould\fP\/ care, of course.  Let's consider what you want from an ISP,
and what the ISP wants.  You want a low-cost, high-reliability, high speed
connection to the Internet.  You may also want technical advice and value-added
services such as DNS (see Chapter
.Sref \*[nchdns] \/)
and web pages.
.P
The main priority of a small ISP (or any other ISP, for that matter) is to get a
good night's sleep.  Next, he wants to ensure the minimum number of nuisance
customers.  After that, he wants to ensure that he doesn't go out of business.
Only \fIthen\fP\/ is he interested in the same things that you are.
.P
In the last few years, a large number of ISPs have gone out of business, and
many more have merged with other companies.  In particular, large companies
frequently bought out small techie ISPs and then ran them into the ground with
their incompetence.  For a humorous view of this phenomenon, see the ``User
Friendly'' cartoon series starting at
\fIhttp://ars.userfriendly.org/cartoons/?id=19980824\fP.
.H3 "Questions to ask an ISP"
.X "ISP, questions to ask"
So how do you choose an ISP?  Don't forget the value of word-of-mouth\(emit's
the most common way to find an ISP.  If you know somebody very technical,
preferably a FreeBSD user, who is already connected, ask him\(emhe'll certainly
be able to tell you about his ISP.  Otherwise, a lot depends on your level of
technical understanding.  It's easy to know more about the technical aspects of
the Internet than your ISP, but it doesn't often help getting good service.
Here are a few questions to ask any prospective ISP:
.LB 2m 0m 0 0 \(sq
.LI
What kind of connections do you provide?
.Aside
See the discussion on page \*[connect-types].
.End-aside
.LI
How do you charge?  By volume, by connect time, or flat rate?
.Aside
Once most ISPs charged by connect time: you paid whether you transfer data or
not.  This made it unattractive to an ISP to provide good performance, since
that would have meant that you could finish your session more quickly.
Nowadays, flat rates are becoming more popular: you pay the same no matter how
much you use the service.  The disadvantage of the flat rate is that there is no
incentive to disconnect, so you might find it difficult to establish
connections.
.P
When comparing connect time and volume rates, expect an average data transfer
rate of about 600 bytes per second for most connections via a 56 kbps modem.
You'll get up to 7 kB per second with traffic-intensive operations like file
downloading, but normally, you'll be doing other things as well, and your data
rate over the session is more likely to be 600 bytes per second if you're
reasonably active, and significantly less if not.  Faster lines typically don't
charge by connect time: in particular, DSL lines are permanently connected and
thus charge by data volume or at a flat rate.
.P
Another alternative that is again becoming more popular is a ``download
limit.''  Your flat monthly fee allows you to download up to a certain amount of
data, after which additional data costs money.  This may seem worse than a flat
rate, but it does tend to keep people from abusing the service.
.End-aside
.LI
Do you have a cheaper charge for data from your own network?
.Aside
Many ISPs maintain web proxy caches, ftp archives and network news.  If they
charge by volume, some will give you free access to their own net.  Don't
overestimate the value of this free data.
.End-aside
.LI
What speed connections do you offer?
.Aside
ADSL connections have two different rates, a faster one for downloads and a
slower one for the uplink.  That's fine if you're planning to use the system as
a client.  If you intend to run servers on your system, things can look very
different.
.P
If you are using a modem connection, they should be the fastest, of course,
which are currently 56 kbps.
.End-aside
.LI
What uplink connections do you have?
.Aside
The purpose of this question is twofold: first, see if he understands the
question.  An uplink connection is the connection that the ISP has to the rest
of the Internet.  If it's inadequate, your connection to the Internet will also
be inadequate.  To judge whether the link is fast enough, you also need to know
how many people are connected at any one time.  See the question about dialup
modems below.
.End-aside
.LI
How many hops are there to the backbone?
.Aside
Some ISPs are a long way from the Internet backbone.  This can be a
disadvantage, but it doesn't have to be.  If you're connected to an ISP with T3
all the way to the backbone, you're better off than somebody connected directly
to the backbone by an ISDN Basic Rate connection.  All other things being equal,
though, the smaller the number of hops, the better.
.End-aside
.LI
How many dialup modems do you have?
.Aside
This question has two points to make as well.  On the one hand, the total
bandwidth of these modems should not exceed the uplink bandwidth by too
much\(emlet's say it shouldn't be more than double the uplink bandwidth.  On the
other hand, you want to be able to get a free line when you dial in.  Nothing is
more frustrating than having to try dozens of times before you can get a
connection.  This phenomenon also causes people not to disconnect when they're
finished, especially if there is no hourly rate.  This makes the problem even
worse.  Of course, the problem depends on the number of subscribers, so ask the
next question too.
.End-aside
.LI
How many subscribers do you have?  What is the average time they connect per
week?
.Aside
Apart from the obvious information, check whether they keep this kind of
statistics.  They're important for growth.
.End-aside
.LI
What's your up-time record?  Do you keep availability statistics?  What are
they?
.Aside
ISPs are always nervous to publish their statistics.  They're never as good as
\fII\fP\/ would like.  But if they publish them, you can assume that that fact
alone makes them better than their competitors.
.End-aside
.LI
What kind of hardware and software are you running?
.Aside
This question will sort out the good techie ISPs from the wannabes.  The real
answers aren't quite as important as the way they explain it.  Nevertheless,
consider that you'll be better off with an ISP who also runs FreeBSD or
BSD/OS.\*F
.FS
BSD/OS is a commercial operating system closely related to FreeBSD.  If you have
a few thousand dollars to spare, you may even find it better than FreeBSD.
Check out \fIhttp://www.wrs.com/\fP\/ for further details.
.FE
Only small ISPs can afford to use UNIX machines (including FreeBSD) as routers;
the larger ones will use dedicated routers.
.P
Next, in my personal opinion, come other UNIX systems (in decreasing order of
preference, Solaris 2.X, Linux and IRIX), and finally, a long way behind,
Windows NT.  If you're looking for technical support as well, you'll be a lot
better off with an ISP who uses FreeBSD or BSD/OS.  You'll also be something
special to them: most ISPs hate trying to solve problems for typical Windows
users.
.End-aside
.LI
How many name servers do you run?
.Aside
The answer should be at least 2.  You'll probably be accessing them for your
non-local name server information, because that will be faster than sending
requests throughout the Internet.
.End-aside
.LI
Can you supply primary or secondary DNS for me?  How much does it cost?
.Aside
I strongly recommend using your own domain name for mail.  That way, if your ISP
folds, or you have some other reason for wanting to change, you don't need to
change your mail ID.  To do this, you need to have the information available
from a name server 24 hours per day.  DNS can generate a lot of traffic, and
unless you're connected to the network 100% of the time, mail to you can get
lost if a system can't find your DNS information.  Even if you are connected
100% of the time, it's a good idea to have a backup DNS on the other side of the
link.  Remember, though, that it doesn't have to be your ISP.  Some ISPs supply
free secondaries to anybody who asks for them, and you might have friends who
will also do it for you.
.P
The ISP may also offer to perform the domain registration formalities for
you\(emfor a fee.  You can just as easily do this yourself: see page
\&\*[domainreg] for more details.  Check the fee, though: in some countries, the
ISP may get a discount for the domain registration fees.  If it's big enough,
registering via the ISP may possibly be cheaper than doing it yourself.
.End-aside
.LI
Can you route a class C network for me?  What does it cost?
.Aside
If you're connecting a local area network to the Internet, routing information
must be propagated to the Net.  ISPs frequently consider this usage to be
``commercial,'' and may jack up the prices considerably as a result.
.P
Alternatives to a full class C network are a group of static addresses (say, 8
or 16) out of the ISP's own assigned network addresses.  There's no particular
problem with taking this route.  If you change ISPs, you'll have to change
addresses, but as long as you have your own domain name, that shouldn't be a
problem.
.P
.X "IP aliasing"
.X "aliasing, IP"
Another possibility might be to use \fIIP aliasing\fP.  See page \*[ip-aliasing]
for more details.
.End-aside
.LI
Can you supply me with a static address?  How much does it cost?
.Aside
It's highly desirable to have static addresses.  See page
\*[static-ip-addresses] for more details.  Unfortunately, many ISPs use static
IPs to distinguish links for commercial use from those for home use, and may
charge significantly more for a static address.
.End-aside
.LI
Do you give complete access to the Internet, or do you block some ports?
.Aside
This is a complicated question.  Many ISPs block services like smtp (mail) or
http (web servers).  If they do, you can't run a mail or web server on your own
machines.  In the case of mail, this is seldom a problem: they will provide you
with their own mail server through which you must relay your mail.  This also
allows the ISP to limit spam, which might otherwise come from any system within
the network.
.P
For http, the situation is different.  Usually ISPs charge money for supplying
access to their own web servers.  On the other hand, this arrangement can
provide much faster web access, especially if you are connected by a slow link,
and you may also save volume charges.  Ultimately it's a choice you need to
make.
.End-aside
.LI
Do you have complete reverse DNS?
.Aside
In previous editions of this book, I didn't ask this question: it seemed
impossible that any ISP would answer ``no.''  Unfortunately, times have changed,
and a number of ISPs not only don't supply DNS, they seem to think it
unnecessary.  Don't have anything to do with them: firstly, it shows complete
incompetence, and secondly it will cause trouble for you accessing a number of
sites, including sending mail to the FreeBSD mailing lists.
.End-aside
.ig
.LI
What restrictions do you place on traffic?
.Aside
Increasingly, ISPs are restricting what you can do with your connection.  Some
restrictions, like not allowing you to send spam, are good.\*F
.FS
They may formulate this differently.  Often it means that you need to relay
outbound mail through their server so that they have a check on what is going
through the network.  If their mail servers are all configured correctly,
including correct reverse DNS, there should be no problem.
.FE
Others, like ``no servers'', are not necessarily good.  They will usually direct
you to an Acceptable Use Policy with lots of fine print.  Try to get them to
explain it to you first.
.End-aside
..
.Le
.bp\" XXX
.H2 "Making the connection"
.Pn ISP-checklist
After calling a few ISPs, you should be able to make a decision based on their
replies to these questions.  The next step is to gather the information needed
to connect.  Use Table \*[info-from-ISP] to collect the information you need.
See Chapter
.Sref \*[nchppp] \&
for information about authentication, user name and password.
.Table-heading "Information for ISP setup"
.TS H
box, tab(#) ;
| lw20 | lw40 | .
Information#Fill in specific value                                       \&
=
.TH N
.sp .8v
IP address of your end of the link
.sp .8v
_
.sp .8v
IP address of the other end of the link
.sp .8v
_
.sp .8v
T{
Kind of authentication (CHAP, PAP, login)
T}
.sp .8v
_
.sp .8v
T{
User or system name
T}
.sp .8v
_
.sp .8v
T{
Password or key
T}
.sp .8v
_
.sp .8v
Primary Name Server name
.sp .8v
_
.sp .8v
Primary Name Server IP address
.sp .8v
_
.sp .8v
Secondary Name Server name
.sp .8v
_
.sp .8v
Secondary Name Server IP address
.sp .8v
_
.sp .8v
Pop (Mail) Server Name
.sp .8v
_
.sp .8v
News Server Name
.sp .8v
.TE
.Tn info-from-ISP
