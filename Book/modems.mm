.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: modems.mm,v 4.10 2003/04/02 03:11:02 grog Exp grog $
.\"
.Chapter \*[nchmodems] "Serial communications"
UNIX has always had a high level of support for serial lines, but their purpose
has changed dramatically.  In the early 70s, the standard ``terminal'' was a
Teletype KSR35, a 10-character-per-second serial printer with keyboard.  Early
UNIX serial line support was geared towards supporting these devices, either
directly connected, or via a modem.
.P
.X "glass tty"
.X "tty, glass"
Even in the early 80s, when 4.2BSD introduced network support, things didn't
change much:  the network support used different hardware.  By this time,
the Teletypes had been replaced with \fIglass tty\fP\/s, in other words serial
terminals with a monitor instead of a printer.  The speeds had gone up from the
110 bps of the Teletype to 9600 bps, but the underlying principles hadn't
changed.
.P
It wasn't until the last 10 years that the glass ttys were replaced by display
boards directly connected to the system bus, or by other machines connected by
Ethernet.  The role of the serial port has changed completely: nowadays, they're
used mainly for mice and dialup Internet connections.
.P
This change in use has invalidated a few basic concepts.  Only a few years ago,
the standard ``high-speed'' modem was a V.22bis 2400 bps modem, even then too
slow for an Internet connection.  The standard data communication line was 56
kb/s, and it was invariably a leased line.  As a result, the Internet grew up
assuming that connections were leased lines, and therefore permanently
connected.  Even today, the Internet protocols do not deal well with dialup
access.
.P
.X "UUCP"
On the other hand, \fIUUCP\fP\/ did use dialup access.  As a result, provisions
for dialup access in UNIX tend to be derived from \fIUUCP\fP.  This doesn't make
for smooth integration.
.P
In this chapter, we'll look at the way FreeBSD handles serial communications, at
how modems work, and how the two fit together.
.H2 "Terminology"
.X "modem, terminology"
.X "terminology, modem"
.X "Data Terminal Equipment"
.X "DTE"
.X "Data Communication Equipment"
.X "DCE"
.X "dataset"
Any serial connection has two ends, which may be computers, terminals, printers
or modems.  In modem terminology, the computers are \fIData Terminal
Equipment\fP\/ or \fIDTE\fP\/ (this terminology arose at a time when the device
connected to a modem was usually a terminal), and modems are \fIData
Communication Equipment\fP\/ or \fIDCE\fP.  You'll also sometimes hear the name
\fIdataset\fP\/ for a modem.
.H2 "Asynchronous and synchronous communication"
.Pn async
.X "synchronous communication"
.X "communication, synchronous"
.X "asynchronous communication"
.X "communication, asynchronous"
There are two different ways to transmit serial data, called \fIsynchronous\fP\/
and \fIasynchronous\fP\/ communication.  They grew up in different worlds:
.H3 "Asynchronous communication"
.X "titty"
.X "teletype"
.X "tty"
Asynchronous communication predates computers.  It was originally developed to
run \fIteletypewriters\fP, electrical typewriters that were run off a serial
data stream, the best-known of which were made by the Teletype corporation.
These machines were frequently used to provide a remote transcript of what
somebody was typing miles away, so they would typically print one character at a
time, stop, and wait for the next.  In the early days of UNIX, the standard
terminal was a Teletype model KSR35, commonly just called \fIteletype\fP\/ or
\fItty\fP\/ (pronounced ``titty'').
.P
Here's a picture of a typical byte encoding:
.PS
h=.3i
v=.8i
off=.2i
line dotted right 2*h; line down v
.\" Start bit
.\" All bits start and finish at bottom baseline
ST: line right h; line dotted up v; line invis down v
.\" Parity
P:  line right h; line dotted up v; line invis down v
D7: line right h; line invis up v; line dotted down v
D6: line right h
D5: line invis right h; line up v from D5.w; line right h; line down v
D4: line right h
D3: line invis right h; line up v from D3.w; line right h; line down v
D2: line right h; line invis up v; line dotted down v
D1: line right h; line up v; line invis down v
D0: line invis right h; line dotted up v from D0.w; line right h; line invis down v
S1: line invis right h; line dotted up v from S1.w; line right h; line invis down v
S2: line invis right h; line dotted up v from S2.w; line right h; line dotted down v
    line invis up v; line dotted right 2*h

"Start" at ST.c+(0,-off)
"Parity" at P.c+(0,-off)
"\f(CW1\fP" at P.c+(0,v/2)
"D7" at D7.c+(0,-off)
"\f(CW1\fP" at D7.c+(0,v/2)
"D6" at D6.c+(0,-off)
"\f(CW1\fP" at D6.c+(0,v/2)
"D5" at D5.c+(0,-off)
"\f(CW0\fP" at D5.c+(0,v/2)
"D4" at D4.c+(0,-off)
"\f(CW1\fP" at D4.c+(0,v/2)
"D3" at D3.c+(0,-off)
"\f(CW0\fP" at D3.c+(0,v/2)
"D2" at D2.c+(0,-off)
"\f(CW1\fP" at D2.c+(0,v/2)
"D1" at D1.c+(0,-off)
"\f(CW1\fP" at D1.c+(0,v/2)
"D0" at D0.c+(0,-off)
"\f(CW0\fP" at D0.c+(0,v/2)
"Stop" at S1.c+(0,-off)
"Stop" at S2.c+(0,-off)
"Bit 1" at S1.c+(0,-off-.17i)
"Bit 2" at S2.c+(0,-off-.17i)
.PE
.Figure-heading "Asynchronous byte encoding"
This figure shows an encoding for the letter \f(CWj\fP, in binary
\f(CW01101011\fP.  We'll see a number of things about it:
.Ls B
.LI
Before the character starts, the line is idle, which shows as a \fIhigh\fP\/
level: this indicates to the teletype that the line is still connected.
.LI
.X "start bit"
First comes a \fIstart bit\fP.  In the olden days, this bit started the motor of
the teletype mechanism.  Now it signals that data is coming.
.LI
.X "parity bit"
Next comes a \fIparity bit\fP.  To detect any transmission errors, this
character is encoded with \fIeven parity\fP.  The parity bit is set to \f(CW1\fP
if the character contains an odd number of bits, and to \f(CW0\fP otherwise,
which ensures that the character, including the parity bit, always has an even
number of bits.  If a single bit is corrupted during transmission, the character
will arrive with odd parity, and the receiver will detect an error.
.LI
Next come the bits of the character, last bit first.  We represent \f(CW1\fP
with a low level and \f(CW0\fP with a high level.
.LI
.X "stop bit"
Finally come one or two \fIstop bits\fP.  The stop bits were originally intended
to give the teletype time to stop the motor, but they are now largely
superfluous.  You needed two stop bits for a teletype, but nowadays you should
always use one.
.LI
This example also shows something else of importance: there are a number of ways
to encode the character.  How many bits?  How many stop bits?  Odd parity?  Even
parity?  No parity?  Mark parity (always a \f(CW1\fP bit)?  Space parity (always
a \f(CW0\fP bit)?  How much time from one bit to the next (what bit rate)?
They're all set with the
.Command stty
program (see man page \fIstty(1)\fP\/), but if you set them wrongly, you'll run
into trouble.
.LI
.X "start-stop communication"
.X "communication, start-stop"
The encoding isn't very efficient.  For every character you send, you also send
a start bit and a stop bit.  Most communications no longer use the parity bit,
but this still means that you have a 25% overhead on communication: for every 8
bits, you send 10, and you could send up to 12, as in this example.  We'll see
that synchronous communication doesn't have this problem.  Users of synchronous
communication protocols often refer to asynchronous communication as
\fIstart-stop\fP\/ communication.
.Le
.H3 "Synchronous communication"
.X "synchronous communication"
.X "communication, synchronous"
.X "SYN character"
.X "character, SYN"
By contrast with asynchronous communication, synchronous communication comes
from the mainframe world, and it assumes that data does not come one byte at a
time.  Instead, it transmits data in \fIblocks\fP.  Each block is preceded by
one or two \fISYN\fP\/ characters that tell the receiver that data is coming,
and that enable it to determine the correct orientation of the bits in the
data.
.P
All modern modems use synchronous communication on the phone line, because it is
more efficient, and it's the basis of protocols such as SNA and X.25, but you
will almost never see any other use of it in UNIX systems.
.H2 "Serial ports"
.X "serial port"
.X "port, serial"
.Pn serial-hardware
Nowadays, all PCs come equipped with two serial ports, which are called
\f(CWCOM1:\fP and \f(CWCOM2:\fP in the DOS world.  UNIX names are different, and
FreeBSD calls these same devices \f(CWsio0\fP and \f(CWsio1\fP.  It's possible
to connect up to four direct serial ports on a standard PC, but due to the
design of the board, each one requires a separate IRQ line.  If you put two
serial ports on the same interrupt line, neither of them will work.
.P
.ne 5v
The first two devices, \fIsio0\fP\/ and \fIsio1\fP, normally use the default
IRQs 4 and 3.  By default, however, PC manufacturers put \fICOM3:\fP and
\fICOM4:\fP\/ also at IRQs 4 and 3.  How can this work?  It can't, if you also
have \fICOM1:\fP\/ and \fICOM2:\fP\/ enabled at those IRQs.  However, DOS tends
to do only one thing at a time, so you can use different ports at different
times on the same IRQ, as long as the interrupts aren't enabled on more than one
of the ports at a time.  This restriction is unacceptable for UNIX, so we have
to put them somewhere else.  The only unused interrupt available to 8-bit boards
is IRQ 5, originally intended for a second parallel printer port.
.P
There's a very good chance that IRQ 5 will already be occupied.  What can you
do?  If one of the boards has a 16-bit or better interface, you can check if one
of the interrupts 10 to 15 is available.  All EISA and PCI boards fit into this
category, and so do ISA boards with two connectors to the motherboard.
Unfortunately, a lot of ISA serial cards only have an 8-bit interface.  The only
alternative is an intelligent serial board that only occupies a single
interrupt.  In this case, you will probably have to build a custom kernel.  See
the man page \fIsio(4)\fP.
.H3 "Connecting to the port"
.X "Receive Data line"
.X "line, Receive Data"
.X "RxD"
.X "Transmit Data line"
.X "line, Transmit Data"
.X "TxD"
.X "Signal Ground line"
.X "line, Signal Ground"
Theoretically, a serial line can consist of only three wires: a \fIReceive
Data\fP\/ line, often abbreviated to \fIRxD\fP, a \fITransmit Data\fP line
(\fITxD\fP\/), and a \fISignal Ground\fP\/ line (\fISG\fP\/).  In fact, it is
possible to get a link to work like this, but there are a number of problems:
.Ls B
.LI
How do we know when the other end is able to accept data?  It may be busy
processing data it has already received.
.LI
How do we know when it's even switched on?
.LI
In the case of a modem, how do we know when it is connected to the modem at the
other end?
.Le
.X "RS-232"
.X "EIA-232"
.X "V.24"
We solve these questions, and more, by the use of additional lines.  The most
common standard is \fIRS-232\fR, also known as \fIEIA-232\fR, a standard for DCE
to DTE connection.  In Europe, it is sometimes confused with the \fICCITT
V.24\fR standard, though V.24 does not in fact correspond exactly to RS-232.
Most external modems display some of these signals on LED, but modem
manufacturers love to create alternative abbreviations for signal names.  Here
are the signals that RS-232 defines, with some of the more common abbreviations
that you may see on external modems.
.br
.\".DF
.ne 2i
.Table-heading "RS-232 signals and modem LEDs"
.TS H
tab(#) ;
lfCWp9 | lf(R)  | lf(CW)w6p9 | lf(R)w48  .
\fB\s10RS-232##\fBModem
\fB\s10name#\fBPin#\fBLED#\fBPurpose
_
.TH N
PG#1##T{
Protective ground.  Used for electrical grounding only.
T}
.X "TxD, RS-232 signal"
.X "RS-232 signal, TxD"
.sp .4v
TxD#2#T{
TD D1
T}#T{
Transmitted data: data coming from the DTE to the modem.
T}
.X "RxD, RS-232 signal"
.X "RS-232 signal, RxD"
.sp .4v
RxD#3#T{
RD D2
T}#T{
Received data: data coming from the modem to the DTE.
T}
.X "RTS, RS-232 signal"
.X "RS-232 signal, RTS"
.X "Request to Send, RS-232 signal"
.X "RS-232 signal, Request to Send"
.sp .4v
RTS#4##T{
Request to send.  Indicates that the device has data to output.
T}
.X "CTS, RS-232 signal"
.X "RS-232 signal, CTS"
.X "Clear to Send, RS-232 signal"
.X "RS-232 signal, Clear to Send"
.sp .4v
CTS#5##T{
Clear to send.  Indicates that the device can receive input.
T}
.Pn DSR
.X "DSR, RS-232 signal"
.X "RS-232 signal, DSR"
.X "Data Set Ready, RS-232 signal"
.X "RS-232 signal, Data Set Ready"
.sp .4v
DSR#6#T{
MR PW ON
T}#T{
Data set ready.  Indicates that the modem is powered on and has passed
self-test.  On some modems, \f(CWPW\fP indicates that power is on, and
\f(CWMR\fP indicates that it is operative.
T}
.X "SG, RS-232 signal"
.X "RS-232 signal, SG"
.sp .4v
SG#7##T{
Signal ground.  Return for the other signals.
T}
.X "DCD, RS-232 signal"
.X "RS-232 signal, DCD"
.X "Data Carrier Detect, RS-232 signal"
.X "RS-232 signal, Data Carrier Detect"
.sp .4v
DCD#8#T{
CD M5
T}#T{
Carrier detect.  Indicates that the modem has connection with another modem.
T}
.X "Data Terminal Ready, RS-232 signal"
.X "RS-232 signal, Data Terminal Ready"
.X "DTR, RS-232 signal"
.X "RS-232 signal, DTR"
.Pn DTR
.sp .4v
DTR#20#T{
DTR S1
T}#T{
Data terminal ready.  Indicates that the terminal or computer is ready to talk
to the modem.
T}
.X "RI, RS-232 signal"
.X "RS-232 signal, RI"
.sp .4v
RI#22#AA#T{
Ring indicator.  Raised by a modem to indicate that an incoming call is
ringing.  The \f(CWAA\fP indicator on a modem will usually flash when the
incoming call is ringing.
T}
.sp .4v
##AA#T{
``Auto Answer.''  Indicates that the modem will answer an incoming call.
T}
.sp .4v
##HS#T{
``High Speed.''  Indicates that the modem is running at a higher speed than its
minimum.  Individual modems interpret this differently, but you can assume that
something is wrong if your modem has this indicator and it's off during
transmission.
T}
.sp .4v
##MNP#T{
Indicates that error correction is active.
T}
.sp .4v
##OH#T{
``Off hook.''  Indicates that the modem has some connection with the phone line.
T}
.sp .4v
##PW#T{
Indicates that modem power is on.  May or may not imply \f(CWDSR\fP.
T}
.TE
.sp 1.5v
.\".DE
.X "DCD, RS-232 signal"
The line \fIDCD\fP\/ tells the DTE that the modem has established a connection.
We'll look at how to use this information on page \*[got-DCD].
.P
.X "clocks, RS-232"
In addition to these signals, synchronous modems supply \fIclocks\fP\/ on pins
17 and 19.  For more details about RS-232, see \fIRS-232 Made easy\fR by Martin
Seyer.
.X "Seyer, Marty"
.H3 "When can I send data?"
.Pn handshaking
.X "hardware handshaking"
.X "handshaking, hardware"
.X "software handshaking"
.X "handshaking, software"
.X "flow control"
There are two ways to determine if the other end is prepared to accept data:
\fIhardware handshaking\fP\/ and \fIsoftware handshaking\fP.  Both are also
referred to as \fIflow control\fP.  In each case, the handshaking is
symmetrical.  We'll look at it from the point of view of the DTE, because this
is the more common viewpoint.
.P
.X "CTS, RS-232 signal"
.X "Clear to Send, RS-232 signal"
.X "Request to Send, RS-232 signal"
.X "half-duplex transmission"
.X "transmission, half-duplex"
.X "RTS, RS-232 signal"
In hardware handshaking, the DCE raises \fICTS\fP\/ (\fIClear to Send\fP\/) when
it's ready to accept input.  The DTE only transmits data when CTS is asserted
from the other end.  You'll often see that the DTE asserts RTS (\fIRequest
to send\fP\/) when it wants to send data.  This is a throwback to the days of
\fIhalf-duplex\fP\/ modems, which could only transmit in one direction at a
time: \fIRTS\fP\/ was needed to switch the modem into send mode.
.P
.X "software handshaking"
.X "handshaking, software"
.X "X-on/X-off"
\fISoftware handshaking\fP\/ is also called \fIX-on/X-off\fP.  The DCE sends a
character (\fIX-off\fP, which corresponds to \fBCtrl-S\fP) when the buffer is
full, and another (\fIX-on\fP, corresponding to \fBCtrl-Q\fP) when there is
space in the buffer again.  You can also use this method on a terminal to
temporarily stop the display of a lot of data, and then restart it.  It's no
longer a good choice for modems.
.P
For hardware handshake to work, your modem must be configured correctly, and you
must have the correct cables.  If it isn't, the symptoms will be very slow
response when transferring large quantities of data: at a higher level, TCP can
recover from these overruns, but it takes at least a second to do so every time.
We'll see how to check that your modem has the correct kind of flow control on
page \*[modem-config].
.H2 "Modems"
.X "modem"
A \fImodem\fP\/ is a device that transfers digital data into a form suitable for
transmission over a transmission line, which is usually a telephone line.
Telephone lines are limited to a frequency of about 3.6 kHz, and this limited
the speed of older modems to about 1200 bits per second.  Modern modems use many
sophisticated techniques to increase the speed way beyond this.  Current modems
transmit at 56 kilobits per second.
.P
Let's consider the modem connection in the reference network on page
\*[reference-net-page], which is repeated in figure
.Sref \*[netmodem] .
As we can see, there are three connections:
.Ls B
.LI
The connection from the router \f(CWgw\fP to the local modem, connected at
57,600 bits per second.
.LI
The connection between the modems, at 56,000 bits per second.
.LI
The connection from the ISP's modem to his router, at 115,200 bits per second.
.Le
.X "baud, definition"
You'll also note another value specified here: the connection between the modems
is 2,400 baud.  Isn't a \fIbaud\fP\/ the same thing as a bit per second?  No,
not always.  The term \fIbaud\fP\/ is a representation of the frequency of data
on a serial line.  On the connections between the systems and the modem, which
handle raw digital data, it corresponds to the bit rate.  On the modem line, it
doesn't.  Here, it indicates that 2,400 units of data are sent per second.
.P
.X "bit rate, definition"
Unfortunately, many people use the term \fIbaud\fP\/ where \fIbit rate\fP\/
should be used.  This didn't make any difference in the old days with simple
modems where the bit rate and baud rate were the same, but nowadays it's
confusing.
.PS
        boxht = .4i
        boxwid = .6i

        move right 1.5i
GW:     box
        line from GW.n to GW.n+(0,.3)
        "\s14\(bu\s0" at GW.n+(0,-.03)
        "\s14\(bu\s0" at GW.s+(0,-.03)
        "\f(CWgw\fP" at GW above
        "Router" at GW below

M1:     box ht .2i at GW.s+(0,-.3)
        "\s14\(bu\s0" at M1.n+(0,-.03)
        "\s14\(bu\s0" at M1.s+(0,-.03)
        "modem" at M1

M2:     box ht .2i at GW.s+(0,-.8)
        "\s14\(bu\s0" at M2.n+(0,-.03)
        "\s14\(bu\s0" at M2.s+(0,-.03)
        "modem" at M2

ISP:    box at GW.s+(0,-1.3)
        "\s14\(bu\s0" at ISP.n+(0,-.03)
        "\s14\(bu\s0" at ISP.s+(0,-.03)
        "ISP" at ISP above
        "router" at ISP below

L1:     line from GW.s to M1.n
        "Local modem connection, 57,600 bps" ljust at L1.c+(.4,0)

PPP:    line from M1.s to M2.n
        "PPP link, 56,000 bps, 2400 baud" ljust at PPP.c+(.4,0)

L2:     line from M2.s to ISP.n
        "ISP modem connection, 115,200 bps" ljust at L2.c+(.4,0)

        line from ISP.s to ISP.s+(0,-.3)

        "DTE" rjust at GW.w+(-.5,0)
        "DCE" rjust at M1.w+(-.5,0)
        "DCE" rjust at M2.w+(-.5,0)
        "DTE" rjust at ISP.w+(-.5,0)
.PE
.Figure-heading "Network modem connection"
.Fn netmodem
.H3 "Modem speeds"
.X "modem, speeds"
.X "protocol, modem"
.X "modem, protocol"
.X "data compression, modem"
.X "modem, data compression"
Two factors determine the data transmission speed of a modem: the
\fIprotocol\fP\/ and the use of \fIdata compression\fP.  Table
.Sref \*[modemprotocols] \&
.\" XXX
on page
.Sref \*[modemprotocols-page]  \&
gives an overview of modem protocols and their speeds.
.P
Currently, the most popular modem protocol is V.90.  V.90 pushes modem
technology to the limit, and it only works when the other end of the link is a
digital (ISDN) connection.  You can't get a 56 kb/s connection with any kind of
analogue modem at the other end.  As a result, they're really only suitable for
connection to a large ISP site.  In addition, the actual speed of the connection
depends greatly on the telephone line quality, so the difference between a V.90
and a V.34bis modem may not be as much as it appears.
.H3 "Data compression"
.X "data compression"
.Pn modem-data-compression
.X "V.42bis, protocol"
.X "protocol, V.42bis"
.X "MNP-5, protocol"
.X "protocol, MNP-5"
.X "V.42, protocol"
.X "protocol, V.42"
.X "MNP2-4, protocol"
.X "protocol, MNP2-4"
In addition, you usually have a choice of data compression: \fIV.42bis\fP\/ or
\fIMNP-5\fP.  The choice depends on what the modem at the other end of the line
does.  You can set most modems to negotiate either protocol.  These protocols
include related error correction standards, called \fIV.42\fP\/ or
\fIMNP2-4\fP\/ respectively.  If you believe the sales claims, these compression
protocols will give you up to 100% increase in data throughput.  Consider this
the upper limit; a lot of data is binary, and when ftp'ing a typical gzipped tar
archive, you will probably get almost no speed improvement.
.P
.X "modem, latency"
.X "latency, modem"
.ne 3v
Data compression has one negative side: it increases the data rate, but it also
increases \fIlatency\fP, the time it takes for data to get from the local DTE to
the remote DTE.  The data doesn't take as long on the line, but it spends more
time in the modems being compressed and uncompressed.  If you're running a
protocol like PPP that supplies optional compression in the software, you may
find it advantageous to turn off compression.  We'll look at that again in
\*[chppp].
.Table-heading "Modem protocols and speeds"
.TS H
tab(#) ;
 l | r  .
Protocol#Speed (bps)
_
.TH
Bell 203#300
V.21#300
Bell 212#1200
V.22#1200
V.22bis#2400
V.32#9600
V.32bis#14400
V.34#28800
V.34bis#33600
V.90#56000
.TE
.Tn modemprotocols
.H3 "The link speed"
The standard PC serial hardware can run at speeds that are a fraction of
115,200 bps (in other words, 115200 divided by a small integer).  This gives the
following combinations:
.br
.ne 1i
.Table-heading "Serial line speeds"
.TS H
tab(#) ;
 r | r  .
Divisor#Speed (bps)
_
.TH
1#115200
2#57600
3#38400
4#28800
5#23040
6#19200
.TE
.sp 1.5v
.ne 3v
You'll notice that it can't run at 33600 or 56000 bps.  Also, looking at the
example above, you'll note that all three links run at different speeds.  How
can that work?  Only a few years ago, it wouldn't, but modern modems can
\fIbuffer\fP\/ data.  For example, the ISP can send data to the modem far faster
than the modem can send it to the other modem.  It stores the data in internal
memory until it can be transmitted.  This can also happen at the other end.  If
you misconfigure your line so that the local link runs at 9600 bps, things will
still work, but of course the total speed is the speed of the slowest link, in
this case 9600 bps.
.P
.ne 3v
This flexibility brings a problem with it: the modem can't know in advance how
fast the connection to the computer is.  It needs a way to find out.  The modem
solves the question of local line speed by a trick: all commands start with
\f(CWAT\fP or \f(CWat\fP (you're not allowed to mix cases, like \f(CWaT\fP or
\f(CWAt\fP).  It can recognize these characters even if they arrive at the wrong
speed, and thus it can establish the speed of the connection.
.H3 "Dialing out"
.X "Auto-Call Unit"
.X "ACU"
Nowadays, all modems are capable of dialing.  That wasn't always the case, and
in some old documentation you may find references to an \fIAuto-Call Unit\fP\/
or \fIACU\fP, which is simply the dialler part of a modem connected via a
separate port.  Typically, one ACU could serve multiple modems.
.P
.X "Hayes command set"
.X "AT command set"
.X "V.25, protocol"
.X "protocol, V.25"
Nearly every modern modem uses a command set designed by Hayes Corporation,
which is thus called the \fIHayes Command Set\fP.  We'll look at it in the
following section.  It is also sometimes called the \fIAT command set\fP,
because nearly all the commands start with the sequence \f(CWAT\fP.  The CCITT
also created an autodial recommendation, \fIV.25\fP, which was occasionally
implemented, but now appears to be dead.
.H2 "Modem commands"
.X "modem, commands"
.X "commands, modem"
.Pn modem-config
.X "S registers, modem"
Modern modems store their state in a number of registers, called \fIS
registers\fP.  The register use varies somewhat from manufacturer to
manufacturer, but most modems have a number in common.  They each store a
one-byte value, ranging between 0 and 255.  Here's a list of the more important
ones for a Rockwell V.34 chip set.  The name of the chip set is not the same as
the name of the modem.  You'll note that one of the commands enables you to find
out the chip set version, as we'll see in the example that follows.
.ne 1i
.Table-heading "Selected S registers"
.TS H
tab(#) ;
 lfCWp9 | lw57  .
\fRRegister number#Purpose
_
.TH
.Pn auto-answer
S0#T{
Number of rings until auto-answer.  0 disables auto-answer.  Set to 0 for no
automatic answer, or 1 for auto-answer.
T}
.sp .4v
.X "escape character, modem"
.X "modem, escape character"
S2#T{
The \fIescape character\fP, which lets you return from online mode to command
mode.  Normally, this character is a \f(CW+\fP.  To return to command mode, wait
a second after any previous input, enter \f(CW+++\fP, and wait a second, after
which the modem should reply with \f(CWOK\fP.
T}
.sp .4v
.X "blind dialing, modem"
.X "modem, blind dialing"
S6#T{
The time, in seconds, to wait before \fIblind dialing\fP.  If you have set your
modem to not wait for a dial tone (maybe because it doesn't understand the dial
tone), it will wait this long and then try to dial anyway.
T}
.sp .4v
S7#T{
The number of seconds to wait after dialing before \f(CWDCD\fP must be asserted
(before a connection is established).  If this is set too short, you will not
be able to establish a connection.  If it's too long, you will waste time when
there is no answer or the line is busy.
T}
.sp .4v
S11#T{
The duration of DTMF (dialing) tones.  If these are set incorrectly, the
telephone exchange may not understand the number you dial.
T}
.TE
.sp 1.5v
The \f(CWAT\fP command set tells the modem to do something specific.  Here are
some of the more important ones.
.br
.ne 1i
.Table-heading "Selected AT commands"
.TS H
tab(#) ;
lfCWp9 | lw59 .
\fRCommand#Meaning
_
.TH
A/#Redial the last number.
ATA#T{
Answer an incoming call manually.  This is an alternative to auto-answer by
setting \f(CWS0\fP.
T}
.sp .4v
ATD\f(BInumber\fP\/#T{
Dial \f(BInumber\fP\/.  This command has a large number of options, but if your
modem is set up correctly, you probably won't need any of them.
T}
.sp .4v
ATE\f(BInumber\fP\/#T{
Enable command echo if \f(BInumber\fP\/ is 1, disable it if \f(BInumber\fP\/ is
0.  The setting of this parameter can be important for some chat scripts, which
may not respond correctly otherwise.
T}
.sp .4v
ATH0#T{
Disconnect the line.
T}
.sp .4v
ATI\f(BInumber\fP\/#T{
Display modem identification.  The values of \f(BInumber\fP\/ vary from one
modem to the next.  See the examples below.
T}
.sp .4v
ATL\f(BInumber\fP\/#T{
Set the speaker volume.  \f(BInumber\fP\/ ranges from 0 to 3.  0 means ``speaker
off,'' 3 is the loudest.
T}
.sp .4v
ATM\f(BInumber\fP\/#T{
Determine when the speaker is on.  0 means ``always off,'' 1 means ``speaker on
until connect,'' 2 means ``speaker always on,'' and 3 means ``speaker off during
dialing and receiving.''
T}
.sp .4v
ATO0#T{
Go back online from command mode.  You don't need this command when dialing: the
modem automatically goes online when the connection is established.
T}
.sp .4v
.X "steam dialing, modem"
.X "modem, steam dialing"
ATP#T{
Select pulse dial.  If your exchange doesn't understand DTMF (tone) dialing,
you should set this mode.  Never use it if your exchange understands DTMF:
pulse dialing (also known as \fIsteam dialing\fP\/) is \fImuch\fP\/ slower.
T}
.sp .4v
ATQ\f(BInumber\fP\/#T{
If \f(BInumber\fP\/ is 0, suppress result codes (like \f(CWOK\fP after every
command).  If \f(BInumber\fP\/ is 1, enable them.  This value can be of
importance for chat scripts.
T}
.sp .4v
ATS\f(BIr\fP\/=\f(BIn\fP\/#T{
Set the value of S register \f(BIr\fP\/ to \f(BIn\fP.
T}
.sp .4v
ATS\f(BInumber\fP\/?#T{
Display the contents of an \fIS\fP\/ register.  See the example below.
T}
.sp .4v
ATT#T{
Set tone (DTMF) dialing.
T}
.sp .4v
ATV\f(BInumber\fP\/#T{
If \f(BInumber\fP\/ is 0, return result codes in numeric form.  If it's 1,
return text.  Don't rely on either form to be consistent from one modem to the
next.
T}
.sp .4v
ATX\f(BInumber\fP\/#T{
Determine the form of the result codes.  This depends a lot on the manufacturer,
but it's important for chat scripts.  If you run into trouble, with chat
scripts, check your modem documentation.
T}
.sp .4v
ATZ#T{
Reset modem configuration to default values.
T}
.sp .4v
AT&K\f(BInumber\fP\/#T{
Select flow control method.  Normally, 3 enables RTS/CTS flow control, which is
what you want.
T}
.sp .4v
AT&R\f(BInumber\fP\/#T{
If \f(BInumber\fP\/ is 0, CTS is only asserted if the DTE asserts RTS, even if
the modem is able to receive data.  If it's set to 1, it behaves normally.  Make
sure this value is set to 1.
T}
.sp .4v
AT&T\f(BInumber\fP\/#T{
Perform modem-specific test \f(BInumber\fP.  This command is the origin of the
statement: ``UNIX is a trademark of AT&T in the USA and other countries.  AT&T
is a modem test command.''
T}
.sp .4v
AT&V#T{
View the current configuration.  See the example below.
T}
.sp .4v
AT&W\f(BInumber\fP\/#T{
Store the current configuration as \fIprofile\fP\/ \f(BInumber\fP.  Most
external modems can store two \fIprofiles\fP, or configurations.  If
\fInumber\fP\/ is not specified, write the profile specified in a previous
\f(CWAT&Y\fP command.  See the example below.
T}
AT&Y\f(BInumber\fP\/#T{
Decide which profile (0 or 1) will be loaded when the modem is reset, and which
will be written by the command \f(CWAT&W\fP
T}
.TE
.sp 1v
.H3 "Dialing out manually"
.Pn got-DCD
In this section, we'll look at what needs to be done to establish a dial-out
connection.  You don't normally do this yourself: some software will do it for
you automatically.  It's useful to know what goes on, though: it can be of
immense help in solving connection problems.
.P
There are two distinct things that you want to do with the modem: first, you
send commands to the modem to set up the link.  Once the link is established,
you don't want to talk to the modem any more, you want to talk to the system at
the other end of the link.
.P
In the old days, the system used a separate ACU to establish the connection, and
the solution was simple: the system issued the dialing commands to the ACU and
opened the modem in such a manner that the open did not complete until a
connection had been established.  Nowadays, the modem handles both dialing and
the establishment of connection.  But to do so, the system has to open the modem
before communication has been established.
.P
.X "DCD, RS-232 signal"
.X "RS-232 signal, DCD"
The terminal parameter \f(CWclocal\fP enables communication with a device that
is not asserting \fIDCD\fP (such as a modem that hasn't established a
connection yet).  When it starts, the software sets \f(CWclocal\fP.  When it has
finished talking to the modem and wants to wait for the connection to be
established, it resets (turns off) \f(CWclocal\fP and waits for \fIDCD\fP.  You
can check this with the
.Command stty
command:
.Dx
# \f(CBstty -f /dev/cuaa2 -a\fP
ppp disc; speed 57600 baud; 0 rows; 0 columns;
lflags: -icanon -isig -iexten -echo -echoe -echok -echoke -echonl
        -echoctl -echoprt -altwerase -noflsh -tostop -flusho -pendin
        -nokerninfo -extproc
iflags: -istrip -icrnl -inlcr -igncr -ixon -ixoff -ixany -imaxbel ignbrk
        -brkint -inpck ignpar -parmrk
oflags: -opost -onlcr -oxtabs
cflags: cread cs8 -parenb -parodd hupcl \f(CB-clocal\fP -cstopb crtscts -dsrflow
        -dtrflow -mdmbuf
cchars: discard = ^O; dsusp = ^Y; eof = ^D; eol = <undef>;
        eol2 = <undef>; erase = ^?; intr = ^C; kill = ^U; lnext = ^V;
        min = 1; quit = ^\e; reprint = ^R; start = ^Q; status = ^T;
        stop = ^S; susp = ^Z; time = 0; werase = ^W;
.De
This example, taken when the modem is connected, shows \f(CWclocal\fP reset.  As
you can see, this is indicated by the text \f(CW-clocal\fP.
.P
.X "DCD, RS-232 signal"
.X "RS-232 signal, DCD"
There's a problem here: what if this line is also enabled for dialup?  As we
shall see on page \*[dialin], there will be a
.Command getty
process in the process of opening the line.  It won't succeed until \fIDCD\fP\/
is asserted, so we can dial with no problem.  But when the connection is
established, how do we stop
.Command getty
from being there first?
.P
.X "sio0, device"
.X "cuaa0, device"
.X "cuaa1, device"
.X "ttyd0, device"
.X "device, sio0"
.X "device, cuaa1"
.X "device, ttyd0"
The FreeBSD solution is to create separate devices for each case.  For the
second serial port, \fIsio1\fP, the system creates a file
.Device cuaa1
for dialing out, and
.Device ttyd1
for dialing in.  If \fIcuaa1\fP\/ is open, an open on \fIttyd1\fP\/ does not
complete when connection is established.
.H3 "Dialing out\(eman example"
For an example of what you might look at, let's consider a manual dialup to an
ISP.  This assumes that you are using user PPP (see page \*[user-ppp]) and that
have an entry \f(CWISP\fP in your
.File /etc/ppp/ppp.conf .
If you don't have an entry for an ISP, you can still test the modem, but in this
case you won't be able to dial.  In this case, simply omit the text \f(CWISP\fP.
.Dx
# \f(CBppp ISP\fP
User Process PPP. Written by Toshiharu OHNO.
Using interface: tun0
Interactive mode
ppp ON freebie> \f(CBterm\fP                      \fIgo into direct connect mode\fP\/
Enter to terminal mode.
Type `~?' for help.
\f(CBat\fP                                        \fIsynchronize with the modem\fP\/
OK
\f(CBat&v\fP                                      \fIlook at the modem profile\fP\/
ACTIVE PROFILE:
B0 E1 L0 M1 N1 Q0 T V1 W0 X4 Y0 &C1 &D2 &G0 &J0 &K4 &Q5 &R1 &S0 &T5 &X0 &Y0
S00:000 S01:000 S02:043 S03:013 S04:010 S05:008 S06:002 S07:060 S08:002 S09:006
S10:014 S11:090 S12:050 S18:000 S25:005 S26:001 S36:007 S37:000 S38:020 S44:020
S46:138 S48:007 S95:000

STORED PROFILE 0:
B0 E1 L0 M1 N1 Q0 T V1 W0 X4 Y0 &C1 &D2 &G0 &J0 &K4 &Q5 &R1 &S0 &T5 &X0
S00:000 S02:043 S06:002 S07:060 S08:002 S09:006 S10:014 S11:090 S12:050 S18:000
S36:007 S37:000 S40:104 S41:195 S46:138 S95:000

STORED PROFILE 1:
B0 E1 L1 M1 N1 Q0 T V1 W0 X4 Y0 &C1 &D2 &G0 &J0 &K3 &Q5 &R1 &S0 &T5 &X0
S00:000 S02:043 S06:002 S07:060 S08:002 S09:006 S10:014 S11:090 S12:050 S18:000
S36:007 S37:000 S40:104 S41:195 S46:138 S95:000

TELEPHONE NUMBERS:
0=T1234567890                          1=
2=                                     3=

OK
.De
The term \fIprofile\fP\/ refers to a set of the complete configuration
information for the modem.  External modems can usually store two different
profiles.  Some modems may not have any stored profiles, or they may have a
different number.  The \f(CWAT&V\fP command shows the current configuration
(``active profile'') and the two stored profiles.  The first line reflects the
parameters set with \f(CWAT\fP commands (for example, \f(CWL0\fP means that the
command \f(CWATL0\fP, turn off the speaker, has been issued).  The next two or
three lines reflect the values of the S registers.  In addition, this modem can
store up to four telephone numbers, a feature that is seldom of great interest.
.P
If you look at this profile, you'll notice that the active profile includes the
parameter \f(CW&K4\fP.  This means ``use XON/XOFF flow control.''  This is not
desirable: it's better to use RTS/CTS flow control.  To fix it,
.Dx
\f(CBat&k3\fP                                     \fIset RTS/CTS flow control\fP\/
OK
\f(CBat&w\fP                                      \fIwrite the active profile\fP\/
OK
\f(CBat&v\fP                                      \fIand check\fP\/
ACTIVE PROFILE:
B0 E1 L0 M1 N1 Q0 T V1 W0 X4 Y0 &C1 &D2 &G0 &J0 &K3 &Q5 &R1 &S0 &T5 &X0 &Y0
S00:000 S01:000 S02:043 S03:013 S04:010 S05:008 S06:002 S07:060 S08:002 S09:006
S10:014 S11:090 S12:050 S18:000 S25:005 S26:001 S36:007 S37:000 S38:020 S44:020
S46:138 S48:007 S95:000

STORED PROFILE 0:
B0 E1 L0 M1 N1 Q0 T V1 W0 X4 Y0 &C1 &D2 &G0 &J0 &K3 &Q5 &R1 &S0 &T5 &X0
S00:000 S02:043 S06:002 S07:060 S08:002 S09:006 S10:014 S11:090 S12:050 S18:000
S36:007 S37:000 S40:104 S41:195 S46:138 S95:000

STORED PROFILE 1:
B0 E1 L1 M1 N1 Q0 T V1 W0 X4 Y0 &C1 &D2 &G0 &J0 &K3 &Q5 &R1 &S0 &T5 &X0
S00:000 S02:043 S06:002 S07:060 S08:002 S09:006 S10:014 S11:090 S12:050 S18:000
S36:007 S37:000 S40:104 S41:195 S46:138 S95:000

TELEPHONE NUMBERS:
0=T1234567890                          1=
2=                                     3=

OK
.De
The active profile includes the parameter \f(CB&Y0\fP, so the \f(CWAT&W\fP
command writes back to stored profile 0.
.P
.X "call failure reason code, modem"
.X "modem, call failure reason code"
The \f(CBAT&V\fP command doesn't show all the S registers.  Some of them relate
to the current state of the modem, and aren't part of the configuration.  For
example, my modem includes an S register \f(CWS86\fP, the \fICall Failure Reason
Code\fP.  If a call fails, it could be interesting to look at it.  To do so:
.Dx
\f(CBats86?\fP                                    \fIshow contents of S86\fP\/
012                                       \fIConnection dropped by other end\fP\/
.De
With this background, we can now proceed to establish a connection:
.Dx
\f(CBatd1234567\fP                                \fIjust dial\fP\/
CONNECT 57600
ppp ON freebie>
PPP ON freebie>
.De
.sp -1v
.H3 "Dialing in"
.Pn dialin
.X "DCD, RS-232 signal"
.X "RS-232 signal, DCD"
Traditionally, UNIX distinguishes between local serial terminals and terminals
connected by modem by whether they assert the \fIDCD\fP\/ signal.  It starts a
.Command getty
(for \fIGet TTY\fP\/) process for each line.
.Command getty
opens the line, but for modems the line state is set in such a way that the call
to \f(CWopen\fP does not complete until the DCE asserts \fIDCD\fP.  This is done
by resetting the flag \f(CWclocal\fP.  If you look at the line state with the
.Command stty
program, it will show \f(CW-clocal\fP if the flag is reset.
.P
To set up a line for dialing in, add information about the line in the file
.File /etc/ttys .
The default file contains a number of lines like:
.Dx
ttyd0   "/usr/libexec/getty std.9600"   unknown off secure
.De
This information has the following meaning:
.Ls B
.LI
The first column is the name of the terminal special file, relative to
.Directory /dev .
In other words, this entry represents the file
.Device ttyd0 .
.LI
The next field consists of the text \f(CW/usr/libexec/getty std.9600\fP.  This
is the invocation for
.Command getty \/:
the
.Command getty
program is
.Command -n /usr/libexec/getty ,
and it is invoked with the parameter \f(CWstd.9600\fP.  This is a label in the
file
.File /etc/gettytab ,
and describes a standard 9600 bps connection.  You'll probably want to upgrade
to \f(CWstd.57600\fP.
.LI
\f(CWunknown\fP refers to the terminal type.  This is the value to which
.Command getty
sets the environment variable \f(CWTERM\fP.  If you know that only people with
VT100 terminals dial in, you might change this string to \f(CWvt100\fP, but you
should do this with care.  It can cause a real mess on the screen, and even make
it impossible to work with it.
.LI
The remaining fields can occur in any order.  \f(CWoff\fP means ``don't start
the
.Command getty
after all.''  If you want to run a
.Command getty
on this line, change this string to \f(CWon\fP.
.P
\f(CWsecure\fP means that only people you trust can access this line, so you can
allow a \f(CWroot\fP login on this line.  That's fine for a direct connect
terminal in the same room, for example.  It's not a good idea for a modem to
which anybody can dial up.  If the line is not secure, just omit the string.
.Le
.X "signal, SIGHUP"
After changing
.File /etc/ttys ,
send
.Daemon init
(process 1) a HUP signal to tell it to re-read
.File /etc/ttys \/:
.Dx
# \f(CBkill -1 1\fP
.De
