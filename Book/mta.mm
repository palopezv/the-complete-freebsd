.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: mta.mm,v 4.17 2003/08/24 02:07:59 grog Exp grog $
.\"
.Chapter \*[nchmta] "Electronic mail: servers"
.Pn MTA
.X "MTA"
.X "Mail Transfer Agent"
.X "bat book"
.X "relay, mail"
.X "mail relay"
In the previous chapter, we looked at email from a user perspective.  The other
part of a mail system is the \fIMail Transfer Agent\fP, or \fIMTA\fP.  As the
name suggests, MTAs perform the transfer of mail from one system to another.
Specifically, they perform three related tasks:
.Ls B
.LI
They send outgoing mail, in other words mail that originates from their system.
If the destination system is not available, they look for an alternative system,
and if none is available, they retry delivery at a later date.  Typically an MTA
will retry every 30 minutes for five days before giving up.
.LI
They receive incoming mail, possibly for multiple domain names.  They may be
quite picky about how they perform this task: since the advent of spam, a number
of techniques have developed.  We'll look at some in the section on
.Daemon postfix
configuration.
.LI
They \fIrelay\fP\/ mail.  Consider the case where a sending MTA can't reach the
destination MTA and chooses an alternative.  The alternative MTA is called a
\fIrelay\fP, and it must be prepared to deliver the mail to the final recipient
at a later time.  Until a few years ago, MTAs performed relaying by default, but
the advent of spam has changed that radically.
.Le
Mail has been around for a long time now, well over 25 years.  In that time,
many mail systems have come and gone.  One seems to have been around for ever:
the
.Daemon sendmail
MTA.
.Daemon sendmail
has an unparalleled reputation.  On the one hand, it can do just about anything,
but on the other hand, its configuration file is one of the most arcane ever to
be seen.  Still, it's holding well against the competition, and it is still
actively being developed.
.P
The definitive book on
.Daemon sendmail ,
called the ``bat book'' after its cover, was written by Bryan Costales and
others (O'Reilly)\(emsee \*[biblio] for more details.  It is over 1000 pages
long.  Obviously this book can't compete with it.
.P
The good news about
.Daemon sendmail
is: it works.  It is possible to install
.Daemon sendmail
and run it with no configuration whatsoever.  The less good news is that in the
past few years it has been constantly changing, and any information I write here
is liable to be out of date by the time you read it.  As a result, I recommend:
.Highlight
If \fBsendmail\fP\/ works for you, use it.  If you have difficulties, use
\fBpostfix\fP\/ instead.
.End-highlight
The following sections show how to configure a mail system using
.Daemon postfix .
In general,
.Daemon sendmail
is quite similar.  You'll find every detail in the bat book, and the original
.Daemon sendmail
distribution, available from
.Href sendmail.org http://www.sendmail.org/ ,
contains instructions for experts.
.H2 "How mail gets delivered"
Ideally, to send mail, the source MTA contact the destination MTA and sends the
message.  In practice, this doesn't always work.  Here's the general method:
.Ls B
.LI
Each time an MTA receives a message not addressed to its system, this MTA
collects all MX records for the destination that are not higher than its own MX
record.
.LI
If the MTA finds any MX records, it attempts to send to one of them, starting at
the lowest preference.
.LI
If the lowest MX record points to the MTA's own machine, then there's a mail
misconfiguration: the MTA doesn't know what to do with it locally, but the MX is
telling it to deliver it locally.  When this happens, the MTA reject the message
(``mail loops back to myself'').
.LI
If there are no MX records at all (which implies that the MTA doesn't have one
either), most, but not all versions of sendmail will look up an A record for the
system name.  If they find one, they will try to deliver there, and only there.
.LI
If all else fails, the MTA rejects the message (``can't find the destination'').
.Le
.H3 "MTA files"
MTAs use three different kinds of files:
.Ls B
.LI
Configuration files tell the MTA what to do.  Typical configuration issues
include what name to present to the outside world, and when to accept mail for
delivery and when to reject it.  The issue of spam (unsolicited commercial
email) makes this quite a complicated issue.
.Daemon postfix
keeps its configuration files in the directory
.Directory /usr/local/etc/postfix ,
and
.Daemon sendmail
keeps them in
.Directory /etc/mail .
.LI
Outgoing
.Daemon postfix
mail is stored in the directory hierarchy
.Directory /var/spool/postfix ,
while
.Daemon sendmail
currently stores its mail in the hierarchies
.Directory /var/spool/mqueue
and
.Directory /var/spool/clientmqueue .
.LI
Incoming mail is stored in the directory
.Directory /var/mail .
Normally each user gets a file that corresponds to his user name.
.Le
.SPUP
.H3 "Who gets the mail?"
.Pn email-MX-record
.X "masquerading, MTA"
.X "MTA, masquerading"
According to RFC 2822, a mail ID is something like \f(CWgrog@example.org\fP.
This looks very much like a user ID, the \f(CW@\fP sign, and the name of a
machine.  This similarity is intended, but it's still only a similarity.
Consider the system manager of \fIexample.org\fP.  At different times he might
send mail from \fIfreebie.example.org\fP, \fIbumble.example.org\fP, and
\fIwait.example.org\fP.  If the mail ID were associated with the machine, he
would have three different mail IDs: \f(CWfred@freebie.example.org\fP,
\f(CWfred@bumble.example.org\fP and \f(CWfred@wait.example.org\fP.  It would
make things a whole lot simpler (and easier to type) if his mail ID were simply
\f(CWfred@example.org\fP.  This name change is called \fImasquerading\fP.
.P
.X "mail exchanger"
One way to do this would be to associate the name \f(CWexample.org\fP as a
\f(CWCNAME\fP with one of the machines\(emsay \fIwait.example.org\fP.  This
would work, but it would mean that mail would always have to come from and go to
\fIwait.example.org\fP.  If for any reason that machine were inaccessible, the
mail would not get through.  In practice, it's possible to run MTAs on more than
one machine.  DNS solves this problem with a special class of record, the
\f(CWMX\fP record (\fImail exchanger\fP\/).  MX records can point to more than
one machine, so if one machine is not accessible, the mail can be sent to
another.  We saw how to add them on page \*[DNS-MX-record].  MX records are not
directly associated with any particular machine, though they point to the names
of machines that run an MTA.
.H2 "Postfix"
.Daemon postfix
is in the Ports Collection, not the base system, so before you can use it, you
must install it.  It is an \fIinteractive\fP\/ port: at various points in the
installation process it asks for input.  The first is a menu offering optional
additional configurations, as shown in Figure
.Sref \*[postfix-build-config] .
For the configuration in this book, you don't need anything in addition to what
the menu suggests; just select \f(CWOK\fP and continue.
.P
Some time later you get the informational messages:
.P
.Dx
Added group "postfix".
Added group "maildrop".
Added user "postfix".
You need user "postfix" added to group "mail".
Would you like me to add it [y]? \f(CBEnter\fP \fIpressed\fP\/
Done.
.De
.PIC images/postfix-config.ps 4.6i
.Figure-heading "Postfix configuration menu"
.Fn postfix-build-config
The build continues for a while, and finally you get the information:
.Dx
Installed HTML documentation in /usr/local/share/doc/postfix
===>   Generating temporary packing list
Would you like to activate Postfix in /etc/mail/mailer.conf [n]? \f(CBy\fP
.De
The output goes on to explain which flags to set in your system configuration
file
.File /etc/rc.conf .
In particular, it tells you to set \f(CWsendmail_enable\fP and finishes by
saying ``This will disable Sendmail completely.''  This may look strange,
especially if you don't have any \fIsendmail\fP-related entries in
.File /etc/rc.conf .
Why should setting \f(CWsendmail_enable\fP to \f(CWYES\fP disable
.Daemon sendmail \/?
Well, it's a somewhat unfortunate choice of naming, and it's possible it will
change, but the answer is in the details: \f(CWsendmail_enable\fP should really
be called something like \f(CWmail_enable\fP.  The other
.Daemon sendmail
parameters turn off all
.Daemon sendmail -related
components.
.H3 "Configuring postfix"
.Daemon postfix
requires only one configuration file,
.File /usr/local/etc/postfix/main.cf .
This file contains a large number of comments: with a little experience you can
configure it without any outside help.  In this section, we'll look at some of
the entries of interest.
.P
.Dx
# The mail_owner parameter specifies the owner of the Postfix queue
# and of most Postfix daemon processes.  Specify the name of a user
# account THAT DOES NOT SHARE ITS USER OR GROUP ID WITH OTHER ACCOUNTS
# AND THAT OWNS NO OTHER FILES OR PROCESSES ON THE SYSTEM.  In
# particular, don't specify nobody or daemon. PLEASE USE A DEDICATED USER.
#
mail_owner = postfix
.De
.ne 3v
Older MTAs used to run as \f(CWroot\fP, which made it easier to write exploits
transmitted by mail.  Modern MTAs use a dedicated user ID.  As we saw above, the
\f(CWpostfix\fP user gets added to your password files when you install it.
.Daemon sendmail
uses another user ID, \f(CWsmmsp\fP.  Don't change this entry.
.H3 "Host and domain names"
A significant portion of the configuration file defines host names.  By default,
the variable \f(CWmyhostname\fP is the fully qualified host name of the system,
for example \f(CWfreebie.example.org\fP.  You should normally leave it like
that; it's not identical to the name that will appear on outgoing mail.
.P
The next variable is \f(CWmydomain\fP, which defaults to the domain name of the
system.  Again, you won't normally want to change it.
.P
Then comes the variable \f(CWmyorigin\fP, which is the name that appears on
outgoing mail.  It defaults to \f(CWmyhostname\fP, which is probably not a good
choice.  As suggested above, a better name would be the domain name,
\f(CWmydomain\fP.  Make the following changes to
.File main.cf \/:
.Dx
# The myorigin parameter specifies the domain that locally-posted
# mail appears to come from. The default is to append $myhostname,
# which is fine for small sites.  If you run a domain with multiple
# machines, you should (1) change this to $mydomain and (2) set up
# a domain-wide alias database that aliases each user to
# user@that.users.mailhost.
#
#myorigin = $myhostname
\f(CBmyorigin = $mydomain\fP
.De
In the original configuration file, the last line is present, but it is
``commented out'': it starts with the \f(CW#\fP character.  Just remove this
character.
.P
.X "daemon, postfix"
.X "postfix, daemon"
The next variable of interest is \f(CWmydestination\fP.  This is a list of host
and domain names for which the MTA considers itself the final destination (in
other words, it accepts mail for final delivery).  By default, it accepts mail
addressed to the name of the machine (\f(CW$myhostname\fP in postfix parlance)
and also \f(CWlocalhost.$mydomain\fP, the local host name for this domain.
In particular, it does \fInot\fP\/ accept mail addressed to the domain, so if
you send mail as \f(CWfred@example.org\fP, any reply will bounce.  To fix this,
add \f(CW$mydomain\fP to the list.
.P
You might also want to accept mail for other domains.  For example, if you also
wanted to accept mail for \fIbeispiel.org\fP, you would add that name here as
well.  The result might look like this:
.Dx
#mydestination = $myhostname, localhost.$mydomain
#mydestination = $myhostname, localhost.$mydomain $mydomain
mydestination = $myhostname, localhost.$mydomain, $mydomain,
        beispiel.org
.De
For the mail for \fIbeispiel.org\fP\/ to actually be delivered to this machine,
the lowest priority MX record for \fIbeispiel.org\fP\/ must point to this host.
.P
.ne 3v
Further down, we'll see a feature called \fIvirtual hosting\fP.  This is a way
to allocate email addresses to people without a UNIX account on this machine.
It works at the user level, not the domain name level.
.H3 "Relaying mail"
One of the favourite tricks of spammers is to send their mail via another system
to give it the aura of respectability.  This is doubly annoying for the
``other'' system: first, it gives it the reputation of being a spammer, and
secondly it often incurs expense, either for data charges or simply from
congestion.
.Daemon postfix
has a number of tricks to help.  The first specifies which networks to trust:
.Daemon postfix
will relay mail coming from these networks.  You could consider this to be
``outgoing'' mail, though the methods
.Daemon postfix
uses don't make this assumption.  By default,
.Daemon postfix
trusts your network and the localhost network 127.0.0.0/8, in other words with a
net mask 255.0.0.0.  But how does it know the net mask for your network?  There
are two possibilities: you tell it, or it guesses.
.P
.Daemon postfix
is pretty simplistic when it comes to guessing.  It takes the default net mask
for the address class, so if your IP address is, say, 61.109.235.17 (a ``class
A'' network), it will accept mail from any network whose first octet is 61.  I
know of at least 20 sources of spam in that range.  In almost every case, you
should specify the network and mask explicitly:
.Dx
mynetworks = 223.147.37.0/24, 127.0.0.0/8
.De
This is a good choice where you know the name of the originating networks, for
example systems that expect you to handle the mail connection to the outside
world.  But what if you want to accept mail from anywhere addressed to specific
domains?  Consider this ``incoming'' mail, though again that's not the way
.Daemon postfix
looks at it.  For example, maybe you're a backup MX for \fIbeispiel.de\fP, so
you want to accept any mail sent to that domain.  In that case, you want to
relay mail to this domain no matter where it comes from.  For this case, use the
\f(CWrelay_domains\fP variable, a list of domain names for which
.Daemon postfix
will always relay.  You might put this in your
.File main.cf \/:
.Dx
relay_domains = $mydestination, $mydomain, beispiel.de
.De
You can also use the \f(CWpermit_mx_backup\fP variable to accept mail for any
domain that lists you as a secondary MX.  This is very dangerous: you don't
have any control over who lists you as a secondary MX, so any spammer could take
advantage of this setting and use you for a relay.
.H3 "Aliases revisited"
.Pn /etc/aliases
On page
.Sref \*[aliases] \&
we looked at how to set up individual aliases for use with
.Command mutt .
.Daemon postfix
and
.Daemon sendmail
also have an alias facility, this time at the system level.  The system installs
a file called
.File /etc/mail/aliases .
It's there by default, so there's no particular reason to move it.  The default
.File /etc/mail/aliases
looks like:
.Dx
# Basic system aliases -- these MUST be present
MAILER-DAEMON: postmaster
postmaster: root

# General redirections for pseudo accounts
bin:    root
daemon: root
games:  root
ingres: root
nobody: root
system: root
toor:   root
uucp:   root

# Well-known aliases -- these should be filled in!
# root:
# manager:
# dumper:
# operator:

root:   grog
.De
Each line contains the name of an alias, followed by the name of the user who
should receive it.  In this case, mail addressed to the users \f(CWbin\fP,
\f(CWdaemon\fP, \f(CWgames\fP, \f(CWingres\fP, \f(CWnobody\fP, \f(CWsystem\fP,
\f(CWtoor\fP and \f(CWuucp\fP will be sent to \f(CWroot\fP instead.  Note that
the last line redefines \f(CWroot\fP to send all mail to a specific user.
.P
You must run the
.Command newaliases
program after changing
.File /etc/aliases
to rebuild the aliases database.  Don't confuse this with the
.Command newalias
program, which is part of the
.Command elm
MUA.
.P
A couple of other uses of aliases are:
.Ls B
.LI
You can also use an alias file for spam protection.  If you want to subscribe to
a mailing list, but you are concerned that spammers might get hold of the
contents of the mailing list, you could subscribe as an alias and add something
like:
.Dx
frednospamplease:  fred
.De
If you do get spam to that name, you just remove the alias (and remember never
to have any dealings with the operator of the mailing list again).
.LI
Another use of aliases is for
.Command majordomo ,
the mailing list manager we'll look at on page
.Sref \*[majordomo] .
.Le
By default,
.Daemon postfix
doesn't have a specific alias file.
.File main.cf
contains:
.Dx
#alias_maps = dbm:/etc/aliases
#alias_maps = hash:/etc/aliases
#alias_maps = hash:/etc/aliases, nis:mail.aliases
#alias_maps = netinfo:/aliases
.De
The texts \f(CWdbm\fP, \f(CWhash\fP and \f(CWnetinfo\fP describe the kind of
lookup to perform.  For
.Daemon sendmail
compatibility, we want \f(CWhash\fP.  Assuming you also want to run
.Command majordomo ,
add the line:
.Dx 1
alias_maps = hash:/etc/mail/aliases,hash:/usr/local/majordomo/aliases.majordomo
.De
.SPUP
.H2 "Rejecting spam"
.X "spam"
One of the biggest problems with email today is the phenomenon of \fIspam\fP,
unsolicited email.  Currently the law and ISPs are powerless against it.
Hopefully the community will find solutions to the problem in the future, but at
the moment keeping spam to manageable proportions is a battle of wits.  There
are a number of ways to combat it, of course:
.Ls B
.LI
Reject mail from domains known to be spammers.
.Daemon postfix
helps here with a file called
.File /usr/local/etc/postfix/access ,
which contains names of domains to reject.
.P
There are a couple of problems with this approach:
.Ls B
.LI
It's relatively easy to register a domain, so you may find the same spam coming
from a different location.
.LI
It's relatively easy to \fIspoof\fP\/ a domain name.  Mail is regularly relayed,
so you have to go by the name on the \f(CWFrom\fP line.  But you can forge that,
so you often see mail from \f(CWyahoo.com\fP or \f(CWhotmail.com\fP that has
never been near those ISPs.  Obviously it doesn't help to complain to the ISP.
.Le
.LI
Of course, if the names are spoofed, you can still find out where the message
really came from from the headers, as we saw on page
.Sref \*[complete-headers-page] .
Or can we?  There are two issues there: firstly, if the message has gone by
another system, a \fIrelay\fP, you can't rely on the headers further back than
one hop.  Anything beyond that can be forged.
.P
.X "open relay"
In the olden days, MTAs would accept mail for relaying from any system: they
were so-called \fIopen relays\fP.  Spammers have found this very useful, and now
most systems have restricted relaying to specific trusted systems.  There are
still a large number of open relays on the net, though.
.P
This is a problem that could theoretically happen to you: if your system is an
open relay, you could end up delivering spam without even knowing it.  By
default, all current MTAs supplied with FreeBSD refuse to relay, but it's
possible to (mis)configure them to be open relays.  Be aware of the problems.
.P
But what if you get a message like this?
.Dx
Received: from femail.sdc.sfba.home.com (femail.sdc.sfba.home.com [24.0.95.82])
        by wantadilla.lemis.com (Postfix) with ESMTP id BCBFF6ACC0
        for <webmaster@lemis.com>; Tue, 19 Jun 2001 13:50:57 +0930 (CST)
Received: from u319 ([24.21.217.142]) by femail2.sdc1.sfba.home.com
          (InterMail vM.4.01.03.20 201-229-121-120-20010223) with SMTP
          id <20010619042005.FBWM26828.femail2.sdc1.sfba.home.com@u319>;
          Mon, 18 Jun 2001 21:20:05 -0700
From: britneyvideo1234@yahoo.com
To:
Subject: stolen britney spears home video!!!
Date: Thu, 19 Jun 2025 13:52:44 -0200
.De
This message \fIhas\fP\/ come from the domain \fIhome.com\fP, though it's
claiming to come from \fIyahoo.com\fP, but the IP address of the originating MTA
does not resolve to a name.  The format of the \f(CWReceived:\fP headers is:
.Dx 1
\fIannounced-name\fP\/ (\fIreal-name\fP\/ [\fIreal-IP\fP\/])
.De
The first header is correct: the name it claims to be
(\fIfemail.sdc.sfba.home.com\fP\/) matches the reverse lookup.  In the second
case though, \fIu319\fP\/ is not a valid fully-qualified domain name, and there
is no second name: the reverse lookup failed.  Some MTAs use the word
\fIunknown\fP\/ in this case, and some even add a warning.
.P
Why should the IP of an MTA not resolve?  It's ideal for spammers, of course: it
makes them almost impossible to trace.  In this case, it's probable that the IP
range belongs to \fIhome.com\fP, because they accepted the message for relaying,
but the lack of a valid reverse lookup says nothing for their professionalism.
.LI
A number of commercial and public service sites maintain a list of known spam
sites.  You can use them to decide whether to accept a mail message.
.LI
The previous example shows another obvious point: this message has been forged
to appear to come from \f(CWyahoo.com\fP.  All messages that really come from
Yahoo! have a header of this nature:
.Dx
Received: from web11207.mail.yahoo.com (web11207.mail.yahoo.com [216.136.131.189])
        by mx1.FreeBSD.org (Postfix) with SMTP id 4079E43E65
        for <freebsd-arch@freebsd.org>; Mon,  7 Oct 2002 10:39:14 -0700 (PDT)
        (envelope-from gathorpe79@yahoo.com)
.De
So if you can recognize messages claiming to come from \f(CWyahoo.com\fP, but
without this kind of header, there's a good chance that they're spam.
.Le
So how do we use this information to combat spam?
.Daemon postfix
helps for the first three, but we need other tools for the last.
.P
The rules for blocking unwanted messages are not included in
.File /usr/local/etc/postfix/main.cf .
Instead, they're in
.File /usr/local/etc/postfix/sample-smtpd.cf .
Copy those you want to the bottom of your
.File /usr/local/etc/postfix/main.cf .
Specifically, the variables of interest are \f(CWsmtpd_helo_restrictions\fP
(which relates to the sending MTA, which could be a relay), and
\f(CWsmtpd_sender_restrictions\fP, which relates to the (claimed) original
sender.  See
.File sample-xmtpd.cf
for details of all possible restrictions.  The more interesting ones are:
.Ls B
.LI
\f(CWreject_unknown_client\fP: reject the request if the client hostname is
unknown, i.e. if the DNS reverse lookup fails.
.LI
\f(CWreject_maps_rbl\fP: reject if the client is listed under
\f(CW$maps_rbl_domains\fP.  We'll discuss this below.
.LI
\f(CWreject_invalid_hostname\fP: reject hostname with bad syntax.
.LI
\f(CWreject_unknown_hostname\fP: reject hostname without DNS A or MX record.
.LI
\f(CWreject_unknown_sender_domain\fP: reject sender domain without A or MX
record.  This is probably a forged domain name.
.LI
\f(CWcheck_sender_access maptype:mapname\fP.  Look the sender address up in the
specified map and decide whether to reject it.  We'll look at this in more
detail below.
.LI
\f(CWreject_non_fqdn_hostname\fP: reject HELO hostname that is not in FQDN form.
.LI
\f(CWreject_non_fqdn_sender\fP: reject sender address that is not in FQDN form.
.Le
.H3 "Rejecting known spam domains"
If you have identified domains that you would rather not hear from again, use
the form \f(CWcheck_sender_access maptype:mapname\fP.  By default, the map is
stored in
.File /usr/local/etc/postfix/access.db .
Add the following text to
.File main.cf \/:
.Dx
smtpd_sender_restrictions = hash:/usr/local/etc/postfix/access
.De
Note that the \f(CW.db\fP is missing from the name.  Now add this line to the
file
.File /usr/local/etc/postfix/access ,
creating it if necessary:
.Dx
spamdomain.com                  550      Mail rejected.  Known spam site.
.De
This form rejects messages from this domain with SMTP error code 550 and the
message that follows.
.P
As we have seen,
.Daemon postfix
reads the file
.File /usr/local/etc/postfix/access.db ,
not
.File /usr/local/etc/postfix/access .
Use the
.Command postmap
program to create or update
.File /usr/local/etc/postfix/access.db \/:
.Dx
# \f(CBpostmap /usr/local/etc/postfix/access\fP
.De
The changes to
.File /usr/local/etc/postfix/main.cf
depend on other items as well, so we'll look at them at the end of this
discussion.
.P
To judge by the name, \fIspamdomain.com\fP\/ is probably a hard-core spam
producer.  But there are others, notably large ISPs with little or no interest
in limiting spam, and they also have innocent users who will also be blocked.
If you find out about it, you can make exceptions:
.Dx
spamdomain.com          550      Mail rejected.  Known spam site.
innocent@spamdomain.com OK
.De
Don't forget to re-run
.Command postmap
after updating
.X "alias file, email"
.X "email, alias file"
.File -n alias .
One way is to create a
.File -n Makefile
in
.Directory /usr/local/etc/postfix
with the following contents:
.Dx
access.db:      access
        /usr/local/sbin/postmap access
.De
Then add the following line to
.File /etc/crontab \/:
.Dx 1
1  *  *  *  *  root    (cd /usr/local/etc/postfix; make) 2>/dev/null >/dev/null
.De
This checks the files every hour and rebuilds
.File /usr/local/etc/postfix/access.db
if necessary.
.H3 "Rejecting sites without reverse lookup"
.Pn reject-no-reverse
A very large number of spam sites don't have reverse lookup on their IP
addresses.  You can reject all such mail: after all, it's misconfigured.  Just
add the \f(CWreject_unknown_sender_domain\fP keyword to the
\f(CWsmtpd_sender_restrictions\fP.  Unfortunately, some serious commercial
enterprises also don't have reverse lookup.  It's your choice whether you want
to accept mail from them and open the flood gates to spam, or to ignore them.
The FreeBSD project has chosen the latter course: if you don't have reverse
lookup, you will not be able to send mail to FreeBSD.org.
.H3 "Rejecting listed sites"
.X "Realtime Blackhole List"
.X "RBL"
Another alternative is to reject sites that have been listed on a public list
of spam sites, sometimes referred to as an \fIrbl\fP\/ (\fIRealtime Blackhole
List\fP\/).  The example given in the configuration file is
.Href MAPS http://www.mail-abuse.org/ ,
but there are others as well.  They maintain a list of spam sites that you can
query before accepting every message.
.P
I don't like these sites for a number of reasons:
.Ls B
.LI
They slow things down.
.LI
They frequently cost money.
.LI
They have a habit of blocking large quantities of address space, including
domains who are not in any way related with the spammers.  I don't know anything
about MAPS, so I can't comment on whether they do this sort of thing.
.Le
If you want to use this kind of service, add the following two lines to your
.File main.cf \/:
.Dx 2
smtpd_client_restrictions = reject_maps_rbl
maps_rbl_domains = rbl.maps.vix.com
.De
The name \fIrbl.maps.vix.com\fP\/ comes from the sample file.  Replace it with
information from your rbl supplier.
.H3 "Recognizing spoofed messages"
There's only so much that
.Daemon postfix
can do to restrict spam.  The Ports Collection contains a couple of other useful
tools,
.Command procmail
and
.Command spamassassin ,
which together can reject a lot of spam.  It involves a fair amount of work,
unfortunately.  Take a look at the ports if you're interested.
.H3 "Sender restrictions: summary"
The restrictions above are interdependent.  I would recommend rejecting senders
based on address and lack of reverse lookup.  To do that, add just the following
lines to your
.File main.cf \/:
.Dx 2
smtpd_sender_restrictions = reject_unknown_sender_domain,
        hash:/usr/local/etc/postfix/access
.De
.SPUP
.H2 "Running postfix at boot time"
.Pn sendmail-enable
By default, the system starts
.Daemon sendmail
at boot time.  You don't need to do anything special.  Just set the following
parameters in
.File /etc/rc.conf \/:
.Dx
sendmail_enable="YES"
sendmail_flags="-bd"
sendmail_outbound_enable="NO"
sendmail_submit_enable="NO"
sendmail_msp_queue_enable="NO"
.De
The flags have the following meanings:
.Ls B
.LI
\f(CWsendmail_enable\fP is a bit of a misnomer.  It should be called
\f(CWmail_enable\fP.
.LI
\f(CW-bd\fP means \fIbecome daemon\fP\/:
.Daemon postfix
runs as a daemon and accepts incoming mail.
.P
.Daemon sendmail
uses an additional parameter, usually something like \f(CW-q30m\fP.  This tells
.Daemon sendmail
how often to retry sending mail (30 minutes in this example).
.Daemon postfix
accepts this option but ignores it.  Instead, you tell it how often to retry
mail (``run the queue'') with the \f(CWqueue_run_delay\fP parameter in the
configuration file, which is set to 1000 seconds, about 16 minutes.  A retry
attempt takes up local and network resources, so don't set this value less than
about 15 minutes.
.LI
The other parameters are only there to stop the system from running
.Daemon sendmail
as well.
.Le
.SPUP
.H3 "Talking to the MTA"
.Pn telnet-to-smtp
.X "Simple Mail Transfer Protocol"
.X "SMTP"
The \fISimple Mail Transfer Protocol\fP, or \fISMTP\fP, is a text-based
protocol.  If you want, you can talk to the MTA directly on the \f(CWsmtp\fP
port.  Try this with
.Command telnet \/:
.Dx
$ \f(CBtelnet localhost smtp\fP
Trying ::1...
telnet: connect to address ::1: Connection refused      \fIattempt to connect with IPv6\fP\/
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
220 freebie.example.org ESMTP Postfix on FreeBSD, the professional's choice
\f(CBehlo freebie.example.org\fP                                \fIsay who you are\fP\/
250-freebie.example.org                                 \fIname\fP\/
250-PIPELINING                                          \fIand list of available features\fP\/
250-SIZE 10240000
250-ETRN
250 8BITMIME
\f(CBmail from: grog@example.org\fP                             \fIwho the mail is from\fP\/
250 Ok
rcpt to: grog@example.org                               \fIand who it goes to\fP\/
250 Ok
data                                                    \fIstart the message body\fP\/
354 End data with <CR><LF>.<CR><LF>
\f(CBTest data\fP                                               \fIThe message\fP\/
\&.                                                       \fIEnd of message\fP\/
250 Ok: queued as 684F081471
\f(CBquit\fP                                                    \fIand exit\fP\/
221 Bye
Connection closed by foreign host.
.De
This rather cumbersome method is useful if you're having trouble with
.Daemon postfix .
.H2 "Downloading mail from your ISP"
.X "downloading mail"
.X "mail, downloading"
As we discussed before, the Internet wasn't designed for dialup use.  Most
protocols assume that systems are up a large proportion of the time: down time
indicates some kind of failure.  This can cause problems delivering mail if you
are not permanently connected to the Internet.
.P
If you have an MX record that points to another system that is permanently
connected, this doesn't seem to be a problem: the mail will be sent to that
system instead.  When you connect, the mail can be sent to you.
.P
How does the mail system know when you connect?  Normally it doesn't.  That's
the first problem.  Most systems set up their MTA to try to deliver mail every
30 to 120 minutes.  If you are connected that long, the chances are good that
the mail will be delivered automatically, but you don't know when.
.P
One possibility here is to tell the remote MTA when you're connected.  You can
do this with the SMTP \f(CWETRN\fP command.  Telnet to the \f(CWsmtp\fP port on
the system where the mail is queued:
.Dx
$ \f(CBtelnet mail.example.net  smtp\fP
Trying 139.130.237.17...
Connected to mail.example.net.
Escape character is '^]'.
220 freebie.example.org ESMTP Sendmail 8.8.7/8.8.7 ready at Mon, 5 May 1997
12:55:10 +0930 (CST)

\f(CBetrn freebie.example.org\fP
250 Queuing for node freebie.example.org started
\f(CBquit\fP
221 mail.example.net closing connection
Connection closed by foreign host.
.De
The mail starts coming after the message \f(CWQueuing for node
freebie.example.org started\fP.  Depending on how much mail it is, it might take
a while, but you don't need to wait for it.
.P
.X "Post Office Protocol"
.X "POP"
Another alternative is the \fIPost Office Protocol\fP, or \fIPOP\fP.  POP was
designed originally for Microsoft-style computers that can't run daemons, so
they have to explicitly request the other end to download the data.  POP is an
Internet service, so you need the cooperation of the other system to run it.
We'll look at POP in the next section.
.H3 "POP: the Post Office Protocol"
.Pn POP
The Post Office Protocol is a means for transferring already-delivered mail to
another site.  It consists of two parts, the client and the server.  A number of
both clients and servers are available.  In this discussion, we'll look at the
server
.Daemon popper
and the client
.Command fetchmail ,
both of which are in the Ports Collection.
.H3 "popper: the server"
Install
.Daemon popper
from the Ports Collection in the usual manner:
.Dx
# \f(CBcd /usr/ports/mail/popper\fP
# \f(CBmake install\fP
.De
.Daemon popper
is designed to be started only via
.Daemon inetd .
To enable it, edit
.File /etc/inetd.conf .
By default, it contains the following line:
.Dx
#pop3   stream  tcp     nowait  root    /usr/local/libexec/popper       popper
.De
This line is \fIcommented out\fP\/ with the \f(CW#\fP character.  Remove that
character to enable the service.  Then cause
.Daemon inetd
to re-read its configuration file:
.Dx
# \f(CBkillall -1 inetd\fP                              \fIsend a SIGHUP\fP\/
.De
To test the server, telnet to the \f(CWpop3\fP port.  You can't do much like
this, but at least you can confirm that the server is answering:
.Pn telnet-to-pop
.Dx
$ \f(CBtelnet localhost pop3\fP
Trying ::1...
telnet: connect to address ::1: Connection refused
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
+OK QPOP (version 2.53) at freebie.example.com starting.  <11755.1028797120@freebie.
example.com>
\f(CBquit\fP
+OK Pop server at freebie.example.com signing off.
Connection closed by foreign host.
.De
.SPUP
.H3 "fetchmail: the client"
Install
.Command fetchmail
from the Ports Collection.  To run it, just specify the name of the server from
which you want to load the mail.
.Dx
$ \f(CBfetchmail hub\fP
querying hub
Enter mailserver password:                      \fIdoesn't echo\fP\/
QPOP (version 2.3) at hub.freebsd.org starting.  <27540.876902406@hub.freebsd.org>
5 messages in folder, 4 new messages.
reading message 1...
flushing message 2
reading message 2....
flushing message 3
reading message 3...
flushing message 4
reading message 4...
flushing message 5
reading message 5....
.De
.Command fetchmail
and
.Daemon popper
are relatively simple to use if you have to, but they add another level of
complexity to the mail system, and they require additional work in a system that
is designed to be automatic.  In addition,
.Command fetchmail
is not a speed demon: if you have a lot of mail to transfer, be prepared to wait
much longer than an SMTP MTA would take.
.H2 "Mailing lists: majordomo"
.Pn majordomo
.\"XXX Get this right
.Command majordomo
is a mail list manager.  If you run mailing lists, you probably want to use
majordomo: it saves you manually modifying the mailing lists.  As usual, you can
find
.Command majordomo
in the Ports Collection, in the directory
.Directory /usr/ports/mail/majordomo .
When installing, you'll notice a
message:
.Dx
To finish the installation, 'su' to root and type:

            make install-wrapper

If not installing the wrapper, type

            cd /usr/local/majordomo; ./wrapper config-test

(no 'su' necessary) to verify the installation.
\&./install.sh -o root -g 54  -m 4755 wrapper /usr/local/majordomo/wrapper
.De
With the exception of the last line, this comes from the original
.Command majordomo
installation procedure.  The last line is the port performing the \f(CWmake
install-wrapper\fP for you.  You don't need to do anything else, and you can
ignore the messages.
.P
After installation, you still need to perform some configuration:
.Ls B
.LI
Customize
.File /usr/local/majordomo/majordomo.cf .
This should be easy enough to read, and you may not need to change anything.
Once you have it up and running, you might like to consider changing the
\f(CWdefault_subscribe_policy\fP.
.LI
Define your lists in
.File /usr/local/majordomo/aliases.majordomo .
This file contains a single list, \f(CWtest-l\fP, which you should remove once
you have things up and running.
.LI
Ensure that there is a mail user \f(CWmajordomo-owner\fP on the system.
The best way to handle this is to add an entry in
.File /etc/mail/aliases
(see page \*[/etc/aliases]):
.Dx
majordomo-owner:        root
.De
Since \f(CWroot\fP should be an alias for your mail ID, this will mean that you
get the mail for \f(CWmajordomo-owner\fP as well.  Don't run
.Command postmap
or
.Command newaliases
yet.
.LI
Add
.File /usr/local/majordomo/aliases.majordomo
to the list
.Daemon postfix
aliases.  We looked at this point above; you need at least the bold text part of
the following line in
.File /usr/local/etc/postfix/main.cf \/:
.Dx
\f(CBalias_maps = \fPhash:/etc/mail/aliases,\f(CBhash:/usr/local/majordomo/aliases.majordomo\fP
.De
.SPUP
.LI
Run
.Command postmap .
.LI
Restart
.Daemon postfix :
.Dx
# \f(CBpostfix reload\fP
.De
This isn't absolutely necessary, but it will take
.Daemon postfix
a few minutes to notice otherwise.
.Le
That's all you need to do.  You don't need to start any processes to run
.Command majordomo \/:
it gets started automatically when a mail message is received.
.\" Work around a bug where the reset macro doesn't work if we're exactly at the
.\" bottom of an odd-numbered page.
.sp 3v
\&
.ig
XXX
Talk about configuring fetchmail to run automatically.
Suggested by: Armand Passelac <apasselac@free.fr>
..
