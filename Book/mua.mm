.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: mua.mm,v 4.16 2003/08/24 02:07:18 grog Exp grog $
.\"
.Chapter \*[nchmua] "Electronic mail: clients"
.Pn mail-setup
.X "electronic mail"
.X "Email"
.X "E-mail"
\fIElectronic mail\fP, usually called \fIemail\fP, \fIe-mail\fP\/ or simply
\fImail\fP, is a method of sending messages to other people on the Net.  As with
other network services, there are two parts to mail software:
.Ls B
.LI
.X "Mail User Agent"
.X "MUA"
The user interface to the mail system is the client, the \fIMail User Agent\fP,
or \fIMUA\fP.  It interacts with the user and handles incoming and outgoing
mail.  People frequently use the word \fImailer\fP when referring to MUAs.  In
this chapter we'll look at my favourite MUA,
.Command mutt ,
and we'll briefly touch on what others are available.
.LI
.X "Mail Transfer Agent"
.X "MTA"
The server part is the \fIMail Transfer Agent\fP, or \fIMTA\fP.  As the name
suggests, it is responsible for moving mail from one system to another.  We'll
look at MTAs in the next chapter,
.Sref "\*[chmta]" .
.Le
.sp -1v
.H2 "Mail formats"
Email is defined by a number of Internet standards, the so-called \fIRFC\fP\/s,
or \fIRequests For Comments\fP.  You can browse the RFCs at
\fIhttp://www.faqs.org/rfcs/\fP.  Here are the most important ones.
.Ls B
.LI
.X "Simple Mail Transfer Protocol"
.X "SMTP"
RFC 2821 is a recent update to the venerable RFC 821, which dates from the early
1980s.  It defines the \fISimple Mail Transfer Protocol\fP or \fISMTP\fP.  It
specifies how to send mail round the network.  For most people it's not very
interesting, but it does impose some restrictions such as the basic line length
limit.  Apart from this problem (which Microsoft abuses), most mail systems
adhere to SMTP.
.P
.LI
Similarly, RFC 2822 replaces RFC 822.  It defines the basic format of a mail
message.  It defines the headers (To:, Cc:, Subject: and so on) and a simple
body made up of US-ASCII text, the message itself.  This was fine for when it
was written, but it can't handle the more complex formats used nowdays, such as
images, binary files or embedded messages.  It also can't handle non-US
character sets, which causes problems in particular in countries like Russia,
Israel and Japan.
.P
.LI
.X "MIME"
RFC 2045, RFC 2046, RFC 2047, RFC 2048 and RFC 2049 together describe the
\fIMultipurpose Internet Mail Extensions\fP, better known as \fIMIME\fP.  They
define how to encode non US-ASCII text and attachments so that they can be
represented in ASCII and hence sent by RFC 2822, and also how to divide the
single RFC 2822 body into multiple parts using ASCII separators.
.P
A number of UNIX MUAs have inadequate MIME support.  Find one which does the job
properly.  On the other hand, if your target audience typically does not use
MIME-aware MUAs, avoid sending MIME messages.
.Le
.sp -1v
.H2 "Mail user agents"
.X "Mail User Agent"
.Pn MUA
.X "email, folder"
.X "folder, email"
A \fImail user agent\fP\/ is a program that interfaces between the user and the
mail system.  It allows the user to read, forward and reply to incoming mail,
and to send his own mail.  It usually has facilities for creating and
maintaining \fIfolders\fP, where you can keep received mail messages.  For most
UNIX MUAs, a folder is the same thing as a file, but some MUAs keep mail
messages as individual files, and the folder corresponds to a directory.
.H3 "mail"
The oldest MUA you're likely to meet is
.Command mail .
It's a very basic, character-oriented program, but nevertheless it has its
advantages.  You can use it in scripts to send mail.  For example, if you have a
job running and producing copious output, where you want to save the output, you
might normally write something like:
.Dx 1
$ \f(CBlongjob 2>&1 > logfile\fP
.De
This command runs
.Command -n longjob .
The sequence \f(CW2>&1\fP redirects the error output to the standard output, and
the \f(CW>\fP writes them to the file \fIlogfile\fP.  While this is a good way
to solve the problem, you might find that you have a lot of such jobs, or that
you tend to forget the log files and leave them cluttering up your disks.  An
alternative is to send mail to yourself.  You can do this with the following
command:
.Dx 1
$ \f(CBlongjob 2>&1 | mail \f(BIme\fP\/\fP
.De
.X "cron, daemon"
.X "daemon, cron"
In this case, \f(BIme\fP\/ is your user ID.  When the job finishes, you get a
mail message with the output of the commands.
.Daemon cron
(see page \*[cron]) uses this method to send you its output.
.H3 "Other MUAs"
.Command mail
.X "MIME"
has a number of limitations: it doesn't deal very well with long mail messages,
it's difficult to keep an overview of large quantities of mail, like most people
seem to accumulate, and it can't handle \fIMIME\fP.
.P
Many more sophisticated MUAs have been written since
.Command mail .
Some of the more popular ones, which are also available in the Ports Collection,
are:
.Ls B
.LI
.Command elm
is one of the oldest full-screen MUAs.  Its age is showing: it has a few
annoying problems that make it less desirable now that there's a choice.
.LI
.Command pine
is not
.Command elm \\(emthat's
what the acronym stands for.  It's quite like
.Command elm ,
nonetheless.
.LI
.Command mutt
is also similar to
.Command elm
and
.Command pine .
It's my current favourite, and we'll look at it in the next section.
.LI
.Command exmh
is built on Rand's
.Command mh
MUA.  Some people like it, but it seems relatively easy to configure it to
mutilate messages.
.LI
.Command xfmail
is an X-based MUA, which you might prefer to the text-based MUAs we're talking
about here.
.LI
.Command sylpheed
is a more recent X-based MUA.  You may prefer it to
.Command xfmail .
.Le
.sp -1v
.H2 "Files, folders or directories?"
There are two schools of thought about how to store mail:
.P
.Ls B
.LI
Traditional MUAs represent folders as files.  They store all the messages in a
folder in that single file.  This is sometimes called the \fImbox\fP\/ method.
.Command mail ,
.Command elm
and
.Command pine
do it this way.
.P
.LI
Other MUAs, including
.Command exmh ,
.Command xfmail
and
.Command sylpheed ,
represent a folder as a directory.  Each message in the folder is then a file by
itself.
.LI
.Command mutt
can use either method, but the default is the mbox method.
.Le
Which method should you use?  Both have their advocates.  The directory and file
approach is more robust (if you trash a file, you only lose one message, not all
of them), and it enables you to have the same message in multiple folders.  On
the other hand, it also imposes a lot higher overhead.  Current versions of
\fIufs\fP, at least on FreeBSD, have a default block size of 16 kB and a
fragment size of 2 kB.  That means that all files have a length that is a
multiple of 2 kB, and so the average waste of space is 1 kB.  In addition, each
file uses an inode.  If you have a lot of mail, this can add up to a lot of
wasted space.  For example, I currently have 508,649 saved mail messages, which
take up a total of 2.1 GB, almost exactly 4 kB per message.  If I stored them in
a directory structure, I would lose about another 500 MB of space, or 25%.  The
file system on which the messages are stored is 9.5 GB in size and has 1.2
million inodes; nearly half of them would be used for the mail messages.
.H3 "mutt"
.Pn mutt
In this section, we'll take a detailed look at
.Command mutt .
Start it by typing in its name.  Like most UNIX MUAs,
.Command mutt
runs on a character-oriented terminal, including of course an
.Command xterm .
We'll take a look into my mailbox.  By default, when starting it up you get a
display like the one shown in Figure
.Sref \*[mutt-main-bw] .
.Df
.PIC images/mutt-main-bw.ps 4.6i
.Figure-heading "mutt main menu"
.Fn mutt-main-bw
.DE
.P
.Command mutt
sets reverse video by default.  You can change the way it displays things,
however.  On page
.Sref \*[muttrc] \&
we'll see how to change it to the style shown in one shown in Figure
\*[mutt-main].
.Df
.PIC images/mutt-main.ps 4.6i
.Figure-heading "mutt main menu"
.Fn mutt-main
.DE
.P
.ne 5v
The display of Figure
.Sref \*[mutt-main] \&
shows a number of things:
.Ls B
.LI
The line at the top specifies the name of the file (``folder'') that contains
the mail messages
.File ( /var/mail/grog \/),
.X "threads, email"
.X "email, threads"
the number of messages in the folder, and its size.  It also states the manner
in which the messages are sorted: first by \fIthreads\fP, then by date.  We'll
look at threads further down.
.LI
The bottom line gives a brief summary of the most common commands.  Each command
is a single character.  You don't need to press \fBEnter\fP.
.LI
.X "index, mutt"
.X "mutt, index"
The rest of the screen contains \fIindex lines\fP, information about the
messages in the folder.  The first column gives the message a number, then come
some flags:
.Ls B
.LI
In the first column, we can see \f(CWr\fP next to some messages.  This
indicates that I have already replied to these messages.
.LI
In the same column, \f(CWN\fP signalizes a \fInew\fP\/ message (an unread
message that has arrived after the last invocation of
.Command mutt
finished).
.LI
The symbol \f(CWD\fP means that the message has been marked for deletion.  It
won't be deleted until you leave
.Command mutt
or update the display with the \f(CB$\fP command, and until then you can
undelete it with the \f(CBu\fP command
.LI
The symbol \f(CW+\fP means that the message is addressed to me, and only to me.
We'll see below how
.Command mutt
decides who I am.
.LI
The symbol \f(CWT\fP means that the message is addressed to me and other people.
.LI
The symbol \f(CWC\fP means that the message is addressed to other people, and
that I have been copied.
.LI
The symbol \f(CWF\fP means that the message is from me.
.LI
The symbol \f(CW*\fP means that the message is \fItagged\fP\/: certain
operations work on all tagged messages.  We'll look at that on page
.Sref \*[tags] .
.Le
.LI
The next column is the date (in international notation in this example, but it
can be changed).
.LI
The next column is the name of the sender, or, if I'm the sender, the name of
the recipient.
.LI
The next column is the name of the recipient.  This is often me, of course, but
frequently enough it's the name of a mailing list.  You'll notice that this is a
column I have added; it's not in the default display.
.LI
The next column gives the size of the message.  The format is variable: you can
specify number of lines (as in the example), or the size in kilobytes.
.LI
The last column is usually the subject.  For messages 56 to 61, it's a series of
line drawings.  This is \fIthreading\fP, and it shows a relationship between a
collection of messages on the same topic.  Message 56 was the original message
in the thread, message 57 is a reply to message 56, and so on.  Messages 60 and
61 are both replies to message 59.
.Command mutt
automatically collects all messages in a thread in one place.
.Le
.P
You'll notice in the example that the lines are of different intensity.  In the
original, these are different colours, and they're a matter of personal choice;
they highlight specific kinds of message.  I use different colours to highlight
messages on different topics.  If you're interested in the exact colours, see
\fIhttp://ezine.daemonnews.org/200210/ports.html\fP, which contains an early
version of this text.
.P
The index line for message 52 appears to be in reverse video.  In fact, it's in
white on a blue background, a colour I don't use for anything else.  This is the
\fIcursor\fP, which you can position either with the cursor up and cursor down
keys, or with the \fIvi\fP\/-like commands \f(CBj\fP (move down) or \f(CBk\fP
(move up).  In the default display, it is in normal video (i.e. not reversed, or
doubly reversed).  You can also move between pages with the left and right
cursor commands.  Many commands, such as \f(CBr\fP (reply) or \fBEnter\fP
(read), operate on the message on which the cursor is currently positioned.  For
example, if you press \fBEnter\fP at this point, you'll see a display like that
in Figure
.Sref \*[mutt-read] .
.PIC images/mutt-read.ps 4.6i
.Figure-heading "mutt message display"
.Fn mutt-read
.P
Here, the display has changed to show the contents of the message.  The top line
now tells you the sender of the message, the subject, and how much of the
message is displayed, in this case 50%.  As before, the bottom line tells you
the most common commands you might need in this context: they're not all the
same as in the menu display.
.P
.X "header, email"
.X "email, header"
The message itself is divided into three parts: the first 6 lines are a
selection of the \fIheaders\fP\/.  The headers can be quite long.  They include
information on how the message got here, when it was sent, who sent it, who it
was sent to, and much more.  We'll look at them in more detail on page
.Sref \*[headers] .
.P
.ne 3v
The headers are separated from the message body by an empty line.  The first
part, which
.Command mutt
displays in \fBbold\fP, is \fIquoted\fP\/ text: by putting a \f(CW>\fP character
before each line, the sender has signalized that the text was written by another
person, often the person to whom it is addressed: this message is a reply, and
the text is what he is replying to.  Normally there is an attribution above the
text, but it's missing in this example.  We'll see attributions below in the
section on replying.
.P
.ne 3v
If the message is longer than one screen, press \fBSPACE\fP to page down and
\f(CB-\fP (hyphen) to page up.  In general, a 25 line display is inadequate for
reading mail.  On an X display, choose as high a window as you can.
.H2 "Creating a new message"
To create a new message, press \f(CBm\fP.
.Command mutt
starts your favourite editor for you.  How does it know which one?  If you
specify the name of the editor in your
.File .muttrc
file, or set your \f(CWEDITOR\fP environment variable to the name of your
editor, it starts that editor; otherwise it starts
.Command vi .
On page
.Sref \*[muttrc] \&
we'll look at what to put in
.File .muttrc .
.P
In this case, we start
.Command emacsclient .
.Command emacsclient
isn't really an editor at all: it simply finds an
.Command -n Emacs
process and latches on to it.  This is much faster than starting a new instance
of
.Command -n Emacs \/:
it's practically instantaneous, whereas even on fast modern machines, starting
.Command -n Emacs
causes a brief delay.  To exit the client, you use the key combination \f(CBc-x
c-#\fP.
.P
.PIC images/mutt-new.ps 4.6i
.Figure-heading "Creating a new message: initial state"
Fill out the name of the intended recipient in the appropriate place, and enter
the text of the message below the headers, leaving one line of space.  Apart
from this, most of the actions involved in sending a new mail message are the
same as those in replying to an existing message, so we'll look at both
activities together in the next section.
.H2 "Replying to a message "
To reply to a message, in this case the message shown in Figure
.Sref \*[mutt-read] ,
simply press \f(CBr\fP.  Before entering any text, the
editor screen looks like Figure
.Sref \*[mail-reply] .
.PIC images/mutt-reply.ps 4.6i
.Figure-heading "Replying to a message: initial state"
.Fn mail-reply 
You'll notice that
.Command mutt
automatically ``quotes'' the text.  The original text started with:
.Dx
>I think I now understand the problem here.  Try the following patch
>and tell me if it solves the problem:
>
>--- vinumio.c   2 May 2002 08:43:44 -0000       1.52.2.6
>+++ vinumio.c   19 Sep 2002 05:10:27 -0000
Tried patch.  System no longer reads ad0h/ad2h, but after the second
\&'vinum start', the system shows 0 drives ('vinum ld' lists nothing.)
.De
This message itself starts with quoted text, which indicates that it was written
by somebody else.  There should be a line at the top stating who wrote it, but
it's missing here.  The text from the submitter starts with \f(CWTried patch\fP.
When you reply, however, all this text is quoted again.  The first line
attributes the text below.  You'll notice that this reply also includes a
selection of headers for the \fIoutgoing\fP\/ message.  This can be very
convenient if you want to tailor your headers to the message you're sending, or
just to add other recipients.
.P
This is a reply to a technical question, so I change the \f(CWFrom:\fP header to
my \f(CWFreeBSD.org\fP address and copy the original mailing list.  I also
remove irrelevant text and add a reply, as shown in Figure
.Sref \*[mutt-reply-2] .
It wasn't necessary to reformat the original text, since it was relatively
short.  The quoting method makes lines grow, though, and many MUAs have
difficulty with long lines, so it's a good idea to reformat long paragraphs.
See \fIhttp://www.lemis.com/email.html\fP\/ for more details.
.P
.X "group reply, email"
.X "email, group reply"
.PIC images/mutt-reply-2.ps 4.6i
.Figure-heading "Replying to a message: after editing"
.Fn mutt-reply-2
In this example, I reply with the \f(CBr\fP (reply to sender) command.
I could also do a \fIgroup reply\fP\/ with the \f(CBg\fP key, which would
include all the original recipients, so it wouldn't be necessary to add the
mailing list again.
.P
Next, I leave the editor with \f(CBc-x c-#\fP and return to the screen in Figure
.Sref \*[mutt-reply-3] .
Here I have another opportunity to change some of the headers before sending the
message.  You'll note what seem to be a couple of additional headers in this
display: \f(CWPGP\fP and \f(CWFcc:\fP.  In fact, they're not headers at all.
\f(CWPGP\fP states what parts of the message, if any, should be encrypted with
.Command pgp
or
.Command gpg .
In this case, \f(CWClear\fP (the default) means not to encrypt anything.
.P
.ne 2v
\f(CWFcc:\fP is also not a header.  It specifies the name of a folder in which
to save the outgoing message.  We'll look at folders in the next section.
.P
After making any further changes to the headers, I send the message with the
\f(CBy\fP command, after which I return to the previous display.
.PIC images/mutt-reply-3.ps 4.6i
.Figure-heading "Replying to a message: ready to send"
.Fn mutt-reply-3
.H2 "Using folders"
.Command mutt
can handle multiple folders.  It defaults to your incoming mail folder,
sometimes called an \fIinbox\fP\/.  On BSD, it is a single file in
.Directory /var/mail
with the same name as your user ID.  We saw that above at the top of the index
screen: mine is called
.File /var/mail/grog .
.P
.Command mutt
stores other folders as single files in the directory
.Directory ~/Mail ,
in other words a subdirectory of your home directory.  Many MUAs use this
method, but not all of them: some use the directory
.Directory ~/mail
instead.  By default, when you write a mail message, the outgoing message gets
copied to a file in this directory.  In the previous section, the
\f(CWCompose\fP menu contained the pseudo-header \f(CWFcc: =jbozza\fP.  This
refers to the file
.File -n ~/Mail/jbozza \/:
.Command mutt
uses the shorthand \f(CW=\fP to refer to the mail directory.
.P
To keep incoming mail, you use the \f(CBs\fP (save) command, which sets a
default folder name from the name of the sender, the same name as when saving
sent messages.  You can thus reply a message, saving a copy in the folder, then
save the original message, without explicitly mentioning a folder name at all.
.P
To read messages in a folder, you can tell
.Command mutt
to read it directly on startup:
.Dx
$ \f(CBmutt -f =fred\fP
.De
Alternatively you can change folders with the \f(CBc\fP command.
.H2 "Deleting messages"
Once you've finished reading a message, you may want to delete it.  You can do
this by entering \f(CBd\fP.  The \f(CWD\fP flag appears on the left of the line,
but nothing much else happens.  The message doesn't get deleted until you exit
.Command mutt ,
or until you enter \f(CB$\fP.
.P
When you save a message to a folder, it is automatically deleted from the
current folder.  If you don't want to do that, or if you have accidentally
deleted a message, you can \fIundelete\fP\/ it by entering \f(CBu\fP.
.P
Finished reading a thread?  You can delete the entire thread by entering
\f(CB^D\fP (\fBControl-D\fP).
.H2 "Tagging messages"
.Pn tags
We've seen that you can delete an entire thread with a single keystroke.  What
about other operations on multiple messages?  There are a couple of useful
possibilities.  You select the messages under the cursor by entering \f(CBt\fP.
In the example above, messages 51 and 64 are tagged.  You can reply to all
tagged messages in one reply by pressing \f(CB;r\fP.  In this case,
.Command mutt
ignores the message under the cursor and replies only to the tagged messages,
reply to all people on the \f(CWTo:\fP headers of each message.  Similarly, you
can do a group reply to all the tagged messages with \f(CB;g\fP, and you can
delete them all with \f(CB;d\fP.
.H2 "Configuring mutt"
.Pn muttrc
We've already see that there are a lot of things that you can change about
.Command mutt 's
behaviour.  They are described in a file
.File ~/.muttrc
(the file
.File -n .muttrc
in your home directory).  Here are a few of the more interesting entries in my
.File .muttrc \/:
.Dx
source /usr/local/etc/Muttrc
.De
The file
.File /usr/local/etc/Muttrc
contains the default definitions for running
.Command mutt .
Put this at the top of your
.File .muttrc
file so that the following definitions can override any previous definitions.
This file also contains a large number of comments about how to set each
variable, and what it does: it's over 3,000 lines long.
.Dx
source ~/.mail_aliases
.De
.File ~/.mail_aliases
.X "alias file, email"
.X "email, alias file"
is the name of an \fIalias file\fP, a file with
abbreviations for frequently used mail addresses.  We'll look at them on page
.Sref \*[aliases] .
.Dx
set alternates="greg.lehey@auug.org.au|groggy@|grog@|auugexec@|core@free"
.De
This string is a regular expression that
.Command mutt
uses to determine whether mail is addressed to me.  If it matches, it sets one
of the flags discussed above: \f(CW+\fP if the message is sent only to me,
\f(CWT\fP if I am mentioned on the \f(CWTo:\fP header, and \f(CWC\fP if I am
mentioned on the \f(CWCc:\fP header.
.Dx
my_hdr Organization:   LEMIS, PO Box 460, Echunga SA 5153, Australia
my_hdr Phone:          +61-8-8388-8286
.De
These lines and more become headers in messages I send; you can see them in the
examples above.
.Dx
set editor=emacsclient
.De
This line overrides the default editor in \f(CWEDITOR\fP.  We've already seen
the use of
.Command emacsclient .
.Dx
set pager_index_lines=10
.De
This tells
.Command mutt
to keep ten lines of the index when displaying a message.
Figure
.Sref \*[mutt-read-2] \&
shows what the display looks like when this is set.
Clearly this isn't much use with a 25 line display.  If, on the other hand, you
have a larger X display, it can be very convenient to have a selection of the
index at the top of the screen.
.Dx
set hdr_format="%4C %Z %{%d-%m-%Y} %-20.20L %-15.15t (%4l) %s" # format of the index
set status_on_top       # put the status bar at the top
set sort=threads
set date_format="%A, %e %B %Y at %k:%M:%S %Z"
.De
These variables tell
.Command mutt
how to display the message index.  They account
for the difference in layout (but not colour) between the default screen and the
custom screen.  \f(CWhdr_format\fP is a \fIprintf\fP\/-like format string that
describes the layout, \f(CWstatus_on_top\fP reverses the information lines at
the top and bottom of the display, \f(CWsort=threads\fP sets a threaded display
by default (you can change it by pressing \f(CBo\fP), and
\f(CWdate_format\fP is set to international conventions.
.PIC images/mutt-read-2.ps 4.6i
.Figure-heading "Reading a message with pager_index_lines set"
.Fn mutt-read-2
.Dx
set edit_hdrs                   # let me edit the message header when composing
set fast_reply                  # skip initial prompts when replying
set attribution="On %d, %n wrote:"
set charset="iso-8859-15"       # character set for your terminal
set sendmail_wait=-1
.De
These variables specify how to write and reply to mail messages:
.Ls B
.LI
\f(CWedit_hdrs\fP tells
.Command mutt
to include the headers in the message you write, as shown in the preceding examples.
.LI
\f(CWfast_reply\fP tells
.Command mutt
not to prompt for a number of the headers.  This is faster, and it's not
necessary when you have the headers in the message you're writing.
.LI
\fIattribution\fP\/ describes the format of the attribution at the beginning of a
reply, the text \f(CWOn Friday, 20 September 2002 at  8:13:44 -0500, Jaime Bozza
wrote:\fP in the example above.
.LI
\f(CWcharset\fP specifies the character set to use for the message.  This should
correspond to the character set of the fonts on your display, otherwise things
may look strange.  ISO 8859-15 is the new Western European character set that
includes the character for the Euro.  You'll still see many message with the
older Western European character set, ISO 8859-1, which is otherwise very
similar.
.LI
Finally, \f(CWsendmail_wait\fP tells
.Command mutt
whether it should wait for the
mail to be sent before continuing.  This can take some time if your MTA has to
perform numerous DNS lookups before it can send the message.  Setting this
variable to -1 tells
.Command mutt
not to wait.
.Le
.SPUP
.Dx
ignore *
unignore  From: Date: To: Cc: Subject: X-Mailer: Resent-From:
hdr_order Date: From: To: Cc: Subject: X-Mailer: Resent-From:
.De
.ne 3v
These specifications tell
.Command mutt
to ignore all headers except for specific ones, and to sort them in the order
specified, no matter what order they occur in in the message.
.H3 "Colours in mutt"
Finally,
.Command .muttrc
contains definitions to describe the colour of the
display.  Many of these are personal preferences, so I'll just show a couple.
Each definition specifies the foreground colour, then the background colour:
.Dx 1
color normal black white
.De
This is the basic default colour, overriding the reverse video shown above.
.Dx 2
color hdrdefault brightblack white
color quoted brightblack white
.De
This tells
.Command mutt
to highlight headers and quoted text in bold.
.Dx 4
color status black yellow
.De
This tells
.Command mutt
to display the status bars in black on a yellow
background.
.Dx 1
color index blue white FreeBSD
.De
This tells
.Command mutt
to display any messages with the text \f(CWFreeBSD\fP in
blue on white, like messages 48 and 49 in the example above.
.P
There are many more variables you can set to customize your
.Command mutt
display.  Read
.File /usr/local/etc/Muttrc
for more details.
.H2 "Mail aliases"
.Pn aliases
You'll find that some people have strange mail IDs: they are unusual, confusing,
or just plain difficult to type.  Most MUAs give you the option of setting up
\fIaliases\fP, short names for people you often contact.  In
.Command mutt ,
you can
put the aliases in the
.File ~/.muttrc
file, or you can put them in a separate
file and tell
.Command mutt
when to find them in the
.File ~/.muttrc
file, as
illustrated above.  The aliases file contains entries like this:
.Dx
alias questions FreeBSD-questions@FreeBSD.org (FreeBSD Questions)
alias stable FreeBSD Stable Users <FreeBSD-stable@FreeBSD.org>
.De
.ne 10v
The format is straightforward:
.Ls B
.LI
First comes the keyword \f(CWalias\fP.  Aliases can be placed in
.File ~/.muttrc ,
so the word \f(CWalias\fP is used to distinguish them from other commands.
.LI
Next is the alias name (\f(CWquestions\fP and \f(CWstable\fP in this example).
.LI
Next comes the mail ID in one of two forms: either the name followed by the mail
ID in angle brackets (\f(CW<>\fP), or the mail ID followed by the name in
parentheses (\f(CW()\fP).
.Le
In
.Command mutt ,
you can add aliases to this file automatically with the \f(CBa\fP command, which
offers default values relating to the current message.
.H2 "Mail headers"
.Pn headers
In the message display above we saw only a selection of the mail headers that a
message might contain.  Sometimes it's interesting to look at them in more
detail, especially if you're having mail problems.  To look at the complete
headers, press the \f(CWh\fP key.  Figure
.Sref \*[complete-headers] \&
shows the complete headers of our message 52.
.Df
.PIC images/mutt-headers.ps 4.6i
.Figure-heading "Complete headers"
.Fn "complete-headers"
.DE
.Ls B
.LI
The first line shows the name of the sender and the date it arrived at this
machine.  The date is in local time.  In this case, the name of the sender is a
mailing list, not the original sender.
.LI
The next line (\f(CWReturn-Path:\fP) is used to indicate the address to which
error messages should be sent if something goes wrong with delivery.  The
FreeBSD mailing lists specify the list owner to avoid spamming senders with
multiple error messages, which can easily happen when you send messages to a
large mailing list.
.LI
The \f(CWDelivered-To:\fP header specifies the user to whom the message was
delivered.
.LI
The next group of headers shows how the message got from the source to the
destination, in reverse chronological order.  There are a total of 11
\f(CWReceived:\fP headers, making up more than half the total number of lines.
This is because it went via a mailing list.  Normal mail messages have only one
or two \f(CWReceived:\fP headers.
.P
The first \f(CWReceived:\fP header is split over three lines.  It shows the most
recent step of the message's journey to its destination.  It shows that it was
received from \fImx2.freebsd.org\fP by \fIwantadilla.lemis.com\fP, and that
\fIwantadilla.lemis.com\fP was running
.Daemon postfix .
It also shows the time the message arrived at \fIwantadilla\fP, Sat, 21 Sep 2002
at 10:23:04.  The time zone is 9� hours ahead of UTC, and the message ID is
\f(CW195CC81743\fP.
.LI
The following \f(CWReceived:\fP headers trace back to the origin of the message,
via \fIhub.freebsd.org\fP, where it went through three transformations.  Before
that, it went through \fImail1.thinkburst.com\fP,
\fImailgate.thinkburstmedia.com\fP, \fIsigma.geocomm.com\fP and
\fIdhcp00.geocomm.com\fP\/.  By pure coincidence, every one of these systems was
running
.Daemon postfix .
Each header contains a message ID, the name of the server and its IP address.
In one case, though, the name looks different:
.Dx 2
Received: from mailgate.thinkburstmedia.com (gateway.thinkburstmedia.com [204
\&.214.64.100])
.De
The first name is the name that the server claims to be, and the second is the
name returned by a reverse DNS lookup of the server IP address.
.LI
The next five headers are the ``normal'' headers: sender, recipient, copied
recipients and date.  This example shows why they are in colour; they can appear
in a large number of different places.
.LI
We've just seen eleven different message IDs.  So why the header
\f(CWMessage-Id:\fP?  That's exactly the reason: the other eleven IDs are local
to the system they pass through.  The line beginning with \f(CWMessage-Id:\fP
gives a definitive message ID that can be used for references.
.LI
The next three headers relate to MIME and describe the version and the manner in
which the message has been encoded (7 bit plain ASCII text).
.LI
The next four headers start with \f(CWX-\fP.  They are \fIofficial custom
headers\fP, and we'll see more below.  The RFCs deliberately don't define their
meaning.  Clearly these ones are used by Microsoft software to communicate
additional information, including the fact that the MUA that created this mail
message was Microsoft Outlook.
.LI
The \f(CWIn-Reply-To:\fP header shows the ID of the message to which this is a
reply.
.Command mutt
uses this field to thread the messages in the index.
.LI
The next two fields, \f(CWImportance:\fP is also not defined by the standards.
It may be a Microsoft ``extension.''  This is not an abuse of the standards: the
RFCs allow use of any undefined header, and the \f(CWX-\fP convention is only
provided to make certain that a specific set of headers remains undefined.
.LI
.X "Bozza, Jaime"
Next comes the \f(CWSender:\fP header is the address of the \fIreal sender\fP\/.
Although this message is \f(CWFrom:\fP Jaime Bozza, it was resent from the
\f(CWFreeBSD-stable\fP mailing list.  This header documents the fact.
.LI
The following \f(CWList-\fP headers are also not defined by the standards.
They're used as comments by the mailing list software.
.LI
\f(CWX-Loop\fP is used by the mailing list software to avoid mailing loops.
The mailing list software recognizes an \f(CWX-Loop\fP header with its own name
to mean that it has somehow sent a message to itself.
.LI
The \f(CWPrecedence:\fP header is used internally by
.Daemon sendmail
to determine the order in which messages should be sent.  \f(CWbulk\fP is a low
priority.
.LI
The \f(CWX-Spam-Status:\fP header is added by
.Command spamassassin ,
which is used to detect spam.  This message has been given a clean bill of
health.
.LI
The final headers are added by
.Command mutt
when it updates the mail folder, for example when it exits.  Other MUAs add
similar headers.
.P
The \f(CWStatus:\fP flag is used by the MUA to set flags in the display.  The
letters each have their own meaning: \f(CWR\fP means that the message has been
read, and \f(CWO\fP means that it is old (in other words, it was already in the
mail folder when the MUA last exited).
.LI
The \f(CWContent-Length:\fP header specifies the \fIapproximate\fP\/ length of
the message (without the headers) in bytes.  It is used by some MUAs to speed
things up.
.LI
The \f(CWLines:\fP header states the length of the message in lines.
.Le
.H3 "How to send and reply to mail"
In the impersonal world of the Internet, your mail messages are the most
tangible thing about you.  Send out a well thought out, clear and legible
message, and you leave a good impression.  Send out a badly formulated, badly
formatted and badly spelt message, and you leave a bad impression.
.P
So what's good and what's bad?  That's a matter of opinion (and
self-expression), of course.  We've seen some of the following things already:
.Ls B
.LI
.X "web, browser"
Unless there's a very good reason, avoid proprietary formats.  Most MUAs can
handle them nowadays, but some can't.  For example, some people set up Microsoft
MUAs to use HTML as the standard format.  Many other MUAs have difficulty with
HTML, though
.Command mutt
can display it with the help of a web browser.  Microsoft MUAs are also often
configured to send out mail in Microsoft Word format, which is illegible to just
about anybody without a Microsoft system.
.LI
When sending ``conventional'' mail, ensure that you adhere to the standards.
Again, Microsoft mailers are often bad in this respect: without telling you,
they may either transform paragraphs into one long line, or they break lines
into two, one long and one short.  The resulting appearance of the message looks
like (taking this paragraph as an example):
.Dx
When sending ``conventional'' mail, ensure that you adhere to the standards.
  Again, Microsoft mailers are often bad in this respect: without telling yo
u, they may either transform paragraphs into one long line, or they break li
nes into two, one long and one short.  The resulting appearance of the messa
ge looks like (taking this paragraph as an example):
.De
.Figure-heading "One line per paragraph"
.SPUP
.Dx
When sending ``conventional'' mail, ensure that you adhere to the
standards.
Again, Microsoft mailers are often bad in this respect: without
telling you,
they may either transform paragraphs into one long line, or they
break lines
into two, one long and one short.  The resulting appearance of the
message looks
like (taking this paragraph as an example):
.De
.Figure-heading "Alternate long and short lines"
This can happen to you without you knowing.  If you get messages from other
people that appear to be garbled, your MUA may be reformatting them on arrival,
in which case it is possibly reformatting them before transmission.
.LI
When replying, ensure that you use a quote convention as shown above.
Place your reply text directly below the part of the text to which you are
replying.
.LI
Messages tend to grow as more and more replies get added.  If large parts of the
original text are irrelevant, remove them from the reply.
.LI
Leave an empty line between the original text and your reply, and leave a space
after the \f(CW>\fP quote character.  Both make the message more legible.  For
example, compare these two fragments:
.Dx
> rdkeys@csemail.cropsci.ncsu.edu writes:
>>Not to pick at nits.... but, I am still confused as to what EXACTLY
>>is the ``stable'' FreeBSD.  Please enlighten me, and tell me the
>>reasoning behind it.
>OK, I'll take a shot at this.  To really understand what 2.2-STABLE is,
>you have to have some idea of how the FreeBSD team uses 'branches'.  In
>particular, we are talking about branches as implemented by the CVS
.De
.Figure-heading "Less legible reply"
.SPUP
.Dx
> rdkeys@csemail.cropsci.ncsu.edu writes:
>> Not to pick at nits.... but, I am still confused as to what EXACTLY
>> is the ``stable'' FreeBSD.  Please enlighten me, and tell me the
>> reasoning behind it.
>
> OK, I'll take a shot at this.  To really understand what 2.2-STABLE is,
> you have to have some idea of how the FreeBSD team uses 'branches'.  In
> particular, we are talking about branches as implemented by the CVS
.De
.Figure-heading "More legible reply"
.SPUP
.LI
What about salutations?  You'll see a lot of messages out there that don't
start with ``Dear Fred,'' and either aren't even signed or just have the name of
the author.  This looks rather rude at first, but it has become pretty much a
standard on the Net.  There's a chance that this will change in the course of
time, but at the moment it's the way things are, and you shouldn't assume any
implicit rudeness on the part of people who write in this manner.
.LI
At the other end of the scale, some people add a standard signature block to
each message.  You can do this automatically by storing the text in a file
called
.File ~/.signature .
If you do this, consider that it appears in
\fIevery\fP\/ message you write, and that it can get on people's nerves if it's
too long or too scurrile.
.LI
Make sure that your user ID states who you are.  It doesn't make a very good
impression to see mail from \f(CWfoobar@greatguru.net (The greatest guru on
Earth)\fP, especially if he happens to make an incorrect statement.  There are
better ways to express your individuality.
.Le
.SPUP
.H3 "Using MIME attachments"
.Pn MIME
MIME allows you to attach all sorts of data to a mail message, including images
and sound clips.  It's a great advantage, but unfortunately many people refuse
to use it, perhaps because the UNIX community haven't got their act together.
Credit where credit's due, this is one area where Microsoft is ahead of the UNIX
crowd.
.P
.ne 10v
Nevertheless, you can do a lot of things wrong with MIME attachments.  Here are
some of the more common ones, most of which are default for Microsoft MUAs.
.Ls B
.LI
\f(BIUse HTML attachments only for web pages\fP.  Many MUAs allow you to send
messages in \f(CWtext/html\fP format by default.  HTML is not an appropriate
format for mail messages: it's intended for the Web.  Of course, if you want to
send somebody a web page, this is the way to do it.
.LI
\f(BIDon't use proprietary attachments\fP.  From time to time, I get attachments
that assume that I have the same software as the sender.  Typical ones are
\f(CWapplication/octet-stream\fP with Microsoft proprietary formats (for
example, one of the Microsoft Word formats), and
\f(CWapplication/mac-binhex40\fP, which is used by Mac MUAs for images.  If the
recipients don't have this software, they can't use the attachment.
.LI
\f(BIDon't send multiple copies in different formats\fP.  Some MUAs send both a
\f(CWtext/plain\fP and a \f(CWtext/html\fP attachment bundled up in a
\f(CWmultipart/alternative\fP attachment.  This wastes space and can cause a lot
of confusion.
.LI
\f(BISpecify the correct attachment type\fP.  If you send a web page as an
attachment, be sure that it is specified as \f(CWtext/html\fP.  The receiving
MUA can use this to display the attachment correctly.  If you specify it, say,
as \f(CWtext/plain\fP, the MUA displays it with all the formatting characters,
which doesn't improve legibility.  If you send a \fI.gif\fP image as
\f(CWimage/gif\fP, the MUA can display the image directly.  Otherwise the user
needs to save the message and perform possibly complex conversions to see the
image.
.P
Microsoft-based MUAs frequently make this mistake.  You may receive attachments
of the type \f(CWapplication/octet-stream\fP, which really describes the
encoding, not the content, but the name might end in \f(CW.doc\fP, \f(CW.gif\fP
or \f(CW.jpg\fP.  Many MUAs assume that these attachments are Microsoft Word
documents or GIF and JPEG images respectively.  This is contrary to the
standards and could be used to compromise the security of your system.
.Le
.ig
Talk about procmail.
..
