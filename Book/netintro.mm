.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: netintro.mm,v 4.16 2003/04/02 06:48:55 grog Exp grog $
.\"
.Chapter \*[nchnetintro] "Networks and the Internet"
.X "Internet"
.X "network"
In this part of the book we'll look at the fastest-growing part of the industry:
\fInetworks\fP, and in particular the \fIInternet\fP.
.P
The industry has seen many different kinds of network software:
.Ls B
.LI
.X "CCITT"
.X "X.25"
Years ago, the \fICCITT\fP\/ started a group of recommendations for individual
protocols.  The CCITT is now called the ITU-T, and its data communications
recommendations have not been wildly successful.  The best known is probably
recommendation \fIX.25\fP, which still has a large following in some parts of
the world.  An X.25 package was available for FreeBSD, but it died for lack of
love.  If you need it, you'll need to invest a lot of work to get it running.
.LI
.X "Systems Network Architecture"
.X "SNA"
IBM introduced their \fISystems Network Architecture\fP, \fISNA\fP, decades ago.
It's still going strong in IBM shops.  FreeBSD has minimal support for it in the
Token Ring package being developed in FreeBSD-CURRENT.
.LI
.X "UUCP"
.X "UNIX to UNIX Copy"
.X "Basic Networking Utilities"
.X "BNU"
Early UNIX machines had a primitive kind of networking called \fIUUCP\fP, for
\fIUNIX to UNIX Copy\fP.  It ran over dialup phone lines or dedicated serial
connections.  System V still calls this system \fIBasic Networking Utilities\fP,
or \fIBNU\fP.  Despite its primitiveness, and despite the Internet, there are
still some applications where UUCP makes sense, but this book discusses it no
further.
.LI
.X "Internet Protocol"
.X "Defense Advanced Research Projects Agency"
.X "DARPA"
.X "ARPANET"
.X "Internet"
The \fIInternet Protocols\fP were developed by the US \fIDefense Advanced
Research Projects Agency\fP\/ (\fIDARPA\fP\/) for its \fIARPANET\fP\/ network.
The software was originally developed in the early 80s by BBN and the CSRG at
the University of California at Berkeley.  The first widespread release was with
the 4.2BSD operating system\(emthe granddaddy of FreeBSD.  After the
introduction of IP, the ARPANET gradually changed its name to \fIInternet\fP.
.P
.X "IP"
.X "TCP/IP"
.X "Transmission Control Protocol"
.X "Internet Protocol"
The Internet Protocol is usually abbreviated to \fIIP\fP.  People often refer to
it as \fITCP/IP\fP\/, which stands for \fITransmission Control Protocol/Internet
Protocol\fP.  In fact, TCP is just one of many other protocols that run on top
of IP.  In this book, I refer to the IP protocol, but of course FreeBSD includes
TCP and all the other standard protocols.  The IP implementation supplied with
FreeBSD is the most mature technology you can find anywhere, at any price.
.Le
In this part of the book, we'll look only at the Internet Protocols.  Thanks to
its background, FreeBSD is a particularly powerful contender in this area, and
we'll go into a lot of detail about how to set up and operate networks and
network services.  In the chapters following, we'll look at:
.Ls B
.LI
How the Internet works, which we'll look at in the rest of this chapter.
.LI
How to set up local network connections in
.Sref "\*[chnetsetup]" .
.LI
How to select an Internet Service Provider in
.Sref "\*[chisp]" .
.LI
How to use the hardware in
.Sref "\*[chmodems]" .
.LI
How to use PPP in
.Sref "\*[chppp]" .
.LI
How to set up domain name services in
.Sref "\*[chdns]" .
.LI
How to protect yourself from intruders in
.Sref "\*[chfirewall]" .
.X "proxy server"
.X "network address translation"
This chapter also describes \fIproxy servers\fP and \fINetwork Address
Translation\fP.
.LI
How to solve network problems in
.Sref "\*[chnetdebug]" .
.LI
.X "network, client"
.X "network, server"
Most network services come in pairs, a \fIclient\fP\/ that requests the
service, and a \fIserver\fP\/ that provides it.  In
.Sref "\*[chclient]" \&
we'll look at the client side of the World Wide Web (``web browser''), command
execution over the net, including
.Command ssh
and
.Command telnet ,
copying files across the network, and mounting remote file systems with NFS.
.LI
In
.Sref "\*[chserver]" \&
.X "samba"
.X "Common Internet File System"
.X "CIFS"
we'll look at the server end of the same services.  In addition, we'll look at
\fISamba\fP, a server for Microsoft's \fICommon Internet File System\fP, or
\fICIFS\fP.
.LI
Electronic mail is so important that we dedicate two chapters to it,
.Sref "\*[chmua]" \&
and
.Sref "\*[chmta]" .
..if XXX
Unwritten chapters
(news.mm)
.LI
How to send
..if XXX
and receive
..endif
faxes in
.Sref "\*[chfax]" .
.LI
How to set up a news feed, starting on page
.Sref \*[news-setup] .
.LI
How to set up a diskless FreeBSD system, starting on page
(dickless.mm)
.Sref \*[bootp-setup] .
..endif
.Le
The rest of this chapter looks at the theoretical background of the Internet
Protocols and Ethernet.  You can set up networking without understanding any of
it, as long as you and your hardware don't make any mistakes.  This is the
approach most commercial systems take.  It's rather like crossing a lake on a
set of stepping stones, blindfolded.  In this book, I take a different approach:
in the following discussion, you'll be inside with the action, not on the
outside looking in through a window.  It might seem unusual at first, but once
you get used to it, you'll find it much less frustrating.
.H2 "Network layering"
.X "World Wide Web"
.X "WWW"
.X "Internet"
.X "network, link layer"
One of the problems with networks is that they can be looked at from a number of
different levels.  End-users of PCs access the World Wide Web (WWW), and often
enough they call it the \fIInternet\fP.  That's just plain wrong.  At the other
end of the scale is the \fILink Layer\fP, the viewpoint you'll take when you
first create a connection to another machine.
.P
.X "OSI reference model"
.X "OSI"
.X "ISO"
.X "Open Systems Interconnect"
Years ago, the International Standards Organization came up with the idea of a
seven-layered model of networks, often called the \fIOSI reference model\fP.
Why \fIOSI\fP\/ and not \fIISO\fP\/?  \fIOSI\fP\/ stands for \fIOpen Systems
Interconnect\fP.  Since its introduction, it has become clear that it doesn't
map very well to modern networks.  W. Richard Stevens presents a better layering
in \fITCP/IP Illustrated\fP, Volume 1, page 6, shown here in Figure
.Sref \*[4layer] .
.Df
.PS
h = .2i
dh = .02i
dw = 2i
move right 3i
down
[
        [
                boxht = h; boxwid = dw
          A:    box ht .35i
          B:    box ht .35i
          C:    box ht .35i
          D:    box ht .35i
                "Application layer" at A
                "Transport layer" at B
                "Network layer" at C
                "Link layer" at D
                ]
]
.PE
.Figure-heading "Four-layer network model"
.Fn 4layer
.ps \n(PS                       \"  point size of standard text
.vs \n(LS                       \" Standard vertical spacing
.DE
.P
We'll look at these layers from the bottom up:
.Ls B
.LI
.X "network, link layer"
.X "Ethernet"
The \fILink layer\fP\/ is responsible for the lowest level of communication,
between machines that are physically connected.  The most common kinds of
connection are Ethernet and telephone lines.  This is the only layer associated
with hardware.
.LI
.X "network, layer"
.X "routing"
The \fINetwork layer\fP\/ is responsible for communication between machines
that are not physically connected.  For this to function, the data must pass
through other machines that are not directly interested in the data.  This
function is called \fIrouting\fP.  We'll look at how it works in Chapter
.Sref "\*[nchnetsetup]" .
.LI
.X "network, transport layer"
The \fITransport Layer\fP\/ is responsible for communication between any two
processes, regardless of the machines on which they run.
.LI
.X "network, application layer"
The \fIApplication Layer\fP\/ defines the format used by specific applications,
such as email or the Web.
.Le
.H3 "The link layer"
.Pn packet-header
.X "network, link layer"
.X "network, packet"
.X "network, datagram"
Data on the Internet is split up into \fIpackets\fP, also called
\fIdatagrams\fP, which can be transmitted independently of each other.  The
\fIlink layer\fP\/ is responsible for getting packets between two systems that
are connected to each other.  The most trivial case is a point-to-point network,
a physical connection where any data sent down the line arrives at the other
end.  More generally, though, multiple systems are connected to the network, as
in an Ethernet.  This causes a problem: how does each system know what is
intended for it?
.P
.X "network, packet header"
IP solves this problem by including a \fIpacket header\fP\/ in each IP packet.
Consider the header something like the information you write on the outside of a
letter envelope: address to send to, return address, delivery instructions.  In
the case of IP, the addresses are 32-bit digits that are conventionally
represented in \fIdotted decimal\fP\/ notation: the value of each byte is
converted into decimal.  The four values are written separated by dots.  Thus
the hexadecimal address \f(CW0xdf932501\fP would normally be represented as
\f(CW223.147.37.1\fP.
.Aside
UNIX uses the notation \f(CW0x\fP in a number to represent a hexadecimal number.
The usage comes from the C programming language.
.End-aside
.P
As we will see in Chapter
.Sref "\*[nchnetdebug]" ,
it makes debugging much easier if we understand the structure of the datagrams,
so I'll show some of the more common ones in this chapter.  Figure
.Sref \*[IP-header] \&
shows the structure of an IP header.
.Df
.PS
.ps 8
h = .3i
dh = .02i
dw = 2i
move right .3i
bit = .14i
nibble = bit * 4
byte = nibble * 2
halfword = byte * 2
word = halfword * 2
boxht = h; boxwid = byte
A:      box wid nibble "Version";
A1:     box wid nibble;
        box "Type of service"
        box wid halfword "Total length in bytes"
B:      box wid halfword at A.sw+(byte,-boxht/2) "identification"
        box wid nibble * 3 / 4 "flags"
        box wid nibble * 13 / 4 "fragment offset"
C:      box at B.sw+(boxwid/2,-boxht/2) "Time to live"
        box "Protocol"
        box wid halfword "Header Checksum"
boxwid = word
D:      box at C.sw+(boxwid/2,-boxht/2) "Source IP address"
E:      box at D.sw+(boxwid/2,-boxht/2) "Destination IP address"

        "IP Header" at A1 above; "length" at A1 below
.\"     Bit numbers
        box invis wid bit "0" at A.nw+(bit/2,boxht/2)
        box invis wid bit "31" at A.nw+(bit * 63/2,boxht/2)
.\" Byte offsets
        "\f(CW0\fP" rjust at A.w+(-.1,0)
        "\f(CW4\fP" rjust at B.w+(-.1,0)
        "\f(CW8\fP" rjust at C.w+(-.1,0)
        "\f(CW12\fP" rjust at D.w+(-.1,0)
        "\f(CW16\fP" rjust at E.w+(-.1,0)
.PE
.ps \n(PS                       \"  point size of standard text
.vs \n(LS                       \" Standard vertical spacing
.Figure-heading "IP Header"
.Fn IP-header
.DE
We'll only look at some of these fields; for the rest, see \fITCP/IP
Illustrated\fP, Volume 1.
.Ls B
.LI
.X "IPv6"
The \fIVersion\fP\/ field specifies the current version of IP.  This is
currently 4.  A newer standard is \fIIPv6\fP, Version number 6, which is
currently in an early implementation stage.  IPv6 headers are very different
from those shown here.
.\".LI
.\"XXXThe \fItype of service\fP\/ indicates the use for the packet
.LI
.X "time to live"
.Pn time-to-live
The \fItime to live\fP\/ field specifies how many times the packet may be passed
from one system to another.  Each time it is passed to another system, this
value is decremented.  If it reaches 0, the packet is discarded.  This prevents
packets from circulating in the net for ever as the result of a routing loop.
.LI
.X "network, protocol"
The \fIprotocol\fP\/ specifies the kind of the packet.  The most common
protocols are TCP and UDP, which we'll look at in the section on the network
layer.
.LI
.X "network, source address"
.X "network, destination address"
Finally come the \fIsource address\fP, the address of the sender, and the
\fIdestination address\fP, the address of the recipient.
.Le
.SPUP
.H3 "The network layer"
.X "network, routing"
The main purpose of the network layer is to ensure that packets get delivered to
the correct recipient when it is not directly connected to the sender.  This
function is usually called \fIrouting\fP.
.P
Imagine routing to be similar to a postal system: if you want to send a letter
to somebody you don't see often, you put the letter in a letter box.  The people
or machines who handle the letter look at the address and either deliver it
personally or forward it to somebody else who is closer to the recipient, until
finally somebody delivers it.
.P
Have you ever received a letter that has been posted months ago?  Did you
wonder where they hid it all that time?  Chances are it's been sent round in
circles a couple of times.  That's what can happen in the Internet if the
routing information is incorrect, and that's why all packets have a \fItime to
live\fP\/ field.  If it can't deliver a packet, the Internet Protocol simply
drops (forgets about) it.  You may find parallels to physical mail here, too.
.P
It's not usually acceptable to lose data.  We'll see how we avoid doing so in
the next section.
.H3 "The transport layer"
.X "network, transport layer"
The \fItransport layer\fP\/ is responsible for end-to-end communication.  The IP
address just identifies the interface to which the data is sent.  What happens
when it gets there?  There could be a large number of processes using the link.
The IP header doesn't contain sufficient information to deliver messages to
specific users within a system, so two additional protocols have been
implemented to handle the details of communications between ``end users.''\*F
.FS
In practice, these end users are processes.
.FE
.X "network, port"
These end users connect to the network via \fIports\fP, or communication end
points, within individual machines.
.H4 "TCP"
.X "Transmission Control Protocol"
.X "TCP"
.X "reliable protocol"
.X "connection oriented protocol"
The \fITransmission Control Protocol\fP, or \fITCP\fP, is a so-called
\fIreliable protocol\fP\/: it ensures that data gets to its destination, and if
it doesn't, it sends another copy.  If it can't get through after a large number
of tries (14 tries and nearly 10 minutes), it gives up, but it doesn't pretend
the data got through.  To perform this service, TCP is also \fIconnection
oriented\fP\/: before you can send data with TCP, you must establish a
connection, which is conceptually similar to opening a file.
.P
.X "TCP, header"
To implement this protocol, TCP packets include a \fITCP header\fP\/ after the
IP header, as shown in Figure
.Sref \*[TCP-header] .
This figure ignores the possible options that follow the IP header.  The offset
of the TCP header, shown here as 20, is really specified by the value of the IP
Header length field in the first byte of the packet.  This is only a 4 bit
field, so it is counted in words of 32 bits: for a 20 byte header, it has the
value 5.
.PS
.ps 8
h = .3i
dh = .02i
dw = 2i
move right .3i
bit = .14i
nibble = bit * 4
byte = nibble * 2
halfword = byte * 2
word = halfword * 2
boxht = h;
boxwid = byte

A:      box wid nibble "Version";
A1:     box wid nibble;
        box "Type of service"
        box wid halfword "Total length in bytes"
B:      box wid halfword at A.sw+(byte,-boxht/2) "identification"
        box wid nibble * 3 / 4 "flags"
        box wid nibble * 13 / 4 "fragment offset"
C:      box at B.sw+(boxwid/2,-boxht/2) "Time to live"
        box "Protocol"
        box wid halfword "Header Checksum"
boxwid = word
D:      box at C.sw+(boxwid/2,-boxht/2) "Source IP address"
E:      box at D.sw+(boxwid/2,-boxht/2) "Destination IP address"

.\" TCP header
boxwid = halfword;
linethick=1.5
T1:     box at E.sw+(byte, -boxht/2) "source port"; box "destination port"
T2:     box wid word at T1.sw+(halfword,-boxht/2) "sequence number"
T3:     box wid word at T2.sw+(halfword,-boxht/2) "acknowledgment number"
T4:     box wid nibble at T3.sw+(bit*2,-boxht/2)
        box wid bit*6 "reserved"; box wid bit*6 "flags"; box "window size"
T5:     box at T4.sw+(byte,-boxht/2) "TCP checksum"; box "urgent pointer"

        "IP Header" at A1 above; "length" at A1 below
        "\s7TCP Header\s0" at T4 above; "length" at T4 below
.\"     Bit numbers
        box invis wid bit "0" at A.nw+(bit/2,boxht/2)
        box invis wid bit "31" at A.nw+(bit * 63/2,boxht/2)
.\" Byte offsets
        "\f(CW0\fP" rjust at A.w+(-.1,0)
        "\f(CW4\fP" rjust at B.w+(-.1,0)
        "\f(CW8\fP" rjust at C.w+(-.1,0)
        "\f(CW12\fP" rjust at D.w+(-.1,0)
        "\f(CW16\fP" rjust at E.w+(-.1,0)
        "\f(CW20\fP" rjust at T1.w+(-.1,0)
        "\f(CW24\fP" rjust at T2.w+(-.1,0)
        "\f(CW28\fP" rjust at T3.w+(-.1,0)
        "\f(CW32\fP" rjust at T4.w+(-.1,0)
        "\f(CW36\fP" rjust at T5.w+(-.1,0)
.PE
.Figure-heading "TCP Header with IP header"
.Fn TCP-header
.P
A number of fields are of interest when debugging network connections:
.Ls B
.LI
.X "TCP, sequence number"
.X "sequence number, TCP"
The \fIsequence number\fP\/ is the byte offset of the last byte that has been
sent to the other side.
.LI
.X "TCP, acknowledgment number"
.X "acknowledgment number, TCP"
The \fIacknowledgment number\fP\/ is the byte offset of the last byte that has
received from the other side.
.LI
.X "TCP, window size"
.X "window size, TCP"
The \fIwindow size\fP\/ is the number of bytes that can be sent before an
acknowledgment is required.
.Le
These three values are used to ensure efficient and reliable transmission of
data.  For each connection, TCP maintains a copy of the highest acknowledgment
number received from the other side and a copy of all data that the other side
has not acknowledged receiving.  It does not send more than \fIwindow size\fP\/
bytes of data beyond this value.  If it does not receive an acknowledgment of
transmitted data within a predetermined time, usually one second, it sends all
the unacknowledged data again and again at increasingly large intervals.  If it
can't transmit the data after about ten minutes, it gives up and closes the
connection.
.H4 "UDP"
.X "User Datagram Protocol"
.X "UDP"
.X "unreliable protocol"
.X "rwhod, daemon"
.X "daemon, rwhod"
The \fIUser Datagram Protocol\fP, or \fIUDP\fP, is different: it's an
\fIunreliable protocol\fP.  It sends data out and never cares whether it gets to
its destination or not.  So why do we use it if it's unreliable?  It's faster,
and thus cheaper.  Consider it a junk mail delivery agent: who cares if you get
this week's AOL junk CD-ROM or not?  There will be another one in next week's
mail.  Since it doesn't need to reply, UDP is connectionless: you can just send
a message off with UDP without worrying about establishing a connection first.
For example, the \fIrwhod\fP\/ daemon broadcasts summary information about a
system on the LAN every few minutes.  In the unlikely event that a message gets
lost, it's not serious: another one will come soon.
.br
.DF
.PS
.ps 8
h = .3i
dh = .02i
dw = 2i
move right .3i
bit = .14i
nibble = bit * 4
byte = nibble * 2
halfword = byte * 2
word = halfword * 2
boxht = h; boxwid = byte
linethick=-1
A:      box wid nibble "Version";
A1:     box wid nibble;
        box "Type of service"
        box wid halfword "Total length in bytes"
B:      box wid halfword at A.sw+(byte,-boxht/2) "identification"
        box wid nibble * 3 / 4 "flags"
        box wid nibble * 13 / 4 "fragment offset"
C:      box at B.sw+(boxwid/2,-boxht/2) "Time to live"
        box "Protocol"
        box wid halfword "Header Checksum"
boxwid = word
D:      box at C.sw+(boxwid/2,-boxht/2) "Source IP address"
E:      box at D.sw+(boxwid/2,-boxht/2) "Destination IP address"

        "IP Header" at A1 above; "length" at A1 below
.\"     Bit numbers
        box invis wid bit "0" at A.nw+(bit/2,boxht/2)
        box invis wid bit "31" at A.nw+(bit * 63/2,boxht/2)
.\" UDP header
boxwid = halfword;
linethick=1.5
U1:     box at E.sw+(byte, -boxht/2) "source port"; box "destination port"
U2:     box at U1.sw+(byte,-boxht/2) "sequence number"; box "checksum"

.\" Byte offsets
        "\f(CW0\fP" rjust at A.w+(-.1,0)
        "\f(CW4\fP" rjust at B.w+(-.1,0)
        "\f(CW8\fP" rjust at C.w+(-.1,0)
        "\f(CW12\fP" rjust at D.w+(-.1,0)
        "\f(CW16\fP" rjust at E.w+(-.1,0)
        "\f(CW20\fP" rjust at U1.w+(-.1,0)
        "\f(CW24\fP" rjust at U2.w+(-.1,0)
.\" Back to default line thickness
        linethick=-1
.PE
.Figure-heading "UDP Header with IP header"
.Fn UDP-header
.DE
.H3 "Port assignment and Internet services"
.X "network, port"
A \fIport\fP\/ is simply a 16 bit number assigned to specific processes and
which represents the source and destination end points of a specific connection.
A process can either request to be connected to a specific port, or the system
can assign one that is not in use.
.P
.X "network, port, well-known"
RFC 1700 defines a number of \fIwell-known ports\fP\/ that are used to request
specific services from a machine.  On a UNIX machine, these are provided by
daemons that \fIlisten\fP\/ on this port number\(emin other words, when a
message comes in on this port number, the IP software passes it to them, and
they process it.  These ports are defined in the file
.File /etc/services .
Here's an excerpt:
.br
.ne 15v
.Dx
# Network services, Internet style
#
# WELL KNOWN PORT NUMBERS
#
ftp              21/tcp    #File Transfer [Control]
ssh		 22/tcp    #Secure Shell Login
ssh		 22/udp    #Secure Shell Login
telnet           23/tcp
smtp             25/tcp    mail         #Simple Mail Transfer
smtp             25/udp    mail         #Simple Mail Transfer
domain           53/tcp    #Domain Name Server
domain           53/udp    #Domain Name Server
\&...
http		 80/tcp	   www www-http		#World Wide Web HTTP
http		 80/udp	   www www-http		#World Wide Web HTTP
.De
This file has a relatively simple format: the first column is a service name,
and the second column contains the port number and the name of the service
(either \f(CWtcp\fP or \f(CWudp\fP).  Optionally, alternative names for the
service may follow.  In this example, \f(CWsmtp\fP may also be called
\f(CWmail\fP, and \f(CWhttp\fP may also be called \f(CWwww\fP.
.P
When the system starts up, it starts specific daemons.  For example, if you're
running mail, you may start up \fIsendmail\fP\/ as a daemon.  Any mail requests
coming in on port 25 (\f(CWsmtp\fP) will then be routed to \fIsendmail\fP\/ for
processing.
.H3 "Network connections"
.Pn quintuple
You can identify a TCP connection uniquely by five parameters:
.Ls B
.LI
The source IP address.
.LI
The source port number.  These two parameters are needed so that the other end
of the connection can send replies back.
.LI
The destination IP address.
.LI
The destination port number.
.LI
The protocol (TCP).
.Le
When you set up a connection, you specify the destination IP address and port
number, and implicitly also the protocol.  Your system supplies the source IP
address; that's obvious enough.  But where does the source port number come
from?  The system literally picks one out of a hat; it chooses an unused port
number somewhere above the ``magic'' value 1024.  You can look at this
information with
.Command netstat \/:
.Dx
$ \f(CBnetstat\fP
Proto Recv-Q Send-Q  Local Address      Foreign Address        (state)
tcp4       0      0  presto.smtp        203.130.236.50.1825    ESTABLISHED
tcp4       0      0  presto.3312        andante.ssh            ESTABLISHED
tcp4       0      0  presto.2593        hub.freebsd.org.ssh    ESTABLISHED
tcp4       0      0  presto.smtp        www.auug.org.au.3691   ESTABLISHED
.De
As you can see, this is the view on a system called \fIpresto\fP.  We'll see
\fIpresto\fP\/ again in our sample network below.  Normally you'll see a lot
more connections here.  For each connection, the protocol is \f(CWtcp4\fP (TCP
on IPv4).  The first line shows a connection to the port \f(CWsmtp\fP on
\fIpresto\fP from port 1825 on a machine with the IP address 203.130.236.50.
.Command netstat
shows the IP address in this case because the machine in question does not have
reverse DNS mapping.  This machine is sending a mail message to \fIpresto\fP.
The second and third lines show outgoing connections from \fIpresto\fP\/ to port
\f(CWssh\fP on the systems \fIandante\fP\/ and \fIhub.freebsd.org\fP.  The last
is another incoming mail message from \fIwww.auug.org.au\fP.  Graphically, you
could display the connection between \fIpresto\fP\/ and \fIwww.auug.org.au\fP\/
like this:
.PS
        boxht = .4i
        boxwid = .6i
PRESTO: box
        "\fIpresto\fP" at PRESTO
WWW:    box at PRESTO+(4,0)
        "\fIwww\fP" at WWW

L:      line from PRESTO.e to WWW.w
        "IP \f(CW223.147.37.2\fP" at PRESTO.e+(.1,.1) ljust
        "Port \f(CW25\fP" at PRESTO.e+(.1,-.1) ljust

        "IP \f(CW150.101.248.57\fP" at WWW.w+(-.1,.1) rjust
        "Port \f(CW3691\fP" at WWW.w+(-.1,-.1) rjust

        "TCP" at L.c+(0,-.1)
.PE
.\" Grrr
.ps \n(PS
Note that the port number for \f(CWsmtp\fP is 25.
.P
For various reasons, it's not always possible to connect directly in this
manner:
.Ls B
.LI
.X "network address translation"
.X "NAT"
The Internet standards define a number of IP address blocks as
\fInon-routable\fP.  In these cases, we'll have to translate at least the IP
addresses to establish connection.  This technique is accordingly called
\fINetwork Address Translation\fP\/ or \fINAT\fP\/, and we'll look at it in
Chapter
.Sref "\*[nchfirewall]" ,
on page
.Sref \*[ip-aliasing] .
.LI
.X "tunneling"
For security reasons, it may not be advisable to make direct connections to
servers via the Internet.  Instead, the only access may be via an encrypted
session on a different port.  This technique is called \fItunneling\fP, and
we'll look at it in Chapter
.Sref "\*[nchclient]" ,
on page
.Sref \*[tunneling] .
.Le
.SPUP
.H2 "The physical network connection"
The most obvious thing about your network connection is what it looks like.  It
usually involves some kind of cable going out of your computer,\*F
.FS
Maybe it won't.  For example, you might use wireless Ethernet, which broadcasts
in the microwave radio spectrum.
.FE
but there the similarity ends.  FreeBSD supports most modern network interfaces:
.Ls B
.LI
.X "local area network"
.X "Ethernet"
The most popular choice for \fILocal Area Networks\fP\/ is \fIEthernet\fP\/,
which transfers data between a number of computers at speeds of 10 Mb/s, 100
Mb/s or 1000 Mb/s (1 Gb/s).  We'll look at it in the following section.
.LI
.X "wireless networking"
An increasingly popular alternative to Ethernet is \fIwireless networking\fP,
specifically local networks based on the IEEE 802.11 standard.  We'll look at
them on page
.Sref \*[802.11] .
.LI
.Pn FDDI
.X "FDDI"
\fIFDDI\fP\/ stands for \fIFiber Distributed Data Interface\fP, and was
originally run over glass fibres. In contrast to Ethernet, it ran at 100 Mb/s
instead of 10 Mb/s.  Nowadays Ethernet runs at 100 Mb/s as well, and FDDI runs
over copper wire, so the biggest difference is the protocol.  FreeBSD does
support FDDI, but we won't look at it here.
.LI
.X "network, token ring"
\fIToken Ring\fP\/ is yet another variety of LAN, introduced by IBM.  It has
never been very popular in the UNIX world.  FreeBSD does have some support for
it, but it's a little patchy, and we won't look at it in this book.
.LI
.X "network, wide-area"
.X "DSL"
Probably the most common connection to a \fIWide-Area Network\fP\/ is via a
telephone with a modem or with DSL.  Modems have the advantage that you can
also use them for non-IP connections such as UUCP and direct dial up (see page
.Sref \*[dialin] ),
but they're much slower than DSL.
.X "point to point, protocol"
.X "protocol, point to point"
.X "PPP"
If you use a modem to connect to the Internet, you'll almost certainly use the
\fIPoint to Point Protocol\fP, \fIPPP\fP\/, which we look at on page
.Sref \*[PPP] .
.X "Serial Line Internet Protocol"
.X "protocol, serial line internet"
.X "SLIP"
In some obscure cases you may need to use the \fISerial Line Internet
Protocol\fP\/, \fISLIP\fP\/, but it's really obsolete.
.LI
.X "cable networking"
An alternative to ADSL or modem lines is \fIcable networking\fP, which uses TV
cable services to supply Internet connectivity.  In many ways, it looks like
Ethernet.
.LI
.X "Integrated Services Digital Network"
.X "ISDN"
In some areas, \fIIntegrated Services Digital Networks\fP\/ (\fIISDNs\fP\/) are
an attractive alternative to modems.  They are much faster than modems, both in
call setup time and in data transmission capability, and they are also much more
reliable.  FreeBSD includes the \fIisdn4bsd\fP\/ package, which was developed in
Germany and allows the direct connection of low-cost German ISDN boards to
FreeBSD.  In other parts of the world, ISDN is not cost effective, and it's also
much slower than ADSL and cable.
.LI
.X "network, satellite link"
In some parts of the world, \fIsatellite links\fP\/ are of interest.  In most
cases, they are unidirectional: they transfer data from the Internet to your
system (the \fIdownlink\fP\/) and require some other connection to get data back
to the Internet (the \fIuplink\fP\/).
.LI
.X "network, leased line"
If you have a large Internet requirement, you may find it suitable to connect to
the Internet via a \fILeased Line\fP\/, a telephone line that is permanently
connected.  This is a relatively expensive option, of course, and we won't
discuss it here, particularly as the options vary greatly from country to
country and from region to region.
.Le
.X "Internet Service Provider"
.X "ISP"
The decision on which WAN connection you use depends primarily on the system you
are connecting to, in many cases an \fIInternet Service Provider\fP\/ or
\fIISP\fP\/.  We'll look at ISPs in Chapter
.Sref "\*[nchisp]" .
.H2 "Ethernet"
.Pn Ethernet
.P
.X "Metcalfe, Bob"
.X "Ethernet"
.X "PARC"
In the early 1970s, the Xerox Company chartered a group of researchers at its
Palo Alto Research Center (\fIPARC\fP\/) to brainstorm the \fIOffice of the
Future\fP.  This innovative group created the mouse, the window interface
metaphor and an integrated, object-oriented programming environment called
\fISmalltalk\fP.  In addition, a young MIT engineer in the group named Bob
Metcalfe came up with the concept that is the basis of modern local area
networking, the \fIEthernet\fP.  The Ethernet protocol is a low-level broadcast
packet-delivery system that employed the revolutionary idea that it was easier
to resend packets that didn't arrive than it was to make sure all packets
arrived.  There are other network hardware systems out there, IBM's Token Ring
architecture and Fiber Channel, for example, but by far the most popular is the
Ethernet system in its various hardware incarnations.  Ethernet is by far the
most common local area network medium.  There are three types:
.Ls N
.LI
.X "Ethernet"
.X "10B5"
.X "AUI"
.X "yellow string"
.X "string, yellow"
.Pn enet-terminator
Originally, Ethernet ran at 10 Mb/s over a single thick coaxial cable, usually
bright yellow in colour.  This kind of Ethernet is often referred to as \fIthick
Ethernet\fP, also called \fI10B5\fP, and the line interface is called \fIAUI\fP.
You may also hear the term \fIyellow string\fP\/ (for tying computers together),
though this term is not limited to thick Ethernet.  Thick Ethernet is now
obsolete: it is expensive, difficult to lay, and relatively unreliable.  It
requires 50 \(*W resistors at each end of the cable to transmit signals
correctly.  If you leave these out, you won't get degraded performance: the
network Will Not Work at all.
.LI
.X "Ethernet"
.X "Cheapernet"
.X "RG58"
.X "BNC"
.X "10 Base 2"
As the name suggests, \fIthin Ethernet\fP\/ is thin coaxial cable, and otherwise
quite like thick Ethernet.  It is significantly cheaper (thus the term
\fICheapernet\fP\/), and the only disadvantage over thick Ethernet is that the
cables can't be quite as long.  The cable is called \fIRG58\fP, and the cable
connectors are called \fIBNC\fP.  Both terms are frequently used to refer to
this kind of connection, as is \fI10 Base 2\fP.  You'll still see thin Ethernet
around, but since it's effectively obsolete.  Performance is poor, and it's no
cheaper than 100 Mb/s Ethernet.  Like thick Ethernet, all machines are connected
by a single cable with terminators at each end.
.LI
.X "10BaseT"
.X "UTP"
.X "unshielded twisted pair"
.X "twisted pair, unshielded "
.X "hub"
Modern Ethernets run at up to 1000 Mb/s over multi-pair cables called
\fIUTP\fP, for \fIUnshielded Twisted Pair\fP.  \fITwisted
pair\fP\/ means that each pair of wires are twisted to minimize external
electrical influence\(emafter all, the frequencies on a 1000 Mb/s Ethernet are
way up in the UHF range.  Unlike coaxial connections, where all machines are
connected to a single cable, UTP connects individual machines to a \fIhub\fP\/
or a \fIswitch\fP, a box that distributes the signals.  We'll discuss the
difference between a hub and a switch on page
.Sref \*[hub-and-switch] .
.ne 3v
You'll also hear the terms \fI10BaseTP\fP, \fI100BaseTP\fP\/ and
\fI1000BaseTP\fP.
.P
Compared to coaxial Ethernet, UTP cables are much cheaper, and they are more
reliable.  If you damage or disconnect a coaxial cable, the whole network goes
down.  If you damage a UTP cable, you only lose the one machine connected to it.
On the down side, UTP requires switches or hubs, which cost money, though the
price has decreased to the point where it's cheaper to buy a cheap switch and
UTP cables rather than the RG58 cable alone.  UTP systems employ a star
architecture rather than the string of coaxial stations with terminators.  You
can connect many switches together simply by reversing the connections at one
end of a switch-to-switch link.  In addition, UTP is the only medium currently
available that supports 100 Mb/s Ethernet.
.Le
.H3 "How Ethernet works"
.X "Ethernet, address"
.X "MAC address"
.X "Address, Ethernet"
.X "Address, MAC"
A large number of systems can be connected to a single Ethernet.  Each system
has a 48 bit address, the so-called \fIEthernet address\fP.  Ethernet addresses
are usually written in bytes separated by colons (\f(CW:\fP), for example
\f(CW0:a0:24:37:0d:2b\fP.  All data sent over the Ethernet contains two
addresses: the Ethernet address of the sender and the Ethernet address of the
receiver.  Normally, each system responds only to messages sent to it or to a
special broadcast address.
.P
.ne 3v
You'll also frequently hear the term \fIMAC address\fP.  \fIMAC\fP\/ stands for
\fIMedia Access Control\fP and thus means the address used to access the network
link layer.  For Ethernets I prefer to use the more exact term \fIEthernet
address\fP.
.P
.X "carrier sense"
.X "collision"
.X "CSMA/CD"
.X "Carrier Sense Multiple Access/Collision Detect"
The fact that multiple machines are on the same network gives rise to a problem:
obviously only one system can transmit at any one time, or the data will be
garbled.  But how do you synchronize the systems?  In traditional Ethernets, the
answer is simple, but possibly surprising: trial and error.  Before any
interface transmits, it checks that the network is idle\(emin the Ethernet
specification, this is called \fICarrier Sense\fP\/.  Unfortunately, this isn't
enough: two systems might start sending at the same time.  To solve this
problem, while it sends, each system checks that it can still recognize what it
is sending.  If it can't, it assumes that another system has started sending at
the same time\(emthis is called a \fIcollision\fP.  When a collision occurs,
both systems stop sending, wait a random amount of time, and try again.  You'll
see this method referred to as \fICSMA/CD\fP\/ (\fICarrier Sense Multiple
Access/Collision Detect\fP\/).
.P
.Pn hub-and-switch
There are a number of problems with this approach:
.Ls B
.LI
.X "half-duplex network"
.X "full-duplex network"
The interface needs to listen while sending, so it can't receive anything while
it's sending: it's running in \fIhalf-duplex\fP\/ mode.  If it could send and
receive at the same time (\fIfull-duplex\fP\/ mode), the network throughput
could be doubled.
.LI
The more active the network, the more likely collisions will be.  This slows
things down too, sometimes to a point where the network hardly transmits any
traffic.
.LI
The more systems on the network, the less bandwidth is available for each
system.
.Le
With the point-to-point connections on a UTP-based network, you would think it
would be possible to change some of this.  After all, the connections look
pretty much like the same wire that joins two modems together, and modems don't
have collisions, and they do run in full-duplex mode.  The problem is the hub:
if you send a packet out to a hub, it doesn't know which connector to send it
down, so it sends it down all of them, thus imitating the old Ethernet.  To send
it just to the destination, it would need to analyze the Ethernet address in
every packet and know where to send it.
.P
This is what a switch does: it learns the Ethernet addresses of each interface
on the network and uses this information to send packets to only the line to
which that interface is connected.  There could be more than one if switches are
cascaded.  This also means that the line can run in full-duplex mode.
.P
Nowadays the price differential between switches and hubs is very small; go into
a computer market and you'll see that the prices overlap.  If at all possible,
buy a switch.
.P
.X "NetBIOS"
.X "IPX"
Transmitting Internet data across an Ethernet has another problem.  Ethernet
evolved independently of the Internet standards.  As a result, Ethernets can
carry different kinds of traffic.  In particular, Microsoft uses a protocol
called \fINetBIOS\fP, and Novell uses a protocol called \fIIPX\fP.  In addition,
Internet addresses are only 32 bits, and it would be impossible to map them to
Ethernet addresses even if they were the same length.  The result?  You guessed
it, another header.  Figure
.Sref \*[Ethernet-frame] \&
shows an Ethernet packet carrying an IP datagram.
.H3 "Finding Ethernet addresses"
.Pn ARP
So we send messages to Ethernet interfaces by setting the correct Ethernet
address in the header.  But how do we find the Ethernet address?  All our IP
packets use IP addresses.  And it's not a good solution to just statically
assign Ethernet addresses to IP addresses: first, there would be problems if an
interface board or an IP address was changed, and secondly multiple boards can
have the same IP address.
.P
.X "Address Resolution Protocol"
.X "ARP"
.ne 5v
.PS
.ps 8
h = .3i
dh = .02i
dw = 2i
move right .3i
bit = .14i
nibble = bit * 4
byte = nibble * 2
halfword = byte * 2
word = halfword * 2
boxht = h;
boxwid = halfword
linethick=1.5
E1:     box invis; box "Upper destination address"
E2:     box wid word at E1.sw+(halfword,-boxht/2) "Rest of destination address"
E3:     box wid word at E2.sw+(halfword,-boxht/2) "Upper source address"
E4:     box at E3.sw+(byte,-boxht/2) "Rest of source address"; box "Frame type"
boxwid = byte
linethick=-1
A:      box at E4.sw+(bit*2,-boxht/2) wid nibble "Version";
A1:     box wid nibble;
        box "Type of service"
        box wid halfword "Total length in bytes"
B:      box wid halfword at A.sw+(byte,-boxht/2) "identification"
        box wid nibble * 3 / 4 "flags"
        box wid nibble * 13 / 4 "fragment offset"
C:      box at B.sw+(boxwid/2,-boxht/2) "Time to live"
        box "Protocol"
        box wid halfword "Header Checksum"
boxwid = word
D:      box at C.sw+(boxwid/2,-boxht/2) "Source IP address"
E:      box at D.sw+(boxwid/2,-boxht/2) "Destination IP address"

.\" TCP header
boxwid = halfword;
T1:     box at E.sw+(byte, -boxht/2) "source port"; box "destination port"
T2:     box wid word at T1.sw+(halfword,-boxht/2) "sequence number"
T3:     box wid word at T2.sw+(halfword,-boxht/2) "acknowledgment number"
T4:     box wid nibble at T3.sw+(bit*2,-boxht/2)
        box wid bit*6 "reserved"; box wid bit*6 "flags"; box "window size"
T5:     box at T4.sw+(byte,-boxht/2) "TCP checksum";
T6:     box "urgent pointer"

        line dotted down boxht from T5.sw
        line dotted down boxht from T6.se
        "Data" at T5.se+(0,-boxht/2)

        "IP Header" at A1 above; "length" at A1 below
        "\s7TCP Header\s0" at T4 above; "length" at T4 below
.\"     Bit numbers
.\"     box invis wid bit "0" at A.nw+(bit/2,boxht/2)
.\"     box invis wid bit "31" at A.nw+(bit * 63/2,boxht/2)
.PE
.Figure-heading "Ethernet frame with TCP datagram"
.Fn Ethernet-frame
The chosen solution is the \fIAddress Resolution Protocol\fP, usually called
\fIARP\fP.  ARP sends out a message on the Ethernet broadcast address saying
effectively ``Who has IP address \f(CW223.147.37.1\fP?  Tell me your Ethernet
address.''  The message is sent on the broadcast address, so each system on the
net receives it.  In each machine, the ARP protocol checks the specified IP
address with the IP address of the interface that received the packet.  If they
match, the machine replies with the message ``I am IP \f(CW223.147.37.1\fP, my
Ethernet address is \f(CW00:a0:24:37:0d:2b\fP''
.H3 "What systems are on that Ethernet?"
.Pn netmask
.X "net mask"
.X "network mask"
.X "interface, address"
Multiple systems can be accessed via an Ethernet, so there must be some means
for a system to determine which other systems are present on the network.  There
might be a lot of them, several hundred for example.  You could keep a list, but
the system has to determine the interface for every single packet, and a list
that long would slow things down.  The preferred method is to specify a
\fIrange\fP\/ of IP addresses that can be reached via a specific interface.
The computer works in binary, so one of the easiest functions to perform is a
\fIlogical and\fP.  As a result, you specify the range by a \fInetwork
mask\fP\/: the system considers all addresses in which a specific set of bits
have a particular value to be reachable via the interface.  The specific set of
bits is called the \fIinterface address\fP.
.P
For example, let's look forward to the reference network on page
.Sref \*[ref-net] \&
and consider the local network, which has the network address
\f(CW223.147.37.0\fP and the netmask \f(CW255.255.255.0\fP.  The value
\f(CW255\fP means that every bit in the byte is set.  The logical \fIand\fP\/
function says ``if a specific bit is set in both operands, set the result bit to
1; otherwise set it to 0.''  Figure
.Sref \*[netmask-figure] \&
shows how the system creates a network address
from the IP address \f(CW223.147.37.5\fP and the net mask \f(CW255.255.255.0\fP.
.PS
        boxht = .2i
        boxwid = .12i
        right
IP:

        box "\fB1\fP"; box "\fB1\fP"; box "0"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP";
        move .1i
        box "\fB1\fP"; box "0"; box "0"; box "\fB1\fP"; box "0"; box "0"; box "\fB1\fP"; box "\fB1\fP";
        move .1i
        box "0"; box "0"; box "\fB1\fP"; box "0"; box "0"; box "\fB1\fP"; box "0"; box "\fB1\fP";
        move .1i
        box "0"; box "0"; box "0"; box "0"; box "0"; box "1"; box "0"; box "1";

        "\s9   IP address\s0" ljust

        move to IP-(boxwid/2,.3)

MASK:   box "\fB1\fP"; box "\fB1\fP"; box "1"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP";
        move .1i
        box "\fB1\fP"; box "1"; box "1"; box "\fB1\fP"; box "1"; box "1"; box "\fB1\fP"; box "\fB1\fP";
        move .1i
        box "1"; box "1"; box "\fB1\fP"; box "1"; box "1"; box "\fB1\fP"; box "1"; box "\fB1\fP";
        move .1i
        box "0"; box "0"; box "0"; box "0"; box "0"; box "0"; box "0"; box "0";
        "\s9   Net mask\s0" ljust

        move to MASK-(boxwid/2,.3)

N:      box "\fB1\fP"; box "\fB1\fP"; box "0"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP"; box "\fB1\fP";
        move .1i
        box "\fB1\fP"; box "0"; box "0"; box "\fB1\fP"; box "0"; box "0"; box "\fB1\fP"; box "\fB1\fP";
        move .1i
        box "0"; box "0"; box "\fB1\fP"; box "0"; box "0"; box "\fB1\fP"; box "0"; box "\fB1\fP";
        move .1i
        box "0"; box "0"; box "0"; box "0"; box "0"; box "0"; box "0"; box "0"
        "\s9   Net address\s0" ljust
.PE
.Figure-heading "Net mask"
.Fn netmask-figure
The result is the same as the IP address for the first three bytes, but the last
byte is 0: \f(CW223.147.37.0\fP.
.P
This may seem unnecessarily complicated.  An easier way to look at it is to say
that the \fI1\fP\/ bits of the net mask describe which part of the address is
the network part, and the \fI0\fP\/ bits describe which part represents hosts on
the network.
.P
Theoretically you could choose your network mask bits at random.  In practice,
it's clear that it makes more sense to make network masks a sequence of binary 1
bits followed by a sequence of binary 0 bits.  It has become typical to
abbreviate the network mask to the number of 1 bits.  Thus the network mask
255.255.255.0, with 24 bits set and 8 bits not set, is abbreviated to
\f(CW/24\fP.  The \f(CW/\fP character is always part of the abbreviation.
.H3 "Address classes"
.Pn inet-address-class
.X "address class"
When the Internet Protocols were first introduced, they included the concept of
a default netmask.  These categories of address were called \fIaddress
classes\fP.  The following classes are defined in RFC 1375:
.br
.ne 10
.Table-heading "Address classes"
.TS
tab(#) ;
r | lfCWp9 | lfCWp9 | r | r | r .
###\fBNetwork\fP#\fBHost\fP#
#\fB\s10Address\fP#\fB\s10Network#\fBaddress\fP#\fBaddress\fP#\fBNumber of\fP
\fBClass\fP#\fB\s10range\fP#\fB\s10mask\fP#\fBbits\fP#\fBbits\fP#\fBsystems\fP
_
A#0-127#255.0.0.0#/8#24#16777216
B#128-191#255.255.0.0#/16#16#65536
C#192-207#255.255.255.0#/24#8#256
F#208-215#255.255.255.240#/28#4#16
G#216-219#\fI(reserved)\fP\/
H#220-221#255.255.255.248#/29#3#8
K#222-223#255.255.255.254#/31#1#2
D#224-239#\fI(multicast)\fP\/
E#240-255#\fI(reserved)\fP\/
.TE
.sp 1.5v
.X "Class C network"
.X "network, Class C"
This method is no longer used for specifying net masks, though the software
still defaults to these values, but it is used for allocating networks.  In
addition you will frequently hear the term \fIClass C network\fP\/ to refer to a
network with 256 addresses in the range 192\(en223.  This usage goes back to
before RFC 1375.
.\" XXX BMC > | Hmm.  Is it worth mentioning the "magic" meaning of the `all 1s' and
.\" XXX > | `all 0s' host addresses?  -bmc
.H3 "Unroutable addresses"
On occasion you may want to have addresses which are not visible on the global
Internet, either for security reasons or because you want to run Network Address
Translation (see page
.Sref \*[ip-aliasing] ).
RFC 1918 provides for three address ranges that should not be routed:
\f(CW10.0.0.0/8\fP (with last address \f(CW10.255.255.255\fP),
\f(CW172.16.0.0/12\fP (with last address \f(CW172.31.255.255), and
\f(CW192.168.0.0/16\fP (with last address \f(CW192.168.255.255\fP).
.H2 "Wireless LANs"
.Pn 802.11
An obvious problem with Ethernet is that you need a cable.  As more and more
machines are installed, the cabling can become a nightmare.  It's particularly
inconvenient for laptops: the network cable restricts where you can use the
machine.
.P
Wireless network cards have been around for some time, but in the last few years
they have become particularly popular.  Modern cards are built around the IEEE
802.11 series of standards.
.Aside
The 802 series of standards cover almost all networking devices; don't let the
number 802 suggest wireless networking.  Ethernet is 802.3, for example.
.End-aside
They are usually PCMCIA (PC Card) cards, though some PCI cards are also
available.  Currently you're liable to come across the following kinds of cards:
.Ls B
.LI
.X "wireless network, 802.11 FHSS"
.X "802.11 FHSS"
.X "Frequency Hopping Spread Spectrum"
\fI802.11 FHSS\fP\/ (\fIFrequency Hopping Spread Spectrum\fP\/) cards, which run
at up to 2 Mb/s.  These are now obsolete, but FreeBSD still supports the WebGear
Aviator card with the
.Device -i ray
driver.
.LI
.X "wireless network, 802.11 DSSS"
.X "802.11 DSSS"
.X "Discrete Sequence Spread Spectrum"
\fI802.11 DSSS\fP\/ (\fIDiscrete Sequence Spread Spectrum\fP\/) cards, which
also run at up to 2 Mb/s.  These are also obsolete.
.LI
.X "wireless network, 802.11b DSSS"
.X "802.11b DSSS"
\fI802.11b DSSS\fP\/ cards, which run at up to 11 Mb/s.  They can interoperate
with the slower 802.11 DSSS cards, but not with FHSS cards.
.LI
.X "wireless network, 802.11a"
.X "802.11a"
.X "Orthogonal Frequency Division Multiplexing"
.X "OFDM"
\fI802.11a\fP\/ cards, which run at 54 Mb/s.  They use a modulation called
\fIOrthogonal Frequency Division Multiplexing\fP\/ or \fIOFDM\fP, and run in the
5 GHz band.  They are not compatible with older cards.  At the time of writing,
they have not achieved significant market penetration.  FreeBSD does not support
them yet, though that may have changed by the time you read this.
.LI
.X "wireless network, 802.11g"
.X "802.11g"
\fI802.11g\fP\/ cards are the newest.  Like 802.11a, they which run at 54 Mb/s,
and they're not supported.  Again, that may have changed by the time you read
this.  Like 802.11b, they run in the 2.4 GHz band.
.Le
Most current cards are 802.11b and run at up to 11 Mb/s.  We'll concentrate on
them in the rest of this section.  They operate in the 2.4 GHz band, which is
shared with a number of other services, including some portable telephones and
microwave ovens.  This kind of portable telephone can completely disrupt a
wireless network.  Interference and range are serious issues: wireless networks
are generally not as reliable as wired networks.
.P
Wireless cards can operate in up to three different modes:
.Ls B
.LI
.X "access point"
.X "base station"
.X "Basic Service Set"
Normally, they interoperate with an \fIaccess point\fP, also called a \fIbase
station\fP.  The base station is normally connected to an external network, so
it also doubles as a gateway.  Unlike Ethernets, however, all traffic in the
network goes via the base station.  This arrangement is called a \fIBasic
Service Set\fP\/ or \fIBSS\fP.
.P
.X "Extended Basic Service Set"
.X "EBSS"
Networks can have multiple base stations which are usually interconnected via a
wired Ethernet.  If the machine with the wireless card moves around, the base
stations negotiate with the machine to decide which base station handles the
card.  In this manner, the machines can cover large distances without losing
network connection.  This arrangement is called an \fIExtended Basic Service
Set\fP\/ or \fIEBSS\fP.
.P
.X "managed mode"
.X "infrastructure mode"
.X "BSS mode"
This mode of operation, with or without an EBSS, is called \fImanaged mode\fP,
\fIinfrastructure mode\fP\/ or \fIBSS mode\fP.
.LI
.X "peer-to-peer mode"
.X "ad-hoc mode"
.X "IBSS mode "
.X "Independent Basic Service Set"
In smaller networks, the cards can interact directly.  This mode of operation is
called \fIpeer-to-peer mode\fP, \fIad-hoc mode\fP\/ or \fIIBSS mode \fP\/ (for
\fIIndependent Basic Service Set\fP).
.LI
.X "demo ad-hoc mode"
Finally, some cards support a method called \fILucent demo ad-hoc mode\fP, which
some BSD implementations used to call \fIad-hoc mode\fP.  But it's not the same
as the previous method, and though the principle is the same, they can't
interoperate.  This mode is not standardized, and there are significant
interoperability issues with it, so even if it's available you should use IBSS
mode.
.Le
.H3 "How wireless networks coexist"
Wireless networks have a number of issues that don't affect Ethernets.  In
particular, multiple networks can share the same geographical space.  In most
large cities you'll find that practically the entire area is shared by multiple
networks.  This raises a number of issues:
.Ls B
.LI
There's only so much bandwidth available.  As the number of networks increase,
the throughput drops.
.P
There's no complete solution to this problem, but it's made a little easier by
the availability of multiple operating frequencies.  Depending on the country,
802.11b cards can have between 11 and 14 frequency channels.  If your area has a
lot of traffic on the frequency you're using, you may be able to solve the
problem by moving to another frequency.  That doesn't mean that this many
networks can coexist in the same space: as the name \fIspread spectrum\fP\/
indicates, the signal wanders off to either side of the base frequency, and in
practice you can use only three or four distinct channels.
.LI
Cards on a given network need to have a way to identify each other.
.P
.X "Service Set Identifier"
.X "SSID"
802.11 solves this issue by requiring a network identification, called a
\fIService Set Identifier\fP\/ or \fISSID\fP.  All networks have an SSID, though
frequently base stations will accept connections from cards that supply a blank
SSID.  SSIDs don't offer any improvement in security: their only purpose is
identifying the network.
.LI
Cards on a given network need to protect themselves against snooping by people
who don't belong to the network.
.P
The 802.11 standard offers a partial solution to this issue by optionally
encrypting the packets.  We'll look at this issue below.
.Le
.H3 "Encryption"
.X "Wired Equivalent Privacy"
.X "WEP"
As mentioned above, security is a big issue in wireless networks.  The
encryption provided is called \fIWired Equivalent Privacy\fP\/ or \fIWEP\fP, and
it's not very good.  Everybody connecting to the network needs to know the WEP
key, so if anybody loses permission to access the network (for example, when
changing jobs), the WEP keys need to be changed, which is a serious
administrative problem.  In some cases it's completely impractical: if you want
to access a wireless network in an airport or a coffee shop (where they're
becoming more and more common), it's not practical to use a WEP key.  In fact,
nearly all such public access networks don't use encryption at all.
.P
As if that weren't bad enough, the WEP algorithm is flawed.  Depending on the
circumstances, it can take less than 10 minutes to crack it.  Don't trust it.
.P
So how do you protect yourself?  The best solution is, of course, don't use
wireless networks for confidential work.  If you have to use a wireless network,
make sure that anything confidential is encrypted end-to-end, for example with
an
.Command ssh
tunnel, which we'll look at on page
.Sref \*[tunneling] .
.bp
.H2 "The reference network"
.Pn ref-net
.\" XXX Don Wilde <don@PartsNow.com>:
.\" XXX > 4) When you describe Ethernet / Cheapernet, spend a little more time
.\" XXX > detailing the hardware of 10Base-T, which is becoming far more common
.\" XXX > than Cheapernet. Maybe add a segment on your reference network which is
.\" XXX > 10Base-T, with a couple of switchs included so you can show the
.\" XXX > reversed-connection cable which links switchs together. Also, a discussion
.\" XXX > of making a gateway for 100Base-T segment routing would also be
.\" XXX > appropriate.
One of the problems in talking about networks is that there are so many
different kinds of network connection.  To simplify things, this book bases on
one of the most frequent environments: a number of computers connected together
by an Ethernet LAN with a single gateway to the Internet.  Figure
.Sref \*[reference-net] \&
shows the layout of the network to which we will refer in the rest of this book.
.PS
.X "freebie.example.org"
.X "presto.example.org"
.X "bumble.example.org"
.X "wait.example.org"
.X "example.org"
        boxht = .4i
        boxwid = .6i
FREEBIE:        box
        "\fIfreebie\fR" at FREEBIE
PRESTO: box at FREEBIE+(1.2,0)
        "\fIpresto\fR" at PRESTO
BUMBLE: box at PRESTO+(1.2i,0)
        "\fIbumble\fR" at BUMBLE
WAIT:   box at BUMBLE+(1.2i,0)
        "\fIwait\fR" at WAIT

.\" Local Ethernet
E:      line thickness 1.5 from FREEBIE.sw-(.1i,.3i) to WAIT.se+(.5i,-.3i)
        "Local Ethernet" ljust at E.w+(0,-.2)
        "Address \s8\f(CW223.147.37.0\fR\s0" ljust at E.w+(0,-.37)
        "Domain \fIexample.org\fR" ljust at E.w+(0,-.53)
.\" Terminators
.X "gw.example.org"
.X "free-gw.example.net"
        box wid .15i ht .08i fill 1 at E.w
        box wid .15i ht .08i fill 1 at E.e

        "\s14\(bu\s0" at FREEBIE.s+(.01,-.03)
        "\s14\(bu\s0" at PRESTO.s+(.01,-.03)
        "\s14\(bu\s0" at PRESTO.n+(.01,-.03)
        "\s14\(bu\s0" at BUMBLE.s+(.01,-.03)
        "\s14\(bu\s0" at WAIT.s+(.01,-.03)

        line from FREEBIE.s to FREEBIE.s-(0,.3i)
        line from PRESTO.s to PRESTO.s-(0,.3i)
        line from BUMBLE.s to BUMBLE.s-(0,.3i)
        line from WAIT.s to WAIT.s-(0,.3i)

GW:     box at E.c+(-.4,-.5)
        line from GW.n to GW.n+(0,.3)
        "\s14\(bu\s0" at GW.n+(0,-.03)
        "\s14\(bu\s0" at GW.s+(0,-.03)
        "\s14\(bu\s0" at GW.e+(0,-.03)
        "\fIgw\fR" at GW above
        "Router" at GW below

M1:     box ht .2i at GW.s+(0,-.3)
        "\s14\(bu\s0" at M1.n+(0,-.03)
        "\s14\(bu\s0" at M1.s+(0,-.03)
        "modem" at M1

M2:     box ht .2i at GW.s+(0,-.8)
        "\s14\(bu\s0" at M2.n+(0,-.03)
        "\s14\(bu\s0" at M2.s+(0,-.03)
        "modem" at M2

.\" ISP's router
ISP:    box wid 2i at GW.s+(-.7i,-1.3)
        "\s14\(bu\s0" at ISP.n+(-.7,-.03)
        "\s14\(bu\s0" at ISP.n+(.7,-.03)
        "\s14\(bu\s0" at ISP.s+(0,-.03)
        "router" at ISP above
        "\fIfree-gw\fR" at ISP below

M3:     box ht .2i at ISP.n+(-.7,.3)
        "\s14\(bu\s0" at M3.n+(0,-.03)
        "\s14\(bu\s0" at M3.s+(0,-.03)
        "modem" at M3
        line from M3.s+(0,-.03) to ISP.n+(-.7,-.03)
        line up .15i from M3.n+(0,-.03)
        "Connection to network" at M3.n+(0,.30)
        "\f(CW\s8223.147.38.0\s0\fR" at M3.n+(0,.17)

L1:     line from GW.s to M1.n
.\"     "Local modem connection" ljust at L1.c+(.4,0)

PPP:    line from M1.s to M2.n
        "PPP link, net \s8\f(CW139.130.136.0\fR\s0" ljust at PPP.c+(.4,0)

L2:     line from M2.s to ISP.n+(.7,0)
.\"     "ISP modem connection" ljust at L2.c+(.4,0)
.X "example.net"

.\" Remote Ethernet
RE:     line thickness 1.5 from E.w+(0,-2.45i) to WAIT.se+(.5i,-2.75i)
        "ISP's Ethernet" ljust at RE.w+(0,-.2)
        "Address \s8\f(CW139.130.237.0\fR\s0" ljust at RE.w+(0,-.37)
        "Domain \fIexample.net\fR" ljust at RE.w+(0,-.54)
        line from ISP.s to RE.c+(-1.1,0)
.\" Terminators
.X "igw.example.net"
.X "ns.example.net"
        box wid .15i ht .08i fill 1 at RE.w
        box wid .15i ht .08i fill 1 at RE.e

.\" igw to global Internet
IGW:    box at RE.w+(2,-.5)
        "\s14\(bu\s0" at IGW.n+(0,-.03)
        "\s14\(bu\s0" at IGW.s+(0,-.03)
        "gateway" at IGW above
        "\fIigw\fR" at IGW below
        line from IGW.n to RE.w+(2,0)
        line wid .5i down from IGW.s

NS:     box at RE.w+(4,-.5)
        "\s14\(bu\s0" at NS.n+(0,-.03)
        "\fIns\fR" at NS
        line from NS.n to RE.w+(4,0)

.\" Wireless network

AP:     box at PRESTO+(0,.8)
        "access" at AP above
        "point" at AP below
        "\s14\(bu\s0" at AP.s+(0,-.03)
        "\s14\(bu\s0" at AP.e+(0,-.03)

AN:     box at AP+(1.5,0)
        "\fIandante\fR\/" at AN above
        "laptop" at AN below
        "\s14\(bu\s0" at AN.w+(0,-.03)

        line from PRESTO.n to AP.s
        line from AP.e to AP.e+(.6,.125) to AP.e+(.4,-.125) to AN.w
        "\s8802.11b wireless net\s0" ljust at AP.e+(-.1,.3)

.\" Interface names
        "\s8\f(CWrl0\fR\s0" rjust at FREEBIE.s+(-.05,-.1)
        "\s8\f(CWdc0\fR\s0" rjust at PRESTO.s+(-.05,-.1)
        "\s8\f(CWxl0\fR\s0" rjust at PRESTO.n+(-.05,.1)
        "\s8\f(CWxl0\fR\s0" rjust at BUMBLE.s+(-.05,-.1)
        "\s8\f(CWfxp0\fR\s0" rjust at WAIT.s+(-.05,-.1)
        "\s8\f(CWdc0\fR\s0" rjust at GW.n+(-.05,.1)
        "\s8\f(CWtun0\fR\s0" rjust at GW.s+(-.05,-.1)
        "\s8\f(CWppp3\fR\s0" rjust at ISP.n+(.65,.1)
        "\s8\f(CWppp0\fR\s0" rjust at ISP.n+(-.75,.1)
        "\s8\f(CWrl0\fR\s0" rjust at ISP.s+(-.05,-.1)
        "\s8\f(CWfddi0\fR\s0" rjust at IGW.s+(-.05,-.1)
        "\s8\f(CWxl0\fR\s0" rjust at IGW.n+(-.05,.1)
        "to" rjust at IGW.s+(-.05i,-.27i)
        "Internet" ljust at IGW.s+(.05i,-.27i)
        "\s8\f(CWxl0\fR\s0" rjust at NS.n+(-.05,.1)

.\" IP addresses
        "\s8\f(CW223.147.37.1\fR\s0" ljust at FREEBIE.s+(.05,-.1)
        "\s8\f(CW223.147.37.2\fR\s0" ljust at PRESTO.s+(.05,-.1)
        "\s8\f(CW192.168.27.1\fR\s0" ljust at PRESTO.n+(.05,.1)
        "\s8\f(CW223.147.37.3\fR\s0" ljust at BUMBLE.s+(.05,-.1)
        "\s8\f(CW223.147.37.4\fR\s0" ljust at WAIT.s+(.05,-.1)
        "\s8\f(CW223.147.37.5\fR\s0" ljust at GW.n+(.05,.1)
        "\s8\f(CW139.130.136.133\fR\s0" ljust at GW.s+(.05,-.1)
        "\s8\f(CW139.130.136.129\fR\s0" ljust at ISP.n+(.75,.1)
        "\s8\f(CW139.130.136.9\fR\s0" ljust at ISP.n+(-.65,.1)
        "\s8\f(CW139.130.237.117\fR\s0" ljust at ISP.s+(.05,-.1)
        "\s8\f(CW139.130.237.65\fR\s0" ljust at IGW.n+(.05,.1)
        "\s8\f(CW139.130.249.201\fR\s0" ljust at IGW.s+(.05,-.1)
        "\s8\f(CW139.130.237.3\fR\s0" ljust at NS.n+(.05,.1)
        "\s8\f(CWwi0 192.168.27.17\fR\s0" at AN.s+(.05,-.1)
.PE
.Figure-heading "Reference network"
.Fn reference-net
.P
.if n \{\
This diagram is very difficult to represent in ASCII.  The texts are too large
to fit in the available space, so they have run into each other and been
truncated.  If you can't understand it, don't worry about it.  The version in
the printed book is much better.
.\}
.P
.ne 5v
This figure contains a lot of information, which we will examine in detail in
the course of the text:
.Ls B
.LI
.X "freebie.example.org"
.X "presto.example.org"
.X "bumble.example.org"
.X "wait.example.org"
The boxes in the top row represent the systems in the local network
\f(CWexample.org\fP: \fIfreebie\fP, \fIpresto\fP, \fIbumble\fP, and \fIwait\fP.
.LI
The line underneath is the local Ethernet.  The network has the address
\f(CW223.147.37.0\fP.  It has a full 256 addresses (``Class C''), so the network
mask is \f(CW255.255.255.0\fP.
.LI
.X "example.org"
.X "bumble.example.org"
The machines on this Ethernet belong to the domain \fIexample.org\fP.  Thus, the
full name of \fIbumble\fP is \fIbumble.example.org\fP.  We'll look at these
names in Chapter
.Sref "\*[nchdns]" .
.LI
.X "interface, name"
The connections from the systems to the Ethernet are identified by two values:
on the left is the \fIinterface name\fP, and on the right the address associated
with the interface name.
.LI
Further down the diagram is the router, \f(CWgw\fP.  It has two interfaces:
\f(CWdc0\fP interfaces to the Ethernet, and \f(CWtun0\fP interfaces to the PPP
line to the ISP.  Each interface has a different addresses.
.LI
.X "igw.example.net"
.X "FDDI"
The lower half of the diagram shows part of the ISP's network.  It also has an
Ethernet, and its router looks very much like our own.  On the other hand, it
interfaces to a third network via the machine \fIigw\fP.  To judge by the name
of the interface, it is a \fIFDDI\fP connection\(emsee page
.Sref \*[FDDI] \&
for more details.
.LI
The ISP runs a name server on the machine \f(CWns\fP, address
\f(CW139.130.237.3\fP.
.LI
.X "terminator, Ethernet
The ends of the Ethernets are thickened.  This represents the
\fIterminators\fP\/ required at the end of a coaxial Ethernet.  We talked about
them on page
.Sref \*[enet-terminator] .
In fact this network is a 100 Mb/s switched network, but they are still
conventionally represented in this form.  You can think of the Ethernets as the
switches that control each network.
.LI
\fIpresto\fP\/ has a wireless access point connected to it.  The diagram shows
one laptop, \fIandante\fP, connected via a NAT interface.
.Le
