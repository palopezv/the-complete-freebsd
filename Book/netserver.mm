.\" This file is in -*- nroff-fill -*- mode
.\" STATUS:  4th edition
.\" $Id: netserver.mm,v 4.19 2003/04/09 20:42:40 grog Exp grog $
.\"
.Chapter \*[nchserver] "Basic network access: servers"
In the previous chapter, we saw how to use clients to access other systems.
This is only half the picture, of course.  At the other end of the link, we need
\fIservers\fP\/ to provide this service.  For each client, there is a server (a
daemon) whose name is usually derived from the client name by adding a \f(CWd\fP
to it:
.br
.ne 1i
.X "telnetd, daemon"
.X "daemon, telnetd"
.X "rshd, daemon"
.X "daemon, rshd"
.X "rlogind, daemon"
.X "daemon, rlogind"
.X "ftpd, daemon"
.X "daemon, ftpd"
.X "rshd, daemon"
.X "daemon, rshd"
.Table-heading "Server daemons for basic services"
.TS H
tab(#) ;
lI | lI .
\fRClient#\fRServer
_
.TH
ssh#sshd
telnet#telnetd
sftp#sftp-server
ftp#ftpd
rsync#rsyncd
\fR(browser)#httpd
\fR(NFS)#nfsd
.TE
.sp 1.5v
In addition to these servers, we look at a few others in other chapters:
.Ls B
.LI
We've already looked at \fIX servers\fP\/ briefly in
.Sref "\*[chunixadmin]" ,
and we'll see more in
.Sref "\*[chxtheory]" .
.LI
Chapter
.Sref \*[nchdns] \&
discussed DNS \fIname servers\fP.
.LI
Chapter
.Sref \*[nchmta] \&
discusses \fIMail Transport Agents\fP\/ or \fIMTAs\fP, also referred to as
\fImail servers\fP.
.Le
Some servers don't need any configuration, and about all you need to do is to
start them.  Others, like web servers, can be very complicated.  None of the
complication is related to FreeBSD.  For example, the issues involved in
configuring \fIapache\fP\/ are the same whether you run it with FreeBSD, NetBSD,
Linux or Solaris.  There are several good books, each at least the size of this
one, on the detailed setup of some of these servers.  In this chapter we'll look
at how to get the servers up and running in a basic configuration, and where to
turn for more information.
.H2 "Running servers from inetd"
.Pn inetd
.Pn services
.X "internet daemon"
If you look at
.File /etc/services ,
you'll find that there are over 800 services available, most of which are only
supported on a small number of machines.  It's not always the best idea to start
up a daemon for every possible service you may want to offer.  IP supplies an
alternative:
.Daemon inetd ,
the \fIInternet daemon\fP, sometimes called a \fIsuper-server\fP, which listens
on multiple ports.  When a request arrives on a specific port,
.Daemon inetd
starts a daemon specific to the port.  For example, FreeBSD supports anonymous
ftp, but most people don't receive enough requests to warrant having the ftp
daemon,
.Daemon ftpd ,
running all the time.  Instead,
.Daemon inetd
starts an
.Daemon ftpd
when a request comes in on port 21.
.P
At startup,
.Daemon inetd
reads a configuration file
.File /etc/inetd.conf
to determine which ports to monitor and what to do when a message comes in.
Here's an excerpt:
.Dx
# $FreeBSD: src/etc/inetd.conf,v 1.58 2002/08/09 17:34:13 gordon Exp $
#
# Internet server configuration database
#
#ftp    stream  tcp     nowait  root    /usr/libexec/lukemftpd  ftpd -l -r
#ftp    stream  tcp     nowait  root    /usr/libexec/ftpd       ftpd -l
#ftp    stream  tcp6    nowait  root    /usr/libexec/ftpd       ftpd -l
#telnet stream  tcp     nowait  root    /usr/libexec/telnetd    telnetd
#telnet stream  tcp6    nowait  root    /usr/libexec/telnetd    telnetd
#shell  stream  tcp     nowait  root    /usr/libexec/rshd       rshd
#shell  stream  tcp6    nowait  root    /usr/libexec/rshd       rshd
#login  stream  tcp     nowait  root    /usr/libexec/rlogind    rlogind
#login  stream  tcp6    nowait  root    /usr/libexec/rlogind    rlogind
#exec   stream  tcp     nowait  root    /usr/libexec/rexecd     rexecd
#shell  stream  tcp6    nowait  root    /usr/libexec/rshd       rshd
.De
This file has the following format:
.Ls B
.LI
The first column is the service on which
.Daemon inetd
should listen.  If it starts with a \f(CW#\fP sign, it's a comment, and
.Daemon inetd
ignores it.  You'll note in this example that all the listed services have been
commented out.  Unless you run the daemon independently of
.Daemon inetd ,
a request for one of these services will be rejected with the message:
.Dx 1
Unable to connect to remote host: Connection refused
.De
.SPUP
.LI
The next three columns determine the nature of the connection, the protocol to
use, and whether
.Daemon inetd
should wait for the process to complete before listening for new connections.
In the example, all the services are TCP, but there are entries both for
\f(CWtcp\fP (the normal TCP protocol for IP Version 4) and \f(CWtcp6\fP (the
same service for IP Version 6).
.LI
.X "NNTP"
.X "Network News Transfer Protocol"
The next column specifies the user as which the function should be performed.
.LI
The next column is the full pathname of the program (almost always a daemon) to
start when a message comes in.  Alternatively, it might be the keyword
\f(CWinternal\fP, which specifies that
.Daemon inetd
should perform the function itself.
.LI
All remaining columns are the parameters to be passed to the daemon.
.Le
Older versions of UNIX ran
.Daemon inetd
as part of the startup procedure.  That isn't always necessary, of course, and
for security reasons the default installation of FreeBSD no longer starts it.
You can change that by adding the following line to your
.File /etc/rc.conf \/:
.Dx
inetd_enable="YES"                      # Run the network daemon dispatcher (YES/NO).
.De
To enable services in
.File /etc/inetd.conf ,
it may be enough to remove the comment from the corresponding line.  This
applies for most the services in the example above.  In some cases, though, you
may have to perform additional steps.  For example,
.Daemon lukemftpd ,
an alternative
.Daemon ftpd ,
and
.Daemon nntpd ,
the \fINetwork News Transfer Protocol\fP, are not part of FreeBSD: they're in
the Ports Collection.  Also,
.Daemon nntpd
is intended to run as user \f(CWusenet\fP, which is not in the base system.
.P
The other daemons are not mentioned in
.File /etc/inetd.conf \/:
.Ls B
.LI
The preferred way to run
.Daemon sshd
is at system startup.  As we'll see, the startup is quite slow, so it's not a
good idea to run it from
.File /etc/inetd.conf ,
though it is possible\(emsee the man page if you really want to.
.LI
.Daemon sftp-server
is the server for
.Command sftp .
It gets started from
.Daemon sshd .
.LI
.Daemon httpd ,
the Apache Web Server, also has quite a long startup phase that makes it
impractical to start it from
.File /etc/inetd.conf .
Note also that
.Daemon httpd
requires a configuration file.  We'll look at that on page
.Sref "\*[web-server]" .
.LI
By contrast, it's perfectly possible to start
.Daemon rsyncd
from
.Daemon inetd .
It's not included in the standard
.File /etc/inetd.conf
file because it's a port.  Yes, so are
.Daemon lukemftpd
and
.Daemon nntpd .
It's just a little inconsistent.  This is the line you need to put in
.File /etc/inetd.conf
to start
.Daemon rsyncd .
.Dx
rsync  stream  tcp     nowait  root    /usr/local/bin/rsync rsync --daemon
.De
The name \fIrsync\fP\/ is not a typo.
.Command rsync
and
.Daemon rsyncd
are the same thing; it's the \f(CW--daemon\fP option that makes
.Command rsync
run as a daemon.
.Le
.ne 3v
.Daemon inetd
doesn't notice alterations to
.File /etc/inetd.conf
automatically.  After modifying the file, you must send it a \f(CWSIGHUP\fP
signal:
.Dx 1
# \f(CBkillall -HUP inetd\fP
.De
You can write \f(CW-1\fP instead of \f(CW-HUP\fP.  This causes
.Daemon inetd
to re-read
.File /etc/inetd.conf .
.P
Instead of starting daemons via
.Daemon inetd ,
you can start them at boot time. 
.Daemon inetd
is convenient for servers that don't get run very often, but if you make
frequent connections, you can save overhead by running the servers continuously.
On the other hand, it's not practical to start
.Daemon rshd ,
.Daemon rlogind ,
.Daemon rexecd
or
.Daemon telnetd
at boot time: they're designed to be started once for each session, and they
exit after the first connection closes.  We'll look at starting the other
daemons in the following sections, along with their configuration.
.H2 "Configuring ftpd"
Normally you'll run
.Daemon ftpd
from
.Daemon inetd ,
as we saw above.  If you want to run it directly, perform the following steps:
.Ls B
.LI
Add the following line in
.File /etc/rc.local \/:
.Dx
echo -n 'starting local daemons:'
# put your local stuff here
\f(CBecho " ftpd" && ftpd -D\fP
.De
The option \f(CW-D\fP tells
.Daemon ftpd
to run as a daemon.  You will possibly want other options as well; see the
discussion below.
.LI
Comment out the
.Command ftp
line in
.File /etc/inetd.conf
by adding a hash mark (\f(CW#\fP) in front of it:
.Dx
\f(CB#\fP ftp   stream  tcp     nowait  root    /usr/libexec/ftpd       ftpd -l
.De
.SPUP
.LI
Either reboot, or cause
.Daemon inetd
to re-read its configuration file:
.Dx
# \f(CBkillall -1 inetd\fP                              \fIsend a SIGHUP\fP\/
.De
If you don't perform this step,
.Daemon inetd
keeps the \fIftp\fP\/ port open, and
.Daemon ftpd
can't run.
.Le
For security reasons, you will probably want to add options such as logging and
anonymous \fIftp\fP.  We'll look at how to do that in the next two sections.
.H3 "anonymous ftp"
.X "anonymous ftp"
.X "ftp, anonymous"
.Pn anonymous-ftp
Anonymous
.Command ftp
gives you a couple of security options:
.Ls B
.LI
It restricts access to the home directory of user
.Command ftp .
From the point of view of the remote user,
.Command ftp \/'s
home directory is the root directory, and he cannot access any files outside
this directory.  Note that this means that you can't use symbolic links outside
the
.Command ftp
directory, either.
.LI
It restricts access to the machine generally: the user doesn't learn any
passwords, so he has no other access to the machine.
.Le
In addition, you can start
.Daemon ftpd
in such a manner that it will allow only anonymous
.Command ftp
connections.
.P
There are a number of preparations for anonymous
.Command ftp \/:
.Ls B
.LI
Decide on a directory for storing anonymous ftp files.  The location will depend
on the amount of data you propose to store there.  By default, it's
.Directory /var/spool/ftp .
.LI
Create a user
\f(CWftp\fP,
with the anonymous
.Directory ftp
directory as the home directory and the shell
.Device null .
Using
.Device null
as the shell
makes it impossible to log in as user
.Command ftp ,
but does not interfere with the use of anonymous
.Command ftp .
.Command ftp
can be a member of group \fIbin\fP\/ or you can create a new group \fIftp\fP\/
by adding the group to
.File /etc/group .
See page \*[adding-user] for more details of adding users, and the man page
\fIgroup(5)\fP\/ for adding groups.
.LI
Create subdirectories
.Directory ~ftp/bin
and
.Directory ~/ftp/pub .
It is also possible to create a directory for incoming data.  By convention its
name is
.Directory ~ftp/incoming .
This is a very bad idea if you're connected to the global Internet: it won't be
long before people start using your system as a server for illicit data.  Only
use this option if you have some other method of stopping unauthorized access.
.P
Set the ownership of the directories like this:
.Dx
dr-xr-xr-x   2 ftp   ftp       512 Feb 28 12:57 bin
drwxrwxrwx   2 ftp   ftp       512 Oct  7 05:55 incoming
drwxrwxr-x  20 ftp   ftp       512 Jun  3 14:03 pub
.De
This enables read access to the
.Directory pub
directory and read-write access to the
.Directory incoming
subdirectory.
.LI
If you have a lot of files that are accessed relatively infrequently, it's
possible you will find people on the Net who copy all the files that they see in
the directory.  Sometimes you'll find multiple connections from one system
copying all the files in parallel, which can cause bandwidth problems.  In some
cases, you might find it more appropriate to distribute the names individually,
and to limit access to reading the directories.  You can do this by setting the
permissions of
.Directory pub
and its subdirectories like this:
.Dx
d--x--x--x  20 ftp   ftp       512 Jun  3 14:03 pub
.De
This allows access to the files, but not to the directory, so the remote user
can't find the names of the files in the directory.
.LI
Copy the following files to
.Directory ~ftp/bin \/:
.Command -n /usr/bin/compress ,
.Command -n /usr/bin/gzip ,
.Command -n /usr/bin/gunzip ,
.Command -n /bin/ls ,
.Command -n /usr/bin/tar
and
.Command -n /usr/bin/uncompress .
The view of anonymous
.Command ftp
users is restricted to the home directory, so all programs that are to be
executed must also be in this directory.
.P
.ne 3v
You can (``hard'') link the files if you want (and if the directory is on the
same file system), but symbolic links will fail, since they contain path names
that do not point to the correct place when running in the anonymous \fIftp\fP\/
environment.
.Le
.H3 "Restricting access and logging"
A number of
.Daemon ftpd
options make it easier to control and monitor
.Command ftp
access:
.Ls B
.LI
.X "syslogd, daemon"
.X "daemon, syslogd"
The \f(CW-l\fP option logs each session, whether successful or not, to
.Daemon syslogd
with the facility \f(CWLOG_FTP\fP.  To enable this logging, your
.File /etc/syslog.conf
should contain a line like
.Dx 1
ftp.*                                           /var/log/ftpd
.De
In addition, the file
.File /var/log/ftpd
must exist.  If it doesn't, create it with:
.Dx
# \f(CBtouch /var/log/ftpd\fP
.De
The \f(CW-l\fP option has two levels: if you specify it once, it logs
connections only.  If you specify it twice, it also lists the files that are
transferred.
.LI
The \f(CW-S\fP option logs all anonymous transfers to the file
.File /var/log/ftpd .
.LI
You can restrict access to \fIonly\fP\/ anonymous \fIftp\fP\/ with the
\f(CW-A\fP option.
.Le
There are a number of other options; see the man page \fIftpd(8)\fP\/ for
further details.
.P
.Pn /etc/shells
In addition to these options, when a real user establishes a connection,
.Daemon ftpd
checks the user's shell.  If it is not listed in
.File /etc/shells ,
.Daemon ftpd
will deny the connection.  This can be useful if you don't want specific users
to access the system: give them a different shell, such as
.Command -n /usr/bin/sh
instead of
.Command -n /bin/sh ,
and ensure that
.Command -n /usr/bin/sh
is not in
.File /etc/shells .
.H4 "Log file format"
The format of the log files is a little unusual.  You'll see things like:
.Dx
Oct 12 16:32:04 freebie ftpd[8691]: ANONYMOUS FTP LOGIN FROM adam.adonai.net, leec@a
donainet
Oct 12 18:33:32 freebie ftpd[9007]: connection from gateway.smith.net.au
Oct 12 18:33:37 freebie ftpd[9007]: ANONYMOUS FTP LOGIN FROM gateway.smith.net.au, m
ike
Oct 12 21:36:28 freebie ftpd[9369]: connection from grisu.bik-gmbh.de
Oct 12 21:36:29 freebie ftpd[9369]: ANONYMOUS FTP LOGIN FROM grisu.bik-gmbh.de, harv
est@
Oct 12 21:36:37 1997!harvest@!grisu.bik-gmbh.de!/pub/cfbsd/README!9228!1
Oct 12 21:37:05 freebie ftpd[9371]: connection from grisu.bik-gmbh.de
Oct 12 21:37:06 freebie ftpd[9371]: ANONYMOUS FTP LOGIN FROM grisu.bik-gmbh.de, harv
est@
Oct 13 09:38:19 freebie ftpd[13514]: connection from 151.197.101.46
Oct 13 09:38:21 freebie ftpd[13514]: ANONYMOUS FTP LOGIN FROM 151.197.101.46, bmc@ho
vercraft.willscreek.com
Oct 13 09:38:58 1997!bmc@hovercraft.willscreek.com!151.197.101.46!/pub/cfbsd/dear-re
viewer!8890!1
Oct 13 09:41:42 1997!bmc@hovercraft.willscreek.com!151.197.101.46!/pub/cfbsd/txt/26-
netdebug.txt.gz!12188!1
Oct 13 09:42:05 1997!bmc@hovercraft.willscreek.com!151.197.101.46!/pub/cfbsd/txt/C-p
ackages.txt.gz!37951!1
Oct 13 09:59:07 freebie ftpd[14117]: connection from 151.197.101.46
Oct 13 09:59:08 freebie ftpd[14117]: ANONYMOUS FTP LOGIN FROM 151.197.101.46, bmc@ho
vercraft.willscreek.com
Oct 13 09:59:24 1997!bmc@hovercraft.willscreek.com!151.197.101.46!/pub/cfbsd/txt/D-b
iblio.txt.gz!1815!1
.De
.ne 3v
This log excerpt shows three kinds of message:
.Ls B
.LI
The messages starting with the text \f(CWconnection from\fP occur when an
\fIftp\fP\/ connection is made.  They don't mean that any permission to access
has been given.  These messages are logged by the \f(CW-l\fP option.
.LI
The \f(CWANONYMOUS FTP LOGIN\fP messages show that somebody has logged in
anonymously.  The name follows, not always in the required username format.
The standard
.Daemon ftpd
does not enforce this requirement; you may find something that does in the
Ports Collection.  These messages are logged by the \f(CW-S\fP option.
.LI
The lines full of \f(CW!\fP marks show files being transferred.  The \f(CW!\fP
marks delimit the fields, which are:
.Ls B
.LI
The year, as an extension of the timestamp.
.LI
The user ID.
.LI
The IP address of the system to which the data is transferred.
.LI
The name of the file transferred.
.LI
The number of bytes transferred.
.Le
.Le
.SPUP
.H2 "Running sshd"
.Pn sshd
Normally you start
.Daemon sshd
from the system configuration file
.File /etc/rc.conf \/:
.Dx
sshd_enable="YES"               # Enable sshd
.De
That's all you need to do for
.Daemon sshd .
You can also start it simply with:
.Dx
# \f(CWsshd\fP
.De
.Pn sshd_config
.Daemon sshd
reads a configuration file
.File /etc/ssh/sshd_config .
Like its companion
.File -n /etc/ssh/ssh_config ,
it contains mainly commented-out lines showing the default values.  Most of them
don't require change, but the following entries may be of interest:
.Ls B
.LI
\f(CWProtocol\fP states which
.Command -n ssh
protocols to use, and in which order.  By default,
.Daemon sshd
tries protocol 2 first, and falls back to protocol 1 if protocol 2 fails.  You
might consider setting it to use only protocol 2.
.LI
When \f(CWPermitRootLogin\fP is set to \f(CWyes\fP, you can log in as
\f(CWroot\fP via
.Command ssh .
Normally it's disabled.
.LI
Set \f(CWPasswordAuthentication\fP to \f(CWno\fP if you want all access to be
via key exchange (see page
.Sref \*[no-password] \&
for more details).
.LI
.Pn sftp-server
If you want to run
.Daemon sftp-server ,
add the following line to
.File /etc/ssh/sshd_config \/:
.Dx 1
Subsystem       sftp    /usr/libexec/sftp-server
.De
It should be present by default.
.Le
.SPUP
.H2 "rsyncd"
.Pn rsyncd
As we've seen,
.Daemon rsyncd
is just another name for
.Command rsync .
You don't need to do any specific configuration to start it: it gets started
from
.Daemon sshd ,
so all you need to do is to ensure that
.Daemon sshd
gets started.
.P
Starting
.Daemon rsyncd
isn't enough, though: it needs configuration.  Create a file
.File /usr/local/etc/rsyncd.conf
with contents something like this:
.Dx
motd file = /usr/local/etc/rsyncd.txt
log file = /var/log/rsyncd.log
transfer logging = true

[groggy]
        path = /home/grog/public_html
        uid = grog
        read only = yes
        list = yes
        comment = Greg's web pages
        hosts allow = 223.147.37.0/24

[tivo]
        path = /var/tivo
        uid = grog
        read only = no
        list = yes
        comment = TiVo staging area
        hosts allow = tivo.example.org
.De
This is the configuration file used in the server examples in Chapter
.Sref "\*[nchclient]" .
It consists of two parts: a \fIglobal\fP\/ part at the beginning, with settings
that apply to all modules, and one or more \fImodule\fP\/ parts describing
files that the server will supply.
.P
The global options here specify the \fImotd file\fP, a file whose contents are
printed when you list modules (the ``be gentle'' message in the examples), and
that transfers should be logged to
.File /var/log/rsyncd.log .
The log output looks something like this:
.Dx
2002/10/24 13:31:49 [16398] send presto.example.org [192.109.197.74] groggy () slash
dot/topicscience.gif 1083
2002/10/24 13:31:49 [16398] send presto.example.org [192.109.197.74] groggy () slash
dot/topicsecurity.gif 3034
2002/10/24 13:31:49 [16398] send presto.example.org [192.109.197.74] groggy () slash
dot/topictv.jpg 951
2002/10/24 13:31:49 [16398] send presto.example.org [192.109.197.74] groggy () slide
\&.pdf 40470
2002/10/24 13:31:49 [16398] send presto.example.org [192.109.197.74] groggy () stock
whip.html 1602
.De
.ne 3v
The next part of the configuration file describes \fImodules\fP, directory
hierarchies that \fIrsyncd\fP\/ makes available.  If you're used to
Microsoft-style configuration files, this will seem relatively familiar.  The
module names are enclosed in square brackets (\f(CW[]\fP), and they don't have
to have any relationship with the name of the directory.  In this case we have
two modules.  Both have a \fIcomment\fP, a descriptive text printed out when you
list the modules, and both allow listing the name of the module (\f(CWlist =
yes\fP).  In addition:
.Ls B
.LI
Module \f(CWgroggy\fP makes available the directory
.Directory /home/grog/public_html ,
my web pages, for read-only access.
.Daemon rsyncd
accesses the module as user \f(CWgrog\fP.  Any host on the 256 address block
starting with 223.147.37.0 can access the data.
.LI
Module \f(CWtivo\fP makes available the directory
.Directory /var/tivo
for read-write access, but only to the host \fItivo.example.org\fP.  Again
.Daemon rsyncd
accesses the data as user \f(CWgrog\fP.
.Le
There are a large number of other options for \fIrsyncd\fP, but this example
shows the most important ones.  See the man page \fIrsyncd.conf(5)\fP\/ for more
information.
.H2 "Setting up a web server"
.X "web server, setting up"
.Pn web-server
.X "apache, daemon"
.X "daemon, apache"
FreeBSD is a system of choice for running web servers, so it's not surprising
that a large number are available.  Probably the most popular is
.Daemon apache ,
which is available in the Ports Collection.  Install with:
.Dx
# \f(CBcd /usr/ports/www/apache13\fP
# \f(CBmake install\fP
.De
In future versions, the name \fIapache13\fP\/ will change.  Apache comes with a
lot of documentation in HTML format (of course), which is installed in
.Directory -n /usr/local/share/doc/apache/manual .
You might find it useful to put a symbolic link to it in your web home
directory:
.Dx
# \f(CBcd /usr/local/www/data\fP
# \f(CBln -s /usr/local/share/doc/apache/manual apachedoc\fP
.De
After this, you can access the documentation at (for example)
\fIhttp://www.example.org/apachedoc/\fP.
.H3 "Configuring apache"
.X "apache, configuring"
The Apache port uses the following directories:
.Ls B
.LI
The configuration files are in the directory hierarchy
.Directory /usr/local/etc/apache .
The port installs prototype configuration files,
but they need to be modified.
.LI
By default, the web pages are in
.Directory /usr/local/www/data .
This is the ``root'' directory for the web pages: the file
.File -n /usr/local/www/data/foo.html
on \fIwww.example.org\fP\/ will have the URL
\fIhttp://www.example.org/foo.html\fP.  You may find it a good idea to change
the directory to the
.Directory /var
file system in a location such as
.Directory /var/www/data .
We'll look at how to do that with the \f(CWDocumentRoot\fP entry in the
configuration file.
.LI
Icons for Apache's own use are stored in
.Directory /usr/local/www/icons .
You can't access these icons by URI, so don't put your own images here.
.LI
CGI scripts are stored in
.Directory /usr/local/www/cgi-bin .
.Le
.H3 "The configuration file"
.X "configuration file, apache"
.X "apache, configuration file"
The \fIapache\fP\/ configuration file is
.File /usr/local/etc/apache/httpd.conf .
Older versions of \fIapache\fP\/ also used the files
.File /usr/local/etc/apache/access.conf
and
.File /usr/local/etc/apache/srm.conf .
The division between these three files was relatively arbitrary, and the current
recommendation is to not use these files, but to put their content in
.File /usr/local/etc/apache/httpd.conf
instead.  See the \fIapache\fP\/ documentation if you need to change the other
files.
.H3 "httpd.conf"
Probably the best way to understand
.File httpd.conf
is to read through it.  It's pretty long and contains a large number of
comments.  Most entries can be left the way there are, so we won't list the
entire file here: instead we'll look at the parameters that may need change.
We'll look at the system-wide features in the following list, and host-related
features in the next section.
.Ls B
.LI
\f(CWServerType\fP states whether you start it from
.Daemon inetd
or standalone (the default).  It's not a good idea to start
.Daemon httpd
from
.Daemon inetd ,
so you should leave this entry unchanged.
.LI
\f(CWServerRoot\fP claims to be the path to the configuration files, but in fact
the files are stored in the subdirectory
.Directory -n etc/apache
of this directory.  You shouldn't need to change it.
.LI
The comments about \f(CWScoreBoardFile\fP suggest that you should check to see
if the system creates one.  Don't bother: FreeBSD doesn't create this file, and
you don't need to worry about it.
.LI
The Keep-Alive extension to HTTP, as defined by the \f(CWHTTP/1.1 draft\fP,
allows persistent connections.  These long-lived HTTP sessions allow multiple
requests to be sent over the same TCP connection, and in some cases have been
shown to result in an almost 50% speedup in latency times for HTML documents
with lots of images.
.LI
The parameters \f(CWMinSpareServers\fP, \f(CWMaxSpareServers\fP,
\f(CWStartServers\fP, \f(CWMaxClients\fP and \f(CWMaxRequestsPerChild\fP are
used for server tuning.  The default values should work initially, but if you
have a lot of Web traffic, you should consider changing them.
.LI
The next area of interest is a large list of \fImodules\fP.  A lot of
\fIapache\fP\/ functionality is optional, and you include it by including a
module.  We'll look at this in more detail below.
.LI
The parameter \f(CWProxyRequests\fP allows Apache to function as a \fIproxy
server\fP.  We'll look at this in more detail below.
.LI
The parameters starting with \f(CWCache\fP apply only to proxy servers, so we'll
look at them below as well.
.LI
The \f(CWListen\fP parameter defines alternate ports on which Apache listens.
.LI
\f(CWDirectoryIndex\fP is a list of names that
.Daemon httpd
recognizes as the main page (``index'') in the directory.  Traditionally it's
.File index.html .
This is the means by which
.Daemon httpd
changes a directory name into an index.  It searches for the names in the order
specified.  For example, if you're using PHP, \f(CWDirectoryIndex\fP gets set to
the string \f(CWindex.php index.php3 index.html\fP, and that's the sequence in
which it looks for a page.
.Le
The file ends with a commented out \f(CWVirtualHost\fP section.  We'll look at
it in detail in the next section, along with a number of parameters that appear
elsewhere in the configuration file, but that relate to virtual hosts.
.H3 "Virtual hosts"
.X "virtual host, web"
.X "web, virtual host"
.Pn virtual-host
.X "example.org"
.X "biguser.com"
Running and maintaining a web server is enough work that you might want to use
the same server to host several sets of web pages, for example for a number of
different organizations.  \fIapache\fP\/ calls this feature \fIvirtual hosts\fP,
and it offers a lot of support for them.  Theoretically, all your hosts can be
virtual, but the configuration file still contains additional information for a
``main'' server, also called a ``default'' server.  The default configuration
does not have any virtual servers at all, though it does contain configuration
information.
.P
There's a good reason to keep the ``main'' server information: it serves as
defaults for all virtual hosts, which can make the job of adding a virtual host
a lot easier.
.P
Consider your setup at \fIexample.org\fP\/: you may run your own web pages and
also a set of pages for \fIbiguser.com\fP\/ (see page \*[ISP-routing]).  To do
this, you add the following section to
.File /usr/local/etc/apache/httpd.conf \/:
.Dx
<VirtualHost *>
ServerAdmin grog@example.org
DocumentRoot /usr/local/www/biguser             \fIwhere we put the web pages\fP\/
ServerName www.biguser.com                      \fIthe name that the server will claim to be\fP\/
ServerAlias biguser.com                         \fIalternative server name\fP\/
ErrorLog /var/log/biguser/error_log
TransferLog /var/log/biguser/access_log
Options +FollowSymLinks
Options +SymLinksIfOwnerMatch
</VirtualHost>
.De
If you look at the default configuration file, you'll find most of these
parameters, but not in the context of a \f(CWVirtualHost\fP definition.  They
are the corresponding parameters for the ``main'' web server.  They have the
same meaning, so we'll look at them here.
.Ls B
.LI
\f(CWServerAdmin\fP is the mail ID of the system administrator.  For the main
server, it's set to \f(CWyou@your.address\fP, which obviously needs to be
changed.  You don't necessarily need a \f(CWServerAdmin\fP for each virtual
domain; that depends on how you run the system.
.LI
\f(CWDocumentRoot\fP is the name of the directory that will become the root of
the web page hierarchy that the server provides.  By default, for the main server
it's
.Directory /usr/local/www/data ,
which is not really a very good place for data that changes frequently.  You
might prefer to change this to
.Directory /var/www ,
as some Linux distributions do.  This is one parameter that you must supply for
each virtual domain: otherwise the domain would have the same content as the
main server.  In this case, it's the location of the files in
\fIhttp://www.example.com/\fP.
.LI
Next you can put information about individual data directories.  The default
server first supplies defaults for all directories:
.Dx
<Directory />
    Options FollowSymLinks
    AllowOverride None
</Directory>
.De
The \f(CW/\fP in the first line indicates the local directory to which these
settings should apply.  For once, this is really the root directory and not
\f(CWDocumentRoot\fP: they're system-wide defaults, and though you don't have to
worry about \fIapache\fP\/ playing around in your root file system, that's the
only directory of which all other directories are guaranteed to be a
subdirectory.  The \f(CWOptions\fP directive ensures that the server can follow
symbolic links belonging to the owner.  Without this option, symbolic links
would not work.  We'll look at the \f(CWAllowOverride\fP directive in the
discussion of the
.File .htaccess
file below.
.P
There's a separate entry for the data hierarchy:
.Dx
<Directory "/usr/local/www/data">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Order allow,deny
    Allow from all
</Directory>
.De
In this case, we have two additional options:
.Ls B
.LI
\f(CWIndexes\fP allows
.Daemon httpd
to display the contents of a directory if no index file, with a name defined in
\f(CWDirectoryIndex\fP, is present.  Without this option, if there is no index
file present, you will not be able to access the directory at all.
.LI
\f(CWMultiViews\fP allows content-based multiviews, which we don't discuss here.
.Le
Note that if you change the name of the default data directory, you should also
change the name on the \f(CWDirectory\fP invocation.
.P
We'll look at the remaining entries in more detail when we see them again in the
discussion of the
.File .htaccess
file.
.LI
.X "www.example.org"
Normally you should set \f(CWServerName\fP.  For example,
\fIwww.example.org\fP\/ is a CNAME for \fIfreebie.example.org\fP\/ (see page
\*[DNS-CNAME]), and if you don't set this value, clients will access
\fIwww.example.org\fP, but the server will return the name
\fIfreebie.example.org\fP.
.LI
.Command httpd
can maintain two log files, an \fIaccess log\fP\/ and an \fIerror log\fP.  We'll
look at them in the next section.  It's a good idea to keep separate log files
for each domain.
.LI
You should have a default \f(CWVirtualHost\fP entry.  People can get quite
confused if they select an invalid name (for example,
\fIhttp://www.big-user.com\fP\/) and get the (default) web page for
\fIhttp://www.example.org\fP.  The default page should not match any other host.
Instead, it should indicate that the specified domain name is invalid.
.LI
For the same reason, it's a good idea to have a \f(CWServerAlias\fP entry for
the same domain name without initial \fIwww\fP.  The entry in the example above
serves the same pages for \fIwww.biguser.com\fP\/ and \fIbiguser.com\fP.
.LI
The directive \f(CWOptions +SymLinksIfOwnerMatch\fP limits following symbolic
links to those links that belong to the same owner as the link.  Normally the
\f(CWOptions\fP directive specifies all the options: it doesn't merge the
default options.  The \f(CW+\fP sign indicates that the option specified should
be added to the defaults.
.Le
.X "apache, daemon"
.X "daemon, apache"
After restarting
.Daemon apache ,
it handles any requests to \fIwww.biguser.com\fP\/ with these parameters.  If
you don't define a virtual host, the server will access the main web pages
(defined by the main \f(CWDocumentRoot\fP in entry
.File /usr/local/etc/apache/access.conf \/).
.H3 "Log file format"
.Daemon httpd
logs accesses and errors to the files you specify.  It's worth understanding
what's inside them.  The following example shows five log entries.  Normally
each entry is all on a very long line.
.Dx
p50859b17.dip.t-dialin.net - -                  \fIname of system, more\fP\/
[01/Nov/2002:07:06:12 +1030]                    \fIdate of access\fP\/
"GET /Images/yaoipower.jpeg HTTP/1.1"           \fIHTML command\fP\/
200                                             \fIstatus (OK)\fP\/
19365                                           \fIlength of data transfer\fP\/

aceproxy3.acenet.net.au - -
[01/Nov/2002:07:35:34 +1030]
"GET /Images/randomgal.big.jpeg HTTP/1.0"
304 -                                           \fIstatus (cached)\fP\/

218.24.24.27 - -                                \fIsystem without reverse DNS\fP\/
[01/Nov/2002:07:39:55 +1030]
"GET /scripts/root.exe?/c+dir HTTP/1.0"         \fIlooking for an invalid file\fP\/
404 284                                         \fIstatus (not found)\fP\/

218.24.24.27 - -
[01/Nov/2002:07:39:56 +1030]
"GET /MSADC/root.exe?/c+dir HTTP/1.0" 404 282

218.24.24.27 - -
[01/Nov/2002:07:39:56 +1030]
"GET /c/winnt/system32/cmd.exe?/c+dir HTTP/1.0" 404 292

218.24.24.27 - -
[01/Nov/2002:07:40:00 +1030]
"GET /_vti_bin/..%255c../..%255c../..%255c../winnt/system32/cmd.exe?/c+dir HTTP/1.0"
404 323
.De
.ne 3v
The fields in the log file are separated by blanks, so empty entries are
replaced by a \f(CW-\fP character.  In this example, the second and third
fields are always empty.  They're used for identity checks and authorization.
.P
To get the names of the clients, you need to specify the \f(CWHostnameLookups
on\fP directive.  This requires a DNS lookup for every access, which can be
relatively slow.
.P
Although we specified hostname lookups, the last four entries don't have any
name: the system doesn't have reverse DNS.  They come from a Microsoft machine
infected with the \fINimda\fP\/ virus and show an attempt to break into the web
server.  There's not much you can do about this virus; it will probably be years
before it goes away.  Apart from nuisance value, it has never posed any threat
to \fIapache\fP\/ servers.
.H3 "Access control"
.Pn htaccess
Sometimes you want to restrict access to a web server, either for specific
directories or for the web site as a whole.  \fIapache\fP\/ has a number of
options to limit access.  One possibility is to set options in
.File /usr/local/etc/apache/httpd.conf
to cover an individual host, but this is seldom useful.  It's more likely that
you will want to restrict access to specific directories, and it's easier to do
that in the file
.File .htaccess
in the same directory.
.P
For \fIapache\fP\/ to even look at
.File .htaccess ,
you need to change the configuration file, however: by default, it disables the
use of
.File .htaccess
altogether, as we saw above:
.Dx
<Directory />
    Options FollowSymLinks
    AllowOverride None
</Directory>
.De
For it to work, you'll have to change the \f(CWAllowOverride\fP parameter to
some other value.  There are five categories of entries that you can allow in
.File .htaccess
files:
.Ls N
.LI
\f(CWAuthConfig\fP
allows
.File .htaccess
to include authorization directives.
.LI
\f(CWFileInfo\fP allows the use of directives controlling document types.
.LI
\f(CWIndexes\fP allows the use of directives controlling directory indexing.
.LI
\f(CWLimit\fP allows the use of directives controlling host access.
.LI
\f(CWOptions\fP
allows the use of directives controlling specific directory features.
.Le
You can find more details in
.File -n /usr/local/share/doc/apache/manual/mod/core.html .
.P
The most common use of the
.File .htaccess
is to require that users authorize themselves before accessing a directory.  In
this case, the browser will pop up a window like this:
.PIC images/authprompt.ps 3i
.ne 3v
To achieve this, add something like this to your
.File .htaccess
file:
.Dx
AuthType Basic
AuthName grog
AuthDBUserFile /usr/local/etc/apache/passwd
Require valid-user
.De
This method is similar to normal login authentication.  You need a password
file, which you can create and update with
.Command dbmmanage \/:
.Dx
# \f(CBdbmmanage /usr/local/etc/apache/passwd adduser grog\fP
New password:
Re-type new password:
User grog added with password encrypted to OzREW8Xx5hUAs using crypt
# \f(CBdbmmanage /usr/local/etc/apache/passwd adduser guest\fP
New password:
Re-type new password:
User guest added with password encrypted to hFCYwd23ftHE6 using crypt
.De
This adds passwords for users \f(CWgrog\fP and \f(CWguest\fP.  The
\f(CWAuthName\fP suggests a name to authenticate, but \f(CWRequire valid-user\fP
states that it can be any user.  Even if you don't care which user logs in, you
need to specify an \f(CWAuthName\fP line.  If you \fIdo\fP\/ insist that only
user \f(CWgrog\fP can log in, you can write:
.Dx
Require user grog
.De
This will fail the authentication for any other user.  You can also specify a
list of users or groups.  For example, you might add the following line:
.Dx
AuthGroupFile /usr/local/etc/apache/group
Require group bigshots
.De
.File /usr/local/etc/apache/group
might then contain:
.Dx
bigshots:  davidb davidp gordon grog liz malcolm
.De
This will allow any of the users specified on this line to access the directory.
.H3 "Apache modules"
\fIapache\fP\/ offers a large quantity of optional functionality, which it
provides in the form of dynamically loadable \fImodules\fP.  We've seen above
that there are two long lists of module names in
.File /usr/local/etc/apache/httpd.conf ;
the first starts with \f(CWLoadModule\fP and tells
.Daemon httpd
which dynamic modules to load.  The order is important; don't change it.
.H3 "Proxy web servers"
.X "proxy web server"
.X "web server, proxy"
.X "natd, daemon"
.X "daemon, natd"
.X "caching, web"
.X "web, caching"
Apache is capable of operating as a proxy server: it can accept requests
for web pages of other systems.   This can be an alternative to a general IP
aliasing package such as
.Daemon natd
(see page \*[ip-aliasing]) if you need it
only for web access.  It's also useful in conjunction with \fIcaching\fP.
.P
Unfortunately, by default the current version of Apache does not support proxy
servers.  You need to rebuild the package manually after enabling it in the
configuration file.  See the file \fIINSTALL\fP\/ in the port build directory
for more details.  This file will be present after building Apache from source,
and it will have a name like
.File -n /usr/ports/www/apache13/work/apache_1.3.23/src/INSTALL .
In addition to reinstalling the server with code for proxy serving, you must set
\f(CWProxyRequests\fP to \f(CWOn\fP to enable the proxy server.
.H3 "Caching"
.X "caching, web"
.X "web, caching"
One reason for enabling the proxy server is to \fIcache\fP\/ data requests.
Caching keeps pages requested through the proxy and presents them again if they
are requested again.  This is particularly useful if the server serves a large
number of people who communicate with each other and are thus likely to request
many of the same pages.
.P
The \f(CWCache\fP parameters are commented out by default.  If you uncomment
them, you should uncomment them all execpt possibly \f(CWNoCache\fP.  When
setting these values, change the name of the directory \f(CWCacheRoot\fP.  A
good name might be
.Directory /usr/local/www/proxy .
.H3 "Running apache"
.X "apache, running"
When you install
.Daemon apache ,
it installs the file
.File /usr/local/etc/rc.d/apache.sh ,
which automatically starts
.Daemon apache
at boot time.  If you don't want to start it automatically, remove this file.
You can start and stop \fIapache\fP\/ manually with the
.Command apachectl
program, which takes a command parameter:
.Dx
# \f(CBapachectl start                          \fIstart httpd\f(CW
# \f(CBapachectl stop                           \fIstop httpd\f(CW
# \f(CBapachectl restart                        \fIrestart httpd,  or start if not running\f(CW
# \f(CBapachectl graceful                       \fIrestart httpd ``gracefully,'' or start if not running\f(CW
# \f(CBapachectl configtest                     \fIdo a configuration syntax test\f(CW
.De
The difference between a normal and a ``graceful'' restart is that the graceful
restart waits for existing connections to complete before restarting the
individual server processes.  Unless you're in a big hurry, use the graceful
restart.
.H2 "NFS server"
.Pn NFS-server
.X "NFS, server"
A number of processes are needed to provide NFS server functionality:
.Ls B
.LI
.X "NFS, daemon"
The \fINFS daemon\fP,
.Daemon nfsd ,
is the basic NFS server.
.LI
The \fImount daemon\fP,
.Daemon mountd ,
processes mount requests from clients.
.LI
.X "NFS, lock daemon"
.X "rpc.lockd, daemon"
.X "daemon, rpc.lockd"
The \fINFS lock daemon\fP,
.Daemon rpc.lockd ,
processes lock requests for NFS file systems.  There are still a lot of problems
with this function on all platforms.  It's best to avoid it if you can.
.LI
.X "status monitoring daemon"
.X "daemon, status monitoring"
The \fIstatus monitoring daemon\fP,
.Daemon rpc.statd ,
provides a status monitoring service.
.Le
.ne 5v
In addition:
.Ls B
.LI
.X "remote procedure call"
.X "RPC"
.X "rpcbind, daemon"
.X "daemon, rpcbind"
Since NFS uses \fIRemote procedure calls\fP\/ (\fIRPC\fP\/), the
.Daemon rpcbind
daemon must be running.
.Daemon rpcbind
is not part of NFS, but it is required to map RPC port numbers to IP service
numbers.  In previous releases of FreeBSD, this function was performed by the
.Daemon portmap
daemon.  It has not been renamed, it has been replaced.
.LI
The server needs a file
.File /etc/exports
to define which file systems to export and how to export them.  We'll look at
this in the next section.
.Le
.P
.H3 "/etc/exports"
.Pn /etc/exports
A number of security implications are associated with NFS.  Without some kind of
authentication, you could mount any file system on the Internet.
.P
NFS was developed at a time when users were relatively trusted.  As a result,
the security precautions are not overly sophisticated.
.File /etc/exports
describes one file system per line.  The format is:
.Dx 1
\fIfile system\fP\/     \fIoptions\fP\/         \fIsystems\fP\/
.De
.ne 3v
\fIsystems\fP\/ is a list of systems allowed to mount the file system.  The only
required field is the name of the file system, but if you're on the Internet,
you should at least limit the number of systems that can mount your file
systems.  By default any system on the Net can mount your file systems.
.P
There are a number of options.  Here are the more important ones:
.Ls B
.LI
The \f(CW-maproot\fP option describes how to treat \f(CWroot\fP.  By default,
\f(CWroot\fP does not have special privileges on the remote system.  Instead,
NFS changes the user ID to user \f(CWnobody\fP, which is user 65534 (or -2).
You can change this with the \f(CW-maproot\fP option.  For example, to map
\f(CWroot\fP to the real \f(CWroot\fP user for a specific file system, you would
add \f(CW-maproot=0\fP to the line describing the file system.
.LI
The \f(CW-mapall\fP option maps the user IDs of other users.  This is relatively
uncommon.  See the man page \fIexports(5)\fP\/ for more details.
.LI
The \f(CW-ro\fP option restricts access to read-only.
.LI
The \f(CW-network\fP\/ option restricts the access to systems on the specified
network.
.LI
The \f(CW-alldirs\fP option allows remote clients to mount any directory in the
file system directly.  Without this option, remote clients can only mount the
root directory of the exported file system.  We'll see an example where
\f(CW-alldirs\fP can be of use during the discussion of diskless booting on page
.Sref \*[netboot] .
.Le
If you come from a System V background, you'll notice that the mechanism is
different.
.File /etc/exports
corresponds in concept roughly to System V's
.File -n /etc/dfs/dfstab
file, except that the \fIshare\fP\/ statement does not exist.
.H4 "Updating /etc/exports"
.X "mountd, daemon"
.X "daemon, mountd"
To grant access to a file system, it's not enough to change the contents of
.File /etc/exports \/:
you also need to tell
.Daemon mountd
that you have done so.  You do this by the relatively common method of sending a
hangup signal (\f(CWSIGHUP\fP) to
.Daemon mountd :
.Dx
# \f(CBkillall -HUP mountd\fP
.De
.X "presto"
A typical
.File /etc/exports
for \fIpresto\fP\/ might be:
.Dx 2
/       -maproot=0               presto bumble wait gw
/usr    -maproot=0   -alldirs    -network 223.147.37.0
.De
.ne 4v
This allows root access to both file systems.  Only the trusted systems
\fIpresto\fP, \fIbumble\fP, \fIwait\fP\/ and \fIgw\fP\/ are allowed to access
the root file system, whereas any system on the local network may access
.Directory -n /usr .
Remote systems may mount any directory on the
.Directory -n /usr
file system directly.
.H2 "Samba"
.X "SMB"
.X "Server Message Block"
.X "LanManager"
.X "NetBIOS"
.X "IPX"
.X "Common Internet File System"
.X "CIFS"
BSD UNIX and the Internet grew up together, but it took other vendors a long
time to accept the Internet Protocols.  In that time, a number of other
protocols arose.  We've already mentioned X.25 and SNA, currently both not
supported by FreeBSD.  The protocols that grew up in the DOS world are more
widespread, in particular Novell's \fIIPX\fP\/ and Microsoft's \fICommon
Internet File System\fP, or \fICIFS\fP.  CIFS was previously known as
\fISMB\fP\/ (\fIServer Message Block\fP\/).
.P
.X "samba"
IPX support is relatively rudimentary.  FreeBSD includes an IPX routing daemon,
.Daemon IPXrouted .
See the man page \fIIPXrouted(8)\fP\/ for further information.  IPX is going out
of use, so it's unlikely that support for it will improve.  By contrast,
Microsoft's CIFS is still alive and kicking.  In the rest of this chapter we'll
look at the standard implementation, \fISamba\fP.  This chapter describes only
the FreeBSD side of the setup; you'll need to follow the Microsoft documentation
for setting up the Microsoft side of the network.
.P
\fISamba\fP\/ is a collection of software components that implement the SMB
protocol over TCP/IP.  You can use it to interface with all current Microsoft
environments.  It is part of the Ports Collection, in
.Directory /usr/ports/net/samba .
.X "Ts, Jay"
.X "Eckstein, Robert"
.X "Collier-Brown, David"
You can get more information from \fIUsing Samba\fP, by Jay Ts, Robert Eckstein
and David Collier-Brown.  At
.URI http://samba.org/
you can get even more information, including support and a mailing list.
.P
\fISamba\fP\/ includes a number of programs, most of which we don't touch on
here.  The ones we look at are:
.ig

-rwxr-xr-x   1 root  wheel   562322 Mar  6 14:26 smbspool
-rwxr-xr-x   1 root  wheel   634074 Mar  6 14:26 smbclient
-rwxr-xr-x   1 root  wheel   383436 Mar  6 14:26 testparm
-rwxr-xr-x   1 root  wheel   383449 Mar  6 14:26 make_smbcodepage
---x--x--x   1 root  wheel  1087253 Mar  6 14:26 smbpasswd
-rwxr-xr-x   1 root  wheel   407856 Mar  6 14:26 testprns
-rwxr-xr-x   1 root  wheel   414961 Mar  6 14:26 smbstatus
-rwxr-xr-x   1 root  wheel   414240 Mar  6 14:26 smbcontrol
-rwxr-xr-x   1 root  wheel    30980 Mar  6 14:26 tdbbackup
-rwxr-xr-x   1 root  wheel   385235 Mar  6 14:26 make_printerdef
-rwxr-xr-x   1 root  wheel  1155166 Mar  6 14:26 rpcclient
-rwxr-xr-x   1 root  wheel   563811 Mar  6 14:26 nmblookup
-rwxr-xr-x   1 root  wheel  1124673 Mar  6 14:26 smbcacls
-rwxr-xr-x   1 root  wheel   380933 Mar  6 14:26 make_unicodemap
-rwxr-xr-x   1 root  wheel     4899 Mar  6 14:26 smbtar
-rwxr-xr-x   1 root  wheel     4382 Mar  6 14:26 findsmb
-r-xr-xr-x   1 root  wheel      205 Mar  6 14:26 make_smbpasswd
..
.Ls B
.LI
.Daemon smbd ,
a daemon that provides file and print services to SMB clients.
.LI
.Daemon nmbd ,
which provides name services for NetBIOS.
.LI
.Command smbpasswd ,
which sets up network passwords for \fISamba\fP.
.LI
.Command smbclient ,
a simple ftp-like client that is useful for accessing SMB shared files on other
servers, such as Windows for Workgroups.  You can also use it to allow a UNIX
box to print to a printer attached to any SMB server.
.LI
.X "samba"
.Command testparm,
which tests the \fISamba\fP\/ configuration file,
.File smb.conf .
.LI
.Command smbstatus
tells you who is using the
.Daemon smbd
daemon.
.Le
.ne 8v
.H3 "Installing the Samba software"
.X "samba, installing"
Install \fISamba\fP\/ from the port:
.Dx
# \f(CBcd /usr/ports/net/samba\fP
# \f(CBmake install\fP
.De
This operation installs the Samba binaries in
.Directory /usr/local/bin ,
the standard location for additional binaries on a BSD system, and the daemons
.Daemon smbd
and
.Daemon nmbd
in
.Directory /usr/local/sbin . 
These locations are appropriate for FreeBSD, but they are \fInot\fP\/ the
locations that the Samba documentation recommends.  It also installs the man
pages in
.Directory /usr/local/man ,
where the
.Command man
program can find them.  Finally, it installs a sample configuration file in
.File -n  /usr/local/etc/smb.conf.default .
We'll look at how to configure Samba below.
.P
There are a number of security implications for the server, since it handles
sensitive data.  To maintain an adequate security level,
.Ls B
.LI
Ensure that the software is readable by all and writeable only by \f(CWroot\fP.
.Daemon smbd
should be executable by all.  Don't make it \fIsetuid\fP.  If an individual user
runs it, it runs with their permissions.
.LI
Put server log files in a directory readable and writable only by \f(CWroot\fP,
since they may contain sensitive information.
.LI
Ensure that the
.Daemon smbd
configuration file in
.File /usr/local/etc/smb.conf
is secured so that only \f(CWroot\fP can change it.
.P
The Samba documentation recommends setting the directory readable and writeable
only by \f(CWroot\fP.  Depending on what other configuration files you have in
.File /etc/local/etc ,
this could cause problems.
.Le
.SPUP
.H3 "smbd and nmbd: the Samba daemons"
The main component of Samba is
.Daemon smbd ,
the SMB daemon.  In addition, you need the Samba name daemon,
.Daemon nmbd ,
which supplies NetBIOS name services for Samba.
.Daemon smbd
requires a configuration file, which we'll look at below, while you don't
normally need one for
.Daemon nmbd .
By default,
.Daemon nmbd
maps DNS host names (without the domain part) to NetBIOS names, though it can
perform other functions if you need them.  In this chapter we'll assume the
default behaviour.  See the man page \fInmbd(8)\fP\/ for other possibilities.
.P
.ne 5v
You have two choices of how to run
.Daemon smbd
and
.Daemon nmbd \/:
you can start them at boot time from
.File /usr/local/etc/rc.d/samba.sh ,
or you can let
.Daemon inetd
start them.  The Samba team recommends starting them at boot time
.P
When you install Samba from the Ports Collection, it installs a file
.File /usr/local/etc/rc.d/samba.sh.sample .
You just need to rename it to
.File /usr/local/etc/rc.d/samba.sh .
As the name suggests, it's a shell script.  You can modify it if necessary, but
it's usually not necessary.
.P
The man page for
.Daemon smbd
gives a number of parameters to specify the configuration file and the log file.
As long as you stick to the specified file names, you shouldn't need to change
anything: by default,
.Daemon smbd
looks for the configuration file at
.File /usr/local/etc/smb.conf ,
and this file contains the names of the other files.
.H4 "Running the daemons from inetd"
To run the daemons from
.Daemon inetd ,
.Ls B
.LI
Edit
.File /etc/inetd.conf . 
You should find the following two lines towards the bottom of the file with a
\f(CW#\fP in front.  Remove the \f(CW#\fP to show the lines as they are here.
If your
.File /etc/inetd.conf
doesn't contain these lines, add them.
.Dx
netbios-ssn stream  tcp  nowait          root    /usr/local/sbin/smbd    smbd
netbios-ns  dgram   udp  wait            root    /usr/local/sbin/nmbd    nmbd
swat        stream  tcp  nowait/400      root    /usr/local/sbin/swat    swat
.De
.Command swat
is an administration tool that we don't discuss here.
.LI
Either reboot, or send a HUP signal to cause
.Daemon inetd
to re-read its configuration file:
.Dx
# \f(CBkillall -1 inetd\fP                              \fIsend a SIGHUP\fP\/
.De
.SPUP
.Le
.SPUP
.H3 "The configuration file"
.X "samba, configuration file"
.X "configuration file, samba"
The Samba configuration file describes the services that the daemon offers.
The port installs a sample configuration file in
.File -n /usr/local/etc/smb.conf.default .
You can use it as the basis of your own configuration file, which must be called
.File /usr/local/etc/smb.conf \/:
simply copy the file, and then edit it as described below.
.P
The configuration file is divided into sections identified by a label in
brackets.  Most labels correspond to a service, but there are also three special
labels: \f(CW[global]\fP, \f(CW[homes]\fP and \f(CW[printers]\fP, all of which
are optional.  We look at them in the following sections.
.H4 "The [global] section"
As the name suggests, the \f(CW[global]\fP section defines parameters that
either apply to the server as a whole, or that are defaults for the other
services.  The interesting ones for us are:
.Ls B
.LI
The \f(CWworkgroup\fP parameter defines the Microsoft workgroup to which this
server belongs.  Set it to match the Microsoft environment.  In these examples,
we'll assume:
.Dx 1
   workgroup = EXAMPLE
.De
.SPUP
.LI
The \f(CWprinting\fP entry specifies what kind of printer support \fISamba\fP\/
provides.  Current versions of \fISamba\fP\/ support CUPS.  If you are using
CUPS (not described in this book), you don't need to do anything.  Otherwise set:
.Dx
   printcap name = /etc/printcap
   printing = bsd
.De
.SPUP
.LI
\f(CWguest account\fP is the account (in UNIX terms: user ID) to use if no
password is supplied.  You probably want to define a guest account, since many
Microsoft clients don't use user IDs.  Ensure that the privileges are set
appropriately.  Alternatively, alter the parameter to point to an existing user.
.LI
Modern versions of Microsoft use a simple form of password encryption; older
versions used none.  Currently, \fISamba\fP\/ defaults to no encryption.  Set
\f(CWencrypt passwords\fP to \f(CWyes\fP.
.LI
Microsoft uses its own version of host name resolution, which doesn't involve
DNS.  Optionally, \fISamba\fP\/ will map Microsoft names to DNS.  To enable this
option, set \f(CWdns proxy\fP to \f(CWyes\fP.
.LI
By default, the log file is specified as
.File -n /var/log/log.%m .
The text \f(CW%m\fP is replaced by the name of the remote machine, so you get
one log file per machine.  Unfortunately, the name doesn't make it clear that
this is a \fISamba\fP\/ log file.  It's better to change this entry to:
.Dx
   log file = /var/log/samba.log.%m
.De
.SPUP
.LI
\f(CWsocket options\fP is hardly mentioned in the documentation, but it's very
important: many Microsoft implementations of TCP/IP are inefficient and
establish a new TCP connection more often than necessary.  Select the socket
options \f(CWTCP_NODELAY\fP and \f(CWIPTOS_LOWDELAY\fP, which can speed up the
response time of such applications by over 95%.
.De
.SPUP
.Le
.H4 "The [homes] section"
The \f(CW[homes]\fP section allows clients to connect to their home directories
without needing an entry in the configuration file.  If this section is present,
and an incoming request specifies a service that is not defined in the
configuration file, Samba checks if it matches a user ID.  If it does, and if
the specified password is correct, Samba creates a service that supplies the
user's home directory.
.P
.ne 5v
The following options are of interest in the \f(CW[homes]\fP section:
.Ls B
.LI
\f(CWwriteable\fP can be \f(CWyes\fP or \f(CWno\fP, and specifies whether the
user is allowed to write to the directory.
.LI
\f(CWcreate mode\fP specifies the permission bits (in octal) to set for files
that are created.
.LI
\f(CWpublic\fP specifies whether other users are allowed access to this
directory.  In combination with a \f(CWguest\fP user, this can be a serious
security liability.
.Le
.H4 "The [printers] section"
The \f(CW[printers]\fP section describes printing services.  It doesn't need the
names of the printers: if it doesn't find the specified service, either in the
configuration file or in the \f(CW[homes]\fP section, if it exists, it looks for
them in the
.File /etc/printcap
file.
.P
The \fISamba\fP\/ documentation claims that \fISamba\fP\/ can recognize BSD
printing system automatically, but this is not always correct.  Ensure that you
have the following entries:
.Dx
   printing = bsd                               \fIin the \f(CW[global]\fP section\fCW
   print command = lpr -r -P'%p' '%s'           \fIin the \f(CW[printers]\fP section\fCW
.De
Note the \f(CWprintable\fP option in the \f(CW[printers]\fP section: this is the
option that distinguishes between printers (``\f(CWyes\fP'') and file shares
(``\f(CWno\fP'').
.H4 "Other sections: service descriptions"
Samba takes any section name except for \f(CW[global]\fP, \f(CW[homes]\fP or
\f(CW[printers]\fP as the definition of a service.  A typical example might be:
.Dx
[ftp]
  comment = ftp server file area
  path = /var/spool/ftp/pub
  read only = yes
  public = yes
  write list = grog
.De
This entry defines access to the anonymous \fIftp\fP\/ section.  Anybody can
read it, but only user \f(CWgrog\fP can write to it.
.H3 "Setting passwords"
\fISamba\fP\/ uses a separate password file,
.File /usr/local/private/secrets.tdb .
To set up users, use the
.Command smbpasswd
command, which copies the information from the system password file:
.Dx
# \f(CBsmbpasswd -a grog\fP
New SMB password:
Retype new SMB password:                        \fIas usual, no echo\fP\/
Password changed for user grog.
.De
.SPUP
.H3 "Testing the installation"
.X "samba, testing"
Once you have performed the steps described above, you can proceed to test the
installation.  First, run
.Command testparm
to check the correctness of the configuration file:
.Dx
$ \f(CBtestparm\fP
Load smb config files from /usr/local/etc/smb.conf
Processing section "[homes]"
Processing section "[printers]"
Processing section "[ftp]"
Processing section "[src]"
Processing section "[grog]"
Loaded services file OK.
Press enter to see a dump of your service definitions           \f(BIPress \fBEnter\f(CW

Global parameters:
\fI\&lots of information which could be of use in debugging\fP\/

[homes]
        comment = Home Directories
        read only = No

[printers]
        comment = All Printers
        path = /var/spool/samba
        guest ok = Yes
        printable = Yes
        browseable = No

[ftp]
        comment = ftp server file area
        path = /var/spool/ftp/pub
        write list = grog
        guest ok = Yes

[grog]
        path = /home/grog
        valid users = grog
        read only = No
.De
.ne 3v
As you see,
.Command testparm
spells out all the parameters that have been created, whether explicitly or by
default.  If you run into problems, this is the first place to which to return.
.P
Next, check that you can log inwith
.Command smbclient .
If you're running the servers as daemons, start them now.  If you're starting
them from
.Daemon inetd ,
you don't need to do anything.
.Dx
$ \f(CBsmbclient -L freebie -U grog\fP
added interface ip=223.147.37.1 bcast=223.147.37.255 nmask=255.255.255.0
Password:                       \fIas usual, no echo\fP\/
Domain=[EXAMPLE] OS=[Unix] Server=[Samba 2.2.7a]

        Sharename      Type      Comment
        ---------      ----      -------
        homes          Disk      Home Directories
        ftp            Disk      ftp server file area
        grog           Disk     
        IPC$           IPC       IPC Service (Samba Server)
        ADMIN$         Disk      IPC Service (Samba Server)

        Server               Comment
        ---------            -------
        FREEBIE              Samba Server
        PRESTO               Samba Server

        Workgroup            Master
        ---------            -------
        EXAMPLE              PRESTO
.De
.X "freebie.example.org"
.Pn multibackslash
If you get this far, your password authentication is working.  Finally, try to
access the shares.  Samba services are specified in Microsoft format:
\f(CW\e\e\fP\fIsystem\fP\/\f(CW\e\fP\fIservice\fP.  To make this worse, UNIX
interprets the \f(CW\e\fP character specially, so you would need to repeat the
character.  For example, to access the \f(CWftp\fP service on \fIfreebie\fP, you
would have to enter \f(CW\e\e\e\efreebie\e\eftp\fP.  Fortunately,
.Command smbclient
understands UNIX-like names, so you can write \f(CW//freebie/ftp\fP instead.
.P
.ne 5v
To test, start
.Command smbclient
from another system:
.br
.ne 1.5i
.Dx
$ \f(CBsmbclient //freebie/ftp -U grog\fP
added interface ip=223.147.37.1 bcast=223.147.37.255 nmask=255.255.255.0
Password:                       \fIas usual, no echo\fP\/
Domain=[EXAMPLE] OS=[Unix] Server=[Samba 2.2.7a]
smb: \e> ls
  .                                  DR        0  Wed Jan 29 12:06:29 2003
  ..                                  D        0  Sat Oct 26 10:36:29 2002
  instant-workstation-1.0.tar.gz            9952  Mon Mar 19 11:49:01 2001
  xtset-1.0.tar.gz                          4239  Mon Aug  5 16:44:14 2002
  gpart-0.1h.tbz.tgz                       27112  Tue Aug 27 10:07:59 2002
.De
If you get this far, Samba is working.  The next step is to attach to the
services from the Microsoft machines.  That's not a topic for this book.  Note,
however, that Samba only works with TCP/IP transport, not with NetBEUI.
.H3 "Displaying Samba status"
.X "samba, displaying status"
You can display the status of Samba connections with
.Command smbstatus .
For example,
.Dx
$ \f(CBsmbstatus\fP
Samba version 2.2.7a
Service    uid     gid      pid     machine
-------------------------------------------
ftp        grog    example  37390   freebie  (223.147.37.1) Mon Mar 31 13:48:13 2003

No locked files
.De
