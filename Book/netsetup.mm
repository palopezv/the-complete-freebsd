.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: netsetup.mm,v 4.21 2003/06/29 09:05:45 grog Exp grog $
.\"
.Chapter \*[nchnetsetup] "Configuring the local network"
.X "PPP"
.X "SLIP"
In Chapter
.Sref \*[nchnetintro] \&
we looked at the basic concepts surrounding BSD networking.  In this chapter and
the following two, we'll look at what we need to do to configure a network,
first manually, then automatically.  Configuring \fIPPP\fP\/ is still a whole
lot more difficult than configuring an Ethernet, and they require more
prerequisites, so we'll dedicate Chapter
.Sref "\*[nchppp]" ,
to that issue.
.P
.X "example.org"
.X "example.net"
In this chapter, we'll first look at \fIexample.org\fP\/ in the reference
network on page
.Sref \*[ref-net] ,
since it's the easiest to set up.  After that, we'll look at what additional
information is needed to configure machines on \fIexample.net\fP.
.H2 "Network configuration with sysinstall"
To configure a network, you must describe its configuration to the system.  The
system initialization routines that we discussed on page
.Sref \*[bootup] \&
include a significant portion that sets up the network environment.  In
addition, the system contains a number of standard IP configuration files that
define your system's view of the network.  If you didn't configure the network
when you installed your system, you can still do it now.
Log in as \f(CWroot\fP and start
.Command sysinstall .
Select the \f(CWIndex\fP, then \f(CWNetwork Interfaces\fP.  You will see the
menu of Figure
.Sref \*[network-setup-menu-2] ,
which is the same as in Figure
.Sref \*[network-services] \&
on page
.Sref \*[network-services-page] .
.X "network, interface, setting up"
On a standard 80x25 display it requires scrolling to see the entire menu.  The
only real network board on this list is \fIxl0\fP, the Ethernet board.  The
others are standard hardware that can also be used as network interfaces.
.P
.PIC images/network-setup.ps 4i
.Figure-heading "Network setup menu"
.Fn network-setup-menu-2
Choose the Ethernet board,
.Device -i xl0
You get a question about whether you want to use IPv6 configuration.  In this
book we doesn't discuss IPv6, so answer \f(CWNo\fP.  Next you get a question
about DHCP configuration.  We discuss DHCP configuration on page
.Sref \*[DHCP] .
If you already have a DHCP server set up, you may prefer to answer \f(CWYes\fP
to this question, which is all you need to do.  If you answer \f(CWNo\fP, the
next menu asks us to set the internet parameters.  Figure
.Sref \*[network-config-menu-2] \&
shows the network configuration menu after filling in the values.
.PIC images/network-config.ps 4i
.Figure-heading "Network configuration menu"
.Fn network-config-menu-2
.ne 3v
Specify the fully qualified local host name.  When you tab to the
\f(CWDomain:\fP field, the domain is filled in automatically.  We have chosen to
call this machine \fIpresto\fP, and the domain is \fIexample.org\fP.  In other
words, the full name of the machine is \fIpresto.example.org\fP.  Its IP address
is \f(CW223.147.37.2\fP.  In this configuration, all access to the outside world
goes via \fIgw.example.org\fP, which has the IP address \f(CW223.147.37.5\fP.
The name server is located on the same host, \fIpresto.example.org\fP.  If the
name server isn't running when this information is needed, we must specify all
addresses in numeric form, as shown.
.P
What happens if you don't have a domain name?  If you're connecting to the
global Internet, you should go out and get one\(emsee page
.Sref \*[domainreg] .
But in the meantime, don't fake it.  Just leave the fields empty.  If you're not
connecting to the Internet, of course, it doesn't make much difference what name
you choose.
.P
As is usual for a class C network, the net mask is \f(CW255.255.255.0\fP.  You
don't need to fill in this information\(emif you leave this field without
filling it in,
.Command sysinstall
inserts it for you.  Normally, as in this case, you wouldn't need any additional
options to
.Command ifconfig .
.P
.Command sysinstall
saves configuration information in
.File /etc/rc.conf .
When the system starts, the startup scripts use this information to configure
the network.  It also optionally starts the interface immediately.  In the next
section we'll look at the commands it uses to perform this function.
.H2 "Manual network configuration"
.X "network, configuration, manual"
Usually FreeBSD configures your network automatically when it boots.  To do so,
it uses the configuration files in
.Directory /etc .
So why do it manually?  There are several reasons:
.Ls B
.LI
It makes it easier to create and maintain the configuration files if you know
what's going on behind the scenes.
.LI
It makes it easier to modify something ``on the fly.''  You don't have to reboot
just because you have changed your network configuration.
.LI
With this information, you can edit the configuration files directly rather than
use the menu interface, which saves a lot of time.
.Le
.Aside
We spend a lot of time discussing this point on the FreeBSD mailing lists.  One
thing's for sure: neither method of configuration is perfect.  Both menu-based
and text-file\(enbased configuration schemes offer you ample opportunity to
shoot yourself in the foot.  But at the moment, the configuration file system is
easier to check \f(BIif you understand what's going on\fP.  That's the reason
for the rest of this chapter.
.End-aside
.P
In this section, we'll look at the manual way to do things first, and then we'll
see how to put it in the configuration files so that it gets done automatically
next time.  You can find a summary of the configuration files and their contents
on page
.Sref \*[configfiles] .
.H3 "Describing your network"
.Pn describing-network
.X "network, interface"
.X "network, interface, broadcast"
.X "broadcast interface"
.X "network mask"
In Table
.Sref \*[reference-net] \&
on page
.Sref \*[reference-net-page] ,
we saw that systems connect to networks via \fInetwork interfaces\fP.  The
kernel detects the interfaces automatically when it starts, but you still need
to tell it what interfaces are connected to which networks, and even more
importantly, which address your system has on each network.  In addition, if the
network is a \fIbroadcast\fP\/ network, such as an Ethernet, you need to specify
a range of addresses that can be reached directly on that network.  As we saw
on page
.Sref \*[netmask] ,
we perform this selection with the \fInetwork mask\fP.
.H4 "Ethernet interfaces"
.X "Ethernet, interface"
.X "interface, Ethernet"
.Pn ifconfig-example
.X "gw.example.org"
Once we have understood these concepts, it's relatively simple to use the
.Command ifconfig
program to set them.  For example, for the Ethernet interface on system
\fIgw\fP, with IP address \f(CW223.147.37.5\fP, we need to configure interface
\fIdc0\fP.  The network mask is the standard value for a class C network,
\f(CW255.255.255.0\fP.  That's all we need to know:
.Dx
# \f(CBifconfig dc0 inet 223.147.37.5 netmask 255.255.255.0 up\fP
.De
In fact, this is more than you usually need.  The \f(CWinet\fP tells the
interface to use Internet protocol Version 4 (the default), and \f(CWup\fP tells
it to bring it up (which it does anyway).  In addition, this is a class C
network address, so the net mask defaults to \f(CW255.255.255.0\fP.  As a
result, you can abbreviate this to:
.Dx
# \f(CBifconfig dc0 223.147.37.5\fP
.De
Note that this is different from what Linux requires.  With Linux you must
supply explicit netmask and broadcast address specifications.
.P
As we saw on page
.Sref \*[netmask] ,
it has become typical to abbreviate net masks to the character \f(CW/\fP
followed by the number of 1 bits set in the network mask.
.Command ifconfig
understands this usage, so if you wanted to set a non-standard network mask of,
say, \f(CW255.255.255.240\fP, which has 28 bits set, you could write:
.Dx
# \f(CBifconfig dc0 223.147.37.5/28\fP
.De
.SPUP
.H4 "Point-to-point interfaces"
.X "point-to-point interface"
.X "interface, point-to-point"
With a point-to-point interface, the software currently requires you to specify
the IP address of the other end of the link as well.  As we shall see in Chapter
.Sref "\*[nchppp]" ,
there is no good reason to do this, but
.Command ifconfig
insists on it.  In addition, we need the network mask for a non-broadcast
medium.  The value is obvious:\*F
.FS
Well, you'd think it was obvious.  We'll see on page
.Sref \*[pppnetmask] \&
that some people think it should be something else.
.FE
you can reach exactly one address at the other end, so it must be
\f(CW255.255.255.255\fP.  With this information, we could configure the PPP
interface on \fIgw\fP:
.Dx 1
# \f(CBifconfig tun0 139.130.136.133 139.130.136.129 netmask 255.255.255.255\fP
.De
In fact, this is almost never necessary; in Chapter
.Sref "\*[nchppp]" \&
we'll see that the PPP software usually sets the configuration automatically.
.H4 "The loopback interface"
.X "interface, loopback"
.X "loopback interface"
.X "lo0"
The IP protocols require you to use an address to communicate with every
system\(emeven your own system.  Theoretically, you could communicate with your
system via the an Ethernet interface, but this is relatively slow: the data
would have to go through the network stack.  Instead, there is a special
interface for communicating with other processes in the same system, the
\fIloopback interface\fP.  Its name is \fIlo0\fP, and it has the address
\f(CW127.0.0.1\fP.  It's straightforward enough to configure:
.Dx
# \f(CBifconfig lo0 127.0.0.1\fP
.De
In fact, though, you don't even need to do this much work: the system
automatically sets it up at boot time.
.H3 "Checking the interface configuration"
.X "interface, checking configuration"
.Command ifconfig
doesn't just set the configuration: you can also use it to check the
configuration.  It's a good idea to do this after you change something:
.Dx
$ \f(CBifconfig\fP
dc0: flags=8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> mtu 1500
        inet 223.147.37.5 netmask 0xffffff00 broadcast 223.147.37.255
        inet6 fe80::280:c6ff:fef9:d3fa%dc0 prefixlen 64 scopeid 0x1
        ether 00:80:c6:f9:d3:fa
        media: Ethernet autoselect (100baseTX <full-duplex>)
        status: active
lp0: flags=8810<POINTOPOINT,SIMPLEX,MULTICAST> mtu 1500
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
        inet6 ::1 prefixlen 128
        inet6 fe80::1%lo0 prefixlen 64 scopeid 0x3
        inet 127.0.0.1 netmask 0xff000000
tun0: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1500
        inet 139.130.136.133 --> 139.130.136.129 netmask 0xffffffff
.De
.SPUP
.Aside
Other BSD systems require you to write \fIifconfig -a\fP. to list the
configuration of all interfaces, and FreeBSD still accepts it.  Some commercial
UNIX systems don't understand even this flag.
.End-aside
.X "network, interface, lp0"
.X "lp0, network interface"
.X "PLIP"
.X "network, interface, sl0"
.X "sl0, network interface"
.X "SLIP"
There are a number of things to note here:
.Ls B
.LI
The \fIdc0\fP interface has both an IPv4 address (\f(CWinet\fP) and a
corresponding IPv6 address (\f(CWinet6\fP).  It also specifies the Ethernet
address (\f(CWether 00:80:c6:f9:d3:fa\fP).  It is capable of negotiating 10
Mb/s, 100 Mb/s half duplex and 100 Mb/s full duplex.  It's connected to a
switch, so it's currently running 100 Mb/s full duplex.
.LI
The interface \fIlp0\fP\/ is the the \fIPLIP\fP\/ interface for connections via
the parallel port.  It is not configured (in other words, it has not been set up
for operation).
.LI
We've already seen the loopback interface \fIlo0\fP.
.LI
.X "tun0, network interface"
.X "network, interface, tun0"
There is also a \fItun0\fP\/ interface for PPP.
.Le
.SPUP
.H3 "The configuration files"
.X "network, configuration files"
.X "configuration file, network"
.X "network, configuration variables"
.X "configuration variables, network"
.Pn set-hostname
The system startup scripts summarize this configuration information in a number
of \fIconfiguration variables\fP.  See Chapter
.Sref "\*[nchstarting]" \&
for more details.  At the moment, the following variables are of interest to us:
.Ls B
.LI
\f(CWhostname\fP is the name of the host.  You should have set it when you
installed the system (see page
.Sref \*[install-set-hostname] ).
You can also set it manually with the
.Command hostname
command:
.Dx
# \f(CBhostname -s gw.example.org\fP
.De
.SPUP
.LI
For each interface, a variable of the form \f(CWifconfig_\f[CI]interface\fR\/
contains the parameters to be passed to
.Command ifconfig
to configure that interface.
.Le
.X "gw.example.org"
.P
Previously, FreeBSD also required you to set a variable
\f(CWnetwork_interfaces\fP, a list of the names of the interfaces to be
configured.  This variable now defaults to the value \f(CWauto\fP to specify
that all interfaces should be configured.  You only need to change it if you
specifically want to exclude an interface from configuration.
.P
For \fIgw\fP, we put the following information in
.File /etc/rc.conf \/:
.Dx
hostname="gw.example.org"
ifconfig_dc0="inet 223.147.37.5"
.De
We don't configure the \fItun0\fP\/ interface here; as we'll see in Chapter
.Sref "\*[nchppp]" ,
the PPP setup works differently.
.H2 "Automatic configuration with DHCP"
.Pn DHCP
.X "DHCP"
.X "Dynamic Host Configuration Protocol"
Maintaining the network configurations for a number of machines can be a pain,
especially if they're laptops that come and go.  There's an alternative for
larger networks: use \fIDHCP\fP, the \fIDynamic Host Configuration Protocol\fP.
DHCP enables a machine to get configuration information automatically from the
network.  The concept is expandable, but typically you get an IP address and net
mask and the names of the default name servers and routers.  In terms of the
configuration we've seen so far, this replaces running the
.Command ifconfig
and
.Command route
programs, and also the file
.File /etc/resolv.conf ,
which describes the locations of name servers.  We'll look at it on page
.Sref \*[resolv.conf] .
.P
There are two parts to DHCP: the client and the server.
.H3 "DHCP client"
To get a configuration, you run
.Command dhclient .
In previous releases of FreeBSD,
.Command dhclient
printed out information about the addresses it received.  In Release 5, it does
not print anything.  Simply start it with the name of the interface:
.Dx
# \f(CBdhclient dc0\fP
.De
To assign an address automatically at boot time, put the special value
\f(CWDHCP\fP in the \f(CWifconfig_dc0\fP\/ variable:
.Dx
ifconfig_dc0=DHCP
.De
.SPUP
.H3 "DHCP server"
.Pn DHCP-server
DHCP requires a server.  The server is not included as part of the base system;
instead, install the \fInet/isc-dhcp3\fP\/ port:
.Dx
# \f(CBcd /usr/ports/net/isc-dhcp3\fP
# \f(CBmake install\fP
.De
To configure
.Daemon dhcpd ,
edit the  configuration file
.File /usr/local/etc/isc-dhcpd.conf .
Here's an example:
.Dx
ddns-update-style ad-hoc;

# 100 Mb/s Ethernet
subnet 223.147.37.0 netmask 255.255.255.0 {
  range 223.147.37.90 223.147.37.110;
  option domain-name-servers freebie.example.com, presto.example.com;
  option domain-name "example.com";
  option routers gw.example.com;
  option subnet-mask 255.255.255.0;
  option broadcast-address 223.147.37.255;
  default-lease-time 86400;
  max-lease-time 259200;
  use-host-decl-names on;                       \fIuse the specified name as host name\fP\/
  host andante {
    hardware ethernet 0:50:da:cf:7:35;
  }
}
.De
This configuration file tells
.Daemon dhcpd \/:
.Ls B
.LI
To dynamically allocate IP addresses in the range \f(CW223.147.37.90\fP to
\f(CW223.147.37.110\fP (\f(CWrange\fP keyword).
.LI
That the domain name servers are \fIfreebie.example.com\fP\/ and
\fIandante.example.com\fP.  We'll look at domain name servers in Chapter
.Sref "\*[nchdns]" .
.LI
The net mask and the broadcast address.
.Le
The variables \f(CWdefault-lease-time\fP and \f(CWmax-lease-time\fP, which are
specified in seconds, determine how long it will be before a system checks its
configuration.  The values here represent one day and three days respectively.
.P
\f(CWuse-host-decl-names\fP tells
.Daemon dhcpd
to use the name on the \f(CWhost\fP line as the host name of the system.
Otherwise you would need an additional \f(CWoption host-name\fP specification
for every system.  For one machine it doesn't make much difference, but if you
have twenty such machines, you'll notice the difference.
.P
.ne 3v
One of the problems with
.Daemon dhcpd
is that by default it doesn't allocate a static IP address.  Theoretically you
could attach a laptop to the same DHCP server and get a different address every
time, but in fact
.Daemon dhcpd
does its best to keep the same address, and sometimes you may find it impossible
to change its mind.  In this configuration file, though, we have explicitly told
.Daemon dhcpd
about \fIandante\fP, which is recognized by its Ethernet address.  This works
relatively well for fixed machines, but there's a problem with laptops and
PC Card:
.Daemon dhcpd
recognizes the network interface, not the machine, and if you swap the interface
card, the IP address moves to the new machine.
.H3 "Starting dhcpd"
The
.Daemon dhcpd
port installs a sample startup file in the directory
.Directory /usr/local/etc/rc.d .
It's called
.File -n isc-dhcpd.sh.sample ,
a name which ensures that it won't get executed.
.hy
This file doesn't normally require any configuration; simply copy it to
.File isc-dhcpd.sh
in the same directory.  This enables the system startup to find it and start
.Daemon dhcpd .
.P
To start
.Daemon dhcpd
during normal system operation, just run this same script:
.Dx
# \f(CB/usr/local/etc/rc.d/isc-dhcpd.sh start\fP
Mar 14 15:45:09 freebie dhcpd: Internet Software Consortium DHCP Server V3.0rc10
Mar 14 15:45:09 freebie dhcpd: Copyright 1995-2001 Internet Software Consortium.
Mar 14 15:45:09 freebie dhcpd: All rights reserved.
Mar 14 15:45:09 freebie dhcpd: For info, please visit http://www.isc.org/products/DHCP
Mar 14 15:45:09 freebie dhcpd: Wrote 0 deleted host decls to leases file.
Mar 14 15:45:09 freebie dhcpd: Wrote 0 new dynamic host decls to leases file.
Mar 14 15:45:09 freebie dhcpd: Wrote 14 leases to leases file.
Mar 14 15:45:09 freebie dhcpd: Listening on BPF/xl0/00:50:da:cf:07:35/223.147.37.0/24
Mar 14 15:45:09 freebie dhcpd: Sending on   BPF/xl0/00:50:da:cf:07:35/223.147.37.0/24
Mar 14 15:45:09 freebie dhcpd: Sending on   Socket/fallback/fallback-net
.De
When you change the configuration file
.File /usr/local/etc/isc-dhcpd.conf ,
you must restart
.Daemon dhcpd :
.Dx
# \f(CB/usr/local/etc/rc.d/isc-dhcpd.sh restart\fP
.De
.SPUP
.H2 "Configuring PC Card networking cards"
.Pn PCMCIA-net
We've looked at PC Card devices on page
.Sref \*[pccard] ,
but there are some special issues involved in configuring networking cards.  Of
course,
.Command ifconfig
works with PC Card networking cards in exactly the same way as it does with PCI
and ISA cards, but you can't configure them in the same manner at startup,
because they might not yet be present.
.P
.ne 5v
On inserting a PC Card device, you will see something like this on the console:
.Dx
Manufacturer ID: 01015751
Product version: 5.0
Product name: 3Com Corporation | 3CCFE575BT | LAN Cardbus Card | 001 |
Functions: Network Adaptor, Memory
CIS reading done
cardbus0: Resource not specified in CIS: id=14, size=80
cardbus0: Resource not specified in CIS: id=18, size=80
xl0: <3Com 3c575B Fast Etherlink XL> port 0x1080-0x10bf mem 0x88002400-0x8800247
f,0x88002480-0x880024ff irq 11 at device 0.0 on cardbus0
xl0: Ethernet address: 00:10:4b:f8:fd:20
miibus0: <MII bus> on xl0
tdkphy0: <TDK 78Q2120 media interface> on miibus0
tdkphy0:  10baseT, 10baseT-FDX, 100baseTX, 100baseTX-FDX, auto
.De
After this,
.Command ifconfig
shows:
.Dx
$ \f(CBifconfig xl0\fP
xl0: flags=8802<BROADCAST,SIMPLEX,MULTICAST> mtu 1500
        ether 00:10:4b:f8:fd:20
        media: Ethernet autoselect (100baseTX <full-duplex>)
.De
The card is there, but it's not configured.  FreeBSD uses the
.Daemon devd
daemon to perform userland configuration after a card has been attached.  We've
already looked at
.Daemon devd
on page
.Sref \*[devd] .
When
.Daemon devd
establishes that the card is a networking card, it calls
.File /etc/pccard_ether
to configure it.  In the following, we'll see how
.File /etc/pccard_ether
configures our \fIxl0\fP interface.  It performs the following steps:
.Ls B
.LI
It reads the configuration from
.File /etc/defaults/rc.conf
and
.File /etc/rc.conf .
.LI
If the interface is already up, it exits.
.LI
If a file
.File /etc/start_if.xl0
exists, it executes it.  After doing so, it continues.
.LI
It checks whether the variable \f(CWremovable_interfaces\fP exists and contains
the name of the interface, \fIxl0\fP.  If not, it continues.
.LI
If the value of \f(CWifconfig_xl0\fP is NO, it exits.
.LI
If the value of \f(CWifconfig_xl0\fP is \f(CWDHCP\fP, it attempts to set up the
interface with DHCP.
.LI
Otherwise it performs the
.Command ifconfig
commands specified in the variable \f(CWifconfig_xl0\fP.
.Le
That's a lot of choice.  What do you use when?  That depends on what you want to
do.  The first thing to note is that nothing happens unless your interface name
is in the variable \f(CWremovable_interfaces\fP, and the variable
\f(CWifconfig_xl0\fP exists.  The question is, what do you put in
\f(CWifconfig_xl0\fP?
.P
In principle, it's the same as with other network cards: either IP address and
other options, or \f(CWDHCP\fP.  The third alternative is important, though.
Let's consider the case where you want to start a number of services when the
system is connected.  You might want to run
.Command ntpdate ,
then start
.Daemon ntpd
and
.Daemon rwhod ,
and you may want to mount some NFS file systems.  You can do all this at startup
with normal network cards, but
.File /etc/pccard_ether
isn't clever enough to do all that.  Instead, create a file called
.File /etc/start_if.xl0
and give it the following contents:
.Dx
dhclient xl0
ntpdate freebie
killall ntpd
ntpd &
killall rwhod
rwhod &
mount -t nfs -a
.De
Don't forget to start DHCP or otherwise set the IP address, because this method
bypasses the standard startups.
.P
In addition, you put this in
.File /etc/rc.conf \/:
.Dx
devd_enable=YES
ifconfig_xl0=NO
removable_interfaces="wi0 xe0 xl0"
.De
The values in the last line only need to include \fIxl0\fP, of course, but
it's good to put in every interface name that you would possibly use.
.H3 "Detaching network cards"
When you remove a network card,
.Daemon devd
invokes
.File /etc/pccard_ether
again.  The actions are similar to the one it performs when the card is
attached:
.Ls B
.LI
If a file
.File /etc/stop_if.xl0
exists, it is executed.
.LI
If the variable \f(CWifconfig_xl0\fP is set to \f(CWDHCP\fP,
.File /etc/pccard_ether
stops the
.Command dhclient
process, which would otherwise loop forever.
.LI
If \f(CWifconfig_xl0\fP contains normal
.Command ifconfig
parameters,
.File /etc/pccard_ether
removes any static routes for that interface.
.Le
If you travel elsewhere with a laptop and suspend the system, make sure you
unmount any NFS file systems first.  You can't do it once you're no longer
connected to the network, and it's possible that things will hang trying to
access NFS-mounted files.
.H2 "Setting up wireless networking"
.Pn wireless-setup
We saw in Chapter
.Sref "\*[nchnetintro]" \&
that wireless cards have a few more tricks up their sleeves than conventional
Ethernets.  To set them up correctly, you need to know:
.Ls B
.LI
Does the network you are joining accept connections with a blank SSID?  If not,
what is its SSID?
.LI
What mode are you running in?  Is it BSS mode, IBSS mode, or Lucent demo ad-hoc?
.LI
If you're running in IBSS or Lucent demo ad-hoc mode, you'll need to know the
frequency (channel) on which the network is running.
.LI
If you're running in IBSS mode, do you already have an IBSS, or is your machine
going to be the IBSS?
.LI
Are you worried about power consumption?  If you're running in BSS mode, you can
significantly reduce the power consumption of the card by turning on power save
mode, but it can slow some things down.
.LI
Are you using WEP?  If so, what's the key?
.Le
Each of these translates into an
.Command ifconfig
command.  Here are some typical examples:
.Dx
ifconfig wi0 ssid Example                       \fIjoin Example network\fP\/
ifconfig wi0 media autoselect mediaopt -adhoc   \fIset BSS mode\fP\/
ifconfig wi0 channel 3                          \fIselect channel 3 (if not in BSS mode)\fP\/
ifconfig wi0 wepmode on                         \fIturn encryption on (if using WEP)\fP\/
ifconfig wi0 wepkey 0x42726f6b21                \fIencryption key (for WEP)\fP\/
.De
When setting media options, you must also select the media, even if it is
unchanged; thus the \f(CWmedia autoselect\fP in the example above.
.P
You have a choice of where to put these specifications.  For example, if you
were connecting to the \fIExample\fP\/ network, which is IBSS, you could put
this in your
.File /etc/rc.conf \/:
.Dx
devd_enable=YES
ifconfig_wi0="192.168.27.4 ssid Example media autoselect mediaopt adhoc \e
             channel 3 wepmode on wepkey 0x42726f6b21
removable_interfaces="wi0 xe0 xl0"
.De
You don't need to do anything special to become an IBSS master in an IBSS
network: if there is no master already, and your card supports it, your system
will become the IBSS master.
.P
If, on the other hand, you were connecting to a non-encrypted network, you would
not need the WEP key, and you might enter:
.Dx
ifconfig_wi0="192.168.27.4 ssid Example media autoselect mediaopt ibss-master channe
l 3 wepmode off"
.De
.SPUP
.H3 "What we can do now"
.X "routing"
At this point, we have configured the link layer.  We can communicate with
directly connected machines.  To communicate with machines that are not directly
connected, we need to set up \fIrouting\fP.  We'll look at that next.
.H2 "Routing"
.X "routing"
Looking back at our example network on page
.Sref \*[reference-net-page] ,
we'll reconsider a problem we met there: when a system receives a normal data
packet, what does it do with it?  There are four possibilities:
.Ls N
.X "broadcast packet"
.X "multicast"
.LI
If the packet is a broadcast packet, or if it's addressed to one of its
interface addresses, it delivers it locally.
.LI
If it's addressed to a system to which it has a direct connection, it sends it
to that system.
.LI
If it's not addressed to a system to which it is directly connected, but it
knows a system that knows what to do with the packet, it sends the packet to
that system.
.LI
If none of the above apply, it discards the packet.
.Le
.Table-heading "The routing table"
.TS
tab(#) ;
lfCWp8 | lfCWp8 | lfCWp8 | l | lfCWp8 .
\fB\s10Destination\fP#\fB\s10Gateway\fP#\fB\s10Net mask\fP#\fBType\fP#\fB\s10Interface
_
127.0.0.1#127.0.0.1#255.0.0.0#Host#lo0
223.147.37.0##255.255.255.0#Direct#dc0
139.130.136.129#139.130.136.133#255.255.255.255#Host#tun0
default#139.130.136.129#0.0.0.0#Gateway#tun0
.TE
.Tn routing-table
.sp 1.5v
.X "routing"
.X "routing, table"
.X "net mask"
.X "gw.example.org"
These decisions are the basis of \fIrouting\fP.  The implementation performs
them with the aid of a \fIrouting table\fP, which tells the system which
addresses are available where.  We've already seen the \fInet mask\fP\/ in
Chapter
.Sref "\*[nchnetintro]" ,
on page
.Sref \*[netmask] .
We'll see that it also plays a significant role in the routing decision.  Table
.Sref \*[routing-table] \&
shows a symbolic view of the routing table for \fIgw.example.org\fP.  It looks
very similar to the
.Command ifconfig
output in the previous section:
.Ls B
.LI
.X "loopback interface"
.X "interface, loopback"
The first entry is the \fIloopback\fP\/ entry: it shows that the local host can
be reached by the interface \fIlo0\fP, which is the name for the loopback
interface on all UNIX systems.  Although this entry specifies a single host, the
net mask allows for 16,276,778 hosts.  The other addresses aren't used.
.LI
The second entry is for the local Ethernet.  In this case, we have a direct
connection, so we don't need to specify a gateway address.  Due to the net mask
\f(CW255.255.255.0\fP, this entry accounts for all addresses from
\f(CW223.147.37.0\fP to \f(CW223.147.37.255\fP.
.P
This entry also emphasizes the difference between the output of
.Command ifconfig
and the routing table.
.Command ifconfig
shows the address of the interface, the address needed to reach our system.  For
the Ethernet interface, it's \f(CW223.147.37.5\fP.  The routing table shows the
addresses that can be reached \fIfrom\fP\/ this system, so it shows the base
address of the Ethernet, \f(CW223.147.37.0\fP.
.LI
The third entry represents the PPP interface.  It is a host entry, like the
loopback entry.  This entry allows access to the other end of the PPP link only,
so the net mask is set to \f(CW255.255.255.255\fP (only one system).
.LI
Finally, the fourth entry is the big difference.  It doesn't have a counterpart
in the
.Command ifconfig
listing.  It specifies how to reach any address not already accounted
for\(emjust about the whole Internet.  In this case, it refers to the other end
address of the PPP link.
.Le
And that's all there is to it!  Well, sort of.  In our example configuration,
we're hidden in one corner of the Internet, and there's only one way out to the
rest of the network.  Things look different when you are connected to more than
one network.
.pageref \*[ISP-routing] "\&" "Further down the page" "On page \*[ISP-routing]"
.X "example.net"
we'll look at the differences we need for the ISP \fIexample.net\fP.  In the
middle of the Internet, things are even more extreme.  There may be dozens of
interfaces, and the choice of a route for a particular address may be much more
complicated.  In such an environment, two problems occur:
.Ls B
.LI
The concept of a default route no longer has much significance.  If each
interface carries roughly equal traffic, you really need to specify the
interface for each network or group of networks.  As a result, the routing
tables can become enormous.
.LI
.X "routing, software"
There are probably multiple ways to route packets destined for a specific
system.  Obviously, you should choose the best route.  But what happens if it
fails or becomes congested?  Then it's not the best route any more.  This kind
of change happens frequently enough that humans can't keep up with it\(emyou
need to run \fIrouting software\fP\/ to manage the routing table.
.Le
.H3 "Adding routes automatically"
.X "adding routes, automatically"
.X "routed, daemon"
.X "daemon, routed"
FreeBSD comes with all the currently available routing software, primarily the
daemon
.Daemon routed .
The newer
.Daemon gated
used to be included as well, but it is no longer available for free.  It is
available from
.URI http://www.nexthop.com/products/howto_order.shtml .
An alternative in the Ports Collection is
.Daemon zebra .
.P
All these daemons have one thing in common: you don't need them.  At any rate,
you don't need them until you have at least two different connections to the
Internet, and even then it's not sure.  As a result, we won't discuss them here.
If you do need to run routing daemons, read all about them in \fITCP/IP Network
Administration\fP, by Craig Hunt.
.P
From our point of view, however, the routing protocols have one particular
significance: the system expects the routing table to be updated automatically.
As a result, it is designed to use the information supplied by the routing
protocols to perform the update.  This information consists of two parts:
.Ls B
.LI
The address and netmask of the network (in other words, the address range).
.LI
.X "gateway"
The address of the \fIgateway\fP\/ that forwards data for this address range.
The gateway is a directly connected system, so it also figures in the routing
table.
.Le
.H3 "Adding routes manually"
.X "adding routes, manually"
.Pn route
As we saw in the previous section, the routing software uses only addresses, and
not the interface name.
.\" XXX FIXME
To add routes manually, we have to give the same information.
.P
.X "route"
The program that adds routes manually is called
.Command route .
We need it to add routes to systems other than those to which we are directly
connected.
.P
.X "freebie.example.org"
.X "presto.example.org"
.X "bumble.example.org"
.X "wait.example.org"
.ne 10v
To set up the routing tables for the systems connected only to our reference
network (\fIfreebie\fP, \fIpresto\fP, \fIbumble\fP and \fIwait\fP\/), we could
write:
.Dx
# \f(CBroute add default gw\fP
.\" Done automatically? # \f(CBroute add freebie localhost\fP
.\" # \f(CBroute add -net 223.147.37 freebie\fP
.De
During system startup, the script
.File /etc/rc.network
performs this operation automatically if you set the following variable in
.File /etc/rc.conf \/:
.Dx
defaultrouter="223.147.37.5"    # Set to default gateway (or NO).
.De
Note that we enter the address of the default router as an IP address, not a
name.  This command is executed before the name server is running.  We can't
change the sequence in which we start the processes: depending on where our name
server is, we may need to have the route in place to access the name server.
.P
On system \fIgw\fP, the default route goes via the \fItun0\fP interface:
.Dx
# defaultrouter="139.130.136.129" # Set to default gateway (or NO).
gateway_enable="YES"            # Set to YES if this host will be a gateway.
.De
.X "PPP"
This is a PPP interface, so you don't need a \f(CWdefaultrouter\fP entry; if you
did, it would look like the commented-out entry above.  On page
.Sref \*[ppp-default-route] \&
we'll see how PPP sets the default route.
.P
We need to enable gateway functionality on this system, since it receives data
packets on behalf of other systems.  We'll look at this issue in more depth on
page
.Sref \*[set-gateway] .
.H2 "ISP's route setup"
.Pn ISP-routing
.X "example.org"
.X "free-gw.example.net"
.X "example.org"
.X "biguser.com"
.X "network, interface, ppp0"
.X "ppp0, network interface"
At the ISP site, things are slightly more complicated than at \fIexample.org\fP.
Let's look at the gateway machine \fIfree-gw.example.net\fP.  It has three
connections, to the global Internet, to \fIexample.org\fP\/ and to another
network, \fIbiguser.com\fP (the network serviced by interface \fIppp0\fP\/).  To
add the routes requires something like the following commands:
.Dx
# \f(CBroute add default 139.130.237.65\fP                  \fIigw.example.net\fP\/
# \f(CBroute add -net 223.147.37.0 139.130.136.133\fP       \fIgw.example.org\fP\/
# \f(CBroute add -net 223.147.38.0 -iface ppp0\fP           \fIlocal ppp0 interface\fP\/
.De
.X "example.org"
.X "biguser.com"
The first line tells the system that the default route is via
\fIgw.example.org\fP.  The second shows that the network with the base IP
address \f(CW223.147.37.0\fP (\fIexample.org\fP\/) can be reached via the
gateway address 139.130.136.133, which is the remote end of the PPP link
connected via \fIppp3\fP.  In the case of \fIbiguser.com\fP, we don't know the
address of the remote end; possibly it changes every time it's connected.  As a
result, we specify the name of the interface instead: we know it's always
connected via \fIppp0\fP.
.P
.ne 10v
The procedure to add this information to
.File /etc/rc.conf
is similar to what we did for the interface addresses:
.Ls B
.LI
The variable \f(CWstatic_routes\fP contains a list of the static routes that
are to be configured.
.LI
.X "freebie.example.org"
.X "example.org"
.X "biguser.com"
For each route, a variable corresponding to the route name specified in
\f(CWstatic_routes\fP, with the text \f(CWroute_\fP prepended.  Unlike the
interfaces, you can assign any name you want to them, as long as it starts with
\f(CWroute_\fP.  It makes sense for them to be related to the domain name, but
they don't have to.  For example, we would have liked to have called our network
\fIfreebie.org\fP, but there's a good chance that this name has been taken, so
we called it \fIexample.org\fP\/ instead.  The old name lives on in the name of
the route, \f(CWroute_freebie\fP.  In the case of \fIbiguser.com\fP, we have
called the route variable \f(CWroute_biguser\fP.
.Le
We put the following entries into \fIfree-gw\fP\/'s
.File /etc/rc.conf \/:
.Dx
defaultrouter="139.130.237.65" # Set to default gateway (or NO).
static_routes="freebie biguser" # list of static routes
route_freebie="-net 223.147.37.0 139.130.237.129"
route_biguser="-net 223.147.38.0 139.130.237.9"
.De
.SPUP
.H2 "Looking at the routing tables"
.X "routing, table, examining"
.X "examining routing table"
.Pn netstat
You can show the routing tables with the
.Command netstat
tool.  Option \f(CW-r\fP shows the routing tables.  For example, on
\f(CWfreebie\fP you might see:
.Dx
# \f(CBnetstat -r\fP
Routing tables

Internet:
Destination    Gateway            Flags     Refs     Use     Netif Expire
default        gw                 UGSc        9     8732       rl0
localhost      localhost          UH          0     1255       lo0
223.147.37     link#2             UC          0        0
presto         0:0:c0:44:a5:68    UHLW       13   139702       rl0   1151
freebie        0:a0:24:37:d:2b    UHLW        3    38698       lo0
wait           0:60:97:40:fb:e1   UHLW        6     1062       rl0    645
bumble         8:0:20:e:2c:98     UHLW        2       47       rl0   1195
gw             0:60:97:40:fb:e1   UHLW        6     1062       rl0    645
broadcast      ff:ff:ff:ff:ff:ff  UHLWb       2     5788       rl0
.De
There's a lot to notice about this information:
.Ls B
.LI
The first column is the name of a host or a network to which packets can be
sent, or the keyword \f(CWdefault\fP.
.LI
.X "gateway"
The second column, the \fIgateway\fP, indicates the path to the destination.
This field differs significantly even from older versions of UNIX.  It can be
the name of a host (for example, \fIgw\fP), a pointer to an interface
(\f(CWlink#2\fP, which means the second Internet interface; the output from
.Command ifconfig
is in the same sequence), or an Ethernet address (\f(CW8:0:20:e:2c:98\fP).
Older versions of UNIX do not use the last two forms.
.LI
We'll look at the flags below.  The most important ones to note are \f(CWG\fP
(gateway) and \f(CWH\fP (host).
.LI
The fields \f(CWRefs\fP, \f(CWUse\fP and \f(CWExpire\fP are only of interest
when you're running a routing protocol.  See the man page \fInetstat(1)\fP\/ for
more details.
.LI
\f(CWNetif\fP is the name of the interface by which the gateway can be reached.
In the case of a link, this is the interface, so the \f(CWNetif\fP field is
empty.
.LI
The order of the entries is not important.  The system searches the table for a
best fit, not a first fit.
.LI
The \f(CWdefault\fP entry points to \fIgw\fP, as we would expect.  The
interface, \fIrl0\fP, is the interface by which \fIgw\fP can be reached.
.LI
.X "address family"
You will also get some additional output for IPv6 (``Internet6'').  If you're
not using IPv6, you can ignore it.  If it gets on your nerves, you can limit
your view to IPv4 by entering the command \f(CWnetstat -rfinet\fP.  The
\f(CW-f\fP flag specifies which \fIaddress family\fP\/ you're interested in, and
\f(CWinet\fP specifies IPv4.
.Le
.H3 "Flags"
Compared to earlier versions of
.Command netstat ,
the current version displays many more flags.
The following table
.\" Table
.\" .Sref \*[netstat-r-flags] \&
.\" .pageref  \*[netstat-r-flags-page] "on page  \*[netstat-r-flags-page]" "\&"  "on page  \*[netstat-r-flags-page]"
gives you an overview.
.Table-heading "netstat -r flags values"
.TS
tab(#) ;
lfCWp9| lfCWp9 | lw65 .
\fB\s10Flag#\fB\s10Name#\fBMeaning
_
1#RTF_PROTO1#Protocol specific routing flag 1
2#RTF_PROTO2#Protocol specific routing flag 2
3#RTF_PROTO3#Protocol specific routing flag 3
B#RTF_BLACKHOLE#Just discard pkts (during updates)
b#RTF_BROADCAST#The route represents a broadcast address
C#RTF_CLONING#Generate new routes on use
c#RTF_PRCLONING#Protocol-specified generate new routes on use
D#RTF_DYNAMIC#Created dynamically (by redirect)
G#RTF_GATEWAY#Destination requires forwarding by intermediary
H#RTF_HOST#Host entry (net otherwise)
L#RTF_LLINFO#Valid protocol to link address translation
M#RTF_MODIFIED#Modified dynamically (by redirect)
R#RTF_REJECT#Host or net unreachable
S#RTF_STATIC#Manually added
U#RTF_UP#Route usable
W#RTF_WASCLONED#Route was generated as a result of cloning
X#RTF_XRESOLVE#External daemon translates proto to link address
.TE
.Tn netstat-r-flags
.H2 "Packet forwarding"
.X "packet forwarding"
.Pn set-gateway
We saw above that when a system receives a packet that is not intended for
itself, it looks for a route to the destination.  In fact, this is not always
the case: by default, FreeBSD just silently drops the packet.  This is desirable
for security reasons, and indeed it's required by RFC 1122, but if you want to
access the Internet via another machine on your local net, it's less than
convenient.
.P
The rationale for this is that most systems are only connected to one network,
and it doesn't make sense to have packet forwarding enabled.  Earlier systems
made this a kernel option, so that disabling packet forwarding also made the
kernel fractionally smaller.  In current versions of FreeBSD, the code is always
there, even if it is disabled.
.P
.X "gateway"
It's straightforward enough to set up your machine as a router (or
\fIgateway\fP\/): you can set it with the
.Command sysctl
command:
.Dx
# \f(CBsysctl -w net.inet.ip.forwarding=1\fP
net.inet.ip.forwarding: 0 -> 1
.De
In
.File /etc/rc.conf ,
you can set this with the variable \f(CWgateway_enable\fP:
.Dx
gateway_enable="YES"            # Set to YES if this host will be a gateway.
.De
.SPUP
.H2 "Configuration summary"
.X "network, configuration summary"
.X "configuration summary, network"
.X "free-gw.example.net"
In the course of this chapter, we've discussed a number of different
configurations.  In this section we'll summarize the configuration for for
\fIfree-gw.example.net\fP, since it is the most complicated.  You enter the
following information in your
.File /etc/rc.conf \/:
.Ls B
.LI
Set your host name:
.Ds
hostname="free-gw.example.net"
.De
.SPUP
.LI
For each interface, specify IP addresses and possibly net masks for each
interface on the machine:
.Dx
ifconfig_rl0="inet 139.130.237.117"
.De
The PPP interfaces are configured independently, so we won't look at them here,
but we might need their addresses for static routes.  The local interface
address for \fIppp0\fP is \f(CW139.130.136.9\fP, and the local address for
\fIppp3\fP is \f(CW139.130.136.129\fP.
.LI
Decide on a default route.  In this case, it is the gateway machine
\fIigw.example.net\fP, with the address \f(CW139.130.237.65\fP:
.Dx
defaultrouter="139.130.237.65" # Set to default gateway (or NO).
.De
.SPUP
.LI
Decide on other routes.  In this case, we have two, to \fIexample.org\fP\/ and
\fIbiguser.com\fP.  List them in the variable \f(CWstatic_routes\fP:
.Dx
static_routes="freebie biguser" # Set to static route list
.De
.SPUP
.LI
For each static route, create a variable describing the route:
.Dx
route_freebie="-net 223.147.37.0 139.130.136.133"
route_biguser="-net 223.147.38.0 -iface ppp0"
.De
.SPUP
.ne 5
.LI
Enable IP forwarding:
.Dx
gateway_enable="YES"            # Set to YES if this host will be a gateway.
.De
.SPUP
.Le
Without the comments, this gives the following entries:
.Dx
hostname="free-gw.example.net"
ifconfig_rl0="inet 139.130.237.117"
defaultrouter="139.130.237.65" # Set to default gateway (or NO).
static_routes="freebie biguser" # Set to static route list
route_freebie="-net 223.147.37.0 139.130.136.133"
route_biguser="-net 223.147.38.0 -iface ppp0"
gateway_enable="YES"            # Set to YES if this host will be a gateway.
.De
For a machine configured with DHCP, you might have:
.Dx
hostname="andante.example.net"
ifconfig_wi0=DHCP
.De
.SPUP
