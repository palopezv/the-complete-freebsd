9menu-1.5#T{
plan9
T}#T{
A simple menu patterened after plan9
T}
9term-1.6.3#T{
plan9
T}#T{
An X11 program which emulates a plan9 window.
T}
9wm-1.1#T{
plan9
T}#T{
An 8 1/2-like Window Manager for X
T}
Fudgit-2.41#T{
math
T}#T{
Multi-purpose data-processing and fitting program.
T}
ImageMagick-3.9.2#T{
graphics
T}#T{
An X11 package for display and interactive manipulation of images.
T}
LPRng-3.3.3#T{
sysutils print
T}#T{
An Enhanced Printer Spooler
T}
Mesa-2.5#T{
graphics
T}#T{
A graphics library similar to SGI's OpenGL.
T}
SSLeay-0.8.1#T{
devel security
T}#T{
SSL and crypto library
T}
STk-3.1#T{
lang
T}#T{
A scheme interpreter with full access to the Tk graphical package.
T}
Scilab-2.3#T{
math cad
T}#T{
A free CACSD Package by INRIA 
T}
SpecTcl-1.1#T{
devel
T}#T{
free drag-and-drop GUI builder for Tk and Java from Sun
T}
Wingz-142#T{
math
T}#T{
A Commercial Spreadsheet
T}
XFree86-3.3.1#T{
x11
T}#T{
X11R6.3/XFree86 core distribution
T}
XFree86-contrib-3.3.1#T{
x11
T}#T{
XFree86 contrib programs
T}
XPostitPlus-2.3#T{
x11
T}#T{
PostIt (R) messages onto your X11 screen
T}
Xaw3d-1.3#T{
x11
T}#T{
A 3-D Athena Widget set that looks like Motif
T}
a2ps--4.9.8#T{
print
T}#T{
Formats an ascii file for printing on a postscript printer.
T}
a2ps-a4-4.3#T{
print
T}#T{
Formats an ascii file for printing on a postscript printer.
T}
aXe-6.1.2#T{
editors
T}#T{
Simple to use text editor for X.
T}
acm-4.7#T{
games
T}#T{
A flight simulator for X11
T}
acroread-3.01#T{
print
T}#T{
View, distribute and print PDF documents.
T}
acs-0.21#T{
cad
T}#T{
A general purpose circuit simulator.
T}
adcomplain-2.78#T{
mail news
T}#T{
complain about inappropriate commercial use (f.e. SPAM) of usenet/e-mail
T}
aero-1.5.2#T{
graphics
T}#T{
An X11 based modeler for povray
T}
afio-2.4.2#T{
sysutils
T}#T{
Archiver & backup program w/ builtin compression
T}
afm-1.0#T{
print
T}#T{
Adobe Font Metrics.
T}
afterstep-1.0#T{
x11
T}#T{
This window manager is a continuation of the Bowman NeXTSTEP clone.
T}
aftp-1.0#T{
emulators
T}#T{
A ftp-like shell for accessing Apple II disk images.
T}
agm-1.1#T{
games
T}#T{
AnaGram search utility
T}
agrep-2.04#T{
textproc
T}#T{
Approximate grep (fast approximate pattern-matching tool)
T}
amanda-2.3.0#T{
misc
T}#T{
The Advanced Maryland Automatic Network Disk Archiver
T}
amiwm-0.20p28#T{
x11
T}#T{
A window manager that makes your desktop look like an Amiga(TM).
T}
amp-0.7.6#T{
audio
T}#T{
another mp3 player
T}
an-0.93#T{
games
T}#T{
fast anagram generator
T}
analog-2.11#T{
www
T}#T{
An extremely fast program for analysing WWW logfiles.
T}
angband-2.8.2#T{
games
T}#T{
Rogue-like game with color, X11 support.
T}
apache-1.2.4#T{
www
T}#T{
The extremely popular Apache http server.  Very fast, very clean.
T}
apache-1.3b3#T{
www
T}#T{
The extremely popular Apache http server.  Very fast, very clean.
T}
apache-php-1.2.1#T{
www
T}#T{
The extremely popular Apache http server with php support.
T}
apacheSSL-1.2.4#T{
www
T}#T{
Apache secure https server using SSL
T}
apc-1.0#T{
math
T}#T{
An xforms based Auto Payment Calculator
T}
apsfilter-4.9.3#T{
print
T}#T{
lpd magic print filter with auto file type recognition
T}
arc-5.21e#T{
archivers
T}#T{
Create & extract files from DOS .ARC files.
T}
archie-1.4.1#T{
net
T}#T{
Prospero client for the archie service.
T}
arena-i18n-beta3b#T{
www
T}#T{
Experimental HTML 3 browser, supports math and style sheets.
T}
arpwatch-2.0.2#T{
net
T}#T{
Monitor arp & rarp requests
T}
asWedit-3.0#T{
editors www
T}#T{
An easy to use HTML and text editor
T}
ascend-radius-970424#T{
net
T}#T{
The Ascend modified Radius Daemon
T}
asclock-1.0#T{
x11
T}#T{
afterstep clock with some language extentions
T}
asedit-1.3.2#T{
editors
T}#T{
Text editor for X/Motif.
T}
asfiles-1.0#T{
x11
T}#T{
X11 file manager. Dockable in WindowMaker.
T}
ashe-1.1.2#T{
www
T}#T{
A simple HTML editor.
T}
asl-1.41r6#T{
devel
T}#T{
Assembler for a variety of microcontrollers/-processors.
T}
asmail-0.50#T{
mail x11
T}#T{
This is a biff type program, designed to match AfterStep.
T}
asprint-1.0#T{
print
T}#T{
A simple browser to allow a user to print.
T}
asrpages-0.1#T{
sysutils
T}#T{
alt.sysadmin.recovery manpage collection
T}
astrolog-5.30#T{
misc
T}#T{
An astrology program for X11 and alpha-numeric terminals
T}
atari800-0.8.1#T{
emulators
T}#T{
Atari 8-bit computer emulator
T}
atlast-1.0#T{
lang
T}#T{
Autodesk Threaded Language Application System Toolkit
T}
aub-2.0.5#T{
news
T}#T{
assemble usenet binaries
T}
auis-6.3.1#T{
x11
T}#T{
Andrew User Interface System
T}
autoconf-2.12#T{
devel
T}#T{
Automatically configure source code on many Un*x platforms
T}
automake-1.2#T{
devel
T}#T{
GNU Standards-compliant Makefile generator
T}
bash-1.14.7#T{
shells
T}#T{
The GNU Borne Again Shell.
T}
bash-2.01.1#T{
shells
T}#T{
The GNU Borne Again Shell.
T}
bb-1.04#T{
net
T}#T{
big brother Unix Network Monitor
T}
bcc-95.3.12#T{
devel lang
T}#T{
Bruce's C compiler (with as and ld); can do 16-bit code
T}
bclock-1.0#T{
x11
T}#T{
A round, analog X11 clock with bezier curve hands.
T}
beav-1.40.7#T{
editors
T}#T{
Binary Editor And Viewer, a full featured binary file editor.
T}
bibcard-0.6.4#T{
print databases
T}#T{
X11 interface for ediing BibTeX files.
T}
bibview-2.2#T{
databases print
T}#T{
Graphical interface for manipulating BibTeX bibliography databases
T}
bind-8.1.1#T{
net
T}#T{
The Berkeley Internet Name Daemon, an implementation of DNS.
T}
bing-1.0.4#T{
net
T}#T{
Bing is a point-to-point bandwith measurement tool.
T}
blas-1.0#T{
math
T}#T{
Basic Linear Algebra, level 1, 2, and 3.
T}
blast-1.0#T{
x11
T}#T{
Blast blows holes through windows.
T}
block-0.5#T{
games
T}#T{
Small text based maze game
T}
blt-2.1#T{
x11 tk41
T}#T{
A Tk extension (with shared libs)
T}
blue-2.3#T{
games
T}#T{
A Blue Moon card solitaire
T}
boehm-gc-4.10#T{
devel
T}#T{
Garbage collection and memory leak detection for C and C++.
T}
bonnie-1.0#T{
benchmarks
T}#T{
Performance Test of Filesystem I/O.
T}
bpatch-1.0#T{
editors
T}#T{
A hex editor that doesn't load the file at once.
T}
bpl+-1.0#T{
comms
T}#T{
B Plus file transfer protocol
T}
bricons-3.0#T{
x11
T}#T{
Quick start up utility for applications on an X display.
T}
bs-2.1#T{
games
T}#T{
Battleships solitaire game with a color interface
T}
bsddip-1.02#T{
net
T}#T{
Dialup IP program
T}
bsvc-2.0#T{
emulators tk41
T}#T{
An extensible hardware simulation framework with MC68K support
T}
btoa-5.2.1#T{
converters
T}#T{
Encode/decode binary to printable ASCII.
T}
buffer-1.17#T{
misc
T}#T{
buffer sporadic binary I/O for faster tape use
T}
bulk_mailer-1.5#T{
mail
T}#T{
Speeds delivery to large mailing lists by sorting & batching addresses.
T}
bwbasic-2.20#T{
lang
T}#T{
The Bywater Basic interpreter.
T}
bytebench-3.1#T{
benchmarks
T}#T{
The BYTE magazine benchmark suite.
T}
bzip-0.21#T{
archivers
T}#T{
A block-sorting file compressor.
T}
bzip2-0.1p2#T{
archivers
T}#T{
A block-sorting file compressor
T}
c2html-0.1#T{
textproc www
T}#T{
C-language sources to HTML converter
T}
c2ps-3.0#T{
print
T}#T{
A PostScript pretty-printer for C source.
T}
cal-3.5#T{
misc
T}#T{
Enhanced color version of standard calendar utility
T}
calc-2.9.3#T{
math
T}#T{
Arbitrary precision calculator.
T}
calctool-2.4.13#T{
math
T}#T{
a multi-GUI (text, X, xview, NeWS, sunview) calculator program
T}
cam-1.02#T{
audio
T}#T{
Cpu's Audio Mixer [curses based]
T}
camediaplay-971009#T{
graphics
T}#T{
digital camera downloading tool for Sanyo protocol (Epson/Sanyo/Olympus/Agfa)
T}
camltk41-1.0#T{
x11
T}#T{
A library for interfacing Objective Caml with Tcl/Tk
T}
cap-6.0.198#T{
net
T}#T{
Columbia AppleTalk Package for UNIX, communication between Macintosh & UNIX
T}
cbb-0.73#T{
misc
T}#T{
Checkbook balancing tool
T}
cccc-2.1.1#T{
devel textproc www
T}#T{
C and C++ Code Counter
T}
cd-write-1.4#T{
sysutils
T}#T{
A X11 based CD-burner
T}
cddbd-1.3.1p1#T{
audio net
T}#T{
Internet CD Database Server.
T}
cdplay-0.9#T{
audio
T}#T{
Neat cd-player with console user interface
T}
cdrecord-1.5#T{
sysutils
T}#T{
This program allows you to create CD's on a CD-Recorder.
T}
cflow-2.0#T{
devel
T}#T{
A call graph generator for C code.
T}
cfs-1.3.3#T{
security
T}#T{
A cryptographic file system implemented as a user-space NFS server.
T}
cgiparse-0.8c#T{
devel
T}#T{
C library to parse CGI Forms
T}
cgoban-1.9.2#T{
games
T}#T{
Internet Go Server client and game editor
T}
chimera-1.65#T{
www
T}#T{
X/Athena World-Wide Web client
T}
chimera-2.0a11#T{
www x11
T}#T{
X/Athena World-Wide Web client - Wilbur (HTML3.2) compliant
T}
chipmunk-5.10#T{
cad
T}#T{
An electronic CAD system.
T}
choparp-971007#T{
net
T}#T{
simple proxy arp daemon.
T}
chord-3.6#T{
misc
T}#T{
Produce PS sheet-music from text input
T}
cim-1.92#T{
lang
T}#T{
Compiler for the SIMULA programming language
T}
clog-1.6#T{
net security
T}#T{
tcp connection logger daemon
T}
cmucl-18a#T{
lang
T}#T{
The CMU implementation of Common Lisp
T}
cnews-cr.g#T{
news
T}#T{
C-news
T}
colorls-2.2.5#T{
misc
T}#T{
An ls that can use color to display file attributes
T}
comline-4.0D#T{
www
T}#T{
W3C Command Line WWW Tool
T}
connect4-3.2#T{
games
T}#T{
A curses version of the classic game.
T}
conserver-5.21#T{
comms
T}#T{
Manage remote serial consoles via TCP/IP
T}
contool-3.3a#T{
sysutils
T}#T{
Console tool for openlook
T}
cops-1.04#T{
security
T}#T{
A system secureness checker.
T}
cosmo-2.0.4#T{
games
T}#T{
Clone of Cosmo Gang the Puzzle (Namco)
T}
cpmemu-0.2#T{
emulators
T}#T{
Cpm emulator.
T}
cpmtools-1.1#T{
emulators
T}#T{
Utility to transfer files from/to CP/M (R) diskettes.
T}
crack-5.0#T{
security
T}#T{
the "Sensible" Unix Password Cracker.
T}
crossfire-0.93.0#T{
games
T}#T{
multiplayer graphical arcade and adventure game made for X-Windows
T}
crossgo32-1.3#T{
devel
T}#T{
Cross Development Environment for 32-bit DOS
T}
crossgo32-djgpp2-2.01#T{
devel
T}#T{
DJGPP V2 libraries and compatability for crossgo32 crosscompiler
T}
crossgo32-djgpp2-pdcurses-2.2#T{
devel
T}#T{
Public domain curses for crossgo32 crosscompiler with djgpp v2 libraries
T}
crosssco-1.3#T{
devel
T}#T{
SCO (R) Cross Development Environment
T}
cs-0.3#T{
devel
T}#T{
Interactively examine C source code
T}
ctk-4.1#T{
misc tk41
T}#T{
A curses port of John Ousterhout's Tk toolkit for X11.
T}
ctwm-3.5#T{
x11
T}#T{
An extension to twm, with support for multiple virtual screens, etc.
T}
cucipop-1.21#T{
mail
T}#T{
Cubic Circle's POP3 daemon (fully RFC1939 compliant).
T}
cutils-1.4#T{
devel
T}#T{
Miscellaneous C programmer's utilities
T}
cvsup-15.2#T{
devel net
T}#T{
A network file distribution and update system for CVS repositories.
T}
cvsup-bin-15.2#T{
devel net
T}#T{
A network file distribution and update system for CVS repositories.
T}
cvsup-mirror-1.0#T{
net
T}#T{
A kit for easily setting up a FreeBSD mirror site using CVSup.
T}
cxref-1.4#T{
devel
T}#T{
C program cross-referencing & documentation tool.
T}
cyrproxy-1.4.2#T{
russian net www
T}#T{
Cyrillic proxy for network protocols
T}
cyrus-1.5.2#T{
mail
T}#T{
the cyrus mail server, supporting POP3, KPOP, and IMAP4 protocols.
T}
db-2.3.12#T{
databases
T}#T{
the Berkeley DB package, revision 2
T}
dc3play-970716#T{
graphics
T}#T{
digital camera downloading tool for Ricoh DC-3
T}
dclock-pl4#T{
x11
T}#T{
A 7-segment digital clock with optional military time and alarm
T}
ddd-2.1.1#T{
devel
T}#T{
Data Display Debugger -- a common graphical front-end for GDB/DBX/XDB
T}
de-manpages-1.0#T{
german
T}#T{
German GNU and Linux manual pages.
T}
de-spinne-1.0.1#T{
german cad
T}#T{
A simulator for digital circuits.
T}
dejagnu-1.3#T{
misc tk80
T}#T{
Automated program/system tester
T}
delegate-4.3.4#T{
net www japanese
T}#T{
General purpose TCP/IP proxy system
T}
detex-2.6#T{
print
T}#T{
Strips TeX/LaTeX codes from a file
T}
dgd-1.1p4#T{
net lang
T}#T{
Dworkin's Generic Driver (network server)
T}
dgd-lpmud-2.4.5#T{
net devel games
T}#T{
LPmud mudlib, for use with DGD.
T}
dgd-net-1.1p4#T{
net lang
T}#T{
Dworkin's Generic Driver (network server) + extra networking support + regexps
T}
diablo-1.10#T{
news
T}#T{
a news transit system for backbone news feeders.
T}
dict-1.0#T{
german
T}#T{
simple english/german dictionary
T}
display-1.0#T{
misc
T}#T{
runs a specified command over and over.
T}
dlmalloc-2.6.4#T{
devel
T}#T{
Small, fast malloc library by Doug Lea.
T}
dmake-4.1#T{
devel
T}#T{
Another hyper make utility.
T}
dnews-4.3f#T{
news
T}#T{
commercial nntp server with feature and speed enhancements over inn and cnews
T}
docbook-3.0#T{
textproc
T}#T{
An SGML DTD designed for computer documentation
T}
donkey-0.5#T{
security
T}#T{
An alternative for S/KEY's key command
T}
dontspace-1.2#T{
games
T}#T{
A solitaire game for X11 modeled after Free Space.
T}
doom-1.8#T{
games
T}#T{
Id Software's Doom for linux
T}
dotfile-2.0#T{
misc tk80
T}#T{
A GUI dotfile generator program to create .config files
T}
dvi2tty-1.0#T{
print
T}#T{
A dvi-file previewer for text only devices.
T}
dvi2xx-0.51a9#T{
print
T}#T{
Convert dvi files to HP LaserJet or IBM 3812 format.
T}
dvips-5.74#T{
print
T}#T{
Convert a TeX DVI file to PostScript.
T}
dvips2ascii-1.0#T{
print
T}#T{
PostScript (created by dvips) to ascii converter
T}
dviselect-1.3#T{
print
T}#T{
Extract pages from DVI files.
T}
e93-1.2.6#T{
editors
T}#T{
A nifty editor based on Tcl/Tk
T}
ecc-1.3.2#T{
misc
T}#T{
GNU error-correcting code library and sample program.
T}
echoping-2.1b#T{
net
T}#T{
A ping-like program that uses tcp and/or http.
T}
ecu-4.08#T{
comms
T}#T{
Extended Call Utility
T}
eiffel-13a#T{
lang
T}#T{
A compiler for the object-oriented language Eiffel
T}
eispack-1.0#T{
math
T}#T{
Eigenvalue system package.
T}
electricfence-2.0.5#T{
devel
T}#T{
A debugging malloc() that uses the VM hardware to detect buffer overruns.
T}
elk-3.0.2#T{
lang
T}#T{
An embeddable Scheme interpreter.
T}
elm-2.4ME+32#T{
mail
T}#T{
ELM Mail User Agent
T}
emacs-19.34b#T{
editors
T}#T{
GNU editing macros.
T}
emiclock-1.0.2#T{
x11
T}#T{
Hyper-animated face analog clock for X11
T}
empire-1.1#T{
games
T}#T{
Solitaire empire game `VMS Empire'
T}
emu-1.3.1#T{
x11
T}#T{
A terminal emulator for the X Window System.
T}
enlightenment-0.13#T{
x11
T}#T{
a very artistic X window manager.
T}
enscript-A4-1.5.0#T{
print
T}#T{
ASCII-to-PostScript filter.
T}
eperl-2.2.8#T{
lang www perl5
T}#T{
Embedded Perl 5 Language
T}
ephem-4.28#T{
astro
T}#T{
an interactive terminal-based astronomical ephemeris program
T}
es-0.9a1#T{
shells
T}#T{
An extensible shell, derived from plan9's rc
T}
estic-1.40#T{
misc
T}#T{
Controller for ISDN TK-Anlage (PBX, Private Branch Exchange) made by Istec
T}
eterm-0.6a2#T{
x11
T}#T{
X-Windows terminal emulator based on rxvt/xterm
T}
etlfonts-noncjk-1.0#T{
x11
T}#T{
X11 supplemental fonts
T}
exim-1.73#T{
mail
T}#T{
High performance MTA for Unix systems on the Internet.
T}
exmh-1.6.9#T{
mail tk41
T}#T{
X11/TK based mail reader front end to MH.
T}
exmh-2.0z#T{
mail tk80
T}#T{
X11/TK based mail reader front end to MH.
T}
expect-5.25#T{
lang tcl80 tk80
T}#T{
A sophisticated scripter based on tcl/tk.
T}
explorer-0.72#T{
x11
T}#T{
File manager which has the same look and feel of the Windows95(tm)
T}
f77flow-0.12#T{
devel
T}#T{
Analyze the structure of a fortran77 program
T}
faces-1.6.1#T{
mail
T}#T{
visual mail, user and print face server
T}
fbsd-icons-1.0#T{
x11
T}#T{
a collection of icons related to the FreeBSD project (daemon gifs and such)
T}
fd-1.03e#T{
misc
T}#T{
A file and directory management tool
T}
felt-3.02#T{
cad
T}#T{
A system for Finite Element Analysis
T}
femlab-1.1#T{
math cad
T}#T{
Interactive program for solving partial differential equations in 2D
T}
fep-1.0#T{
misc
T}#T{
A general purpose front end for command line editing.
T}
fetchmail-4.3.3#T{
mail
T}#T{
batch mail retrieval/forwarding utility for pop2, pop3, apop, imap
T}
fftpack-1.0#T{
math
T}#T{
biharmonic equation in rectangular geometry and polar coordinates
T}
figlet-2.2#T{
misc
T}#T{
sysV banner like program prints strings in fancy ASCII art large characters
T}
filerunner-2.4#T{
x11 tk80
T}#T{
Filemanager with FTP capabilities. Uses Tcl/Tk.
T}
flying-6.20#T{
games
T}#T{
Pool/snooker/billiards/carrom/etc game.
T}
fmsx-1.5#T{
emulators
T}#T{
The Portable MSX/MSX2/MSX2+ Emulator
T}
fping-1.20#T{
net
T}#T{
quickly ping N hosts to determine their reachability w/o flooding the network
T}
fpp-1.1#T{
devel
T}#T{
Fortran preprocessor for FORTRAN 77 and Fortran 90 programs.
T}
freeWAIS-0.5#T{
net
T}#T{
freeWAIS from CNIDR
T}
freeciv-1.0k#T{
games
T}#T{
A civilisation clone for x;  multiplayer
T}
freefem-3.4#T{
math cad
T}#T{
A language for the Finite Element Method
T}
freefonts-0.10#T{
x11
T}#T{
A collection of ATM fonts from the CICA archives.
T}
freewais-sf-2.1.2#T{
net databases
T}#T{
An enhanced Wide Area Information Server
T}
freeze-2.5#T{
archivers
T}#T{
FREEZE / MELT compression program - often used in QNX
T}
fspclient.0.0.h#T{
net
T}#T{
A client for the fsp service.
T}
ftpsearch-1.0#T{
net
T}#T{
A system for indexing contents on ftp servers.
T}
ftptool-4.6#T{
net
T}#T{
Graphic ftp shell based on xview
T}
fvwm-1.24r#T{
x11
T}#T{
the fvwm window manager
T}
fvwm-2.0.43#T{
x11
T}#T{
beta of the fvwm window manager - requires XPM
T}
fvwm-2.0.46#T{
x11
T}#T{
Internationalized (not japanised) fvwm version 2, a window manager for X
T}
fvwm95-2.0.43a#T{
x11
T}#T{
Win95 lookalike version of the fvwm2 window manager.
T}
fwf-3.8#T{
x11
T}#T{
The Free Widget Foundation Widget Release
T}
fwtk-1.3#T{
security net
T}#T{
A toolkit used for building firewalls based on proxy services
T}
fxhtml-1.6.7#T{
www
T}#T{
Server side extension to HTML which eliminates the need for CGI scripts.
T}
fxtv-0.45#T{
graphics x11
T}#T{
X-based TV-Card Display and Capture Application (for use with bt848 driver)
T}
g77-0.5.19.1#T{
lang math
T}#T{
The GNU Fortran 77 compiler.
T}
galaxis-1.1#T{
games
T}#T{
Clone of the nifty little Macintosh game
T}
gated-3.5.7#T{
net
T}#T{
Routing protocol daemon.
T}
gcc11-2.6.3#T{
devel
T}#T{
The gcc-2.6.3 cross-compiler for the 6811
T}
gcl-2.2.2#T{
lang
T}#T{
GNU Common Lisp
T}
gd-1.2#T{
graphics
T}#T{
A graphics library for fast GIF creation
T}
gdb-4.16#T{
devel
T}#T{
The developer's version of GNU debugger.
T}
gdbm-1.7.3#T{
databases
T}#T{
The GNU database manager.
T}
gdbtk-4.16#T{
devel tk42
T}#T{
Tk interface to gdb.
T}
geomview-1.6.1#T{
graphics tk42
T}#T{
an interactive viewer for 3- and 4-D geometric objects
T}
getbdf-1.0#T{
x11
T}#T{
convert any X server font to .bdf format
T}
gfont-1.0.2#T{
graphics print www
T}#T{
Graphics Font - Create GIF image rendered with TeX-available Font
T}
ghostscript-2.6.2#T{
print
T}#T{
GNU Postscript interpreter.
T}
ghostscript-3.53#T{
print
T}#T{
Aladdin Postscript interpreter.
T}
ghostscript-4.03#T{
print
T}#T{
Aladdin Postscript interpreter.
T}
ghostscript-5.03#T{
print
T}#T{
Aladdin Postscript interpreter.
T}
ghostview-1.5#T{
print
T}#T{
An X11 front-end for ghostscript, the GNU postscript previewer.
T}
giflib-3.0#T{
graphics
T}#T{
Tools and library routines for working with GIF images.
T}
gifmerge-1.33#T{
graphics
T}#T{
A tool for making a GIF Animation.
T}
giftool-1.0#T{
graphics
T}#T{
a tool for GIF89a transparent option and interlace mode
T}
giftrans-1.12#T{
graphics
T}#T{
a tool for GIF89a transparent option and inerlace mode.
T}
gimp-0.99.14#T{
graphics
T}#T{
developer's beta release of the General Image Manipulation Program
T}
git-4.3.16#T{
misc
T}#T{
GNU Interactive Tools - a file system browser for UNIX systems
T}
glimpse-4.0#T{
textproc databases
T}#T{
Text search engine
T}
gmake-3.76.1#T{
devel
T}#T{
GNU version of 'make' utility
T}
gn-2.24#T{
www net
T}#T{
GN gopher and http server
T}
gnat-3.09#T{
lang
T}#T{
The GNU Ada Translator.
T}
gnats-3.104b#T{
databases
T}#T{
Cygnus GNATS bug tracking system.
T}
gnu-finger-1.37#T{
net
T}#T{
GNU version of finger.
T}
gnuchess-4.0.77#T{
games
T}#T{
"Classic" Gnu Chess
T}
gnugo-1.2#T{
games
T}#T{
The game of Go.
T}
gnuls-3.16#T{
misc
T}#T{
FreeBSD port of colorized GNU `ls'.
T}
gnuplot-336#T{
math graphics
T}#T{
A command-driven interactive function plotting program.
T}
gnushogi-1.2.3#T{
games
T}#T{
GNU version of Shogi
T}
gofer-2.30a#T{
lang
T}#T{
A lazy functional language.
T}
golddig-2.0#T{
games
T}#T{
Getting the Gold and Avoiding Death.
T}
gopher-2.1.3#T{
net
T}#T{
Client and server for access to a distributed document service.
T}
gsm-1.0.10#T{
audio
T}#T{
Audio converter and library for converting u-law to gsm encoding.
T}
gtk-971201#T{
x11
T}#T{
General Toolkit for X11 GUI.
T}
guavac-1.0#T{
lang
T}#T{
Guavac, a java compiler and decompiler developed under GPL.
T}
guile-1.2#T{
lang
T}#T{
GNU's Ubiquitous Intelligent Language for Extension
T}
gv-3.5.8#T{
print
T}#T{
A PostScript and PDF previewer.
T}
gvim-4.6#T{
editors
T}#T{
A vi "workalike", with many additional features.
T}
gwm-1.8c#T{
x11
T}#T{
Generic Window Manager
T}
gwstat-1.13.1.3#T{
www
T}#T{
generate GIF graphs that illustrate the httpd server traffic
T}
ha-0.999b#T{
archivers
T}#T{
The HA archiver using the HSC compression method.
T}
harvest-1.5#T{
www
T}#T{
Collect information from all over the Internet
T}
hdf-4.1r1#T{
graphics
T}#T{
Hierarchical Data Format library (from NCSA)
T}
hexcalc-1.11#T{
math
T}#T{
A multi-radix calculator for x11
T}
hfs-0.37#T{
emulators
T}#T{
Read Macintosh HFS floppy disks, hard drives and CDROMs.
T}
hfsutils-2.0#T{
emulators
T}#T{
Yet Another HFS Utility
T}
hobbes-icons-xpm3-1.0#T{
graphics
T}#T{
collection of over 3000 icons in XPM3 format.
T}
hpack-0.79a#T{
archivers
T}#T{
Multi-System Archiver with open keys PGP-based security.
T}
hpscan-1.0#T{
graphics
T}#T{
HP scanner driver.
T}
html-4.0#T{
textproc
T}#T{
All W3C published SGML DTDs for HTML
T}
html2latex-0.9#T{
print
T}#T{
convert HTML document into LaTeX
T}
http-analyze-1.9#T{
www
T}#T{
A fast Log-Analyzer for web servers
T}
hugs-1.4#T{
lang
T}#T{
Nottingham's and Yale's Haskell interpreter/programming environment.
T}
hylafax-4.0.1#T{
comms
T}#T{
A fax software.
T}
ical-2.1b2#T{
misc
T}#T{
A calendar application.
T}
icewm-0.8.6#T{
x11
T}#T{
cool window manager developped in a very hot day in a winter
T}
icmpinfo-1.11#T{
net sysutils
T}#T{
looks at the icmp messages received by the host
T}
icon-9.3#T{
lang
T}#T{
The Icon programming language.
T}
id-utils-3.2#T{
devel
T}#T{
The classic Berkeley gid/lid tools for looking up variables in code.
T}
idl4#T{
lang
T}#T{
Research Systems Inc.'s Interactive Data Language
T}
idled-1.16#T{
sysutils
T}#T{
A daemon that logs out idle users and those users hogging resources.
T}
ifmail-2.12#T{
news
T}#T{
FidoNet(tm) support package for UN*X platform
T}
ile-2.0#T{
misc
T}#T{
An Input Line Editor that wraps itself around programs.
T}
ilu-2.0a11#T{
devel
T}#T{
Xerox PARC ILU - CORBA-compatible distributed objects for multiple languages
T}
im-76#T{
mail
T}#T{
A set of user interfaces of Email and NetNews.
T}
imap-uw-4.1b#T{
mail
T}#T{
University of Washington IMAPv4.1/POP2/POP3 mail servers
T}
imaze-1.3#T{
games
T}#T{
A multi-player network action game for TCP/IP with 3D graphics.
T}
imlib-0.11#T{
graphics
T}#T{
a graphic library for enlightenment package
T}
imm-3.5a1#T{
mbone tk80
T}#T{
Internet Image(or other data) Multicaster (and receiver).
T}
indent-1.9.1#T{
devel
T}#T{
GNU indent.
T}
ines-0.7#T{
emulators
T}#T{
Nintendo Entertainment System emlator for X
T}
info2html-1.1#T{
textproc
T}#T{
Translate GNU info files into HTML pages.
T}
inn-1.5.1sec2#T{
news
T}#T{
InterNetNews -- the Internet meets Netnews.
T}
iozone-2.01#T{
benchmarks
T}#T{
Performance Test of Sequential File I/O.
T}
irc-2.9.2.3#T{
net
T}#T{
The 'Internet Relay Chat' Server.
T}
ircii-2.8.2-epic3.004#T{
net
T}#T{
An enhanced version of ircII, the Internet Relay Chat client.
T}
ircii-2.9#T{
net
T}#T{
The 'Internet Relay Chat' Client.
T}
irsim-9.4.1#T{
cad
T}#T{
An event-driven logic-level simulator for MOS circuis.
T}
isc-dhcp-b5.16#T{
net
T}#T{
ISC Dynamic Host Configuration Protocol client and server code
T}
isc-dhcp2.b1.0#T{
net
T}#T{
ISC Dynamic Host Configuration Protocol client and server code
T}
isearch-1.14#T{
textproc databases
T}#T{
Text Search Engine by CNIDR
T}
ish-1.11#T{
converters
T}#T{
ish binary-to-text file converter
T}
iso12083-1993#T{
textproc
T}#T{
SGML DTDs from the The Electronic Publishing Special Interest Group
T}
iso8879-1986#T{
textproc
T}#T{
Character entity sets from ISO 8879:1986 (SGML)
T}
ispell-3.1.20#T{
textproc
T}#T{
An interactive spelling checker.
T}
itcl-2.1#T{
lang
T}#T{
[incr Tcl] (A.K.A. ``itcl'').
T}
iv-3.1#T{
x11
T}#T{
InterViews: A toolkit from Stanford University and Silicon Graphics.
T}
ivs-3.4#T{
net
T}#T{
INRIA Videoconference Software.
T}
ja-Canna-3.2.2#T{
japanese
T}#T{
Kana-Kanji conversion system
T}
ja-Wnn-4.2#T{
japanese
T}#T{
A Japanese/Chinese/Korean input method (only Japanese built)
T}
ja-Wnn6-97.6.6#T{
japanese
T}#T{
A Japanese input method (this is not free)
T}
ja-Wnn6-lib-97.6.6#T{
japanese
T}#T{
include files and a library of Wnn6
T}
ja-a2ps-1.41#T{
japanese print
T}#T{
Text file to postscript converter (with Japanese support)
T}
ja-camltk41-1.0#T{
x11 japanese
T}#T{
A library for interfacing Objective Caml with Tcl/Tk
T}
ja-cdrom2-1996.06.16#T{
japanese misc
T}#T{
A tool to lookup CD-ROM dictionaries
T}
ja-chimera-1.65#T{
japanese www
T}#T{
X/Athena World-Wide Web client + kanji patch
T}
ja-ckinput2-2.0.1#T{
japanese x11
T}#T{
An input server of Japanese text (Canna version)
T}
ja-cskinput2-2.0.1#T{
japanese x11
T}#T{
An input server of Japanese text (Canna+SJ3 version)
T}
ja-cwkinput2-2.0.1#T{
japanese x11
T}#T{
An input server of Japanese text (Canna+Wnn version)
T}
ja-dserver-2.2.2#T{
japanese
T}#T{
CDROM dictionary server & clients
T}
ja-dvi2ps-2.0#T{
japanese print
T}#T{
DVI to PostScript converter Japanese version
T}
ja-dvi2tty-ascii-5.0#T{
japanese print
T}#T{
Character-based DVI file previewer.
T}
ja-elisa8-1.0#T{
japanese x11
T}#T{
X11 8-dot kanji font 'elisa font'
T}
ja-elvis-1.8.4#T{
japanese editors
T}#T{
A clone of vi/ex, the standard UNIX editor, with Japanese patch.
T}
ja-escpf-0.4b2#T{
japanese print
T}#T{
Text filters for ESC/P, ESC/Page and ESC/PS printers
T}
ja-ewipe-0.6.0#T{
japanese x11
T}#T{
tcl/tck-based presentation tool.
T}
ja-expect-5.25#T{
japanese lang
T}#T{
A sophisticated scripter based on Japanized tcl/tk.
T}
ja-gawk-2.15.6#T{
japanese
T}#T{
GNU awk + multi-byte extension.
T}
ja-ghostscript300-2.6.1.4#T{
japanese print
T}#T{
GNU Postscript interpreter + Japanese patch.
T}
ja-gn-gnspool-1.35#T{
japanese news
T}#T{
Simple Japanese Newsreader with Local Spool Support
T}
ja-gp-2.01#T{
japanese print
T}#T{
A GUI Printer manager written with Tcl/Tk.
T}
ja-grep-2.0#T{
japanese
T}#T{
GNU grep + multi-byte extension.
T}
ja-groff-0.99#T{
japanese print
T}#T{
Japanese enhancement of GNU groff
T}
ja-gxditview-0.98#T{
japanese print
T}#T{
Japanized GNU's modified xditview
T}
ja-handbook-2.2#T{
japanese
T}#T{
The Japanese version of FreeBSD handbook.
T}
ja-hex-2.03#T{
japanese
T}#T{
A hexadecimal dump tool which handles Japanese.
T}
ja-ircii-2.8.2#T{
japanese net
T}#T{
The 'Internet Relay Chat' Client.
T}
ja-iv-3.1#T{
japanese
T}#T{
A toolkit from Stanford University and Silicon Graphics + Japanese patches
T}
ja-japaneseAFM-1.0#T{
japanese print
T}#T{
Japanese AFM fonts.
T}
ja-jlatex209-a17-n152#T{
japanese print
T}#T{
Japanese TeX based on LaTeX-209 with both NTT and ASCII
T}
ja-jlatex209-a17#T{
japanese print
T}#T{
ASCII Japanese pTeX based on LaTeX-209
T}
ja-jlatex209-n152#T{
japanese print
T}#T{
NTT Japanese TeX based on LaTeX-209
T}
ja-k12font-1.0#T{
japanese x11
T}#T{
X11 12-dot kanji font
T}
ja-kakasi-2.2.5#T{
japanese
T}#T{
Kanji-Kana Simple Inverter, language filter for Japanese.
T}
ja-kcc-1.0#T{
japanese
T}#T{
Kanji code conversion Filter
T}
ja-kinput2-2.0.1#T{
japanese x11
T}#T{
An input server of Japanese text (Canna+SJ3+Wnn version)
T}
ja-kon2-0.3#T{
japanese
T}#T{
Kanji On Console -- Display kanji characters on your own console.
T}
ja-kterm-6.2.0#T{
japanese x11
T}#T{
An xterm that speaks Japanese
T}
ja-less-332#T{
japanese
T}#T{
less + zcat + ISO-2022 - a pager similar to more and pg
T}
ja-lipsf-1.13c#T{
japanese print
T}#T{
text to LIPS filter
T}
ja-man-1.1c#T{
japanese
T}#T{
A manual display command for Japanese (EUC).
T}
ja-man-doc-2.2.2g#T{
japanese
T}#T{
Japanese online manual pages corresponding to /usr/share/man/man[18]
T}
ja-mew-1.92.4#T{
japanese mail
T}#T{
Message interface to Emacs Window(for japanese mule)
T}
ja-mh-6.8.3#T{
japanese mail
T}#T{
Rand MH mail handling system + Japanese patches
T}
ja-mimekit-1.7#T{
japanese devel mail
T}#T{
Library to handle messages with MIME-encoded headers.
T}
ja-mmm-0.40#T{
www japanese
T}#T{
WWW browser using Objective Caml, Tcl/Tk.
T}
ja-mnews-1.20#T{
japanese news
T}#T{
Simple news and E-mail reader
T}
ja-mule-canna+sj3+wnn4-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with Canna, sj3 and Wnn4 support built in (Only the executables)
T}
ja-mule-canna+sj3+wnn6-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with Canna, sj3, Wnn4 and Wnn6 support built in (Only the executables)
T}
ja-mule-canna+sj3-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with Canna and sj3 support built in (Only the executables)
T}
ja-mule-canna+wnn4-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with Canna and Wnn4 support built in (Only the executables)
T}
ja-mule-canna+wnn6-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with Canna, Wnn4 and Wnn6 support built in (Only the executables)
T}
ja-mule-canna-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with Canna support built in (Only the executables)
T}
ja-mule-sj3+wnn4-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with sj3 and Wnn4 support built in (Only the executables)
T}
ja-mule-sj3+wnn6-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with sj3, Wnn4 and Wnn6 support built in (Only the executables)
T}
ja-mule-sj3-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with SJ3 support built in (Only the executables)
T}
ja-mule-wnn4-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with Wnn4 support built in (Only the executables)
T}
ja-mule-wnn6-2.3#T{
japanese editors
T}#T{
A multilingual emacs, with Wnn4 and Wnn6 support built in (Only the executables)
T}
ja-nethack-1.0.5.19970924#T{
japanese games
T}#T{
A dungeon explorin', slashin', hackin' game
T}
ja-newosaka-1.0#T{
japanese
T}#T{
translator of Japanese EUC documents into Osaka language
T}
ja-nkf-1.62#T{
japanese
T}#T{
Network Kanji code conversion Filter
T}
ja-nvi-eucjp-1.79-970820#T{
japanese editors
T}#T{
A clone of vi/ex, with multilingual patch, default settings for euc-jp.
T}
ja-nvi-iso2022jp-1.79-970820#T{
japanese editors
T}#T{
A clone of vi/ex, with multilingual patch, default settings for iso-2022-jp.
T}
ja-nvi-sjis-1.79-970820#T{
japanese editors
T}#T{
A clone of vi/ex, with multilingual patch, default settings for sjis.
T}
ja-okphone-1.2#T{
japanese net
T}#T{
conference-calling phone with Japanese support
T}
ja-oleo-1.6#T{
japanese math
T}#T{
A Spreadsheet Program + Japanese patches.
T}
ja-perl-5.004.01#T{
japanese perl5
T}#T{
Pattern Extraction and Recognition Language + Japanese patches.
T}
ja-pine-3.95#T{
japanese mail
T}#T{
Program for Internet News and E-mail with Japanese Support
T}
ja-pkfonts300-1.0#T{
japanese print
T}#T{
Japanese PK fonts, for ghostscripts, xdvi and so on.
T}
ja-plain2-2.54#T{
textproc
T}#T{
A text converter from plain to any format.
T}
ja-platex2e-common-97.07.02.2#T{
japanese print
T}#T{
Character code independent files for ASCII Japanese pLaTeX2e.
T}
ja-platex2e-euc-97.07.02.2#T{
japanese print
T}#T{
ASCII Japanese pLaTeX2e with Japanese EUC code support.
T}
ja-platex2e-jis-97.07.02.2#T{
japanese print
T}#T{
ASCII Japanese pLaTeX2e with JIS code support.
T}
ja-platex2e-sjis-97.07.02.2#T{
japanese print
T}#T{
ASCII Japanese pLaTeX2e with Shift-JIS code support.
T}
ja-prn-1.0#T{
japanese print
T}#T{
A yet another converter from text file to postscript (with Japanese support)
T}
ja-ptex-common-2.1.5#T{
japanese print
T}#T{
Character code independent files for ASCII Japanese TeX
T}
ja-ptex-euc-2.1.5#T{
japanese print
T}#T{
Japanese pTeX files to support EUC character set.
T}
ja-ptex-jis-2.1.5#T{
japanese print
T}#T{
Japanese pTeX files to support JIS character set.
T}
ja-ptex-sjis-2.1.5#T{
japanese print
T}#T{
Japanese pTeX files to support Shift-JIS character set.
T}
ja-qkc-1.0#T{
japanese
T}#T{
Quick Kanji Code Converter (C version)
T}
ja-recjis-1.0#T{
japanese
T}#T{
tool for recovery of broken japanese text
T}
ja-samba-1.9.17.4#T{
japanese net
T}#T{
A LanManager(R)-compatible server suite for Unix (config w/ Japanese filename)
T}
ja-samba-des-1.9.17.4#T{
japanese net
T}#T{
A LanManager(R)-compat. server suite w/ WinNT encrypted password + jp filename
T}
ja-sed-1.18#T{
japanese
T}#T{
GNU sed + multi-byte extension.
T}
ja-sj3-2.0.1.13#T{
japanese
T}#T{
A Japanese input method.
T}
ja-sjxa-1.5.11#T{
japanese x11
T}#T{
A X11 frontend of Japanese input method SJ3.
T}
ja-skinput2-2.0.1#T{
japanese x11
T}#T{
An input server of Japanese text (SJ3 version)
T}
ja-skk-9.6#T{
japanese
T}#T{
Simple Kana Kanji Converter: a Japanese-input software running on Nemacs or Mule.
T}
ja-swkinput2-2.0.1#T{
japanese x11
T}#T{
An input server of Japanese text (SJ3+Wnn version)
T}
ja-tcl-7.6#T{
japanese lang
T}#T{
Japanized Tcl (Tool Command Language).
T}
ja-tgif-3.0.13#T{
japanese
T}#T{
2-D drawing facility (Japanese version).
T}
ja-tk-4.2#T{
japanese x11
T}#T{
Japanized TK (Tcl Tool Kit).
T}
ja-today-2.10b#T{
japanese games
T}#T{
Tells you what day today is.
T}
ja-typist-2.0#T{
japanese
T}#T{
Typing lessons (Japanese version)
T}
ja-vfghostscript-2.6.2#T{
japanese print
T}#T{
GNU Postscript interpreter + Japanese VFontlib patch.
T}
ja-vfghostscript-5.03#T{
japanese print
T}#T{
Aladdin Postscript and PDF interpreter with Japanese vector font library.
T}
ja-vflib-2.22.10#T{
japanese print
T}#T{
Japanese Vector font library with free vector font.
T}
ja-vfxdvi300-17#T{
japanese print
T}#T{
DVI Previewer for X. + Japanese patch + vector font support
T}
ja-w3-2.2.26#T{
www japanese
T}#T{
WWW browser based on emacs/mule
T}
ja-weblint97-0.12#T{
japanese www
T}#T{
an internationalized HTML checker with japanese message(EUC-JP)
T}
ja-wkinput2-2.0.1#T{
japanese x11
T}#T{
An input server of Japanese text (Wnn version)
T}
ja-x0212fonts-1.0#T{
japanese x11
T}#T{
X11 X0212-1990 font.  14, 16, 24 and 40 dots availble.
T}
ja-xdvi300-17#T{
japanese print
T}#T{
A DVI Previewer for the X Window System with Japanese patch.
T}
ja-xshodo-2.0#T{
x11
T}#T{
A paint tool for Shodo, the Japanese traditional writing character
T}
ja-zipcodes-1.0#T{
japanese
T}#T{
japanese zipcode tables. includes both 3/5 and 7 digits form.
T}
jade-1.0.1#T{
textproc
T}#T{
An object-oriented SGML parser toolkit and DSSSL engine
T}
javac_netscape-1.0.1#T{
lang
T}#T{
Java compiler using Netscape 3.0+.
T}
jbigkit-0.9#T{
graphics
T}#T{
A lossless image compression library
T}
jdk-1.0.2#T{
lang devel
T}#T{
Java Developers Kit, Class Libraries
T}
jed-0.98.4#T{
editors
T}#T{
The JED text editor
T}
jetpack-1.0#T{
games
T}#T{
Arcade action game for X Windows.
T}
jive-1.1#T{
misc
T}#T{
filter that converts English text to Jive
T}
joe-2.8#T{
editors
T}#T{
Joe's own editor.
T}
john-1.4.2#T{
security
T}#T{
featureful Unix password cracker
T}
jove-4.16#T{
editors
T}#T{
Jonathan's Own Version of Emacs.
T}
jpeg-6a#T{
graphics
T}#T{
IJG's jpeg compression utilities.
T}
kaffe-0.9.2#T{
lang
T}#T{
A virtual machine capable of running Java(tm) code (including an awt-package)
T}
kaskade-3.1.1#T{
cad
T}#T{
Adaptive linear scalar elliptic and parabolic problem solver
T}
kde-1.2b#T{
x11 kde
T}#T{
The "meta-port" for the KDE integrated X11 desktop
T}
kdebase-2.1b#T{
x11 kde
T}#T{
Base modules for the KDE integrated X11 desktop
T}
kdegames-1.2b#T{
games kde
T}#T{
Games for the KDE integrated X11 desktop
T}
kdelibs-2.1b#T{
x11 kde
T}#T{
Support libraries for the KDE integrated X11 desktop
T}
kdenetwork-1.2b#T{
net kde
T}#T{
Network modules for the KDE integrated X11 desktop
T}
kdesupport-1.2b#T{
converters kde
T}#T{
Mime and UUENCODE/DECODE libraries for the KDE integrated X11 desktop
T}
kdeutils-2.1b#T{
misc kde
T}#T{
Utilities for the KDE integrated X11 desktop
T}
kermit-6.0.192#T{
comms
T}#T{
File transfer and terminal emulation utilitiy for serial lines and sockets.
T}
kirc-0.3.2#T{
net kde
T}#T{
KDE project's IRC client
T}
klondike-1.8#T{
games
T}#T{
A solitaire game for X11.
T}
knews-0.9.8#T{
x11 news
T}#T{
A threaded nntp newsreader for X.
T}
ko-Wnn-4.2#T{
korean
T}#T{
A Japanese/Chinese/Korean input method (only Korean built)
T}
ko-afterstep-1.0pr3h1#T{
korean x11
T}#T{
This Bowman-NeXTSTEP clone window manager with Korean character support.
T}
ko-elm-2.4h4.1#T{
korean mail
T}#T{
ELM Mail User Agent, patched for Korean E-Mail
T}
ko-fvwm95-2.0.42ah3#T{
korean x11
T}#T{
Win95 lookalike version of the fvwm2 window manager with Korean support.
T}
ko-h2ps-a4-1.0#T{
korean print
T}#T{
Formats an ascii file for printing on a postscript printer with Korean char.
T}
ko-han-1.0fb#T{
korean
T}#T{
A hangul console.
T}
ko-hanmutt-0.60h9#T{
korean mail
T}#T{
"The Mongrel of Mail User Agents" (part Elm, Pine, mh), with Hangul support
T}
ko-hanterm-304b3af#T{
korean x11
T}#T{
An xterm hacks for managing Korean languages.
T}
ko-hanterm-304b3#T{
korean x11
T}#T{
An xterm hacked for managing Korean languages.
T}
ko-hanterm-xf86-3.2#T{
korean x11
T}#T{
An X11R6-based xterm hacked for managing Korean languages.
T}
ko-hanyangfonts-1.0#T{
korean x11
T}#T{
Hangul fonts for X11(hanyang) used in many hangul-related programs.
T}
ko-hcode-2.1.2#T{
korean
T}#T{
Hangul code conversion utility
T}
ko-helvis-1.8h2-#T{
korean editors
T}#T{
A clone of vi/ex, the standard UNIX editor, supporting Hangul.
T}
ko-hfvwm-2.0.43#T{
korean x11
T}#T{
The fvwm2 window manager, with Korean support
T}
ko-hlatexpsfonts-0.95#T{
korean
T}#T{
Hangul Type 1 fonts for HLaTeX-0.95 and other applications
T}
ko-hmconv-1.0p3#T{
korean mail
T}#T{
Hangul code conversion utility for E-mail
T}
ko-hpscat-1.3jshin#T{
korean print
T}#T{
Hangul Text Printing Utility
T}
ko-johabfonts-304#T{
korean x11
T}#T{
Hangul fonts for X11(johab) used in many hangul-related programs.
T}
ko-linuxdoc-sgml-1.6bh2#T{
korean textproc
T}#T{
Korean patch version of Linuxdoc-SGML.
T}
ko-mule-wnn4-2.3#T{
korean editors
T}#T{
A multilingual emacs, with Wnn4 support built in (Only the executables)
T}
ko-netscape-3.01intl#T{
korean www
T}#T{
netscape-3.01 web-surfboard (international version, Korean)
T}
ko-nh2ps-a4-1.0p1#T{
korean print
T}#T{
Formats an ascii file for printing on a postscript printer with Korean char.
T}
ko-nhpf-1.42#T{
korean www
T}#T{
Hangul Printing Filter for Netscape with embedded font
T}
ko-nhppf-1.2#T{
korean
T}#T{
Hangul Printing Filter for Netscape with HLaTeX-0.95 PS font
T}
ko-nvi-euckr-1.79-970820#T{
korean editors
T}#T{
A clone of vi/ex, with multilingual patch, default settings for euc-kr.
T}
ko-nvi-iso2022kr-1.79-970820#T{
korean editors
T}#T{
A clone of vi/ex, with multilingual patch, default settings for iso-2022-kr.
T}
ko-pine-3.96k#T{
korean mail
T}#T{
Program for Internet E-mail and News, patched for Korean E-Mail transfer
T}
ko-pinetreefonts-1.0#T{
korean x11
T}#T{
Hangul fonts for X11(pinetree, KSC5601-1987-0 encoding)
T}
ko-ztelnet-1.0p1#T{
korean net
T}#T{
Telnet program with zmodem transfer.
T}
kp-0.96#T{
misc tk41
T}#T{
The Keyboard Practicer, touch-type training program
T}
ktamaga-0.2#T{
games kde
T}#T{
Tamagochi emulator for the KDE integrated X11 desktop
T}
lapack-2.0#T{
math
T}#T{
A library of Fortran 77 subroutines for linear algebra.
T}
latex2e-97.06#T{
print
T}#T{
LaTeX2e - a TeX macro package
T}
lcc-3.6#T{
lang
T}#T{
compiler from `A Retargetable C Compiler: Design and Implementation'
T}
lclint-2.2a#T{
devel
T}#T{
A tool for statically checking C programs.
T}
ldap-3.3#T{
net
T}#T{
An implementation of the Lightweight Directory Access Protocol
T}
leafnode-1.0.2#T{
news
T}#T{
Leafnode is a USENET software package designed for small sites.
T}
less-332#T{
misc
T}#T{
a better pager utility
T}
lesstif-0.82#T{
x11
T}#T{
API compatible clone of the Motif toolkit.
T}
lftp-0.13.2#T{
net
T}#T{
Shell-like command line ftp client
T}
lha-1.14c#T{
archivers
T}#T{
Archive files using LZW compression (.lzh files).
T}
libXgFonts-1.0#T{
plan9
T}#T{
These are the UNICODE fonts for use with 9term and sam.
T}
libdnd-1.1#T{
x11
T}#T{
Drag and drop library.
T}
libident-0.20#T{
devel net security
T}#T{
A small library to interface the ident protocol server (rfc1413).
T}
libmalloc-1.18#T{
devel
T}#T{
Small, fast malloc library with comprehensive error checking
T}
libpics-1.0#T{
www devel
T}#T{
The W3C sample PICS library
T}
libslang-0.99.38#T{
devel
T}#T{
Routines for rapid alpha-numeric terminal applications development
T}
libsocket++-1.10#T{
net
T}#T{
A C++ wrapper library to the sockets
T}
libsx-1.1#T{
x11
T}#T{
Simple X Windows library.
T}
libwww-5.1b#T{
www devel
T}#T{
The W3C Reference Library.
T}
lincity-1.09#T{
games
T}#T{
rich city simulation game for X
T}
linemode-4.0D#T{
www
T}#T{
WWW LineMode Browser from the W3 Consortium (W3C)
T}
linpack-1.0#T{
math
T}#T{
Linear Algebra package.
T}
linux-netscape-4.03#T{
www
T}#T{
netscape web-surfboard
T}
linux_devel-0.2#T{
devel
T}#T{
Linux cross development package
T}
linux_kdump-1.0#T{
devel
T}#T{
Linux-compatability ktrace.out processor.
T}
linux_lib-2.4#T{
emulators
T}#T{
These are the libraries necessary for linux compatibility.
T}
linuxdoc-1.0#T{
textproc
T}#T{
The Linuxdoc SGML DTD
T}
linuxgdb-4.16#T{
devel
T}#T{
A Linux debugger for cross-development purposes.
T}
llnlxdir-2.0#T{
net
T}#T{
Motif FTP client with drag-and-drop file transfer.
T}
llnlxftp-2.1#T{
net
T}#T{
Motif FTP client
T}
lmbench-1.1#T{
benchmarks
T}#T{
A system performance measurement tool
T}
logsurfer-1.41#T{
misc
T}#T{
Processes logfiles and perform certain actions.
T}
lout-3.10#T{
print
T}#T{
A document creation system like LaTeX, but smaller
T}
lprps-2.5#T{
print
T}#T{
PostScript printer filter package supporting a bidirectional serial channel
T}
lrzsz-0.12.16#T{
comms
T}#T{
Receive/Send files via X/Y/ZMODEM protocol.  (unrestrictive)
T}
lsof-4.15#T{
sysutils
T}#T{
Lists information about open files.
T}
luna-1.9#T{
astro games
T}#T{
Display her phase.
T}
lupe-0.07#T{
x11
T}#T{
Real-time magnifying glass for X11
T}
lynx-2.7.1ac-0.98#T{
www
T}#T{
An alphanumeric display oriented World-Wide Web Client.
T}
lynx-2.7.1#T{
www
T}#T{
An alphanumeric display oriented World-Wide Web Client.
T}
lyx-0.11.45#T{
print
T}#T{
A graphical frontend for LaTeX (nearly WYSIWYG)
T}
m4-1.4#T{
devel
T}#T{
GNU's m4.
T}
macutils-2.0b3#T{
emulators
T}#T{
Utilities for Apple Macintosh files.
T}
magic-6.5#T{
cad
T}#T{
An interactive editor for VLSI layouts.
T}
mailagent-3.0.58#T{
mail
T}#T{
A sophisticated automatic mail-processing tool.
T}
majorcool-1.1.2#T{
mail
T}#T{
A Web Interface To Majordomo
T}
majordomo-1.94.4#T{
mail
T}#T{
The Majordomo mailing list manager
T}
makedepend-95.07.05#T{
devel
T}#T{
A dependency generator for makefiles.
T}
makeindex-3.0.8#T{
print
T}#T{
A general purpose, formatter-independent index processor.
T}
manck-1.2#T{
sysutils
T}#T{
Manual page consistency checker
T}
mapedit-2.24#T{
www
T}#T{
A WWW authoring tool to create clickable maps
T}
maplay-1.2#T{
audio
T}#T{
An MPEG audio player/decoder that decodes layer I and II MPEG audio streams.
T}
mars-2.1#T{
cad
T}#T{
Maryland Routing Simulator
T}
mastergear-1.0#T{
emulators
T}#T{
SEGA Master System and Game Gear emulator for X
T}
maxima-5.0#T{
math
T}#T{
A large computer algebra system for symbolic and numerical computations.
T}
mbone_vcr-1.4a2#T{
mbone tk42
T}#T{
A tool to record and play back multicast sessions
T}
mc-4.1.11#T{
misc
T}#T{
Midnight Commander, a free Norton Commander Clone
T}
mei-1.53#T{
sysutils
T}#T{
formats magneto-optical disk for MS-DOS FAT filesystem.
T}
mew-1.92.4#T{
mail
T}#T{
Message interface to Emacs Window(for emacs)
T}
mew-common-1.92.4#T{
mail
T}#T{
Message interface to Emacs Window(common parts)
T}
mew-mule-1.92.4#T{
mail
T}#T{
Message interface to Emacs Window(for mule)
T}
mgdiff-1.0#T{
textproc
T}#T{
A graphical front end to the Unix diff command
T}
mgetty-1.1.9#T{
comms
T}#T{
Handle external logins, send and receive faxes.
T}
mh-6.8.4#T{
mail
T}#T{
Rand MH mail handling system
T}
mhonarc-2.1#T{
www mail
T}#T{
WWW front end for mail archives.  
T}
mikmod-2.14#T{
audio
T}#T{
Mod player which plays MTM, STM, XM, MOD, S3M, ULT and UNI mods.
T}
mimepp-1.0#T{
converters
T}#T{
C++ class library for MIME messages
T}
minicom-1.75#T{
comms
T}#T{
An MS-DOS Telix serial communication program "workalike".
T}
mirror-2.8#T{
net
T}#T{
Mirror packages on remote sites.
T}
mit-scheme-7.3#T{
lang
T}#T{
MIT Scheme: includes runtime, compiler, and edwin binaries.
T}
mixal-1.06#T{
lang
T}#T{
assembler and interpreter for Donald Knuth's mythical MIX computer
T}
mkisofs-1.11.1#T{
sysutils
T}#T{
create ISO9660 filesystems with [optional] Rockridge extensions
T}
mkmf-4.11#T{
devel
T}#T{
Creates program and library makefiles for the make(1) command.
T}
mlvwm-0.8.5#T{
x11
T}#T{
Macintosh like window manager for X11
T}
mm-2.7#T{
mail
T}#T{
Implementation of MIME, the Multipurpose Internet Mail Extensions.
T}
mmm-0.40#T{
www
T}#T{
WWW browser using Objective Caml, Tcl/Tk.
T}
mmr-1.5.1#T{
mail
T}#T{
A curses based MIME aware mailer
T}
mmv-1.01b#T{
misc
T}#T{
move/copy/append/link multiple files with sophisticated wildcard matching
T}
mocka-95.02#T{
lang
T}#T{
Modula 2 Compiler from University of Karlsruhe
T}
modula-3-3.6#T{
lang
T}#T{
Modula-3 compiler and libraries from DEC Systems Research Center.
T}
modula-3-lib-3.6#T{
lang
T}#T{
The shared libraries needed for executing Modula-3 programs.
T}
modula-3-socks-1.0#T{
lang
T}#T{
SOCKS support for Modula-3 programs.
T}
momspider-1.00#T{
www
T}#T{
WWW Spider for multi-owner maintenance.
T}
mosaic-2.7b5#T{
www
T}#T{
A World Wide Web browser.
T}
moscow_ml-1.4#T{
lang
T}#T{
Moscow ML, a version of Standard ML
T}
most-4.7#T{
misc
T}#T{
A pager (like less) which has support for windows and binary files
T}
mouseclock-1.0#T{
x11
T}#T{
display the current time using the X root cursor
T}
movemail-1.0#T{
mail
T}#T{
Move your mail box to annother location.
T}
moxftp-2.2#T{
net
T}#T{
ftp shell under X Window System.
T}
mp-letter-3.0.1#T{
print
T}#T{
A PostScript printing util for ASCII files, email, USENET news articles, etc.
T}
mpack-1.5#T{
converters mail news
T}#T{
External MIME packer/unpacker.
T}
mpd-1.0b4#T{
net
T}#T{
Multilink PPP daemon
T}
mpeg2codec-1.2#T{
graphics
T}#T{
An MPEG-2 Encoder and Decoder.
T}
mpeg2play-1.1b#T{
graphics
T}#T{
A program to play mpeg-2 movies on X displays.
T}
mpeg_encode-1.5b#T{
graphics
T}#T{
UCB's MPEG-I video stream encoder
T}
mpeg_lib-1.2.1#T{
graphics
T}#T{
A collection of C routines to decode MPEG movies.
T}
mpeg_play-2.3#T{
graphics
T}#T{
A program to play mpeg movies on X displays
T}
mpeg_stat-2.2b#T{
graphics
T}#T{
an MPEG-I statistics gatherer.
T}
mpegaudio-3.9#T{
audio
T}#T{
An MPEG/audio Layer 1 and Layer 2 encoder/decoder package.
T}
mpegedit-2.2#T{
graphics
T}#T{
a program to edit encoded mpeg streams.
T}
mpg123-0.59k#T{
audio
T}#T{
Command-line player for mpeg layer 1, 2 and 3 audio
T}
mplex-1.1#T{
graphics
T}#T{
multiplexes MPEG component streams into system layers
T}
mprof-3.0#T{
devel
T}#T{
Memory Profiler and Leak Detector.
T}
mrtg-2.4.1#T{
net
T}#T{
the multi-router traffic grapher
T}
mshell-1.0#T{
misc
T}#T{
A Unix menuing shell.
T}
msql-2.0.1#T{
databases
T}#T{
the Mini SQL server, version 2
T}
mtools-3.8#T{
emulators
T}#T{
A collection of tools for manipulating MSDOS files.
T}
mule-2.3#T{
editors
T}#T{
A multilingual emacs (Only the executables)
T}
mule-common-2.3#T{
editors chinese japanese korean
T}#T{
For a multilingual emacs (mule-2.3), emacs lisp files, info pages, etc (except executables)
T}
musixtex-T80#T{
print
T}#T{
A set of TeX macros to typeset music.
T}
mutt-0.88#T{
mail
T}#T{
"The Mongrel of Mail User Agents" (part Elm, Pine, mh)
T}
mutt-pgp-0.88#T{
mail
T}#T{
"The Mongrel of Mail User Agents" (part Elm, Pine, mh)
T}
mxv-1.10#T{
audio
T}#T{
Sound file editor/player/recorder/converter for X Window System.
T}
mysql-3.20.32a#T{
databases
T}#T{
a multithreaded SQL database.
T}
nana-1.09#T{
devel
T}#T{
Improved support for assertion checking and logging using GNU C and GDB.
T}
nas-1.2.5#T{
audio
T}#T{
Network Audio System.
T}
nasm-0.95#T{
devel lang
T}#T{
General-purpose multi-platform x86 assembler
T}
ncftp-1.9.5#T{
net
T}#T{
FTP replacement with advanced user interface.
T}
ncftp-2.4.2#T{
net
T}#T{
FTP replacement with advanced user interface.
T}
neXtaw-0.5.1#T{
x11
T}#T{
Athena Widgets with N*XTSTEP appearance
T}
nedit-5.0#T{
editors x11
T}#T{
An X11/Motif GUI text editor for programs and plain text files.
T}
netatalk-1.4b2#T{
net print
T}#T{
File and print server for AppleTalk networks
T}
netcat-1.10#T{
net
T}#T{
simple utility which reads and writes data across network connections
T}
nethack-3.2.2#T{
games
T}#T{
A dungeon explorin', slashin', hackin' game.
T}
nethack-qt-3.2.2#T{
games x11
T}#T{
A dungeon explorin', slashin', hackin' game with graphic and sound support
T}
netpbm-94.3.1#T{
graphics
T}#T{
A toolkit for conversion of images between different formats
T}
netperf-2.1.3#T{
benchmarks
T}#T{
Rick Jones' <raj@cup.hp.com> network performance benchmarking package.
T}
netpipes-3.2#T{
net
T}#T{
A group of shell utilities to connect programs to sockets
T}
netris-0.4#T{
games
T}#T{
A network head to head version of T*tris.
T}
netscape-3.04#T{
www
T}#T{
netscape ver 3 web-surfboard (regular or gold)
T}
netscape-communicator-4.04#T{
www
T}#T{
netscape ver 4 communicator web-surfboard
T}
netscape-navigator-4.04#T{
www
T}#T{
netscape ver 4 navigator web-surfboard
T}
nettest-92.11.09#T{
net
T}#T{
Performs client and server functions for timing data throughput.   
T}
newsfetch-1.0#T{
news
T}#T{
download news articles from NNTP server.
T}
nmh-0.17#T{
mail
T}#T{
A cleaned up MH mailer suite.
T}
nn-6.5.0#T{
news
T}#T{
NN newsreader.
T}
nntp-1.5.11.5#T{
news
T}#T{
NNTP with NOV support.
T}
nntpbtr-1.7#T{
news
T}#T{
NNTP bulk transfer
T}
nntpcache-1.0.7.1#T{
news
T}#T{
News caching system.
T}
noweb-2.8a#T{
devel
T}#T{
A simple, extensible literate-programming tool.
T}
nspmod-0.1#T{
audio
T}#T{
MOD/S3M/MTM tracker that does it's own DSP, uses VoxWare v2.90+
T}
nte-1.5a23#T{
mbone tk42
T}#T{
Multicast Network Text Editor
T}
nulib-3.25#T{
archivers
T}#T{
NuFX archive utility
T}
numpy-1.0b3#T{
math
T}#T{
The Numeric Extension to Python
T}
nvi-m17n-1.79-970820#T{
japanese editors
T}#T{
A clone of vi/ex, with multilingual patch, no default settings.
T}
nwrite-1.9#T{
misc
T}#T{
improved, recipient configureable replacement for /usr/bin/write
T}
objc-1.6.8#T{
lang
T}#T{
Portable Object Compiler
T}
ocaml-1.03#T{
lang
T}#T{
A ML language based on complete class-based objective system
T}
octave-2.0.9#T{
math
T}#T{
High-level interactive language for numerical computations.
T}
oleo-1.6#T{
math
T}#T{
A Spreadsheet Program.
T}
olvwm-4.1#T{
x11
T}#T{
OpenLook Virtual Window manager 
T}
omniBroker-1.0#T{
devel
T}#T{
A CORBA 2 implementation
T}
omniORB-2.2.0#T{
devel
T}#T{
A CORBA 2 implementation
T}
oneko-1.2#T{
games
T}#T{
A cat chasing a mouse all over the screen
T}
oonsoo-1.1#T{
games
T}#T{
A solitaire card game for X.
T}
p2c-1.21a#T{
lang
T}#T{
Pascal to C translator.
T}
p5-Apache-1.00b2#T{
www perl5
T}#T{
Embeds a Perl interpreter in the Apache server
T}
p5-Archie-1.5#T{
net perl5
T}#T{
perl5 module to make Archie queries via Prospero requests.
T}
p5-Array-PrintCols-2.0#T{
misc perl5
T}#T{
perl5 module to print arrays of elements in sorted columns.
T}
p5-Authen-Radius-0.05#T{
security net perl5
T}#T{
a perl5 module to provide simple Radius client facilities
T}
p5-B-a3#T{
devel lang perl5
T}#T{
a perl compiler kit, alpha release. Write a c source code from a perl script.
T}
p5-BSD-Resource-1.06#T{
devel perl5
T}#T{
perl5 module to access BSD process resource limit and priority functions.
T}
p5-Business-CreditCard-0.1#T{
misc perl5
T}#T{
perl5 module to validate/generate credit card checksums/names.
T}
p5-C-Scan-0.4#T{
devel perl5
T}#T{
perl5 module to scan C language files for easily recognized constructs.
T}
p5-CGI-2.76#T{
www perl5
T}#T{
modules for perl5, for use in writing CGI scripts.
T}
p5-CGI_Lite-1.8#T{
www perl5
T}#T{
perl5 module to process and decode WWW form information.
T}
p5-Compress-Zlib-0.50#T{
archivers perl5
T}#T{
perl5 interface to zlib compression library.
T}
p5-ConfigReader-0.5#T{
devel perl5
T}#T{
perl5 module to read directives from a configuration file.
T}
p5-Convert-UU-0.04#T{
converters perl5
T}#T{
perl5 module for uuencode and uudecode.
T}
p5-Crypt-DES-1.0#T{
security perl5
T}#T{
perl5 interface to DES block cipher.
T}
p5-Crypt-IDEA-1.0#T{
security perl5
T}#T{
perl5 interface to IDEA block cipher.
T}
p5-Curses-1.01#T{
devel perl5
T}#T{
perl5 module for terminal screen handling and optimization.
T}
p5-DBD-Pg-0.62#T{
databases perl5
T}#T{
provides access to PostgreSQL databases through the DBI
T}
p5-DBI-0.90#T{
databases perl5
T}#T{
the perl5 Database Interface.  Required for DBD::* modules.
T}
p5-Data-Dumper-2.07#T{
devel perl5
T}#T{
perl5 module for stringified perl data structures.
T}
p5-Data-Flow-0.05#T{
devel perl5
T}#T{
Perl extension for simple-minded recipe-controlled build of data.
T}
p5-Data-ShowTable-3.3#T{
misc perl5
T}#T{
perl5 module to pretty-print arrays of data
T}
p5-Date-Manip-5.10#T{
devel perl5
T}#T{
perl5 module containing date manipulation routines
T}
p5-Devel-DProf-19970614#T{
devel perl5
T}#T{
a Perl code profiler
T}
p5-Devel-Peek-0.83#T{
devel perl5
T}#T{
a perl5 data debugging tool for the XS programmer
T}
p5-Devel-Symdump-2.00#T{
devel perl5
T}#T{
a perl5 module that dumps symbol names or the symbol table
T}
p5-Errno-1.01#T{
devel perl5
T}#T{
a perl5 module providing access to System errno constants
T}
p5-File-BasicFlock-96.072401#T{
devel perl5
T}#T{
perl5 module for file locking with flock.
T}
p5-File-Lock-0.9#T{
devel perl5
T}#T{
perl5 module for file locking (flock,fcntl).
T}
p5-File-Slurp-96.042202#T{
devel perl5
T}#T{
perl5 module for single call read & write file routines.
T}
p5-File-Tools-2.0#T{
devel perl5
T}#T{
perl5 module for several file operations: Copy, Recurse and Tools.
T}
p5-Filter-1.12#T{
devel perl5
T}#T{
a number of source filters for perl5 programs
T}
p5-FreezeThaw-0.3#T{
devel perl5
T}#T{
a module for converting Perl structures to strings and back.
T}
p5-GD-1.14#T{
graphics perl5
T}#T{
a perl5 interface to Gd Graphics Library
T}
p5-HTML-0.6#T{
www perl5
T}#T{
perl5 module for writing HTML documents.
T}
p5-HTML-QuickCheck-1.0b1#T{
www perl5
T}#T{
a simple and fast HTML syntax checking package for perl 4 and perl 5.
T}
p5-HTML-Stream-1.40#T{
www perl5
T}#T{
perl5 HTML output stream class, and some markup utilities.
T}
p5-HTTPD-Tools-0.55#T{
www perl5
T}#T{
perl5 module for a HTTP server authentication class.
T}
p5-Image-Size-2.4#T{
graphics www perl5
T}#T{
perl5 module to determine the size of images in several common formats.
T}
p5-Include-1.02a#T{
devel perl5
T}#T{
perl5 package which allows the use of macros defined in 'C' header files.
T}
p5-IniConf-0.92#T{
devel perl5
T}#T{
perl5 module for reading .ini-style configuration files.
T}
p5-Ioctl-0.7#T{
devel perl5
T}#T{
perl5 module for getting the value of the C Ioctl constants.
T}
p5-Locale-Codes-0.003#T{
misc perl5
T}#T{
perl5 module providing access to ISO3166 and ISO639 Country Codes
T}
p5-MD5-1.7#T{
security perl5
T}#T{
perl5 interface to the RSA Data Security Inc. MD5 Message-Digest Algorithm
T}
p5-MIME-Base64-2.03#T{
converters perl5
T}#T{
perl5 module for Base64 and Quoted-Printable encodings
T}
p5-MLDBM-1.24#T{
databases perl5
T}#T{
store multi-level hash structure in single level tied hash
T}
p5-Mail-Folder-0.06#T{
mail perl5
T}#T{
perl module for a folder-independant interface to email folders.
T}
p5-Mail-POP3Client-1.15#T{
mail perl5
T}#T{
perl5 module to talk to a POP3 (RFC1081) server
T}
p5-Mail-Tools-1.09#T{
mail perl5
T}#T{
a set of perl5 modules related to mail applications.
T}
p5-MatrixReal-1.2#T{
math perl5
T}#T{
A perl module implementing a Matrix of Reals.
T}
p5-Msql-modules-1.1811#T{
databases perl5
T}#T{
perl5 modules for accessing MiniSQL (mSQL) databases.
T}
p5-Mysql-modules-1.1810#T{
databases perl5
T}#T{
perl5 modules for accessing MiniSQL (mSQL) databases.
T}
p5-Net-1.0602#T{
net perl5
T}#T{
perl5 modules to access and use network protocols.
T}
p5-Net-DNS-0.12#T{
net perl5
T}#T{
perl5 interface to the DNS resolver
T}
p5-Net-Whois-0.22#T{
net perl5
T}#T{
a perl5 module to get information using the Whois protocol.
T}
p5-PGP-0.3a#T{
security perl5
T}#T{
perl5 module to work with PGP messages.
T}
p5-PV-1.0#T{
net perl5
T}#T{
a perl5 library for text-mode user interface widgets
T}
p5-Penguin-3.00#T{
devel security perl5
T}#T{
a framework for passing digitally signed perl5 code between machines.
T}
p5-Penguin-Easy-1.1#T{
devel security perl5
T}#T{
a quick and easy implemention of the p5-Penguin module
T}
p5-Pg-1.6.1#T{
databases perl5
T}#T{
an interface between perl5 and the database Postgres95.
T}
p5-Proc-Simple-1.12#T{
devel perl5
T}#T{
perl5 module to launch and control background processes
T}
p5-Religion-1.04#T{
devel perl5
T}#T{
perl5 module to install die() and warn() handlers.
T}
p5-Resources-1.04#T{
devel perl5
T}#T{
perl5 module handling application defaults in Perl.
T}
p5-SNMP-1.6#T{
net perl5
T}#T{
a perl5 module for interfacing with the CMU SNMP library
T}
p5-Search-0.2#T{
misc perl5
T}#T{
perl5 module to provide framework for multiple searches.
T}
p5-Sort-Versions-1.1#T{
devel perl5
T}#T{
a perl 5 module for sorting of revision-like numbers
T}
p5-Storable-0.5#T{
devel perl5
T}#T{
persistency for perl data structures
T}
p5-Tcl-Tk-b1#T{
x11 lang perl5
T}#T{
perl5 module to access to Tk via the Tcl extension
T}
p5-Tcl-b1#T{
lang perl5
T}#T{
a Tcl extension module for Perl5
T}
p5-Term-ReadKey-2.07#T{
devel perl5
T}#T{
a perl5 module for simple terminal control
T}
p5-Time-960117#T{
devel perl5
T}#T{
a collection of functions to convert and use time variables in perl5
T}
p5-TimeDate-1.07#T{
devel perl5
T}#T{
perl5 module containing a better/faster date parser for absolute dates
T}
p5-Tk-402.002#T{
x11 tk41 perl5
T}#T{
a re-port of a perl5 interface to Tk4.0p3.
T}
p5-TraceFuncs-0.1#T{
devel perl5
T}#T{
a perl5 module to trace function calls as they happen.
T}
p5-WWW-Search-1.010#T{
www perl5
T}#T{
a perl5 module for WWW searches
T}
p5-ePerl-2.2.8#T{
lang www perl5
T}#T{
Perl Modules of ePerl package: Parse::ePerl, Apache::ePerl
T}
p5-libwww-5.16#T{
www perl5 devel
T}#T{
perl5 library for WWW access.
T}
par-1.50#T{
textproc
T}#T{
Pargraph reformatter for email
T}
pari-1.39.03#T{
math
T}#T{
Mathmatics library and advanced calculator package
T}
pbasic-2.0#T{
lang
T}#T{
Phil Cockroft's Basic Interpreter (previously Rabbit Basic)
T}
pcb-1.4.0#T{
cad
T}#T{
X11 interactive printed circuit board layout system
T}
pccts-1.33#T{
devel
T}#T{
The Purdue Compiler Construction Tool Set
T}
pcemu-1.01a#T{
emulators
T}#T{
An 8086 PC emulator, by David Hedley
T}
pcnfsd-93.02.16#T{
net
T}#T{
Sun PC NFS authentication and printing server.
T}
pdksh-5.2.13#T{
shells
T}#T{
The Public Domain Korn Shell.
T}
pdore-6.0#T{
graphics
T}#T{
The Dynamic Object Rendering Environment.
T}
perl-5.00404#T{
lang devel perl5
T}#T{
Pattern Extraction and Recognition Language.
T}
pfe-0.9.9#T{
lang
T}#T{
Implementation of ANSI Forth.
T}
pgaccess-0.62#T{
databases
T}#T{
a Tcl/Tk interface to PostgreSQL
T}
pgcc-2.7.2.9#T{
lang
T}#T{
gcc optimized for INTEL Pentium CPU
T}
pgcc-2.7.2c#T{
lang
T}#T{
gcc optimized for INTEL Pentium CPU, Developer Release
T}
pgp-2.6.2#T{
security
T}#T{
PGP MIT or International version - Public-Key encryption for the masses
T}
pgperl-2.06#T{
graphics perl5
T}#T{
A perl5 extension which makes available the pgplot library.
T}
pgplot-5.2#T{
graphics
T}#T{
A C/FORTRAN library for drawing graphs on a variety of display devices.
T}
pgpsendmail-1.4#T{
mail
T}#T{
PGP sign/encrypt/decrypt messages automatically.
T}
photopc-1.6#T{
graphics
T}#T{
A utility to manage an Epson PhotoPC 500 digital camera.
T}
pib-1.1#T{
sysutils
T}#T{
GUI Ports Collection management tool.
T}
pidentd-2.8a4#T{
security net
T}#T{
An RFC1413 identification server.
T}
piewm-1.0#T{
x11
T}#T{
A tvtwm with pie (circular) menus.
T}
pilot-link-0.8.7#T{
comms tk80
T}#T{
PalmPilot communications utilities (backup/restore/install/debug/...)
T}
pilot_makedoc-0.7#T{
textproc
T}#T{
converts text into the Doc format used by PalmPilots.
T}
pine-3.96#T{
mail news
T}#T{
Program for Internet E-mail and News
T}
pixmap-2.6#T{
graphics
T}#T{
A pixmap editor based on XPM library.
T}
pkfonts300-1.0#T{
print
T}#T{
English PK fonts, for ghostscripts, xdvi and so on
T}
plan-1.6.1#T{
misc
T}#T{
An X/Motif schedule planner with calendar
T}
playmidi-2.3#T{
audio
T}#T{
MIDI player.
T}
plor-0.3.1#T{
news
T}#T{
An alpha-release reader for reading SOUP and QWK packets.
T}
plotmtv-1.4.1#T{
graphics
T}#T{
A multipurpose X11 plotting program
T}
plplot-4.99j#T{
math
T}#T{
A scientific plotting package.
T}
pmf-1.13.1#T{
net
T}#T{
Padrone's Mud Frontend.
T}
png-0.96#T{
graphics
T}#T{
Library for manipulating PNG images.
T}
popclient-3.0b6#T{
mail
T}#T{
client for pop2, pop3, apop, rpop
T}
poppassd-4.0#T{
mail
T}#T{
A server to allow users to change their password from within Eudora
T}
portlint-1.61#T{
devel
T}#T{
a verifier for FreeBSD port directory.
T}
postgresql-6.2.1#T{
databases
T}#T{
a robust, next generation, object-relational DBMS
T}
povray-3.0#T{
graphics
T}#T{
Persistence of Vision Ray Tracer Version 3.0
T}
prc-tools-0.5.0#T{
devel
T}#T{
Development environment for PalmPilot(tm) handheld computer.
T}
prcs-1.2.1#T{
devel
T}#T{
The Project Revision Control System
T}
premail-0.45#T{
security mail
T}#T{
E-mail privacy package, support anon remailers, PGP, nyms
T}
procmail-3.11p7#T{
mail
T}#T{
A local mail delivery agent.
T}
prodosemu-0.1#T{
emulators
T}#T{
A text based Apple IIe ProDOS emulator.
T}
psutils-a4-1.17#T{
print
T}#T{
Utilities for manipulating PostScript documents.
T}
python-1.4#T{
lang tk80
T}#T{
An interpreted object-oriented programming language.
T}
pythonqt-0.1.2#T{
x11
T}#T{
A set of Python bindings for Qt.
T}
qcc-1.01#T{
games
T}#T{
The QuakeC compiler, for building custom games of Quake.
T}
qpage-3.2#T{
comms
T}#T{
SNPP client/server for sending messages to an alphanumeric pager.
T}
qpopper-2.41b1#T{
mail
T}#T{
Berkeley POP 3 server (now maintained by Qualcomm).
T}
qt-1.31#T{
x11
T}#T{
A C++ X GUI toolkit.
T}
quakeserver-1.0#T{
games
T}#T{
a server to host Quake network games under FreeBSD.
T}
qvplay#T{
graphics
T}#T{
digital camera downloading tool for Casio QV series
T}
qvwm-1.0b8g#T{
x11
T}#T{
The most win95-like window manager. Standalone, not fvwm patched.
T}
radio-2.0.4#T{
audio net
T}#T{
Radio over the Internet.
T}
radius-2.4.23#T{
net
T}#T{
A remote authentication server.
T}
ranlib-1.0#T{
math
T}#T{
Library of Routines for Random Number Generation
T}
rar-2.01#T{
archivers
T}#T{
File archiver (binary port)
T}
ratoolset-3.5.2#T{
net
T}#T{
a suite of policy analysis tools
T}
rc-1.5b1#T{
plan9
T}#T{
A unix incarnation of the plan9 shell.
T}
rclock-2.20#T{
x11
T}#T{
analog clock for X w/appointment reminder and mail notification
T}
rdist-6.1.3#T{
net
T}#T{
A network file distribution/synchronisation utility.
T}
recode-3.4#T{
converters
T}#T{
Converts files between character sets and usages.
T}
rexx-imc-1.6d#T{
lang
T}#T{
a procedural programming language designed by IBM's UK Laboratories.
T}
ripetools-2.2#T{
net
T}#T{
RIPE's own version of whois, and a RIPE database tool.
T}
rkive3.1#T{
news
T}#T{
A USENET newsgroup archiver.
T}
rman-3.0.4#T{
textproc
T}#T{
Reverse compile man pages from formatted form.
T}
rmsg-1.64#T{
net
T}#T{
A network messaging system.
T}
rosegarden-2.0.1#T{
audio
T}#T{
The Rosegarden Editor and Sequencer suite.
T}
rplay-3.2.0b6#T{
audio
T}#T{
Network audio player.
T}
rpm-2.4.10#T{
misc
T}#T{
The Red Hat Package Manager
T}
rsaref-2.0#T{
security
T}#T{
encryption/authentication library, RSA/MDX/DES
T}
rshell-1.0#T{
net
T}#T{
A front end for rsh(1) and rcp(1).
T}
rsync-1.6.3#T{
net
T}#T{
A network file distribution/synchronisation utility.
T}
rsynth-2.0#T{
audio
T}#T{
Speech synthesizer.
T}
rtf2latex-1.5#T{
print
T}#T{
A filter that converts RTF (MS's Rich Text Format) into LaTeX.
T}
rtpmon-1.0a7#T{
mbone
T}#T{
A program to montior loss in multicast sessions
T}
rtty-3.2#T{
sysutils
T}#T{
multiuser "tip"/"cu" replacement with logging
T}
ru-X11-3.2#T{
russian x11
T}#T{
Setup X locale/nls/keyboard/fonts for russian language (koi8-r)
T}
ru-d1489-1.3#T{
russian
T}#T{
cp866<->koi8-r and cp1251<->koi8-r decoders and font converter acc. to RFC1489
T}
ru-elm-2.4ME#T{
russian mail
T}#T{
Tune Elm with MIME for KOI8-R defaults
T}
ru-netscape-3.01#T{
russian www
T}#T{
Tune netscape3.01 to work with russian (koi8-r) fonts
T}
ru-pgp-2.6.3ia#T{
russian
T}#T{
Russian language module for PGP
T}
ru-pine-3.95#T{
russian mail
T}#T{
Tune Pine for Russian (KOI8-R) defaults
T}
ruby-1.0#T{
lang
T}#T{
An object-oriented interpreted scripting language
T}
rumba-0.4#T{
net
T}#T{
An userland smbfs --- SMB to NFS protocols converter
T}
rwhois-1.0b9.2#T{
net
T}#T{
the Internic referral whois server
T}
rxvt-2.20#T{
x11
T}#T{
A low memory usage xterm replacement that supports color
T}
rzsz-3.47#T{
comms
T}#T{
Receive/Send files via X/Y/ZMODEM protocol.
T}
s3mod-1.09#T{
audio
T}#T{
MOD/S3M player.
T}
sam-4.3#T{
plan9
T}#T{
A unix version of Rob Pike's editor for plan9
T}
samba-1.9.17.4#T{
net
T}#T{
A LanManager(R)-compatible server suite for Unix
T}
samba-des-1.9.17.4#T{
net
T}#T{
A LanManager(R)-compat. server suite w/ WinNT encrypted password
T}
sather-1.0.5#T{
lang
T}#T{
Sather compiler
T}
sattrack-3.1.6#T{
astro
T}#T{
Real-time satellite tracking and orbit propagation program.
T}
sawt-0.4.2#T{
devel
T}#T{
A cleanroom AWT implementation for Kaffe
T}
sced-0.94#T{
graphics
T}#T{
SCene EDitor for povray/renderman/genray/etc
T}
scheme48-0.46#T{
lang
T}#T{
The Scheme Underground's implementation of R4RS.
T}
schemetoc-93.3.15#T{
lang
T}#T{
Scheme-to-C, a compiler and interpreter for compiling scheme into C.
T}
scm-4e1#T{
lang
T}#T{
A scheme interpreter.
T}
scogdb-4.16#T{
devel
T}#T{
An SCO debugger for cross-development purposes.
T}
scotty-2.1.6#T{
net tk80
T}#T{
network management extensions to tcl
T}
screen-3.7.4#T{
misc
T}#T{
A multi-screen window manager.
T}
scsh-0.5.1#T{
shells lang
T}#T{
A Unix shell embedded into Scheme, with access to all Posix calls
T}
sdr-2.3a1#T{
mbone tk80
T}#T{
MBone Session Directory
T}
setquota-0.1#T{
sysutils
T}#T{
Command line quota tools.
T}
seyon-2.14b#T{
comms x11
T}#T{
A communications package for the X Window system.
T}
sfio-97#T{
devel
T}#T{
The Safe/Fast I/O Library
T}
sgmlformat-1.4#T{
textproc print
T}#T{
Generates groff and HTML from linuxdoc and docbook SGML documents
T}
sharutils-4.2#T{
archivers
T}#T{
Allow packing and unpacking of shell archives.
T}
sim-2.3#T{
emulators
T}#T{
PDP-8, PDP-11, PDP-1, other 18b PDP, Nova, and IBM 1401 simulators.
T}
sim6811-1.6#T{
emulators
T}#T{
A Motorola 6811 simulator.
T}
simpack-3.0#T{
math
T}#T{
SimPack & Sim++ libraries and tools for simulatiom modelling
T}
skill-3.7.5#T{
sysutils
T}#T{
SuperKILL, kill or renice processes by pid, name, tty or uid
T}
sliderule-1.0#T{
x11
T}#T{
The part of X11R3's xcalc featuring a slide rule
T}
slingshot-2.1#T{
x11
T}#T{
Supplemental Libraries to extend Xview
T}
slirp-1.0c#T{
net
T}#T{
A SLIP/CSLIP emulator for users with shell accounts.
T}
slnr-2.3.0#T{
news
T}#T{
A simplistic reader for SOUP (HDPF) mail and news packets.
T}
slrn-0.9.4.3#T{
news
T}#T{
SLang-based newsreader
T}
slurp-1.10#T{
news
T}#T{
A passive NNTP client that retrieves Usenet news articles from a remote server.
T}
smail-3.2.0.97#T{
mail
T}#T{
A program used for receiving and delivering mail.
T}
smalltalk-1.1.5#T{
lang
T}#T{
GNU Smalltalk.
T}
snes9x-0.23#T{
emulators
T}#T{
Super Nintendo Entertainment System(SNES) Emulator
T}
snooper-970212#T{
comms
T}#T{
serial line protocol analyzer (need two serial interfaces)
T}
socket-1.1#T{
sysutils net
T}#T{
create tcp socket and connect to stdin/out
T}
socks5-1.0.2#T{
net security
T}#T{
SOCKS v5 application layer gateway and clients
T}
sokoban-1.0#T{
games
T}#T{
Logical game: problems with packets in cave
T}
sox-12.12#T{
audio
T}#T{
SOund eXchange - universal sound sample translator.
T}
sp-1.2.1#T{
textproc
T}#T{
A free, object-oriented toolkit for SGML parsing and entity management 
T}
speak_freely-6.1c#T{
mbone audio security
T}#T{
Voice Communication Over Data Networks.
T}
spice-3f5#T{
cad
T}#T{
Simulation Program for Integrated Circuit Electronics
T}
spider-1.1#T{
games
T}#T{
A challenging double decked solitaire game
T}
spigot-1.6#T{
graphics
T}#T{
Video spigot for Windows library.
T}
spim-5.9#T{
emulators
T}#T{
MIPS R2000 Simulator
T}
splay-0.5#T{
audio
T}#T{
An audio player/decoder that decodes MPEG Layer I,II,III and WAV files.
T}
splitmpg-1.0#T{
graphics
T}#T{
Splits an ISO 11172-1 into its componets
T}
splitvt-1.6.3#T{
misc
T}#T{
run two shells in a split window/terminal
T}
squeak-1.18#T{
lang
T}#T{
A Smalltalk system with graphical user interface.
T}
squid-1.1.18#T{
www
T}#T{
Post-Harvest_cached WWW proxy cache and accelerator
T}
squid-1.2b8#T{
www
T}#T{
The successful WWW proxy cache and accelerator
T}
squid-novm-1.1.18#T{
www
T}#T{
Low-VM Post-Harvest_cached WWW proxy cache and accelerator
T}
sr-2.3.1#T{
lang
T}#T{
A parallel language "Synchronization Resources"
T}
ss-1.3.3#T{
math
T}#T{
A curses-based SpreadSheet program
T}
ssh-1.2.21#T{
security net
T}#T{
Secure shell client and server (remote login program).
T}
star-1.1#T{
sysutils
T}#T{
unique standard tape archiver with many enhancements
T}
starlanes-1.2.2#T{
games
T}#T{
The classic space-age stock trading game
T}
staroffice-3.1#T{
editors
T}#T{
Integrated wordprocessor, spreadheet, drawing, charting and web browser suite.
T}
stars-1.0#T{
astro x11
T}#T{
Star field demo.
T}
stat-1.3#T{
sysutils
T}#T{
Print inode contents.
T}
stella-0.6#T{
emulators
T}#T{
Atari 2600 VCS emulator
T}
stonx-0.6.7e#T{
emulators
T}#T{
AtariST emulator
T}
strobe-1.03#T{
net security
T}#T{
Fast scatter/gather TCP port scanner
T}
su2-1.3#T{
sysutils
T}#T{
an enhanced su, allows users to su with own password + more
T}
suck-3.8.0#T{
news
T}#T{
Receives/sends news to/from localhost via NNTP.
T}
sudo-1.5.3#T{
security
T}#T{
Allow others to run commands as root.
T}
sunclock-1.3#T{
astro
T}#T{
Shows which portion of the Earth's surface is illuminated by the Sun.
T}
sup-2.0#T{
net
T}#T{
CMU's Software Update Protocol package.
T}
super-3.9.7#T{
security sysutils
T}#T{
Allow others to run commands as root.
T}
swatch-2.2#T{
security sysutils
T}#T{
The Simple WATCHer and filter
T}
swi-pl-2.8.6#T{
lang
T}#T{
Edinburgh-style Prolog compiler.
T}
swig-1.1p3#T{
devel
T}#T{
Simplified Wrapper and Interface Generator.
T}
swisswatch-0.06#T{
x11
T}#T{
A Swiss railway clock emulation, and a fancy default appearance.
T}
sxpc-1.4#T{
x11
T}#T{
The Simple X Protocol Compressor
T}
tac_plus-2.1#T{
net
T}#T{
A remote authentication/authorization/accounting server.
T}
tcl-7.5.1#T{
lang tcl75
T}#T{
Tool Command Language.
T}
tcl-7.6#T{
lang tcl76
T}#T{
Tool Command Language.
T}
tcl-8.0.2#T{
lang tcl80
T}#T{
Tool Command Language.
T}
tclX-7.5.2#T{
lang tk41
T}#T{
Extended TCL
T}
tclcheck-1.1.11#T{
devel
T}#T{
A program to check the nesting of parenthesis in tcl scripts.
T}
tclplugin-2.0.4b#T{
lang
T}#T{
Tcl Plug-in for Netscape Navigator
T}
tcp_wrappers-7.6#T{
security net
T}#T{
This is the TCP/IP daemon wrapper package.
T}
tcpblast-1.0#T{
net benchmarks
T}#T{
Measures the throughput of a tcp connection.
T}
tcplist-2.2#T{
sysutils net
T}#T{
lists tcp connections to/from the local machine (+username on both sides)
T}
tcpshow-1.0#T{
net security
T}#T{
Decode tcpdump(1) output.
T}
tcptrace-4.0.2#T{
net
T}#T{
a TCP dump file analysis tool
T}
tcsh-6.07.02#T{
shells
T}#T{
An extended C-shell with many useful features.
T}
teTeX-0.4#T{
print
T}#T{
TeX distribution for UNIX compatible systems
T}
team-3.1#T{
misc
T}#T{
Portable multi-buffered tape streaming utility.
T}
telnetx-940401#T{
net
T}#T{
Telnet client with binary transfer protocols support
T}
tex-3.14159#T{
print
T}#T{
TeX and METAFONT.
T}
texi2html-1.51#T{
textproc
T}#T{
Texinfo to HTML converter
T}
texinfo-2.124#T{
print
T}#T{
Typeset Texinfo files for printing. Uses TeX.
T}
tf-4.0a3#T{
net games
T}#T{
A popular programmable MUD client, with macro support and more.
T}
tgif-3.0p13#T{
graphics
T}#T{
An Xlib-based two-dimensional drawing facility.
T}
thoteditor-2.1b#T{
editors www
T}#T{
An structured document editor, offering a graphical WYSIWYG interface
T}
tiff-3.3#T{
graphics
T}#T{
A library for reading and writing TIFF data files.
T}
tiff-3.4#T{
graphics
T}#T{
libtiff provides support for the Tag Image File Format (TIFF)
T}
timidity-0.2i#T{
audio
T}#T{
MIDI to WAV renderer and player
T}
tinpre-1.4-971204#T{
news
T}#T{
TIN newsreader (termcap based)
T}
tintin-1.5.6#T{
net games
T}#T{
A client program to help playing muds.
T}
tix-4.1.0.006#T{
x11 tk80
T}#T{
An extension to the Tk toolkit.
T}
tk-4.1.1#T{
x11 tk41
T}#T{
Graphical toolkit for TCL.
T}
tk-4.2#T{
x11 tk42
T}#T{
Graphical toolkit for TCL.
T}
tk-8.0.2#T{
x11 tk80
T}#T{
Graphical toolkit for TCL.
T}
tkcron-2.12#T{
misc tk80
T}#T{
A frontend to crontab.
T}
tkcvs-6.0#T{
devel tk80
T}#T{
Tcl/Tk frontends to CVS and diff 
T}
tkdesk-1.0b3#T{
misc x11 tk41
T}#T{
A graphical, highly configurable and powerful file manager.
T}
tkgoodstuff-8.0#T{
x11
T}#T{
TkGoodstuff module for fvwm2 window manager - requires XPM and fvwm2
T}
tkhfs-1.27#T{
emulators x11 tk41
T}#T{
A Tcl/Tk front end to the hfs program. 
T}
tkhylafax-3.0#T{
comms
T}#T{
a tcl/tk interface to Sam Leffler's fax package
T}
tkinfo-1.8#T{
misc tk80
T}#T{
A tk script to read GNU "info" files and display them.
T}
tkman-2.0.4#T{
misc tk80
T}#T{
A Tcl/Tk based manual browser
T}
tkrat-1.0.5#T{
mail tk80
T}#T{
An advanced scriptable GUI Mail UA for X-, Win32 and Mac
T}
tkref-1.01#T{
devel x11
T}#T{
A programmer's GUI reference for Tcl/Tk and major packages.
T}
tksol-1.0#T{
games tk80
T}#T{
A version of the card game solitaire.
T}
tkstep-8.0p2#T{
x11 tk80
T}#T{
The Tk toolkit with a NeXTSTEP look and more.
T}
tosha-0.05#T{
audio
T}#T{
Read CD digital audio data through the SCSI bus.
T}
tr2latex-2.2#T{
print
T}#T{
Convert a document from troff to LaTeX.
T}
traceroute-961230#T{
net
T}#T{
A version of 'traceroute' that shows the AS network number of each hop.
T}
tracker-5.3#T{
audio
T}#T{
MOD player.
T}
trafshow-2.0#T{
net
T}#T{
Full screen visualization of the network traffic.
T}
trans-1.20#T{
converters russian
T}#T{
Character Encoding Converter Generator Package.
T}
transfig-3.2#T{
print
T}#T{
Tools to convert Xfig's .fig files.
T}
tripwire-1.2#T{
security net
T}#T{
File system security and verification program.
T}
trn-3.6#T{
news
T}#T{
Threaded Read News newsreader.
T}
ttt-0.4.1#T{
net
T}#T{
Tele Traffic Tapper, a network traffic monitoring tool
T}
tua-4.0#T{
sysutils
T}#T{
The Uucp Analyzer.
T}
tvision-0.5#T{
devel
T}#T{
The Turbo Vision C++ CUI library for UNIX
T}
tvp-0.9.7.1#T{
games tk80
T}#T{
Play the cardgame President (or Ass) against 3 AIs.
T}
tvtwm-pl11#T{
x11
T}#T{
twm with a virtual desktop.
T}
txtmerge-1.01#T{
graphics
T}#T{
txtmerge, a tool for making GIF Animations.
T}
tycoon-1.07o#T{
x11
T}#T{
A nifty set of desktop apps, including floating buttons
T}
typhoon-1.10.3#T{
databases
T}#T{
a relational database library
T}
ucblogo-3.3#T{
lang
T}#T{
Brian Harvey's logo language interpreter.
T}
ucd-snmp-3.2#T{
net
T}#T{
An extendable SNMP implimentation 
T}
uemacs-3.12#T{
editors
T}#T{
A full screen editor.
T}
umatrix-1.1#T{
math
T}#T{
Simple matrix package
T}
unarj-2.41#T{
archivers
T}#T{
Allows files to be extracted from ARJ archives.
T}
unclutter-8#T{
misc
T}#T{
Remove idle cursor image from screen.
T}
unrar-2.01#T{
archivers
T}#T{
Extract, view & test RAR archives.
T}
unroff-1.0.2#T{
textproc
T}#T{
A programmable troff translator with backend for HTML.
T}
unzip+crypt-5.3.2#T{
archivers
T}#T{
List, test and extract compressed files in a ZIP archive, with encryption.
T}
unzip-5.3.2#T{
archivers
T}#T{
List, test and extract compressed files in a ZIP archive.
T}
urt-3.1b#T{
graphics
T}#T{
Utah raster toolkit - a toolkit and library for raster image processing.
T}
uudeview-0.5.13#T{
converters
T}#T{
A program for uu/xx/Base64/BinHex de-/encoding.
T}
uudx-2.99#T{
converters
T}#T{
Extractor from uuencoded files.
T}
uulib-0.5.13#T{
converters
T}#T{
A library for uu/xx/Base64/BinHex de-/encoding.
T}
uzap-1.0#T{
editors
T}#T{
Visual binary file editor
T}
vat-4.0b2#T{
mbone tk80
T}#T{
The Visual Audio Tool - multicast audioconferencing
T}
vcg-1.30#T{
graphics
T}#T{
A Visualization Tool for compiler graphs
T}
vgb-0.7#T{
emulators
T}#T{
Nintendo GameBoy(tm) emulator
T}
vi-vn7to8-1.1.1#T{
vietnamese
T}#T{
converts between 7-bit Vietnamese VIQR and 8-bit VISCII formats
T}
vi-vnelvis-1.4#T{
vietnamese editors
T}#T{
A vi clone that speaks Vietnamese
T}
vi-vnless-1.0#T{
vietnamese
T}#T{
A pager utility that speaks Vietnamese
T}
vi-vnlpr-2.0#T{
vietnamese print
T}#T{
shell script and set of fonts to print Vietnamese text on PostScript printer
T}
vi-vnpstext-1.1#T{
vietnamese print
T}#T{
converts 8-bit VISCII Vietnamese text into PostScript
T}
vi-vnroff-2.0#T{
vietnamese print
T}#T{
converts Vietnamese VIQR text into troff format
T}
vi-vnterm-3.3#T{
vietnamese x11
T}#T{
An xterm that speaks Vietnamese
T}
vic-2.8#T{
mbone tk80
T}#T{
MBONE video tool.
T}
viewfax-2.3#T{
x11
T}#T{
Display files containing g3 and/or g4 coded fax pages.
T}
vile-7.3#T{
editors
T}#T{
VI Like Emacs.  a vi "workalike", with many additional features.
T}
vim-4.6#T{
editors
T}#T{
A vi "workalike", with many additional features.
T}
vim-5.0s#T{
editors
T}#T{
A vi "workalike", with many additional features.
T}
virtualpaper-1.4#T{
print
T}#T{
Virtual Paper document storage and viewing software.
T}
viz-1.1.1#T{
misc
T}#T{
Convert invisible (binary) characters to a visible form
T}
vmsbackup-3.0#T{
emulators sysutils
T}#T{
Reads VMS BACKUP tapes.
T}
vrweb-1.5#T{
www
T}#T{
a browser for a VRML 1.0 format file
T}
vscan-1.0.2#T{
security
T}#T{
scan MS-DOS files for viruses
T}
vtcl-1.10#T{
devel x11 tk80
T}#T{
An Application Development Environment For the TCL/TK language.
T}
w3-2.2.26#T{
www
T}#T{
WWW browser based on emacs/mule
T}
w3c-httpd-3.0A#T{
www
T}#T{
WWW server from the W3 Consortium (W3C).
T}
wb-1.59#T{
mbone
T}#T{
A shared drawing (whiteboard) tool using multicast.
T}
wdiff-0.5#T{
textproc
T}#T{
Display word differences between text files.
T}
webcopy-0.98b7#T{
www
T}#T{
A Web Mirroring Program
T}
weblint-1.020#T{
www
T}#T{
HTML validator and sanity checker
T}
webstone-2.0.1#T{
www benchmarks
T}#T{
world wide web server benchmarking
T}
wget-1.4.5#T{
net
T}#T{
Retrieve files from the 'net via HTTP and FTP.
T}
whirlgif-2.01#T{
graphics
T}#T{
a tool for making GIF animations.
T}
wide-dhcp-1.3b#T{
net
T}#T{
Dynamic Host Configuration Protocol, WIDE-Implimentation.
T}
wily-0.13.36#T{
plan9 editors
T}#T{
A clone of the Plan9 editor `acme'.
T}
windowmaker-0.6.3#T{
x11
T}#T{
This window manager is a GNUStep-compliant NeXTStep clone.
T}
wine-97.11.30#T{
emulators x11
T}#T{
MS-Windows 3.1/95/NT emulator for Unix (Alpha release).
T}
wm2-3.0#T{
x11
T}#T{
A very simple window manager for X
T}
wml-1.4.5#T{
www lang
T}#T{
Website META Language, a webdesigner's toolkit for HTML generation
T}
wn-1.18.3#T{
www
T}#T{
A great http server
T}
workman-1.3a#T{
audio
T}#T{
Open Look-based CD player tool
T}
wu-ftpd-2.4.2b15#T{
net
T}#T{
A replacement ftp server for Un*x systems.
T}
wwwcount-2.4#T{
www
T}#T{
Access counter, clock and date for WWW pages
T}
wwwstat-2.01#T{
www
T}#T{
webserver logfile analysis package
T}
x3270-3.1.1.6#T{
x11
T}#T{
3270 Terminal emulator.
T}
x48-0.4.0#T{
emulators
T}#T{
an HP48sx emulator
T}
xalarm-3.06#T{
x11
T}#T{
An X based alarm clock.
T}
xanim-2.70.6.4#T{
graphics x11
T}#T{
play most popular animation formats and show pictures
T}
xaniroc-1.02#T{
x11
T}#T{
Animate your root-cursor.
T}
xantfarm-1.16#T{
x11
T}#T{
ant hill simulation on X11 root window
T}
xaos-2.2#T{
graphics
T}#T{
A real time fractal browser for X11 and ASCII terminals.
T}
xarchie-2.0.10#T{
net
T}#T{
X11 front-end program for the archie network search service
T}
xasteroids-5.0#T{
games
T}#T{
X windows based asteroids style arcade game
T}
xautolock-1.10#T{
x11
T}#T{
Used to activate xlock after a user defined time of inactivity.
T}
xbat-1.11#T{
games
T}#T{
XEVIOUS like shooting game
T}
xbatt-1.2#T{
sysutils x11
T}#T{
Laptop battery status display for X11
T}
xbattle-5.4.1#T{
games
T}#T{
A concurrent multi-player battle strategy game
T}
xbill-2.0#T{
games
T}#T{
Save your computers from the evil clutches of Bill
T}
xbl-1.0h#T{
games
T}#T{
XBlockout - A 3d version of Tetris
T}
xblackjack-2.2#T{
games
T}#T{
an X11/Motif blackjack game
T}
xblast-2.6b#T{
games
T}#T{
Multi-player real-time strategy game for X11
T}
xbmbrowser-5.1#T{
graphics
T}#T{
View complete directories of X bitmaps and X pixmaps.
T}
xboard-3.6.2#T{
games
T}#T{
X frontend for GNU Chess and the Internet Chess Server
T}
xboing-2.4#T{
games
T}#T{
X windows arcade game.
T}
xbuffy-3.3#T{
x11
T}#T{
A replacement for xbiff that handles multiple mail files.
T}
xcalendar-4.0#T{
misc
T}#T{
A calendar with a notebook for X11 (internationarized version)
T}
xcb-2.3#T{
x11
T}#T{
A tool for managing x11 cut-buffers
T}
xcd-1.6#T{
audio tk80
T}#T{
A Tcl/Tk CD player.
T}
xcdplayer-2.2#T{
audio
T}#T{
CD player for X.
T}
xcept-2.1.2#T{
comms
T}#T{
a decoder for the CEPT (Btx) protocol
T}
xchomp-pl1#T{
games
T}#T{
Pac-man-like game under X Windows
T}
xco-1.3#T{
x11
T}#T{
Display X11 color names and colors
T}
xcoloredit-1.2#T{
x11
T}#T{
Find colour values by graphical colour mixing
T}
xcolors-1.3#T{
x11
T}#T{
Display all (ok, most of) the colors in the universe
T}
xcopilot-0.4.8#T{
emulators
T}#T{
Emulator for US Robotics Pilot PDA
T}
xcoral-3.1#T{
editors
T}#T{
A multiwindow mouse-based text editor for X.
T}
xcpustate-2.4#T{
sysutils
T}#T{
System monitoring utility graphicaly showing cpu load and status.
T}
xcubes-5.4.3#T{
games
T}#T{
cube puzzle for X-Window
T}
xdaliclock-2.10#T{
x11
T}#T{
A rather neat animated clock.
T}
xdeblock-1.0#T{
games
T}#T{
block action game
T}
xdeview-0.5.13#T{
converters tk80
T}#T{
An X11 program for uu/xx/Base64/BinHex de-/encoding.
T}
xdino-5.4.3#T{
games
T}#T{
dino puzzle game for X-Window
T}
xdl-2.1#T{
graphics x11
T}#T{
Display DL animations on an X screen
T}
xdtm-2.5.5#T{
x11
T}#T{
Desktop Manager is a graphical shell for the X Window System.
T}
xdu-3.0#T{
sysutils x11
T}#T{
Graphically display output of du
T}
xdvi-pl20#T{
print
T}#T{
A DVI Previewer for the X Window System.
T}
xearth-1.0#T{
astro x11
T}#T{
Set the root window to the image of earth.
T}
xed-1.3#T{
editors x11
T}#T{
A text editor for X.
T}
xemacs-19.16#T{
editors
T}#T{
XEmacs text editor binaries
T}
xemacs-20.2#T{
editors
T}#T{
XEmacs text editor version 20
T}
xemeraldia-0.3#T{
games
T}#T{
A game of breaking blocks
T}
xengine-1.0.1#T{
benchmarks x11
T}#T{
reciprocating engine for X
T}
xephem-3.0#T{
astro
T}#T{
an interactive astronomical ephemeris program
T}
xevil-1.5#T{
games
T}#T{
A side-view, fast-action, kill everything type of game. 
T}
xeyes+-2.01#T{
games
T}#T{
Horrible eyes looking at your mouse cursor
T}
xfaces-3.3#T{
mail
T}#T{
mail image display for X
T}
xfce-1.2.4#T{
x11
T}#T{
XForm Cool Environment
T}
xfed-1.0#T{
x11
T}#T{
A program that will let you edit X fonts (.bdf files)
T}
xfedor-1.0#T{
x11
T}#T{
a .bdf fonts/.xbm bitmaps/.xpm colored pixmaps/mouse cursor editor
T}
xfig-3.2#T{
graphics x11
T}#T{
A drawing program for X11
T}
xfishtank-2.2#T{
x11
T}#T{
Make fish swim in the background of your screen.
T}
xfm-1.3.2#T{
x11
T}#T{
The X File Manager.
T}
xfmail-1.1#T{
mail
T}#T{
An X Window System application for receiving electronic mail
T}
xforms-0.86#T{
graphics x11
T}#T{
A graphical user interface toolkit for X Window System.
T}
xfractint-3.04#T{
graphics
T}#T{
The Unix port of fractint.
T}
xfrisk-0.99c09#T{
games
T}#T{
A multi-player networked Risk game for X11.
T}
xfsm-1.95#T{
sysutils x11
T}#T{
X File System Monitor
T}
xgalaga-1.6c#T{
games
T}#T{
Galaga resurrected on X
T}
xgammon-0.98#T{
games
T}#T{
A backgammon program for X11.
T}
xgas-1.0#T{
misc
T}#T{
The animated simulation of an ideal gas
T}
xgmod-2.0.2#T{
audio
T}#T{
Gravis Ultrasound .MOD player with X Interface.
T}
xgolgo-1.0#T{
games
T}#T{
Xgolgo watches what you are doing and whether you're hostile or not
T}
xgrab-2.41#T{
x11
T}#T{
An X-Windows image grabber.
T}
xgraph-11.3.2#T{
math print
T}#T{
A program that helps you plot graphs
T}
xgrasp-1.7d#T{
graphics x11
T}#T{
Display GL animations on X screen
T}
xgs-0.50#T{
emulators
T}#T{
Apple IIGS emulator stable release 0.50
T}
xinetd-2.2.1#T{
security
T}#T{
Replacement for inetd with control and logging
T}
xinvaders-1.1#T{
games
T}#T{
Shoot-em-up them nasty little bugs.
T}
xinvest-2.5.1#T{
misc
T}#T{
A personal finance tracking and performance tool
T}
xipmsg-0.8087#T{
net
T}#T{
A popup style message communication tool for X11
T}
xjewel-1.6#T{
games
T}#T{
X windows dropping jewels game.
T}
xjig-2.4#T{
games
T}#T{
jigsaw puzzle game for X11
T}
xkeycaps-2.38#T{
x11
T}#T{
Graphically display and edit the keyboard mapping.
T}
xkobo-1.11#T{
games
T}#T{
Multi-way scrolling shoot 'em up game for X.  Strangely addictive.
T}
xlbiff-3.0#T{
mail x11
T}#T{
the X Literate Biff - displays the from and subject from incoming mails
T}
xless-1.7#T{
misc
T}#T{
An X11 viewer for text files.  Useful as an add-on tool for other apps.
T}
xli-1.16#T{
graphics x11
T}#T{
xli, xsetbg, xview, xlito - utilities to display images on X11.
T}
xlife-3.0#T{
games
T}#T{
John Horton Conway's Game of Life
T}
xlispstat-3.44#T{
math lang
T}#T{
A statistics/X11 based lisp interpreter.
T}
xloadface-1.6.1#T{
sysutils
T}#T{
network load monitor for X11
T}
xloadimage-3.03#T{
x11
T}#T{
X11 Image Loading Utility.
T}
xlockmore-4.06.1#T{
x11
T}#T{
Like XLock session locker/screen saver, but just more.
T}
xmaddressbook-1.5.3#T{
misc
T}#T{
XmAddressbook is a X11/Motif based addressbook program.
T}
xmahjongg-1.0#T{
games
T}#T{
The Chinese game of Mahjongg for X11
T}
xmake-1.01#T{
devel
T}#T{
A powerful make utility
T}
xmame-0.29.1#T{
emulators
T}#T{
Emulator that emulates many classic arcade game machines
T}
xmandel-1.0#T{
graphics
T}#T{
Window based interface to Mandelbrot sets and Julia sets
T}
xmascot-2.5p2#T{
x11
T}#T{
moving mascot on your X-Window screen.
T}
xmbase-grok-1.3.2#T{
databases x11
T}#T{
Graphical Resource Organizer Kit
T}
xmbdfed-2.7#T{
x11
T}#T{
A Motif tool for editing X11 bitmap fonts
T}
xmbibtex-1.3#T{
print databases
T}#T{
A reference manager based on the BibTeX file format.
T}
xmcd-2.2#T{
audio
T}#T{
Motif CD player.
T}
xmfract-1.4#T{
graphics
T}#T{
the dos based `fractint' with an X/Motif front end
T}
xmgr-3.01#T{
print
T}#T{
plotting tool for workstations
T}
xmille-2.0#T{
games
T}#T{
X window mille bourne game
T}
xmine-1.0.3#T{
games
T}#T{
The `Athena' port of the xminesweeper game
T}
xminehunter-0.4#T{
games
T}#T{
A Motif minesweeper game.
T}
xminesweep-3.0#T{
games
T}#T{
X windows minesweeper game.
T}
xmix-2.1#T{
audio
T}#T{
Mixer for X Window System.
T}
xmj-1.0#T{
games
T}#T{
Mahjongg
T}
xmmix-1.2#T{
audio
T}#T{
a Motif based audio mixer
T}
xmold-1.0#T{
x11
T}#T{
mold spreading over your X-Window screen
T}
xmorph-97.01.17#T{
graphics
T}#T{
a digital image warping program
T}
xmountains-2.4#T{
graphics
T}#T{
X11 based fractal landscape generator. 
T}
xmpeg3-1.0#T{
audio
T}#T{
Simple TCL interface to the Amp Mpeg-3 player
T}
xmris-4.04#T{
games
T}#T{
A version of the Mr Do video arcade game for X11.
T}
xmysql-1.5#T{
databases
T}#T{
X11 front end to the MySQL database engine
T}
xmysqladmin-0.2.0#T{
databases
T}#T{
X11 front end to the msqladmin command of the MySQL database engine
T}
xneko-4.4#T{
x11 games
T}#T{
The classic BSD4.4 cat-and-mouse.
T}
xonix-1.4#T{
games
T}#T{
Try to win land without colliding with `flyers' and `eaters'.
T}
xopps-1.13#T{
misc
T}#T{
A tool which lets you draw gantt charts.
T}
xosview-1.5.0#T{
sysutils x11
T}#T{
A graphical performance meter
T}
xpacman-1.0#T{
games
T}#T{
An old action game.
T}
xpaint-2.5.2#T{
graphics x11
T}#T{
A simple paint program.
T}
xpat2-1.04#T{
games
T}#T{
An X11 solitaire game with 14 variations.
T}
xpbiff-1.27#T{
mail
T}#T{
A replacement for xbiff that handles popup window with mail header.
T}
xpbiff-youbin-1.27#T{
mail
T}#T{
A replacement for xbiff that handles popup window with mail header via youbin.
T}
xpdf-0.7#T{
graphics print
T}#T{
Display tool for PDF files
T}
xperfmon++-1.40#T{
sysutils x11
T}#T{
A graphical X11 system performance monitor.
T}
xphoon-91.9.18#T{
astro x11
T}#T{
Set the root window to the moon in its current phase
T}
xpilot-3.6.1#T{
games
T}#T{
xpilot(client) and xpilots(server)
T}
xpipeman-1.0#T{
games
T}#T{
Connect the pipes to stop the leaks.
T}
xpl486-4.1#T{
lang
T}#T{
The XPL compiler generator system
T}
xplot-0.89#T{
math graphics net
T}#T{
X-windows plotting package
T}
xpm-3.4j#T{
graphics x11
T}#T{
The X Pixmap library.
T}
xpns-1.0#T{
misc cad
T}#T{
Petri-Net Simulator for Xwindows
T}
xpostit-3.3.1#T{
x11
T}#T{
PostIt (R) messages onto your X11 screen
T}
xprompt-1.4#T{
x11
T}#T{
Displays a dialog box and prompts user for text.  
T}
xpuyo-0.0#T{
games
T}#T{
puyo-puyo, tetris like puzzle game for X-Window.
T}
xpuzzletama-1.5#T{
games
T}#T{
Puzzle tama, a tetris like game
T}
xripple-1.0#T{
games
T}#T{
Nifty X program to make the screen bottom ripple like a pool of water.
T}
xrisk-2.13#T{
games
T}#T{
A game for X Windows, much like the popular board game.
T}
xroach-4.4#T{
x11 games
T}#T{
Cockroaches hide under your windows.
T}
xrobots-1.0#T{
games
T}#T{
Fight off villainous robots (X version).
T}
xrubik-5.4.3#T{
games
T}#T{
X-based rubik's cube(tm)
T}
xscrabble-1.0#T{
games
T}#T{
X version of the popular board game
T}
xscreensaver-2.11#T{
x11
T}#T{
Save your screen while you entertain your cat
T}
xshisen-1.36#T{
games
T}#T{
Shisen-sho puzzle game for X11
T}
xshogi-1.2.3#T{
games
T}#T{
The Japanese chess-like game for X Window System.
T}
xskat-1.6#T{
games
T}#T{
Play the card game Skat.
T}
xskyroot-920428#T{
x11
T}#T{
realtime sky drawer for X root window
T}
xsnow-1.40#T{
x11
T}#T{
Create a snowy and Santa-y desktop.
T}
xsokoban-3.3b#T{
games
T}#T{
a puzzle of pushing objects to the goals.
T}
xsol-2.1.1#T{
games
T}#T{
Solitaire.
T}
xsoldier-0.96#T{
games x11
T}#T{
shooting game for x11
T}
xspread-2.1#T{
math
T}#T{
A spreadsheet program under X.
T}
xspringies-1.1#T{
games
T}#T{
a mass and spring simulation system.
T}
xsysinfo-1.4#T{
sysutils
T}#T{
A system information display tool.
T}
xsysstats-1.34#T{
sysutils x11
T}#T{
A system information display tool
T}
xtacy-1.13#T{
x11
T}#T{
an X11 trippy color-cycling toy
T}
xtar-1.4#T{
misc
T}#T{
View and manipulate contents of a tar file.
T}
xtattr-1.0#T{
x11
T}#T{
A tool for changing xterm attributes
T}
xtestpicture-1.1#T{
x11
T}#T{
Create a full-screen image to adjust your monitor
T}
xtic-1.12#T{
games
T}#T{
An X version of a simple but tricky board game
T}
xtide-1.6.2#T{
astro
T}#T{
Harmonic tide clock and tide predictor
T}
xtimer-0.8087#T{
misc
T}#T{
A super simple digital timer for X11
T}
xtoolwait-1.1#T{
x11
T}#T{
Tool startup utility for X11
T}
xtriangles-5.4.3#T{
games
T}#T{
Triangles puzzle
T}
xv-3.10a#T{
graphics x11
T}#T{
An X11 program that displays images of various formats.
T}
xvgr-2.10.1#T{
math graphics
T}#T{
ACE/gr - graphics for exploratory data analysis
T}
xview-clients-3.2.1#T{
x11
T}#T{
OpenLook applications and man pages.
T}
xview-config-3.2.1#T{
x11
T}#T{
OpenLook Toolkit config files.
T}
xview-lib-3.2.1#T{
x11
T}#T{
OpenLook Toolkit libs, includes, and man pages.
T}
xvile-7.3#T{
editors
T}#T{
VI Like Emacs, X11 version -- a fully "X aware" vi work-alike
T}
xvnews-2.3.2#T{
news
T}#T{
An OPEN LOOK news reader.
T}
xwpe-1.4.2#T{
devel
T}#T{
A Borland-like IDE programming environment.
T}
xxgdb-1.12#T{
devel
T}#T{
An X window interface for gdb
T}
xzoom-0.3#T{
x11
T}#T{
magnify, rotate, mirror the image on the X screen
T}
xzx-2.2.1#T{
emulators x11
T}#T{
ZX Spectrum (48K/128K/+3) Emulator for X11 Windows
T}
yahtzee-1.0#T{
games
T}#T{
A curses version of the dice game for one or more players.
T}
yale-tftpd-3.0#T{
net
T}#T{
Enhanced tftpd(8) from Yale University and cisco Systems
T}
yamsweeper-1.9#T{
games
T}#T{
Yet Another Mine Sweeper.
T}
yorick-1.4.1#T{
lang math
T}#T{
An Interpreted Language for Scientific Computing
T}
youbin-2.13#T{
mail
T}#T{
Mail arrival notification service package
T}
ytalk-3.0.2#T{
net
T}#T{
A new "talk" that can talk to multiple parties at once
T}
zephyr-2.0.4#T{
net
T}#T{
Real time message passing and notification service.
T}
zh-Wnn-4.2#T{
chinese
T}#T{
A Japanese/Chinese/Korean input method (only Chinese built)
T}
zh-big5con-0.1#T{
chinese
T}#T{
Big5 Chinese console
T}
zh-big5fonts-1.0#T{
chinese x11
T}#T{
X11 Big5 font, widely used in taiwan/hong kong.
T}
zh-c2t-1.0#T{
chinese
T}#T{
translates GB/Big5 encoding to tone pinyin
T}
zh-celvis-1.3#T{
chinese editors
T}#T{
A vi/ex clone that speaks Chinese
T}
zh-cless-290#T{
chinese misc
T}#T{
A better pager utility (and it speaks Chinese)
T}
zh-cnsfonts-1.0#T{
chinese x11
T}#T{
X11 CNS11643-1986 traditional chinese (taiwanese standard) font collectionn.
T}
zh-cxterm-5.0.3#T{
chinese x11
T}#T{
An xterm that speaks Chinese
T}
zh-gb2ps-2.02#T{
chinese print
T}#T{
converts Chinese GB (simple) encoded text to PostScript
T}
zh-gbfonts-1.0#T{
chinese x11
T}#T{
X11 GB2312-80 Chinese national standard fonts (Guo2jia1 Biao2zhun3) fonts.
T}
zh-gbscript-1.11#T{
chinese
T}#T{
converts GB simplified Chinese text to PostScript
T}
zh-hc-3.0#T{
chinese
T}#T{
Hanzi Converter -- converts between GB and BIG-5 codes
T}
zh-hztty-2.0#T{
chinese
T}#T{
a translator between GuoBiao / Big5 and HZ
T}
zh-kcfonts-1.01#T{
chinese x11
T}#T{
Kuo Chau Chinese Fonts collection.
T}
zh-lunar-2.1#T{
chinese
T}#T{
convert between the Gregorian Solar Calendar (SC) and the Lunar Calendar (LC).
T}
zh-mule-wnn4-2.3#T{
chinese editors
T}#T{
A multilingual emacs, with Wnn4 support built in (Only the executables)
T}
zh-nvi-big5-1.79-970820#T{
chinese editors
T}#T{
A clone of vi/ex, with multilingual patch, default settings for big5.
T}
zh-nvi-euccn-1.79-970820#T{
chinese editors
T}#T{
A clone of vi/ex, with multilingual patch, default settings for euc-cn.
T}
zh-nvi-euctw-1.79-970820#T{
chinese editors
T}#T{
A clone of vi/ex, with multilingual patch, default settings for euc-tw.
T}
zh-pine-3.96#T{
chinese mail news
T}#T{
Program for Internet E-mail and News with Chinese support
T}
zh-rxvt-2.4.4#T{
chinese x11
T}#T{
A low memory usage xterm replacement that supports color & chinese
T}
zh-samba-1.9.17.4#T{
chinese net
T}#T{
A LanManager(R)-compatible server suite for Unix (config w/ Chinese filename)
T}
zh-samba-des-1.9.17.4#T{
chinese net
T}#T{
A LanManager(R)-compat. srv suite w/ WinNT en. pass + cn filename
T}
zh-ted-4.0h#T{
chinese editors
T}#T{
A Small and Powerful Text Editor for X Window with big5 support
T}
zh-tin-1.4.971204#T{
chinese news
T}#T{
TIN newsreader (termcap based)
T}
zip+crypt-2.2#T{
archivers
T}#T{
Create/update ZIP files compatible with pkzip, passwords enabled
T}
zip-2.2#T{
archivers
T}#T{
Create/update ZIP files compatabile with pkzip.
T}
zircon-1.18.71#T{
net tk80
T}#T{
An X11 interface to Internet Relay Chat.
T}
zmtx-zmrx-1.02#T{
comms
T}#T{
Receive/Send files via ZMODEM protocol.  (unrestrictive)
T}
zoo-2.10.1#T{
archivers
T}#T{
Manipulate archives of files in compressed form.
T}
zorro-1.1.8#T{
misc tk80
T}#T{
A simple to-do list manager.
T}
zsh-3.0.5#T{
shells
T}#T{
The Z shell.
T}
ztrack-1.0#T{
games
T}#T{
Simple ncurses based pseudo-3D driving game
T}
