# Paths for standard programs.  This is included by the main
# Makefile
#
# $Id: paths.make,v 2.10 2006/02/25 04:43:34 grog Exp $
#
TOOLS = ../tools
TBL = tbl
PIC = pic
TROFF = groff
MPIC = /usr/share/tmac/tmac.pic
MACROFILE = ../tools/tmac.Mn
HTMACROFILE = ../tools/tmac.html
MANMACROS = ${TOOLS}/tmac.andoc-book
SOELIM = soelim 2>/dev/null
WWW	=	/echunga/usr/local/www/data/cfbsd
