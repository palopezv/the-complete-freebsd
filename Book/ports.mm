.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: ports.mm,v 4.12 2003/04/02 06:43:08 grog Exp grog $
\"
.Chapter \*[nchports] "The Ports Collection"
.Pn ports-collection
.X "porting"
The Internet is full of free software that is normally distributed in source
form.  That can be a problem in itself: the way from the source archive that you
get free from the Internet to the finished, installed, running program on your
machine\(emnormally called \fIporting\fP\(emcan be a long and frustrating one.
See my book \fIPorting UNIX Software\fP\/ for more details of the porting
process.
.P
To get a software package up and running on your system, you need to go through
most of these steps:
.Ls N
.LI
.X "archive"
Get the source files on your machine.  They are usually contained in an
\fIarchive\fP, a file containing a number of other files.  Archives used for the
ports collection are generally \fIgzipped tar\fP\/ files, packaged with
.Command tar
and compressed with
.Command gzip ,
but other formats are also possible.  Whatever the format, you'll typically use
.Command ftp
to get them to your machine.
.LI
.X "source tree"
Unpack the archive into a \fIsource tree\fP, in this case using
.Command gunzip
and
.Command tar .
.LI
Configure the package.  Most packages include shell scripts to do this.
Configuration performs a threefold adaptation of the package:
.Ls N
.LI
It adapts it to the system hardware.
.LI
It adapts it to the software environment you're running (in this case, FreeBSD).
.LI
It adapts it to your personal preferences.
.Le
.LI
.Pn make
Build the package.  For most packages, this involves compiling the source files
and creating executables.  The main tool for this purpose is
.Command make ,
which uses a set of rules, traditionally stored in a file called
.File Makefile ,
to decide how to build the package.  There is nearly always a
.File Makefile
in the sources, but the Ports Collection includes a second one that controls
the build at a higher level.
.LI
Install the package.  This involves mainly copying the executables,
configuration files and documentation created by a build to the correct place in
the directory hierarchy.
.LI
Configure the installed software.  This is similar in concept to package
configuration, except that it occurs in the run-time environment.  The package
configuration may perform all the necessary configuration for you.
.Le
.X "Ports Collection"
These are a lot of steps, and you'll often find they're laid through a
minefield: one false move, and everything blows up.  To make porting and
installing software easier, the FreeBSD team created a framework called the
\fIPorts Collection\fP, which makes it trivial to perform these steps.  It also
provides a method of packaging and installing the resultant ported software,
called \fIpackages\fP.  The CD-ROM edition of FreeBSD includes a large number of
pre-built packages that can be installed directly.
.P
In this chapter, we'll consider the following points as they relate to the
FreeBSD ports collection:
.Ls B
.LI
How to install a pre-compiled package.  We'll look at this in the next section.
.LI
What the ports tree is, and how to compile and install (``build'') a package.
We'll look at this on page
.Sref \*[build-port] .
.\" XXX check page number.
.LI
How to create and submit a new port, on page
.Sref \*[new-port] .
.Le
.H2 "How to install a package"
.X "installing, package"
.X "package, installing"
.Pn install-package
.X "packages, directory"
In FreeBSD parlance, a package is simply a special archive that contains those
files (usually executable binary files) that are installed when you build and
install a port.  Effectively it's a snapshot of the port build process that we
saw above, taken after step 4 has completed.  Compared to the full-blown port,
packages are much faster to install\(emit's usually a matter of seconds.  On the
other hand, they don't give you the choice of configuration that the complete
port does.  The distribution CD-ROMs contain a directory \fIpackages\fP with a
large number of pre-compiled software packages.  Alternatively, you can find
FreeBSD packages on many servers on the Internet\(emcheck the online handbook
for some places to look.
.P
To help maintain an overview, both ports and packages are divided into
categories.  They are stored in directories named after the category.  See the
file
.File /usr/ports/INDEX
for a list.  For example,
.Command emacs
under \fIeditors\fP\/ is currently in the file
\fIpackages/editors/emacs-21.2.tgz\fP, though this name will change with updated
versions of
.Command emacs .
For the latest version of the packages only, you'll find another copy without
the extension in
.File packages/Latest/emacs.tgz .
To install it, you enter:
.Dx
# \f(CBpkg_add /cdrom/packages/Latest/emacs.tgz\fP
.De
Alternatively, you can install packages from the
.Command sysinstall
final configuration menu shown in Figure
.Sref \*[config-menu] \&
on page
.Sref \*[config-menu-page] .
.H2 "Building a port"
.X "building ports"
.X "port, building"
.Pn build-port
.X "port, FreeBSD definition"
The more general way to install third-party software is with a \fIport\fP.  The
FreeBSD project uses the term \fIport\fP\/ to describe the additional files
needed to adapt a package to build under FreeBSD.  It does \fInot\fP\/ include
the source code itself, though the CD-ROM distribution includes many code
archives in the directory
.Directory /ports/distfiles ,
spread over several of the CD-ROMs.
.P
.X "/usr/ports, directory"
Before you get started with the ports, you need to install the port information
on your system.  Normally this will be in
.Directory /usr/ports .
.X "ports tree"
This directory tree is frequently called the \fIPorts Tree\fP.  There are a
number of ways to install them.
.H3 "Installing ports during system installation"
.X "ports, installing"
The simplest way to install the Ports Collection is when you install the system.
When you choose the components to install,
.Command sysinstall
offers to install the Ports Collection for you as well.
.H3 "Installing ports from the first CD-ROM"
The file \fIports/ports.tgz\fP\/ on the first CD-ROM is a \fItar\fP\/ archive
containing all the ports.  If you didn't install it during system installation,
use the following method to install the complete collection (about 200 MB).
Make sure your CD-ROM is mounted (in this example on
.Directory /cdrom \/),
and enter:
.Dx
# \f(CBcd /usr\fP
# \f(CBtar xzvf /cdrom/ports/ports.tgz\fP
.De
If you only want to extract a single package, say
.Command inn ,
which is in the category \fInews\fP, enter:
.Dx
# \f(CBcd /usr\fP
# \f(CBtar xzvf /cdrom/ports/ports.tgz  ports/news/inn\fP
.De
It takes a surprisingly long time to install the ports; although there isn't
much data in the archive, there are about 250,000 files in it, and creating that
many files takes a lot of disk I/O.
.H3 "Installing ports from the live file system CD-ROM"
Alternatively, the files are also on the live file system CD-ROM.  This is not
much of an advantage for installation, but you may find it convenient to browse
through the source trees in the directory \fIports\fP\/ on the CD-ROM.  Let's
assume you have found a directory
.Directory /usr/ports/graphics/hpscan
on the CD-ROM, and it is your current working directory.  You can move the data
across with the following:
.Dx
# \f(CBcd /cdrom/ports/graphics\fP
# \f(CBmkdir -p /usr/ports/graphics\fP
# \f(CBtar cf - . | (cd /usr/ports/graphics; tar xvf -)\fP
.De
.sp -1v
.H3 "Getting new ports"
.X "ports, getting new versions"
What happens when a new version of a port comes out?  For example, you've been
using Emacs Version 20 forever, and now Version 21.2 becomes available?  It's
brand new, so it's obviously not on your CD-ROM.
.P
One way to get the port is via \fIftp\fP.  This used to be quite convenient: you
could download a tarball directly and extract it locally.  That is unfortunately
no longer possible: currently you must download files a directory at a time.  If
you're following the Ports Collection at all closely, you should consider using
.Command cvsup ,
which can keep your sources up to date automatically.  See  Chapter
.Sref \*[nchcurrent] ,
page
.Sref \*[CVSup] ,
for more details.
.P
All ports are kept in subdirectories of the URL
\fIftp://ftp.FreeBSD.org/pub/FreeBSD/ports/\fP.  This directory has the
following contents:
.Dx
drwxr-xr-x    6 1006  1006      512 Jun  8 13:18 alpha
drwxr-xr-x  209 1006  1006   401408 May 28 14:08 distfiles
drwxr-xr-x    6 1006  1006     1536 May 28 17:53 i386
drwxr-xr-x    3 1006  1006      512 Apr  6 13:45 ia64
drwxr-xr-x   83 1006  1006     3072 May 20 15:35 local-distfiles
lrwxrwxrwx    1 root  wheel      13 Jun  1  2001 packages -> i386/packages
lrwxrwxrwx    1 root  wheel      24 Jun  1  2001 ports -> ../FreeBSD-current/ports
lrwxrwxrwx    1 root  wheel       5 Jun  1  2001 ports-current -> ports
lrwxrwxrwx    1 root  wheel       5 Jun  1  2001 ports-stable -> ports
drwxr-xr-x    4 1006  1006      512 Apr  9 10:37 sparc64
.De
The directories \fIalpha\fP, \fIi386\fP, \fIia64\fP\/ and \fIsparc64\fP\/
contain packages (not ports) for the corresponding architecture.
\fIdistfiles\fP\/ contains a large number of the original sources for the
third-party packages; it's intended as a ``last resort'' location if you can't
find them at other locations.
.P
The directory \fIlocal-distfiles\fP\/ is used by people working on the Ports
Collection; you don't normally need anything from these directories.  The
important directories for you are \fIports\fP, \fIports-current\fP\/ and
\fIports-stable\fP.  Currently these are really all the same directory, but
things may not remain like that.
.P
Getting back to your
.Command emacs
port: you would find it in the directory
.Directory /pub/FreeBSD/ports/ports/editors/ .
Note the final \f(CW/\fP in that directory name: if you leave it out,
.Command ftp
prints an error message and exits.  Here's what might happen:
.Dx
$ \f(CBftp ftp://ftp.FreeBSD.org/pub/FreeBSD/ports/ports/editors/\fP
Connected to ftp.beastie.tdk.net.
220 ftp.beastie.tdk.net FTP server (Version 6.00LS) ready.
331 Guest login ok, send your email address as password.
230- The FreeBSD mirror at Tele Danmark Internet.
\&\fI...much blurb omitted\fP\/
250 CWD command successful.
250 CWD command successful.
ftp> \f(CBls\fP
229 Entering Extended Passive Mode (|||55649|)
150 Opening ASCII mode data connection for '/bin/ls'.
total 704
\&\fI...\fP\/
drwxr-xr-x  3 1006  1006   512 May 20 10:07 emacs
drwxr-xr-x  4 1006  1006   512 May 20 10:08 emacs20
drwxr-xr-x  4 1006  1006   512 May 20 10:08 emacs20-dl
drwxr-xr-x  4 1006  1006   512 May 20 10:08 emacs20-mule-devel
drwxr-xr-x  3 1006  1006   512 May 20 10:08 emacs21
drwxr-xr-x  2 1006  1006   512 May 20 10:08 eshell-emacs20
\&\fI...\fP\/
.De
This shows that your files will be in the directory \fIemacs21\fP.  You can get
them with the
.Command ftp
\f(CWmget\fP command:
.Dx
ftp> \f(CBmget emacs21\fP
mget emacs21/files [anpqy?]? \f(CBa\fP          \fIanswer \f(CBa\fI for all files\f(CW
Prompting off for duration of mget.
ftp: local: emacs21/files: No such file or directory
ftp: local: emacs21/Makefile: No such file or directory
\fI(etc)\fP\/
.De
This happens because you need to create the destination directory manually.  Try
again:
.Dx
ftp> \f(CB!mkdir emacs21\fP                     \fIcreate the local directory\fP\/
ftp> \f(CBmget emacs21\fP
mget emacs21/files [anpqy?]? \f(CBa\fP
Prompting off for duration of mget.
229 Entering Extended Passive Mode (|||57074|)
550 emacs21/files: not a plain file.
229 Entering Extended Passive Mode (|||57085|)
150 Opening BINARY mode data connection for 'emacs21/Makefile' (2185 bytes).
100% |*************************************|  2185       2.34 MB/s    00:00 ETA
226 Transfer complete.
\fI(etc)\fP\/
.De
You get one of these for each file transferred.  But note the error message:
\fInot a plain file\fP.  \fIemacs21/files\fP\/ is a directory, so we need to get
it separately:
.Dx
ftp> \f(CB!mkdir emacs21/files\fP
ftp> \f(CBmget emacs21/files\fP
mget emacs21/files/patch-lib-src:Makefile.in [anpqy?]? \f(CBa\fP
Prompting off for duration of mget.
229 Entering Extended Passive Mode (|||57258|)
150 Opening BINARY mode data connection for 'emacs21/files/patch-lib-src:Makefile.in
\&' (908 bytes).
100% |*************************************|   908       1.64 MB/s    00:00 ETA
226 Transfer complete.
\fI(etc)\fP\/
.De
Note that
the
.Command ftp
command specifies the URL of the directory.  It must have a trailing \f(CW/\fP,
otherwise
.Command ftp
will complain.  This form is supported by FreeBSD \fIftp\fP, but many other
.Command ftp
clients will require you to do it in two steps:
.Dx
# \f(CBftp ftp.FreeBSD.org\fP
Connected to ftp.beastie.tdk.net.
\fI(etc)\fP
ftp> \f(CBcd /pub/FreeBSD/ports/ports/editors\fP
250 CWD command successful.
.De
.sp -1v
.H3 "What's in that port?"
.Pn ports-contents
One problem with the Ports Collection is the sheer number.  It can be difficult
just to find out what they're supposed to do.  If you build all the ports,
you'll be busy for weeks, and there's no way you could read all the
documentation in one lifetime.  Where can you get an overview?  Here are some
suggestions.  In each case, you should have the directory
.Directory /usr/ports
as
your current working directory.
.Ls B
.LI
There's an index in
.File /usr/ports/INDEX .
If you have updated the ports tree, you can make the index with the following
commands:
.Dx
# \f(CBcd /usr/ports\fP
# \f(CBmake index\fP
.De
\f(CWindex\fP is the name of a \fItarget\fP, the part of a rule that identifies
it.  It's usually either a file name or an abbreviation for an operation to
perform.  We'll see a number of
.Command make
targets in the course of the book.
.P
The index is intended for use by other programs, so it's written as a single
long line per package, with fields delimited by the vertical bar character
(\f(CW|\fP).  Here are two lines as an example, wrapped over three lines to fit
on the page:
.Dx
mp3asm-0.1.3|/usr/ports/audio/mp3asm|/usr/local|MP3 frame level editor|/usr/port
s/audio/mp3asm/pkg-descr|ports@FreeBSD.org|audio| autoconf213-2.13.000227_1||htt
p://mp3asm.sourceforge.net/
.sp .3v
mp3blaster-3.0p8|/usr/ports/audio/mp3blaster|/usr/local|MP3 console ncurses-base
d player|/usr/ports/audio/mp3blaster/pkg-descr|greid@FreeBSD.org|audio|||http://
www.stack.nl/~brama/mp3blaster.html
.De
You'll probably want to process it with other tools.
.LI
You can print the index with the following commands:
.Dx
# \f(CBcd /usr/ports\fP
# \f(CBmake print-index | lpr\fP
.De
Note that there are about 1,000 pages of output, which look like this:
.br
.Dx
Port:   zip-2.3_1
Path:   /usr/ports/archivers/zip
Info:   Create/update ZIP files compatible with pkzip
Maint:  ache@FreeBSD.org
Index:  archivers
B-deps: unzip-5.50
R-deps:
.De
.SPUP
.LI
You can search for a specific keyword with the \f(CWsearch\fP target.  For
example, to find ports related to \fIEmacs\fP, you might enter:
.Dx
# \f(CBcd /usr/ports\fP
# \f(CBmake search key=Emacs | less\fP
.De
Pipe the output through
.Command less \/:
it can be quite a lot.
.LI
You can build a series of nearly 10,000 \fIhtml\fP\/ pages like this:
.Dx
# \f(CBcd /usr/ports\fP
# \f(CBmake readmes\fP
.De
You can then browse them at the URL \fIfile:///usr/ports/README.html\fP.
.Le
.H3 "Getting the source archive"
.X tarball
You'll see from the above example that there are not many files in the port.
Most of the files required to build the software are in the original source code
archive (the ``tarball''), but that's not part of the port.
.P
There are a number of places from which you can get the sources.  If you have a
CD-ROM set, many of them are scattered over the CD-ROMs, in the directory
.Directory /cdrom/ports/distfiles
on each CD-ROM.  The Ports Collection Makefiles look for them in this directory
(another good reason to mount your CD-ROM on
.Directory /cdrom \/)
and also in
.Directory /usr/ports/distfiles .
.P
If you don't have the source tarball, that's not a problem.  Part of the
function of the Ports Collection is to go out on the Net and get them for you.
This is completely automatic: you just type \f(CWmake\fP, and the build process
gets the source archive for you and builds it.  Of course, you must be connected
to the Internet for this to work.
.P
If you mount your CD-ROM elsewhere (maybe because you have more than one CD-ROM
drive, and so you have to mount the CD-ROM on, say,
.Directory -n /cd4 \/),
the Makefiles will not find the distribution files and will try to load the
files from the Internet.  One way to solve this problem is to create a symbolic
link from
.Directory -n /cd4/ports/distfiles
to
.Directory /usr/ports/distfiles .
The trouble with this approach is that you will then no longer be able to load
new distribution files into
.Directory /usr/ports/distfiles ,
because it will be on CD-ROM.  Instead, do:
.Dx
# \f(CBcd /cd4/ports/distfiles\fP
# \f(CBmkdir -p /usr/ports/distfiles\fP         \fImake sure you have a distfiles directory\fP\/
# \f(CBfor i in *; do\fP
> \f(CB  ln -s /cd4/ports/distfiles/$i /usr/ports/distfiles/$i\fP
> \f(CBdone\fP
.De
If you're using
.Command csh
or
.Command tcsh ,
enter:
.Dx
# \f(CBcd /cd4/ports/distfiles\fP
# \f(CBmkdir -p /usr/ports/distfiles\fP         \fImake sure you have a distfiles directory\fP\/
# \f(CBforeach i (*)\fP
? \f(CB  ln -s /cd4/ports/distfiles/$i /usr/ports/distfiles/$i\fP
? \f(CBend\fP
.De
This creates a symbolic link to each distribution file, but if the file for a
specific port isn't there, the Ports Collection can fetch it and store it in the
directory.
.H3 "Building the port"
.X "building ports"
.X "ports, building"
.Pn new-port
Once you have the skeleton files for the port, the rest is simple.  Just enter:
.Dx
# \f(CBcd /usr/ports/editors/emacs21\fP
# \f(CBmake\fP
# \f(CBmake install\fP
====>
====> To enable menubar fontset support, define WITH_MENUBAR_FONTSET
====>
>> emacs-21.2.tar.gz doesn't seem to exist in /usr/ports/distfiles/.
>> Attempting to fetch from ftp://ftp.gnu.org/gnu/emacs/.
===>  Extracting for emacs-21.2_1
>> Checksum OK for emacs-21.2.tar.gz.
===>   emacs-21.2_1 depends on executable: gmake - found
===>   emacs-21.2_1 depends on executable: autoconf213 - not found
===>    Verifying install for autoconf213 in /usr/ports/devel/autoconf213
===>  Extracting for autoconf213-2.13.000227_2
>> Checksum OK for autoconf-000227.tar.bz2.
===>   autoconf213-2.13.000227_2 depends on executable: gm4 - not found
===>    Verifying install for gm4 in /usr/ports/devel/m4
===>  Extracting for m4-1.4_1
>> Checksum OK for m4-1.4.tar.gz.
===>  Patching for m4-1.4_1
===>  Applying FreeBSD patches for m4-1.4_1
===>  Configuring for m4-1.4_1
creating cache ./config.cache
checking for mawk... no
\fI(etc)\fP\/
.De
It's a good idea to perform the
.Command make
step first: \fImake install\fP\/ does not always build the package.
.H3 "Port dependencies"
.X "ports, dependencies"
Sometimes, it's not enough to build a single port.  Many ports depend on other
ports.  If you have the complete, up-to-date ports tree installed on your
system, the Ports Collection will take care of this for you: it will check if
the other port is installed, and if it isn't, it will install it for you.  For
example,
.Command tkdesk
depends on
.Command tk .
.Command tk
depends on
.Command tcl .
If you don't have any of them installed, and you try to build
.Command tkdesk ,
it will recursively install
.Command tk
and
.Command tcl
for you.
.H2 "Package documentation"
Once you have installed your port, you'll want to use it.  In almost every case,
that requires documentation.  Most packages have documentation, but
unfortunately it's not always obvious where it is.  In some cases, the port
doesn't install all the documentation.
.P
More generally, there are the following possibilities:
.Ls B
.LI
If the port includes man pages, they will be installed in
.Directory /usr/X11R6/man
if the package is related to X, and
.Directory /usr/local/man
if they are not.  Typically installing the man pages is the last thing that
happens during the installation, so you should see it on the screen.  If not, or
if you want to check, you can have a look at the package list:
.Dx
$ \f(CBcd /var/db/pkg\fP
$ \f(CBpkg_info -L emacs-21.2_1|grep /man/\fP
/usr/local/man/man1/ctags.1.gz
/usr/local/man/man1/emacs.1.gz
/usr/local/man/man1/etags.1.gz
/usr/local/man/man1/gfdl.1.gz
.De
You don't need to change the directory to
.Directory /var/db/pkg ,
but if you do, you can use file name completion to finish the name of the
package.  We use \f(CW/man/\fP as the search string, and not simply \f(CWman\fP,
because otherwise other files might match as well.
.LI
If the package includes GNU \fIinfo\fP\/ pages, you can use the same method to
look for them:
.Dx
$ \f(CBpkg_info -L emacs-21.2_1|grep /info/\fP
/usr/local/info/ada-mode
/usr/local/info/autotype
/usr/local/info/ccmode
/usr/local/info/cl
\fI(many more)\fP\/
.De
This isn't normally necessary, though: if you're using GNU
.Command info ,
the index page will be updated to include the package.
.LI
If the package includes hardcopy documentation, it may or may not be included in
the port.  The
.Command -n Emacs
documentation also includes a user's guide and a programmer's guide.  The user's
guide, all 640 pages of it, is in the directory
.File -n man
of the
.Command -n Emacs
build directory, but it doesn't get built during installation.  This is typical
of most ports.  In this case you'll have to build the documentation yourself.
.Le
.SPUP
.H2 "Getting binary-only software"
A lot of software doesn't need to be ported.  For example, if you want
.Command -n Netscape ,
you can just download it from \fIftp.netscape.com\fP.  But how do you install
it?  Netscape's installation procedures are getting better, but they still leave
something to be desired.
.P
The answer's simple: take the port!  Although Netscape comes only in binary
form, the port handles getting the correct version and installing it for you.
Another advantage to using a port instead of installing the package manually is
that the port installs the software as a FreeBSD package, which makes it much
easier to remove the software later.
.P
This method can be used to install some other software as well, for example
StarOffice.  The moral is simple: always check the Ports Collection before
getting a software package from the Net.
.H2 "Maintaining ports"
.X "maintaining ports"
.X "ports, maintaining"
Once you install a port, you might consider that to be the end of the story.
That's seldom the case.  For example:
.Ls B
.LI
You might need to replace a port with a newer version.  How do you do it?  We'll
look at that below.
.LI
One day, you might find your disk fills up, so you go looking for old ports you
don't use any more.  We'll look at some utility commands on page
.Sref "\*[portsutils]" .
.Le
.SPUP
.H2 "Upgrading ports"
.Pn portsupgrade
From time to time, new versions of software will appear.  There are a number of
approaches to upgrading:
.Ls B
.LI
You can remove the old version of the port and install a new version.  The
trouble here is that removing the old version might remove any configuration
files as well.
.LI
You can install a new version without removing the old version.  The trouble
here is that you end up with two entries in the packages database
.Directory /var/db/pkg \/:
.Dx
$ \f(CBpkg_info | grep emacs\fP
emacs-21.1_5        GNU editing macros
emacs-21.2_1        GNU editing macros
.De
Clearly you don't need \fIemacs-21.1_5\fP\/ any more.  In fact, it's not
complete any more, because the program
.Command -n /usr/local/bin/emacs
has been overwritten by the new version.  But you can't remove it either: that
would remove components of \fIemacs-21.2_1\fP, which you want to keep.  On the
other hand, if you don't remove it, you are left with nearly 50 MB of disk space
used up in the directory
.Directory -n /usr/local/share/emacs/21.1 .
.LI
You can use
.Command portupgrade ,
a program that does some of the upgrading automatically.  We'll look at this
below.
.Le
.SPUP
.H3 "Using portupgrade"
Portupgrade is\(emwhat else?\(ema port.  Install it in the usual manner:
.Dx
# \f(CBcd /usr/ports/sysutils/portupgrade\fP
# \f(CBmake install\fP
.De
Before you can perform the upgrade, you should first back up
.Directory /var/db/pkg ,
then build a ports database with
.Command pkgdb .
A typical build might look like this:
.br
.ne 6v
.Dx
# \f(CBcd /var/db\fP
# \f(CBtar czvf db.pkg.tar.gz pkg/\fP
# \f(CBpkgdb -F\fP
[Updating the pkgdb <format:bdb1_btree> in /var/db/pkg ... - 181 packages
found (-5 +92) (...)........................................ done]
Checking the origin of AbiWord-1.0.3
Checking the origin of ImageMagick-5.5.1.1
Checking the origin of ORBit-0.5.17
\fI\&...\fP\/
Checking the origin of xv-3.10a_3
Checking the origin of zip-2.3_1
Checking for origin duplicates
Checking AbiWord-1.0.3
Checking ImageMagick-5.5.1.1
Stale dependency: ImageMagick-5.5.1.1 -> ghostscript-gnu-7.05_3:
ghostscript-gnu-6.52_4 (score:64%) ? ([y]es/[n]o/[a]ll) [no] \f(CBy\fP
Fixed. (-> ghostscript-gnu-6.52_4)
Checking ORBit-0.5.17
Checking XFree86-4.2.0_1,1
\fI\&...\fP\/
Checking bonobo-1.0.21_1
Stale dependency: bonobo-1.0.21_1 -> ghostscript-gnu-7.05_3:
ghostscript-gnu-6.52_4 ? ([y]es/[n]o/[a]ll) [yes] \f(BIEnter\fP \fIpressed
\fP\/
Fixed. (-> ghostscript-gnu-6.52_4)
Checking cdrtools-1.11.a28
\fI\&...\fP\/
Checking xv-3.10a_3
Checking zip-2.3_1
Regenerating +REQUIRED_BY files
Checking for cyclic dependencies
.De
In this example, the port \fIghostscript-gnu-7.05_3\fP\/ had been replaced by
the earlier version \fIghostscript-gnu-6.52_4\fP, since
.Command -n ghostscript
Release 7 has some annoying bugs.  The dialogue shows how
.Command pkgdb
recognized the discrepancy, and how it recovered from it.
.P
Now you can start the upgrade.  To upgrade a specific port, simply specify its
base name, without the version number.  This example uses the \f(CW-v\fP option
to show additional information:
.Dx
# \f(CBportupgrade -v bison
\fP
--->  Upgrade of devel/bison started at: Mon, 04 Nov 2002 13:20:52 +1030
--->  Upgrading 'bison-1.35_1' to 'bison-1.75' (devel/bison)
--->  Build of devel/bison started at: Mon, 04 Nov 2002 13:20:52 +1030
\fI\&... normal port build output\fP\/
===>   Registering installation for bison-1.75
\fImake clean issued by portupgrade\fP\/
===>  Cleaning for libiconv-1.8_2
===>  Cleaning for gettext-0.11.5_1
\&\fI...\fP\/
--->  Removing the temporary backup files
--->  Installation of devel/bison ended at: Mon, 04 Nov 2002 13:23:00 +1030 (consume
d 00:00:06)
--->  Removing the obsoleted dependencies
--->  Cleaning out obsolete shared libraries
--->  Upgrade of devel/bison ended at: Mon, 04 Nov 2002 13:23:01 +1030 (consumed 00:
02:08)
--->  Reporting the results (+:succeeded / -:ignored / *:skipped / !:failed)
        + devel/bison (bison-1.35_1)
.De
.ne 4v
If the port is already up to date, you'll see something like this:
.Dx
# \f(CBportupgrade -v perl-5.8.0_3\fP
** No need to upgrade 'perl-5.8.0_3' (>= perl-5.8.0_3). (specify -f to force)
--->  Reporting the results (+:succeeded / -:ignored / *:skipped / !:failed)
        - lang/perl5.8 (perl-5.8.0_3)
.De
To upgrade all ports, use the command:
.Dx
# \f(CBportupgrade -a\fP
.De
.SPUP
.H2 "Controlling installed ports"
.X "ports, controlling"
.Pn portsutils
We've already seen the program
.Command pkg_add
when installing pre-compiled packages.  There are a number of other
.Command -n pkg_
programs that can help you maintain installed ports, whether they have been
installed by
.Command pkg_add
or by \fImake install\fP\/ from the Ports Collection:
.Ls B
.LI
.Command pkg_info
tells you which ports are installed.  For example:
.Dx
$ \f(CBpkg_info | less\fP
AbiWord-1.0.3       An open-source, cross-platform WYSIWYG word proces
ImageMagick-5.5.1.1 Image processing tools (interactive optional--misc
ORBit-0.5.17        High-performance CORBA ORB with support for the C
XFree86-4.2.0_1,1   X11/XFree86 core distribution (complete, using min
\fI\&... etc\fP\/
bash-2.05b.004      The GNU Bourne Again Shell
bison-1.75          A parser generator from FSF, (mostly) compatible w
bonobo-1.0.21_1     The component and compound document system for GNO
cdrtools-1.11.a28   Cdrecord, mkisofs and several other programs to re
\fI\&... etc\fP\/
elm-2.4ME+22        ELM Mail User Agent
elm-2.4ME+32        ELM Mail User Agent
.De
Note that the last two entries in this example show that two versions of
.Command elm
are installed.  This can't be right; it happens when you install a new version
without removing the old version and without running
.Command portupgrade .
We'll discuss this matter further below.
.LI
If you have the ports tree installed, you can use
.Command pkg_version
to check whether your ports are up to date.
.Command pkg_version
is a little cryptic in its output:
.Dx
AbiWord-gnome                       =
ImageMagick                         <
ORBit                               <
Wingz                               =
XFree86                             <
\&\fI...\fP\/
x2x-1.28                            ?
.De
The symbols to the right of the package names have the following meanings:
.br
.ne 1i
.TS H
tab(#) ;
lfCWp9  lw58 .
.TH
\=#T{
The installed version of the package is current.
T}
<#T{
The installed version of the package is older than the current version.
T}
>#T{
The installed version of the package is newer than the current version.  This
situation can arise with an out-of-date index file, or when testing new ports.
T}
?#T{
The installed package does not appear in the index.  This could be due to an out
of date index or a package that has not yet been committed.
T}
*#T{
There are multiple versions of a particular software package listed in the index
file.
T}
!#T{
The installed package exists in the index but for some reason,
.Command pkg_version
was unable to compare the version number of the installed package with the
corresponding entry in the index.
T}
.TE
.LI
There are two ways to remove a port: if you've built it from source, and you're
in the build directory, you can write:
.Dx
# \f(CBmake deinstall\fP
.De
Alternatively, you can remove any installed package with
.Command pkg_delete .
For example, the list above shows two versions of the
.Command elm
mail user agent.  To remove the older one, we enter:
.Dx
# \f(CBpkg_delete elm-2.4ME+22\fP
File `/usr/local/man/man1/answer.1' doesn't really exist.
Unable to completely remove file '/usr/local/man/man1/answer.1'
File `/usr/local/man/man1/checkalias.1' doesn't really exist.
Unable to completely remove file '/usr/local/man/man1/checkalias.1'
\fI\&... etc\fP\/
Couldn't entirely delete package (perhaps the packing list is
incorrectly specified?)
.De
In this case, it looks as if somebody has tried to remove the files before, so
.Command pkg_delete
couldn't do so.
.P
Another problem with
.Command pkg_delete
is that it might delete files of the same name that have been replaced by newer
packages.  After performing this operation, we try:
.Dx
$ \f(CBelm\fP
bash: elm: command not found
.De
Oops!  We tried to delete the old version, but we deleted at least part of the
new version.  Now we need to install it again.
.P
The moral of this story is that things aren't as simple as they might be.  When
you install a new version of a package, you may want to test it before you
commit to using it all the time.  You can't just go and delete the old version.
One possibility would be to install the new package, and try it out.  When
you've finished testing, delete \fIboth\fP\/ packages and re-install the one you
want to keep.
.Le
.H4 "Keeping track of updates"
The best way to find out about updates is to subscribe to the
\f(CWFreeBSD-ports\fP mailing list.  That way, you will get notification every
time something changes.  If you're tracking the ports tree with \fICVSup\fP, you
also get the updates to the ports tree automatically.  Otherwise you will have
to download the port.  In either case, to update your installed port, just
repeat the build.
.SPUP
.H2 "Submitting a new port"
.X "submitting a port"
.X "ports, submitting"
The Ports Collection is constantly growing.  Hardly a day goes by without a new
port being added to the list.  Maybe you want to submit the next one?  If you
have something interesting that isn't already in the Ports Collection, you can
find instructions on how to prepare the port in the \fIFreeBSD Porter's
Handbook\fP\/.  The latest version is available on the FreeBSD web site, but
you'll also find it on your system as
.File /usr/share/doc/en/porters-handbook/index.html .
