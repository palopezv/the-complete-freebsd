.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: postinstall.mm,v 4.15 2003/12/15 00:52:31 grog Exp grog $
.\"
.Chapter \*[nchpostinstall] "Post-installation configuration"
In the last chapter we looked at the installation of the basic system, up to the
point where it could be rebooted.  It's barely possible that this could be
enough.  Almost certainly, though, you'll need to perform a number of further
configuration steps before the system is useful.  In this chapter we roughly
follow the final configuration menu, but there are a few exceptions.  The most
important things to do are:
.Ls B
.LI
Install additional software.
.LI
Create accounts for normal users.
.LI
Set up networking support.
.LI
Configure the system to start all the services you need.
.LI
Configure the X Window System and desktop.
.Le
In this chapter, we'll concentrate on getting the system up and running as
quickly as possible.  Later on in the book we'll go into more detail about these
topics.
.P
At the end of the previous chapter, we had a menu asking whether we wanted to
visit the ``last options'' menu.  If you answer \f(CWYES\fP, you get the
configuration menu shown in Figure
.Sref \*[config-menu] .
If you have rebooted the machine, log in as \f(CWroot\fP and start
.Command sysinstall .
Then select \f(CWConfigure\fP, which gets you into the same menu.
.PIC images/configuration-menu.ps 4i
.Figure-heading "Configuration menu"
.Fn config-menu
.P
As the markers under the word \f(CWNetworking\fP indicate, this menu is larger
than the window in which it is displayed.  We'll look at some of the additional
entries below.  Only some of these entries are of interest in a normal install;
we'll ignore the rest.
.P
There may be some reasons to deviate from the sequence in this chapter.  For
example, if your CD-ROM is mounted on a different system, you may need to set up
networking before installing additional software.
.Pn sysinstall
.H2 "Installing additional software"
.Pn pkg_add
The first item of interest is \f(CWPackages\fP.  These are some of the ports in
the Ports
..if complete
Collection, which we'll look at in more detail in Chapter
.Sref "\*[nchports]" .
..else
Collection.
..endif
.P
The Ports Collection contains a large quantity of software that you may want to
install.  In fact, there's so much that just making up your mind what to install
can be a complicated process: there are over 8,000 ports in the collection.
Which ones are worth using?  I recommend the following list:
.Ls B
.LI
.Command acroread
is the Acrobat reader, a utility for reading and printing PDF files.  We look at
it briefly on page
.Sref \*[acroread] .
.LI
.Command bash
is the shell recommended in this book.  We'll look at it in more detail on page
.Sref \*[shell] .
Other popular shells are
.Command tcsh
and
.Command csh ,
both in the base system.
.LI
.Command cdrecord
is a utility to burn SCSI CD-Rs.  We'll discuss it in chapter
.Sref "\*[cdburn]" .
You don't need it if you have an IDE CD-R drive.
.LI
.X emacs
.Command -n Emacs
is the GNU Emacs editor recommended in this book.  We'll look at it on page
.Sref \*[Emacs] .
Other popular editors are
.Command vi
(in the base system) and
.Command vim
(in the Ports Collection).
.LI
.Command fetchmail
is a program for fetching mail from POP mailboxes.  We look at it on page
.Sref \*[POP] .
.LI
.Command fvwm2
is a window manager that you may prefer to a full-blown desktop.  We look at it
on page
.Sref \*[fvwm2] .
.LI
.Command galeon
is a web browser.  We'll look at it briefly on page
.Sref \*[www] .
.LI
.Command ghostscript
is a PostScript interpreter.  It can be used to display PostScript on an X
display, or to print it out on a non-PostScript printer.  We'll look at it on
page
.Sref \*[ghostscript] .
.LI
.Command gpg
is an encryption program.
.LI
.Command gv
is a utility that works with \fIghostscript\fP\/ to display PostScript on an X
display.  It allows magnification and paging, both of which \fIghostscript\fP\/
does not do easily.  We'll look at it on page
.Sref \*[ghostscript] .
.LI
.Command ispell
is a spell check program.
.LI
.Command kde
is the desktop environment recommended in this book.  We'll look at it in more
detail in
.Sref "\*[chunixref]" .
.LI
.Command mkisofs
is a program to create CD-R images.  We look at it in chapter
.Sref "\*[cdburn]" .
.LI
.Command mutt
is the mail user agent (MUA, or mail reader) recommended in
.Sref "\*[chmua]" .
.LI
.Daemon postfix
is the mail transfer agent (MTA) recommended in chapter
.Sref "\*[chmta]" .
.LI
.Command xtset
is a utility to set the title of an
.Command xterm
window.  It is used by the
.File .bashrc
file installed with the \fIinstant-workstation\fP\/ package.
.LI
.Command xv
is a program to display images, in particular \fIjpeg\fP\/ and \fIgif\fP.
.\" XXX Others which we don't describe.
.ig
.LI
.Command xmms
is an X-based multimedia player.
.LI
.Command xamp
is a graphical MPEG player.
.LI
.Command dos2unix
is a small utility for converting Microsoft text formats to UNIX format.  It
does \fInot\fP\/ convert formats such as Microsoft Word and RTF.
.LI
.Command grip
provides an automated frontend for MP3 encoders, letting you take a disc and
transform it easily straight into MP3s.
.LI
The
.Command gimp
is designed to provide an intuitive graphical interface to a variety of image
editing operations.
..
.Le
Why do I recommend these particular ports?  Simple: because I like them, and I
use most of them myself.  That doesn't mean they're the only choice, though.
Others prefer the
.X "desktop, Gnome"
.X "Gnome, desktop"
.Command -n Gnome
window manager to
.X "desktop, kde"
.X "kde, desktop"
.Command -n kde ,
or the
.Command pine
or
.Command elm
MUAs to
.Command mutt ,
or the
.Command vim
editor to \fIEmacs\fP.  This is the stuff
of holy wars.  See
.URI http://catb.org/~esr/jargon/html/H/holy-wars.html
for more details.
.H3 "Instant workstation"
.Pn instant-workstation
The ports mentioned in the previous section are included in the
\fImisc/instant-workstation\fP\/ port, which installs typical software and
configurations for a workstation and allows you to be productive right away.  At
a later point you may find that you prefer other software, in which case you can
install it.
.P
.ne 2v
It's possible that the CD set you get will not include
\fIinstant-workstation\fP.  That's not such a problem.  Due to space
restrictions, some CD distributions include \fIinstant-workstation-lite\fP
instead.  If that's not there either, just install the individual ports from
this list.  You can also do this if you don't like the list of ports.
.H3 "Changing the default shell for root"
After installation, you may want to change the default shell for existing users
to
.Command bash .
If you have installed \fIinstant-workstation\fP, you should copy the file
.File /usr/local/share/dot.bashrc
to \f(CWroot\fP's home directory and call it \&
.File .bashrc
and
.File .bash_profile .
First, start
.Dx
presto# \f(CBcp /usr/local/share/dot.bashrc .bashrc\fP
presto# \f(CBln .bashrc .bash_profile\fP
presto# \f(CBbash\fP
=== root@presto (/dev/ttyp2) ~ 1 -> \f(CBchsh\fP
.De
The last command starts an editor with the following content:
.Dx
#Changing user database information for root.
Login: root
Password:
Uid [#]: 0
Gid [# or name]: 0
Change [month day year]:
Expire [month day year]:
Class:
Home directory: /root
Shell: /bin/csh
Full Name: Charlie &
Office Location:
Office Phone:
Home Phone:
Other information:
.De
Change the \f(CWShell\fP line to:
.Dx
Shell: \f(CB/usr/local/bin/bash\fP
.De
Note that the
.Command bash
shell is in the directory
.Directory /usr/local/bin ;
this is because it is not part of the base system.  The standard shells are in
the directory
.Directory /bin .
.H2 "Adding users"
.Pn firstuser
A freshly installed FreeBSD system has a number of users, nearly all for system
components.  The only login user is \f(CWroot\fP, and you shouldn't log in as
\f(CWroot\fP.  Instead you should add at least one account for yourself.  If
you're transferring a
.File master.passwd
file from another system, you don't need to do anything now.  Otherwise select
this item and then the menu item \f(CWUser\fP, and fill out the resulting menu
like this:
.PIC images/adduser-menu.ps 4i
.Figure-heading "Adding a user"
You should not need to enter the fields \f(CWUID\fP and \f(CWHome directory\fP:
.Command sysinstall
does this for you.  It's important to ensure that you are in group \f(CWwheel\fP
so that you can use the
.Command su
command to become \f(CWroot\fP, and you need to be in group \f(CWoperator\fP to
use the
.Command shutdown
command.
.P
Don't bother to add more users at this stage; you can do it later.  We'll look
at user management in Chapter
.Sref "\*[nchunixadmin]" ,
on page
.Sref \*[users-and-groups] .
.H3 "Setting the root password"
Next, select \f(CWRoot Password\fP.
..if tutorial
..else
We'll talk about passwords more on page
.Sref \*[logging-in] .
..endif
Select this item to set the password in the normal manner.
.H2 "Time zone"
.Pn set-timezone
Next, select the entry \f(CWtime zone\fP.  The first entry asks you if the
machine CMOS clock (i.e. the hardware clock) is set to UTC (sometimes
incorrectly called GMT, which is a British time zone).  If you plan to run only
FreeBSD or other UNIX-like operating systems on this machine, you should set the
clock to UTC.  If you intend to run other software that doesn't understand time
zones, such as many Microsoft systems, you have to set the time to local time,
which can cause problems with daylight savings time.
.PIC images/timezone-menu.ps 4i
.Figure-heading "Time zone select menu: USA"
.P
The next menu asks you to select a ``region,'' which roughly corresponds with a
continent.  Assuming you are living in Austin, TX in the United States of
America, you would select \f(CWAmerica -- North and South\fP and then (after
scrolling down) \f(CWUnited States of America\fP.  The next menu then looks like
this: Select \f(CWCentral Time\fP and select \f(CWYes\fP when the system asks
you whether the abbreviation \f(CWCST\fP sounds reasonable.
.P
This particular step is relatively cumbersome.  You may find it easier to look
in the directory
.Directory /usr/share/zoneinfo
after installation.  There you find:
.Dx
# \f(CBcd /usr/share/zoneinfo/\fP
# \f(CBls\fP
Africa         Australia      Etc            MET            WET
America        CET            Europe         MST            posixrules
Antarctica     CST6CDT        Factory        MST7MDT        zone.tab
Arctic         EET            GMT            PST8PDT
Asia           EST            HST            Pacific
Atlantic       EST5EDT        Indian         SystemV
.De
If you want to set the time zone to, say, Singapore, you could enter:
.Dx
# \f(CBcd Asia/\fP
# \f(CBls\fP
Aden           Chungking      Jerusalem      Novosibirsk    Tehran
Almaty         Colombo        Kabul          Omsk           Thimbu
Amman          Dacca          Kamchatka      Phnom_Penh     Tokyo
Anadyr         Damascus       Karachi        Pyongyang      Ujung_Pandang
Aqtau          Dili           Kashgar        Qatar          Ulaanbaatar
Aqtobe         Dubai          Katmandu       Rangoon        Ulan_Bator
Ashkhabad      Dushanbe       Krasnoyarsk    Riyadh         Urumqi
Baghdad        Gaza           Kuala_Lumpur   Saigon         Vientiane
Bahrain        Harbin         Kuching        Samarkand      Vladivostok
Baku           Hong_Kong      Kuwait         Seoul          Yakutsk
Bangkok        Hovd           Macao          Shanghai       Yekaterinburg
Beirut         Irkutsk        Magadan        Singapore      Yerevan
Bishkek        Istanbul       Manila         Taipei
Brunei         Jakarta        Muscat         Tashkent
Calcutta       Jayapura       Nicosia        Tbilisi
# \f(CBcp Singapore /etc/localtime \fP
.De
Note that the files in
.Directory /usr/share/zoneinfo/Asia
(and the other directories) represent specific towns, and these may not
correspond with the town in which you are located.  Choose one in the same
country and time zone.
.P
You can do this at any time on a running system.
.H2 "Network services"
.X "network, services"
.DF
.PIC images/network-services.ps 4i
.Figure-heading "Network services menu"
.Fn network-services
.DE
The next step is to configure your networking equipment.  Figure
.Sref \*[network-services] \&
.\" XXX .pageref \*[network-services-page] "on page \*[network-services-page]"
shows the Network Services Menu.  There are a number of ways to get to this
menu:
.Ls B
.LI
If you're running the recommended Custom installation, you'll get it
automatically after the end of the installation.
.LI
If you're running the Standard and Express installations, you don't get it at
all: after setting up your network interfaces,
.Command sysinstall
presents you with individual items from the Network Services Menu instead.
.LI
If you're setting up after rebooting, or if you missed it during installation,
select \f(CWConfigure\fP from the main menu and then \f(CWNetworking\fP.
.De
The first step should always be to set up the network interfaces, so this is
where you find yourself if you are performing a Standard or Express installation.
.H3 "Setting up network interfaces"
.X "network, interface, setting up"
.Pn net-setup
Figure
.Sref \*[network-setup-menu] \&
.\" XXX .pageref \*[network-setup-menu-page] "on page \*[network-setup-menu-page]"
shows the network setup menu.  On a standard 80x25 display it requires scrolling
to see the entire menu.  If you installed via FTP or NFS, you will already have
set up your network interfaces, and
.Command sysinstall
won't ask the questions again.  The only real network board on this list is
\fIxl0\fP, the Ethernet board.  The others are standard hardware that can also
be used as network interfaces.  Don't try to set up PPP here; there's more to
PPP configuration than
.Command sysinstall
can handle.  We'll look at PPP configuration in Chapter
.Sref "\*[nchppp]" .
.P
.PIC images/network-setup.ps 4i
.Figure-heading "Network setup menu"
.Fn network-setup-menu
In our case, we choose the Ethernet board.  The next menu asks us to set the
internet parameters.  Figure
.Sref \*[network-config-menu] \&
.\" XXX .pageref \*[network-config-menu-page] "on page \*[network-config-menu-page]"
shows the network configuration menu after filling in the values.
Specify the fully qualified local host name; when you tab to the \f(CWDomain:\fP
field, the domain is filled in automatically.  The names and addresses
correspond to the example network that we look at in Chapter
.Sref "\*[nchnetintro]" ,
on page
.Sref \*[reference-net-page] .
We have chosen to call this machine \fIpresto\fP, and the domain is
\fIexample.org\fP.  In other words, the full name of the machine is
\fIpresto.example.org\fP.  Its IP address is \f(CW223.147.37.2\fP.  In this
configuration, all access to the outside world goes via \f(CWgw.example.org\fP,
which has the IP address \f(CW223.147.37.5\fP.  The name server is located on
the same host, \fIpresto.example.org\fP.  The name server isn't running when
this information is needed, so we specify all addresses in numeric form.
.P
.ne 3v
What happens if you don't have a domain name?  If you're connecting to the
global Internet, you should go out and get one\(emsee page
.Sref \*[domainreg] .
But in the meantime, don't fake it.  Just leave the fields empty.  If you're not
connecting to the Internet, of course, it doesn't make much difference what name
you choose.
.PIC images/network-config.ps 4i
.Figure-heading "Network configuration menu"
.Fn network-config-menu
.P
As is usual for a class C network, the net mask is \f(CW255.255.255.0\fP.  You
don't need to fill in this information\(emif you leave this field without
filling it in,
.Command sysinstall
inserts it for you.  Normally, as in this case, you wouldn't need any additional
options to
.Command ifconfig .
.H3 "Other network options"
It's up to you to decide what other network options you would like to use.  None
of the following are essential, and none need to be done right now, but you may
possibly find some of the following interesting:
.Ls B
.LI
.Daemon inetd
allows connections to your system from outside.  We'll look at it in more detail
on page
.Sref \*[inetd] .
Although it's very useful, it's also a security risk if it's configured
incorrectly.  If you don't want to accept any connections from outside, you can
disable
.Daemon inetd
and significantly reduce possible security exposures.
.LI
\fINFS client\fP.  If you want to mount NFS file systems located on other
machines, select this box.  An \f(CWX\fP appears in the box, but nothing further
happens.  See Chapters
.Sref "\*[nchclient]" \&
and
.Sref "\*[nchserver]" \&
for further details of NFS.
.LI
\fINFS server\fP.  If you want to allow other systems to mount file systems
located on this machine, select this box.  You get a prompt asking you to create
the file
.File /etc/exports ,
which describes the conditions under which other systems can mount the file
systems on this machine.  You must enter the editor, but there is no need to
change anything at this point.  We'll look at
.File /etc/exports
in more detail on page
.Sref \*[/etc/exports] .
.LI
.Command ntpdate
and
.Daemon ntpd
are programs that automatically set the system time from time servers located
on the Internet.  See page
.Sref \*[ntp] \&
for more details.  If you wish, you can select the server at this point.
.LI
.Daemon rwhod
broadcasts information about the status of the systems on the network.  You can
use the
.Command ruptime
program to find the uptime of all systems running
.Daemon rwhod ,
and
.Command rwho
to find who is running on these systems.  On a normal-sized display, you need to
scroll the menu down to find this option.
.LI
You don't need to select \f(CWsshd\fP: it's already selected for you.  See page
.Sref \*[sshd] \&
for further details of
.Command ssh
and
.Daemon sshd .
.Le
You don't need to specify  any of the remaining configuration options during
..if verylong
configuration, so we'll look at them in Chapter
.Sref \*[nchnetsetup] .
..else
configuration.  See the online handbook for further details.
..endif
.\" XXX .H2 "Security profiles"
.H2 "Startup preferences"
The next step of interest is the \f(CWStartup\fP submenu, which allows you to
choose settings that take effect whenever you start the machine.  See Chapter
.Sref "\*[nchstarting]" \&
for details of the startup files.
.PIC images/startup-menu.ps 4i
.Figure-heading "Startup configuration menu"
.Fn startup-config-menu
.ne 6v
The most important ones are:
.Ls B
.ig
.LI
Select \f(CWpccard\fP if you're running FreeBSD on a laptop.  This enables the
PCMCIA card code.  You normally don't need to change the items \f(CWpccard
mem\fP and \f(CWpccard ifconfig\fP, even if you're not running a laptop.
..
.LI
.\" XXX link candidate?
Select \f(CWAPM\fP if you're running a laptop.  It enables you to power the
system down in \fIsuspend to RAM\fP\/ or \fIsuspend to disk\fP\/ mode,
preserving the currently running system, and to resume execution at a later
date.
.LI
If you have USB peripherals, select \f(CWusbd\fP to enable the
.Daemon usbd
daemon, which recognizes when USB devices are added or removed.
.\" XXX reference to details?
.LI
\f(CWnamed\fP starts a name daemon.  Use this if you're connecting to the
Internet at all, even if you don't have a DNS configuration: the default
configuration is a \fIcaching name server\fP, which makes name resolution
faster.  Just select the box; you don't need to do anything else.  We'll look at
.Daemon named
in Chapter
.Sref "\*[nchdns]" .
.LI
Select \f(CWlpd\fP, the \fIline printer daemon\fP, if you have a printer
connected to the machine.  We'll look at
.Daemon lpd
in Chapter
.Sref "\*[nchprinters]" .
.LI
Select \f(CWlinux\fP if you intend to run Linux binaries.  This is almost
certainly the case, and by default the box is already ticked for you.
.LI
Select SVR4 and SCO if you intend to run UNIX System V.4 (SVR4) or SCO
OpenDesktop or OpenServer (SCO) binaries respectively.
.Le
.SPUP
.H2 "Configuring the mouse"
FreeBSD detects PS/2 mice at boot time only, so the mouse must be plugged in
when you boot.  If not, you will not be able to use it.  To configure, select
\f(CWMouse\fP\/ from the configuration menu.  The menu in Figure
.Sref \*[mouse-menu] \&
appears.
.PIC images/mouse-menu.ps 4i
.Figure-heading "Mouse menu"
.Fn mouse-menu
.ne 2v
With a modern PS/2 mouse, you don't need to do any configuration at all.  You
just enable the \fImouse daemon\fP\/ or
.Daemon moused .
Select the menu item \f(CWEnable\fP: you have the chance to move the mouse and
note that the cursor follows.  The keys don't work in this menu: select
\f(CWYes\fP and exit the menu.  That's all you need to do.
.P
If you're running a serial mouse, choose the item \f(CWSelect mouse port\fP and
set it to correspond with the port you have; if you have an unusual protocol,
you may also need to set it with the \f(CWType menu\fP.  For even more exotic
connections, read the man page for
.Daemon moused
and set the appropriate parameters.
.H2 "Configuring X"
.Pn xf86cfg
.Pn X-config
You should have installed X along with the rest of the system\(emsee page
.Sref \*[select-distribution] .
If you haven't, install the package \fIx11/XFree86\fP.  In this section, we'll
look at what you need to do to get X up and running.
.P
X configuration has changed a lot in the course of time, and it's still
changing.  The current method of configuring X uses a program called
.Command xf86cfg ,
which is still under development, and it shows a few strangenesses.  Quite
possibly the version you get will not behave identically with the following
description.  The differences should be relatively clear, however.
.P
.PIC images/xf86cfg-main.ps 4i
.Figure-heading "xf86cfg main menu"
.Fn xf86cfg-main-menu
The configuration is stored in a file called
.File -n XF86Config ,
though the directory has changed several times in the last few years.  It used to
be in
.File /etc/X11/XF86Config
or
.File /etc/XF86Config ,
but the current preferred place is
.File /usr/X11R6/lib/X11/XF86Config .
The server looks for the configuration file in multiple places, so if you're
upgrading from an earlier version, make sure you remove any old configuration
files.
We'll look at the contents of the file in detail in Chapter
.Sref "\*[nchxtheory]" .
In this section, we'll just look at how to generate a usable configuration.
.P
From the configuration menu, select \f(CWXFree86\fP and then \f(CWxf86cfg\fP.
There is a brief delay while
.Command xf86cfg
creates an initial configuration file, then you see the main menu of Figure
.Sref \*[xf86cfg-main-menu] .
This application runs without knowing what the hardware is, so the rendering is
pretty basic.  The window on the left shows the layout of the hardware, and the
window on the right is available in case your mouse isn't working.  Select the
individual components with the mouse or the numeric keypad.  For example, to
configure the mouse, select the image at top left:
.PIC images/xf86cfg-mouse-top.ps 2i
.Figure-heading "xf86cfg mouse menu"
In all likelihood that won't be necessary.  The configuration file that
.Command xf86cfg
has already created may be sufficient, so you could just exit and save the file.
You'll probably want to change some things, though.  In the following, we'll go
through the more likely changes you may want to make.
.H4 "Configuring the keyboard"
You can select a number of options for the keyboard, including alternative key
layouts.  You probably  won't need to change anything here.
.PIC images/xf86cfg-keyboard.ps 2i
.Figure-heading "xf86cfg keyboard menu"
.H4 "Describing the monitor"
Probably the most important thing you need to change are the definitions for the
monitor and the display card.  Some modern monitors and most AGP display cards
supply the information, but older devices do not.  In this example we'll
configure a Hitachi CM813U monitor, which does not identify itself to
.Command xf86cfg .
Select the monitor image at the top right of the window, then \f(CWConfigure
Monitor(s)\fP.  You see:
.PIC images/xf86cfg-monitor.ps 2i
.Command xf86cfg
doesn't know anything about the monitor, so it assumes that it can only display
standard VGA resolutions at 640x480.  The important parameters to change are the
horizontal and vertical frequencies.  You can select one of the listed
possibilities, but unless you don't know your's monitor specifications, you
should set exactly the frequencies it can do.  In this case, the monitor
supports horizontal frequencies from 31 kHz to 115 kHz and vertical frequencies
from 50 Hz to 160 Hz, so that's what we enter.  At the same time, we change the
identifier to indicate the name of the monitor:
.PIC images/xf86cfg-monitor-configured.ps 2i
.Figure-heading "xf86cfg monitor menu"
Select OK to return to the previous menu.
.H4 "Configuring the display card"
.Command xf86cfg
recognizes most modern display cards, including probably all AGP cards, so you
probably don't need to do anything additional to configure the display card.  If
you find that the resultant configuration file doesn't know about your card,
you'll have to select the card symbol at the top of the screen.  Even if the
card has been recognized, you get this display:
.PIC images/xf86cfg-card-select.ps 2i
The only indication you have that
.Command xf86cfg
has recognized the card (here a Matrox G200) is that it has selected \f(CWmga\fP
for the driver name.  If you need to change it, scroll down the list until you
find the card:
.PIC images/xf86cfg-card-configured.ps 2i
.Figure-heading "xf86cfg card select menu"
.H4 "Selecting display resolutions"
The display resolution is defined by \fIMode Lines\fP, which we'll look at in
detail on page
.Sref \*[ModeLine] .
The names relate to the resolution they offer.  By default,
.Command xf86cfg
only gives you 640x480, so you'll certainly want to add more.  First, select the
field at the top left of the screen:
.PIC images/xf86cfg-config-select.ps 2i
.Figure-heading "xf86cfg configuration selection"
From this menu, select \f(CWConfigure ModeLine\fP.  You see:
.PIC images/xf86cfg-modeline.ps 2i
.Figure-heading "xf86cfg mode line menu"
If you pass the cursor over the image of the screen, you'll see this warning:
.PIC images/xf86cfg-modeline-warning-cropped.ps 2i
.Figure-heading "xf86cfg mode line warning"
Take it seriously.  We'll look at this issue again in Chapter
.Sref "\*[nchxtheory]" \&
on page
.Sref \*[fry-monitor] .
For an initial setup, you shouldn't use this interface.  Instead, select
\f(CWAdd standard VESA mode\fP at the top.  We get another menu:
.PIC images/xf86cfg-vesamodes.ps 2i
.Figure-heading "xf86cfg VESA mode lines"
.Pn screen-resolution
Select the resolutions you want with the highest frequency that your hardware
can handle.  In this case, you might select 1024x768 @ 85 Hz, because it's still
well within the range of the monitor.  Answer \f(CWYes\fP to the question of
whether you want to add it.  You can select as many resolutions as you want, but
the ModeLine window does not show them.
.P
You can also use the ModeLine window to tune the display, but it's easier with
another program,
.Command xvidtune .
We'll look at those details in Chapter
.Sref "\*[nchxtheory]" .
.P
Finally, select \f(CWQuit\fP at the bottom right of the display.  You get this
window:
.PIC images/xf86cfg-quit.ps 2i
.Figure-heading "xf86cfg quit"
When you answer \f(CWYes\fP, you get a similar question asking whether you want
to save the keyboard definition.  Once you've done that, you're finished.
.br
.ne 10v
.H3 "Desktop configuration"
.Pn Desktop
Next, select \f(CWDesktop\fP from the Configuration menu.  You get this menu:
.PIC images/desktop-select.ps 4i
.Figure-heading "Desktop select menu"
Which one do you install?  You have the choice.  If you know what you want, use
it.  There are many more window managers than shown here, so if you don't see
what you're looking for, check the category \fIx11-wm\fP\/ in the Ports
Collection.  The select menu gives you the most popular ones: \fIGnome\fP,
\fIAfterstep\fP, \fIEnlightenment\fP, \fIKDE\fP, \fIWindowmaker\fP\/ and
\fIfvwm2\fP.  In this book, we'll consider the \fIKDE\fP\/ desktop and the
\fIfvwm2\fP\/ window manager.  KDE is comfortable, but it requires a lot of
resources.  Gnome is similar in size to KDE.  By contrast, \fIfvwm2\fP\/ is much
faster, but it requires a fair amount of configuration.  We'll look at KDE and
.Command fvwm2
in Chapter
.Sref "\*[nchunixref]" .
.H3 "Additional X configuration"
At this point, we're nearly done.  A few things remain to be done:
.Ls B
.LI
Decide how you want to start X.  You can do it explicitly with the
.Command startx
command, or you can log in directly to X with the
.Daemon xdm
display manager.  If you choose
.Command startx ,
you don't need to do any additional configuration.
.LI
For each user who runs X, create an X configuration file.
.Le
.bp
.H4 "Configuring xdm"
.Pn xdm
.X "display manager"
To enable
.Daemon xdm ,
edit the file
.File /etc/ttys .
By default it contains the following lines:
.Dx
ttyv8   "/usr/X11R6/bin/xdm -nodaemon"  xterm   off secure
.De
Using an editor, change the text \f(CWoff\fP to \f(CWon\fP\/:
.Dx
ttyv8   "/usr/X11R6/bin/xdm -nodaemon"  xterm   \f(CBon\fP secure
.De
If you do this from a running system, send a HUP signal to
.Daemon init
to cause it to re-read the configuration file and start
.Daemon xdm \/:
.Dx
# \f(CBkill -1 1\fP
.De
This causes an
.Daemon xdm
screen to appear on
.Device ttyv8 .
You can switch to it with \fBAlt-F9\fP.
.H4 "User X configuration"
If you're starting X manually with
.Command startx ,
create a file
.File .xinitrc
in your home directory.  This file contains commands that are executed when X
starts.  Select the line that corresponds to your window manager or desktop from
the following list, and put it in
.File .xinitrc \/:
.Dx
startkde                                        \fIfor kde\fP\/
exec gnome-session                              \fIfor Gnome\fP\/
fvwm2                                           \fIfor fvwm2\fP\/
.De
If you're using
.Daemon xdm ,
you put the same content in the file
.File .xsession
in your home directory.
.H2 "Rebooting the new system"
When you get this far, you should have a functional system.  If you're still
installing from CD-ROM, you reboot by exiting
.Command sysinstall .
If you have already rebooted, you exit
.Command sysinstall
and reboot with:
.Dx
# \f(CBshutdown -r now\fP
.De
Don't just press the reset button or turn the power off.  That can cause data
loss.  We'll look at this issue in more detail on page
.Sref \*[shutdown] .
