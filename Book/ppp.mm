.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: ppp.mm,v 4.14 2003/04/02 08:14:21 grog Exp grog $
.\"
.ig
XXX What are all these rc.conf flags?
..
.Chapter \*[nchppp] "Configuring PPP"
.Pn PPP
.X "SLIP"
.X "Serial Line Internet Protocol"
.X "PPP"
.X "point to point, protocol"
.X "PPP, over Ethernet"
.X "PPPoE"
Two protocols support connection to the Internet via modem: \fISLIP\fP\/
(\fISerial Line Internet Protocol\fP\/) and \fIPPP\fP\/ (\fIPoint to Point
Protocol\fP).  As the name suggests, SLIP supports only IP.  It is an older,
less rugged protocol.  Its only advantage is that it may be available where PPP
isn't.  If you have the choice, always take PPP: it differs from SLIP in being
able to handle multiple protocols simultaneously, and it's also used on many DSL
links (\fIPPP over Ethernet\fP\/ or \fIPPPoE\fP\/).  In this chapter, we'll look
only at PPP.
.P
PPP can perform a number of functions:
.Ls B
.LI
It dials and establishes a phone connection if necessary.  Strictly speaking,
this isn't part of the PPP specification, but it is supported by most PPP
implementations.
.LI
.X "authentication, PPP"
.X "PPP, authentication"
It performs \fIauthentication\fP\/ to ensure that you are allowed to use the
connection.
.LI
.X "negotiation, PPP"
.X "PPP, negotiation"
It performs \fInegotiation\fP\/ to decide what kind of protocol to use over the
link.  You might think, ``that's OK, I'm just using IP,'' but in fact there are
a number of different ways to transmit IP datagrams over a PPP link.  In
addition, the other end may be able to handle non-Internet protocols such as
X.25, SNA and Novell's IPX.
.LI
.X "line quality monitoring, PPP"
.X "PPP, line quality monitoring"
It can perform \fIline quality monitoring\fP\/ to ensure that the modems are
able to understand each other.
.Le
.ne 5v
FreeBSD provides two versions of PPP:
.Ls B
.LI
.X "Kernel PPP"
.X "PPP, Kernel"
Traditional BSD implementations of IP are located in the kernel, which makes for
more efficiency.  The corresponding implementation of PPP is referred to as
\fIkernel PPP\fP.  We'll look at it on page
.Sref \*[kernel-ppp] .
.LI
.X "User PPP"
.X "PPP, User"
.X "iijppp"
.X "Internet Institute of Japan"
.X "tunnel driver"
.X "driver, tunnel"
Although kernel PPP is more efficient, it's also frequently more difficult to
debug.  As a result, FreeBSD also supplies an implementation known as \fIuser
PPP\fP\/ or \fIiijppp\fP, after the \fIInternet Institute of Japan\fP, which
supplied the original base code.  It uses the \fItunnel driver\fP\/ to pass IP
packets up to a user process.  It's easier to configure and debug, and though
it's not as efficient, the difference is not usually a problem.  We'll look at
this implementation on page \*[user-ppp].
.Le
.X "PPPoE"
If you have a DSL link, you don't have a choice of version: currently, only User
PPP supports PPPoE.
.H2 "Quick setup"
The following sections go into some detail about how PPP works.  It's not
completely necessary to know it all to set up PPP.  If you're in a hurry, you
can move on to the configuration summaries on page \*[user-ppp] for user PPP, or
page \*[pppd-quick-setup] for kernel PPP.
.H2 "How PPP works"
.Pn PPP-theory
The following steps are necessary to set up a PPP connection:
.Ls B
.LI
Set up a serial connection between the two systems.  This could be a direct wire
connection, but normally it's a dialup modem or an ISDN or DSL link.
.LI
.X "dialing"
.X "DCD, RS-232 signal"
.X "RS-232 signal, DCD"
.X "Data Carrier Detect, RS-232 signal"
.X "RS-232 signal, data carrier detect"
For a modem link, establish connection, traditionally called \fIdialing\fP\/
the other end.  The modems then set up a link and assert \fIDCD\fP\/ (\fIData
Carrier Detect\fP\/) to tell the machines to which they are connected that the
modem connection has been established.
.LI
Start PPP.  PPP selects a network interface to use for this connection.
.LI
The two PPP processes negotiate details like IP address, protocol, and
authentication protocols.
.LI
Establish routes to the systems at the other end of the link.
.Le
On the following pages, we'll look at these points in detail.
.H3 "The interfaces"
.X "ifconfig, command"
.X "command, ifconfig"
Most network interfaces are dedicated to networking.  For example, an Ethernet
adapter can't be used for anything else.  Serial lines are different: you could
also use them to connect a mouse or even a remote terminal.  There's another
difference, too: you access serial lines via their device names.  You access
network interfaces via the
.Command ifconfig
program, because they don't usually have device names\(emin technical jargon,
they're in a separate \fIname space\fP\/ from files.  How do we solve this
conflict?
.P
The solution may seem a little surprising: PPP uses two different devices for
each connection.  You decide which serial line you want to use, and the software
chooses a network interface for you, though you can override this choice if
you're using user PPP.  For example, your serial line might be called
.Device cuaa0 ,
.Device cuaa1
or
.Device cuaa2 ,
while your interface will be called
.Device -i tun0
or
.Device -i tun1
(for user PPP), or
.Device -i ppp0
or
.Device -i -n ppp1
.X "PPPoE"
(for kernel PPP).  It's possible to connect to a DSL line without PPP, but when
you use PPPoE, you also have two devices, the Ethernet interface and
.Device -i tun0
(Kernel PPP does not support PPPoE).
.P
The tunnel device uses a device interface called \fI/dev/tun\f(BIn\fR, where
\f(BIn\fP\/ is a digit, to read and write to the other side of the corresponding
network interface.
.P
User PPP runs in user space, so it \fIdoes\fP\/ require a device name for the
network interface, for example
.Device -i tun0 .
It uses this device to read and write to the back end of the tunnel interface.
.H3 "Dialing"
If you're running a PPP connection over a dial-up link, you'll need to establish
a telephone connection, which is still called \fIdialing\fP\/.  That's a modem
function, of course, and it's not defined in the PPP standard.
.P
User PPP includes both built-in dialing support and external dialing support,
while kernel PPP supplies only the latter.  In practice, the only difference is
the way your configuration files look.  We'll look at these when we discuss the
individual implementations.
.P
You don't need to dial for a DSL connection.
.H3 "Negotiation"
.X "negotiation, PPP"
.X "PPP, negotiation"
Once the connection is established and the PPP processes can talk to each other,
they negotiate what PPP features they will use.\*F
.FS
Years ago, you might have first have had to perform a normal UNIX login (``login
authentication'').  This was usually handled by the dialing script (``chat
script'').  Microsoft didn't support this kind of authentication, so it's
practically obsolete now, though there's nothing wrong with the idea.
.FE
The negotiation is successful if the two sides can agree on a functional subset
of the features both would like to have.
.P
For each feature of the link, PPP negotiation can perform up to two actions.
User PPP uses the following terms to describe them, viewed from the local end of
a link:
.Ls B
.LI
.X "enable, PPP negotiation"
.X "PPP, negotiation, enable"
To \fIenable\fP\/ a feature means: ``request this feature.''
.LI
.X "disable, PPP negotiation"
.X "PPP, negotiation, disable"
To \fIdisable\fP\/ a feature means: ``do not request this feature.''
.LI
.X "accept, PPP negotiation"
.X "PPP, negotiation, accept"
To \fIaccept\fP\/ a feature means: ``if the other side requests this feature,
use it.''
.LI
.X "deny, PPP negotiation"
.X "PPP, negotiation, deny"
To \fIdeny\fP\/ a feature means: ``if the other side requests this feature,
refuse it.''
.Le
Negotiation is successful if each end accepts all the features that the other
end has enabled.  In some cases, however, PPP systems have an alternative.  For
example, if you accept PAP and deny CHAP, a router may first request CHAP, and
when you deny it, it may then request PAP.  You do this by enabling both PAP and
CHAP in your PPP configuration files.
.H3 "Who throws the first stone?"
.Pn first-stone
The first step in negotiation is to decide which side starts.  One of them
starts the negotiation, and the other one responds.  If you configure your end
incorrectly, one of these things can happen:
.Ls N
.LI
You both wait for the other end to start.  Nothing happens.  After a while, one
of you times out and drops the connection.
.LI
You both fire away and place your demands, and listen for the other one to
reply.  The software should recognize that the other end is talking, too, and
recover, but often enough both ends give up and drop the connection.
.LI
One side initiates negotiations before the other, and things work normally
despite the misconfiguration.  This is the most difficult kind to recognize:
sometimes the connection will work, and sometimes it won't, apparently dependent
on the phase of the moon.
.Le
In general, systems with login authentication also initiate the negotiation.
ISPs with \fIPAP\fP\/ or \fICHAP\fP\/ authentication tend to expect the end user
to start first, because that's the way Microsoft does it.  It's easier for
debugging to assume that the other end will start.  If it doesn't, and you have
an external modem, you'll notice that there is no traffic on the line, and that
the line has dropped.  Then you can switch to active mode negotiation.
.P
It makes more sense for the called system to start the negotiation: the calling
system is ready to use the link immediately, but the called system often takes a
certain amount of time execute its PPP server program.  A common cause of
problems is when the server machine is busy and it takes a while to invoke the
PPP process.  In this case the caller sends its initial configuration data and
the called system's tty device may echo it back, resulting in a lot of confusion
at the caller's end.  User PPP can typically survive about three reflections of
this type before getting too confused to recover.
.P
Typical features that require negotiation are:
.Ls B
.LI
.X "CHAP"
.X "PAP"
\fIWhat kind of authentication?\fP\/ Login authentication doesn't count here,
because it's not part of PPP.  You may choose to offer \fICHAP\fP\/ or
\fIPAP\fP\/ negotiation.  You may also require the other end to authenticate
itself.  You can accept both \fICHAP\fP\/ and \fIPAP\fP\/ authentication\(emthat
way, you can accept whichever the other end asks for.  If the other end is an
ISP, you will probably not be able to authenticate him, but you should check
with the ISP.
.P
A common configuration problem is when a user enables some form of
authentication without first agreeing this with the ISP.  For example, very few
ISPs perform authentication from their end (to prove to you that they're really
the ISP you dialed).  You can specify this type of authentication in your
configuration file, but if the ISP refuses to authenticate, you will never
establish a connection.
.LI
.X "LQR"
.X "Link Quality Request"
\fILQR\fP, \fILink Quality Requests\fP, give you an overview of your line
quality, \fIif\fP\/ your modem doesn't use error correction.  If it does use
error correction, it will hide any LQR problems.  Occasionally LQR packets can
confuse a PPP implementation, so don't enable it if you don't intend to use it.
.LI
.X "Jacobson, Van"
.X "data compression"
.X "compression, data"
.X "header compression"
.X "compression, header"
.X "Predictor 1, compression"
.X "compression, Predictor 1"
.X "BSD compression"
.X "compression, BSD"
.X "deflate compression"
.X "compression, deflate"
\fIData and header compression\fP.  You have a choice here: modern modems offer
various kinds of data compression, and so do the PPP implementations.  As we saw
on page \*[modem-data-compression], modem compression increases the data
throughput, but also increases the latency.  If your ISP supports the same kind
of data compression as your PPP software, you might find that it improves
matters to disable modem data compression.  Both implementations support \fIVan
Jacobson\fP, \fIdeflate\fP and \fIPredictor 1\fP\/ compression, and kernel PPP
also supports \fIBSD compression\fP.
.P
Which do you choose?  \fIVan Jacobson\fP\/ compression works at the TCP level.
It compresses only the headers (see page \*[packet-header] for more details),
and the other compression schemes work at the frame level.  You can always
enable Van Jacobson compression.  As far as the others are concerned, use
whatever the other side offers.  In case of doubt, enable all available
compression types and allow PPP to negotiate the best combination.
.P
Compression negotiation is handled by the \fICompression Control Protocol\fP,
usually known as \fICCP\fP.  It uses its own protocol number so that it can be
distinguished from other protocols that the remote system might offer, such as
IP, X.25, SNA and IPX.
.LI
\fIIP addresses\fP.  In many cases, the server machine allocates a dynamic IP
address.  We'll look at the implications below.
.LI
.X "proxy ARP"
\fIProxy ARP\fP.  Some systems can't understand being at the other end of a PPP
link.  You can fool them by telling the router to respond to \fIARP\fP\/
requests for machines at the other end of the link.  You don't need this
subterfuge in FreeBSD.
.Le
.H3 "Authentication"
Nearly every PPP link requires some kind of identification to confirm that you
are authorized to use the link.  On UNIX systems, the authentication
traditionally consisted of the UNIX \fIlogin\fP\/ procedure, which also allows
you to dialup either to a shell or to a PPP session, depending on what user ID
you use.  Login authentication is normally performed by the dial-up chat script.
.P
.X "PAP"
.X "CHAP"
Microsoft has changed many things in this area.  Their platforms don't normally
support daemons, and in some cases not even multiple users, so the UNIX login
method is difficult to implement.  Instead, you connect directly to a PPP server
and perform authentication directly with it.  There are two different
authentication methods currently available, \fIPAP\fP\/ (\fIPassword
Authentication Protocol\fP\/) and \fICHAP\fP (\fIChallenge Handshake
Authentication Protocol\fP\/).  Both perform similar functions.  From the PPP
point of view, you just need to know which one you are using.  Your ISP should
tell you this information, but a surprising number don't seem to know.  In case
of doubt, accept either of them.
.P
Just to confuse matters, Microsoft has implemented authentication protocols of
its own, such as MS LanMAN, MS CHAP Version 1 (also known as CHAP type
\f(CW0x80\fP) and MS CHAP Version 2, also known as CHAP type \f(CW0x81\fP.  User
PPP supports both kinds.
.P
.ne 3v
.X "PAP"
.X "CHAP"
If you're using \fIPAP\fP\/ or \fICHAP\fP, you need to specify a system name and
an authentication key.  These terms may sound complicated, but they're really
just a fancy name for a user name and a password.  We'll look at how to specify
these values when we look at the individual software.
.P
How do you decide whether you use \fIPAP\fP\/ or \fICHAP\fP\/?  You don't need
to\(emaccept both and let the other end decide which kind to use.
.H3 "Which IP addresses on the link?"
After passing authentication, you may need to negotiate the addresses on the
link.  At first sight, you'd think that the IP addresses on the link would be
very important.  In fact, you can often almost completely ignore them.  To
understand this, we need to consider what the purpose of the IP addresses is.
.P
An IP address is an address placed in the source or the destination field in an
IP packet to enable the software to route it to its destination.  As we saw in
\*[chnetsetup], it is not necessarily the address of the interface to which the
packet is sent.  If your packet goes through 15 nodes on the way through the
Internet, quite a normal number, it will be sent to 14 nodes whose address is
not specified in the packet.
.P
The first node is the router at the other end of the PPP link.  This is a
point-to-point link, so it receives all packets that are sent down the line, so
you don't need to do anything special to ensure it gets them.  This is in marked
contrast to a router on a broadcast medium like an Ethernet: on an Ethernet you
must specify the IP address of the router for it to receive the packets.
.Aside
.X "ARP"
.X "Address Resolution Protocol"
On an Ethernet, although the IP address in the packets doesn't mention the
router, the Ethernet headers do specify the Ethernet address of the router as
the destination address.  Your local system needs the IP address to determine
the Ethernet address with the aid of \fIARP\fP, the \fIAddress Resolution
Protocol\fP.
.End-aside
In either case, except for testing, it's very unlikely that you will ever want
to address a packet directly to the router, and it's equally unlikely that the
router would know what to do with most kinds of packets if they are addressed to
itself.  So we don't really need to care about the address.
.P
What if we set up the wrong address for the other end of the link?  Look at the
router \f(CWgw.example.com\fP in the reference network on page
\*[reference-net-page].  Its PPP link has the local address
\f(CW139.130.136.133\fP, and the other end has the address
\f(CW139.130.136.129\fP.  What happens if we get the address mixed up and
specify the other end as \f(CW139.130.129.136\fP?  Consider the commands we
might enter if we were configuring the interface manually (compare with page
\*[ifconfig-example]):
.Dx
# \f(CBifconfig tun0  139.130.136.133  139.130.129.136  netmask 255.255.255.255\f(CW
# \f(CBroute add default 139.130.129.133\fP
.De
.Figure-heading "Configuring an interface and a route"
.Tn ifconfig-ex
.ne 5v
You need to specify the netmask, because otherwise
.Command ifconfig
chooses one based on the network address.  In this case, it's a class B address,
so it would choose \f(CW255.255.0.0\fP.  This tells the system that the other
end of the link is \f(CW139.130.129.136\fP, which is incorrect.  It then tells
the system to route all packets that can't be routed elsewhere to this address
(the default route).  When such a packet arrives, the system checks the routing
table, and find that \f(CW139.130.129.136\fP can be reached by sending the
packet out from interface
.Device -i tun0 .
It sends the packet down the line.
.P
At this point any memory of the address \f(CW139.130.129.136\fP (or, for that
matter, \f(CW139.130.136.129\fP) is gone.  The packet arrives at the other end,
and the router examines it.  It still contains only the original destination
address, and the router routes it accordingly.  In other words, the router never
finds out that the packet has been sent to the incorrect ``other end'' address,
and things work just fine.
.P
What happens in the other direction?  That depends on your configuration.  For
any packet to get to your system from the Internet, the routing throughout the
Internet must point to your system.  Now how many IP addresses do you have?  If
it's only a single IP address (the address of your end of the PPP link), it must
be correct.  Consider what would happen if you accidentally swapped the last two
octets of your local IP address:
.Dx
# \f(CBifconfig tun0  139.130.\f(BI133.136\fP  139.130.129.136
.De
If \f(CWgw\fP sends out a packet with this source address, it does not prevent
it from getting to its destination, because the source address does not play any
part in the routing.  But when the destination system replies, it sends it to
the address specified in the source field, so it will not get back.
.P
So how can this still work?  Remember that routers don't change the addresses in
the packets they pass.  If system \f(CWbumble\fP sends out a packet, it has the
address \f(CW223.147.37.3\fP.  It passes through the incorrectly configured
system \f(CWgw\fP unchanged, so the reply packet gets back to its source with no
problems.
.P
.X "dynamic addressing"
.X "addressing, dynamic"
In practice, of course, it doesn't make sense to use incorrect IP addresses.  If
you don't specify an address at either end of the link, PPP can negotiate one
for you.  What this does mean, though, is that you shouldn't worry too much
about what address you get.  There is one exception, however: the issue of
\fIdynamic addressing\fP.  We'll look at that below.
.H3 "The net mask for the link"
.X "routing"
.Pn pppnetmask
As we saw on page \*[netmask], with a broadcast medium you use a net mask to
specify which range of addresses can be addressed directly via the interface.
This is a different concept from \fIrouting\fP, which specifies ranges of
addresses that can be addressed indirectly via the interface.  By definition, a
point-to-point link only has one address at the other end, so the net mask must
be \f(CW255.255.255.255\fP.
.H3 "Static and dynamic addresses"
.Pn static-ip-addresses
.X "IPv6"
Traditionally, each interface has had a specific address.  With the increase in
the size of the Internet, this has caused significant problems: a few years ago,
people claimed that the Internet was running out of addresses.  As a solution,
Version 6 of the Internet Protocol (usually called \fIIPv6\fP\/) has increased
the length of an address from 32 bits to 128 bits, increasing the total number
of addresses from 4,294,967,296 to 3.4\(mu10\u\s-338\d\s0\(emenough to assign
multiple IP addresses to every atom on Earth (though there may still be a
limitation when the Internet grows across the entire universe).  FreeBSD
contains full support for IPv6, but unfortunately that's not true of most ISPs,
so at present, IPv6 is not very useful.  This book doesn't discuss it further.
.P
.X "dynamic IP addresses"
.X "IP addresses, dynamic"
ISPs don't use IPv6 because they have found another ``solution'' to the address
space issue: \fIdynamic IP addresses\fP.  With dynamic addresses, every time you
dial in, you get a free IP address from the ISP's address space.  That way, an
ISP only needs as many IP addresses as he has modems.  He might have 128 modems
and 5000 customers.  With static addresses, he would need 5000 addresses, but
with dynamic addresses he only needs 128.  Additionally, from the ISPs point of
view, routing is trivial if he assigns a block of IP addresses to each physical
piece of hardware.
.P
Dynamic addresses have two very serious disadvantages:
.Ls N
.LI
IP is a peer-to-peer protocol: there is no master and no slave.  Theoretically,
any system can initiate a connection to any other, as long as it knows its IP
address.  This means that your ISP could initiate the connection if somebody was
trying to access your system.  With dynamic addressing, it is absolutely
impossible for anybody to set up a connection: there is no way for any other
system to know in advance the IP address that you will get when the link is
established.
.P
This may seem unimportant\(emmaybe you consider the possibility of the ISP
calling you even dangerous\(embut consider the advantages.  If you're travelling
somewhere and need to check on something on your machine at home, you can just
connect to it with
.Command ssh .
If you want to let somebody collect some files from your system, there's no
problem.  In practice, however, very few ISPs are prepared to call you, though
that doesn't make it a bad idea.
.LI
.X "idle timeout"
.X "timeout, idle"
Both versions of PPP support an \fIidle timeout\fP\/ feature: if you don't use
the link for a specified period of time, it may hang up.  Depending on where you
live, this may save on phone bills and ISP connect charges.  It only disconnects
the phone link, and not the TCP sessions.  Theoretically you can reconnect when
you want to continue, and the TCP session will still be active.  To continue the
session, however, you need to have the same IP address when the link comes up
again.  Otherwise, though the session isn't dead, you can't reconnect to it.
.Le
.H3 "Setting a default route"
.Pn ppp-default-route
.X "default route"
.X "route, default"
Very frequently, the PPP link is your only connection to the Internet.  In this
case, you should set the \fIdefault route\fP\/ to go via the link.  You can do
this explicitly with the \fIroute add\fP\/ command, but both versions of PPP can
do it for you.
.P
\fIWhen\fP\/ you set your default route depends on what kind of addressing
you're using.  If you're using static addressing, you can specify it as one of
the configuration parameters.  If you're using dynamic addressing, this isn't
possible: you don't know the address at that time.  Both versions have a
solution for this, which we'll look at when we get to them.
.H3 "Autodial"
A PPP link over modem typically costs money.  You will normally pay some or even
all of the following charges:
.Ls B
.LI
Telephone call setup charges, a charge made once per call.  Unlike the other
charges, these make it advantageous to stay connected as long as possible.
.LI
Telephone call duration charges.  In some countries, you pay per time unit (for
example, per minute), or you pay a fixed sum for a variable unit of time.
.LI
ISP connect charges, also per time unit.
.LI
ISP data charges, per unit of data.
.Le
Typically, the main cost depends on the connection duration.  To limit this
cost, both PPP implementations supply methods to dial automatically and to
disconnect when the line has been idle for a predetermined length of time.
.H2 "The information you need to know"
Whichever PPP implementation you decide upon, you need the following
information:
.Ls B
.LI
Which physical device you will use for the connection.  For a modem, it's
normally a serial port like
.Device cuaa0 .
.X "PPPoE"
For PPPoE, it's an Ethernet adapter, for example
.Device -i xl0 .
.LI
If it's a modem connection, whom are you going to call?  Get the phone number
complete with any necessary area codes, in exactly the format the modem needs to
dial.  If your modem is connected to a PABX, be sure to include the access code
for an external line.
.LI
The user identification and password for connection to the ISP system.
.LI
The kind of authentication used (usually CHAP or PAP).
.Le
In addition, some ISPs may give you information about the IP addresses and
network masks, especially if you have a static address.  You should have
collected all this information in the table on page \*[ISP-checklist].
.H2 "Setting up user PPP"
.Pn user-ppp
.Pn PPPoE
This chapter contains a lot of information about PPP setup.  If you're in a
hurry, and you have a ``normal'' PPP connection, the following steps may be
enough to help you set it up.  If it doesn't work, just read on for the in-depth
explanation.
.Ls B
.LI
Edit
.File /etc/ppp/ppp.conf .
Find these lines lines:
.Dx
papchap:
 \fI(comments omitted)\fP\/
 set phone PHONE_NUM                            \fIonly for modem connections\fP\/
 set authname USERNAME
 set authkey PASSWORD
.De
.X "PPPoE"
Replace the texts \f(CWPHONE_NUM\fP, \f(CWUSERNAME\fP and \f(CWPASSWORD\fP with
the information supplied by the ISP.  If you're using PPPoE, remove the \f(CWset
phone\fP line.
.LI
Still in
.File /etc/ppp/ppp.conf ,
check that the device is correct.  The default is
.Device cuaa1 .
If you're connecting to a different serial line, change the device name
accordingly.  If you're running PPPoE, say over the Ethernet interface
.Device -i xl0 ,
change it to:
.Dx
 set device PPPoE:xl0
.De
.SPUP
.LI
Modify
.File /etc/rc.conf .
First, check the PPP settings in
.File /etc/defaults/rc.conf .
Currently they are:
.Dx
# User ppp configuration.
ppp_enable="NO"         # Start user-ppp (or NO).
ppp_mode="auto"         # Choice of "auto", "ddial", "direct" or "dedicated".
                        # For details see man page for ppp(8). Default is auto.
ppp_nat="YES"           # Use PPP's internal network address translation or NO.
ppp_profile="papchap"   # Which profile to use from /etc/ppp/ppp.conf.
ppp_user="root"         # Which user to run ppp as
.De
Don't change this file: just add the following line to
.File /etc/rc.conf \/:
.Dx
ppp_enable=YES          # Start user-ppp (or NO).
.De
.sp -1v
.LI
If you have a permanent connection (in other words, you don't ever want to
disconnect the line), you should also add the following line to
.File /etc/rc.conf \/:
.Dx
ppp_mode=ddial          # Choice of "auto", "ddial", "direct" or "dedicated".
.De
This tells PPP not to disconnect at all.
.LI
After this, PPP will start automatically on boot and will connect whenever
necessary.  If you are not planning to reboot, you can start PPP immediately
with the following command:
.Dx 1
# \f(CB/usr/sbin/ppp -quiet -auto papchap\fP
.De
.Le
.SPUP
If that works for you, you're done.  Otherwise, read on.
.H3 "Setting up user PPP: the details"
The user PPP configuration files are in the directory
.Directory /etc/ppp .
In addition to them, you probably want to modify
.File /etc/rc.conf
to start PPP and possibly to include global Internet information.  The main
configuration file is
.File /etc/ppp/ppp.conf .
It contains a number of multi-line entries headed by a label.  For example, the
\f(CWdefault\fP entry looks like:
.Dx
default:
 set log Phase Chat LCP IPCP CCP tun command
 ident user-ppp VERSION (built COMPILATIONDATE)

 # Ensure that "device" references the correct serial port
 # for your modem. (cuaa0 = COM1, cuaa1 = COM2)
 #
 set device /dev/cuaa1                  \fIdevice to use\fP\/

 set speed 115200                       \fIconnect at 115,200 bps\fP\/
 set dial "ABORT BUSY ABORT NO\e\esCARRIER TIMEOUT 5 \e
           \e"\e" AT OK-AT-OK ATE1Q0 OK \e\edATDT\e\eT TIMEOUT 40 CONNECT"
 set timeout 180                        # 3 minute idle timer (the default)
 enable dns                             # request DNS info (for resolv.conf)
.De
.ne 3v
Let's look at this entry in detail.
.Ls B
.LI
Note the format: labels begin at the beginning of the line, and other entries
must be indented by one character.
.LI
The line \f(CWdefault:\fP identifies the default entry.  This entry is always
run when PPP starts.
.LI
The \f(CWset log\fP line specifies which events to log.  This can be helpful if
you run into problems.
.LI
The \f(CWident\fP line specifies what identification the system will present to
the other end of the connection.  You don't need to change it.
.LI
The \f(CWset device\fP line specifies the device that PPP should use to
establish the connection, in this case the second serial port,
.Device cuaa1 .
.X "PPPoE"
For PPPoE connections, use the name of the Ethernet interface, prepended by the
text \f(CWPPPoE\fP.
.Dx
 set device PPPoE:xl0
.De
.SPUP
.LI
For modem connections, the \f(CWset speed\fP line sets the speed of the link
between the modem and the computer.  Some older PCs had problems at 115,200 bps,
but you shouldn't have any need to change it any more, especially since the next
lower speed for conventional PC hardware is 57,600 bps, which is too slow to use
the full bandwidth when compression is enabled.
.LI
.X "chat script"
.X "script, chat"
Also for modems only, \f(CWset dial\fP describes a \fIchat script\fP, a series
of responses and commands to be exchanged with the modem.
.LI
\f(CWenable dns\fP tells PPP to get information about name servers when setting
up the link.  If the remote site supplies this information, you don't need to
set it manually.  You should remove this line if you're running a local name
server, which I strongly recommend.  See \*[chdns], for more details.
.Le
The default entry alone does not supply enough information to create a link.  In
particular, it does not specify who to call or what user name or password to
use.  In addition to the default entry, you need an entry describing how to
connect to a specific site.  The bare minimum would be the first three
\f(CWset\fP lines of the \f(CWpapchap\fP entry in \fIppp.conf\fP:
.Dx
papchap:
 #
 # edit the next three lines and replace the items in caps with
 # the values which have been assigned by your ISP.
 #

 set phone PHONE_NUM
 set authname USERNAME
 set authkey PASSWORD

 set ifaddr 10.0.0.1/0 10.0.0.2/0 255.255.255.0 0.0.0.0
 add default HISADDR                    # Add a (sticky) default route
.De
.X "profile"
PPP calls this entry a \fIprofile\fP.  \f(CWpapchap\fP is the profile supplied
in the default installation.  You can change the name, for example to the name
of your ISP.  This is particularly useful if you connect to more than one ISP
(for example, with a laptop).  In these examples, we'll stick with
\f(CWpapchap\fP.
.P
.X "PPPoE"
As the comment states, replace the texts \f(CWPHONE_NUM\fP, \f(CWUSERNAME\fP and
\f(CWPASSWORD\fP with your specific information.  If you are using PPPoE,
replace the \f(CWset phone\fP line with a \f(CWset device\fP line as discussed
above.
.P
The last two lines may or may not be needed.  The line \f(CWset ifaddr\fP
specifies addresses to assign to each end of the link, and that they can be
overridden.  This line is seldom needed, even for static addressing: the ISP
will almost always allocate the correct address.  We'll look at this issue again
below when we discuss dynamic addresses.
.P
Finally, the last line tells
.Command ppp
to set a default route on this interface when the line comes up.
\f(CWHISADDR\fP is a keyword specifying the other end of the link.  This is the
only way to specify the route for dynamic addressing, but it works just as well
for static addressing.  If your primary connection to the Internet is via a
different interface, remove this entry.
.H3 "Negotiation"
As we saw on page \*[first-stone], you need to decide who starts negotiation.
By default, user PPP starts negotiation.  If the other end needs to start
negotiation, add the following line to your
.File /etc/ppp/ppp.conf \/:
.Dx 1
 set openmode passive
.De
.ne 10v
User PPP uses four keywords to specify how to negotiate:
.Ls B
.LI
.X "enable, PPP negotiation"
.X "PPP, negotiation, enable"
To \fIenable\fP\/ a feature means: ``request this feature.''
.LI
.X "disable, PPP negotiation"
.X "PPP, negotiation, disable"
To \fIdisable\fP\/ a feature means: ``do not request this feature.''
.LI
.X "accept, PPP negotiation"
.X "PPP, negotiation, accept"
To \fIaccept\fP\/ a feature means: ``if the other side requests this feature,
accept it.''
.LI
.X "deny, PPP negotiation"
.X "PPP, negotiation, deny"
To \fIdeny\fP\/ a feature means: ``if the other side requests this feature,
refuse it.''
.Le
We'll see examples of this in the following sections.
.H3 "Requesting LQR"
By default, user PPP disables LQR, because it has been found to cause problems
under certain circumstances, but it accepts it for modem lines.  If you want to
enable it, include the following line in your dial entry:
.Dx
 enable lqr
.De
.sp -1v
.H3 "Authentication"
.Pn BigBird
.X "PAP"
.X "CHAP"
The configuration file syntax is the same for PAP and CHAP.  Normally, your ISP
assigns you both system name and authorization key.  Assuming your system name
is \fIFREEBIE\fP, and your key is \fIX4dWg9327\fP, you would include the
following lines in your configuration entry:
.Dx
 set authname FREEBIE
 set authkey  X4dWg9327
.De
User PPP accepts requests for PAP and CHAP authentication automatically, so this
is all you need to do unless you intend to authenticate the other end, which is
not normal with ISPs.
.H4 "/etc/ppp/ppp.secret"
The PPP system name and authentication key for PAP or CHAP are important data.
Anybody who has this information can connect to your ISP and use the service at
your expense.  Of course, you should set the permissions of your
.File /etc/ppp/ppp.conf
to \f(CW-r--------\fP and the owner to \f(CWroot\fP, but it's easy and costly to
make a mistake when changing the configuration.  There is an alternative: store
the keys in the file
.File /etc/ppp/ppp.secret .
.\" XXX must use this on dial-in
Here's a sample:
.Dx
# Sysname       Secret Key      Peer's IP address
oscar           OurSecretKey    192.244.184.34/24
FREEBIE         X4dWg9327       192.244.184.33/32
gw              localPasswdForControl
.De
.ne 10v
There are a few things to note here:
.Ls B
.LI
As usual, lines starting with \f(CW#\fP are comments.
.LI
The other lines contain three values: the system name, the authentication key,
and possibly an IP address.
.LI
The last line is a password for connecting to the \fIppp\fP\/ process locally:
you can connect to the process by starting:
.Dx
# \f(CBtelnet localhost 3000\fP
.De
The local password entry matches the host name.  See the man page \fIppp(8)\fP\/
for further details.
.Le
.SPUP
.H3 "Dynamic IP configuration"
.Pn dynamic-ppp
If you have to accept dynamic IP addresses, user PPP can help.  In fact, it
provides fine control over which addresses you accept and which you do not.  To
allow negotiation of IP addresses, you specify how many bits of the IP addresses
at each end are of interest to you.  For static addresses, you can specify them
exactly:
.Dx
 set ifaddr 139.130.136.133  139.130.136.129
.De
You can normally maintain some control over the addressing, for example to
ensure that the addresses assigned don't conflict with other network
connections.  The addresses assigned to you when the link comes up are almost
invariably part of a single subnet.  You can specify that subnet and allow
negotiation of the host part of the address.  For example, you may say ``I don't
care what address I get, as long as the first three bytes are
\f(CW139.130.136\fP, and the address at the other end starts with \f(CW139\fP.''
You can do this by specifying the number of bits that interest you after the
address:
.Dx
 set ifaddr 139.130.136.133/24  139.130.136.129/8
.De
This says that you would prefer the addresses you state, but that you require
the first 24 bits of the local interface address and the first eight bits of the
remote interface address to be as stated.
.P
If you really don't care which address you get, specify the local IP address as
0:
.Dx
 set ifaddr 0 0
.De
If you do this, you can't use the \f(CW-auto\fP modes, because you need to send
a packet to the interface to trigger dialing.  Use one of the previous methods
in this situation.
.H3 "Running user PPP"
.Pn pedestrian-uppp
After setting up your PPP configuration, run it like this:
.Dx
$ \f(CBppp\fP
Working in interactive mode
Using interface: tun0
ppp ON freebie> \f(CBdial papchap\fP                    \fIthis is the name of the entry in ppp.conf\fP\/
Dial attempt 1 of 1
Phone: 1234567                                  \fIthe phone number\fP\/
dial OK!                                        \fImodem connection established\fP\/
login OK!                                       \fIauthentication complete\fP\/
ppp ON freebie> Packet mode.                    \fIPPP is running\fP\/
ppp ON freebie>
PPP ON freebie>                                 \fIand the network connection is complete\fP\/
.De
You'll notice that the prompt (\f(CWppp\fP) changes to upper case (\f(CWPPP\fP)
when the connection is up and running.  At the same time,
.Command ppp
writes some messages to the log file
.File /var/log/ppp.log \/:
.Dx
Sep  2 15:12:38 freebie ppp[23679]: Phase: Using interface: tun0
Sep  2 15:12:38 freebie ppp[23679]: Phase: PPP Started.
Sep  2 15:12:47 freebie ppp[23679]: Phase: Phone: 1234567
Sep  2 15:13:08 freebie ppp[23679]: Phase: *Connected!
Sep  2 15:13:11 freebie ppp[23679]: Phase: NewPhase: Authenticate
Sep  2 15:13:11 freebie ppp[23679]: Phase:  his = c223, mine = 0
Sep  2 15:13:11 freebie ppp[23679]: Phase:  Valsize = 16, Name = way3.Adelaide
Sep  2 15:13:11 freebie ppp[23679]: Phase: NewPhase: Network
Sep  2 15:13:11 freebie ppp[23679]: Phase: Unknown protocol 0x8207
Sep  2 15:13:11 freebie ppp[23679]: Link:  myaddr = 139.130.136.133  hisaddr = 139.1
30.136.129
Sep  2 15:13:11 freebie ppp[23679]: Link: OsLinkup: 139.130.136.129
Sep  2 15:14:11 freebie ppp[23679]: Phase: HDLC errors -> FCS: 0 ADDR: 0 COMD: 0 PRO
TO: 1
.De
You'll notice a couple of messages that look like errors.  In fact, they're not:
\f(CWUnknown protocol 0x8207\fP means that the other end requested a protocol
that
.Command ppp
doesn't know (and, in fact, is not in the RFCs.  This is a real example, and the
protocol is in fact Novell's IPX).  The other message is \f(CWHDLC errors ->
FCS: 0 ADDR: 0 COMD: 0 PROTO: 1\fP.  In fact, this relates to the same
``problem.''
.H3 "How long do we stay connected?"
.Pn connection-duration
  The following entries in
.File /etc/defaults/rc.conf
relate to user ppp:
.Dx
# User ppp configuration.
ppp_enable="NO"         # Start user-ppp (or NO).
ppp_mode="auto"         # Choice of "auto", "ddial", "direct" or "dedicated".
                        # For details see man page for ppp(8). Default is auto.
ppp_nat="YES"           # Use PPP's internal network address translation or NO.
ppp_profile="papchap"   # Which profile to use from /etc/ppp/ppp.conf.
ppp_user="root"         # Which user to run ppp as
.De
Now our PPP connection is up and running.  How do we stop it again?  There are
two possibilities:
.Ls B
.LI
To stop the connection, but to leave the
.Command ppp
process active, enter \f(CWclose\fP:
.Dx
PPP ON freebie> \f(CBclose\fP
ppp ON freebie>
.De
.sp -1v
.LI
To stop the connection and the
.Command ppp
process, enter \f(CWq\fP or \f(CWquit\fP:
.Dx
PPP ON freebie> \f(CBq\fP
#
.De
.X "idle timeout"
.X "timeout, idle"
There are a couple of problems with this method: first, a connection to an ISP
usually costs money in proportion to the time you are connected, so you don't
want to stay connected longer than necessary.  On the other hand, you don't want
the connection to drop while you're using it.  User PPP approaches these
problems with a compromise: when the line has been idle for a certain time (in
other words, when no data has gone in either direction during this time), it
disconnects.  This time is called the \fIidle timeout\fP, and by default it is
set to 180 seconds.  You can set it explicitly:
.Dx
 set timeout 300
.De
This sets the idle timeout to 300 seconds (5 minutes).
.Le
.SPUP
.H3 "Automating the process"
Finally, setting up the connection this way takes a lot of time.  You can
automate it in a number of ways:
.Ls B
.LI
If you have a permanent connection, you can tell user PPP to stay up all the
time.  Use the \f(CW-ddial\fP modifier:
.Dx
$ \f(CBppp -ddial papchap\fP
.De
Again, \f(CWpapchap\fP is the name of the PPP profile.  This version dials
immediately and keeps the connection up regardless of whether traffic is passing
or not.
.LI
If you want to be able to connect to the Net automatically whenever you have
something to say, use the \f(CW-auto\fP modifer:
.Dx
$ \f(CBppp -auto papchap\fP
.De
In this case, user PPP does not dial immediately.  As soon as you attempt to
send data to the Net, however, it dials automatically.  When the line has been
idle for the idle timeout period, it disconnects again and waits for more data
before dialing.  This only makes sense for static addresses or when you know
that no IP connections remain alive after the line disconnects.
.LI
Finally, you can just write
.Dx 1
$ \f(CBppp -background papchap\fP
.De
.ne 5v
The \f(CW-background\fP option tells user PPP to dial immediately and stay in
the background.  After the idle timeout period, the user PPP process disconnects
and exits.  If you want to connect again, you must restart the process.
.Le
.SPUP
.H3 "Actions on connect and disconnect"
If you don't have a permanent connection, there are some things that you might
like to do every time you connect, like flush your outgoing mail queue.  User
PPP provides a method for doing this: create a
.File /etc/ppp/ppp.linkup
with the same format as
.File /etc/ppp/ppp.conf .
If it exists, PPP looks for the profile you used to start PPP (\f(CWpapchap\fP
in our examples) and executes the commands in that section.  Use the exclamation
mark (\f(CW!\fP) to specify that the commands should be performed by a shell.
For example, to flush your mail queue, you might write:
.Dx
papchap:
 ! sendmail -q
.De
Similarly, you can create a file
.File /etc/ppp/ppp.linkdown
with commands to be executed when the link goes down.  You can find sample files
in the directory
.Directory /usr/share/examples/ppp .
.H3 "If things go wrong"
.X "tunnel driver"
.X "driver, tunnel"
Things don't always work ``out of the box.''  You could run into a number of
problems.  We'll look at the more common ones on page
.Sref "\*[ppp-debug]" .
.H2 "Setting up kernel PPP"
.Pn kernel-ppp
It makes more sense to run PPP in the kernel than in user space: in the kernel
it's more efficient and theoretically less prone to error.  The implementation
has fewer features than user PPP, and it's not quite as easy to debug, so it is
not used as much.
.P
The configuration files for kernel PPP are in the same directory as the
user PPP configuration files.  You can also set up your own
.File ~/.ppprc
file, though I don't recommend this: PPP is a system function and should not be
manipulated at the user level.
.P
.\" Following text thanks to Daniel J. O'Connor <darius@senet.com.au>
.X "pppd, daemon"
.X "daemon, pppd"
Kernel PPP uses a daemon called
.Daemon pppd
to monitor the line when it is active.  Kernel PPP interface names start with
\fIppp\fP\/ followed by a number.  You need one for each concurrent link.  You
don't need to specifically build a kernel for the \fIppp\fP\/ interface: FreeBSD
Release 5 loads the PPP module
.File /boot/kernel/if_ppp.ko \&
dynamically and adds interfaces as required.  This also means that you can no
longer check for ppp support with the
.Command ifconfig
command.  The interface won't be there until you need it.
.P
Kernel PPP used to provide a number of build options to enable some features,
including the compression options described below.  The options are still there,
but they're set by default, so you don't need to do anything there either.
.P
When kernel PPP starts, it reads its configuration from the file
.File /etc/ppp/options .
Here is a typical example:
.Dx
# Options file for PPPD
defaultroute                                 \fIset the default route here when the line comes up\fP\/
crtscts                                      \fIuse hardware flow control\fP\/
modem                                        \fIuse modem control lines\fP\/
deflate 12,12                                \fIuse deflate compression\fP\/
predictor1                                   \fIuse predictor 1 compression\fP\/
vj-max-slots 16                              \fIVan Jacobson compression slots\fP\/
user FREEBIE                                 \fIour name (index in password file)\fP\/
lock                                         \fIcreate a UUCP lock file\fP\/
.De
This is quite a short file, but it's full of interesting stuff:
.Ls B
.LI
The \f(CWdefaultroute\fP line tells the kernel PPP to set the default route via
this interface after it establishes a connection.
.LI
The \f(CWcrtscts\fP line tells it to use hardware flow control (necessary to
prevent loss of characters).  You could also specify \f(CWxonxoff\fP, which uses
software flow control, but hardware flow control is preferable.
.LI
.X "DCD, RS-232 signal"
.X "RS-232 signal, DCD"
The \f(CWmodem\fP line says to monitor the modem \fIDCD\fP\/ (Carrier detect)
line.  If the connection is lost without proper negotiation, the only way that
kernel PPP can know about it is because of the drop in \fIDCD\fP.
.LI
.X "deflate compression"
.X "compression, deflate"
The line \fIdeflate\fP\/ tells kernel PPP to request \fIdeflate\fP\/
compression, which can increase the effective bandwidth.
.LI
\f(CWpredictor1\fP tells PPP to use \fIPredictor 1\fP\/ compression where
possible.
.LI
\f(CWvj-max-slots\fP specifies how many slots to use for \fIVan Jacobson\fP\/
header compression.  Having more slots can speed things up.
.LI
The \f(CWuser\fP line tells kernel PPP the user ID to use.  If you don't specify
this, it takes the system's name.
.LI
.Pn UUCP-lock-file
\f(CWlock\fP tells kernel PPP to create a UUCP-style lock on the serial line.
This prevents other programs, such as
.Command getty ,
from trying to open the line while it is running PPP.
.Le
None of these options are required to run
.Daemon pppd ,
though you'll probably need a \f(CWuser\fP entry to establish connection.  It's
a good idea to set the indicated options, however.
.H3 "Authentication"
.X "CHAP"
.X "PAP"
We've seen that
.File /etc/ppp/options
contains a user name, but no password.  The passwords are stored in separate files,
.File /etc/ppp/chap-secrets
for \fICHAP\fP, or
.File /etc/ppp/pap-secrets
for \fIPAP\fP.  The format of either file is:
.Dx
\fIusername  systemname  password\fP\/
.De
.ne 5v
To match any system name, set \fIsystemname\fP\/ to \f(CW*\fP.  For example, to
authenticate the \fIFREEBIE\fP\/ we saw on page \*[BigBird], we would enter the
following in the file:
.Dx
FREEBIE * X4dWg9327
.De
In addition, you should add a \f(CWdomain\fP line to specify your domain for
authentication purposes:
.Dx
domain example.org
.De
.sp -1v
.H3 "Dialing"
.X "chat, command"
.X "command, chat"
.X "kermit, command"
.X "command, kermit"
Kernel PPP does not perform dialing, so you need to start a program that does
the dialing.  In the following example, we use
.Command chat ,
a program derived from UUCP intended exactly for this purpose.  Some people use
.Command kermit ,
which is in fact a complete communications program for a PC protocol, to perform
this function, but this requires manual intervention.
.Command chat
does the whole job for you.
.H4 "Chat scripts"
.Pn chat-script
.X "chat script"
.X "script, chat"
.X "expect string, chat"
.X "chat, expect string"
.X "send string, chat"
.X "chat, send string"
.Command chat
uses a \fIchat script\fP to define the functions to perform when establishing a
connection.  See the man page \fIchat(8)\fP\/ for further details.  The chat
script consists primarily of alternate \fIexpect strings\fP, which
.Command chat
waits to receive, followed by \fIsend\fP\/ strings, which
.Command chat
sends when it receives the \fIexpect\fP\/ string.
.P
In addition to these strings, the chat script can contain other commands.  To
confuse things, they are frequently written on a single line, though this is not
necessary:
.Command chat
does not pay any attention to line breaks.  Our chat script, which we store in
.File /etc/ppp/dial.chat ,
looks more intelligible written in the following manner:
.Dx
# Abort the chat script if the modem replies BUSY or NO CARRIER
ABORT BUSY
ABORT 'NO CARRIER'
# Wait up to 5 seconds for the reply to each of these
TIMEOUT 5
\&'' ATZ
OK ATDT1234567
# Wait 40 seconds for connection
TIMEOUT 40
CONNECT
.De
This script first tells
.Command chat
to abort dial-up on a \f(CWBUSY\fP or \f(CWNO CARRIER\fP response from the
modem.  The next line waits for nothing (\f(CW''\fP) and resets the modem with
the command \f(CWATZ\fP.  The following line waits for the modem to reply with
\f(CWOK\fP, and dials the ISP.
.P
Call setup can take a while, almost always more than five seconds for real
(analogue) modems, so we need to extend the timeout, in this case to 40 seconds.
During this time we must get the reply \f(CWCONNECT\fP from the modem.
.H3 "Who throws the first stone?"
On page \*[first-stone] we saw how to specify whether we should start
negotiating or whether we should wait for the other end to start.  By default,
kernel PPP starts negotiation.  If you want the other end to start, add the
keyword \f(CWpassive\fP in your
.File /etc/ppp/options
file.
.H3 "Dynamic IP configuration"
By default, kernel PPP performs dynamic address negotiation, so you don't need
to do anything special for dynamic IP.  If you have static addresses, add the
following line to
.File /etc/ppp/conf \/:
.Dx
139.130.136.133:139.130.136.129
.De
These are the addresses that you would use on machine \f(CWgw.example.org\fP to
set up the PPP link in the middle of Figure \*[reference-net] on page
\*[reference-net-page].  The first address is the local end of the link (the
address of the \fIppp\f(BIn\fP\fR\/ device), and the second is the address of
the remote machine (\f(CWfree-gw.example.net\fP).
.H3 "Running kernel PPP"
.X "pppd, daemon"
.X "daemon, pppd"
To run
.Daemon pppd ,
enter:
.Dx
# \f(CBpppd /dev/cuaa1 115200 connect 'chat -f /etc/ppp/dial.chat'\fP
.De
This starts kernel PPP on the serial line
.Device cuaa1
at 115,200 bps.  The option \f(CWconnect\fP tells kernel PPP that the following
argument is the name of a program to execute: it runs
.Command chat
with the options \f(CW-f /etc/ppp/dial.chat\fP, which tells
.Command chat
the name of the chat file.
.P
.X "ifconfig, command"
.X "command, ifconfig"
After you run
.Daemon pppd
with these arguments, the modem starts dialing and then negotiates a connection
with your provider, which should complete within 30 seconds.  During
negotiation, you can observe progress with the
.Command ifconfig
command:
.Dx
$ \f(CBifconfig ppp0\fP
ppp0: flags=8010<POINTOPOINT,MULTICAST> mtu 1500
        \fI\&at this point, the interface has not yet started\fP\/
$ \f(CBifconfig ppp0\fP
ppp0: flags=8810<POINTOPOINT,RUNNING,MULTICAST> mtu 1500
        \fI\&now the interface has been started\fP\/
$ \f(CBifconfig ppp0\fP
ppp0: flags=8811<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1500
        inet 139.130.136.133 --> 139.130.136.129 netmask 0xffffffff
        \fI\&now the connection has been established\fP\/
.De
.sp -1v
.ne 12v
.H3 "Automating the process"
You can automate connection setup and disconnection in a number of ways:
.Ls B
.LI
If you have a permanent connection, you can tell kernel PPP to stay up all the
time.  Add the following line to
.File /etc/ppp/options \/:
.Dx
persist
.De
If this option is set, kernel PPP dials immediately and keeps the connection up
regardless of whether traffic is passing or not.
.LI
If you want to be able to connect to the Net automatically whenever you have
something to say, use the \f(CWdemand\fP option:
.Dx
demand
.De
In this case, kernel PPP does not dial immediately.  As soon as you attempt to
send data to the net, however, it dials automatically.  When the line has been
idle for the idle timeout period, it disconnects again and waits for more data
before dialing.
.LI
Finally, you can start kernel PPP without either of these options.  In this
case, you are connected immediately.  After the idle timeout period, kernel PPP
disconnects and exits.  If you want to connect again, you must restart the
process.
.Le
.H3 "Timeout parameters"
A number of options specify when kernel PPP should dial and disconnect:
.Ls B
.LI
The \f(CWidle\fP parameter tells kernel PPP to disconnect if the line has been
idle for the specified number of seconds, and if \f(CWpersist\fP (see above) has
not been specified.  For example, to disconnect after five minutes, you could
add the following line to the
.File /etc/ppp/options
file:
.Dx
idle 300
.De
.sp -1v
.LI
The \f(CWactive-filter\fP\/ parameter allows you to specify which packets to
count when determining whether the line is idle.  See the man page for more
details.
.LI
The \f(CWholdoff\fP parameter tells kernel PPP how long to wait before
redialing when the line has been disconnected for reasons other than being
idle.  If the line is disconnected because it was idle, and you have specified
\f(CWdemand\fP, it dials as soon as the next valid packet is received.
.Le
.H3 "Configuration summary"
.Pn pppd-quick-setup
.X "CHAP"
To summarize the examples above, we'll show the kernel PPP versions of the user
PPP examples on page \*[user-ppp].  As before, we assume that the reference
network on page \*[reference-net-page] uses \fICHAP\fP\/ authentication, and we
have to initiate.  The
.File /etc/ppp/options
looks like:
.Dx
# Options file for PPPD
defaultroute                                 \fIset the default route here when the line comes up\fP\/
crtscts                                      \fIuse hardware flow control\fP\/
modem                                        \fIuse modem control lines\fP\/
domain example.org                           \fIspecify your domain name\fP\/
persist                                      \fIstay up all the time\fP\/
deflate 12,12                                \fIuse deflate compression\fP\/
predictor1                                   \fIuse predictor 1 compression\fP\/
vj-max-slots 16                              \fIVan Jacobson compression slots\fP\/
user FREEBIE                                 \fIname to present to ISP\fP\/
139.130.136.133:139.130.136.129              \fIspecify IP addresses of link\fP\/
.De
.File /etc/ppp/dial.chat
is unchanged from the example on page
\*[chat-script]:
.Dx
# Abort the chat script if the modem replies BUSY or NO CARRIER
ABORT BUSY
ABORT 'NO CARRIER'
# Wait up to 5 seconds for the reply to each of these
TIMEOUT 5
\&'' ATZ
OK ATDT1234567
# Wait 40 seconds for connection
TIMEOUT 40
CONNECT
.De
.File /etc/ppp/chap-secrets
contains:
.Dx
FREEBIE * X4dWg9327
.De
.X "PAP"
With kernel PPP, there's no need to disable \fIPAP\fP\/: that happens
automatically if it can't find an authentication for \fIFREEBIE\fP\/ in
.File /etc/pap-secrets .
.P
The change for dynamic addressing is even simpler.  Remove the line with the IP
addresses from the
.File /etc/ppp/options
file:
.Dx
# Options file for PPPD
defaultroute                                 \fIset the default route here when the line comes up\fP\/
crtscts                                      \fIuse hardware flow control\fP\/
modem                                        \fIuse modem control lines\fP\/
domain example.org                           \fIspecify your domain name\fP\/
persist                                      \fIstay up all the time\fP\/
deflate 12,12                                \fIuse deflate compression\fP\/
predictor1                                   \fIuse predictor 1 compression\fP\/
vj-max-slots 16                              \fIVan Jacobson compression slots\fP\/
user FREEBIE                                 \fIname to present to ISP\fP\/
.De
.SPUP
.H3 "Actions on connect and disconnect"
If you don't have a permanent connection, there are some things that you might
like to do every time you connect, like flush your outgoing mail queue.  We've
seen that user
PPP provides a method for doing this with the
.File -n /etc/ppp/ppp.linkup
and
.File -n /etc/ppp/ppp.linkdown
files.  Kernel PPP supplies similar functionality with
.File /etc/ppp/auth-up
and
.File /etc/ppp/auth-down .
Both of these files are shell scripts.  For example, to flush your mail queue,
you might put the following line in
.File /etc/ppp/auth-up \/:
.Dx 1
sendmail -q
.De
.H2 "Things that can go wrong"
.Pn ppp-debug
Setting up PPP used to be a pain.  Two things have made it easier than it used
to be.  Firstly, the widespread adoption of dialup Internet connections has
consolidated the procedure, so one size fits nearly everybody.  Secondly, the
software has had some of the rough edges taken off, so now it almost works out
of the box.  Still there are a number of things that can go wrong.
.H3 "Problems establishing a connection"
The first thing you need to do is to dial the connection.  If you have an
external modem, you can follow the process via the indicator LEDs.  The
following steps occur:
.Ls B
.LI
First, the \f(CWOH\fP LED (``off hook'') goes on, indicating that the modem is
dialing.  If this doesn't happen, check the cables and that you're talking to
the right device.
.LI
Next you should see a brief flicker of the \f(CWRD\fP and \f(CWTD\fP LEDs.  If
that doesn't happen, you may also have cable problems, or it could be a problem
with the chat script.
.LI
When the \f(CWCD\fP (or \f(CWDCD\fP) LED goes on, you have a connection to the
remote system.  If you don't get that, check the phone number.
.LI
If you get this far, but you still don't get a connection, check the system log
files.  It's most likely to be an authentication failure.  See page
.Sref "\*[pedestrian-uppp]" \&
for an example of the messages from user PPP.  Kernel PPP is much less verbose.
.Le
