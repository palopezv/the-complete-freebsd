.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\"
.\" $Id: preface.mm,v 4.21 2003/05/25 09:02:19 grog Exp grog $
.\"
.Chapter 0 Preface
FreeBSD is a high-performance operating system derived from the \fIBerkeley
Software Distribution\fP (\fIBSD\fP\/), the version of UNIX developed at the
University of California at Berkeley between 1975 and 1993.  FreeBSD is not a
UNIX clone.  Historically and technically, it has greater rights than UNIX
System V to be called \fIUNIX\fP.  Legally, it may not be called UNIX, since
UNIX is now a registered trade mark of The Open Group.
.P
This book is intended to help you get FreeBSD up and running on your system and
to familiarize you with it.  It can't do everything, but plenty of UNIX books
and online documentation are available, and a large proportion of them are
directly applicable to FreeBSD.  In the course of the text, I'll repeatedly
point you to other documentation.
.P
I'm not expecting you to be a guru, but I do expect you to understand the basics
of using UNIX.  If you've come from a Microsoft background, I'll try to make the
transition a little less rocky.
.H2 "The fourth edition"
This book has already had quite a history.  Depending on the way you count, this
is the fourth or fifth edition of \fIThe Complete FreeBSD\fP\/: the first
edition of the book was called \fIInstalling and Running FreeBSD\fP, and was
published in March 1996.  The next edition was called ``The Complete FreeBSD'',
first edition.  The first three editions were published by Walnut Creek CDROM,
which ceased publishing activities in 2000.  This is the first edition to be
published by O'Reilly and Associates.
.P
During this time, FreeBSD has changed continually, and it's difficult for a book
to keep up with the change.  This doesn't mean that FreeBSD has changed beyond
recognition, but people have done a great job of working away those little rough
edges that make the difference between a usable operating system and one that
is a pleasure to use.  If you come to FreeBSD from System V, you'll certainly
notice the difference.
.P
During the lifetimes of the previous editions of this book, I realised that much
of the text becomes obsolete very quickly.  For example, in the first edition I
went to a lot of trouble to tell people how to install from an ATAPI CD-ROM,
since at the time the support was a little wobbly.  Almost before the book was
released, the FreeBSD team improved the support and rolled it into the base
release.  The result?  Lots of mail messages to the \f(CWFreeBSD-questions\fP
mailing list saying, ``Where can I get \fIATAPI.FLP\fP\/?''.  Even the
frequently posted errata list didn't help much.
.P
This kind of occurrence brings home the difference in time scale between
software releases and book publication.  FreeBSD CD-ROMs are released several
times a year.  A new edition of a book every year is considered very frequent,
but it obviously can't hope to keep up with the software release cycle.  As a
result, this book contains less time-sensitive material than previous editions.
For example, the chapter on building kernels no longer contains an in-depth
discussion of the kernel build parameters.  They change too frequently, and the
descriptions, though correct at the time of printing, would just be confusing.
Instead, the chapter now explains where to find the up-to-date information.
.P
Another thing that we discovered was that the book was too big.  The second
edition contained 1,100 pages of man pages, the FreeBSD manual pages that are
also installed online on the system.  These printed pages were easier to read,
but they had two disadvantages: firstly they were slightly out of date compared
to the online version, and secondly they weighed about 1 kilogram (2.2 lbs).
The book was just plain unwieldy, and some people reported that they had
physically torn out the man pages from the book to make it more manageable.  As
a result, the third edition had only the most necessary man pages.
.P
Times have changed since then.  At the time, \fIThe Complete FreeBSD\fP\/ was
the only English-language book on FreeBSD.  Now there are several\(emsee
.Sref "\*[biblio]" ,
for more detail.  In particular, the FreeBSD online handbook is available both
in printed form and online at \fIhttp://www.freebsd.org/handbook/index.html\fP,
so I have left much of the more time-sensitive issues out of this book.  See the
online handbook instead.  Alternatively, you can print out the man pages
yourself\(emsee page \*[howto-print-manpages] for details.
.P
It's very difficult to find a good sequence for presenting that material in this
book.  In many cases, there is a chicken and egg problem: what do you need to
know first?  Depending on what you need to do, you need to get information in
different sequences.  I've spent a lot of time trying to present the material in
the best possible sequence, but inevitably you're going to find that you'll have
to jump through one of the myriad page cross references.
.H2 "Conventions used in this book"
.Pn conventions
In this book, I use \fBbold\fP for the names of keys on the keyboard.  We'll see
more about this in the next section.
.P
I use \fIitalic\fP\/ for the names of UNIX utilities, directories, file names
and \fIURI\fP\/s (\fIUniform Resource Identifier\fP, the file naming technology
of the World Wide Web), and to emphasize new terms and concepts when they are
first introduced.  I also use this font for comments in the examples.
.P
I use \f(CWconstant width\fP in examples to show the contents of files, the
output from commands, program variables, actual values of keywords, for mail
IDs, for the names of \fIInternet News\fP\/ newsgroups, and in the text to
represent commands.
.P
I use \f(CIconstant width italic\fP in examples to show variables for which
context-specific substitutions should be made.  For example, the variable
\f(CIfilename\fP would be replaced by an actual file name.
.P
I use \f(CBconstant width bold\fP in examples to show commands or text that
would be typed in literally by the user.
.P
In this book, I recommend the use of the Bourne shell or one of its descendents
.Command ( sh ,
.Command bash ,
.Command pdksh ,
.Command ksh
or
.Command zsh ).
.Command sh
is in the base system, and the rest are all in the Ports Collection, which we'll
look at in chapter
.Sref \*[nchports] .
I personally use the
.Command bash
shell.  This is a personal preference, and a recommendation, but it's not the
standard shell: the traditional BSD shell is the C shell
.Command ( csh ),
which FreeBSD has replaced with a fuller-featured descendent,
.Command tcsh .
In particular, the standard installation sets the \f(CWroot\fP\/ user up with a
.Command csh .
See page \*[chsh] for details of how to change the shell.
.P
In most examples, I'll show the shell prompt as \f(CW$\fP, but it doesn't
normally matter which shell you use.  In some cases, however, it does:
.Ls B
.LI
Sometimes you need to be super-user, the user who can do anything.  If this is
necessary, I indicate it by using the prompt \f(CW#\fP.
.LI
Sometimes the commands only work with the Bourne Shell and derivatives
.Command ( zsh ,
.Command bash ),
and they won't work with
.Command csh ,
.Command tcsh
and friends.  In these cases I'll show the
.Command csh
alternative with the standard
.Command csh
prompt \f(CW%\fP.
.Le
.Aside
In the course of the text I'll occasionally touch on a subject that is not of
absolute importance, but that may be of interest.  I'll print such notes in
smaller text, like this.
.End-aside
.H3 "Describing the keyboard"
.X "carriage return, key"
.X "key, carriage return"
.X "line feed"
One of the big differences between UNIX and other operating systems concerns the
way they treat so-called ``carriage control codes.''  When UNIX was written, the
standard interactive terminal was still the Teletype model KSR 35.  This
mechanical monstrosity printed at 10 characters per second, and the carriage
control characters really did cause physical motion of the carriage.  The two
most important characters were \fICarriage Return\fP\/, which moved the carriage
(which carried the print head) to the left margin, and \fILine Feed\fP\/, which
turned the platen to advance the paper by the height of a line.  To get to the
beginning of a new line, you needed to issue both control characters.  We don't
have platens or carriages any more, but the characters are still there, and in
many systems, including Microsoft, a line of text is terminated by a carriage
return character and a line feed character.  UNIX only uses a ``new line''
character, which corresponds to the line feed.  This difference sometimes gives
rise to confusion.  We'll look at it in more detail on page
.Sref \*[staircase] .
.P
.ne 3v
.X "any, key"
.X "key, any"
It's surprising how many confusing terms exist to describe individual keys on
the keyboard.  My favourite is the \fIany\fP\/ key (``\f(CWPress any key to
continue\fP'').  We won't be using the \fIany\fP\/ key in this book, but there
are a number of other keys whose names need understanding:
.Ls B
.LI
.X "Enter, key"
.X "key, Enter"
.X "Return, key"
.X "key, Return"
The \fIEnter\fP\/ or \fIReturn\fP\/ key.  I'll call this \fBENTER\fP.
.LI
Control characters (characters produced by holding down the \fBCtrl\fP key and
pressing a normal keyboard key at the same time).  I'll show them as, for
example, \fBCtrl-D\fR in the text, but these characters are frequently echoed on
the screen as a caret (^) followed by the character entered, so in the examples,
you may see things like \f(CW^D\fP.
.LI
The \fBAlt\fP key, which Emacs aficionados call a \fBMETA\fP key, works in the
same way as the \fBCtrl\fP key, but it generates a different set of characters.
These are sometimes abbreviated by prefixing the character with a tilde
(\fB~\fP) or the characters \fBA-\fP.  I personally like this method better, but
to avoid confusion I'll represent the character generated by holding down the
\fBAlt\fP key and pressing \fBD\fP as \fBAlt-D\fP.
.LI
.X "new line, key"
.X "key, new line"
\fBNL\fP is the \fInew line\fP\/ character.  In ASCII, it is \fBCtrl-J\fP, but
UNIX systems generate it when you press the \fBENTER\fP key.  UNIX also refers
to this character as \f(CW\en\fP, a usage which comes from the C programming
language.
.LI
\fBCR\fP is the \fIcarriage return\fP\/ character, in ASCII \fBCtrl-M\fP.  Most
systems generate it with the \fBENTER\fP key.  UNIX also refers to this
character as \f(CW\er\fP\(emagain, this comes from the C programming language.
.LI
.X "horizontal tab, key"
.X "key, horizontal tab"
\fBHT\fP is the ASCII \fIhorizontal tab\fP\/ character, \fBCtrl-I\fP.  Most
systems generate it when the \fBTAB\fP key is pressed.  UNIX and C also refer to
this character as \f(CW\et\fP.
.LE
.H2 "Acknowledgments"
This book is based on the work of many people, first and foremost the FreeBSD
documentation project.
.Pn handbook-extract
Years ago, I took significant parts from the FreeBSD handbook, in particular
\*[chunixref].  The FreeBSD handbook is supplied as online documentation with
the FreeBSD release\(emsee page \*[handbook] for more information.  It is
subject to the BSD documentation license, a variant of the BSD software license.
.P
.X "BSD License"
.X "License, BSD"
.in .1i
.ll -.1i
Redistribution and use in source (SGML DocBook) and `compiled' forms (SGML,
HTML, PDF, PostScript, RTF and so forth) with or without modification, are
permitted provided that the following conditions are met:
.Ls
.LI
Redistributions of source code (SGML DocBook) must retain the above copyright
notice, this list of conditions and the following disclaimer as the first lines
of this file unmodified.
.LI
Redistributions in compiled form (transformed to other DTDs, converted to PDF,
PostScript, RTF and other formats) must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
.Le
This documentation is provided by the FreeBSD Documentation Project ``as is''
and any express or implied warranties, including, but not limited to, the
implied warranties of merchantability and fitness for a particular purpose are
disclaimed. In no event shall the FreeBSD Documentation Project be liable for
any direct, indirect, incidental, special, exemplary, or consequential damages
(including, but not limited to, procurement of substitute goods or services;
loss of use, data, or profits; or business interruption) however caused and on
any theory of liability, whether in contract, strict liability, or tort
(including negligence or otherwise) arising in any way out of the use of this
documentation, even if advised of the possibility of such damage.
.ll
.in 0i
.H3 "Book reviewers"
.X "Hubbard, Jordan"
.X "Palmer, Gary"
.X "Bishop, Bob"
.X "Elischer, Julian"
.X "Esser, Stefan"
.X "Fieber, John"
.X "Foster, Glen"
.X "Smith, Michael"
.X "Williams, Nate"
.X "Kamp, Poul-Henning"
This book wouldn't be the same without the help of a small group of dedicated
critics who tried out what I said and pointed out that it didn't work.  In
particular, I'd like to thank Jack Velte of Walnut Creek CDROM, who had the idea
of this book in the first place, Jordan Hubbard and Gary Palmer for tearing the
structure and content apart multiple times, and also Bob Bishop, Julian
Elischer, Stefan Esser, John Fieber, Glen Foster, Poul-Henning Kamp, Michael
Smith, and Nate Williams for valuable contributions (``What, you expect new
users to know that you have to shut down the machine before powering it
off?'').\*F
.FS
See page \*[shutdown] for details on how to shut down the system.
.FE
.X "Ritter, Andreas"
.X "M�llers, Josef"
.X "Velte, Jack"
Finally, special thanks to Josef M�llers, Andreas Ritter, and Jack Velte, who
put early drafts of this book through its paces and actually installed FreeBSD
with their help.
.P
The second edition had much more review than the first.  A number of dedicated
reviewers held through for several months as I gradually cranked out usable
copy.  In particular, special thanks to
.X "Anderson, Annelise"
Annelise Anderson,
.X "Blake, Sue"
Sue Blake,
.X "Bresler, Jonathan M."
Jonathan M. Bresler,
.X "Bulley, William"
William Bulley,
.X "Cambria, Mike"
Mike Cambria,
.X "Clapper, Brian"
Brian Clapper,
.X "Coyne, Paul"
Paul Coyne,
.X "Crites, Lee"
Lee Crites,
.X "Dunham, Jerry"
Jerry Dunham,
.X "Esser, Stefan"
Stefan Esser,
.X "Gardella, Patrick"
Patrick Gardella,
.X "Giovannelli, Gianmarco"
Gianmarco Giovannelli,
.X "Kelly, David"
David Kelly,
.X "Klemm, Andreas"
Andreas Klemm,
.X "MacIntyre, Andrew"
Andrew MacIntyre,
.X "Michaels, Jonathan"
Jonathan Michaels,
.X "Micheel, J�rg"
J�rg Micheel,
.X "Molteni, Marco"
Marco Molteni,
.X "Mott, Charles"
Charles Mott,
.X "Nelson, Jay D."
Jay D. Nelson,
.X "O'Connor, Daniel J."
Daniel J. O'Connor,
.X "Perry, Andrew"
Andrew Perry,
.X "Peters, Kai"
Kai Peters,
.X "Peters, Wes"
Wes Peters,
.X "Prior, Mark"
Mark Prior,
.X "Rooij, Guido van"
Guido van Rooij,
.X "Rutherford, Andrew"
Andrew Rutherford,
.X "Vickery, Thomas"
Thomas Vickery and
.X "Wilde, Don" 
Don Wilde.
.X "Birrell, John"
.X "Endsley, Michael A."
.P
Many of the second edition reviewers came back for the third edition.  In
addition, thanks to John Birrell for his help with the Alpha architecture, and
Michael A. Endsley for ferreting out bugs, some of which had been present since
the days of \fIInstalling and Running FreeBSD\fP.
.P
The following people helped with  the fourth edition:
.X "Beebe, Nelson H. F."
Nelson H. F. Beebe \f(CW<beebe@math.utah.edu>\fP
.P
The following people helped with the fourth edition:
.X "Anderson, Annelise"
Annelise Anderson,
.X "Arnold, Jonathan"
Jonathan Arnold,
.X "Blake, Sue"
Sue Blake,
.X "Barton, Doug"
Doug Barton,\" <DougB@dougbarton.net>
.X "Clapper, Brian"
Brian Clapper,\"  <bmc@clapper.org>
.X "Dunham, Jerry"
Jerry Dunham,
.X "Geddes, Matt"
Matt Geddes,
.X "Gowdy, Jeremiah"
Jeremiah Gowdy, \" <jeremiah@freedomvoice.com>
.X "Hemmerich, Daniel B."
Daniel B. Hemmerich, \"<dan@spot.org>
.X "Heath, Justin"
Justin Heath, \" <justinh@theplanet.com>
.X "Hansteen, Peter N. M."
Peter N. M. Hansteen, \" <peter@bgnett.no>
.X "Hoadley, Paul A."
Paul A. Hoadley, \" a1026582@cs.adelaide.edu.au
.X "Irvine, Ed"
Ed Irvine,
.X "Lind, John"
John Lind,
.X "Lochmann, Johannes"
Johannes Lochmann, \" <j.lochmann@i-penta.at>
.X "Losh, Warner"
Warner Losh,
.X "Mar, Yin Cheung `Yogesh'"
Yin Cheung `Yogesh' Mar, \" <yogesh@coffeeman.ca>
.X "MacIntyre, Andrew"
Andrew MacIntyre,\"  <andymac@bullseye.apana.org.au>
.X "Michaels, Jonathan"
Jonathan Michaels,
.X "Olsen, Ove Ruben R."
Ove Ruben R. Olsen,\" <ruben@vestvind.no>
.X "Pandya, Hiten"
Hiten Pandya,
.X "Pham, Linh"
Linh Pham, \" question@closedsrc.org
.X "Phillips, Daniel"
Daniel Phillips,
Siegfried P Pietralla. \"<siegfried.pietralla@eds.com>
.X ""Roznowski, .X"
.X "Roznowski, Stephen J."
Stephen J. Roznowski,
.X "Shearer, Dan"
Dan Shearer and
.X "Stokely, Murray"
Murray Stokely.
.P
.X "Oram, Andy"
In addition, my thanks to the people at O'Reilly and Associates, particularly
Andy Oram, with whom I had discussed this project for years before he was
finally able to persuade O'Reilly that it was a good idea.  Subsequently it was
Andy who coordinated seeing this rather unusual project through O'Reilly
channels.  Emma Colby designed the cover, and David Futato provided
specifications, advice, and examples for the format.  Linley Dolby proofread the
document after I thought it was ready, and found tens of mistakes on nearly
every page, ensuring that the book is better than its predecessors.
.P
.X "Lloyd, David"
Finally, thanks to David Lloyd for the loan of an ATA CD-R drive while writing
the ATA section of \*[cdburn].
.ig
.\" XXX Not enough people.  Next edition...
.H3 "Feedback"
As I state at the beginning of the book, I welcome feedback about errors in the
text.  Over the course of the years, a number of people have sent me
corrections.  Thanks go to
.X "Garcia Cuesta, Manuel Enrique"
Manuel Enrique Garcia Cuesta, for helping make this book more legible and more
accurate.
..
.H2 "How this book was written"
This book was written and typeset entirely with tools supplied as standard with
the FreeBSD system, including the Ports Collection.
.X "emacs, command"
.X "command, emacs"
The text of this book was written with the GNU \fIEmacs\fP\/ editor, and it was
formatted on \*[formatdate] with the GNU \fIgroff\fP\/ text formatter, Version
\n(.x.\n(.y, and some heavily modified \fImm\fP\/ macros.  The process was
performed under \*[os] \*[osver].  Even the development versions of \*[os] are
stable enough to perform heavy-duty work like professional text formatting.
.P
The source files for this book are kept under \fIRCS\fP, the \fIRevision Control
System\fP (see the man page \fIrcs(1)\fP\/).  Here are the RCS Version IDs for
the chapters of this particular book.
.P
.Pn RCS-Ids
.nf
.na
.Dx
.\" Revids are defined in idids.mm
.nf
.na
.so idids.mm
.De
.if o \{\
.nr firstpage 3
\&
.bp
.\}
