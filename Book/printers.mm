.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: printers.mm,v 4.18 2003/06/30 23:43:32 grog Exp grog $
.\"
.Chapter \*[nchprinters] "Printers"
.Pn printers
.X "spooler"
In this chapter, we'll look at some aspects of using printers with FreeBSD.  As
a user, you don't access printers directly.  Instead, a series of processes,
collectively called the \fIspooler\fP, manage print data.  One process,
.Command lpr ,
writes user print data to disk, and another,
.Daemon lpd ,
copies the print data to the printers.  This method enables processes to write
print data even if the printers are busy and ensures optimum printer
availability.
.P
In this section, we'll look briefly at what you need to do to set up printers.
For more details, look in the online handbook section on printing.
.P
.Daemon lpd
is the central spooler process.  It is responsible for a number of things:
.Ls B
.LI
It controls access to attached printers and to printers attached to other hosts
on the network.
.LI
It enables users to submit files to be printed.  These submissions are known as
jobs.
.LI
It prevents multiple users from accessing a printer at the same time by
maintaining a queue for each printer.
.LI
It can print header pages, also known as banner or burst pages, so users can
easily find jobs they have printed in a stack of printouts.
.LI
It takes care of communications parameters for printers connected on serial
ports.
.LI
It can send jobs over the network to another spooler on another host.
.LI
It can run special filters to format jobs to be printed for various printer
languages or printer capabilities.
.LI
It can account for printer usage.
.Le
Through a configuration file, and by providing the special filter programs, you
can enable the spooler to do all or some subset of the above for a great variety
of printer hardware.
.P
This may sound like overkill if you are the only user on the system.  It
\fIis\fP\/ possible to access the printer directly, but it's not a good idea:
.Ls B
.LI
The spooler prints jobs in the background.  You don't have to wait for data to
be copied to the printer.
.LI
The spooler can conveniently run a job to be printed through filters to add
headers or convert special formats (such as PostScript) into a format the
printer will understand.
.LI
Most programs that provide a print feature expect to talk to the spooler on your
system.
.Le
.sp -1v
.H2 "Printer configuration"
.X "printer configuration"
.X "parallel port"
There are three commonly used ways to connect a  printer to a computer:
.Ls B
.LI
Older UNIX systems frequently used serial printers, but they are no longer in
common use.  Serial printers seldom transmit more than 1,920 characters per
second, which is too slow for modern printers.
.LI
Most printers are still connected by a \fIparallel port\fP.  Parallel ports
enable faster communication with the printer, up to about 100,000 bytes per
second.  Such speeds may still not be enough for complex PostScript or
bit-mapped images.  Most parallel ports require CPU intervention via an
interrupt for each character transmitted, and 100,000 interrupts per second can
use the entire processing power of a fast machine.
.LI
More modern printers have USB or Ethernet interfaces, which enable them to
connect to several machines at once at much higher speeds.  The load on the host
computer is also much lower.
.Le
.X "device, lpt"
.X "lpt, device"
It's pretty straightforward to connect a parallel printer.  You don't need to do
anything special to configure the line printer driver \fIlpt\fP\/: it's in the
kernel by default.  All you need to do is to plug in the cable between the
printer and the computer.  If you have more than one parallel interface, of
course, you'll have to decide which one to use.  Parallel printer devices are
called /dev/lpt\f(BIn\fR, where \fIn\fP\/ is the number, starting with
\f(CW0\fP.  USB devices have names like \fI/dev/ulpt\f(BIn\fR.  See Table
\*[FreeBSD-devices-table] on page \*[FreeBSD-devices-table-page] for further
details.
.P
Configuring an Ethernet-connected printer is more complicated.  You obviously
need an IP address, which you configure on the printer.  Most modern printers
then appear like a remote computer to the spooler.  We look at spooling to
remote computers on page
.Sref \*[remote-printer] .
.H3 "Testing the printer"
.X "testing, printer"
.X "printer, testing"
When you have connected and powered on a parallel port printer, run the built-in
test if one is supplied: typically there's a function that produces a printout
describing the printer's features.  After that, check the communication between
the computer and the printer.
.Dx
# \f(CBlptest > /dev/lpt0\fP
.De
If you have a pure PostScript printer, one which can't print anything else, you
won't get any output.  Even here, though, you should see some reaction on the
status display.
.H3 "Configuring /etc/printcap"
.X "/etc/printcap"
.Pn printcap
The next step is to configure the central configuration file,
.File /etc/printcap .
This file is not the easiest to read, but after a while you'll get used to it.
Here are some typical entries:
.Dx
lp|lj|ps|local LaserJet 6MP printer:\e
        :lp=/dev/lpt0:sd=/var/spool/output/lpd:lf=/var/log/lpd-errs:sh:mx#0:\e
        :if=/usr/local/libexec/lpfilter:

rlp|sample remote printer:\e
        :rm=freebie:sd=/var/spool/output/freebie:lf=/var/log/lpd-errs:\e
        :rp=lp:
.De
Let's look at this in detail:
.Ls B
.LI
All fields are delimited by a colon (\f(CW:\fP).
.LI
Continuation lines require a backslash character (\f(CW\e\fP).  Note
particularly that you require a colon at the end of a continued line, and
another at the beginning of the following line.
.LI
The first line of each entry specifies a number of names that you can use to
specify this printer when talking to
.Command lpr
or
.Daemon lpd .
The names are separated by vertical bar symbols \f(CW|\fP.  By tradition, the
last name is a more verbose description, and you wouldn't normally use it to
talk to programs.
.LI
The following fields describe \fIcapabilities\fP, descriptions of how to do
something.  Capabilities are described by a two-letter keyword and optionally a
parameter, which is separated by a delimiter indicating the type of parameter.
If the field takes a string parameter, the delimiter is \f(CW=\fP, and if it
takes a numeric value, the delimiter is \f(CW#\fP.  You'll find a full
description in the man page.
.LI
The first entry defines a local printer, called \f(CWlp\fP, \f(CWlj\fP,
\f(CWps\fP and \f(CWlocal LaserJet 6MP printer\fP.  Why so many names?
\f(CWlp\fP is the default, so you should have it somewhere.  \f(CWlj\fP is
frequently used to talk to printers that understand HP's LaserJet language (now
PCL), and \f(CWps\fP might be used to talk to a printer that understands
PostScript.  The final name is more of a description.
.LI
The entry \f(CWlp=/dev/lpt0\fP tells the spooler the name of the physical device
to which the printer is connected.  Remote printers don't have physical devices.
.LI
\f(CWsd\fP tells the spooler the directory in which to store jobs awaiting
printing.  This directory must exist; the spooler doesn't create it.
.LI
\f(CWlf=/var/log/lpd-errs\fP specifies the name of a file in which to log
errors.
.LI
\f(CWsh\fP is a flag telling
.Daemon lpd
to omit a header page.  If you don't have that, every job will be preceded by a
descriptor page.  In a small environment, this doesn't make sense and is just a
waste of paper.
.LI
The parameter \f(CWmx\fP tells
.Daemon lpd
the maximum size of a spool job in kilobytes.  If the job is larger than this
value,
.Daemon lpd
refuses to print it.  In our case, we don't want to limit the size.  We do this
by setting \f(CWmx\fP to 0.
.LI
.X "print filter"
.X "filter, print"
\f(CWif\fP tells
.Daemon lpd
to apply a \fIfilter\fP\/ to the job before printing.  We'll look at this below.
.LI
In the remote printer entry, \f(CWrm=freebie\fP tells
.Daemon lpd
to send the data to the machine called \f(CWfreebie\fP.  This could be a fully
qualified domain name, of course.
.LI
In the remote printer entry, \f(CWrp=lp\fP tells
.Daemon lpd
the name of the printer on the remote machine.  This doesn't have to be the same
name as the name on the local machine.
.Le
.H3 "Remote printing"
.Pn remote-printer
In a network, you don't need to have a printer on every machine; you can print
on another machine (which may be a printer) on the same network.  There are a
couple of things to consider:
.Ls B
.LI
There are two machines involved in remote printing, the client (``local'')
machine and the server (``remote'') machine.
.LI
On the client, you specify the name of the server machine with the \f(CWrm\fP
capability, and you specify the name of the printer with the \f(CWrp\fP
capability.  You don't specify any \f(CWlp\fP (device name) capability.  A
typical entry might look like this:
.Dx
lp|HP LaserJet 6MP on freebie:\e
        :rm=freebie:sd=/var/spool/output/freebie:lf=/var/log/lpd-errs:mx#0:
.De
.sp -1v
.LI
On the client machine, you must also create the spool directory,
.Directory /var/spool/output/freebie
in the example above.
.LI
On the server machine, you don't need to do anything special with the
.File /etc/printcap
file.  You need an entry for the printer specified in the client machine's
\f(CWrp\fP entry, of course.
.LI
On the server machine you must allow spooler access from the client machine.
For a BSD machine, you add the name of the machine to the file
.File /etc/hosts.lpd
on a line by itself.
.Le
.H3 "Spooler filters"
.X "spooler filter"
.X "print filter"
.X "filter, print"
.Pn staircase
Probably the least intelligible entry in the configuration file on page
.Sref \*[printcap] \&
was the \f(CWif\fP entry.  It specifies the name of an \fIinput filter\fP, a
program through which
.Daemon lpd
passes the complete print data before printing.
.P
What does it do that for?  There can be a number of reasons.  Maybe you have
data in a format that isn't fit to print.  For example, it might be PostScript,
and your printer might not understand PostScript.  Or it could be the other way
around: your printer understands \fIonly\fP\/ PostScript, and the input isn't
PostScript.
.P
There's a more likely reason to require a filter, though: most printers still
emulate the old teletypes, so they require a carriage return character
(\fBCtrl-M\fP or \f(CB^M\fP) to start at the beginning of the line, and a new
line character (\fBCtrl-J\fP or \f(CB^J\fP) to advance to the next line.  UNIX
uses only \f(CB^J\fP, so if you copy data to it, you're liable to see a
\fIstaircase effect\fP.  For example,
.Command ps
may tell you:
.Dx
$ \f(CBps\fP
  PID  TT  STAT      TIME COMMAND
 2252  p1  Ss     0:01.35 /bin/bash
 2287  p1  IW     0:04.77 e /etc/printcap
 2346  p1  R+     0:00.05 ps
.De
When you try to print it, however, you get:
.Dx
  PID  TT  STAT      TIME COMMAND
                                  2252  p1  Ss   0:01.35 /bin/bash
                                                                   2287  p1  IW   0

.De
The rest of the page is empty: you've gone off the right margin.
.P
There are a number of ways to solve this problem:
.Ls B
.LI
You may be able to configure your printer to interpret \fBCtrl-J\fP as both new
line and return, and to ignore \fBCtrl-M\fP.  Check your printer handbook.
.LI
You may be able to issue a control sequence to your printer to tell it to
interpret \fBCtrl-J\fP as both new line and return to the beginning of the line,
and to ignore \fBCtrl-M\fP.  For example, HP LaserJets and compatibles will do
this if you send them the control sequence \fBESC\f(CW&k2G\fR.
.LI
You can write an \fIinput filter\fP\/ that transforms the print job into a form
that the printer understands.  We'll look at this option below.
.Le
.ne 15v
There are a couple of options for the print filter.  One of them, taken from the
online handbook, sends out a LaserJet control sequence before every job.  Put
the following shell script in
.File /usr/local/libexec/lpfilter \/:
.Dx
#!/bin/sh
printf "\e033&k2G" && cat && printf "\ef" && exit 0
exit 2
.De
.Figure-heading "Simple print filter"
.Fn print-filter
This approach does not work well with some printers, such as my HP LaserJet 6MP,
which can print both PostScript and LaserJet (natural) formats at random.  They
do this by recognizing the text at the beginning of the job.  This particular
filter confuses them by sending a LaserJet command code, so the printer prints
the PostScript as if it were plain text.
.P
In this kind of situation, the standard filters are no longer sufficient.  You
can solve the problem with the port \fIapsfilter\fP, which is in the Ports
Collection.
.\" XXX or at .Sref \*[apsfilter] .
.H2 "Starting the spooler"
As we saw above, the line printer daemon
.Daemon lpd
is responsible for printing spooled jobs.  By default it isn't started at boot
time.  If you're \f(CWroot\fP, you can start it by name:
.Dx
# \f(CBlpd\fP
.De
Normally, however, you will want it to be started automatically when the system
starts up.  You do this by setting the variable \f(CWlpd_enable\fP in
.File /etc/rc.conf \/:
.Dx
lpd_enable="YES"                        # Run the line printer daemon
.De
See page \*[rc.conf] for more details of
.File /etc/rc.conf .
.P
You can also add another line referring to the line printer daemon to
.File /etc/rc.conf \/:
.Dx
lpd_flags=""                            # Flags to lpd (if enabled).
.De
You don't normally need this line.  See the man page for
.Daemon lpd
for details of the flags.
.H2 "Testing the spooler"
.X "testing, spooler"
.X "spooler, testing"
To test the spooler, you can run the
.Command lptest
program again.  This time, however, instead of sending it directly to the
printer, you send it to the spooler:
.Dx
$ \f(CBlptest 80 5 | lpr\fP
.De
The results should look like:
.Dx
!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\e]^_`abcdefghijklmnop
"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\e]^_`abcdefghijklmnopq
#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\e]^_`abcdefghijklmnopqr
$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\e]^_`abcdefghijklmnopqrs
%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\e]^_`abcdefghijklmnopqrst
.De
.SPUP
.ne 3i
.H2 "Troubleshooting"
.X "troubleshooting, spooler"
.X "spooler, troubleshooting"
Here's a list of the most common problems and how to solve them.
.br
.Table-heading "Common printer problems"
.TS H
tab(#) ;
lw27 | lw42 .
\fBProblem#\fBCause
_
.TH N
T{
.nh
The printer prints, but the last page doesn't appear.  The status shows that the
printer still has data in the buffer.  After several minutes, the last page may
appear.
.hy
T}#T{
Your output data is not ejecting the last page.  The printer is configured to
either wait for an explicit eject request (the ASCII \fIForm feed\fP\/
character, \fBCtrl-L\fP) or to eject after a certain period of time.
.hy
.P
You have a choice as to what you do about this.  Usually you can configure the
printer, or you could get the print filter to print a form feed character at the
end of the job.  Figure \*[print-filter] already does this\(emthat's the
\f(CWprintf "\ef".
T}
.sp
.X "staircase effect"
T{
The lines wander off to the right edge of the paper and are never seen again.
T}#T{
This is the \fIstaircase effect\fP.  Refer to page
.Sref \*[print-filter-page] \&
for a couple of solutions.
T}
.sp
T{
Individual characters or whole sections of text are missing.
T}#T{
This problem occurs almost only on serial printers.  It's a result of incorrect
handshaking\(emsee page \*[handshaking] and the online handbook for more
details.
T}
.sp
T{
The output contained completely unintelligible random characters.
T}#T{
On a serial printer, if the characters appear slowly, and there's a predominance
of the characters \f(CW{|}~\fP, this probably means that you have set up the
communication parameters incorrectly.  Check the online handbook for a solution.
Make sure you don't confuse this problem with the following one.
T}
.sp
T{
The text was legible, but it bore no relationship to what you wanted to print.
T}#T{
One possibility is that you are sending PostScript output to your printer.  See
the discussion on page
.Sref \*[PostScript] \&
to check if it is PostScript.  If it is, your printer is not interpreting it
correctly, either because it doesn't understand PostScript, or because it has
been confused (see the discussion on page \*[print-filter-page] for one reason).
T}
.sp
T{
The display on the printer shows that data are arriving, but the printer doesn't
print anything.
T}#T{
You might be sending normal text to a PostScript printer that doesn't
understand normal text.  In this case, too, you will need a filter to convert
the text to PostScript\(emthe opposite of the previous problem.
.P
Alternatively, your printer port may not be interrupting correctly.  This will
not stop the printer from printing, but it can take up to 20 minutes to print a
page.  You can fix this by issuing the following command, which puts the printer
.Device lpt0
into polled mode:
.Dx
# \f(CBlptcontrol -p\fP
.De
T}
.\" XXX find a better way to do this.
T{
You get the message \f(CWlpr: cannot create freebie/.seq\fP
T}#T{
You have forgotten to create the spool directory
.Directory /var/spool/output/freebie .
T}
.TE
.hy
.H2 "Using the spooler"
.X "Using the spooler"
Using the spooler is relatively simple.  Instead of outputting data directly to
the printer, you \fIpipe\fP\/ it to the spooler
.Command lpr
command.  For example, here is the same print command, first printing directly
to the printer, and secondly via the spooler:
.Dx
# \f(CBps waux > /dev/lpt0\fP
$ \f(CBps waux | lpr\fP
.De
.X "job, spooler"
.X "spooler, job"
Note the difference in prompt: you have to be \f(CWroot\fP to write directly to
the printer, but normally anybody can write to the spooler.  The spooler creates
a \fIjob\fP\/ from this data.  You can look at the current print queue with the
.Command lpq
program:
.Dx
$ \f(CBlpq\fP
waiting for lp to become ready (offline ?)
Rank   Owner      Job  Files                                 Total Size
1st    grog       313  (standard input)                      9151 bytes
2nd    grog       30   (standard input)                      3319 bytes
3rd    yvonne     31   (standard input)                      3395 bytes
4th    root       0    (standard input)                      2611 bytes
.De
.ne 5v
The first line is a warning that
.Daemon lpd
can't currently print.  Take it seriously.  In this example, the printer was
deliberately turned off so that the queue did not change from one example to the
next.
.P
Normally, the job numbers increase sequentially: this particular example came
from three different machines.  You can get more detail with the \f(CW-l\fP
option:
.Dx
$ \f(CBlpq -l\fP
waiting for lp to become ready (offline ?)

grog: 1st                                [job 313freebie.example.org]
        (standard input)                 9151 bytes

grog: 2nd                                [job 030presto.example.org]
        (standard input)                 3319 bytes

yvonne: 3rd                              [job 031presto.example.org]
        (standard input)                 3395 bytes

root: 4th                                [job 000bumble.example.org]
        (standard input)                 2611 bytes
.De
.H3 "Removing print jobs"
.X "removing print jobs"
Sometimes you may want to delete spool output without printing it.  You don't
need to do this because of a printer configuration error: just turn the printer
off, fix the configuration error, and turn the printer on again.  The job should
then be printed correctly.  But if you discover that the print job itself
contains garbage, you can remove it with the
.Command lprm
program.  First, though, you need to know the job number.  Assuming the list we
have above, we might want to remove job 30:
.Dx
# \f(CBlprm 30\fP
dfA030presto.example.org dequeued
cfA030presto.example.org dequeued
# \f(CBlpq\fP
waiting for lp to become ready (offline ?)
Rank   Owner      Job  Files                                 Total Size
1st    grog       313  (standard input)                      9151 bytes
2nd    yvonne     31   (standard input)                      3395 bytes
3rd    root       0    (standard input)                      2611 bytes
.De
If the printer is offline, it may take some time for the
.Command lprm
to complete.
.H2 "PostScript"
.Pn PostScript
.X "PostScript"
.X "Page Description Language"
We've encountered the term \fIPostScript\fP\/ several times already.  It's a
\fIPage Description Language\fP.  With it, you can transmit detailed documents
such as this book electronically and print them out in exactly the same form
elsewhere.\*F
.FS
This is in fact the way this book was sent to the printers.
.FE
PostScript is a very popular format on the World Wide Web, and web browsers like
Netscape usually print in PostScript format.
.P
.X "escape sequences, printer"
Most other document formats describe special print features with \fIescape
sequences\fP, special commands that start with a special character.  For
example, the HP LaserJet and PCL formats use the ASCII \fBESC\fP character
(\f(CW0x1b\fP) to indicate the beginning of an escape sequence.  PostScript uses
the opposite approach: unless defined otherwise, the contents of a PostScript
file are commands, and the printable data is enclosed in parentheses.
PostScript documents start with something like:
.Dx
%!PS-Adobe-3.0
%%Creator: groff version 1.10
%%CreationDate: Fri Oct 31 18:36:45 1997
%%DocumentNeededResources: font Symbol
%%+ font Courier
%%+ font Times-Roman
%%DocumentSuppliedResources: file images/vipw.ps
%%Pages: 32
%%PageOrder: Ascend
%%Orientation: Portrait
%%EndComments
%%BeginProlog
.De
.X "prologue, PostScript"
.X "PostScript, prologue"
This is the \fIprologue\fP\/ (the beginning) of the PostScript output for this
chapter.  The \fIprologue\fP\/ of such a program can be several hundred
kilobytes long if it includes embedded fonts or images.  A more typical size is
about 500 lines.
.P
You can do a number of things with PostScript:
.Ls B
.LI
You can look at it with
.Command gv ,
which is in the Ports Collection.  We'll look at this option below.
.LI
Many printers understand PostScript and print it directly.  If yours does, you
probably know about it, since it's an expensive option.  In case of doubt, check
your printer manual.
.LI
If your printer doesn't understand PostScript, you can print with the aid of
ghostscript.  The \fIapsfilter\fP\/ port
.\" XXX (page .Sref \*[apsfilter] )
does this for you.
.Le
.H3 "Viewing with gv"
.X "viewing with gv"
.X "gv, viewing with"
.X "selFile, gv window"
.Command gv
is part of the instant workstation port that we discussed on page
.Sref \*[instant-workstation] .
To view a file with
.Command gv ,
simply start it:
.Dx
$ \f(CBgv \f[CBI]filename\fP\/ &\fP
.De
If you don't specify a file name, you get a blank display.  You can then open a
file window by pressing \f(CWo\fP, after which you can select files and display
them.  Figure \&\*[gv] shows the display of a draft version of this page with an
overlaid open window at the top right.  The \fIOpen File\fP\/ window contains a
field at the top into which you can type the name of a file.  Alternatively, the
columns below, with scroll bars, allow you to browse the current directory and
the parent directories.
.P
The window below shows the text of the previous page (roughly) on the right hand
side.  Instead of scroll bars, there is a scroll area below the text \f(CWSave
Marked\fP.  You can scroll the image in all directions by selecting the box with
the left mouse button and moving around.  At top left are menu buttons that you
can select with the left mouse button.  Note also the button \f(CW1.414\fP at
the top of the window: this is the magnification of the image.  You can change
it by selecting this button: a menu appears and gives you a range of
magnifications to choose from.
.P
The column to the right of these buttons is a list of page numbers.  You can
select a page number with the middle mouse button.  You can also get an
enlargement display of the text area around the mouse cursor by pressing the
left button.
.PIC "images/gv.ps" 4i
.Figure-heading "gv display"
.Fn gv
.H3 "Printing with ghostscript"
.Pn ghostscript
.X "printing with ghostscript"
.X "ghostscript, printing with"
If your printer doesn't support PostScript, you can still print some semblance
of the intended text with the help of
.Command ghostscript .
The results are very acceptable with modern laser and inkjet printers, less so
with older dot matrix printers.
.P
.X "driver, ghostscript"
.X "ghostscript, driver"
To print on your particular printer, you first need to find a \fIdriver\fP\/ for
it in
.Command ghostscript .
In this context, the term \fIdriver\fP\/ means some code inside
\fIghostscript\fP\/ that converts the data into something that the printer can
print.
.P
We've already seen how to use
.File /etc/printcap .
In this case, we'll need an \fIinput filter\fP, a script or program that
transforms the PostScript data into a form that the printer understands.  The
entry in
.File /etc/printcap
is pretty much the same for all printers:
.Dx
ps|HP OfficeJet 725 with PostScript:\e
      :lp=/dev/lpt0:sd=/var/spool/output/colour:lf=/var/log/lpd-errs:sh:mx#0:\e
      :if=/usr/local/libexec/psfilter:
.De
This entry defines a printer called \fIps\fP.  The comment states that it's an
HP OfficeJet, but that's only a comment.  Obviously you should choose a comment
that matches the printer you really have.
.P
The printer is connected to
.Device lpt0 ,
the first parallel printer.  Spool data is collected in the directory
.Directory /var/spool/output/colour .
You must create this directory, or printing will fail, and depending on what you
use to print, you may not even see any error messages.  They also don't appear
on the log file, which in this case is
.File /var/log/lpd-errs .
.P
The important entry is in the last line, which refers to the input filter
.File /usr/local/libexec/psfilter .
This file contains the instructions to convert the PostScript into something
that the printer can understand.  For example, for the HP OfficeJet we're
talking about here, it contains:
.Dx
#!/bin/sh
/usr/local/bin/gs -sDEVICE=pcl3 -q -sPaperSize=a4 -dNOPAUSE -sOutputFile=- -
.De
These options state:
.Ls B
.LI
Use \fIghostscript\fP\/ device \fIpcl3\fP.  This is the driver to choose for
most Hewlett Packard inkjet printers.  We'll see alternatives for other printers
below.
.LI
The output file is \fIstdin\fP\/ (see page
.Sref \*[stdout] ).
By convention, a number of programs use the character \f(CW-\fP to represent the
\fIstdout\fP\/ stream.
.LI
\f(CW-q\fP means \fIquiet\fP.  Normally
.Command ghostscript
outputs a message on startup, and it often outputs other informative messages as
well.  In this case, we're using it as a filter, so we don't want any output
except what we print.
.LI
Don't pause between pages.  If you don't specify this parameter,
.Command ghostscript
waits for a key press at the end of each page.
.LI
The paper size is the international A4 format.  By default,
.Command ghostscript
produces output for American standard 8.5 \(mu 10 inch ``letter'' paper.
.LI
The character \f(CW-\fP by itself tells
.Command ghostscript
that the input is from \fIstdin\fP.  Together with the output to \fIstdout\fP,
this makes
.Command ghostscript
function as a filter.
.Le
.H3 "Which driver?"
The previous example used the driver for the HP DeskJet.  Well, to be more
precise, it used one of a plethora of drivers available.  You can find more
information in the HTML driver documentation at
.URI /usr/local/share/ghostscript/7.05/doc/Devices.htm .
The \fI7.05\fP\/ in the name refers to the release of
.Command ghostscript ,
which will change.
.P
The documentation isn't the easiest to read.  It's probably older than your
printer, so there's a good chance that it won't mention your specific printer
model.  You may need to experiment a little before you get things working the
way you want.
.H4 "Printer drivers for DeskJets"
There are at least six sets of drivers for HP DeskJets.  They're all described
in
.File Devices.htm ,
but the following summary may help:
.Ls B
.LI
Hewlett Packard supply their own drivers.  In addition to
.Command ghostscript ,
they require server software that you can install from the Ports collection
.File ( /usr/ports/print/hpijs ).
.LI
Next come three different independently written drivers for specific models of
DeskJet, probably all now obsolete.  If you recognize your printer or something
similar in one of them, that's a good first choice.
.LI
Next comes the generic \fIpcl3\fP\/ driver that was used in the example above.
It's not mentioned in the documentation.
.LI
Finally, \fIuniprint\fP\/ is a completely different driver framework for a
number of different makes of printer.  It requires a slightly different command
line, and we'll look at it separately below.
.Le
If you're using a DeskJet, you have the choice.  Unfortunately, there's no way
to know which is best until you've tried them all.  Similar considerations apply
to other makes of printer.
.H4 "uniprint drivers"
The \fIuniprint\fP\/ drivers have a somewhat different kind of interface.
They're described towards the end of the same
.File Devices.htm
file.  To use them, change the driver specification as in the following example,
that refers to an Epson
.Dx
#!/bin/sh
/usr/local/bin/gs @stc500ph.upp -q -sPaperSize=a4 -dNOPAUSE -sOutputFile=- - -c quit
.De
The differences here are:
.Ls B
.LI
The name of the driver (\fIstc500ph.upp\fP\/) is specified differently.
.LI
The line ends with a command to the driver itself (\f(CW-c quit\fP).  The exact
meaning is not documented, though it's easy to guess.
.SPUP
.Le
.H4 "Which drivers?"
Another problem you might encounter is that it's possible to specify the drivers
you want in your
.Command ghostscript
executable when you build the port.  It's quite possible that the drivers
described in
.File -n Devices.htm
don't exist on your system.  To find out, run
.Command ghostscript
interactively with the \f(CW-h\fP (help) option:
.Dx
$ \f(CBgs -h\fP
GNU Ghostscript 7.05 (2002-04-22)
Copyright (C) 2002 artofcode LLC, Benicia, CA.  All rights reserved.
Usage: gs [switches] [file1.ps file2.ps ...]
Most frequently used switches: (you can use # in place of =)
 -dNOPAUSE           no pause after page   | -q       `quiet', fewer messages
 -g<width>x<height>  page size in pixels   | -r<res>  pixels/inch resolution
 -sDEVICE=<devname>  select device         | -dBATCH  exit after last file
 -sOutputFile=<file> select output file: - for stdout, |command for pipe,
                                         embed %d or %ld for page #
Input formats: PostScript PostScriptLevel1 PostScriptLevel2 PDF
Available devices:
   x11 x11alpha x11cmyk x11gray2 x11gray4 x11mono x11rg16x x11rg32x md2k
   md5k md50Mono md50Eco md1xMono bj10e bj10v bj10vh bj200 bjc600 bjc800
   lips2p lips3 lips4 bjc880j lips4v uniprint dmprt epag escpage lp2000
   alc8600 alc8500 alc2000 alc4000 lp8800c lp8300c lp8500c lp3000c lp8200c
   lp8000c epl5900 epl5800 epl2050 epl2050p epl2120 lp7500 lp2400 lp2200
   lp9400 lp8900 lp8700 lp8100 lp7700 lp8600f lp8400f lp8300f lp1900 lp9600s
   lp9300 lp9600 lp8600 lp1800 mjc180 mjc360 mjc720 mj500c deskjet djet500
   cdeskjet cdjcolor cdjmono cdj550 cdj670 cdj850 cdj880 cdj890 cdj1600
   cdj970 laserjet ljetplus ljet2p ljet3 ljet3d ljet4 ljet4d cljet5 cljet5c
   cljet5pr lj5mono lj5gray pj pjxl pjxl300 pxlmono pxlcolor pcl3 hpdj ijs
   npdl rpdl gdi bmpmono bmpgray bmp16 bmp256 bmp16m bmp32b bmpsep1 bmpsep8
   faxg3 faxg32d faxg4 jpeg jpeggray pcxmono pcxgray pcx16 pcx256 pcx24b
   pcxcmyk pdfwrite bit bitrgb bitcmyk pbm pbmraw pgm pgmraw pgnm pgnmraw
   pnm pnmraw ppm ppmraw pkm pkmraw pksm pksmraw pngmono pnggray png16
   png256 png16m psmono psgray psrgb pswrite epswrite tiffcrle tiffg3
   tiffg32d tiffg4 tiff12nc tiff24nc tifflzw tiffpack nullpage
Search path:
   . : /opt/lib/ghostscript : /opt/lib/ghostscript/fonts :
   /opt/lib/ghostscript/garamond : /usr/local/share/ghostscript/7.05/lib :
   /usr/local/share/ghostscript/fonts
For more information, see /usr/local/share/ghostscript/7.05/doc/Use.htm.
Report bugs to bug-gs@ghostscript.com, using the form in Bug-form.htm.
.De
.SPUP
.H2 "PDF"
\fIPDF\fP, or \fIPortable Document Format\fP, is a newer format for transferring
print documents.  Like PostScript, it comes from Adobe, and it is becoming
increasingly important as a document interchange format on the Internet.
.P
There are two ways to handle PDF:
.Ls B
.Pn acroread
.LI
Use \fIAcrobat Reader\fP, available in the Ports Collection as
\fI/usr/src/print/acroread5\fP.  The \fI5\fP\/ refers to the version of Acrobat
Reader and may change.  Acrobat Reader is proprietary, but it's available for
free, unfortunately only in binary form.  It is quite a convenient way to view
PDF documents, and it can print them in PostScript formats.  This means that you
can also use it to convert PDF to PostScript.
.LI
\fIghostscript\fP\/ also understands PDF, and it is capable of converting
between PostScript and PDF in both directions.
.Command ghostscript
provides two scripts,
.Command pdf2ps
and
.Command ps2pdf ,
which act as a front end to
.Command ghostscript
to make the job easier.
.Le
Unlike PostScript, an editor is available for PDF (\fIAcrobat\fP, the big
brother of Acrobat Reader).  Unfortunately, it's proprietary and not free, and
worse still, it's not available for FreeBSD.
