.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: quickinstall.mm,v 4.11 2003/04/09 19:26:40 grog Exp grog $
.\"
.Chapter \*[nchquickinstall] "Quick installation"
.Pn quick-install
In Chapters \*[nchshareinstall] to \*[nchpostinstall] we'll go into a lot of
detail about how to install the system.  Maybe this is too much detail for you.
If you're an experienced UNIX user, you should be able to get by with
significantly less reading.  This chapter presents checklists for some of the
more usual kinds of installation.  Each refers you to the corresponding detailed
descriptions in Chapters \*[nchshareinstall] through \*[nchpostinstall].
.P
On the following pages we'll look at the simplest installation, where FreeBSD is
the only system on the disk.  Starting on page \*[shared-disk] we'll look at
sharing the disk with Microsoft, and on page \*[X-install-checklist] we'll look
at how to install XFree86.  You may find it convenient to photocopy these pages
and to mark them up as you go along.
.H2 "Making things easy for yourself"
It is probably easier to install FreeBSD than any other PC operating system,
including Microsoft products.  Well, most of the time, anyway.  Some people
spend days trying to install FreeBSD, and finally give up.  That happens with
Microsoft's products as well, but unfortunately it happens more often with
FreeBSD.
.P
Now you're probably saying, ``That doesn't make sense.  First you say it's
easier to install, then you say it's more likely to fail.  What's the real
story?''
.P
As you might expect, the real story is quite involved.  In \*[chconcepts], I
went into some of the background.  Before you start, let's look at what you can
do to make the installation as easy as possible:
.Ls B
.LI
Use known, established hardware.  New hardware products frequently have
undocumented problems.  You can be sure that they work under Microsoft, because
the manufacturer has tested them in that environment.  In all probability, he
hasn't tested them under any flavour of UNIX, let alone FreeBSD.  Usually the
problems aren't serious, and the FreeBSD team solves them pretty quickly, but if
you get the hardware before the software is ready, you're the guinea pig.
.P
At the other end of the scale, you can have more trouble with old hardware as
well.  It's not as easy to configure, and old hardware is not as well supported
as more recent hardware.
.LI
Perform a standard installation.  The easiest way to install FreeBSD is by
booting from a CD-ROM and installing on an empty hard disk from the CD-ROM.  If
you proceed as discussed in \*[chinstall], you shouldn't have any difficulty.
.LI
If you need to share your hard disk with another operating system, it's easier
to install both systems from scratch.  If you do already have a Microsoft system
on the disk, you can use
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
.Command -n FIPS
(see page
.Sref \*[FIPS] )
to make space for it, but this requires more care.
.LI
.X "RTFM"
If you run into trouble, \fIRTFM\fP.\*F
.FS
Hackerspeak for ``Read The Manual''\(emthe \fBF\fP is usually silent.
.FE
I've gone to a lot of trouble to anticipate the problems you might encounter,
and there's a good chance that you will find something here to help.
.LI
If you do all this, and it still doesn't work, see page \*[support] for ways of
getting external help.
.Le
.H2 "FreeBSD on a disk with free space"
.Pn exclusive-install
This procedure applies if you can install FreeBSD without first having to make
space on disk.  Perform the following steps:
.LB 2m 0m 0 0 \(sq
.LI
Boot from CD-ROM.  Most systems support booting from CD-ROM, but if yours
doesn't:
.Ls B
.LI
Create two boot floppies by copying the images
.File -n /cdrom/floppies/kern.flp
and
.File -n /cdrom/floppies/mfsroot.flp
to 3\(12\f(CW"\fP diskettes.  Refer to page \*[create-floppies] for more
details.
.LI
Insert the CD-ROM in the drive before booting.
.LI
Boot from the
.File kern.flp
floppy.  After loading, insert the
.File mfsroot.flp
floppy when the system prompts you to do so, then press \fBEnter\fP.
.Aside
If you have a larger floppy, such as 2.88 MB or LS-120, you can copy the image
.File /cdrom/floppies/boot.flp
to it and boot from it.  In this case you don't need to change disks.
.End-aside
.Le
.sp -1v
.LI
Select the \f(CWCustom\fP installation.  Refer to page \*[sysinstall-main].
.LI
What you do in the partition editor depends on whether you want to share the
drive with another operating system or not:
.Ls B
.LI
If you want to use the drive only for FreeBSD, delete any existing slices, and
allocate a single FreeBSD slice that takes up the entire disk.  On exiting from
the partition editor, select the \fIStandard\fP\/ MBR.  Refer to page
\*[select-mbr].
.LI
If you want to share the disk with other systems, delete any unwanted slices and
use them for FreeBSD.  On exiting from the partition editor, select the
\fIBootMgr\fP\/ MBR.  Refer to page \*[select-mbr].
.Le
.LI
In the disk label editor, delete any existing UNIX partitions.  Create the file
systems manually.  If you don't have any favourite layout, create a root file
system with 4 GB, a swap partition with at least 512 MB (make sure it's at least
1 MB larger than the maximum memory you intend to install in your system).
Allocate a
.File /home
file system as large as you like, as long as it can fit on a single tape when
backed up.  If you have any additional space, leave it empty unless you know
what to use it for.  See page \*[partition-size] for the rationale of this
approach, which is not what
.Command sysinstall
recommends.
.LI
Install the complete system, including X and the Ports Collection.  This
requires about 1 GB of disk space.  Refer to page \*[select-distribution] if you
want to limit it.
.LI
Select CD-ROM as installation medium.  Refer to page \*[select-medium].
.LI
Give final confirmation.  The system will be installed.  Refer to page
\*[commit].
.LI
After installation, set up at least a user ID for yourself.  Refer to page
\*[logging-in].
.Le
.ds Section*title\" No title here
.H2 "FreeBSD shared with Microsoft"
.Pn shared-disk
If you have a disk with Microsoft installed on only part of the disk, and you
don't want to change the partition layout, you can proceed as in the
instructions above.  This is pretty unusual, though: normally Microsoft takes
the whole disk, and it's difficult to persuade it otherwise.  To install FreeBSD
on a disk that currently contains a single Microsoft partition taking up the
entire disk, go through the following steps:
.LB 2m 0m 0 0 \(sq
.LI
\f(BIMake a backup!\fP\/ There's every possibility of erasing your data, and
there's absolutely no reason why you should take the risk.
.LI
If you have an old machine with an IDE disk larger than 504 MB, you may run into
problems.  Refer to page \*[EIDE] for further details.
.LI
Boot Microsoft and repartition your disk with
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
.Command -n FIPS .
Refer to page
.Sref \*[FIPS] .
.LI
Insert the CD-ROM in the drive before booting.
.LI
Shut the machine down and reboot from the FreeBSD CD-ROM.  If you have to boot
from floppy, see page \*[exclusive-install] for details.
.LI
Select the \f(CWCustom\fP installation.
.LI
In the partition editor, delete \f(BIonly the second primary Microsoft
partition\fP.  The first primary Microsoft partition contains your Microsoft
data, and if there is an extended Microsoft partition, it will also contain your
Microsoft data.
.LI
Create a FreeBSD slice in the space that has been freed.  Refer to page
\*[building-partition-table].
.LI
On exiting from the partition editor, select the \fIBootMgr\fP\/ MBR.  Refer to
page \*[select-mbr].
.LI
In the disk label editor, delete any existing UNIX partitions.  Create the file
systems manually.  If you don't have any favourite layout, create a root file
system with 4 GB, a swap partition with at least 512 MB (make sure it's at least
1 MB larger than the maximum memory you intend to install in your system).
Allocate a
.File /home
file system as large as you like, as long as it can fit on a single tape when
backed up.  If you have any additional space, leave it empty unless you know
what to use it for.  See page \*[partition-size] for the rationale of this
approach, which is not what
.Command sysinstall
recommends.
.LI
Before leaving the disk label editor, also select mount points for your DOS
partitions if you intend to mount them under FreeBSD.  Refer to page
\*[dos-mount-point].
.LI
Install the complete system, including X and the Ports Collection.  This
requires about 1 GB of disk space.  Refer to page \*[select-distribution] if you
want to limit it.
.LI
Select CD-ROM as installation medium.  Refer to page \*[select-medium].
.LI
Give final confirmation.  The system will be installed.  Refer to page
\*[commit].
.LI
After installation, set up at least a user ID for yourself.  Refer to page
\*[logging-in].
.Le
.\" XXX.ds Section*title\" No title here
.H2 "Configuring XFree86"
.Pn X-install-checklist
You can configure XFree86 during installation or after reboot.
.LB 2m 0m 0 0 \(sq
.LI
Make sure your mouse is connected to the system at boot time.  Depending on the
hardware, if you connect it later, it may not be recognized.
.LI
If you have already rebooted the machine, log in as \f(CWroot\fP and restart
.Command sysinstall .
.LI
Select the
.Command sysinstall
\f(CWConfiguration\fP menu, \f(CWXFree86\fP and then \f(CWxf86cfg\fP, and follow
the instructions.  See page
.Sref \*[X-config] \&
for further details.
.LI
Select the \f(CWDesktop\fP menu and install the window manager of your choice.
See page
.Sref \*[Desktop] \&
for further discussion.
.Le
