.\" Reset page layout between chapters
.\" .OP
.\" $Id: reset.mm,v 2.5 2003/04/09 19:57:45 grog Exp grog $
.ds Section*title "The Complete FreeBSD\" Don't leave a heading on a blank page
.ds Chapter*title\" Don't carry a heading over to the next chapter
.ad b
.nr H1 0			\" And reset the chapter number so it doesn't head
.nr firstpage 2
.br
.if o \{\
.	bp 
\&
.br
.\}
.nr firstpage 2
.bp
.\" XXXreset \n[%] \n[firstpage]
.br
