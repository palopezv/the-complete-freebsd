.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: shareinstall.mm,v 4.12 2003/04/09 19:26:50 grog Exp grog $
.\"
.Chapter \*[nchshareinstall] "Shared OS installation"
In many cases, you won't want to install FreeBSD on the system by itself: you
may need to use other operating systems as well.  In this chapter, we'll look at
what you need to do to prepare for such an installation.  If you're only running
FreeBSD on the machine, you don't need to read this chapter, and you can move on
to
.Sref "\*[chinstall]" .
.Highlight
Before you start the installation, read this chapter carefully.  It's easy to
make a mistake, and one of the most frequent results of mistakes is the total
loss of all data on the hard disk.
.End-highlight
Currently, only the ia32 (Intel) port of FreeBSD is capable of sharing with
other operating systems.  We'll concentrate on how to share your system with
Microsoft, because that's both the most difficult and the most common, but most
of this chapter applies to other operating systems as well.  You may want to
refer to the discussion of Microsoft and FreeBSD disk layouts on page
.Sref \*[partitions] .
.H2 "Separate disks"
The first question is: do you need to share a disk between FreeBSD and the other
operating system?  It's \fBmuch\fP easier if you don't have to.  In this
section, we'll look at what you need to do.
.P
Many operating systems will only boot from the first disk identified by the
BIOS, usually called the \f(CWC:\fP disk in deference to Microsoft.  FreeBSD
doesn't have this problem, so the easiest thing is to install FreeBSD on the
entire second disk.  BIOS restrictions usually make it difficult to boot from
any but the first two disks.
.P
.X "boot manager"
In this case, you don't really need to do anything special, although it's always
a good idea to back up your data first.  Install FreeBSD on the second disk, and
choose the \fIBoot Manager\fP\/ option in the partition editor (page
\*[bootmgr]).  This will then give you the choice of booting from the first or
second disk.  Note that you should not change the order of disks after such an
installation; if you do, the system will not be able to find its file systems
after boot.
.H2 "Sharing a disk"
If you intend to share a disk between FreeBSD and another operating system, the
first question is: is there enough space on the disk for FreeBSD?  How much you
need depends on what you want to do with FreeBSD, of course, but for the sake of
example we'll take 120 MB as an absolute minimum.  In the following section,
we'll consider what to do if you need to change your partitions.  If you already
have enough space for a FreeBSD partition (for example, if you have just
installed Microsoft specifically for sharing with FreeBSD, and thus have not
filled up the disk), continue reading on page
.Sref \*[2-partitions] .
.H2 "Sharing with Linux or another BSD"
Sharing with other free operating systems is relatively simple.  You still need
to have space for FreeBSD, of course, and unlike Microsoft, there are no tools
for shrinking Linux or BSD file systems: you'll have to remove them or recreate
them.  You can find some information about sharing
with Linux in the mini-Howto at
.URI http://www.linux.org/docs/ldp/howto/mini/Linux+FreeBSD.html .
.P
NetBSD and OpenBSD file systems and slices are very similar to their FreeBSD
counterparts.  They're not identical, however, and you may find that one of the
systems recognizes the partition of another system and complains about it
because it's not quite right.  For example, NetBSD has a \fId\fP\/ partition
that can go outside the boundary of the slice.  FreeBSD does not allow this, so
you get a harmless error message.
.H2 "Repartitioning with FIPS"
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
.Pn FIPS
Typically, if you've been running Microsoft on your machine, it will occupy the
entire disk.  If you need all this space, of course, there's no way to install
another operating system as well.  Frequently, though, you'll find that you have
enough free space in the partition.  Unfortunately, that's not where you want
it: you want the space in a new partition.  There are a number of ways to do so:
.Ls B
.LI
You can reinstall the software.  This approach is common in the Microsoft world,
but FreeBSD users try to avoid it.
.LI
You can use
.Command -n FIPS
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
to shrink a Microsoft partition, leaving space for FreeBSD.
.Command -n FIPS
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
is a public domain utility, and it is included on the FreeBSD CD-ROM.
.LI
If you can't use
.Command -n FIPS ,
use a commercial utility like
.Command PartitionMagic .
This is not included on the CD-ROMs, and we won't discuss it further.
.Le
In the rest of the section, we'll look at how to shrink a partition with
.Command -n FIPS .
If you do it with PartitionMagic, the details are different, but the principles
are the same.  In particular:
.Highlight
Before repartitioning your disk, make a backup.  You can shoot yourself in the
foot with this method, and the result will almost invariably be loss of data.
.End-highlight
If you've been running Microsoft on your system for any length of time, the data
in the partition will be spread all around the partition.  If you just truncate
the partition, you'll lose a lot of data, so you first need to move all the data
to the beginning of the partition.  Do this with the Microsoft defragmentation
utility.  Before proceeding, consider a few gotchas:
.Ls B
.LI
The new Microsoft partition needs to be big enough to hold not only the current
data, but also anything you will want to put in it in the future.  If you make
it exactly the current size of the data, it will effectively be full, and you
won't be able to write anything to it.
.LI
The second partition is also a Microsoft partition.  To install FreeBSD on it,
you need to convert it into a FreeBSD partition.
.LI
.X "MSCDEX, Microsoft driver"
.Command -n FIPS
may result in configuration problems with your Microsoft machine.  Since it adds
a partition, any automatically assigned partitions that follow will have a
different drive letter.  In particular, this could mean that your CD-ROM drive
will ``move.''  After you delete the second Microsoft partition and change it
into a FreeBSD partition, it will ``move'' back again.
.Le
For further information, read the
.Command -n FIPS
documentation in
.File /cdrom/tools/fips.doc .
In particular, note these limitations:
.Ls B
.LI
.Command -n FIPS
works only with Hard Disk BIOSes that use interrupt \f(CW0x13\fP for low-level
hard disk access.  This is generally not a problem.
.LI
.Command -n FIPS
does not split partitions with 12 bit FATs, which were used by older versions of
Microsoft.  These are less than 10 MB in size and thus too small to be worth
splitting.
.LI
.X "Linux"
.Command -n FIPS
splits only Microsoft partitions.  The partition table and boot sector must
conform to the MS-DOS 3.0+ or Windows 95 conventions.  This is marked by the
system indicator byte in the partition table, which must have the value 4 (16
bit sector number) or 6 (32 bit sector number).  In particular, it will
\fInot\fP\/ split Linux or Windows 2000 and later partitions.
.LI
.Command -n FIPS
does not yet work on extended Microsoft partitions.
.LI
.Command -n FIPS
needs a free partition entry.  It will not work if you already have four
partitions.
.LI
.Command -n FIPS
will not reduce the original partition to a size of less than 4085 clusters,
because this would involve rewriting the 16 bit FAT to a 12 bit FAT.
.Le
.SPUP
.H3 "Repartitioning\(eman example"
In this section, we'll go through the mechanics of repartitioning a disk.  We'll
start with a disk containing a single, complete Microsoft system.
.P
First, run the Microsoft error check utility on the partition you want to split.
Make sure no ``dead'' clusters remain on the disk.
.P
.X "RESTORRB, MS-DOS program"
Next, prepare a bootable floppy.  When you start
.Command -n FIPS ,
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
you will be given the opportunity to write backup copies of your root and boot
sector to a file on drive \f(CWA:\fP.  These will be called
\fIROOTBOOT.00\f(BIx\fR, where \f(BIx\fP represents a digit from \f(CW0\fP to
\f(CW9\fP.  If anything goes wrong while using
.Command -n FIPS ,
you can restore the original configuration by booting from the floppy and
running
.Command -n RESTORRB .
.X "RESTORRB, MS-DOS program"
.Aside
If you use
.Command -n FIPS
more than once (this is normally not necessary, but it might happen), your
floppy will contain more than one
.File ROOTBOOT
file.
.Command -n RESTORRB
.X "RESTORRB, MS-DOS program"
lets you choose which configuration file to restore.  The file
.File RESTORRB.000
contains your original configuration.  Try not to confuse the versions.
.End-aside
Before starting
.Command -n FIPS
you \fImust\fP\/ defragment your disk to ensure that the space to be used for
the new partition is free.
.X "IMAGE, MS-DOS program"
.X "MIRROR, MS-DOS program"
If you're using programs like
.Command -n IMAGE
.X "IMAGE, MS-DOS program"
or
.Command -n MIRROR ,
.X "MIRROR, MS-DOS program"
note that they store a hidden system file with a pointer to your mirror files in
the last sector of the hard disk.  You \fImust\fP\/ delete this file before
using
.Command -n FIPS .
It will be recreated the next time you run
.Command -n MIRROR .
.X "MIRROR, MS-DOS program"
To delete
it, in the root directory enter:
.Dx
C\e:> \f(CBattrib -r -s -h image.idx                       \fIfor IMAGE\f(CW
C\e:> \f(CBattrib -r -s -h mirorsav.fil                    \fIfor MIRROR
.De
Then delete the file.
.P
.ne 7v
If
.Command -n FIPS
does not offer as much disk space for creation of the new
partition as you expect, this may mean that:
.Ls B
.LI
You still have too much data in the remaining partition.  Consider making the
new partition smaller or deleting some of the data.  If you delete data, you
must defragment and run
.Command -n FIPS
again.
.LI
There are hidden files in the space of the new partition that have not been
moved by the defragmentation program.  Make sure which program they belong to.
If a file is a swap file of some program (for example NDOS) it is possible that
it can be safely deleted (and will be recreated automatically later when the
need arises).  See your manual for details.
.P
.ne 2v
If the file belongs to some sort of copy protection, you must uninstall the
program to which it belongs and reinstall it after repartitioning.
.Le
.Highlight
If you are running early versions of MS-DOS (before 5.0), or another operating
system, such OS/2, or you are using programs like Stacker, SuperStor, or
Doublespace, read the FIPS documentation for other possible problems.
.End-highlight
.H4 "Running FIPS"
After defragmenting your Microsoft partition, you can run
.Command -n FIPS \/:
.Dx
.\" For some reason, this next line formats differently for the complete
.\" book and for the individual chapter.
C:\e> \f(CBD:\fP                                         \fIchange to CD-ROM\fP\/
D:\e> \f(CBcd \etools\fP                                  \fImake sure you're in the tools directory\fP\/
D:\etools\e> \f(CBfips\fP                                 \fIand start the FIPS program\fP\/
\fI\&... a lot of copyright information omitted\fP\/
Press any key                                   \fIdo what the computer says\fP\/
Which Drive (1=0x80/2=0x81)?
.De
The message \fIWhich Drive\fP\/ may seem confusing.  It refers to BIOS internal
numbering.  Don't worry about it: if you want to partition the first physical
drive in the system, (\f(CWC:\fP), enter \f(CW1\fP, otherwise enter \f(CW2\fP.
Like the BIOS,
.Command -n FIPS
handles only two hard disks.
.P
If you start
.Command -n FIPS
under Windows, it will complain and tell you to boot from a floppy disk.  It
won't stop you from continuing, but it is a Bad Idea to do so.
.P
Next,
.Command -n FIPS
reads the root sector of the hard disk and displays the partition table:
.Dx
     |        |     Start      |      |      End       | Start  |Number of|
Part.|bootable|Head Cyl. Sector|System|Head Cyl. Sector| Sector |Sectors  |  MB
-----+--------+----------------+------+----------------+--------+---------+----
1    |    yes |   1    0      1|   0ch| 239 2047     63|      63| 40083057|19571
2    |     no |   0    0      0|   00h|   0    0      0|       0|        0|   0
3    |     no |   0    0      0|   00h|   0    0      0|       0|        0|   0
4    |     no |   0    0      0|   00h|   0    0      0|       0|        0|   0
.De
This shows that only the first partition is occupied, that it is bootable, and
that it occupies the whole disk (19571 MB, from Cylinder 0, Head 1, Sector 1 to
Cylinder 2047, Head 238, Sector 63).  It also claims that this makes 40083057
sectors.  It doesn't: the cylinder number has been truncated, and
.Command -n FIPS
complains about a partition table inconsistency, which it fixes.  After this, we
have:
.Dx
     |        |     Start      |      |      End       | Start  |Number of|
Part.|bootable|Head Cyl. Sector|System|Head Cyl. Sector| Sector |Sectors  |  MB
-----+--------+----------------+------+----------------+--------+---------+----
1    |    yes |   1    0      1|   0ch| 239 \f(CB2650\fP     63|      63| 40083057|19571
2    |     no |   0    0      0|   00h|   0    0      0|       0|        0|   0
3    |     no |   0    0      0|   00h|   0    0      0|       0|        0|   0
4    |     no |   0    0      0|   00h|   0    0      0|       0|        0|   0
.De
Don't worry about the ``bootable'' flag here\(emwe'll deal with that in the
FreeBSD installation.  First,
.Command -n FIPS
does some error checking and then reads and displays the boot sector of the
partition:
.Dx
Checking boot sector ... OK
Press any Key                                   \fIdo what it says\fP\/
Bytes per sector: 512
Sectors per cluster: 32
Reserved sectors: 32
Number of FATs: 2
Number of rootdirectory entries: 0
Number of sectors (short): 0
Media descriptor byte: f8h
Sectors per FAT: 9784
Sectors per track: 63
Drive heads: 240
Hidden sectors: 63
Number of sectors (long): 40083057
Physical drive number: 80h
Signature: 29h
.De
After further checking,
.Command -n FIPS
.X "FIPS, MS-DOS program"
.X "MS-DOS program, FIPS"
asks you if you want to make a backup floppy.  Enter your formatted floppy in
drive \f(CWA:\fP and make the backup.  Next, you see:
.Dx
Enter start cylinder for new partition (35 - 2650):
Use the cursor keys to choose the cylinder, <enter> to continue
Old partition      Cylinder       New Partition
  258.4 MB           35          19313.4 MB
.De
Use the \fBCursor Left\fP and \fBCursor Right\fP keys to adjust the cylinder
number at which the new partition starts.  You can also use the keys \fBCursor
Up\fP and \fBCursor Down\fP to change in steps of ten cylinders.
.Command -n FIPS
updates the bottom line of the display to show the new values selected.
Initially,
.Command -n FIPS
chooses the smallest possible Microsoft partition, so initially you can only
increase the size of the old partition (with the \fBCursor Right\fP key).  When
you're happy with the sizes, press \fBEnter\fP to move on to the next step.
.Highlight
Be very sure you're happy before you continue.  If you make the first partition
too small, there is no way to make it larger again.  On the other hand, if you
make it too large, you can split it again and then use \fRfdisk\fP\/ or MS-DOS
\fRFDISK\fP\/ to remove the superfluous partitions.
.End-highlight
.ne 10v
In this example, we choose equal-sized partitions:
.Dx
Old partition      Cylinder       New Partition
   251.5 MB          511            251.5 MB
\fI(pressed \fBEnter\fI)\f(CW
     |        |     Start      |      |      End       | Start  |Number of|
Part.|bootable|Head Cyl. Sector|System|Head Cyl. Sector| Sector |Sectors  |  MB
-----+--------+----------------+------+----------------+--------+---------+----
1    |    yes |   0    0      1|   06h|  15  511     63|       0|   515088| 251
2    |     no |   0    512    1|   06h|  15 1023     63|       0|   515088| 251
3    |     no |   0    0      0|   00h|   0    0      0|       0|        0|   0
4    |     no |   0    0      0|   00h|   0    0      0|       0|        0|   0

Do you want to continue or reedit the partition table (c/r)? \f(CBc\fP
.De
.X "CONFIG.SYS, file"
.X "AUTOEXEC.BAT, file"
.X "MIRROR, MS-DOS program"
.X "IMAGE, MS-DOS program"
.X "CHKDSK, MS-DOS program"
To ensure that the partition is recognized, reboot immediately.  Make sure to
disable all programs that write to your disk in
.File CONFIG.SYS
and
.File AUTOEXEC.BAT
before rebooting.  It might be easier to to rename the files or to boot from
floppy.  Be particularly careful to disable programs like
.Command -n MIRROR
.X "MIRROR, MS-DOS program"
and
.Command -n IMAGE ,
.X "IMAGE, MS-DOS program"
which might get confused if the partitioning
is not to their liking.  After rebooting, use
.Command -n CHKDSK
.X "CHKDSK, MS-DOS program"
or Norton Disk
Doctor to make sure the first partition is OK.  If you don't find any errors,
you may now reboot with your normal
.File CONFIG.SYS
and
.File AUTOEXEC.BAT .
Start some programs and make sure you can still read your data.
.P
After that, you have two valid Microsoft partitions on your disk.  We'll look at
what to do with them in the next chapter.  The specific differences from a
dedicated install are on page
.Sref \*[2-partitions] ,
but you'll need to start from the beginning of the chapter to do the install.
