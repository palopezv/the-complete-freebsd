.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: starting.mm,v 4.24 2003/08/07 22:48:18 grog Exp grog $
.\"
.Chapter \*[nchstarting] "Starting and stopping the system"
Before you can run FreeBSD, you need to start it up.  That's normally pretty
straightforward: you turn the machine on, a lot of things scroll off the screen,
and about a minute later you have a \f(CWlogin:\fP prompt or an X login window
on the screen.  Sometimes, though, the process is of more interest.  You have a
number of options when booting, and it's also a source of a number of problems,
so it pays to understand it.  In this chapter we'll look at the following
topics:
.Ls B
.LI
In the next section, we'll look at the startup process in more detail.
.LI
On page
.Sref \*[before-boot] \&
we'll look at how to control the boot process.
.LI
If something goes wrong, and the system doesn't come up, those messages that
scrolled off the screen are very important.  We'll look at what they mean on
page
.Sref \*[booting] .
.LI
It's not so obvious that you need to adhere to a procedure when shutting down
the system.  We'll look at the hows and whys on page
.Sref \*[shutdown] .
.LI
There are a number of ways of starting the system for particular applications.
On page
.Sref "\*[diskless]" \&
we'll look at how to run FreeBSD without a disk.
.Le
.sp -1v
.H2 "Starting the system"
.Pn bootup
When you power up the system, or when you reboot, a number of actions occur
before the system is up and running.  Starting the system is usually called
``bootstrapping,'' after the story of Baron von Munchhausen pulling himself up
by his own bootstraps.  The following sequence describes the sequence on the PC
architecture, but there are only relatively minor differences on other
platforms.
.Ls B
.LI
.X "Power On Self Test"
.X "POST"
First, the BIOS\*F
.FS
.X "BIOS"
.X "Basic Input/Output System"
.X "SRM"
.X "Open Firmware"
More accurately, the system firmware.  The firmware is called \fIBIOS\fP\/
(\fIBasic Input/Output System\fP\/) on the i386 architecture, \fISRM\fP\/ on the
Alpha architecture, and \fIOpen Firmware\fP\/ on a number of other
architectures.
.FE
performs tests that check that the machine is working correctly and determines
the hardware configuration.  This \fIPower On Self Test\fP\/ or \fIPOST\fP\/ has
nothing to do with FreeBSD.
.LI
.X "starting the system"
.X "bootstrap"
.X "master boot record"
Next, the BIOS bootstrap loads the \fIMaster Boot Record\fP\/ from the first
sector of the first disk on the system (\f(CWC:\fP in BIOS parlance) into memory
and executes it.    This step is the same for all operating systems on PCs.
.LI
It's up to this bootstrap to decide which operating system to boot.  The
bootstrap in the MBR may or may not be part of the FreeBSD system.  FreeBSD can
install two different MBRs, as we saw on page
.Sref \*[MBR-choices-page] .
The standard MBR works without intervention, while the boot manager gives you
the choice of booting from any partition on the disk.
.LI
.X "BTX"
The FreeBSD bootstrap first loads the second-level bootstrap, \fIBTX\fP, from
the next 15 sectors on disk and executes it.
.LI
.X "loader, bootstrap"
The second-level boot locates the third-level bootstrap, called \fIloader\fP,
and loads it into memory.  \fIloader\fP is an intelligent bootstrap component
that allows preloading of multiple kernel components.  See the man page
\fIloader(8)\fP\/ for more information.  By default, \fIloader\fP\/ locates the
kernel, the file
.File /boot/kernel/kernel
on the root file system, and loads it into memory.  You can interrupt the loader
at this point, for example to load different or additional files.
.LI
.X "probing"
The kernel performs its own tests to look for hardware it knows about.  It's
quite verbose about this, and prints messages about both the hardware it finds
and the hardware it doesn't find.  This operation is called \fIprobing\fP.  Most
kernels are built to recognize a large selection of hardware, so it's normal to
have more ``not found'' messages than ``found'' messages.
.LI
.X "swapper"
After the probe, the kernel starts two processes.  The first, process 0, is the
.Daemon swapper
and is responsible for emergency clearing of memory when the standard virtual
memory algorithms aren't fast enough.
.LI
.Pn init
Process 1 is called
.Daemon init .
As the name suggests, it is responsible for
starting up the system and daemons.  When coming up in the default multi-user
mode, it spawns a shell to execute the shell script
.Command -n /etc/rc .
.LI
.Command -n /etc/rc
first reads in the description files
.File /etc/defaults/rc.conf ,
which contains defaults for a number of configuration variables, and
.File /etc/rc.conf ,
which contains your modifications to the defaults.  It then proceeds to perform
the steps necessary to bring up the system, first starting virtual disk drivers,
mounting swap space and checking the file system integrity if necessary.
.LI
When
.Command -n /etc/rc
exits,
.Daemon init
reads the file
.File /etc/ttys
and
starts processes as determined there.  It spends the rest of its life looking
after these processes.
.Le
.H2 "Things you can do before booting"
.Pn before-boot
You can do a number of things before you boot the system:
.Ls B
.LI
The most obvious thing to do is to decide what you're going to boot.  The boot
loader gives you the chance to load different operating systems or different
FreeBSD kernels and modules.  We'll look at that below.
.LI
You can also set a number of options for the kernel loader, including
specification of hardware and software characteristics.  We'll look at that on
page
.Sref \*[io-config] .
.Le
.H2 "What are you going to boot?"
.Pn booting
If you have multiple operating systems on your system, you can use the boot
manager described on page
.Sref \*[bootmgr] ,
to choose which one to boot.  For example, if you have two disks, the first of
which contains four partitions, the first stage of the boot looks something like
this:
.Dx
F1: FreeBSD
F2: Windows
F3: Linux
F4: FreeBSD
F5: Drive 1

Default: F1
.De
After 10 seconds, the boot manager attempts to load the bootstrap from the
default partition; you can choose any of the selections by pressing the
corresponding function key.  If you press \fBF5\fP, you get a menu showing the
partitions on the second disk, with F5 to return to the first disk.
.P
.Pn boot-msg-start
If you choose to boot FreeBSD, the bootstrap loaders load, and you see something
like:
.Dx
/                                               \fIthis is a ``twirling baton''\fP\/
BTX loader 1.00  BTX version is 1.01
BIOS drive A: is disk0
BIOS drive C: is disk1
BIOS drive D: is disk1
BIOS 639kB/130048kB available memory
.De
.X "BTX"
These messages are printed by \fIBTX\fP.
.X "baton, twirling"
.X "twirling baton"
If you're loading from disk, the \f(CW/\fP character at the end of the previous
line keeps changing through \f(CW-\fP, \f(CW\e\fP, and \f(CW|\fP before going
back to \f(CW/\fP again, giving the impression that the character is rotating.
This display, called a \fItwirling baton\fP, is your indication that the system
hasn't crashed and burned.  It's normal for it to take a few seconds before the
baton starts to twirl.
.P
Next,
.Command loader
prints its prompt:
.Dx
FreeBSD/i386 bootstrap loader, Revision 0.8
(grog@freebie.example.com, Thu Jun 13 13:06:03 CST 2002)
Loading /boot/defaults/loader.conf

Hit [Enter] to boot immediately, or any other key for command prompt.
Booting [kernel] in 6 seconds...                \fIthis counts down from 10 seconds\fP\/
.De
At this point, you would normally continue with the boot, either by pressing the
\fBEnter\fP key or just waiting the 10 seconds.  We'll see what happens then on
page
.Sref \*[run-kernel] .
.P
Sometimes you may want to change software or hardware characteristics.  In this
case, you press the ``any other key'' (by tradition the space bar) and enter
commands to the loader.
.P
.H3 "Loader commands"
There are two ways to communicate with the loader:
.Ls B
.LI
A number of files in the directory
.Directory /boot
on the root file system tell
the loader what to do.  Most are not intended to be changed, but you can create
a file called
.File /boot/loader.conf ,
into which you can enter commands to override the commands in
.File /boot/defaults/loader.conf .
We'll look at this
below.
.LI
In addition, the file
.File /boot/device.hints
takes the place of many configuration file entries and allows you to set
hardware characteristics such as information about IRQ, DMA, I/O address and
other settings for the hardware.  You can change these values during booting.
.P
The CD-ROM installation installs
.File /boot/device.hints ,
but a kernel install does not.  You'll find it in the
.Directory conf
directory for your architecture.  For example,
.Directory /usr/src/sys/i386/conf
includes the configuration file
.File GENERIC
and the corresponding hints file
.File GENERIC.hints .
Install it like this:
.Dx
# \f(CBcp /usr/src/sys/i386/conf/GENERIC.hints /boot/device.hints\fP
.De
The hints file contains entries of the following nature:
.Dx
hint.sio.0.at="isa"
hint.sio.0.port="0x3F8"
hint.sio.0.flags="0x10"
hint.sio.0.irq="4"
hint.sio.1.at="isa"
hint.sio.1.port="0x2F8"
hint.sio.1.irq="3"
hint.sio.2.at="isa"
hint.sio.2.disabled="1"
hint.sio.2.port="0x3E8"
hint.sio.2.irq="5"
hint.sio.3.at="isa"
hint.sio.3.disabled="1"
hint.sio.3.port="0x2E8"
hint.sio.3.irq="9"
.De
These entries describe the serial port configuration.  They replace the older
method of hard coding the information in the kernel.  For example, the hints
above contain the configuration information contained in these lines of the
Release 4 configuration file:
.Dx
device          sio0    at isa? port IO_COM1 flags 0x10 irq 4
device          sio1    at isa? port IO_COM2 irq 3
device          sio2    at isa? disable port IO_COM3 irq 5
device          sio3    at isa? disable port IO_COM4 irq 9
.De
The corresponding line in the Release 5 configuration file is:
.Dx
device          sio             # 8250, 16[45]50 based serial ports
.De
More importantly, though, this means that you don't need to recompile the kernel
if you change the hardware addresses.
.LI
You can enter commands directly to the command prompt.
.Le
When you hit the space bar, you get the following prompt:
.Dx
Type '?' for a list of commands, 'help' for more detailed help.
ok \f(CB?\fP
Available commands:
  reboot           reboot the system
  heap             show heap usage
  bcachestat       get disk block cache stats
  boot             boot a file or loaded kernel
  autoboot         boot automatically after a delay
  help             detailed help
  ?                list commands
  show             show variable(s)
  set              set a variable
  unset            unset a variable
  more             show contents of a file
  lsdev            list all devices
  include          read commands from a file
  ls               list files
  load             load a kernel or module
  unload           unload all modules
  lsmod            list loaded modules
  pnpscan          scan for PnP devices
.De
The most important of these commands are \f(CWset\fP, \f(CWshow\fP,
\f(CWload\fP, \f(CWunload\fP and \f(CWboot\fP.  We'll see some examples of their
use in the following sections.  Note, however, that if you have accidentally hit
the ``any'' key during boot and just want to continue with the boot, you just
have to enter \f(CBboot\fP.
.H3 "loader.conf"
.Pn io-config
Much of the behaviour of the loader is controlled by entries in
.File /boot/defaults/loader.conf .
You shouldn't change this file, though: put changes in a file
.File /boot/loader.conf ,
which may not exist.  There are a large number of possible entries; in
.File /boot/defaults/loader.conf
you'll
see the default values, frequently commented out because the loader already
knows the defaults.  Here are some of the more interesting ones:
.Dx
kernel="kernel"

verbose_loading="NO"            # Set to YES for verbose loader output

#autoboot_delay="10"            # Delay in seconds before autobooting
#console="vidconsole"           # Set the current console
#currdev="disk1s1a"             # Set the current device
module_path="/boot/kernel;/boot/modules;/modules"       # Set the module search path
#prompt="\\${interpret}"        # Set the command prompt
#root_disk_unit="0"             # Force the root disk unit number
#rootdev="disk1s1a"             # Set the root filesystem
.De
.Ls B
.LI
The \f(CWkernel\fP entry gives the name of the kernel, relative to the kernel
directory
.Directory /boot/kernel .
Sometimes it might be of interest to change this value, for example when
testing.
.LI
\f(CWconsole=vidconsole\fP tells the loader where to output its messages.
\f(CWvidconsole\fP is short for \fIvideo console\fP\/; you can also select
\f(CWcomconsole\fP if you have a serial terminal connected to a specified serial
port.
.LI
\f(CWcurrdev\fP specifies where to look for the root file system.  If you have
multiple BIOS partitions on a disk, you can select the correct one with this value.
.Le
There are many more options to the loader; read the man page for more details.
.H3 "Loading other modules at boot time"
By default,
.Command loader
loads only the kernel.  That may not be what you want.  You might want to load a
different kernel, or you may want to load a kld as well.
.P
There are two ways to do this.  If you only want to do this once, you can
interrupt the boot sequence by pressing the space bar, and tell
.Command loader
what to do:
.X "twirling baton"
.X "baton, twirling"
.Dx
Booting [kernel] in 6 seconds...                \fIthis counts down from 10 seconds\fP\/
\fI(space bar hit)\fP\/
Type '?' for a list of commands, 'help' for more detailed help.
ok \f(CBunload\fP                                       \fInot the kernel we wanted\fP\/
OK \f(CBload /boot/kernel.old/kernel\fP                 \fIload the old kernel\fP\/
/boot/kernel.old/kernel text=0x3e474c data=0x52f00+0x81904 syms=[0x4+0x4cab0+0x4+0x5
b458]
OK \f(CBload /boot/kernel.old/vinum.ko\fP               \fIand the old vinum module\fP\/
/boot/kernel.old/vinum.ko text=0x149a4 data=0xaf75c+0x164 syms=[0x4+0x11e0+0x4+0xcac]
ok \f(CBboot\fP                                         \fIthen start the kernel\fP\/
Copyright (c) 1992-2002 The FreeBSD Project.
Copyright (c) 1979, 1980, 1983, 1986, 1988, 1989, 1991, 1992, 1993, 1994
        The Regents of the University of California. All rights reserved.
FreeBSD 5.0-RELEASE #0: Sat 15 Feb 16:30:26 CST 2003
    grog@monorchid.example.org:/usr/src/sys/i386/compile/BUMBLE
Preloaded elf kernel "/boot/kernel.old/kernel" at 0xc072a000.
Preloaded elf module "/boot/kernel.old/vinum.ko" at 0xc072a0bc.
Timecounter "i8254"  frequency 1193182 Hz
\fI(etc)\fP\/
.De
This example shows two separate activities: one is changing the kernel from
.File /boot/kernel/kernel
to
.File /boot/kernel.old/kernel ,
and the other is loading the \fIvinum\fP\/ kld.  You don't need to reload the
kernel to load the \fIvinum\fP\/ module.
.H3 "Automatic kld load"
The method described above is cumbersome if you want to load the kld every time
you boot.  In this case, it's easier to add the following line to
.File /boot/loader.conf \/:
.Dx
vinum_load="YES"
.De
To see what commands you can use, look in
.File /boot/defaults/loader.conf ,
where you would find all normal configuration entries commented out.
.Dx
\fI\&...\fP\/
ccd_load="NO"                   # Concatenated disk driver
vinum_load="NO"                 # Concatenated/mirror/raid driver
md_load="NO"                    # Memory disk driver (vnode/swap/malloc)
\fI\&...\fP\/
.De
Don't change this file; it's designed to be replaced on upgrade, and any changes
would get lost when you upgrade.
.H2 "Running the kernel"
.Pn run-kernel
The next step in the boot process is to run the kernel.  This is what happens by
default if you do nothing at the \f(CWBooting [kernel]\fP prompt, or if you
press \fBEnter\fP.  If you have interrupted the boot process, you continue with
the command:
.Dx
ok \f(CBboot\fP
.De
The following example shows the output of booting an Abit BP6 dual processor
motherboard.  This board also has four IDE controllers on board, and the system
had two SCSI host adapters connected to it.
.P
The loader transfers control to the kernel it has preloaded.  Messages from the
kernel are in high-intensity text (brighter than normal).  This is the most
common time to see them, though they sometimes appear during normal machine
operation.
These messages also get copied to the kernel message buffer, and you can
retrieve the most recent messages with the
.Command dmesg
program.  In the course of time, other messages may fill the buffer, and you
will no longer be able to find the boot messages with
.Command dmesg ,
so one of the final steps in the startup saves the content of the boot messages
in the file
.File /var/run/dmesg.boot ,
which should always contain the complete startup messages.  In the case of
laptops, the message buffer normally does not get cleared on shutdown, not even
if the power goes down, so you may find logs for multiple boots.
.P
.X "probing"
.Pn device-probe
Once it has finished loading, the kernel prints some summary information and
then calls all configured drivers to examine the hardware configuration of the
machine on which it is running.  This is called \fIprobing\fP\/ for the devices.
If you have time, it's a good idea to confirm that it's correct.  Much of it
appears so quickly that you can't read it, but once the boot is complete, you
can examine it with the
.Command dmesg
command.  If something goes wrong, it \fIwon't\fP\/ scroll off the screen.  The
place where it stops is then of interest.
.P
Under normal circumstances, we see something like:
.Dx
Copyright (c) 1992-2002 The FreeBSD Project.
Copyright (c) 1979, 1980, 1983, 1986, 1988, 1989, 1991, 1992, 1993, 1994
        The Regents of the University of California. All rights reserved.
FreeBSD 5.0-RELEASE #0: Sat 15 Feb 16:30:26 CST 2003
    grog@monorchid.example.org:/usr/src/sys/i386/compile/BUMBLE
Preloaded elf kernel "/boot/kernel/kernel" at 0xc0663000.
.De
Here the kernel identifies itself with information about the release number,
when and where it was built, and where it was loaded from.
.Dx
Timecounter "i8254"  frequency 1193182 Hz
CPU: Pentium II/Pentium II Xeon/Celeron (467.73-MHz 686-class CPU)
  Origin = "GenuineIntel"  Id = 0x665  Stepping = 5
  Features=0x183fbff<FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,CMOV,P
AT,PSE36,MMX,FXSR>
real memory  = 134217728 (128 MB)
avail memory = 123465728 (117 MB)
.De
The lines above identify the basic hardware.  There is one time counter (some
motherboards have two), the CPU is a Celeron, Pentium II or Xeon, and it runs at
466 MHz.  This information is relatively reliable.  The real memory value is the
size of RAM.  Some older systems reserve 1 kB of RAM in real mode, but this
should not have any effect on the value of real memory.  Available memory is the
memory available to users after the kernel has been loaded and initialized.
.P
On some older machines, the kernel reports only 16 MB although the system has
more memory.  This is due to BIOS incompatibilities, and occurs surprisingly
often on big-name machines.  To fix it, build a custom kernel that specifies
the memory size explicitly\(emsee the description of the \f(CWMAXMEM\fP
parameter, which is described in the verbose configuration file
.File /usr/src/sys/i386/conf/NOTES .
.P
This machine is in fact a multiprocessor with two CPUs, so we see:
.Dx
Programming 24 pins in IOAPIC #0
IOAPIC #0 intpin 2 -> irq 0
IOAPIC #0 intpin 16 -> irq 10
IOAPIC #0 intpin 17 -> irq 9
IOAPIC #0 intpin 18 -> irq 11
FreeBSD/SMP: Multiprocessor System Detected: 2 CPUs
 cpu0 (BSP): apic id:  0, version: 0x00040011, at 0xfee00000
 cpu1 (AP):  apic id:  1, version: 0x00040011, at 0xfee00000
 io0 (APIC): apic id:  2, version: 0x00170011, at 0xfec00000
.De
.ne 3v
.X "IOAPIC"
The IOAPIC is the \fII/O Advanced Programmable Interrupt Controller\fP\/ used by
SMP machines only.  It reassigns some interrupt requests.  This information is
provided in case you need to debug the kernel.  None of this appears for a
normal machine.
.Dx
Initializing GEOMetry subsystem
Pentium Pro MTRR support enabled
npx0: <math processor> on motherboard                    \fInumeric coprocessor, on chip\fP\/
npx0: INT 16 interface
.De
.X "GEOM"
.X "MTRR"
The \fIGEOM\fP\/etry subsystem is a disk I/O system that was introduced in
FreeBSD Release 5.  This processor is a P6 class processor, so it has \fIMemory
Type Range Registers\fP\/ or \fIMTRR\fP\/s, which are used to optimize memory
usage.
.P
Next we look at the other chips on the motherboard, starting with the so-called
``chipset,'' the processor support chips.
.Dx
pcib0: <Intel 82443BX (440 BX) host to PCI bridge> at pcibus 0 on motherboard
pci0: <PCI bus> on pcib0
agp0: <Intel 82443BX (440 BX) host to PCI bridge> mem 0xe0000000-0xe3ffffff at devic
e 0.0 on pci0
pcib1: <PCIBIOS PCI-PCI bridge> at device 1.0 on pci0
pci1: <PCI bus> on pcib1
.De
This motherboard has an Intel 82443 BX chipset with two PCI buses.  Next we see
some of the devices on the motherboard:
.Dx
pci1: <Matrox MGA G200 AGP graphics accelerator> at 0.0
isab0: <Intel 82371AB PCI to ISA bridge> at device 7.0 on pci0
isa0: <ISA bus> on isab0                                 \fIISA bus\fP\/
atapci0: <Intel PIIX4 ATA33 controller> port 0xf000-0xf00f at device 7.1 on pci0
ata0: at 0x1f0 irq 14 on atapci0                         \fIprimary IDE controller\fP\/
ata1: at 0x170 irq 15 on atapci0                         \fIsecondary IDE controller\fP\/
uhci0: <Intel 82371AB/EB (PIIX4) USB controller> port 0xc000-0xc01f irq 10 at device
 7.2 on pci0                                             \fIUSB controller\fP\/
usb0: <Intel 82371AB/EB (PIIX4) USB controller> on uhci0 \fIUSB bus\fP\/
usb0: USB revision 1.0
uhub0: Intel UHCI root hub, class 9/0, rev 1.00/1.00, addr 1
uhub0: 2 ports with 2 removable, self powered
Timecounter "PIIX"  frequency 3579545 Hz
pci0: <bridge, PCI-unknown> at device 7.3 (no driver attached)
.De
The system doesn't know which devices are implemented internally in the chipset,
which are separate chips on the mother board, and which are on plug-in boards.
So far it has found the IDE controllers, but not the disks; it'll look for them
later.
.P
Next we find two Symbios SCSI host adapters:
.Dx
sym0: <875> port 0xc400-0xc4ff mem 0xec002000-0xec002fff,0xec003000-0xec0030ff irq 1
0 at device 9.0 on pci0
sym0: Symbios NVRAM, ID 7, Fast-20, SE, NO parity
sym0: open drain IRQ line driver, using on-chip SRAM
sym0: using LOAD/STORE-based firmware.
sym0: SCAN FOR LUNS disabled for targets 0.
sym1: <875> port 0xc800-0xc8ff mem 0xec001000-0xec001fff,0xec000000-0xec0000ff irq 9
 at device 13.0 on pci0
sym1: No NVRAM, ID 7, Fast-20, SE, parity checking
.De
.ne 3v
The first Symbios adapter is on IRQ 10.  It is on ID 7, like most SCSI host
adapters, and it doesn't support parity.  The second board is on IRQ 9 and does
support parity, but it doesn't have a BIOS.  This is not a problem for FreeBSD,
which doesn't use the BIOS, but if it were in the system by itself, the POST
would not find it.  In this case, the BIOS on the other Symbios board does in
fact find the second host adapter.
.Dx
dc0: <Macronix 98715AEC-C 10/100BaseTX> port 0xe000-0xe0ff mem
0xe7800000-0xe78000ff irq 11 at device 11.0 on pci0
dc0: Ethernet address: 00:80:c6:f9:a6:c8
miibus0: <MII bus> on dc0
dcphy0: <Intel 21143 NWAY media interface> on miibus0
dcphy0:  10baseT, 10baseT-FDX, 100baseTX, 100baseTX-FDX, auto
.De
This is a Macronix Ethernet card with associated PHY interface at IRQ 11.
.P
After that, we return to on-board peripherals, in this case two additional IDE
controllers and legacy ISA peripherals:
.Dx
atapci1: <HighPoint HPT366 ATA66 controller> port 0xd800-0xd8ff,0xd400-0xd403,0xd000
-0xd007 irq 11 at device 19.0 on pci0
ata2: at 0xd000 on atapci1                               \fIThird IDE controller\fP\/
atapci2: <HighPoint HPT366 ATA66 controller> port 0xe400-0xe4ff,0xe000-0xe003,0xdc00
-0xdc07 irq 11 at device 19.1 on pci0                    \fIFourth IDE controller\fP\/
ata3: at 0xdc00 on atapci2
orm0: <Option ROMs> at iomem 0xc0000-0xc7fff,0xc8000-0xc87ff on isa0
fdc0: ready for input in output                          \fIFloppy controller\fP\/
fdc0: cmd 3 failed at out byte 1 of 3
.De
The floppy driver command failure here is caused by the lack of any floppy drive
on this machine.
.Dx
atkbdc0: <Keyboard controller (i8042)> at port 0x60,0x64 on isa0
atkbd0: <AT Keyboard> flags 0x1 irq 1 on atkbdc0         \fIkeyboard\fP\/
kbd0 at atkbd0
vga0: <Generic ISA VGA> at port 0x3c0-0x3df iomem 0xa0000-0xbffff on isa0
sc0: <System console> at flags 0x100 on isa0             \fIsystem console\fP\/
sc0: VGA <16 virtual consoles, flags=0x300>
sio0 at port 0x3f8-0x3ff irq 4 flags 0x10 on isa0        \fIfirst serial port\fP\/
sio0: type 16550A                                        \fIit's a buffered UART\fP\/
sio1 at port 0x2f8-0x2ff irq 3 on isa0                   \fIsecond serial port\fP\/
sio1: type 16550A
sio2 not found at 0x3e8                                  \fIno more serial I/O ports\fP\/
sio3 not found at 0x2e8
.De
.X "COM1:"
UNIX starts counting device numbers from 0, whereas Microsoft starts counting
from 1.  Devices
.Device sio0
through
.Device -n sio3
are known as \fICOM1:\fP\/ through \fICOM4:\fP\/ in the Microsoft world.
.Dx
ppc0: <Parallel port> at port 0x378-0x37f irq 7 on isa0  \fIparallel port controller\fP\/
ppc0: Generic chipset (NIBBLE-only) in COMPATIBLE mode
plip0: <PLIP network interface> on ppbus0
lpt0: <Printer> on ppbus0                                \fIline printer on parallel port\fP\/
lpt0: Interrupt-driven port
ppi0: <Parallel I/O> on ppbus0                           \fIalternate I/O on the same port\fP\/
.De
.ne 3v
Next, on this multiprocessor board, we get some SMP-specific messages.  The
system tests the IO-APIC, which can sometimes cause problems, and then starts
the second processor:
.Dx
APIC_IO: Testing 8254 interrupt delivery
APIC_IO: routing 8254 via IOAPIC #0 intpin 2
SMP: AP CPU #1 Launched!
.De
.ne 5v
Finally, the system detects the disks connected to this machine:
.Dx
ad0: 19574MB <WDC WD205BA> [39770/16/63] at ata0-master UDMA33
ad4: 19574MB <WDC WD205BA> [39770/16/63] at ata0-master UDMA66
Waiting 15 seconds for SCSI devices to settle
(noperiph:sym0:0:-1:-1): SCSI BUS reset delivered.
da0 at sym1 bus 0 target 3 lun 0
da0: <SEAGATE ST15230W SUN4.2G 0738> Fixed Direct Access SCSI-2 device
da0: 20.000MB/s transfers (10.000MHz, offset 15, 16bit), Tagged Queueing Enabled
da0: 4095MB (8386733 512 byte sectors: 255H 63S/T 522C)
da1 at sym1 bus 0 target 0 lun 0
da1: <SEAGATE ST15230W SUN4.2G 0738> Fixed Direct Access SCSI-2 device
da1: 20.000MB/s transfers (10.000MHz, offset 15, 16bit), Tagged Queueing Enabled
da1: 4095MB (8386733 512 byte sectors: 255H 63S/T 522C)
.De
Here, we have four disks, one each on the first and third IDE controllers, both
as master, and two on the second SCSI host adapter.  There is nothing on the
first host adapter.
.P
.ne 2v
Finally, the system starts Vinum and mounts the root file system and the swap
partition:
.Dx
Mounting root from ufs:/dev/ad0s1a
vinum: loaded
vinum: reading configuration from /dev/ad0s1h
vinum: updating configuration from /dev/ad4s2h
swapon: adding /dev/ad0s1b as swap device
swapon: /dev/vinum/swap: No such file or directory
Automatic reboot in progress...
.De
At this point, the system is up and running, but it still needs to start some
services.  The remaining messages come from processes, not from the kernel, so
they are in normal intensity.
.Dx
add net default: gateway 223.147.37.5
Additional routing options: tcp extensions=NO TCP keepalive=YES.
routing daemons:.
Mounting NFS file systems.
additional daemons: syslogd
Doing additional network setup: portmap.
Starting final network daemons: rwhod.
setting ELF ldconfig path: /usr/lib /usr/lib/compat /usr/X11R6/lib /usr/local/lib
setting a.out ldconfig path: /usr/lib/aout /usr/lib/compat/aout /usr/X11R6/lib/aout
starting standard daemons: inetd cron
Initial rc.i386 initialization:.
rc.i386 configuring syscons: blank_time.
Local package initialization:.
Additional TCP options:.
Tue Apr 23 13:59:05 CST 2000
.De
At this point, the kernel has finished probing, and it transfers control to the
shell script
.Command -n /etc/rc .
From this point on the display is in normal intensity.
.Command -n /etc/rc
first reads the configuration information in
.File /etc/defaults/rc.conf
and
.File /etc/rc.conf
(see page
.Sref \*[rc.conf] ).
Next, it checks the consistency of the file systems.  Normally you'll see
messages like this for each file system in
.File /etc/fstab \/:
.Dx
/dev/da0s1a: FILESYSTEM CLEAN; SKIPPING CHECKS
/dev/da0s1a: clean, 6311 free (367 frags, 743 blocks, 0.9% fragmentation)
/dev/da0s1e: FILESYSTEM CLEAN; SKIPPING CHECKS
/dev/da0s1e: clean, 1577 files, 31178 used, 7813 free (629 frags, 898 blocks, 1.6% fr
agmentation)
.De
If your system has crashed, however, either due to a software or hardware
problem, or because it was not shut down correctly, it will perform a file
system check
.Command ( fsck ),
which can take quite a while, up to an hour on very big file systems.  You'll
see something like:
.Dx
WARNING: / was not properly dismounted
/dev/da0s1a: 6311 free (367 frags, 743 blocks, 0.9% fragmentation)
.De
On a large file system,
.Command fsck
can take a long time to complete, up to several hours in extreme cases.  By
default, the system does not need to wait for it to terminate; the
.Command fsck
continues in the background.  This is a relatively new feature in FreeBSD, so
you can turn it off in case you have problems with it.  See page
.Sref "\*[fsck-options]" \&
for more details.
.P
.ne 10v
Next,
.Command -n /etc/rc
invokes the first of three network start invocations.  This one initializes the
interfaces, sets the routes and starts the firewall if necessary:
.Dx
Doing initial network setup: hostname.
dc0: flags=8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> mtu 1500
        inet 223.147.37.81 netmask 0xffffff00 broadcast 223.147.37.255
.\" Yes, this is an Easter Egg.
        inet6 fe80::280:c6ff:fef9:a6c8%dc0 prefixlen 64 scopeid 0x1
        ether 00:80:c6:f9:a6:c8
        media: autoselect (100baseTX <full-duplex>) status: active
        supported media: autoselect 100baseTX <full-duplex> 100baseTX 10baseT/UTP <f
ull-duplex> 10baseT/UTP 100baseTX <hw-loopback> none
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
        inet 127.0.0.1 netmask 0xff000000

add net default: gateway 223.147.37.5
Additional routing options:.
routing daemons:.
.De
In this example, there were no additional routing options and no routing
daemons.  The messages accordingly have nothing between the character \f(CW:\fP
and the final period.  You'll see this relatively frequently.
.P
Next,
.Command -n /etc/rc
mounts the network file systems, cleans up
.Directory /var/run
and then starts
.Daemon syslogd \/:
.Dx
Mounting NFS file systems.
Additional daemons: syslogd.
.De
Then it checks if we have a core dump.  If so, it tries to save it to
.Directory /var/crash .
.Dx
checking for core dump...savecore: no core dump
.De
Saving the core dump may fail if there isn't enough space in
.Directory /var/crash .
If this happens, you can clean up and save the dump later, as long as you
haven't used enough swap space to overwrite the dump.
.P
Next comes the second pass of the network startup, which starts our choice of
.Daemon named ,
.Command ntpdate ,
.Daemon ntpd ,
.Daemon timed ,
.Daemon portmap ,
.Daemon ypserv ,
.Daemon rpc.ypxfrd ,
.Daemon rpc.yppasswdd ,
.Daemon ypbind ,
.Daemon ypset ,
.Daemon keyserv
and
.Daemon rpc.ypupdated \/:
.Dx
Doing additional network setup: named xntpd portmap.
starting.  named 8.1.2 Sun May  9 13:04:13 CST 1999  grog@freebie.example.org:/usr
/obj/usr.sbin/named
master zone "example.org" (IN) loaded (serial 1997010902)
master zone "37.147.223.in-addr.arpa" (IN) loaded (serial 1996110801)
listening on [223.147.37.149].53 (ep0)
listening on [127.0.0.1].53 (lo0)
Forwarding source address is [0.0.0.0].1063
Ready to answer queries.
.De
With the exception of the first line, all the messages come from
.Daemon named .
They may come in the middle of the first line, rather than waiting for the end
of the line.
.P
Next,
.Command -n /etc/rc
enables quotas if asked, and then runs the third network pass, which starts our
choice of
.Daemon mountd ,
.Daemon nfsd ,
.Daemon rpc.lockd ,
.Daemon rpc.statd ,
.Daemon nfsiod ,
.Daemon amd ,
.Daemon rwhod
and
.Daemon kerberos \/:
.Dx
Starting final network daemons: mountd nfsd rpc.statd nfsiod rwhod.
.De
Now we're almost done.
.Command -n /etc/rc
rebuilds a couple of internal databases (for use by the
.Command ps
command and some others), then it sets the default paths for
.Command ldconfig \/:
.Dx
setting ELF ldconfig path: /usr/lib /usr/lib/compat /usr/X11R6/lib /usr/local/lib
setting a.out ldconfig path: /usr/lib/aout /usr/lib/compat/aout
/usr/X11R6/lib/aout
/usr/local/lib/aout
.De
Next, it starts your choice of
.Daemon inetd ,
.Daemon cron ,
.Daemon printer ,
.Daemon sendmail
and
.Daemon usbd \/:
.Dx
starting standard daemons: inetd cron sendmail.
.De
The last thing that
.Command -n /etc/rc
does is to check for other startup files.  These could be in the files specified
in the variable \f(CWlocal_startup\fP, or in the file
.File /etc/rc.local .
In our case, there are none, so all we see is:
.Dx
Local package initialization:.
.De
Finally, we're done.
.Command -n /etc/rc
stops, and
.Daemon init
processes
.File /etc/ttys ,
which starts
.Command getty
processes on specified terminals.  On the console, we see:
.Dx
Mon May 13 13:52:00 CST 2002

FreeBSD (freebie.example.org) (ttyv0)

login:
.De
At this point, we're at the beginning of Chapter
.Sref \*[nchunixref] \&
(page
.Sref \*[login-prompt] ).
.H2 "Single-user mode"
.Pn single-user
.X "single-user mode"
Sometimes it's inconvenient that multiple users can access the system.  For
example, if you're repartitioning a disk, you don't want other people walking
all over the disk while you're doing so.  Even if you're the only user on the
system, daemons may be doing things in the background.  To avoid this problem,
you can stop the boot process before most of the daemons have been started and
enter \fIsingle-user mode\fP.  To do this, set the \f(CWboot_single\fP variable,
or specify the \f(CW-s\fP flag at boot time:
.Dx
ok \f(CBboot -s\fP
.De
As soon as the device probes have been completed, the system startup is
interrupted, and you are prompted for a shell.  Only the root file system is
accessible, and it is mounted read-only.  The reason for this is that the file
system may be damaged and require repair before you can write to it.  If you do
need to write to the root file system, you should first check the consistency of
the file system with
.Command fsck ,
after which you can mount it with the \f(CW-u\fP (update) option.  For example,
.Dx
\f(CBnpx0 on motherboard
npx0: INT 16 interface                          \fIend of the probes (high intensity display)\f(CW
Enter pathname of shell or RETURN for sh:       \fIhit Enter\fP\/
erase ^H, kill ^U, intr ^C
# \f(CBfsck -y /dev/ad0s1a\fP                           \fIcheck the integrity of the root file system\fP\/
** /dev/ad0s1a
** Last Mounted on /
** Root file system
** Phase 1 - Check Blocks and Sizes
** Phase 2 - Check Pathnames
** Phase 3 - Check Connectivity
** Phase 4 - Check Reference Counts
** Phase 5 - Check Cyl groups
1064 files, 8190 used, 6913 free (61 frags, 1713 blocks, 0.4% fragmentation)
# \f(CBmount -u /\fP                                    \fIremount root file system read/write\fP
# \f(CBmount /usr\fP                                    \fImount any other file systems you need\fP
.De
To leave single-user mode and enter multi-user mode, just enter \fBCtrl-D\fP:
.Dx
# \f(CB^D\fP
Skipping file system checks...
\fI(the rest of the boot sequence)\fP\/
.De
.X "run levels"
.X "init, S command"
System V and Linux have the concept of \fIrun levels\fP, which are controlled by
.Daemon init .
Single-user mode corresponds to run level 1 or S, and multi-user mode
corresponds roughly to System V run level 3 or Linux run level 4.  Nothing
corresponds with the other System V run levels, in particular run level 2, which
starts a System V system without networking.  Networking is such an integral
part of FreeBSD that this is just not practicable.  FreeBSD
.Daemon init
now understands a syntax similar to the System V
.Daemon init .
Table
.Sref \*[initlevel] \&
shows the supported levels.  For example, to read in the
.File /etc/ttys
file, you could enter:
.Dx 1
# \f(CBinit q\fP
.De
.br
.sp -1v
.ne 1i
.Table-heading "init levels"
.TS
tab(#) ;
r | l | l .
Level#Signal#Action
_
0#SIGUSR2#Halt and turn the power off
1#SIGTERM#Go to single-user mode
6#SIGINT#Reboot the machine
c#SIGTSTP#Block further logins
q#SIGHUP#Rescan the ttys(5) file
.TE
.Tn initlevel
.sp 1.5v
You can also enter single-user mode from a running FreeBSD system with the
.Command shutdown
command, which we'll look at in the next section.
.H3 "Password protecting single-user mode"
If you run a secure environment, you could be concerned about the fact that you
can start up in single-user mode without entering a password.  That's the
default\(emnormally, if somebody can access your system console, a password is
no longer much use, and it can be a nuisance\(embut you can change it.  Find
this entry in
.File /etc/ttys ,
and change the word \f(CWsecure\fP to
\f(CWinsecure\fP:
.Dx
# If you want to be asked for password, change "secure" to "insecure" here
console none                            unknown off \f(CBin\fPsecure
.De
If you do this, you will be in real trouble if you forget the root password.
.H2 "Shutting down and rebooting the system"
.Pn shutdown
FreeBSD uses a number of sophisticated techniques to achieve its high
performance.  In particular, when you write data to a disk, the system doesn't
put it on the disk immediately: it waits for more data to arrive, which reduces
the number of disk accesses by up to several orders of magnitude and thus
improves performance dramatically.
.P
The result of turning power off before the data is written is equally dramatic.
You may just lose the data, but if the data is information on a change in file
system structure, your file system will be damaged.  To check for this, the
system runs a program called
.Command fsck
(File System Check) at startup.
.Command fsck
can repair minor damage, but it's obviously a better idea to avoid damage by
ensuring that the system is shut down in an orderly way.
.Highlight
Never stop your machine by just turning off the power.  The results could be
devastating.
.End-highlight
.P
The correct way to shut a system down is with the
.Command shutdown
command.  To quote the man page \fIshutdown(8)\fP\/:
.P
.in +1m
.X hacker
.X guru
.vs 10
.ps 9
Shutdown provides an automated shutdown procedure for super-users to nicely
notify users when the system is shutting down, saving them from system
administrators, hackers, and gurus, who would otherwise not bother with such
niceties.
.br
.ps
.vs
.in
.P
This command has a number of useful options:
.Ls B
.LI
Use the \f(CW-r\fP option to reboot the computer.  You sometimes need to do
this, for example after installing a new kernel.
.LI
Use the \f(CW-h\fP option to stop the machine.  This isn't the default.
.LI
Without an option, \f(CWshutdown\fP attempts to put the machine in single-user
mode.  This doesn't always work as well as booting in single-user mode.
.LI
.Command shutdown
requires a time parameter to tell it when to actually perform the shutdown.
This is useful in a multi-user environment, but normally you'll want to shut
down immediately, so
.Command shutdown
understands the keyword \f(CWnow\fP.
.Le
In the normal case, where you want to stop the machine immediately so you can
turn the power off, you type:
.Dx
# \f(CBshutdown -h now\fP
Feb  4 12:38:36 freebie shutdown: halt by grog:
Feb  4 12:38:39 freebie syslogd: exiting on signal 15
syncing disks... done
The operating system has halted.
Please press any key to reboot.
.De
Be sure to wait for this message before you turn off the power.
.H2 "FreeBSD without disks"
.\" XXX check the changes to rc.diskless.
.Pn diskless
Disks are getting much cheaper and their capacity is continually increasing, so
it's easy to think that there would never be a reason to want to run FreeBSD
without a disk at all.  Still, there are reasons:
.Ls B
.LI
Disks are unreliable.  For a machine that you want to keep running for a long
time, the most likely hardware failure is a disk failure.
.LI
Disks are noisy.  There are places where you might not want the noise.
.LI
Disks are sensitive.  You may not want to place them in some environments.
.LI
.\" XXX
You may find systems administration easier if all the configuration files are in
one place.
.LI
The ``sweet spot'' for hard disk prices, the place where you get the most
storage for your dollar, is currently around 80 GB, about 10 times what it was
five years before.  You may not want that much disk space for any one machine,
but for a group, it might make more sense to have a disk for the whole group on
one machine.
.Le
There are a number of ways to run a system without a disk.  You can replace the
disk with something else, such as a flash card, floppy disk or CD-ROM drive, or
you can access a remote disk via a network.  We'll consider network booting in
the following section, and we'll look at the disk replacement strategy on page
.Sref \*[diskersatz] .
.H2 "Network booting"
.X "diskless workstation"
.Pn netboot
Network booting is not a new idea.  It was the original reason for Sun's
\fINetwork File System\fP, which we looked at in Chapters
.Sref \*[nchclient] \&
and
.Sref \*[nchserver] .
Nowadays people normally use NFS for additional shared file systems; in the case
of net booting, you mount your own private NFS file system as your root file
system.  Clearly, the first thing you need to do is to create this file system.
.P
Next, you need to find a way to boot the system.  There are a few possibilities
here:
.Ls B
.LI
You can boot a minimal system from floppy disk or CD-ROM and use this to mount
the file systems remotely.  This is different from running the system from
floppy or CD-ROM: in this case, the disk device serves effectively as a
bootstrap, and the operating system is located elsewhere.
.LI
You can create a boot PROM for your network card and use that to boot.
.LI
You can use PXE if your card supports it.
.Le
Whichever method you use, you need to set up a network interface very early.  In
Chapter
.Sref \*[nchnetsetup] \&
we saw that the network setup is part of the system initialization, and that the
configuration is stored in
.File /etc/rc.conf .
.X "DHCP"
For a network boot, the network must be running before the kernel can be loaded,
so that method won't work here.  Instead, we use \fIDHCP\fP, which we looked at
on page
.Sref \*[DHCP] .
We could also use the
.Daemon bootpd
daemon, but it's more limited, so it's better to use \fIDHCP\fP.
.P
If you use floppy or CD-ROM,  you could theoretically load the bootstrap from
that device.  This isn't the same as the alternative we'll see on page
.Sref \*[diskersatz] ,
where we load the kernel from floppy or CD: here we only load the bootstrap and
then load the kernel from the network.  This minor difference has significant
implications on the ease of system administration.
.P
.X "TFTP"
.X "Trivial File Transfer Protocol"
The next step is to actually transfer the data.  We do this with \fITFTP\fP, the
\fITrivial File Transfer Protocol\fP.  As the name suggests, \fITFTP\fP\/ is a
relatively simple replacement for \fIFTP\fP.  In particular, it knows almost
nothing about security.  If you use \fITFTP\fP, make sure that it can't be
accessed from outside your network, for example by using a firewall.  The
default firewall rules block \fITFTP\fP.
.P
.ne 2v
In the following sections we'll look at the example of setting up
\fIbumble.example.org\fP\/ as a diskless machine.
.H3 "Setting up the file systems"
There are a number of ways to put the files on the NFS server:
.Ls B
.LI
You might copy the files in the root and
.Directory /usr
file systems of the server machine.
.LI
You could install FreeBSD on a separate disk and NFS mount it where the remote
system can access it.  By itself, this doesn't have much of an advantage over
having a local disk on the machine, but it's possible to install a number of
systems on a single disk and have different machines access the different
installations.
.LI
You could combine those two methods and copy a freshly installed system to a
file system where you need it.
.Le
We'll look at refining this technique after the system is up and running.
.H3 "Building a diskless kernel"
You still need to build a special kernel for diskless workstations.  The
following entries in the configuration file are relevant:
.Dx
# Kernel BOOTP support

options         BOOTP           # Use BOOTP to obtain IP address/hostname
options         BOOTP_NFSROOT   # NFS mount root filesystem using BOOTP info
options         BOOTP_NFSV3     # Use NFS v3 to NFS mount root
options         BOOTP_COMPAT    # Workaround for broken bootp daemons.
options         BOOTP_WIRED_TO=fxp0 # Use interface fxp0 for BOOTP
.De
Only the first two are required.  If you use \f(CWBOOTP_WIRED_TO\fP, make sure
that the interface name matches the network card you are using.
.P
Build the kernel, as described on page
.Sref \*[buildkernel] .
To install, you need to set the \f(CWDESTDIR\fP variable to specify the
directory in which you want to install the kernel:
.Dx 1
# \f(CBmake install DESTDIR=/src/nodisk/bumble\fP
.De
.SPUP
.H3 "Configuring TFTP"
Next we need to set up \fITFTP\fP\/ to deliver the kernel to the system.  The
first question is whether the firmware on the Ethernet card can load the kernel
directly or not.  Some boot PROMs run in 16 bit 8086 mode, which limits their
addressing capability to 640 kB.  That's too small for any FreeBSD kernel, and
if you try to load the kernel directly you'll get a message like this:
.Dx
File transfer error: Image file too large for low memory.
.De
In this case, you'll need to load a loader, such as
.Command pxeboot .
.P
.ne 3v
As a minor concession to security, the
.Daemon tftpd
daemon refuses to access files outside its data directory hierarchy, which by
convention is called
.Directory /tftpboot .
You can use symbolic links, however.  It makes more sense to have the kernel in
the same place as on machines with disks, namely in
.File /boot/kernel/kernel
on the root file system, so we create symbolic links:
.Dx
# \f(CBmkdir /tftpboot\fP
# \f(CBln -s /src/nodisk/bumble/boot/kernel/kernel /tftpboot/kernel.bumble\fP
# \f(CBln -s /boot/pxeboot /tftpboot/pxeboot\fP
.De
We also need to ensure that we can start the \fITFTP\fP\/ daemon,
.Daemon tftpd .
Unless you're constantly booting, there's no need to have it running constantly:
just enable it in
.File /etc/inetd.conf ,
which has the following entries in the distribution file:
.Dx
#tftp   dgram   udp     wait    root    /usr/libexec/tftpd      tftpd -s /tftpboot
#tftp   dgram   udp6    wait    root    /usr/libexec/tftpd      tftpd -s /tftpboot
.De
These are entries for IPv4 and IPv6 respectively.
We enable
.Daemon tftpd
by uncommenting the first line (removing the \f(CW#\fP character) and sending a
HUP signal to
.Daemon inetd \/:
.Dx
# \f(CBkillall -1 inetd\fP                              \fIsend a SIGHUP\fP\/
.De
.De
.SPUP
.H3 "Configuring DHCP"
We already looked at
.Daemon dhcpd \/'s
configuration file
.File /usr/local/etc/dhcpd.conf
on page
.Sref \*[DHCP] ,
In addition to the information we looked at there, we need to know what file to
load, which system to load it from, and where the root file system is located.
For our diskless system \fIbumble\fP\/ we might add the text in bold to the
configuration we saw on page
.Sref \*[DHCP-server] \/:
.Dx
subnet 223.147.37.0 netmask 255.255.255.0 {
  range 223.147.37.90 223.147.37.110;
  option domain-name-servers freebie.example.com, presto.example.com;
  option domain-name "example.com";
  option routers gw.example.com;
  option subnet-mask 255.255.255.0;
  option broadcast-address 223.147.37.255;
  default-lease-time 86400;
  max-lease-time 259200;
  host sydney {
    hardware ethernet 0:50:da:cf:7:35;
  }

.ft CB
  host bumble {
    hardware ethernet 0:50:da:cf:17:d3;
    next-server presto.example.com;             \fIonly if on a different machine\fP\/
    filename "/tftpboot/bumble/kernel.bumble";  \fIfor direct booting\fP\/
    filename "/tftpboot/pxeboot";               \fIfor PXE\fP\/
    option root-path 223.147.37.1:/src/nodisk/bumble;
  }
.ft CW
}
.De
.ne 3v
There are a few things to note here:
.Ls B
.LI
The \f(CWnext-server\fP line tells where the \fITFTP\fP\/ server is located.  If
it's the same as the machine running the \fIDHCP\fP\/ server, you don't need
this specification.
.LI
As we've seen, hardware restrictions may make it impossible to load the kernel
directly.  In this case you need to load a loader.  The only one that FreeBSD
currently supplies is
.Command pxeboot .\*F
.FS
See
.URI http://www.freebsd.org/doc/en_US.ISO8859-1/articles/pxe/index.html
for documentation for setting up
.Command pxeboot
on FreeBSD.
.FE
Choose one of the two \f(CWfilename\fP lines.
.LI
You have to specify the root path as an IP address, because no name services are
available when the root file system is mounted.
.Le
.SPUP
.H3 "Other Ethernet bootstraps"
If your Ethernet card doesn't have a boot ROM, you can make one with the
.Directory net/etherboot
port, or you can copy the necessary information to a floppy disk or CD-R and use
that to start the bootstrap.  In either case, you first build the port and then
copy the data to your selected medium.  For example, to create a boot disk for a
Compex RL2000 card, a 10 Mb/s PCI NE-2000 clone, you first look up the card in
.File -n /usr/ports/net/etherboot/work/etherboot-5.0.5/src/NIC ,
where you read:
.Dx
# Compex RL2000
compexrl2000    ns8390          0x11f6,0x1401
.De
This information is mainly for the build process; you just need to know the
\f(CWcompexrl2000\fP, which is the name of the driver.
.Dx
# \f(CBcd /usr/ports/net/etherboot\fP
# \f(CBmake all\fP
# \f(CBcd work/ether*/src\fP
# \f(CBcat bin/boot1a.bin bin32/compexrl2000.lzrom > /dev/fd0\fP
.De
.File -n bin/boot1a.bin
is a disk bootstrap intended to load and start
.File -n compexrl2000.lzrom .
You can also put
.File -n compexrl2000.lzrom
in an EPROM.  This requires a little more care, and the information is subject
to change.  You can find detailed information about how to proceed at the web
site
.URI http://etherboot.sourceforge.net/doc/html/documentation.html .
.P
.Command etherboot
uses NFS, not TFTP.  As a result, things change: you can use absolute path
names, and you can't use symbolic links.  An entry in
.File -n dhcpd.conf
for this method might look like this:
.Dx
  host bumble {
    hardware ethernet 00:80:48:e6:a0:61;
    filename "/src/nodisk/bumble/boot/kernel/kernel";
    fixed-address bumble.example.org;
    option root-path "192.109.197.82:/src/nodisk/bumble";
  }
.De
When booting in this manner, you don't see any boot messages.  The boot loader
outputs several screens full of periods, each indicating a downloaded block.  It
finishes like this:
.Dx
\&................................done
\&.
.De
After that, nothing appears on the screen for quite some time.  In fact, the
boot is proceeding normally, and the next thing you see is a login prompt.
.br
.ne 10v
.H3 "Configuring the machine"
Setting up a diskless machine is not too difficult, but there are some gotchas:
.Ls B
.LI
Currently, locking across NFS does not work properly.  As a result, you may see
messages like this:
.Dx
Dec 11 14:18:50 bumble sm-mta[141]: NOQUEUE: SYSERR(root): cannot flock(/var/run/
sendmail.pid, fd=6, type=2, omode=40001, euid=0): Operation not supported
.De
One solution to this problem is to mount
.Directory /var
as an MD (memory) file system.  This is what currently happens by default,
though it's subject to change: at startup, when the system detects that it is
running diskless (via the sysctl \f(CWvfs.nfs.diskless_valid\fP), it invokes the
configuration file
.File /etc/rc.diskless1 .
This file in turn causes the file
.File /etc/rc.diskless2
to be invoked later in the startup procedure.  Each of these files adds an MD
file system.  In the course of time, this will be phased out and replaced by the
traditional configuration via
.File /etc/fstab ,
but at the moment this file has no provision for creating MD file systems.
.P
You should probably look at these files carefully: they may need some tailoring
to your requirements.
.LI
It is currently not possible to add swap on an NFS file system.
.Command swapon
(usually invoked from the startup scripts) reports, incorrectly:
.Dx
Dec 11 14:18:46 bumble savecore: 192.109.197.82:/src/nodisk/swap/bumble: No such
 file or directory
.De
This, too, will change; in the meantime, it \fIis\fP\/ possible to mount swap on
files, even if they are NFS mounted, but not on the NFS file system itself.
This means that the first of the following entries in
.File /etc/fstab
will not work, but the second will:
.Dx
192.109.197.82:/src/nodisk/swap/bumble  none swap  sw           0       0
/src/nodisk/swap/bumble                 none swap  sw           0       0
echunga:/src            /src            nfs     rw              0       0
.De
.ne 5v
The reason here is the third line:
.Directory -n /src/nodisk/swap/bumble
is NFS mounted, so this is a swap-to-file situation.  For this to work, you may
have to add the following line at the end of your
.File /etc/rc.diskless2 \/:
.Dx
swapon -a
.De
.ne 3v
This is because the standard system startup mounts swap before mounting
additional NFS file systems.  If you place the swap file on the root file
system, it will still work, but frequently you will want the root file system to
be read-only to be able to share it between several machines.
.LI
If the machine panics, it's not possible to take a dump, because you have no
disk.  The only alternative would be a kernel debugger.
.Le
.H3 "Sharing system files between multiple machines"
In many cases, you may have a number of machines that you want to run diskless.
If you have enough disk (one image for each machine), you don't have anything to
worry about, but often it may be attractive to share the system files between
them.  There are a lot of things to consider here:
.Ls B
.LI
Obviously, any changeable data specific to a system can't be shared.
.LI
To ensure that things don't change, you should mount shared resources read-only.
.LI
Refer to Table
.Sref "\*[dirowners]" \&
on page
.Sref "\*[dirowners-page]" \&
for an overview of FreeBSD installed directories.  Of these directories, only
.Directory /etc
and
.Directory /usr/local/etc
must be specific for a particular system, though there are some other issues:
.Ls B
.LI
Installing ports, for example, will install ports for all systems.  That's not
necessarily a bad thing, but if you have two systems both installing software in
the same directory, you can expect conflicts.  It's better to designate one
system, possibly the host with the disk, to perform these functions.
.LI
If you share
.Directory /boot
and make some configuration changes, the options will apply to all systems.
.LI
When building system software, you can use the same
.Directory /usr/src
and
.Directory /usr/obj
directories as long as all systems maintain the same release of FreeBSD.  You
can even have different kernels: each kernel build directory carries the name of
the configuration file, which by convention matches the name of the system.
.Le
.Le
The big problem is
.Directory /etc .
In particular,
.File /etc/rc.conf
contains information like the system name.  One way to handle this is to have a
separate
.Directory /etc
directory for each system.  This may seem reasonable, because
.Directory /etc
is only about 1.5 MB in size.  In fact, this implies mounting the entire root
file system with the other top-level directories, and that means more like 60
MB.
.br
.ne 10v
.H2 "Disk substitutes"
.Pn diskersatz
The other alternative to network booting is to find a local substitute for the
disk.  This is obviously the only alternative for a stand-alone machine.  There
are a number of alternatives:
.Ls B
.LI
.X "PicoBSD"
For really small systems, you can use \fIPicoBSD\fP, a special small version of
FreeBSD that fits on a single floppy disk.  It requires a fair amount of memory
as RAM disk, and obviously it's very limited.
.P
PicoBSD is good for some special applications.  As the FreeBSD kernel grows,
it's becoming more and more difficult to get even the kernel onto a single
floppy, let alone any application software.  Still, you can find a number of
different configurations in the source tree in
.Directory /usr/src/release/picobsd .
Be prepared for some serious configuration work.
.LI
Alternatively, you can boot from CD-R or CD-ROM.  In this case, you can have up
to 700 MB of data, enough for a number of applications.  It's possible to run
programs directly from the CD, but there's little advantage to having files on
CD instead of on disk.  The most likely application for this alternative is for
systems where the reliability of rotating media is insufficient, where the CD is
used only for booting, and after that the system runs from RAM disk.
.LI
.X "flash memory"
Yet another alternative is \fIFlash memory\fP, often abbreviated simply as
\fIFlash\fP, which we looked at in Chapter
.Sref \*[nchunixadmin] ,
on page
.Sref \*[flash] .
Flash is available in sizes up to several hundred megabytes, and Compact Flash
cards look like disks to their interface.  They don't fit IDE connectors, but
adapters are available.
.P
Flash memory is intended mainly for reading.  It is much slower to write than to
read, and it can only take a certain number of write cycles before it fails.
Clearly it's a candidate for read-only file systems.
.Le
