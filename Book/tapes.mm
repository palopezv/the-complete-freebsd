.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: tapes.mm,v 4.12 2003/06/29 03:06:33 grog Exp grog $
.\"
.Chapter \*[nchtapes] "Tapes, backups and floppy disks"
.P
In Chapter
.Sref \*[nchdisks] \&
we looked at hard disks.  In this chapter, we'll consider how to guard against
data loss, and how to transfer data from one location to another.  These are
functions that UNIX traditionally performs with tapes, and we'll look at them in
the next sections.  Because FreeBSD runs on PCs, however, you can't completely
escape floppy disks, though it would be an excellent idea.  We'll look at
floppies on page
.Sref \*[bloody-floppies] .
.H2 "Backing up your data"
.X "backing up your data"
No matter how reliable your system, you are never completely protected against
loss of data.  The most common reasons are hardware failure and human error.
By comparison, it's \fIvery\fP\/ seldom that a software error causes data loss,
but this, too, can happen.
.P
.X "archive"
.X "tarball"
UNIX talks about \fIarchives\fP, which are copies of disk data in a form
suitable for writing on a serial medium such as tape.  You can, however, write
them to disk files as well, and that's what people do when they want to move a
source tree from one system to another.  You'll also hear the term
\fItarball\fP\/ for an archive made by the
.Command tar
program, which we discuss below.
.H3 "What backup medium?"
Traditionally, PCs use floppy disks as a removable storage medium.  We'll look
at floppies below, but you can sum the section up in one statement: don't use
floppy disks.
.P
Floppy disks are particularly unsuited as a backup medium for modern computers.
Consider even a minimal system with a 2 GB hard disk.  Storing 2 GB of data on
floppies requires about 1,500 floppies, which, at $0.30 each, would cost you
$450.  Copying the data to a floppy takes about 50 seconds per floppy, so the
raw backup time would be about 21 hours, plus the time it takes you to change
the floppies, which could easily take another three or more hours.  During this
time you have to sit by the computer playing disk jockey, a total of three days'
work during which you could hardly do anything else.  When you try to read in
the data again, there's a virtual certainty that one of the floppies has a data
error, especially if you read them with a different drive.
.P
By contrast, a single DDS or Exabyte cassette stores several gigabytes and costs
about $6.  The backup time for 2 GB is about 90 minutes, and the operation can
be performed completely unattended.
.P
A number of cheaper tape drives are also available, such as Travan tapes.
FreeBSD supports them, but for one reason or another, they are not popular.
FreeBSD once used to have support for ``floppy tape,'' run off a floppy
controller, but these tapes were very unreliable, and they are no longer
supported.
.P
You can also use writeable ``CD-ROMs'' (\fICD-R\fP\/s) for backup purposes.  By
modern standards, the media are small (up to 700 MB), but they have the
advantage of being readily accessible on other systems.  We looked at CD-Rs in
Chapter
.Sref "\*[ncdburn]" .
.H3 "Tape devices"
.X "tape, device"
.Pn tape-devices
FreeBSD tape devices have names like
.Device nsa0
(see page
.Sref \*[sa0] ).
Each letter has a significance:
.Ls B
.LI
.X "tape, device"
\f(CWn\fP means \fInon-rewinding\fP.  When the process that accesses the tape
closes it, the tape remains at the same position.  This is inconvenient if you
want to remove the tape (before which you should rewind it), but it's the only
way if you want to handle multiple archives on the tape.  The name of the
corresponding \fIrewind device\fP\/ has no \f(CWn\fP (for example, the rewind
device corresponding to
.Device nsa0
is
.Device sa0 \/).
A rewind device rewinds the tape when it is closed.
.Aside
Older releases of FreeBSD used the names
.Device nrsa0
and
.Device rsa0 .
\f(CWr\fP stands for \fIraw\fP, in other words a character device.  Since the
removal of block devices, this letter is superfluous, but you might see it
occasionally in older documents.
.End-aside
.LI
\f(CWsa\fP stands for \fIserial access\fP, and is always SCSI.  You can also get
ATAPI tape drives, which are called
.Device ast0
and
.Device nast0 ,
and the older QIC-02 interface tapes are called
.Device wst0
and
.Device nwst0 .
.LI
\f(CW0\fP is the \fIunit number\fP.  If you have more than one tape, the next
will be called
.Device -n nsa1 ,
and so on.
.Le
.H3 "Backup software"
.X "backup software"
.Pn backups
FreeBSD does not require special ``backup software.''  The base operating system
supplies all the programs you need.  The tape driver is part of the kernel, and
the system includes a number of backup programs.  The most popular are:
.Ls B
.LI
.X "tape, archiver"
.Command tar ,
the \fItape archiver\fP, has been around longer than anybody can remember.  It
is particularly useful for data exchange, since everybody has it.  There are
even versions of
.Command tar
for Microsoft platforms.  It's also an adequate backup program.
.LI
.Command cpio
is an alternative backup program.  About its only advantage over
.Command tar
is that it can read
.Command cpio
format archives.
.LI
.Command pax
is another alternative backup program.  It has the advantage that it can also
read and write
.Command tar
and
.Command cpio
archives.
.LI
.Command dump
is geared more towards backups than towards archiving.  It can maintain multiple
levels of backup, each of which backs up only those files that have changed
since the last backup of the next higher (numerically lower) level.  It is less
suited towards data exchange because its formats are very specific to BSD.  Even
older releases of FreeBSD cannot read dumps created under FreeBSD Release 5.
.LI
.Command amanda ,
in the Ports Collection, is another popular backup program.
.Le
Backup strategies are frequently the subject of religious wars.  I personally
find that
.Command tar
does everything I want, but you'll find plenty of people who recommend
.Command dump
or
.Command amanda
instead.  In the following section, we'll look at the basics of using
.Command tar .
See the man page \fIdump(8)\fP\/ for more information on
.Command dump .
.H3 "tar"
.X "tape, archiver"
.Command tar ,
the \fItape archiver\fP, performs the following functions:
.Ls B
.LI
.X "archive"
Creating an \fIarchive\fP, which can be a serial device such as a tape, or a
disk file, from the contents of a number of directories.
.LI
Extracting files from an archive.
.LI
Listing the contents of an archive.
.Le
.Command tar
does not compress the data.  The resulting archive is slightly larger than the
sum of the files that it contains, since it also contains a certain amount of
header information.  You can, however, use the
.Command gzip
program to compress a
.Command tar
archive, and
.Command tar
invokes it for you automatically with the \f(CW-z\fP option.  The size of the
resultant archives depends strongly on the data you put in them.  JPEG images,
for example, hardly compress at all, while text compresses quite well and can be
as much as 90% smaller than the original file.
.H4 "Creating a tar archive"
.X "creating a tar archive"
.X "tar, creating an archive"
Create an archive with the \f(CWc\fP option.  Unlike most UNIX programs,
.Command tar
does not require a hyphen (\f(CW-\fP) in front of the options.  For example, to
save your complete kernel source tree, you could write:
.Dx
# \f(CBtar cvf source-archive.tar /usr/src/sys\fP
tar: Removing leading / from absolute path names in the archive.
usr/src/sys/
usr/src/sys/CVS/
usr/src/sys/CVS/Root
usr/src/sys/CVS/Repository
usr/src/sys/CVS/Entries
usr/src/sys/compile/
usr/src/sys/compile/CVS/
\fI\&(etc)\fP\/
.De
The parameters have the following meaning:
.Ls B
.LI
\f(CWcvf\fP are the options.  \f(CWc\fP stands for \fIcreate\fP\/ an archive,
\f(CWv\fP specifies \fIverbose\fP\/ operation (in this case, this causes
\fItar\fP\/ to produce the list of files being archived), and \f(CWf\fP
specifies that the next parameter is the name of the archive file.
.LI
\f(CWsource-archive.tar\fP is the name of the archive.  In this case, it's a
disk file.
.LI
\f(CW/usr/src/sys\fP is the name of the directory to archive.
.Command tar
archives all files in the directory, including most devices.  For historical
reasons,
.Command tar
can't back up devices with minor numbers greater than 65536, and changing the
format would make it incompatible with other systems.
.Le
The message on the first line (\f(CWRemoving leading / ...\fP) indicates that,
although the directory name was specified as \f(CW/usr/src/sys\fP,
.Command tar
treats it as
.Directory -n usr/src/sys .
This makes it possible to restore the files into another directory at a later
time.
.P
You can back up to tape in exactly the same way:
.Dx
# \f(CBtar cvf /dev/nsa0 /usr/src/sys\fP
.De
There is a simpler way, however: if you don't specify a file name,
.Command tar
looks for the environment variable \f(CWTAPE\fP.  If it finds it, it interprets
it as the name of the tape drive.  You can make things a lot easier by setting
the following line in the configuration file for your shell (
.File .profile
for
.Command sh ,
.File .bashrc
for
.Command bash ,
.File .login
for
.Command csh
and
.Command tcsh ):
.Dx
TAPE=/dev/nsa0 export TAPE                      \fIfor sh and bash\fP\/
setenv TAPE /dev/nsa0                           \fIfor csh and tcsh\fP\/
.De
After this, the previous example simplifies to:
.Dx
# \f(CBtar cv /usr/src/sys\fP
.De
.sp -1v
.H4 "Listing an archive"
.X "listing a \fItar\fP\/ archive"
.X "tar, listing an archive"
To list an archive, use the option \f(CWt\fP:
.Dx
# \f(CBtar t\fP                                         \fIfrom tape\fP\/
usr/src/sys/
usr/src/sys/CVS/
usr/src/sys/CVS/Root
usr/src/sys/CVS/Repository
usr/src/sys/CVS/Entries
usr/src/sys/compile/
usr/src/sys/compile/CVS/
usr/src/sys/compile/CVS/Root
\fI\&(etc)\fP\/
# \f(CBtar tvf source-archive.tar\fP                    \fIfrom disk\fP\/
drwxrwxrwx root/bin          0 Oct 25 15:07 1997 usr/src/sys/
drwxrwxrwx root/bin          0 Oct 25 15:08 1997 usr/src/sys/CVS/
-rw-rw-rw- root/wheel        9 Sep 30 23:13 1996 usr/src/sys/CVS/Root
-rw-rw-rw- root/wheel       17 Sep 30 23:13 1996 usr/src/sys/CVS/Repository
-rw-rw-rw- root/bin        346 Oct 25 15:08 1997 usr/src/sys/CVS/Entries
drwxrwxrwx root/bin          0 Oct 27 17:11 1997 usr/src/sys/compile/
drwxrwxrwx root/bin          0 Jul 30 10:52 1997 usr/src/sys/compile/CVS/
\fI\&(etc)\fP\/
.De
This example shows the use of the \f(CWv\fP (\fIverbose\fP\/) option with
\f(CWt\fP.  If you don't use it,
.Command tar
displays only the names of the files (first example, from tape).  If you do use
it,
.Command tar
also displays the permissions, ownerships, sizes and last modification date in a
form reminiscent of \fIls -l\fP\/ (second example, which is from the disk file
.File -n source-archive.tar ).
.H4 "Extracting files"
.X "tar, extracting files"
To extract a file from the archive, use the \f(CWx\fP option:
.Dx
# \f(CBtar xv usr/src/sys/Makefile\fP                   \fIfrom tape\fP\/
usr/src/sys/Makefile                            \fIconfirms that the file was extracted\fP\/
.De
As with the \f(CWc\fP option, if you don't use the \f(CWv\fP option,
.Command tar
does not list any file names.  If you omit the names of the files to extract,
.Command tar
extracts the complete archive.
.H4 "Compressed archives"
.X "tar, compressed archives"
You can combine
.Command gzip
with
.Command tar
by specifying the \f(CWz\fP option.  For example, to create the archive
.File -n source-archive.tar.gz
in compressed format, write:
.Dx
# \f(CBtar czf source-archive.tar.gz /usr/src/sys\fP
.De
You \fImust\fP\/ specify the \f(CWz\fP option when listing or extracting
compressed archives, and you must not do so when listing or extracting
non-compressed archives.  Otherwise you get messages like:
.Dx
# \f(CBtar tzvf source-archive.tar \fP
gzip: stdin: not in gzip format
tar: child returned status 1
# \f(CBtar tvf source-archive.tar.gz \fP
tar: only read 2302 bytes from archive source-archive.tar.gz
.De
.sp -1v
.H2 "Using floppy disks under FreeBSD"
.X "floppy disk"
.Pn bloody-floppies
I don't like floppy disks.  UNIX doesn't like floppy disks.  Probably you don't
like floppy disks either, but we occasionally have to live with them.
.P
FreeBSD uses floppy disks for one thing only: for initially booting the system
on systems that can't boot from CD-ROM.  We've already seen that they're
unsuitable for archival data storage and data transfer.  For this purpose,
FreeBSD uses tapes and CD-ROMs, which are much more reliable, and for the data
volumes involved in modern computers, they're cheaper and faster.
.P
So why use floppies?  The only good reasons are:
.Ls B
.LI
You have a floppy drive.  You may not have a tape drive.  Before you go out and
buy all those floppies, though, consider that it might be cheaper to buy a tape
drive and some tapes instead.
.LI
You need to exchange data with people using Microsoft platforms, or with people
who don't have the same kind of tape as you do.
.Le
In the following sections, we'll look at how to handle floppies under FreeBSD,
with particular regard to coexisting with Microsoft.  Here's an overview:
.Ls B
.LI
Always format floppies before using them on your system for the first time, even
if they've been formatted before.  We'll look at that in the next section.
.LI
Just occasionally, you need to create a UNIX file system on floppy.  We'll look
at that on page
.Sref \*[make-floppy-fs] .
.LI
When exchanging with Microsoft users, you need to create a Microsoft file system.  We'll
look at that on page
.Sref \*[DOS-fs] .
.LI
When exchanging with other UNIX users, whether FreeBSD or not, use
.Command tar
or
.Command cpio .
We'll look at how to do that on page
.Sref \*[frisbee] .
.LE
.H3 "Formatting a floppy"
.X "formatting, floppy disk"
.X "floppy disk, formatting"
.Pn fdformat
.X "FORMAT, MS-DOS program"
.X "MS-DOS program, FORMAT"
.X "low-level format"
.X "format, low-level"
.X "high-level format"
.X "format, high-level"
Even if you buy preformatted floppies, it's a good idea to reformat them.  Track
alignment can vary significantly between individual floppy drives, and the
result can be that your drive doesn't write quite on top of the pre-written
tracks.  I have seen read failure rates as high as 2% on pre-formatted floppies:
in other words, after writing 100 floppies with valuable data, the chances are
that two of them have read errors.  You can reduce this problem by reformatting
the floppy in the drive in which it is to be written, but you can't eliminate
it.
.P
.X "FORMAT, MS-DOS program"
.X "MS-DOS program, FORMAT"
.X "low-level format"
.X "high-level format"
.X "format, low-level"
.X "format, high-level"
On Microsoft platforms, you format floppies with the
.Command -n FORMAT
program, which performs two different functions when invoked on floppies: it
performs both a \fIlow-level\fP\/ format, which rewrites the physical sector
information, and then it performs what it calls a \fIhigh-level\fP\/ format,
which writes the information necessary for Microsoft platforms to use it as a
file system.  UNIX calls the second operation creating a file system.  It's not
always necessary to have a file system on the diskette\(emin fact, as we'll see,
it can be a disadvantage.  In addition, FreeBSD offers different kinds of file
system, so it performs the two functions with different programs.  In this
section, we'll look at
.Command fdformat ,
which performs the low-level format.  We'll look at how to create a \fIUFS\fP\/
or Microsoft file system in the next section.
.P
To format a diskette in the first floppy drive,
.Device fd0 ,
you would enter:
.Dx
$ \f(CBfdformat /dev/fd0\fP
Format 1440K floppy `/dev/fd0'? (y/n): \f(CBy\fP
Processing ----------------------------------------
.De
Each hyphen character (\f(CW-\fP) represents two tracks.  As the format
proceeds, the hyphens change to an \f(CBF\fP (Format) and then to \f(CBV\fP
(Verify) in turn, so at the end the line reads
.Dx
Processing VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV done.
.De
.sp -1v
.H3 "File systems on floppy"
.X "file system, floppy"
.Pn make-floppy-fs
.X "UFS"
It's possible to use floppies as file systems under FreeBSD.  You can create a
\fIUFS\fP\/ file system on a floppy just like on a hard disk.  This is not
necessarily a good idea: the \fIUFS\fP\/ file system is designed for
performance, not maximum capacity.  By default, it doesn't use the last 8% of
disk space, and it includes a lot of structure information that further reduces
the space available on the disk.  Here's an example of creating a file system,
mounting it on the directory
.Directory /A ,
and listing the remaining space available on an empty 3\(12\f(CW"\fP floppy.
Since release 5, FreeBSD no longer requires a partition table on a floppy, so
you don't need to run
.Command bsdlabel
(the replacement for the older
.Command disklabel
program).
.Dx
# \f(CBnewfs -O1 /dev/fd0\fP                            \fIcreate a new file system\fP\/
/dev/fd0: 1.4MB (2880 sectors) block size 16384, fragment size 2048
        using 2 cylinder groups of 1.00MB, 64 blks, 128 inodes.
super-block backups (for fsck -b #) at:
 32, 2080
# \f(CBmount /dev/fd0 /A\fP                             \fImount the floppy on /A\fP\/
# \f(CBdf -k /A\fP                                      \fIdisplay the space available\fP\/
Filesystem  1024-blocks     Used    Avail Capacity  Mounted on
/dev/fd0           1326        2     1218     0%    /A
.De
Let's look at this in a little more detail:
.Ls B
.LI
.Command newfs
creates the \fIUFS\fP\/ file system on the floppy.  We use the \f(CW-O1\fP flag
to force the older UFS1 format, which leaves more usable space than the default
UFS2.
.LI
We have already seen
.Command mount
on page
.Sref \*[mount] .
In this case, we use it to mount the floppy on the file system
.Directory /A .
.LI
The
.Command df
program shows the maximum and available space on a file system.
By default,
.Command df
displays usage in blocks of 512 bytes, an inconvenient size.  In this example we
use the \f(CW-k\fP option to display it in kilobytes.  You can set a default
block size via the environment variable \f(CWBLOCKSIZE\fP.  If it had been set
to 1024, we would see the same output without the \f(CW-k\fP option.  See page
.Sref \*[environment-variables] \&
for more details of environment variables.
.Le
The output of
.Command df
looks terrible!  Our floppy only has 1218 kB left for normal user data, even
though there is nothing on it and even
.Command df
claims that it can really store 1326 kB.  This is because \fIUFS\fP\/ keeps a
default of 8% of the space free for performance reasons.  You can change this,
however, with
.Command tunefs ,
the file system tune program:\*F
.X "tuna fish"
.FS
To quote the man page: \fIYou can tune a file system, but you can't tune a
fish\fP.
.FE
.Dx
# \f(CBumount /A\fP                                     \fIfirst unmount the floppy\fP\/
# \f(CBtunefs -m 0 /dev/fd0\fP                          \fIand change the minimum free to 0\fP\/
tunefs: minimum percentage of free space changes from 8% to 0%
tunefs: should optimize for space with minfree < 8%
# \f(CBtunefs -o space /dev/fd0\fP                      \fIchange the optimization\fP\/
tunefs: optimization preference changes from time to space
# \f(CBmount /dev/fd0 /A\fP                             \fImount the file system again\fP\/
# \f(CBdf /A\fP                                         \fIand take another look\fP\/
Filesystem  1024-blocks     Used    Avail Capacity  Mounted on
/dev/fd0           1326        2     1324     0%    /A
.De
Still, this is a far cry from the claimed data storage of a Microsoft disk.  In
fact, Microsoft disks can't store the full 1.4 MB either: they also need space
for storing directories and allocation tables.  The moral of the story: only use
file systems on floppy if you don't have any alternative.
.H3 "Microsoft file systems"
.X "Microsoft file systems"
.Pn DOS-fs
To create a Microsoft FAT12, FAT16 or FAT32 file system, use the
.Command newfs_msdos
command:
.Dx
$ \f(CBnewfs_msdos -f 1440 /dev/fd0\fP
.De
The specification \f(CW-f 1440\fP tells
.Command newfs_msdos
that this is a 1.4 MB floppy.
Alternatively, you can use the
.Command mformat
command:
.Dx
$ \f(CBmformat A: \fP
.De
You can specify the number of tracks with the \f(CW-t\fP option, and the number
of sectors with the \f(CW-s\fP option.  To explicitly specify a floppy with 80
tracks and 18 sectors (a standard 3\(12\f(CW"\fP 1.44 MB floppy), you could
enter:
.Dx
$ \f(CBmformat -t 80 -s 18 A:\fP
.De
.Command mformat
is one of the
.Command mtools
that we look at in the next section.
.H3 "Other uses of floppies"
.X "frisbee"
.Pn frisbee
Well, you could take the disks out of the cover and use them as a kind of
frisbee.  But there is one other useful thing you can do with floppies: as an
archive medium, they don't need a file system on them.  They just need to be
low-level formatted.  For example, to write the contents of the current
directory onto a floppy, you could enter:
.Dx
$ \f(CBtar cvfM /dev/fd0 .\fP
\&./
\&.xfmrc
\&.x6530modkey
\&.uwmrc
\&.twmrc
\&.rnsoft
\&.rnlast
\fI\&...etc\fP\/
Prepare volume #2 for /dev/fd0 and hit return:
.De
Note also the solitary dot (\f(CW.\fP) at the end of the command line.  That's
the name of the current directory, and that's what you're backing up.  Note also
the option \f(CWM\fP, which is short for \f(CW--multi-volume\fP.  There's a very
good chance that you'll run out of space on a floppy, and this option says that
you have a sufficient supply of floppies to perform the complete backup.
.P
To extract the data again, use
.Command tar
with the \f(CWx\fP option:
.Dx
$ \f(CBtar xvfM /dev/fd0\fP
\&./
\&.xfmrc
\&.x6530modkey
\&.uwmrc
\fI\&...etc\fP\/
.De
See the man page \fItar(1)\fP\/ for other things you can do with
.Command tar .
.H3 "Accessing Microsoft floppies"
.Pn mtools
.X "ATTRIB, MS-DOS program"
.X "MS-DOS program, ATTRIB"
.X "CD, MS-DOS program"
.X "MS-DOS program, CD"
.X "COPY, MS-DOS program"
.X "MS-DOS program, COPY"
.X "DEL, MS-DOS program"
.X "MS-DOS program, DEL"
.X "DIR, MS-DOS program"
.X "MS-DOS program, DIR"
.X "FORMAT, MS-DOS program"
.X "MS-DOS program, FORMAT"
.X "LABEL, MS-DOS program"
.X "MS-DOS program, LABEL"
.X "MD, MS-DOS program"
.X "MS-DOS program, MD"
.X "RD, MS-DOS program"
.X "MS-DOS program, RD"
.X "READ, MS-DOS program"
.X "MS-DOS program, READ"
.X "REN, MS-DOS program"
.X "MS-DOS program, REN"
Of course, most of the time you get data on a floppy, it's not in
.Command tar
format: it has a Microsoft file system on it.  We've already seen the Microsoft
file system type on page
.Sref \*[msdosfs] ,
but that's a bit of overkill if you just want to copy files from floppy.  In
this case, use the
.Command mtools
package from the Ports Collection.
.Command mtools
is an implementation of the MS-DOS programs
.Command -n ATTRIB ,
.Command -n CD ,
.Command -n COPY ,
.Command -n DEL ,
.Command -n DIR ,
.Command -n FORMAT ,
.Command -n LABEL ,
.Command -n MD ,
.Command -n RD ,
.Command -n READ ,
.Command -n REN ,
and
.Command -n TYPE
under UNIX.  To avoid confusion with existing utilities, the UNIX versions of
these commands start with the letter \f(CWm\fP.  They are also written in lower
case.  For example, to list the contents of a floppy and copy one of the files
to the current (FreeBSD) directory, you might enter:
.Dx
$ \f(CBmdir\fP                                          \fIlist the current directory on A:\fP\/
 Volume in drive A is MESSED OS
 Directory for A:/

IO       SYS       33430    4-09-91   5:00a
MSDOS    SYS       37394    4-09-91   5:00a
COMMAND  COM       47845   12-23-92   5:22p
NFS              <DIR>     12-24-92  11:03a
DOSEDIT  COM        1728   10-07-83   7:40a
CONFIG   SYS         792   10-07-94   7:31p
AUTOEXEC BAT         191   12-24-92  11:10a
MOUSE            <DIR>     12-24-92  11:09a
      12 File(s)      82944 bytes free
$ \f(CBmcd nfs\fP                                       \fIchange to directory A:\eNFS\fP\/
$ \f(CBmdir\fP                                          \fIand list the directory\fP\/
 Volume in drive A is MESSED OS
 Directory for A:/NFS

\&.                <DIR>     12-24-92  11:03a
\&..               <DIR>     12-24-92  11:03a
HOSTS               5985   10-07-94   7:34p
NETWORK  BAT         103   12-24-92  12:28p
DRIVES   BAT          98   11-07-94   5:24p
\&\fI...and many more\fP\/
      51 File(s)      82944 bytes free
$ \f(CBmtype drives.bat\fP                              \fItype the contents of DRIVES.BAT\fP\/
net use c: presto:/usr/dos
c:
cd \enfs
# net use f: porsche:/dos
# net use g: porsche:/usr
$ \f(CBmcopy a:hosts .\fP                               \fIcopy A:HOSTS to local UNIX directory\fP\/
Copying HOSTS
$ \f(CBls -l hosts\fP                                   \fIand list it\fP\/
-rw-rw-rw-   1 root     wheel        5985 Jan 28 18:04 hosts
.De
You must specify the drive letter to
.Command mcopy ,
because it uses this indication to decide whether the file name is a UNIX or a
Microsoft file name.  You can copy files from FreeBSD to the floppy as well, of
course.
.P
.Pn Microsoft-staircase
\fIA word of warning\fP.  UNIX uses a different text data format from Microsoft: in
UNIX, lines end with a single character, called \fBNewline\fP, and represented
by the characters \f(CW\en\fP in the C programming language.  It corresponds to
the ASCII character \fBLine Feed\fP (represented by \f(CW^J\fP).  Microsoft uses
two characters, a \fBCarriage Return\fP (\f(CW^M\fP) followed by a \fBLine
Feed\fP.  This unfortunate difference causes a number of unexpected
compatibility problems, since both characters are usually invisible on the
screen.
.P
In FreeBSD, you won't normally have many problems.  Occasionally a program
complains about non-printable characters in an input line.  Some, like
.X "emacs, command"
.X "command, emacs"
.Command -n Emacs ,
show them.  For example, Emacs shows our last file,
.File drives.bat ,
like this:
.Dx
net use c: presto:/usr/dos^M
c:^M
cd \enfs^M
# net use f: porsche:/dos^M
# net use g: porsche:/usr^M
.De
This may seem relatively harmless, but it confuses some programs, including the
C compiler and pagers like
.Command more ,
which may react in confusing ways.  You
can remove them with the \f(CW-t\fP option of
.Command mcopy \/:
.Dx
$ \f(CBmcopy -t a:drives.bat .\fP
.De
Transferring files in the other direction is more likely to cause problems.  For
example, you might edit this file under FreeBSD and then copy it back to the
diskette.  The results depend on the editor, but assuming we changed all
occurrences of the word \f(CWporsche\fP to \f(CWfreedom\fP, and then copied the
file back to the diskette, Microsoft might then find:
.Dx
C:> \f(CBtype drives.bat\fP
net use c: presto:/usr/dos
                          c:
                            cd \enfs
                                   # net use f: freedom:/dos
                                                            # net use g: freedom:/usr
.De
This is a typical result of removing the \fBCarriage Return\fP characters.  The
\f(CW-t\fP option to
.Command mcopy
can help here, too.  If you use it when copying \fIto\fP\/ a Microsoft file
system, it reinserts the \fBCarriage Return\fP characters.
