.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: draft
.\"
.\" $Id: textintro.mm,v 3.2 1999/05/14 09:16:45 grog Exp grog $
.\"
.af % i
.OF '\\*[RCS-ID]''%'
.EF '%''\\*[RCS-ID]'
.Chapter 0 "Introduction to the text version"
.so ids.mm
This document is the complete text of the third edition of ``The Complete
FreeBSD'', including all updates as of \*[formatdate].  It does not include the
man pages, which you can view more conveniently using the \fIman\fP\| program,
but they are included in the table of contents so that there are page numbers
for the cross references.  Don't use these cross reference numbers: they are
different from the page numbers in the printed book.
.P
You can best view the text in a window at least 85 characters wide and exactly
58 lines high: this is the height of the "page".  Some of the tables are wider
than 80 characters, and if you can display 100 characters width, it will work
better.
.P
``The Complete FreeBSD'' includes standard printing features such as:
.Ls B
.LI
Text of varied size and font to indicate specific meanings.  This ASCII version
can't reproduce them.  This will make it very hard to read. See the discussion
on page \*[conventions] to see how much difference this makes.
.LI
Illustrations such as screen images.  I have had to completely omit them from
this version.
.LI
Diagrams.  Most of these are still present, but the low resolution of
\fInroff\fP\| means that some would be distorted beyond recognition, for example
the monitor waveform diagrams in the chapter on X theory.  There's no good way
to do this kind of diagram in ASCII, so I have omitted them.  You may still be
able to recognize something in the diagram of the reference network on page
\*[reference-net-page], so I have left it in there.
.Le
All in all, the result is very second-rate.  So why even bother?  There are a
couple of reasons:
.Ls 
.LI
You can use programs such as \fIgrep\fP\| to search for specific texts.
.LI
It may be better than nothing if you're in trouble installing the system.
.LI
Since none of these problems exist with the printed book, it may make you decide
to buy the book :-)
.Le
One thing you should note, though: this book is copyright.  By buying the
CD-ROM, you have a license to use it for your own personal use.  You may not
give it to other people, or use the text in other documents without the prior
written approval of Walnut Creek CDROM.  If you distribute this image without
prior agreement, a royalty of $1.00 per copy shall apply.
.bp
