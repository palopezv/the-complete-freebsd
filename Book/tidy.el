(defun tidy ()
  (interactive)
  (beginning-of-buffer)
  (replace-regexp "^\\.NH.*
" "" nil)
  (beginning-of-buffer)
  (replace-regexp "\\.P
\\.P" ".P" nil)
  (beginning-of-buffer)
  (replace-regexp "\\.P
.H" ".H" nil)
  (beginning-of-buffer)
  (replace-regexp ".br
.po 0.75i
.ll 6.0i
.ft C
.LP
.DS L
.ft R
.eo
" ".Ds
" nil)
  (beginning-of-buffer)
  (replace-regexp ".ft P
.LP
.br
.po 0.25i
.ll 7.0i
.ft R
" "" nil)
  (beginning-of-buffer)
  (replace-regexp "^\\.nr.*
" "" nil)
  (beginning-of-buffer)
  (replace-regexp ".br
.po 0.75i
.ll 6.0i
.LP
" "" nil)
  (beginning-of-buffer)
  (replace-regexp "\\.IP " ".H3 " nil)
  (beginning-of-buffer)
  (replace-regexp "^\\(\\.H.*\\)
\\.P" "\\1" nil)
  (beginning-of-buffer)
  (replace-regexp "^\\(\\.H. \"\\)\\\\f.\\(.*\\)\\\\f." "\\1\\2" nil)
  (beginning-of-buffer)
  (replace-regexp ".br
.po 0.25i
.ll 7.0i
.LP
" "" nil)
  (beginning-of-buffer)
  (replace-string "\\*h
.XS \\n%
\\*(SN \\*h
.XE
.P
" "" nil)
  (beginning-of-buffer)
  (replace-string "\\*h
.XS \\n%
\\*(SN \\*h
" "" nil)  
  (beginning-of-buffer)
  (replace-string ".LP
" "" nil)
  (beginning-of-buffer) 
  (replace-string "
.DE" "
.De" nil)
  (beginning-of-buffer)
  (replace-string ".P
.H" ".H" nil)
  (beginning-of-buffer)
  (replace-string "
.ec
" "
" nil)
  (beginning-of-buffer)
  (replace-regexp "^\\.RS
" ".LB 10 0 \\& \\0 1 1" nil)
  (beginning-of-buffer)
  (replace-string "
.RE" "
.Le" nil)
  (beginning-of-buffer)
  (replace-regexp "\\.De
\\.P" ".De" nil)
  (beginning-of-buffer)
  (replace-regexp "\\.De

" ".De
" nil)  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (beginning-of-buffer)
  (replace-regexp "^\\.ds h \\(.*\\)$" ".H2 \"\\1\"" nil) )

(defun demenu ()
  (interactive)
  (let (here (point))
    (query-replace-regexp "^[^\"]+\"" "" nil)
    (goto-char here)
    (query-replace-regexp "\"[^\"]+$" "" nil)
    (goto-char here)
    (query-replace-regexp "\".*\"" "	" nil)
    (goto-char here) ) )

(defun move-indices ()
  (interactive)
  "Starting at point, find the next index hit (.X) and move it up to the
next troff command."
  (if (re-search-forward "^\\.X " nil t)
      (progn
	(beginning-of-line)
	(kill-line 1)
	(re-search-backward "^\\." nil t)
	(forward-line)
	(yank) ) ) )

(global-set-key (quote [M-f2]) (quote move-indices))

(defun new-tables ()
  (interactive)
  (beginning-of-buffer)
  (query-replace-regexp "^=$" "_" nil nil nil)
  (beginning-of-buffer)
  (query-replace-regexp "^\\.TE" "_
.TE" nil nil nil)
  (beginning-of-buffer)
  (query-replace-regexp "box, *center," "" nil nil nil) )

(defun ref-net nil
  (interactive)
  (save-excursion 
    (beginning-of-buffer)
    (replace-string "echunga" "freebie" nil nil nil)
    (beginning-of-buffer)
    (replace-string "battunga" "presto" nil nil nil)
    (beginning-of-buffer)
    (replace-string "zaphod" "bumble" nil nil nil)
    (beginning-of-buffer)
    (replace-string "gregl1.lnk.telstra" "exorg-gw.example.***" nil nil nil)
    (beginning-of-buffer)
    (replace-string "Cont0.way3.Adelaid" "free-gw.example.ne***" nil nil nil)
    (beginning-of-buffer)
    (replace-string "192.109.197" "223.147.37" nil nil nil)
    (beginning-of-buffer)
    (replace-string "telstra" "example" nil nil nil)
    (beginning-of-buffer)
    (replace-string "sydney" "andante" nil nil nil) ) )

(defun fixfiles ()
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (query-replace-regexp "\\\\fI\\([/~][^\\]+\\)\\\\fP\\([^\\]\\) *" "
.File \\1 \\2
" nil nil nil)
    
    (beginning-of-buffer)
    (query-replace-regexp "\\\\fI\\([/~][^\\]+\\)\\\\fP\\\\. *" "
.File \\1
" nil nil nil)
    ) )

(defun fixcommands ()
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (query-replace-regexp "\\\\fI\\([^\\]+\\)\\\\fP\\([^\\]\\) *" "
.Command \\1 \\2
" nil nil nil)
    
    (beginning-of-buffer)
    (query-replace-regexp "\\\\fI\\([^\\]+\\)\\\\fP\\\\. *" "
.Command \\1
" nil nil nil) 
    (beginning-of-buffer)
    (query-replace-regexp "^\\.X \"/.*
" "" nil nil nil)
    ) )

(defun dosref ()
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (query-replace-regexp " *\\\\\\*\\[" "
.Sref \\\\*[" nil nil nil)
    (beginning-of-buffer)
;; Replace trailing space by a \n
    (query-replace-regexp "\\(.Sref.*\\]\\) +" "\\1
" nil nil nil)
    (beginning-of-buffer)
;; Insert a space after the sref
    (query-replace-regexp "\\(.Sref.*\\]\\)\\([^ 
]+\\) *" "\\1 \\2
" nil nil nil)
    (beginning-of-buffer)
;; Add a \\& as default second parameter
    (query-replace-regexp "\\(.Sref.*\\]\\)$" "\\1 \\\\&" nil nil nil) 
    (beginning-of-buffer)
;; Put "" round chapter names
    (query-replace-regexp "\\([^\"]\\)\\(\\\\\\*\\[ch.*\\]\\)" "\\1\"\\2\"" nil nil nil)
) )
