.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: unixadmin.mm,v 4.14 2003/10/09 06:06:31 grog Exp grog $
.\"
.Chapter \*[nchunixadmin] "Taking control"
In Chapter
.Sref \*[nchunixref] \&
we saw the basics of working with FreeBSD.  In this part of the book, we'll look
at some more system-specific issues.  This chapter discusses the following
topics:
.Ls B
.LI
UNIX is a multi-user operating system.  We've already skimmed over creating user
accounts, but on page
.Sref \*[more-users-and-groups] \&
we'll look at it in more detail.
.LI
Not all users are created equal.  In particular, the system administration login
\f(CWroot\fP has power over all other users.  We'll look at \f(CWroot\fP on page
.Sref \*[root] .
.LI
UNIX implements multi-tasking via a mechanism called \fIprocesses\fP.  We'll
look at them on page
.Sref \*[processes] .
.LI
Timekeeping is extremely important in a networking system.  If your system has
the wrong time, it can cause all sorts of strange effects.  On page
.Sref \*[timekeeping] \&
we'll look at how to ensure that your system is running the correct time.
.LI
A number of events are of interest in keeping a machine running smoothly.  The
system can help by keeping track of what happens.  One mechanism for this is
\fIlog files\fP, files that contain information about what has happened on the
machine.  We'll look at them on page
.Sref \*[logfiles] .
.LI
On page
.Sref \*[smp] ,
we'll look at how FreeBSD handles systems with more than one processor.  This is
also called \fISymmetrical Multi-Processor\fP\/ or \fISMP\fP\/ support.
.LI
Nearly every modern laptop has as special bus for plugin cards.  It used to be
called \fIPCMCIA\fP, an acronym for the rather unlikely name \fIPersonal
Computer Memory Card International Association\fP.  Nowadays it's called
\fIPC Card\fP.  It was later upgraded to a 32 bit bus called \fICardBus\fP.
We'll look at how FreeBSD supports PC Card and CardBus on page
.Sref \*[pccard] .
.LI
Starting on page
.Sref \*[emulate] ,
we'll look at FreeBSD's support for emulating other operating systems.
.LI
Other aspects of FreeBSD are so extensive that we'll dedicate separate chapters
to them.  We'll look at them in Chapters
.Sref "\*[nchports]" \&
to
.Sref "\*[nchprinters]" .
.LI
Starting and stopping the system is straightforward, but there are a surprising
number of options.  Many of them are related to networking, so Chapter
.Sref "\*[nchstarting]" \&
is located after the networking section.
.Le
.SPUP
.H2 "Users and groups"
.Pn more-users-and-groups
We've already looked at users in Chapter
.Sref "\*[nchunixref]" .
In this chapter, we'll take a deeper look.
.Pn master-passwd
.P
In traditional UNIX, information about users was kept in the file
.File /etc/passwd .
As the name suggests, it included the passwords, which were stored in encrypted
form.  Any user could read this file, but the encryption was strong enough that
it wasn't practical to decrypt the passwords.  Nowadays processors are much
faster, and it's too easy to crack a password.  As a result, FreeBSD keeps the
real information in a file called
.File /etc/master.passwd ,
and for performance reasons it also makes it available in database form in
.File /etc/pwd.db
and
.File /etc/spwd.db .
None of these file are user-readable.
.File /etc/passwd
remains for compatibility reasons: some third-party programs access it directly
to get information about the environment in which they are running.
.H3 "Choosing a user name"
.Pn logging-in
.X "logging in"
So what user name do you choose?  User names are usually related to your real
name and can be up to eight characters long.  Like file names, they're
case-sensitive.  By convention, they are in all lower case, even when they
represent real names.  Typical ways to form a user name are:
.Ls B
.LI
First name.  In my personal case, this would be \f(CWgreg\fP.
.LI
Last name (\f(CWlehey\fP).
.LI
First name and initial of last name (\f(CWgregl\fP).
.LI
Initial of first name, and last name (\f(CWglehey\fP).
.LI
.X "GPL"
Initials (\f(CWgpl\fP).
.LI
Nickname (for example, \f(CWgrog\fP).
.Le
I choose the last possibility, as we will see in the following discussion.
.H3 "Adding users"
.X "adding users"
.X "user, adding"
.Pn adding-user
.X "Schneider, Wolfram"
.X "Lehey, Yana"
We've already seen how to use
.Command sysinstall
to create a user.  It's not the only way.  There are at least two other methods.
One is the program
.Command adduser \/:
.Dx
# \f(CBadduser\fP
Use option ``-verbose'' if you want see more warnings & questions
or try to repair bugs.

Enter username [a-z0-9]: \f(CByana\fP
Enter full name []: \f(CBYana Lehey\fP
Enter shell bash csh date no sh [bash]:         \fIaccept the default\fP\/
Uid [1000]:                                     \fIaccept the default\fP\/
Enter login class: default []:                  \fIaccept the default\fP\/
Login group yana [yana]: \f(CBhome\fP
Login group is ``home''. Invite yana into other groups: \f(CBno\fP
[no]: \f(CBwheel\fP                                     \fIto be able to use su\fP\/
Enter password []:                              \fIno echo\fP\/
Enter password again []:                        \fIno echo\fP\/

Name:     yana
Password: ****
Fullname: Yana Lehey
Uid:      1000
Gid:      1001 (home)
Class:
Groups:   home wheel
HOME:     /home/yana
Shell:    /bin/bash
OK? (y/n) [y]:                                  \fIaccept the default\fP\/
Added user ``yana''
Add another user? (y/n) [y]: \f(CBn\fP
.De
.X "Lehey, Norman"
.Pn vipw
.P
An alternative way of adding or removing users is with the
.Command vipw
program.  This is a more typical UNIX-hackish approach:
.Command vipw
starts your favourite editor and allows you to edit the contents of the file
.File /etc/master.passwd .
After you have finished, it checks the contents and rebuilds the password
database.  Figure
.Sref \*[vipw-screen] \&
shows an example.
.PIC "images/vipw.ps" 5i
.Figure-heading "vipw display"
.Fn vipw-screen
You might be wondering why would you ever want to do things this way, and you
might find it funny that most experienced UNIX administrators prefer it.  The
reason is that you get more of an overview than with a peephole approach that
graphical environments give you, but of course you need to understand the format
better.  It's less confusing once you know that each line represents a single
user, that the lines are divided into \fIfields\fP\/ (which may be empty), and
that each field is separated from the next by a colon (\f(CW:\fP).  Table
.Sref \*[master.passwd-format] \&
describes the fields you see on the line on which the cursor is positioned.  You
can read more about the format of
.File /etc/master.passwd
in the man page \fIpasswd(5)\fP.
.Pn user-class
.Table-heading "/etc/master.passwd format"
.TS
tab(#) ;
lfCWp9w15 | lw57  .
\s10\fBField#\fBMeaning
_
yvonne#T{
User name.
T}
.sp .3v
(gibberish)#T{
Encrypted password.  When adding a new user, leave this field empty and add it
later with the
.Command passwd
program.
T}
.sp .3v
1005#T{
User number.
T}
.sp .3v
1001#T{
Group number.
T}
.sp .3v
(empty)#T{
Login class, which describes a number of parameters for the user.  We'll look at
it in Chapter
.Sref "\*[nchstarting]" ,
on page
.Sref \*[login-class] .
This field is not included in
.File /etc/passwd .
T}
.sp .3v
0#T{
Password change time.  If non-0, it is the time in seconds after which the
password must be changed.  This field is not included in
.File /etc/passwd .
T}
.sp .3v
0#T{
Account expiration time.  If non-0, it is the time in seconds after which the
user expires.  This field is not included in
.File /etc/passwd .
T}
.sp .3v
.X "Lehey, Yvonne"
.X "gecos"
T{
Yvonne\ Lehey\fP\/
T}#T{
The so-called \fIgecos\fP\/ field, which describes the user.  This field is used
by a number of programs, in particular mail readers, to extract the real name of
the user.
T}
.sp .3v
/home/yvonne#T{
The name of the home directory.
T}
.sp .3v
/bin/bash#T{
The shell to be started when the user logs in.
T}
.TE
.Fn master.passwd-format
.SPUP
.H2 "The super user"
.X "super user"
.Pn root
FreeBSD has a number of privileged users for various administration functions.
Some are just present to be the owners of particular files, while others, such
as \f(CWdaemon\fP and \f(CWuucp\fP, exist to run particular programs.  One user
stands above all others, however: \f(CWroot\fP may do just about anything.  The
kernel gives \f(CWroot\fP special privileges, and you need to become
\f(CWroot\fP to perform a number of functions, including adding other users.
Make sure \f(CWroot\fP has a password if there is any chance that other people
can access your system (this is a must if you have any kind of dialup access).
Apart from that, \f(CWroot\fP is a user like any other, but to quote the man
page \fIsu(1)\fP:
.br
.ne 1i
.Quote
\fIBy default (unless the prompt is reset by a startup file) the super user
prompt is set to \f(CW#\fP to remind one of its awesome power.
.End-quote
.H3 "Becoming super user"
.X "becoming super user"
.X "super user, becoming"
Frequently when you're logged in normally, you want to do something that
requires you to be \f(CWroot\fP.  You can log out and log in again as
\f(CWroot\fP, of course, but there's an easier way:
.Dx
$ \f(CBsu\fP                                    \fIbecome super user\fP\/
Password:                               \fIas usual, it doesn't echo\fP\/
#                                       \fIroot prompt\fP\/
.De
To use
.Command su ,
you must be a member of the group \f(CWwheel\fP.  Normally you do this when you
add the user, but otherwise just put the name of the user at the end of the line
in
.File /etc/group \/:
.Dx
wheel:*:0:root\f(CB,grog\fP                     \fIadd the text in bold face\fP\/
.De
.SPUP
.Aside
BSD treats
.Command su
somewhat differently from System V.  First, you need to be a member of the group
\f(CWwheel\fP, and secondly BSD gives you more of the super user environment
than System V.  See the man page for further information.
.End-aside
.P
Having a single \f(CWroot\fP password is a security risk on a system where
multiple people know the password.  If one of them leaves the project, you need
to change the password.  An alternative is the
.Command sudo
port
.Directory ( /usr/ports/security/sudo ).
It provides fine-grained access to \f(CWroot\fP privileges, all based on the
user's own password.  Nobody needs to know the \f(CWroot\fP password.  If a user
leaves, you just remove his account, and that cancels his access.
.H3 "Adding or changing passwords"
.X "adding passwords"
.X "changing passwords"
.X "password, adding"
.X "password, changing"
If your system has any connection with the outside world, it's a good idea to
change your password from time to time.  Do this with the
.Command passwd
program.  The input doesn't look very interesting:
.Dx
$ \f(CBpasswd\fP
Changing local password for yana.
Old password:                           \fIdoesn't echo\fP\/
New password:                           \fIdoesn't echo\fP\/
Retype new password:                    \fIdoesn't echo\fP\/
passwd: rebuilding the database...
passwd: done
.De
You have to enter the old password to make sure that some passer-by doesn't
change it for you while you're away from your monitor, and you have to enter the
new password twice to make sure that you don't mistype and lock yourself out of
your account.  If this does happen anyway, you can log in as \f(CWroot\fP and
change the password: \f(CWroot\fP doesn't have to enter the old password, and it
can change anybody's password.  For example:
.Dx
# \f(CBpasswd yana\fP
Changing local password for yana.
New password:                           \fIdoesn't echo\fP\/
Retype new password:                    \fIdoesn't echo\fP\/
passwd: rebuilding the database...
passwd: done
.De
In this case, you specify the name of the user for whom you change the password.
.P
.X "root"
If you are changing the \f(CWroot\fP password, be careful: it's easy enough to
lock yourself out of the system if you mess things up, which could happen if,
for example, you mistyped the password twice in the same way (don't laugh, it
happens).  If you're running X, open another window and use
.Command su
to become \f(CWroot\fP.  If you're running in character mode, select another
virtual terminal and log in as \f(CWroot\fP there.  Only when you're sure you
can still access \f(CWroot\fP should you log out.
.P
If you \fIdo\fP\/ manage to lose the \f(CWroot\fP password, all may not be lost.
Reboot the machine to single-user mode (see page
.Sref \*[single-user] ),
and enter:
.Dx
# \f(CBmount -u /\fP                            \fImount root file system read/write\fP\/
# \f(CBmount /usr\fP                            \fImount /usr file system (if separate)\fP\/
# \f(CBpasswd root\fP                           \fIchange the password for \f(CWroot\f(CW
Enter new password:
Enter password again:
# \f(CB^D\fP                                    \fIenter ctrl-D to continue with startup
.De
If you have a separate
.Directory /usr
file system (the normal case), you need to mount it as well, since the
.Command passwd
program is in the directory
.Directory /usr/bin .
Note that you should explicitly state the name \f(CWroot\fP: in single-user
mode, the system doesn't have the concept of user IDs.
.H2 "Processes"
.X "process, defined"
.Pn processes
.Pn ps-command
.X "process, ID"
.X "PID"
As we have seen, UNIX is a multi-user, multi-tasking operating system.  In
particular, you can run a specific program more than once.  We use the term
\fIprocess\fP\/ to refer to a particular instance of a running program.  Each
process is given a \fIprocess ID\fP, more frequently referred to as \fIPID\fP, a
number between 0 and 99999 that uniquely identifies it.  There are many things
that you might like to know about the processes that are currently running,
such as:
.Ls B
.LI
How many processes are running?
.LI
Who is running the processes?
.LI
Why is the system so slow?
.LI
Which process is blocking my access to the modem?
.Le
.X "process, status"
Your primary tool for investigating process behaviour is the
.Command ps
(\fIprocess status\fP\/) command.  It has a large number of command options, and
it can tell you a whole lot of things that you will only understand when you
have investigated how the kernel works, but it can be very useful for a number
of things.  Here are some typical uses:
.H3 "What processes do I have running?"
After starting a large number of processes in a number of windows under X, you
probably can't remember what is still running.  Maybe processes that you thought
had stopped are still running.  To display a brief summary of the processes you
have running, use the
.Command ps
command with no options:
.Dx
$ \f(CBps\fP
  PID  TT  STAT      TIME COMMAND
  187  p0  Is+    0:01.02 -bash (bash)
  188  p1  Ss     0:00.62 -bash (bash)
  453  p1  R+     0:00.03 ps
.De
This display shows the following information:
.Ls B
.LI
The PID of the process.
.LI
.X "controlling terminal"
.X "teletype"
\f(CWTT\fP is short for \fIteletype\fP, and shows the last few letters of the
name of the \fIcontrolling terminal\fP, the terminal on which the process is
running.  In this example, the terminals are
.Device ttyp0
and
.Device ttyp1 .
.LI
\f(CWSTAT\fP shows the current process status.  It's involved and requires a
certain amount of understanding of how the kernel runs to interpret it\(emsee
the man page for
.Command ps
for more details.
.LI
\f(CWTIME\fP is the CPU time that the process has used in minutes, seconds and
hundredths of a second.  Note that many other UNIX systems, particularly System
V, only show this field to the nearest second.
.LI
\f(CWCOMMAND\fP is normally the command you entered, but don't rely on this.  In
the next section, you'll see that
.Daemon sendmail
has changed its \f(CWCOMMAND\fP field to tell you what it is doing.  You'll
notice that the command on the last line is the
.Command ps
that performs the listing.  Due to some complicated timing issue in the kernel,
this process may or may not appear in the listing.
.Le
.SPUP
.H3 "What processes are running?"
There are many more processes in the system than the list above shows.  To show
them all, use the \f(CWa\fP option to
.Command ps .
To show daemons as well (see the next section for a definition of \fIdaemon\fP\/),
use the \f(CWx\fP option.  To show much more detail, use the \f(CWu\fP or
\f(CWl\fP options.  For example:
.Dx
$ \f(CBps waux\fP
.\" This doesn't fit on the page in the standard point size.  Reduce.
.\" Should do now. .ps 6000u
USER      PID %CPU %MEM   VSZ  RSS  TT  STAT STARTED      TIME COMMAND
root       12 95.7  0.0     0   12  ??  RL    1Jan70 1406:43.85  (idle: cpu0)
root       11 95.1  0.0     0   12  ??  RL    1Jan70 1406:44.64  (idle: cpu1)
root        1  0.0  0.0   708   84  ??  ILs   1Jan70   0:09.10 /sbin/init --
root       12  0.0  0.0     0   12  ??  WL    1Jan70  15:04.95  (swi1: net)
root       13  0.0  0.0     0   12  ??  WL    1Jan70  21:30.29  (swi6: tty:sio clock)
root       15  0.0  0.0     0   12  ??  DL    1Jan70   2:17.27  (random)
root       18  0.0  0.0     0   12  ??  WL    1Jan70   0:00.00  (swi3: cambio)
root       20  0.0  0.0     0   12  ??  WL    1Jan70   0:00.00  (irq11: ahc0 uhci0++)
root       21  0.0  0.0     0   12  ??  WL    1Jan70  39:00.32  (irq5: rl0)
root       22  0.0  0.0     0   12  ??  WL    1Jan70   7:12.92  (irq14: ata0)
root       23  0.0  0.0     0   12  ??  WL    1Jan70   0:47.99  (irq15: ata1)
root       24  0.0  0.0     0   12  ??  DL    1Jan70   0:00.08  (usb0)
root       25  0.0  0.0     0   12  ??  DL    1Jan70   0:00.00  (usbtask)
root       26  0.0  0.0     0   12  ??  DL    1Jan70   0:00.07  (usb1)
root       27  0.0  0.0     0   12  ??  DL    1Jan70   0:00.08  (usb2)
root      340  0.0  0.1  1124  280  ??  S    18Dec02  16:41.11 nfsd: server (nfsd)
root      375  0.0  0.0  1192   12  ??  Ss   18Dec02   0:01.70 /usr/sbin/lpd
daemon    408  0.0  0.0  1136  152  ??  Ss   18Dec02   0:11.41 /usr/sbin/rwhod
root      420  0.0  0.1  2648  308  ??  Ss   18Dec02   0:04.20 /usr/sbin/sshd
root      491  0.0  0.1  2432  368  ??  Ss   18Dec02   0:38.61 /usr/local/sbin/httpd
root      551  0.0  0.0  1336   12  ??  Ss   18Dec02   0:02.71 /usr/sbin/inetd -wW
root      562  0.0  0.0  1252  216  ??  Is   18Dec02   0:15.50 /usr/sbin/cron
root      572  0.0  0.0  1180    8  v2  IWs+ -         0:00.00 /usr/libexec/getty Pc
www       582  0.0  0.0  2432    8  ??  IW   -         0:00.00 /usr/local/sbin/httpd
grog      608  0.0  0.1  1316  720  v0  I    18Dec02   0:00.04 -bash (bash)
root     2600  0.0  0.0  1180    8  v1  IWs+ -         0:00.00 /usr/libexec/getty Pc
root    33069  0.0  0.3  5352 1716  ??  Ss   29Dec02   0:01.30 xterm -name xterm
grog    33081  0.0  0.1  1328  752  p8  Is+  29Dec02   0:00.09 /usr/local/bin/bash
.De
This list is just an excerpt.  Even on a freshly booted system, the real list of
processes will be much larger, about 50 processes.
.P
We've seen a number of these fields already.  The others are:
.Ls B
.LI
.X "real user ID"
\f(CWUSER\fP is the \fIreal user ID\fP\/ of the process, the user ID of the
person who started it.
.LI
\f(CW%CPU\fP is an approximate count of the proportion of CPU time that the
process has been using in the last few seconds.  This is the column to examine
if things suddenly get slow.
.LI
\f(CW%MEM\fP is an approximate indication of the amount of physical memory that
the process is using.
.LI
\f(CWVSZ\fP (\fIvirtual size\fP\/) is the amount of virtual memory that the
process is using, measured in kilobytes.
.LI
\f(CWRSS\fP (\fIresident segment size\fP\/) is the amount of physical memory
currently in use, measured in kilobytes.
.LI
\f(CWSTARTED\fP is the time or date when the process was started.
.Le
In addition, a surprising number of processes don't have a controlling
terminal.  They are \fIdaemons\fP, and we'll look at them in the next
section.
.H2 "Daemons"
.X daemon
.Pn daemon
A significant part of the work in a FreeBSD system is performed by
\fIdaemons\fP.  A daemon is not just the BSD mascot described on page
.Sref \*[bsdd] \(emit's
also a process that goes around in the background and does routine work such as
sending mail
.Daemon ( sendmail ),
handling incoming Internet connections
.Daemon ( inetd ),
or starting jobs at particular times
.Daemon ( cron ).
.Aside
To quote the \fIOxford English Dictionary\fP\/: \fBDemon\fP Also \fBd�mon\fP. ME
[In form, and in sense I, a. L. \fId�mon\fP (med. L. \fIdemon\fP\/)...]  1a. In
ancient Greek mythology (= \fI\(*d\(*a\h'.05m'\(*i\h"-.03i"\'\(*m\(*w\(*n\fP\/):
A supernatural being of a nature intermediate between that of gods and men, an
inferior divinity, spirit, genius (including the souls of deceased persons,
\fIesp\fP\/ deified heros).  Often written \fId�mon\fP\/ for distinction.
.End-aside
.ne 3v
You can recognize daemons in a \f(CWps waux\fP listing by the fact that they
don't have a controlling terminal\(eminstead you see the characters \f(CW??\fP.
Each daemon has a man page that describes what it does.
.P
Normally, daemons are started when the system is booted and run until the system
is stopped.  If you stop one by accident, you can usually restart them.  One
exception is
.Daemon init ,
which is responsible for starting other processes.  If you kill it, you
effectively kill the system.  Unlike traditional UNIX systems, FreeBSD does not
allow
.Daemon init
to be killed.
.H3 "cron\/"
.X "cron"
.Pn cron
One of the more useful daemons is
.Daemon cron ,
named after Father Time.
.Daemon cron
performs functions at specific times.  For example, the system runs the script
.File /etc/periodic/daily
every day at 2:00 am, the script
.File /etc/periodic/weekly
every Saturday at 3:30 am, and the script
.File /etc/periodic/monthly
on the first day of every month at 5:30 am.
.P
.X "crontab"
To tell
.Daemon cron
to perform a function at a particular time, you need a file called a
.File crontab .
The system keeps the real
.File crontab
where you can't get at it, but you can keep a copy.  It's a good idea to call it
.File crontab
as well.
.P
Let's look at the format of the default system \fIcrontab\fP, located in
.File /etc/crontab \/:
.Dx
# /etc/crontab - root's crontab for FreeBSD
#
# $I\&d: crontab,v 1.10 1995/05/27 01:55:21 ache Exp $
# From: I\&d: crontab,v 1.6 1993/05/31 02:03:57 cgd Exp
#
SHELL=/bin/sh
PATH=/etc:/bin:/sbin:/usr/bin:/usr/sbin
HOME=/var/log
#
#minute hour    mday    month   wday    who     command
#
*/5     *       *       *       *       root    /usr/libexec/atrun
#
# rotate log files every hour, if necessary
#0      *       *       *       *       root    /usr/bin/newsyslog
#
# do daily/weekly/monthly maintenance
0       2       *       *       *       root    /etc/daily 2>&1
30      3       *       *       6       root    /etc/weekly 2>&1
30      5       1       *       *       root    /etc/monthly 2>&1
#
# time zone change adjustment for wall cmos clock,
# See adjkerntz(8) for details.
1,31    0-4     *       *       *       root    /sbin/adjkerntz -a
.De
As usual, lines starting with \f(CW#\fP are comments.  The others have seven
fields.  The first five fields specify the minute, the hour, the day of the
month, the month, and the day of the week on which an action should be
performed.  The character \f(CB*\fP means ``every.''  Thus, \f(CW0 2 * * *\fP
(for
.File /etc/daily \/)
means ``0 minutes, 2 o'clock (on the 24 hour clock), every day of the month,
every month, every weekday.''
.P
Field number six is special: it only exists in
.File /etc/crontab ,
not in private
.File crontab s.
It specifies the user for whom the operation should be performed.  When you
write your own
.File crontab
file, don't use this field.
.P
The remaining fields define the operation to be performed.
.Daemon cron
doesn't read your shell initialization files.  In particular, this can mean that
it won't find programs you expect it to find.  It's a good idea to put in
explicit \f(CWPATH\fP definitions, or specify an absolute pathname for the
program, as is done in this example.
.Daemon cron
mails the output to you, so you should check \f(CWroot\fP's mail from time to
time.
.P
To install or list a \fIcrontab\fP, use the
.File crontab
program:
.Dx
$ \f(CBcrontab crontab\fP                       \fIinstall a crontab\fP\/
$ \f(CBcrontab -l\fP                            \fIlist the contents of an installed crontab\fP\/
# DO NOT EDIT THIS FILE - edit the master and reinstall.
# (crontab installed on Wed Jan  1 15:15:10 1997)
# (Cron version -- $\&Id: crontab.c,v 1.7 1996/12/17 00:55:12 pst Exp $)
0 0 * * * /home/grog/Scripts/rotate-log
.De
.H3 "Processes in FreeBSD Release 5"
Some of the processes in the example above are specific to FreeBSD Release 5:
.Ls B
.LI
FreeBSD Release 5 has an \fIidle process\fP to use up the excess processor time
and perform certain activities needed when no process is active.  This example
machine has two processors, so there are two of them:
.Dx
root  12 95.7  0.0  0   12  ??  RL    1Jan70 1406:43.85  (idle: cpu0)
root  11 95.1  0.0  0   12  ??  RL    1Jan70 1406:44.64  (idle: cpu1)
.De
.LI
A number of the processes have names starting with \f(CWirq\fP or \f(CWswi\fP:
.Dx
root  12  0.0  0.0    0   12  ??  WL    1Jan70  15:04.95  (swi1: net)
root  13  0.0  0.0    0   12  ??  WL    1Jan70  21:30.29  (swi6: tty:s
root  18  0.0  0.0    0   12  ??  WL    1Jan70   0:00.00  (swi3: cambi
root  20  0.0  0.0    0   12  ??  WL    1Jan70   0:00.00  (irq11: ahc0
root  21  0.0  0.0    0   12  ??  WL    1Jan70  39:00.32  (irq5: rl0)
root  22  0.0  0.0    0   12  ??  WL    1Jan70   7:12.92  (irq14: ata0)
root  23  0.0  0.0    0   12  ??  WL    1Jan70   0:47.99  (irq15: ata1)
.De
These processes handle hardware interrupts (\f(CWirq\fP) or software interrupts
(\f(CWswi\fP).  The text which follows gives an idea of which devices or
software services they support.
.Le
.SPUP
.H3 "top"
.X "top"
Another tool for investigating system performance is
.Command top ,
which shows a number of performance criteria, including the status of the
processes are using the most resources.  Start it with the number of
processes you want displayed.  Figure
.Sref \*[top] \&
gives an example.
.br
.ne 10v
.Dx
$ \f(CBtop -S 10\fP
last pid:  3992; load averages:  0.59, 0.17, 0.06 up 0+23:54:49  17:25:13
87 processes:  3 running, 73 sleeping, 8 waiting, 3 lock
CPU states: 10.2% user, 0.0% nice, 18.8% system, 1.7% interrupt, 69.4% idle
Mem: 43M Active, 36M Inact, 31M Wired, 7460K Cache, 22M Buf, 2996K Free
Swap: 512M Total, 512M Free

 PID USER  PRI NICE   SIZE   RES STATE  C   TIME   WCPU    CPU COMMAND
  12 root  -16    0     0K   12K RUN    0  23.7H 55.32% 55.32% idle: cpu0
  11 root  -16    0     0K   12K CPU1   1  23.7H 54.49% 54.49% idle: cpu1
2854 grog   97    0  4940K 3932K *Giant 1   0:04  3.88%  3.86% xterm
  20 root  -64 -183     0K   12K WAIT   1   0:08  0.83%  0.83% irq14: ata0
2925 root   96    0   712K  608K select 1   0:01  0.15%  0.15% make
3193 grog   96    0  2220K 1304K CPU0   0   0:01  0.15%  0.15% top
3783 root   96    0   520K  416K select 1   0:00  0.10%  0.05% make
 167 root   96    0 13876K 2112K select 0   1:02  0.00%  0.00% xcpustate
  25 root  -68 -187     0K   12K WAIT   0   0:28  0.00%  0.00% irq9: xl0
 110 root   96    0  1528K  956K select 1   0:26  0.00%  0.00% ntpd
.De
.SPUP
.Figure-heading "top display"
.Fn top
.P
By default, the display is updated every two seconds and contains a lot of
information about the system state:
.Ls B
.LI
The first line gives information about the last PID allocated (you can use this
to follow the number of processes being created) and the \fIload average\fP, which
gives information about how many processes are waiting to be scheduled.
.LI
The next line gives an overview of process statistics, and in what state they
are.  A process waits for external events to complete; it waits on a lock if
some other process has a kernel resource which it wants.
.LI
The third line shows the percentage of time used in user mode, in system
(kernel) mode and by interrupts.
.LI
The fourth line shows memory usage.
.LI
The fifth line shows swap statistics.  When swapping activity occurs, it also
appears on this line.
.LI
The remaining lines show the ten most active processes (because the parameter 10
was specified on the command line).  The \f(CW-S\fP option tells
.Command top
to include system processes, such as the idle and the interrupt processes.  The
state can be:
.Ls B
.LI
\f(CWRUN\fP, when the process is waiting for a processor to run on.
.LI
\f(CWCPU0\fP or \f(CWCPU1\fP, when the process is actively executing.
.LI
\f(CW*\f(CIlock\fR, where \f(CIlock\fP\/ is the name of a kernel lock.  In this
example, the
.Command xterm
is waiting on the lock \f(CWGiant\fP.
.LI
A \fIwait string\fP, which indicates an event on which the process is waiting.
.Le
.Le
See the man page \fItop(1)\fP\/ for more details.
.H2 "Stopping processes"
.X "stopping processes"
.X "process, stopping"
.Pn stopping-processes
Sometimes you may find that you want to stop a currently running process.  There
are a number of ways to do this, but the easiest are:
.Ls B
.LI
If the process is running on a terminal, and it's accepting input, hitting the
EOF key (usually \fBCtrl-D\fP) will often do it.
.LI
If EOF doesn't do it, try the INTR key (usually \fBCtrl-C\fP).
.LI
If the process is ignoring INTR, or if it is not associated with a terminal, use
the
.Command kill
command.  For example, to find who is using all the CPU time, use
.Command ps
and look at the \f(CW%CPU\fP field:
.Dx
# \f(CBps waux | grep cron\fP
root   105  97.3  1.1  236  340  ??  Is    9:11AM 137:14.29 cron
.De
Here,
.Daemon cron
is using 97% of the CPU time, and has accumulated over 2 hours of CPU time since
this morning.  It's obviously sick, and we should put it out of its misery.  To
stop it, enter:
.Dx
# \f(CBkill 105\fP
.De
This command sends a signal called \f(CWSIGTERM\fP (terminate) to the process.
This signal gives the process time to tidy up before exiting, so you should
always try to use it first.  The 105 is
.Daemon cron 's
PID, which we got from the
.Command ps
command.
.P
If the process doesn't go away within a few seconds, it's probably ignoring
\f(CWSIGTERM\fP.  In this case, you can use the ultimate weapon:
.Dx
# \f(CBkill -9 105\fP
.De
The \f(CW-9\fP is the number of \f(CWSIGKILL\fP, a signal that cannot be caught
or ignored.  You can find a list of the signals and their numeric values in
.File /usr/include/sys/signal.h ,
which is part of the software development package.
.Le
FreeBSD also has a script called
.Command killall .
As the name implies, it kills a group of processes, by name.  If you find that
you have, say, a whole lot of runaway
.Daemon sendmail
processes, you might save the day by writing:
.Dx
# \f(CBkillall sendmail\fP
.De
As we'll see elsewhere, you can also use
.Command killall
to send a signal to a single process when you know that only one is present.
For example, to cause
.Daemon inetd
to re-read its configuration file, you could write:
.Dx 1
# \f(CBkillall -1 inetd\fP
.De
.SPUP
.H2 "Timekeeping"
.X "timekeeping"
.Pn timekeeping
FreeBSD is a networking system, so keeping the correct time is more important
than on a standalone system.  Apart from the obvious problem of keeping the same
time as other local systems, it's also important to keep time with systems in
other time zones.
.P
.X "epoch"
.X "UTC, time zone"
.X "Universal Coordinated Time"
.X "time zone"
.X "PST"
.X "Pacific Standard Time"
.X "PDT"
.X "Pacific Daylight Time"
Internally, FreeBSD keeps the time as the number of seconds since the
\fIepoch\fP, the beginning of recorded history: 00:00:00 UTC, 1 January 1970.
\fIUTC\fP is the international base time zone, and means \fIUniversal
Coordinated Time\fP, despite the initials.  It corresponds very closely, but not
exactly, to Greenwich Mean Time (GMT), the local time in England in the winter.
It would be inconvenient to keep all dates in UTC, so the system understands the
concept of time zones.  For example, in Walnut Creek, CA, the time zone in the
winter is called \fIPST\fP\/ (\fIPacific Standard Time\fP\/), and in the summer
it is \fIPDT\fP\/ (\fIPacific Daylight Time\fP\/).  FreeBSD comes with a set of
time zone description files in the directory hierarchy
.Directory /usr/share/zoneinfo .
We've already seen on page
.Sref \*[set-timezone] \&
that when you install the system, it stores information about the local time
zone in the file
.File /etc/localtime .
If you move
time zones, you should change the time zone, not the time, either by running the
.Command tzsetup
program, or simply by copying the file.  For example, if you travel with a
laptop from Adelaide, South Australia, to San Francisco CA, you would do:
.Dx
# \f(CBcp /usr/share/zoneinfo/America/Los_Angeles /etc/localtime\fP
.De
.ne 3v
When you get home again, you would do:
.Dx
# \f(CBcp /usr/share/zoneinfo/Australia/Adelaide /etc/localtime\fP
.De
At no time do you need to change the date or time directly.
.P
Why \f(CWLos_Angeles\fP and not \f(CWSan_Francisco\fP?  The developers of the
time zone package chose the largest city in the time zone.  You need to have a
certain understanding of the time zones to choose the correct one.
.H3 "The TZ environment variable"
An alternate means of describing the time zone is to set the environment
variable \f(CWTZ\fP, which we looked at on page
.Sref \*[environment-variables] .
You might use this form if you're connected to a remote system in a different
time zone, or maybe just to find the time at some other place.  For example, in
Adelaide, SA I might find:
.Dx
$ \f(CBdate\fP
Sun Apr 14 13:31:15 CST 2002
$ \f(CBTZ=America/Los_Angeles date\fP
Sat Apr 13 21:01:15 PDT 2002
.De
Set the \f(CWTZ\fP variable to the name of the time zone info file in the
.Directory /usr/share/zoneinfo
hierarchy.  For example, the value of \f(CWTZ\fP for Berlin, Germany is
\f(CWEurope/Berlin\fP in FreeBSD.
.P
.ne 3v
This is not the same as the usage of the \f(CWTZ\fP variable in UNIX System V.
System V doesn't have the time zone definition files in
.Directory /usr/share/zoneinfo ,
so the \f(CWTZ\fP variable tells it information about the time zone.  If you
were using System V in Berlin, you would set your \f(CWTZ\fP variable to
\f(CWMEZ1MSZ2\fP, indicating time zone names and offsets from UTC.
.H3 "Keeping the correct time"
.X "ntp"
.Pn ntp
If you're connected to the Internet on a reasonably regular basis, there are a
number of programs which can help you synchronize your time via the \fIntp\fP\/
(\fINetwork Time Protocol\fP\/) service.
.P
A number of systems around the world supply time information via the \fIntp\fP\/
service.  Look at
.URI http://www.eecis.udel.edu/~mills/ntp/servers.html
to find one near you.
.P
Your choice of program depends on the nature of your connection to the Internet.
If you're connected full time, you'll probably prefer
.Daemon ntpd ,
which keeps the system synchronized.  Otherwise you can use
.Command ntpdate ,
which you can run as you feel like it.
..if review
| (bmc) Or when you bring up your PPP link.
..endif
.H4 "ntpd"
.Daemon ntpd
performs periodic queries to keep the system synchronized with a time server.
There are many ways to run it\(emsee the man page \fIntpd(8)\fP.  In most cases,
you can set up one system on the network to connect to an external time
reference, and the other systems on the same Ethernet can get the time
information from the first system.
.P
To get the time from an external source and broadcast it to the other systems on
the network, create a file
.File /etc/ntp.conf
with a content like this:
.Dx
server     227.21.37.18                  \fIthis address is invalid; check what's near you\fP\/
driftfile  /etc/ntp.drift
broadcast  223.147.37.255
.De
The first line defines the server.  The value in this example is invalid , so
don't try to use it.  It's important to get one near you: network delays can
significantly impair the accuracy of the results.
.Daemon ntpd
uses the file
.File /etc/ntp.drift
to record information about the (in)accuracy of the local system's clock.  You
only need the final line if you have other systems on the network which wait for
a broadcast message.  It specifies the broadcast address for the network and
also tells
.Daemon ntpd
to broadcast on this address.
.P
After setting up this file, you just need to start
.Daemon ntpd \/:
.Dx
# \f(CBntpd\fP
.De
To ensure that
.Daemon ntpd
gets started every time you reboot, make sure that you have the following lines
in
.File /etc/rc.conf \/:
.Dx 1
ntpd_enable="\f(CBYES\fP"               # Run ntpd Network Time Protocol (or NO).
.De
The comment on the first line is misleading: the value of \f(CWntpd_enable\fP
must be \f(CWYES\fP.  You don't need any flags.  You put exactly the same text
in the
.File /etc/rc.conf
on the other machines, and simply omit the file
.File /etc/ntp.conf .
This causes
.Daemon ntpd
on these machines to monitor broadcast messages.
.P
In previous versions of FreeBSD,
.Daemon ntpd
was called
.Daemon xntpd ,
so you may find things like \f(CWxntpd_enable\fP in your
.File /etc/rc.conf .
If you do, you'll have to change the name.
.H4 "ntpdate"
If you connect to the Internet infrequently,
.Daemon ntpd
may become discouraged and not keep good time.  In this case, it's better to use
.Command ntpdate .
Simply run it when you want to set the time:
.Dx
# \f(CBntpdate \f(CIserver
.De
You can't use both
.Command ntpdate
and
.Daemon ntpd
at the same time: they both use the same port.
.Daemon ntpd
takes quite some time to synchronize, and if the time is wildly out, it won't
even try, so it's often a good idea to run
.Command ntpdate
on startup and then start
.Daemon ntpd
manually.
.H2 "Log files"
.Pn logfiles
Various components of FreeBSD report problems or items of interest as they
happen.  For example, there can always be problems with mail delivery, so a mail
server should keep some kind of record of what it has been doing.  If hardware
problems occur, the kernel should report them.  If somebody tries to break into
the machine, the components affected should report the fact.
.P
.ne 3v
FreeBSD has a generalized system for \fIlogging\fP\/ such events.  The
.Daemon syslogd
daemon takes messages from multiple sources and writes them to multiple
destinations, usually log files in the directory
.Directory /var/log .
You can change this behaviour by modifying the file
.File /etc/syslog.conf .
See \fIsyslog.conf(5)\fP\/ for further details.
.\" XXX link to configfiles entry?
In addition to
.Daemon syslogd ,
other programs write directly to files in this directory.  The following files
are of interest:
.Ls B
.LI
.File XFree86.0.log
contains the log file for the last (or current) X session started on display 0.
This is a prime source of information if you run into problems with X.
.LI
.File auth.log
contains information about user authentication.  For example, you might see:
.Dx
Dec 10 10:55:11 bumble su: grog to root on /dev/ttyp0
Dec 10 12:00:19 bumble sshd[126]: Server listening on :: port 22.
Dec 10 12:00:19 bumble sshd[126]: Server listening on 0.0.0.0 port 22.
Dec 10 12:06:52 bumble sshd[167]: Accepted publickey for grog from 223.147.37.80
 port 49564 ssh2
Dec 10 12:06:58 bumble su: BAD SU grog to root on /dev/ttyp0
.De
The first line is a successful
.Command su
invocation; the last line is an unsuccessful one (because the password was
mistyped).  The messages at 12:00:19 are from
.Daemon sshd
startup, and the message at 12:06:52 is a successful remote login with
.Command ssh .
.LI
.File cron
is a log file for
.Daemon cron .
It's relatively uninteresting:
.Dx
Jan  5 16:00:00 bumble newsyslog[2668]: logfile turned over
Jan  5 16:05:00 bumble /usr/sbin/cron[2677]: (root) CMD (/usr/libexec/atrun)
Jan  5 16:05:00 bumble /usr/sbin/cron[2678]: (root) CMD (/usr/libexec/atrun)
Jan  5 16:10:00 bumble /usr/sbin/cron[2683]: (root) CMD (/usr/libexec/atrun)
.De
If you have problems with
.Daemon cron ,
that could change rapidly.
.LI
.File dmesg.today
and
.File dmesg.yesterday
are created by a
.Daemon cron
job at 2 am every day.  The
.Command dmesg
message buffer wraps around, overwriting older entries, so they can be of use.
.LI
.File lastlog
is a binary file recording last login information.  You don't normally access it
directly.
.LI
.File maillog
contains information about mail delivery.
.LI
.File messages
is the main log file.
.LI
The files
.File mount.today
and
.File mount.yesterday
show the currently mounted file systems in the format needed for
.File /etc/fstab .
.LI
The file
.File ppp.log
contains information on PPP connections.  We look at it on page
.Sref \*[pedestrian-uppp] .
.LI
The files
.File setuid.today
and
.File setuid.yesterday
contain a list of \fIsetuid\fP\/ files.  The daily security check compares them
and sends a mail message if there are any differences.
.LI
The file
.File vinum_history
contains information about
.Command vinum
activity.
.LI
The file
.File wtmp
contains information about logins to the system.  Like
.File lastlog ,
it's in binary form.  See \fIutmp(5)\fP\/ for the format of both
.File lastlog
and
.File wtmp .
.Le
A number of the more important log files are kept through several cycles.  As
the example above shows,
.Daemon cron
runs the
.Command newsyslog
command every hour.
.Command newsyslog
checks the size of the files, and if they are larger than a certain size, it
renames the old ones by giving them a numerical extension one higher than the
current one, then renames the base file with an extension \fI.0\fP and
compresses it.  The result looks like this:
.Dx
-rw-r--r--  1 root  wheel  31773 Jan  5 13:01 messages
-rw-r--r--  1 root  wheel   8014 Jan  2 01:00 messages.0.bz2
-rw-r--r--  1 root  wheel  10087 Dec 15 14:00 messages.1.bz2
-rw-r--r--  1 root  wheel   9940 Dec  3 17:00 messages.2.bz2
-rw-r--r--  1 root  wheel   9886 Nov 16 11:00 messages.3.bz2
-rw-r--r--  1 root  wheel   9106 Nov  5 18:00 messages.4.bz2
-rw-r--r--  1 root  wheel   9545 Oct 15 17:00 messages.5.bz2
.De
.Command newsyslog
has a configuration file
.File /etc/newsyslog.conf ,
which we discuss on page
.Sref \*[newsyslog.conf] .
.H2 "Multiple processor support"
.Pn smp
FreeBSD Release 5 can support most current Intel and AMD multiprocessor
motherboards with the ia32 architecture.  It also supports some Alpha, SPARC64
and Intel ia64 motherboards. Documentation on SMP support is currently rather
scanty, but you can find some information at
\fIhttp://www.freebsd.org/~fsmp/SMP/SMP.html\fP.
.P
The \f(CWGENERIC\fP kernel does not support SMP, so you must build a new kernel
before you can use more than one processor.  The configuration file
.File /usr/src/sys/i386/conf/GENERIC
contains the following commented-out entries:
.Dx
# To make an SMP kernel, the next two are needed
#options        SMP                     # Symmetric MultiProcessor Kernel
#options        APIC_IO                 # Symmetric (APIC) I/O
.De
For other platforms, you don't need \f(CWAPIC_IO\fP.  See  Chapter
.Sref \*[nchbuild] \&
for information on how to build a new kernel.
.H2 "PC Card devices"
.Pn pccard
As we have already seen, PC Card devices are special because they can be
hot-plugged.  They are also intended to be recognized automatically.  Starting
with Release 5, FreeBSD recognizes card insertion and removal in the kernel and
invokes the appropriate driver to handle the event.  When you insert a card you
will see something like this on the system console:
.Pn flash
.Dx
ata2 at port 0x140-0x14f irq 11 function 0 config 1 on pccard0
ad4: 7MB <LEXAR ATA FLASH> [251/2/32] at ata2-master BIOSPIO
.De
This is a compact flash memory card, which the system sees as an ATA disk.  The
kernel has created the necessary structures, but it can't know how to mount the
device, for example.  We'll look at what we can do about this in the next section.
.H3 "devd: The device daemon"
.Pn devd
.X "digital camera"
.X "camera, digital"
.X "flash memory"
The device daemon,
.Daemon devd ,
provides a way to run userland programs when certain kernel events happen.  It
is intended to handle userland configuration of PC Card devices such as Ethernet
cards, which it can do automatically.  We'll look at this automatic usage on
page
.Sref \*[PCMCIA-net] .
.P
.Daemon devd
reads the kernel event information from the device
.Device devctl
and processes it according to rules specified in the configuration file
.File /etc/devd.conf ,
which is installed with the system.  If you want to use it for other devices,
you must modify
.File /etc/devd.conf .
This file contains a number of sections, referred to as \fIstatements\fP\/ in
the man page:
.Ls B
.LI
The \fIoptions\fP\/ statement describes file paths and a number of regular
expressions (patterns) to look for in the messages it reads from
.Device devctl .
.LI
\fIattach\fP\/ statements specify what action to perform when a device is
attached.  For example:
.Dx
attach 0 {
        device-name "$scsi-controller-regex";
        action "camcontrol rescan all";
};
.De
The \f(CWdevice-name\fP entry uses the regular expression
\f(CW$scsi-controller-regex\fP to recognize the name of a SCSI controller in the
attach message.  The \f(CWaction\fP entry then specifies what action to take
when such a device is attached to the system.  In this case, it runs the
.Command camcontrol
program to rescan the SCSI buses and recognize any new devices that have been
added.
.P
Multiple \fIattach\fP\/ statements can match a specific event, but only one will
be executed.  The order in which they are checked is specified by a
\fIpriority\fP, a numerical value after the keyword \f(CWaction\fP.  The
statements are checked in order of highest to lowest numerical priority.
.LI
\fIdetach\fP\/ statements have the same syntax as \fIattach\fP\/ statements.  As
the name suggests, they are executed when a device is detached.
.P
It's not always possible or necessary to perform any actions when a device is
removed.  In the case of SCSI cards, there is no \fIdetach\fP\/ statement.
We'll look at this issue in more detail below.
.LI
Finally, if the kernel was unable to locate a driver for the card, it generates
a \fIno match\fP\/ event, which is handled by the \fInomatch\fP\/ statement.
.Le
So what does
.Daemon devd
do when we insert the compact flash card?  By default, nothing.  The ATA driver
recognizes and configures the card.  It would be nice to get
.Daemon devd
to mount it as well.  That's relatively simple:
.Ls B
.LI
Ensure that you have an entry for the device in
.File /etc/fstab .
Digital cameras create a single MS-DOS file system on flash cards.  An
appropriate entry in
.File /etc/fstab
for this device might be:
.Dx
/dev/ad4s1              /camera         msdos   rw,noauto       0       0
.De
This is a removable device, so you should use the \f(CWnoauto\fP keyword to stop
the system trying to mount it on system startup.
.LI
In the \fIoptions\fP\/ section of
.File /etc/devd.conf ,
add an expression to recognize the names of ATA controllers:
.Dx
        set ata-controller-regex
                "ata[0-9]+";
.De
.SPUP
.LI
Add an \fIattach\fP\/ section for the device:
.Dx
attach 0 {
        device-name "$ata-controller-regex";
        action "mount /camera";
};
.De
.SPUP
.LI
Restart
.Daemon devd \/:
.Dx
# \f(CBkillall devd\fP
# \f(CBdevd\fP
.De
.SPUP
.Le
After this, the file system will be automatically mounted when you insert the
card.
.H3 "Removing PC Card devices"
The next thing we'd like to do is to unmount the file system when you remove the
flash card.  Unfortunately, that isn't possible.  Unmounting can involve data
transfer, so you have to do it before you remove the card.  If you forget, and
remove the card without unmounting, the system may panic next time you try to
access the card.
.P
After unmounting, you can remove the card.  On the console you'll see something
like:
.Dx
ad4: removed from configuration
ad4: no status, reselecting device
ad4: timeout sending command=e7 s=ff e=04
ad4: flushing cache on detach failed
ata2: detached
.De
.SPUP
.H3 "Alternate PC Card code"
.Pn OLDCARD
.X "NEWCARD"
.X "OLDCARD"
The PC Card implementation described here, called \fINEWCARD\fP, is new in
FreeBSD Release 5.  At the time of writing, the older implementation, called
\fIOLDCARD\fP, is still included in the system.  It's possible that you might
have an older card that is supported by OLDCARD but not by NEWCARD.  In that
case, you will need to build a kernel with OLDCARD support.  Check the
.File NOTES
files in
.Directory /usr/src/sys/conf
and
.Directory -n /usr/src/sys/\f(CWarch\fP/conf \fR,
where \fIarch\fP\/ is the architecture of your system, and the man pages
\fIpccardd\fP\/ and \fIpccard.conf\fP.
.H3 "Configuring PC Card devices at startup"
A number of entries in
.File /etc/rc.conf
relate to the use of PC Card devices, but nearly all of them are for OLDCARD.
You only need one for NEWCARD:
.Dx
devd_enable="YES"
.De
This starts
.Daemon devd
at system startup.
.SPUP
.H2 "Emulating other systems"
.Pn emulate
.X "emulation"
.X "/usr/ports/emulators"
A large number of operating systems run on Intel hardware, and there is a lot of
software that is available for these other operating systems, but not for
FreeBSD.
.H3 "Emulators and simulators"
There are a number of ways to execute software written for a different
platform.  The most popular are:
.Ls B
.LI
\fISimulation\fP\/ is a process where a program executes the functions that are
normally performed by the native instruction set of another machine.  They
simulate the low-level instructions of the target machine, so simulators don't
have to run on the same kind of machine as the code that they execute.  A good
example is the port \fIemulators/p11\fP, which simulates a PDP-11 minicomputer,
the machine for which most early versions of UNIX were written.
.P
Simulators run much more slowly than the native instruction set: for each
simulated instruction, the simulator may execute hundreds of machine
instructions.  Amusingly, on most modern machines, the \fIp11\fP\/ emulator
still runs faster than the original PDP-11: modern machines are over 1,000 times
faster than the PDP-11.
.LI
In general, \fIemulators\fP\/ execute the program instructions directly and only
simulate the operating system environment.  As a result, they have to run on the
same kind of hardware, but they're not noticeably slower than the original.  If
there is any difference in performance, it's because of differences between the
host operating system and the emulated operating system.
.LI
Another use for the term \fIemulator\fP\/ is where the hardware understands a
different instruction set than the native one.  Obviously this is not the kind
of emulator we're talking about here.
.Le
FreeBSD can emulate many other systems to a point where applications written for
these systems will run under FreeBSD.  Most of the emulators are in the Ports
Collection in the directory
.Directory /usr/ports/emulators .
.P
In a number of cases, the emulation support is in an experimental stage.  Here's
an overview:
.Ls B
.LI
FreeBSD will run most BSD/OS programs with no problems.  You don't need an emulator.
.LI
FreeBSD will also run most NetBSD and OpenBSD executables, though not many
people do this: it's safer to recompile them under FreeBSD.
.LI
.X "Linux"
.X "a.out, object format"
.X "object format, a.out"
.X "ELF, object format"
.X "object format, ELF"
.X "kld"
.X "loadable kernel module"
FreeBSD runs Linux executables with the aid of the \fIlinux\fP\/ \fIkld\fP\/
(\fIloadable kernel module\fP\/).  We'll look at how to use it in the next
section.
.LI
FreeBSD can run SCO COFF executables with the aid of the \fIibcs2\fP\/
\fIkld\fP.  This support is a little patchy: although the executables will run,
you may run into problems caused by differences in the directory structure
between SCO and FreeBSD.  We'll look at it on page
.Sref \*[SCO-emulation] .
.LI
.X "Microsoft, Windows"
A \fIMicrosoft Windows\fP\/ emulator is available.  We'll look at it on page
.Sref \*[wine] .
.Le
.SPUP
.H2 "Emulating Linux"
.X "Torvalds, Linus"
Linux is a UNIX-like operating system that in many ways is very similar to
FreeBSD.  We discussed it on page
.Sref \*[Linux] .
Although it looks very UNIX-like, many of the internal kernel interfaces are
different from those of FreeBSD or other UNIX-based systems.  The Linux
compatibility package handles these differences, and most Linux software will
run on FreeBSD.  Most of the exceptions use specific drivers that don't run on
FreeBSD, though there is a considerable effort to minimize even this category.
.P
To install the Linux emulator, you must:
.Ls B
.LI
Install the compatibility libraries.  These are in the port
.Directory /usr/ports/emulators/linux_base .
.LI
.X "linux, kld"
.X "kld, linux"
Run the Linux emulator kld, \fIlinux\fP.
.Le
.ne 10v
.H3 "Running the Linux emulator"
.Pn Linux-emulation
Normally you load the Linux emulator when you boot the system.  Put the
following line in your
.File /etc/rc.conf \/:
.Dx
linux_enable="YES"
.De
If you don't want to do this for some reason, you can start it from the command line:
.Dx
# \f(CBkldload linux\fP
.De
You don't interact directly with the emulator module: it's just there to supply
kernel functionality, so you get a new prompt immediately when you start it.
.P
.X "kldstat, command"
.X "command, kldstat"
\fIlinux\fP\/ is a kld, so it doesn't show up in a
.Command ps
listing.  To check whether it is loaded, use
.Command kldstat \/:
.Dx
$ \f(CBkldstat\fP
Id Refs Address    Size     Name
 1    5 0xc0100000 1d08b0   kernel
 2    2 0xc120d000 a000     ibcs2.ko
 3    1 0xc121b000 3000     ibcs2_coff.ko
 5    1 0xc1771000 e000     linux.ko
.De
This listing shows that the SCO UNIX emulation (\fIibcs2\fP\/) has also been
loaded.
.P
.ne 3v
The Linux emulator and many Linux programs are located in the directory
hierarchy
.Directory /usr/compat/linux .
You won't normally need to access them directly, but if you get a Linux program
that includes libraries destined for
.Directory /lib ,
you will need to manually place them in
.Directory /usr/compat/linux/lib .
Be \f(BIvery\fP\/ careful not to replace any files in the
.Directory /usr/lib
hierarchy with Linux libraries; this would make it impossible to run FreeBSD
programs that depend on them, and it's frequently very difficult to recover
from such problems.  Note that FreeBSD does not have a directory
.File /lib ,
so the danger is relatively minor.
.H3 "Linux procfs"
.X "procfs"
Linux systems have a file system called \fIprocfs\fP, or \fIProcess File
System\fP, which contains information used by many programs.  FreeBSD also has a
\fIprocfs\fP, but it is completely different.  To be able to run Linux programs
which refer to \fIprocfs\fP, place the following entry in your
.File /etc/fstab
file:
.Dx
linproc        /compat/linux/proc  linprocfs   rw      0       0
.De
.SPUP
.H3 "Problems executing Linux binaries"
.X "branding ELF binaries"
.X "StarOffice, package"
One of the problems with the ELF format used by older Linux binaries is that
they may contain no information to identify them as Linux binaries.  They might
equally well be BSD/OS or UnixWare binaries.  That's normally not a problem,
unless there are library conflicts: the system can't decide which shared library
to use.  If you have this kind of binary, you must \fIbrand\fP\/ the executable
using the program
.Command brandelf .
For example, to brand the StarOffice program
.Command -n swriter3 ,
you would enter:
.Dx
# \f(CBbrandelf -t Linux /usr/local/StarOffice-3.1/linux-x86/bin/swriter3\fP
.De
This example deliberately shows a very old version of StarOffice: it's not clear
that there are any modern binaries that cause such problems.
.H2 "Emulating SCO UNIX"
.Pn SCO-emulation
.X "SCO, UNIX"
.X "SCO, OpenDesktop"
.X "COFF, object format"
.X "object format, COFF"
.X "Common Object File Format"
\fISCO UNIX\fP\/, also known as \fISCO OpenDesktop\fP\/ and \fISCO Open
Server\fP, is based on UNIX System V.3.2.  This particular version of UNIX was
current in the late 1980s.  It uses an obsolete binary format called
\fICOFF\fP\/ (\fICommon Object File Format\fP\/).
.P
.X "ibcs2, kld"
.X "kld, ibcs2"
Like Linux support, SCO support for FreeBSD is supplied as a loadable kernel
module.  It's not called
.Command sco ,
though: a number of older System V.3.2 systems, including Interactive UNIX, also
support the \fIibcs2\fP\/\*F
.FS
.X "Intel Binary Compatibility System 2"
\fIibcs2\fP\/ stands for \fIIntel Binary Compatibility System 2\fP.
.FE
standard.
As a result, the kld is called \fIibcs2\fP.
.P
Run ibcs2 support like Linux support: start it manually, or modify
.File /etc/rc.conf
to start it automatically at bootup:
.Dx
ibcs2_enable="YES"      # Ibcs2 (SCO) emulation loaded at startup (or NO).
.De
Alternatively, load the kld:
.Dx
# \f(CBkldload ibcs2\fP
.De
One problem with SCO emulation is the SCO shared libraries.  These are required
to execute many SCO executables, and they're not supplied with the emulator.
They \fIare\fP\/ supplied with SCO's operating systems.  Check the SCO license
to determine whether you are allowed to use them on FreeBSD.  You may also be
eligible for a free SCO license\(emsee the SCO web site for further details.
.\"XXX.H2 "UNIX System V.4 emulation"
.\"XXX.Pn svr4-emulation
.\"XXXThe UNIX System V.4 emulation package specifically emulates SCO UnixWare. XXX
.H2 "Emulating Microsoft Windows"
.Pn wine
.X "wine, command"
.X "command, wine"
The \fIwine\fP\/ project has been working for some time to provide an emulation
of Microsoft's \fIWindows\fP\/ range of execution environments.  It's changing
continually, so there's little point describing it here.  You can find
up-to-date information at
.URI http://www.winehq.com/about/ ,
and you can install it from the port \fIemulators/wine\fP.  Be prepared for a
fair amount of work.
.H3 "Accessing Microsoft files"
Often you're not as interested in running Microsoft applications as decoding
their proprietary formats.  For example, you might get a mail message with an
attachment described only as
.Dx
[-- Attachment #2: FreeBSD.doc --]
[-- Type: application/octet-stream, Encoding: x-unknown, Size: 15K --]

[-- application/octet-stream is unsupported (use 'v' to view this part) --]
.De
This attachment has an unspecific MIME type,\*F
.FS
See
.Sref "\*[chmua]" ,
page
.Sref \*[MIME] ,
for more information about MIME.
.FE
but you might guess that it is Microsoft Word format because the file name ends
in \fI.doc\fP.  That doesn't make it any more legible.  To read it, you need
something that understands the format.  A good choice is \fIOpenOffice.org\fP, a
clone of Microsoft's ``Office'' product.  Install from the Ports Collection
.Directory ( /usr/ports/editors/openoffice ).
.P
OpenOffice.org is not a good example of the UNIX way.  It breaks a number of
conventions, and in general it's a lot more difficult to use than normal FreeBSD
tools.  Its only real advantage is that you can process Microsoft document
formats.
