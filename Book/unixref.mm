.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: unixref.mm,v 4.16 2003/04/02 06:41:29 grog Exp grog $
.\"
.Chapter \*[nchunixref] "The tools of the trade"
So now you have installed FreeBSD, and it successfully boots from the hard disk.
If you're new to FreeBSD, your first encounter with it can be rather puzzling.
You probably didn't expect to see the same things you know from other platforms,
but you might not have expected what you see either:
.Pn login-prompt
.Dx
   FreeBSD (freebie.example.org) (ttyv0)

login:
.De
If you have installed
.Daemon xdm ,
you'll at least get a graphical display, but it still asks you to log in and
provide a password.  Where do you go from here?
.P
.X "Abrahams, Paul W."
.X "Larson, Bruce R."
.X "Peek, Jerry"
.X "O'Reilly, Tim"
.X "Loukides, Mike"
.X "Nemeth, Evi"
.X "Snyder, Garth"
.X "Seebass, Scott"
.X "Hein, Trent R."
There isn't space in this book to explain everything there is about working with
FreeBSD, but in the following few chapters I'd like to make the transition
easier for people who have prior experience with Microsoft platforms or with
other flavours of UNIX.  You can find a lot more information about these topics
in \fIUNIX for the Impatient\fP\/, by Paul W. Abrahams and Bruce R. Larson,
\fIUNIX Power Tools\fR, by Jerry Peek, Tim O'Reilly, and Mike Loukides, and
\fIUNIX System Administration Handbook\fP, by Evi Nemeth, Garth Snyder, Scott
Seebass, and Trent R. Hein.  The third edition of this book also covers FreeBSD
Release 3.2.  See
.Sref "\*[biblio]" ,
for more information.
.P
If you've come from Microsoft, you will notice a large number of differences
between UNIX and Microsoft, but in fact the two systems have more in common than
meets the eye.  Indeed, back in the mid-80s, one of the stated goals of MS-DOS
2.0 was to make it more UNIX-like.  You be the judge of how successful that
attempt was, but if you know the MS-DOS command-line interface, you'll notice
some similarities in the following sections.
.P
In this chapter, we'll look at FreeBSD from the perspective of somebody with
computer experience, but with no UNIX background.  If you \fIdo\fP\/ have a UNIX
background, you may still find it interesting.
.P
If you're coming from a Microsoft platform, you'll be used to doing just about
everything with a graphical interface.  In this book I recommend that you use X
and possibly a desktop, but the way you use it is still very different.
FreeBSD, like other UNIX-like systems, places much greater emphasis on the use
of text.  This may seem primitive, but in fact the opposite is true.  It's
easier to point and click than to type, but you can express yourself much more
accurately and often more quickly with a text interface.
.P
As a result, the two most important tools you will use with FreeBSD are the
\fIshell\fP\/ and the \fIeditor\fP.  Use the shell to issue direct commands to
the system, and the editor to prepare texts.  We'll look at these issues in more
detail in this chapter.  In
.Sref "\*[chunixadmin]" ,
we'll look at other aspects of the system.  First, though, we need to get access
to the system.
.H2 "Users and groups"
.X "user"
.X "user, groups"
.Pn users-and-groups
Probably the biggest difference between most PC operating systems and FreeBSD
also takes the longest to get used to: FreeBSD is a multi-user, multi-tasking
system.  This means that many people can use the system at once, and each can do
several things at the same time.  You may think ``Why would I want to do
that?.''  Once you've got used to this idea, though, you'll never want to do
without it again.  If you use the X Window System, you'll find that all windows
can be active at the same time\(emyou don't have to select them.  You can
monitor some activity in the background in another window while writing a
letter, testing a program, or playing a game.
.P
.X "user"
Before you can access a FreeBSD system, you must be registered as a \fIuser\fP.
The registration defines a number of parameters:
.Ls B
.LI
.X "user, name"
.X "user, ID"
A \fIuser name\fP, also often called \fIuser ID\fP.  This is a name that you
use to identify yourself to the system.
.LI
.Pn password
.X "password"
A \fIpassword\fP, a security device to ensure that other people don't abuse your
user ID.  To log in, you need to specify both your user ID and the correct
password.  When you type in the password, nothing appears on the screen, so that
people looking over your shoulder can't read it.
.P
It might seem strange to go to such security measures on a system that you alone
use.  The incidence of Internet-related security problems in the last few years
has shown that it's not strange at all, it's just common sense.  Microsoft
systems are still subject to a never-ending series of security exploits.
FreeBSD systems are not.
.LI
.Pn shell
.X "shell"
A \fIshell\fP, a program that reads in your commands and executes them.  MS-DOS
uses the program
.Command COMMAND.COM
to perform this function.  UNIX has a large choice of shells: the traditional
UNIX shells are the Bourne shell
.Command sh
and the C shell
.Command csh ,
but FreeBSD also supplies
.Command bash ,
.Command tcsh ,
.Command zsh
and others.  I personally use the
.Command bash
shell, and the examples in this book are based on it.
.LI
.Pn home-directory
.X "home directory"
.X "directory, home"
A \fIhome directory\fP.  The system can have multiple users, so each one needs a
separate directory in which to store his private files.  Typically, users have a
directory \fI/home/\f(BIusername\fR, where \f(BIusername\fP\/ is the name they
use to log in.  When you log in to the system, the shell sets the current
directory to your home directory.  In it, you can do what you want, and normally
it is protected from access by other users.  Many shells, including the
.Command bash
shell used in these examples, use the special notation \fB~\fP\/ (tilde) to
represent the name of the home directory.
.LI
.X "group"
.Pn group
A \fIgroup\fP\/ number.  UNIX collects users into \fIgroups\fP\/ who have
specific common access permissions.  When you add a user, you need to make him a
member of a specific group, which is entered in the password information.  Your
group number indirectly helps determine what you are allowed to do in the
system.  As we'll see on page
.Sref \*[permissions] ,
your user and group determine what access you have to the system.  You can
belong to more than one group.
.P
Group numbers generally have names associated with them.  The group names and
numbers are stored in the file
.File /etc/group .
In addition, this file may contain user IDs of users who belong to another
group, but who are allowed to belong to this group as well.
.P
If you find the concept of groups confusing, don't worry about them.  You can
get by quite happily without using them at all.  You'll just see references to
them when we come to discuss file permissions.  For further information, look at
the man page for \fIgroup(5)\fP.
.Le
By the time you get here, you should have defined a user name, as recommended on
page
.Sref \*[firstuser] .
If you haven't, you'll have to log in as \f(CWroot\fP and create one as
described there.
.H2 "Gaining access"
Once you have a user name, you can log in to the system.  Already you have a
choice: FreeBSD offers both \fIvirtual terminals\fP\/ and the X Window System.
The former displays plain text on the monitor, whereas the latter uses the
system's graphics capabilities.  Once running, you can switch from one to the
other, but you have the choice of which interface you use first.  If you don't
do anything, you get a virtual terminal.  If you run
.Daemon xdm ,
you get X.
.P
It's still relatively uncommon to use
.Daemon xdm ,
and in many instances you may not want X at all, for example if you're running
the system as a server.  As a result, we'll look at the ``conventional'' login
first.
.P
If you're logging in on a virtual terminal, you'll see something like this:
.Pn login
.Dx
login: \f(CBgrog\fP
Password:                                 \fIpassword doesn't show on the screen\fP\/
Last login: Fri Apr 11 16:30:04 from canberra
Copyright (c) 1980, 1983, 1986, 1988, 1990, 1991, 1993, 1994
        The Regents of the University of California.  All rights reserved.

FreeBSD 5.0-RELEASE (FREEBIE) #0: Tue Dec 31 19:08:24 CST 2002

Welcome to FreeBSD!

You have mail.
erase ^H, kill ^U, intr ^C, status ^T
Niklaus Wirth has lamented that, whereas Europeans pronounce his name
correctly (Ni-klows Virt), Americans invariably mangle it into
(Nick-les Worth).  Which is to say that Europeans call him by name, but
Americans call him by value.
=== grog@freebie (/dev/ttyv0) ~ 1 ->
.De
There's a lot of stuff here.  It's worth looking at it in more detail\/:
.Ls B
.LI
The program that asks you to log in on a terminal window is called
.Command getty .
It reads in your
user ID and starts a program called
.Command login
and passes the user ID to it.
.LI
.Command login
asks for the password and checks your user ID.
.LI
If the user ID and password are correct,
.Command login
starts your designated
shell.
.LI
While starting up, the shell looks at a number of files.
..if verylong
We'll look at them in more detail further down, after we've seen what they need
to do.
..else
See the man page for your particular shell for details of what they are for.
..endif
.X "message of the day"
.Pn motd
In this case, though, we can see the results: one file contains the time you
last logged in, another one contains the \fIMessage of the day\fP\/
.File ( /etc/motd ),
and a third one informs you that you have mail.  The shell prints out the
message of the day verbatim\(emin this case, it contains information about the
name of the kernel and a welcome message.  The shell also prints information on
last login time (in this case, from a remote system) and whether you have mail.
.LI
The line ``\f(CWerase ^H, kill ^U, intr ^C, status ^T\fP'' looks strange.  It's
telling you the current editing control characters.  We'll look at these on page
.Sref \*[control-chars] .
.X "home directory"
.X "directory, home"
At this point, the shell changes the current directory to your \fIhome
directory\fP.  There is no output on the screen to indicate this.
.LI
.X "Wirth, Niklaus"
The shell runs the
.Command fortune
program, which prints out a random quotation from a database of ``fortune
cookies.''  In this case, we get a message about Niklaus Wirth, the inventor of
the Pascal programming language.
.LI
Finally, the last line is a prompt, the information that tells you that the
shell is ready for input.
.Le
The prompt illustrates a number of things about the UNIX environment.  By
default,
.Command sh
and friends prompt with a \f(CW$\fP, and
.Command csh
and friends prompt with a \f(CW%\fP.  You can change it to just about anything
you want with the UNIX shells.  You don't have to like my particular version,
but it's worth understanding what it's trying to say.
.P
.Pn shell-prompt
The first part, \f(CW===\fP, is just to make it easier to find in a large list
on an X display.  An
.Command xterm
window on a high resolution X display can contain up to 120 lines, and searching
for command prompts can be non-trivial.
.P
Next, \f(CWgrog@freebie\fP is my user ID and the name of system on which I am
working, in the RFC 2822 format used for mail IDs.  Multiple systems and
multiple users can all be present on a single X display.  This way, I can figure
out which user I am and what system I am running on.
.P
.Device ttyv0
is the name of the terminal device.  This can sometimes be useful.
.P
\f(CW~\fP is the name of the home directory.  Most shells, but not all of them,
support this symbolism.
.P
\f(CW1\fP is the prompt number.  Each time you enter a command, it is associated
with this number, and the prompt number is incremented.  One way to re-execute
the command is to enter \f(CW!!1\fP (two exclamation marks and the number of the
command).  We'll look at more comfortable ones on page
.Sref \*[control-chars] .
.P
To start X from a virtual terminal shell, use the
.Command startx
command:
.Dx
$ \f(CBstartx\fP
.De
If you use
.Daemon xdm ,
you bypass the virtual terminals and go straight into X.  Enter your user name
and password to the login prompt or the
.Daemon xdm
login screen, and press \fBEnter\fP.  If you use the
.Daemon xdm
login, you'll go straight into X.
.PIC images/kde-main-4.ps 5i
.Figure-heading "KDE display"
.Fn kde-main
Either way, assuming that you've installed and configured
.Command kde ,
you'll get a display similar to that in Figure
.Sref \*[kde-main] .
This example includes four windows that are not present on startup.  On startup
the central part of the screen is empty.  We'll look at the windows further
below.
.H2 "The KDE desktop"
.Pn using-kde
KDE is a complicated system, and good documentation is available at
.URI http://www.kde.org/documentation/ .
Once you have KDE running, you can access the same information via the help icon
on the panel at the bottom (the life ring icon).  The following description
gives a brief introduction.
.P
The KDE display contains a number of distinct areas.  At the top is an optional
menu, at the bottom an almost optional \fIpanel\fP, and the middle of the screen
is reserved for windows.
.H3 "The Desktop Menu"
The \fIDesktop Menu\fP\/ is at the very top of the screen.  It provides
functionality that is not specific to a particular application.  Select the
individual categories with the mouse.  For example, the \f(CWNew\fP menu looks
like this:
.PIC images/kde-new-menu.ps 3i
.Figure-heading "KDE desktop menu"
As the menu indicates, you can use these menus to create new files.
.H4 "The Panel"
At the bottom of the screen is the panel, which consists of a number of fields.
The left-hand section is used for starting applications.
.PIC images/kde-taskbar-left.ps 4i
.P
The stylized letter K at the extreme left is the \fIApplication Starter\fP.
When you select it, a long vertical menu appears at the left of the screen and
allows you to start programs (``applications'') or access just about any other
function.
.P
Next comes an icon called ``show desktop.''  This is a convenient way to iconify
all the windows currently on the desktop.
.P
The remaining icons on this part of the panel represent various applications.
.Ls B
.LI
The
.Command konsole
terminal emulator.
.LI
The \fIcommand center\fP, which you use to configure KDE.
.LI
The help system.
.LI
Access to the home directory with the browser
.Command konqueror .
.LI
Access to the Web, also with the browser
.Command konqueror .
.LI
The
.Command Kmail
MUA.
.LI
The
.Command KWord
word processor, which can understand Microsoft Word documents.
.LI
The
.Command Kspread
spreadsheet.
.LI
The
.Command Kpresenter
presentation package.
.LI
The
.Command Kate
editor.
.Le
The next section of the panel contains some control buttons and information
about the current desktop layout:
.PIC images/kde-taskbar-middle.ps 4i
The section at the left shows the current contents of four screens, numbered 1
to 4.  Screen 1 is the currently displayed screen; you can select one of the
others by moving the cursor in the corresponding direction, or by selecting the
field with the mouse.
.P
To the right of that are icons for the currently active windows.  The size
expands and contracts depending on the number of different kinds of window
active.  If you select one of these icons with the left mouse button, it will
iconify or deiconify (``minimize'' or ``maximize'') the window.
If you have multiple
.Command xterm s
active, you will only have one icon.  In this case, if you select the icon, you
will get another pop-up selection menu to allow you to choose the specific
window.
.P
The right part of the panel contains a further three fields:
.PIC images/kde-taskbar-right.ps 4i
.Ls B
.LI
The first one
shows a stylized padlock (for locking the session when you leave the machine;
unlock by entering your password) and a stylized off switch, for logging out of
the session.
.LI
The next section shows a stylized power connector, which displays the current
power status of the machine, and a clipboard.
.LI
The right side shows a digital clock.
.Le
Probably the most useful part of this section of the panel is not very obvious:
the right-pointing arrow allows you to remove the panel if you find it's in the
way.  The entire panel is replaced by a single left-pointing arrow at the
extreme right of the display.
.H4 "Using the mouse"
By default,
.Command kde
only uses the left and the right mouse buttons.  In general, the left button is
used to select a particular button, and the right button is used for an
auxiliary menu.
.H4 "Manipulating windows"
You'll notice that each window has a frame around it with a number of features.
In X terminology, they're called \fIdecorations\fP.  Specifically:
.Ls B
.LI
There's a \fItitle bar\fP\/ with the name of the program.  If you select the bar
itself, you raise the window above all others.  If you hold down the button on
the title bar, you can move the window.
.LI
At the left of the title bar there is an X logo.  If you select this logo, you
get a menu of window operations.
.LI
At the right of the title bar, there are three buttons that you can select.  The
left one iconifies the window, the middle one \fImaximizes\fP\/ the window,
making it take up the entire screen, and the one on the right kills the
application.  If the window is already maximized, the middle button restores it
to its previous size.
.LI
You can select any corner of the window, or any of the other edges, to change
the size of the window.
.Le
.SPUP
.H2 "The fvwm2 window manager"
.Pn fvwm2
If you come from a conventional PC background, you shouldn't have much
difficulty with KDE.  It's a relatively complete, integrated environment.  But
it isn't really UNIX.  If you come from a UNIX environment, you may find it too
all-encompassing.  You may also find that there are significant delays when you
start new applications.
.P
UNIX has a very different approach to windows.  There is no desktop, just a
window manager.  It takes up less disk space, less processor time, and less
screen real estate.  By default, XFree86 comes with the
.Command twm
window manager, but that's really a little primitive.  With modern machines,
there's no reason to choose such a basic window manager.  You may, however, find
that
.Command fvwm2
is more your style than KDE.
.H3 "Starting fvwm2"
Like KDE, you install
.Command fvwm2
from the Ports Collection.  It's not designed to work completely correctly out
of the box, though it does work.  As with KDE, the first thing you need to do is
to create a
.File .xsession
or
.File .xinitrc
file, depending on whether you're running
.Daemon xdm .
It must contain at least the line:
.Dx
fvwm2
.De
Start X the same way you did for KDE.  This time you see, after starting the
same applications as before:
.PIC images/fvwm-main.ps 5i
This picture shows both similarities with and differences from KDE.  The
similarities include:
.Ls B
.LI
Each window has a frame and a title.  The exact form of the decorations is
different, but the purpose is the same.  There is no ``close application''
button: for most UNIX applications, you should get the program to exit rather
than killing it.
.LI
There is a task bar at the bottom right, taking up only half the width of the
screen.  The currently active window (the
.Command xterm
at the left in this example) is highlighted.
.LI
The default
.Command fvwm2
display also has four screens, and the task bar shows the position of the
windows on the task bar.
.Le
.ne 4v
Still, there are a number of differences as well:
.Ls B
.LI
Unless you have a top-end machine, it's \fImuch\fP\/ faster in what it does.
.LI
The background (the \fIroot window\fP\/) doesn't have any pattern; it's just a
grey cross-hatch.
.LI
You can move from one screen to the other using the cursor, and windows can
overlap.  In this example, the
.Command galeon
web browser window goes down to the screen below, and the Emacs window goes over
all four screens, as the display on the task bar shows.  With KDE, the only way
to display the rest of these windows is to move the window.
.LI
Paradoxically, you can do a lot more with the mouse.  On the root window, the
left mouse button gives you a menu for starting various programs, both locally
and remotely, and also various window utilities.  The middle button gives you
direct access to the window manipulation utilities, and the right button gives a
drop-down list to select any of the currently active windows:
.XPSPIC -L -W 1i -0 images/fvwm-menu-2.ps
.XPSPIC -I 2.3i -W 2.5i -0 images/fvwm-right-menu.ps
.XPSPIC -I 1.3i -W .7i images/fvwm-middle-menu.ps
.Le
The menus above show one of the problems: look at those system names on the left
submenu (\fIdopey\fP, \fIsnoopy\fP\/ and friends).  They don't exist on our
sample network, and the chance of them existing on your network are pretty low
as well.  They're hard-coded in the sample configuration file,
.File /usr/X11R6/etc/system.fvwm2rc .
To use
.Command fvwm2
effectively, you'll have to modify the configuration file.  The best thing to do
is to make a copy of
.File /usr/X11R6/etc/system.fvwm2rc
in your own directory, as
.File -n ~/.fvwm2/.fvwm2rc .
Then you can have lots of fun tweaking the file to do exactly what you want it
to do.  Clearly, KDE is easier to set up.
.H2 "Changing the X display"
.X "Ctrl-Alt-Keypad -, keystroke"
.X "keystroke, Ctrl-Alt-Keypad -"
.X "Ctrl-Alt-Keypad +, keystroke"
.X "keystroke, Ctrl-Alt-Keypad +"
.X "X, changing screen resolution"
.X "screen resolution, changing under X"
When you set up your
.File XF86Config
file, you may have specified more than one resolution.  For example, on page
.Sref \*[screen-resolution] \&
we selected the additional resolution 1024x768 pixels.  When you start X, it
automatically selects the first resolution, in this case 640x480.  You can
change to the previous resolution (the one to the left in the list) by pressing
the \fBCtrl\fP-\fBAlt\fP-\fBKeypad -\fP key, and to the following resolution
(the one to the right in the list) with \fBCtrl\fP-\fBAlt\fP-\fBKeypad +\fP.
\fIKeypad +\fP\/ and \fIKeypad -\fP\/ refer to the \fB+\fP and \fB-\fP symbols
on the numeric keypad at the right of the keyboard; you can't use the \fB+\fP
and \fB-\fP symbols on the main keyboard for this purpose.  The lists wrap
around: in our example, if your current resolution is 640x480, and you press
\fBCtrl\fP-\fBAlt\fP-\fBKeypad -\fP, the display changes to 1024x768.  It's a
very good idea to keep the default resolution at 640x480 until you have debugged
your \fIXF86Config\fP\/ parameters: 640x480 almost always works, so if your
display is messed up, you can just switch back to a known good display with a
single keystroke.
.H3 "Selecting pixel depth"
You can configure most display boards to display a number of different pixel
depths (a different number of bits per pixel, which translates to a different
number of colours).  When you start X, however, it defaults to 8 bits per pixel
(256 colours), which is a very poor rendition.  To start it with a different
number, specify the number of planes.  For example, to start with 32 bits per
pixel (4,294,967,296 colours), enter:
.Dx
$ \f(CBstartx -\^- -bpp 32\fP
.De
With older display boards, which had relatively limited display memory, there
was a tradeoff between maximum resolution and maximum pixel depth.  Modern
display cards no longer have this limitation.  We'll look at this issue in more
detail on page
.Sref \*[pixel-depth] .
.H2 "Getting a shell"
As we saw at the beginning of the chapter, your main tools are the shell and the
editor, and that's what we saw on the sample screens.  But when you start X,
they're not there: you need to start them.
.P
In KDE, you have two ways to start a terminal window:
.Ls B
.LI
You can select the icon showing a monitor with a shell in front of it, third
from the left at the bottom of the example above.  This starts the
.Command konsole
terminal emulator.
.LI
You can start an
.Command xterm
by pressing \fBAlt-F2\fP.  You see a window like the one in the centre left of
Figure
.Sref \*[kde-main] ,
enter the text \f(CWxterm\fP (as shown) and press \f(CWRun\fP or the \fBEnter\fP
key.
.Le
Obviously the first is the intended approach, and it's easier.  Nevertheless, I
recommend using
.Command xterm
at least until you're sure you want to stick with
.Command -n kde \/:
there are some subtle differences, and
.Command konsole
is intended to work with
.Command -n kde
only.  If you do stick with KDE, you should change the configuration of the
.Command konsole
button to start
.Command xterm
instead; that's relatively straightforward.
.P
In
.Command fvwm2 ,
you start an
.Command xterm
from the left mouse menu, as shown above.
.H3 "Shell basics"
The most basic thing you can do with the shell is to start a program.  Consider
program names to be commands: like you might ask somebody to ``wash the dishes''
or ``mow the lawn,'' you can tell the shell to ``remove those files'':
.Dx
$ \f(CBrm file1 file2 file3\fP
.De
This starts a program called
.Command rm
(remove), and gives it a list of three file names to remove.
.P
If you're removing a whole lot of files, this could take a while.  Consider
removing the entire directory hierarchy
.File -n /usr/obj ,
which is created when building a new version of the system (see page
.Sref \*[buildworld] ).
This directory hierarchy contains about 15,000 files and directories, and it'll
take a while to remove it.  You can do this with the following command:
.Dx
# \f(CBrm -rf /usr/obj &\fP
.De
In this example, we have a couple of options led in by a hyphen (\f(CW-\fP) and
also the character \f(CW&\fP at the end of the line.
.Ls B
.LI
The \f(CWr\fP option tells
.Command rm
to \fIrecursively\fP\/ descend into subdirectories.  If you didn't specify this,
it would remove all files in the directory
.Directory /usr/obj
and then exit, complaining that it can't delete directories.
.LI
The \f(CWf\fP (\fIforce\fP\/) option tells
.Command rm
to continue on error; otherwise if anything goes wrong, it will stop.
.LI
The \f(CW&\fP character at the end of the line tells the shell (not
.Command rm )
to continue after starting the program.  It can run for some time, and there's
no need to wait for it.
.Le
.SPUP
.H3 "Options"
In the previous example, we saw a couple of options.  By convention, they come
between the command name and other parameters, and they're identified because
they start with a hyphen character (\f(CW-\fP).  There's a lot of variation,
though, depending on the individual program.
.Ls B
.LI
Sometimes, as in the previous example, options consist of a single letter and
can often be joined together.
.LI
Some programs, like
.Command tar
and
.Command ps ,
don't insist on the hyphen lead-in.  In Chapter
.Sref "\*[nchunixadmin]" ,
we'll see the command:
.Dx
# \f(CBps waux\fP
.De
.ne 5v
This command could equally well be written:
.Dx
# \f(CBps -waux\fP
.De
You may also come across programs that refuse to accept the hyphen at all.
.LI
.ne 4v
Sometimes options can have values.  For example, in Chapter
.Sref "\*[nchnetdebug]" \&
we'll see:
.Dx
# \f(CBtcpdump -i ppp0 host hub.freebsd.org\fP
.De
Here, \f(CWppp0\fP is an argument to the \f(CWi\fP option.  In some cases, it
must be written with a space; in others, it must be written without a space; and
in others again, it can be written either way.  Pay attention to this detail
when reading man pages.
.LI
In other cases, they can be keywords, in which case they need to be written
separately.  The GNU project is particularly fond of this kind of option.  For
example, when building the system you may see compiler invocations like these:
.Dx
cc -O -pipe -Dinline=rpcgen_inline -Wall -Wno-format-y2k -Wno-uninitialized \e
-D__FBSDID=__RCSID -c /usr/src/usr.bin/rpcgen/rpc_main.c
.De
With the exception of the last parameter, all of these texts are options, as the
hyphen suggests.
.LI
Options are specific to particular commands, though often several commands
attempt to use the same letters to mean the same sort of thing.  Typical ones
are \f(CWv\fP for verbose output, \f(CWq\fP for quiet output (i.e. less than
normal).
.LI
Sometimes you can run into problems when you supply a parameter that looks like
an option.  For example, how do you remove a file called
.File -n -rf ?
There are a number of solutions for this problem.  In this example, you could
write:
.Dx
$ \f(CBrm ./-rf\fP
.De
This is an alternative file naming convention that we'll look at again on page
.Sref \*[relative-paths] .
.Le
.SPUP
.H3 "Shell parameters"
.X "shell, parameters"
.X "parameters, shell"
.X "parsing"
.X "argument, shell"
.X "white space"
.X "space, white"
When you invoke a program with the shell, it first \fIparses\fP\/ the input line
before passing it to the program: it turns the line into a number of parameters
(called \fIarguments\fP\/ in the C programming language).  Normally the
parameters are separated by \fIwhite space\fP, either a space or a tab
character.  For example, consider the previous example:
.Dx
$ \f(CBrm file1 file2 file3\fP
.De
the program receives four arguments, numbered 0 to 3:
.br
.ne 1i
.Table-heading "Program arguments"
.TS
tab(#) ;
rfCWp9 | lfCWw65 .
\s10\fBArgument#\fBValue
_
0#rm
1#file1
2#file2
3#file3
.TE
.sp 1.5v
What happens if you want to pass a name with a space?  For example, you might
want to look for the text ``\f(CWMail rejected\fP'' in a log file.  UNIX has a
standard program for looking for text, called
.Command grep .
The syntax is:
.Dx
grep \f(CIexpression files
.De
Argument 1 is the expression; all additional arguments are the names of files to
search.  We could write:
.Dx
$ \f(CBgrep Mail rejected /var/log/maillog\fP
.De
but that would try to look for the text \f(CWMail\fP in the files
\fIrejected\fP\/ (probably causing an error message that the file did not exist)
and
.File /var/log/maillog
(where just about every line contains the text \f(CWMail\fP).  That's not what
we want.  Instead, we do pretty much what I wrote above:
.Dx
$ \f(CBgrep "Mail rejected" /var/log/maillog\fP
.De
In other words, if we put quote characters \f(CW""\fP around a group of words,
the shell will interpret them as a single parameter.  The first parameter that
is passed to
.Command grep
is \f(CWMail rejected\fP, not \f(CW"Mail rejected"\fP.
.P
This behaviour of the shell is a very good reason not to use file names with
spaces in them.  It's perfectly legitimate to embed spaces into UNIX file names,
but it's a pain to use.  If you want to create a file name that contains
several words, for example \fIAll\ files\ updated\ since\ last\ week\fP,
consider changing the spaces to underscores:
\fIAll_files_updated_since_last_week\fP.
.P
It's even more interesting to see what happens when you pass a globbing
character to a program, for example:
.Dx
$ \f(CBcc -o foo *.c\fP
.De
This invocation compiles all C source files (\fI*.c\fP\/) and creates a program
.Command -n foo .
If you do this with Microsoft, the C compiler gets four parameters, and it has
to find the C source files itself.  In UNIX, the shell expands the text
\fI*.c\fP\/ and replaces it with the names of the source files.  If there are
thirty source files in the directory, it will pass a total of 33 parameters to
the compiler.
.H3 "Fields that can contain spaces"
The solution to the ``Mail rejected'' problem isn't ideal, but it works well
enough as long as you don't have to handle fields with blanks in them too often.
In many cases, though, particularly in configuration files, fields with blanks
are relatively common.  As a result, a number of system configuration files use
a colon (\f(CW:\fP) as a delimiter.  This looks very confusing at first, but it
turns out not to be as bad as the alternatives.  We'll see some examples in the
\f(CWPATH\fP environment variable on page
.Sref \*[PATH] ,
in the password file on page
.Sref \*[master-passwd] ,
and in the login class file on page
.Sref \*[login-class] .
.H2 "Files and file names"
.Pn file-names
Both UNIX and Microsoft environments store disk data in \fIfiles\fP, which in
turn are placed in \fIdirectories\fP.  A file may be a directory: that is, it
may contain other files.
.X "file names"
.X "file name extension"
The differences between UNIX and Microsoft start with \fIfile names\fP.
Traditional Microsoft file names are rigid: a file name consists of eight
characters, possibly followed by a period and another three characters (the
so-called \fIfile name extension\fP\/).  There are significant restrictions on
which characters may be used to form a file name, and upper and lower case
letters have the same meaning (internally, Microsoft converts the names to UPPER
CASE).  Directory members are selected with a backslash (\fB\e\fP), which
conflicts with other meanings in the C programming language\(emsee page
\*[slash] for more details.
.P
FreeBSD has a very flexible method of naming files.  File names can contain any
character except \f(CW/\fP, and they can be up to 255 characters long.  They are
\fIcase-sensitive\fP\/: the names \fIFOO\fP, \fIFoo\fP\/ and \fIfoo\fP\/ are
three different names.  This may seem silly at first, but any alternative means
that the names must be associated with a specific character set.  How do you
upshift the German name \fIungleichm��ig\fP?  What if the same characters appear
in a Russian name?  Do they still shift the same?  The exception is because the
\f(CW/\fP\/ character represents directories.  For example, the name
.File -n /home/fred/longtext-with-a-long-name
represents:
.Ls N
.LI
First character is a \f(CW/\fP, representing the \fIroot file system\fP.
.LI
\fIhome\fP\/ is the name of a directory in the root file system.
.LI
\fIfred\fP\/ is the name of a directory in
.File -n /home .
.LI
The name suggests that
.File -n longtext-with-a-long-name
is probably a file, not a directory, though you can't tell from the name.
.Le
As a result, you can't use \f(CW/\fP in a file name. In addition, binary 0s (the
ASCII \f(CWNUL\fP\/ character) can confuse a lot of programs.  It's almost
impossible to get a binary 0 into a file name anyway: that character is used to
represent the end of a string in the C programming language, and it's difficult
to input it from the keyboard.
.P
Case sensitivity no longer seems as strange as it once did: web browsers have
made UNIX file names more popular with \fIUniform Resource Indicators\fP\/ or
\fIURIs\fP, which are derived from UNIX names.
.H3 "File names and extensions"
The Microsoft naming convention (name, period and extension) seems similar to
that of UNIX.  UNIX also uses extensions to represent specific kinds of files.
The difference is that these extensions (and their lengths) are implemented by
convention, not by the file system.  In Microsoft, the period between the name
and the extension is a typographical feature that only exists at the display
level: it's not part of the name.  In UNIX, the period is part of the name, and
names like \fIfoo.bar.bazzot\fP\/ are perfectly valid file names.  The system
doesn't assign any particular meaning to file name extensions; instead, it looks
for \fImagic numbers\fP, specific values in specific places in the file.
.H3 "Relative paths"
.Pn relative-paths
Every directory contains two directory entries,
.Directory .
and
.Directory ..
(one and two periods).  These are \fIrelative directory entries\fP\/:
.Directory .
is an alternative way to refer to the current directory, and
.Directory ..
refers to the parent directory.  For example, in
.Directory /home/fred ,
.Directory .
refers to
.Directory /home/fred ,
and
.Directory ..
refers to
.Directory /home .
The root directory doesn't have a parent directory, so in this directory only,
.Directory ..
refers to the same directory.  We'll see a number of cases where this is
useful.\*F
.FS
Interestingly, the Microsoft file systems also have this feature.
.FE
.H3 "Globbing characters"
.X "globbing characters"
.X "characters, globbing"
Most systems have a method of representing groups of file names and other names,
usually by using special characters for representing an abstraction.  The most
common in UNIX are the characters \f(CW*\fP, \f(CW?\fP and the square brackets
\f(CW[]\fP.  UNIX calls these characters \fIglobbing characters\fP.  The
Microsoft usage comes from UNIX, but the underlying file name representation
makes for big differences.  Table
.Sref \*[globbing-example] \&
gives some examples.
.br
.Table-heading "Globbing examples"
.TS
tab(#) ;
lfCWp9 | lw28 | lw28 .
\fBName#\fBMicrosoft meaning#\fBUNIX meaning
_
CONFIG.*#T{
All files with the name \fICONFIG\fP, no matter what their extension.
T}#T{
All files whose name starts with \fICONFIG.\fP, no matter what the rest is.
Note that the name contains a period.
T}
.sp .4v
CONFIG.BA?#T{
.nh
All files with the name \fICONFIG\fP\/ and an extension that starts with
\fIBA\fP, no matter what the last character.
.hy
T}#T{
All files that start with \fICONFIG.BA\fP\/ and have one more character in their
name.
T}
.sp .4v
*#T{
Depending on the Microsoft version, all files without an extension, or all
files.
T}#T{
All files.
T}
.sp .4v
*.*#T{
All files with an extension.
T}#T{
All files that have a period in the middle of their name.
T}
.sp .4v
foo[127]#T{
In older versions, invalid.  In newer versions with long file name support, the
file with the name \fIfoo[127]\fP.
T}#T{
The three files \fIfoo1\fP, \fIfoo2\fP\/ and \fIfoo7\fP.
T}
.TE
.sp 1.5v
.Tn globbing-example
.H3 "Input and output"
.Pn stdout
Most programs either read input data or write output data.  To make it easier,
the shell usually starts programs with at least three open files:
.Ls B
.LI
\fIStandard input\fP, often abbreviated to \fIstdin\fP, is the file that most
programs read to get input data.
.LI
\fIStandard output\fP, or \fIstdout\fP, is the normal place for programs to
write output data.
.LI
\fIStandard error output\fP, or \fIstderr\fP, is a separate file for programs to
write error messages.
.Le
With an interactive shell (one that works on a terminal screen, like we're
seeing here), all three files are the same device, in this case the terminal
you're working on.
.P
Why two output files?  Well, you may be collecting something important, like a
backup of all the files on your system.  If something goes wrong, you want to
know about it, but you don't want to mess up the backup with the message.
.H4 "Redirecting input and output"
But of course, even if you're running an interactive shell, you don't want to
back up your system to the screen.  You need to change \fIstdout\fP\/ to be a
file.  Many programs can do this themselves; for example, you might make a
backup of your home directory like this:
.Dx
$ \f(CBtar -cf /var/tmp/backup ~\fP
.De
This creates (option \f(CWc\fP) a file (option \f(CWf\fP) called
.File -n /var/tmp/backup ,
and includes all the files in your home directory
.Directory ( ~ ).
Any error messages still appear on the terminal, as \fIstderr\fP\/ hasn't been
changed.
.P
This syntax is specific to
.Command tar .
The shell provides a more general syntax for redirecting input and output
streams.  For example, if you want to create a list of the files in your current
directory, you might enter:
.Dx
$ \f(CBls -l\fP
  drwxr-xr-x   2 root  wheel    512 Dec 20 14:36 CVS
  -rw-r--r--   1 root  wheel   7928 Oct 23 12:01 Makefile
  -rw-r--r--   5 root  wheel    209 Jul 26 07:11 amd.map
  -rw-r--r--   5 root  wheel   1163 Jan 31  2002 apmd.conf
  -rw-r--r--   5 root  wheel    271 Jan 31  2002 auth.conf
  -rw-r--r--   1 root  wheel    741 Feb 19  2001 crontab
  -rw-r--r--   5 root  wheel    108 Jan 31  2002 csh.cshrc
  -rw-r--r--   5 root  wheel    482 Jan 31  2002 csh.login
\fI(etc)\fP\/
.De
You can redirect this output to a file with the command:
.Dx
$ \f(CBls -l > /var/tmp/etclist\fP
.De
This puts the list in the file
.File -n /var/tmp/etclist .
The symbol \f(CW>\fP tells the shell to redirect \fIstdout\fP\/ to the file
whose name follows.  Similarly, you could use the \f(CW<\fP to redirect
\fIstdin\fP\/ to that file, for example when using
.Command grep
to look for specific texts in the file:
.Dx
$ \f(CBgrep csh < /var/tmp/etclist\fP
  -rw-r--r--   5 root  wheel    108 Jan 31  2002 csh.cshrc
  -rw-r--r--   5 root  wheel    482 Jan 31  2002 csh.login
  -rw-r--r--   5 grog  lemis    110 Jan 31  2002 csh.logout
.De
In fact, though, there's a better way to do that: what we're doing here is
feeding the output of a program into the input of another program.  That happens
so often that there's a special method of doing it, called \fIpipes\fP\/:
.Dx
$ \f(CBls -l | grep csh\fP
  -rw-r--r--   5 root  wheel    108 Jan 31  2002 csh.cshrc
  -rw-r--r--   5 root  wheel    482 Jan 31  2002 csh.login
  -rw-r--r--   5 grog  lemis    110 Jan 31  2002 csh.logout
.De
The \f(CW|\fP symbol causes the shell to start two programs.  The first has a
special file, a \fIpipe\fP, as the output, and the second has the same pipe as
input.  Nothing gets written to disk, and the result is much faster.
.P
A typical use of pipes are to handle quantities of output data in excess of a
screenful.  You can pipe to the
.Command less \*F
.FS
Why
.Command less ?
Originally there was a program called
.Command more ,
but it isn't as powerful.
.Command less
is a new program with additional features, which proves beyond doubt that
.Command less
is
.Command more .
.FE
program, which enables you to page backward and forward:
.Dx
$ \f(CBls -l | less\fP
.De
Another use is to sort arbitrary data:
.Dx
$ \f(CBps aux | sort -n +1\fP
.De
This command takes the output of the
.Command ps
command and sorts it by the numerical (\f(CW-n\fP) value of its \fIsecond\fP\/
column (\f(CW+1\fP).  The first column is numbered 0.  We'll look at
.Command ps
on page
.Sref \*[ps-command] .
.H3 "Environment variables"
.X "environment variables"
.X "variables, environment"
.Pn environment-variables
.X "environment variables"
The UNIX programming model includes a concept called \fIenvironment
variables\fP.  This rather unusual sounding name is simply a handy method of
passing relatively long-lived information of a general nature from one program
to another.  It's easier to demonstrate the use than to describe.  Table
.Sref \*[environment-variable-table] \&
takes a look at some typical environment variables.  To set environment
variables from Bourne-style shells, enter:
.Dx
$ \f(CBexport TERM=xterm\fP
.De
This sets the value of the \f(CWTERM\fP variable to \f(CWxterm\fP.  The word
\f(CWexport\fP tells the shell to pass this information to any program it
starts.  Once it's exported, it stays exported.  If the variable isn't exported,
only the shell can use it.
.P
.ne 5v
Alternatively, if you want to set the variable only once when running a program,
and then forget it, you can set it at the beginning of a command line:
.Dx
$ \f(CBTERM=xterm-color mutt\fP
.De
This starts the
.Command mutt
mail reader (see page
.Sref \*[mutt] )
with
.Command xterm 's
colour features enabled.
.P
For
.Command csh
and
.Command tcsh ,
set environment variables with:
.Dx
% \f(CBsetenv TERM xterm\fP
.De
To start a process with these variables, enter:
.Dx
% \f(CBenv xterm-color mutt\fP
.De
.br
.Table-heading "Common environment variables"
.TS H
tab(#) ;
lfCWp9 | lw60 .
\fB\s10Name#\fBPurpose
_
.TH N
BLOCKSIZE#T{
The size of blocks that programs like
.Command df
count.  The default is 512 bytes, but it's often more convenient to use 1024 or
even 1048576 (1 MB).
T}
.sp .3v
DISPLAY#T{
When running X, the name of the X server.  For a local system, this is typically
\f(CWunix:0\fP.  For remote systems, it's in the form
.br
\f(CIsystem-name\fP\/:\f(CIserver-number\fP\/.\f(CIscreen-number\fP.  For the
system \fIbumble.example.org\fP, you would probably write \f(CWbumble.example.org:0\fP.
T}
.sp .3v
EDITOR#T{
The name of your favourite editor.  Various programs that start editors
look at this variable to know which editor to start.
T}
.sp .3v
HOME#T{
The name of your home directory.
T}
.sp .3v
.X "locale"
LANG#T{
The \fIlocale\fP\/
that you use.  This should be the name of a directory in
.Directory /usr/share/locale .
T}
.sp .3v
MAIL#T{
Some programs use this variable to find your incoming mail file.
T}
.sp .3v
MANPATH#T{
A list of path names, separated by colons (\f(CW:\fP), that specifies where the
.Command man
program should look for man pages.  A typical string might be
\f(CW/usr/share/man:/usr/local/man\fP, and specifies that there are man pages in
each of the directories
.Directory /usr/share/man
and
.Directory /usr/local/man .
T}
.sp .3v
NTAPE#T{
The name of the non-rewinding tape device.  See page
.Sref \*[tape-devices] \&
for more details.
T}
.sp .3v
PATH#T{
A list of path names, separated by colons (\f(CW:\fP), that specifies where the
shell should look for executable programs if you specify just the program name.
T}
.sp .3v
PS1#T{
In Bourne-style shells, this is the prompt string.  It's usually set to
\f(CB$\fP, but can be changed.  See page
.Sref \*[shell-prompt] \&
for a discussion of a possible prompt for
.Command bash .
T}
.sp .3v
PS2#T{
In Bourne-style shells, this is the prompt string for continuation lines.  It's
usually set to \f(CB>\fP.
T}
.sp .3v
SHELL#T{
The name of the shell.  Some programs use this for starting a shell.
T}
.sp .3v
TAPE#T{
The name of the rewinding tape device.  See page
.Sref \*[tape-devices] \&
for more details.
T}
.sp .3v
TERM#T{
The type of terminal emulation you are using.  This is very important: there is
no other way for an application to know what the terminal is, and if you set it
to the wrong value, full-screen programs will behave incorrectly.
T}
TZ#T{
Time zone.  This is the name of a file in
.Directory /usr/share/zoneinfo
that describes the local time zone.  See the section on timekeeping on page
.Sref \*[timekeeping] \&
for more details.
T}
.Tn environment-variable-table
.TE
.sp 1.5v
.Pn PATH
Note particularly the \f(CWPATH\fP variable.  One of the most popular questions
in the \f(CWFreeBSD-questions\fP mailing list is ``I have compiled a program,
and I can see it in my directory, but when I try to run it, I get the message
``\f(CWcommand not found\fP.''  This is usually because \f(CWPATH\fP does not
include the current directory.
.Aside
It's good practice \fInot\fP\/ to have your current directory or your home
directory in the \f(CWPATH\fP: if you do, you can be subject to security
compromises.  For example, somebody could install a program called
.Command -n ps
in the directory
.Directory -n /var/tmp .
Despite the name, the program might do something else, for example remove all
files in your home directory.  If you change directory to
.Directory -n /var/tmp
and run
.Command -n ps ,
you will remove all files in your home directory.  Obviously much more subtle
compromises are possible.
.P
Instead, run the program like this:
.Dx
$ \f(CB./program\fP
.De
.End-aside
.sp -1v
.Pn profile
You should set your \f(CWPATH\fP variable to point to the most common executable
directories.  Add something like this to your
.File .profile
file (for
Bourne-style shells):
.Dx
PATH=/usr/bin:/usr/local/bin:/usr/sbin:/bin:/sbin:/usr/X11R6/bin
export PATH
.De
This variable is of great importance: one of the leading problems that beginners
have is to have an incorrect \f(CWPATH\fP variable.
.H4 "Printing out environment variables"
So you can't start a program, and you're wondering whether your \f(CWPATH\fP
environment variable is set correctly.  You can find out with the
.Command echo
command:
.Dx
$ \f(CBecho $PATH\fP
/bin:/usr/bin
.De
The \f(CW$\fP at the beginning of \f(CW$PATH\fP tells the shell to substitute
the value of the environment variable for its name.  Without this, the shell has
no way of knowing that it's an environment variable, so it passes the text
\f(CWPATH\fP to
.Command echo ,
which just prints it out.
.P
If you want to print out all the environment variables, use the
.Command printenv
command:
.Dx
$ \f(CBprintenv | sort\fP
BLOCKSIZE=1048576
CLASSPATH=/usr/local/java/lib:/usr/local/java/lib/classes.zip:/home/grog/netscape/
CVSROOT=/home/ncvs
DISPLAY=freebie:0
EDITOR=emacs
HOME=/home/grog
PAGER=less
PATH=.:/usr/bin:/usr/sbin:/bin:/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin
XAUTHORITY=/home/grog/.Xauthority
.De
This example sorts the variables to make it easier to find them.  In all
probability, you'll find many more variables.
.H3 "Command line editing"
.X "command line editing"
.Pn control-chars
Typing is a pain.  If you're anything like me, you're continually making
mistakes, and you may spend more time correcting typing errors than doing the
typing in the first place.  It's particularly frustrating when you enter
something like:
.Dx
$ \f(CBgroff -rex=7.5 -r$$ -rL -rW -rN2 -mpic tmac.M unixerf.mm\f(CW
troff: fatal error: can't open `unixerf.mm': No such file or directory
.De
This command \fIshould\fP\/ create the PostScript version of this chapter, but
unfortunately I messed up the name of the chapter: it should have been
\fIunixref.mm\fP, and I typed \fIunixerf.mm\fP.
.Aside
Yes, I know this looks terrible.  In fact, UNIX has ways to ensure you almost
never need to write commands like this.  The command I really use to format this
chapter is ``\f(CWmake unixref\fP''.
.End-aside
It would be particularly frustrating if I had to type the whole command in
again.  UNIX offers a number of ways to make life easier.  The most obvious one
is so obvious that you tend to take it for granted: the \fBBackspace\fP key
erases the last character you entered.  Well, most of the time.  What if you're
running on a machine without a \fBBackspace\fP key?  You won't have that problem
with a PC, of course, but a lot of workstations have a \fBDEL\fP key instead of
a \fBBackspace\fP key.  UNIX lets you specify what key to use to erase the last
character entered.  By default, the erase character really is \fBDEL\fP, but the
shell startup changes it and prints out a message saying what it has done:
.Dx
 erase ^H, kill ^U, intr ^C, status ^T
.De
in the example on page
.Sref \*[login] .
\fB^H\fP (\fBCtrl-H\fP) is an alternative representation for \fBBackspace\fP.
.P
The three other functions \f(CWkill\fP, \f(CWintr\fP, and \f(CWstatus\fP perform
similar editing functions.  \f(CWkill\fP erases the whole line, and \f(CWintr\fP
stops a running program.
.Aside
.X "signal"
More correctly, \f(CWintr\fP sends a \fIsignal\fP\/ called \f(CWSIGINT\fP to
the process.  This normally causes a program to stop.
.End-aside
You'll notice that it is set to \fBCtrl-C\fP, so its function is very similar to
that of the MS-DOS \fBBreak\fP key.  \f(CWstatus\fP is an oddball function: it
doesn't change the input, it just displays a statistics message.
.Command bash
doesn't in fact use it: it has a better use for \fBCtrl-T\fP.
.P
In fact, these control characters are just a few of a large number of control
characters that you can set.  Table
.Sref \*[terminal-control-chars] \&
gives an overview of the more common control characters.  For a complete list,
see the man page \fIstty(1)\fP.
.br
.Table-heading "Terminal control characters"
.TS H
tab(%) ;
 lfCWp9 | lfCWp9 | lf(R)w54  .
\s10\fBName%\fBDefault%\fBFunction
_
.TH N
CR%\er%T{
Go to beginning of line.  Normally, this also terminates input (in other words,
it returns the complete line to the program, which then acts on the input).
T}
.sp .4v
NL%\en%T{
End line.  Normally, this also terminates input.
T}
.sp .4v
INTR%Ctrl-C%T{
Generate a \s10\f(CWSIGINT\fR\s0 signal.  This normally causes the process
to terminate.
T}
.sp .4v
QUIT%Ctrl-|%T{
Generate a \s10\f(CWSIGQUIT\fR\s0 signal.  This normally causes the process
to terminate and \fIcore dump\fP, to save a copy of its memory to disk for later
analysis.
T}
.sp .4v
ERASE%DEL%T{
Erase last character.  FreeBSD sets this to \fBBackspace\fP on login, but
under some unusual circumstances you might find it still set to \fBDEL\fP.
T}
.sp .4v
KILL%Ctrl-U%T{
Erase current input line.
T}
.sp .4v
EOF%Ctrl-D%T{
Return end-of-file indication.  Most programs stop when they receive an EOF.
T}
.sp .4v
STOP%Ctrl-S%T{
Stop output.  Use this to examine text that is scrolling faster than you can
read.
T}
.sp .4v
START%Ctrl-Q%T{
Resume output after stop.
T}
.sp .4v
SUSP%Ctrl-Z%T{
Suspend process.  This key generates a \s10\f(CWSIGTSTP\fR\s0 signal when typed.
This normally causes a program to be suspended.  To restart, use the
.Command fg
command.
T}
.sp .4v
DSUSP%Ctrl-Y%T{
Delayed suspend.
Generate a \s10\f(CWSIGTSTP\fR\s0 signal when the character is read.  Otherwise,
this is the same as \f(CWSUSP\fP.
T}
.sp .4v
REPRINT%Ctrl-R%T{
Redisplay all characters in the input queue (in other words, characters that
have been input but not yet read by any process).  The term "print" recalls the
days of harcopy terminals.  Many shells disable this function.
T}
.sp .4v
DISCARD%Ctrl-O%T{
Discard all terminal output until another \s10\f(CWDISCARD\fR\s0
character arrives, more input is typed or the program clears the
condition.
T}
.TE
.Tn terminal-control-chars
.sp 1.5v
.ne 4v
To set these characters, use the
.Command stty
program.  For example, if you're used to erasing the complete input line with
\fBCtrl-X\fP, and specifying an end-of-file condition with \fBCtrl-Z\fP, you
could enter:
.Dx
$ \f(CBstty susp \e377 kill ^X eof ^Z\fP
.De
You need to set \f(CWSUSP\fP to something else first, because by default it is
\f(CWCtrl-Z\fP, so the system wouldn't know which function to perform if you
press \f(CW^Z\fP.
.Aside
The combination \f(CW\e377\fP represents the character octal 377 (this notation
comes from the C programming language, and its origin is lost in the mists of
time, back in the days when UNIX ran on PDP-11s).  This character is the
``null'' character that turns off the corresponding function.  System V uses
the character \f(CW\e0\fP for the same purpose.
.End-aside
In this particular case, \f(CW^X\fP really does mean the character \f(CW^\fP
followed by the letter \f(CWX\fP, and not \fBCtrl-X\fP, the single character
created by holding down the \fBControl\fP character and pressing \fBX\fP at the
same time.
.H3 "Command history and other editing functions"
.X "command history"
.Pn command-history
Nowadays, most shells supply a \fIcommand history\fP\/ function and additional
functionality for editing it.  We'll take a brief look at these features
here\(emfor more details, see the man pages for your shell.
.P
Shell command line editing has been through a number of evolutionary phases.
The original Bourne shell supplied no command line editing at all, though the
version supplied with FreeBSD gives you many of the editing features of more
modern shells.  Still, it's unlikely that you'll want to use the Bourne shell as
your shell:
.Command bash ,
.Command ksh ,
and
.Command zsh
are all compatible with the Bourne shell, but they also supply better command
line editing.
.P
The next phase of command line editing was introduced with the C shell,
.Command csh .
By modern standards, it's also rather pitiful.  It's described in the
.Command csh
man page if you really want to know.  About the only part that is still useful
is the ability to repeat a previous command with the \f(CW!!\fP construct.
.Pn set-cle
Modern shells supply command line editing that resembles the editors
.Command vi
or \fIEmacs\fP.  In
.Command bash ,
.Command sh ,
.Command ksh ,
and
.Command zsh
you can
make the choice by entering:
.Dx
$ \f(CBset -o emacs\fP                          \fIfor Emacs-style editing\fP\/
$ \f(CBset -o vi\fP                             \fIfor vi-style editing\fP\/
.De
In
.Command tcsh ,
the corresponding commands are:
.Dx
% \f(CBbind emacs\fP
% \f(CBbind vi\fP
.De
Normally you put one of these commands in your startup file.
.P
In \fIEmacs\fP\/ mode, you enter the commands simply by typing them in.  In
\fIvi\fP\/ mode, you have to press \fBESC\fP first.  Table
.Sref \*[bash-cli-edit] \&
shows an overview of the more typical Emacs-style commands in
.Command bash .
Many other shells supply similar editing support.
.P
As the name suggests, the
.X "emacs, command"
.X "command, emacs"
.Command -n Emacs
editor understands the same editing characters.  It also understands many more
commands than are shown here.  In addition, many X-based commands, including web
browsers, understand some of these characters.
.\"XXX
.br
.Table-heading "Emacs editing characters"
.TS H
tab(#) ;
lf(B)w12 | lw57  .
Key#\fBFunction
_
.TH N
T{
Ctrl-A
T}#T{
Move to the beginning of the line.
T}
T{
LeftArrow
T}#T{
Move to previous character on line.
T}
T{
Ctrl-B
T}#T{
Move to previous character on line (alternative).
T}
T{
Ctrl-D
T}#T{
Delete the character under the cursor.  Be careful with this character: it's
also the shell's end-of-file character, so if you enter it on an empty line, it
stops your shell and logs you out.
T}
T{
Ctrl-E
T}#T{
Move to the end of the line.
T}
T{
RightArrow
T}#T{
Move to next character on line.
T}
T{
Ctrl-F
T}#T{
Move to next character on line (alternative).
T}
T{
Ctrl-K
T}#T{
Erase the rest of the line.  The contents are saved to a \fIring buffer\fP\/ of
erased text and can be restored, possibly elsewhere, with \fBCtrl-Y\fP.
T}
T{
Ctrl-L
T}#T{
Erase screen contents (shell) or redraw window (\fIEmacs\fP\/).
T}
T{
DownArrow
T}#T{
Move to next input line.
T}
T{
Ctrl-N
T}#T{
Move to next input line (alternative).
T}
T{
UpArrow
T}#T{
Move to previous input line.
T}
T{
Ctrl-P
T}#T{
Move to previous input line (alternative).
T}
T{
Ctrl-R
T}#T{
Incremental search backward for text.
T}
T{
Ctrl-S
T}#T{
Incremental search forward for text.
T}
T{
Ctrl-T
T}#T{
Transpose the character under the cursor with the character before the cursor.
T}
T{
Ctrl-Y
T}#T{
Insert previously erased with \fBCtrl-K\fP or \fBAlt-D\fP.
T}
T{
Ctrl-_
T}#T{
Undo the last command.
T}
T{
Alt-C
T}#T{
Capitalize the following word.
T}
T{
Alt-D
T}#T{
Delete the following word.
T}
T{
Alt-F
T}#T{
Move forward one word.
T}
T{
Alt-L
T}#T{
Convert the following word to lower case.
T}
T{
Alt-T
T}#T{
Transpose the word before the cursor with the one after it.
T}
T{
Alt-U
T}#T{
Convert the following word to upper case.
T}
T{
Ctrl-X Ctrl-S
T}#T{
Save file
.X "command, emacs"
.X "emacs, command"
.Command -n ( Emacs \&
only).
T}
T{
Ctrl-X Ctrl-C
T}#T{
Exit the
.Command -n Emacs
editor.
T}
.TE
.Tn bash-cli-edit
.sp 1.5v
You'll note a number of alternatives to the cursor keys.  There are two reasons
for them: firstly, the shell and \fIEmacs\fP\/ must work on systems without
arrow keys on the keyboard.  The second reason is not immediately obvious: if
you're a touch-typer, it's easier to type \fBCtrl-P\fP than take your hands away
from the main keyboard and look for the arrow key.  The arrows are good for
beginners, but if you get used to the control keys, you'll never miss the arrow
keys.
.H4 "File name completion"
.X "file name completion"
As we have seen, UNIX file names can be much longer than traditional Microsoft
names, and it becomes a problem to type them correctly.  To address this
problem, newer shells provide \fIfile name completion\fP.  In \fIEmacs\fP\/
mode, you typically type in part of the name, then press the \fBTab\fP key.  The
shell checks which file names begin with the characters you typed.  If there is
only one, it puts in the missing characters for you.  If there are none, it
beeps (rings the ``terminal bell'').  If there are more than one, it puts in as
many letters as are common to all the file names, and then beeps.  For example,
if I have a directory
.Directory docco
in my home directory, I might enter:
..if review
| (bmc) Except in `ksh'.  In most versions of `ksh', including the `pdksh'
| that's available for FreeBSD, file name completion is done via "ESC ESC"
| in emacs mode, and "ESC \" in vi mode, by default.  `pdksh' permits you to
| set force tab completion: `set -o vi-tabcomplete' does it for vi mode.  It
| only appears to affect vi mode, though.  To force tab completion in emacs
| mode, you have to use
|       bind '^I'=complete-file
..endif
.Dx
=== grog@freebie (/dev/ttyp4) ~ 14 -> cd \f(CBdoc\f(CWco/
=== grog@freebie (/dev/ttyp4) ~/docco 15 -> \f(CBls\f(CW
freebsd.faq  freebsd.fbc  freeware
=== grog@freebie (/dev/ttyp4) ~/docco 16 -> \f(CBemacs f\fPree\fIbeep\f(CBb\f(CWsd.f\fIbeep\f(CBaq
.De
Remember that my input is in \f(CBconstant width bold\fP font, and the shell's
output is in \f(CWconstant width\fP font.  On the first line, I entered the
characters \f(CWcd doc\fP followed by a \fBTab\fP character, and the shell
completed with the text \f(CWco/\fP.  On the last line, I entered the characters
\f(CWemacs f\fP and a \fBTab\fP.  In this case, the shell determined that there
was more than one file name that started like this, so it added the letters
\f(CWree\fP and rang the bell.  I entered the letter \f(CWb\fP and pressed
\fBTab\fP again, and the shell added the letters \f(CWsd.f\fP and beeped again.
Finally, I added the letters \f(CWaq\fP to complete the file name
\fIfreebsd.faq\fP.
.P
Command line completion in \fIvi\fP\/ mode is similar: instead of pressing
\fBTab\fP, you press \fBESC\fP twice.
.H3 "Shell startup files"
.X "shell, startup files"
.Pn bashrc
As we saw above, there are a lot of ways to customize your shell.  It would be
inconvenient to have to set them every time, so all shells provide a means to
set them automatically when you log in.  Nearly every shell has its own startup
file.  Table
.Sref \*[.rcnames] \&
gives an overview.
.P
.br
.Table-heading "Shell startup files"
.TS H
tab(#) ;
lI | lI .
\fBShell#\fBstartup file
_
.TH
bash#.profile\fR, then\fP .bashrc
csh#.login\fP on login, always\fP .cshrc
sh#.profile
tcsh#.login\fP on login, always\fI .tcshc\/\fR,\fI .cshrc \fRif\fI .tcshrc\fR not found
.TE
.Tn .rcnames
.sp 1.5v
.P
.ne 6v
These files are shell scripts\(emin other words, straight shell commands.
Figure
.Sref \*[.bashrc] \&
shows a typical
.File .bashrc
file to set the environment variables we discussed.
.Dx
umask 022
export BLOCKSIZE=1024   # for df
export CVSROOT=/src/ncvs
export EDITOR=/opt/bin/emacs
export MANPATH=/usr/share/man:/usr/local/man
export MOZILLA_HOME=/usr/local/netscape
export PAGER=less
export PATH=/usr/bin:/usr/local/bin:/usr/sbin:/bin:/sbin:/usr/X11R6/bin
PS1="=== \eu@\eh (`tty`) \ew \e# -> "
PS2="\eu@\eh \ew \e! ++ "
export SHELL=/usr/local/bin/bash
export TAPE=/dev/nsa0   # note non-rewinding as standard
if [ "$TERM" = "" ]; then
  export TERM=xterm
fi
if [ "$DISPLAY" = "" ]; then
  export DISPLAY=:0
fi
/usr/games/fortune              # print a fortune cookie
.De
.Figure-heading "Minimal .bashrc file"
.Fn .bashrc
It would be tedious for every user to put settings in their private
initialization files, so the shells also read a system-wide default file.  For
the Bourne shell family, it is
.File /etc/profile ,
while the C shell family has
three files:
.File /etc/csh.login
to be executed on login,
.File /etc/csh.cshrc
to be executed when a new shell is started after you log in, and
.File /etc/csh.logout
to be executed when you stop a shell.  The start files are executed before the
corresponding individual files.
.P
In addition, login classes (page
.Sref \*[login-class] )
offer another method of setting environment variables at a global level.
.H3 "Changing your shell"
.Pn chsh
The FreeBSD installation gives \f(CWroot\fP a C shell,
.Command csh .
This is the traditional BSD shell, but it has a number of disadvantages: command
line editing is very primitive, and the script language is significantly
different from that of the Bourne shell, which is the \fIde facto\fP\/ standard
for shell scripts: if you stay with the C shell, you may still need to
understand the Bourne shell.  The latest version of the Bourne shell
.Command sh
also includes some command line editing.  See page
.Sref \*[set-cle] \&
for details of how to enable it.
.P
If you want to stay with a
.Command csh -like
shell, you can get better command line editing with
.Command tcsh ,
which is also in the base system.  You can get both better command line editing
and Bourne shell syntax with
.Command bash ,
in the Ports Collection.
.P
If you have \f(CWroot\fP access, you can use
.Command vipw
to change your shell, but there's a more general way: use
.Command chsh
(\fIChange Shell\fP\/).  Simply run the program.  It starts your favourite
editor (as defined by the \f(CWEDITOR\fP environment variable).  Here's an
example before:
.Dx
.X "Velte, Jack"
#Changing user database information for velte.
Shell: /bin/csh
Full Name: Jack Velte
Location:
Office Phone:
Home Phone:
.De
You can change anything after the colons.  For example, you might change this
to:
.Dx
#Changing user database information for velte.
Shell: \f(CB/usr/local/bin/bash\fP
Full Name: Jack Velte
Location: \f(CBOn the road\fP
Office Phone: \f(CB+1-408-555-1999\fP
Home Phone:
.De
.Command chsh
checks and updates the password files when you save the modifications and exit
the editor.  The next time you log in, you get the new shell.
.Command chsh
tries to ensure you don't make any mistakes\(emfor example, it won't let you
enter the name of a shell that isn't mentioned in the file
.File /etc/shells \(embut
it's a \fIvery\fP\/ good idea to check the shell before logging out.  You can
try this with
.Command su ,
which you normally use to become super user:
.Dx
bumble# \f(CBsu velte\fP
Password:
su-2.00$                                \fInote the new prompt\fP\/
.De
You might hear objections to using
.Command bash
as a root shell.  The argument goes something like this:
.Command bash
is installed in
.Directory /usr/local/bin ,
so it's not available if you boot into single-user mode, where only the root
file system is available.  Even if you copy it to, say,
.Directory /bin ,
you can't run it in single-user mode because it needs libraries in
.Directory /usr/lib .
.P
In fact, this isn't a problem.  If you install the system the way I recommend in
Chapter
.Sref \*[nchinstall] ,
.Directory /usr
\fIis\fP\/ on the root file system.  Even if it isn't, though, you don't have to
use
.Command bash
in single-user mode.  When you boot to single-user mode, you get a prompt asking
you which shell to start, and suggesting
.Command -n /bin/sh .
.br
.ne 14v
.H2 "Differences from Microsoft"
If you're coming from a Microsoft background, there are a few gotchas that you
might trip over.
.H3 "Slashes: backward and forward"
.X "root, directory"
.X "slash, character"
.X "escape, character"
.X "character, slash"
.X "character, escape"
.Pn slash
\f(CW/\fP (slash) and \f(CW\e\fP (backslash) are confusing.  As we've seen, UNIX
uses \f(CW/\fP to delimit directories.  The backslash \f(CW\e\fP is called an
\fIescape character\fP.  It has several purposes:
.Ls B
.LI
You can put it in front of another special character to say ``don't interpret
this character in any special way.''  We've seen that the shell interprets a
space character as the end of a parameter.  In the previous example we changed
\f(CWMail rejected\fP to \f(CW"Mail rejected"\fP to stop the shell from
interpreting it.  We could also have written: \f(CWMail\e\ rejected\fP.
.P
.X "quoting"
A more common use for this \fIquoting\fP\/ is to tell the shell to ignore the
end of a line.  If a command line in a shell script gets too long, you might
like to split it up into several lines; but the shell sees the end of a line as
a go-ahead to perform the command.  Stop it from doing so by putting a backslash
\fIimmediately\fP\/ before the end of the line:
.Dx
$ \f(CBgrep \e
  "Mail rejected" \e
   /var/log/maillog\fP
.De
Don't put any spaces between the \f(CW\e\fP and the end of the line; otherwise
the shell will interpret the first space as a parameter by itself, and then it
will interpret the end of line as the end of the command.
.LI
.X "control characters"
.X "character, control"
In the C programming language, the backslash is used to represent several
\fIcontrol characters\fP.  For example, \f(CW\en\fP means ``new line.''  This
usage appears in many other places as well.
.LI
Using \f(CW\e\fP as an escape character causes problems: how do we put a
\f(CW\e\fP character on a line?  The answer: quote it.  Write \f(CW\e\e\fP when
you mean \f(CW\e\fP.  This causes particular problems when interfacing with
..if Samba
Microsoft\(emsee page
.Sref \*[multibackslash] \&
for an example.
..else
Microsoft: if you give a Microsoft path name to a shell, it needs the doubled
backslashes: \fIC:\e\eWINDOWS\fP.
..endif
.Le
.H3 "Tab characters"
.X "Tab characters"
.X "character, tab"
We've seen that the shell treats ``white space,'' either spaces or tab
characters, as the same.  Unfortunately, some other programs do not.
.Command make ,
.Daemon sendmail
and
.Daemon syslogd
make a distinction between the two kinds of characters, and they all require
tabs (not spaces) in certain places.  This is a \fIreal\fP\/ nuisance, because
hardly any editor makes a distinction between them.
.H3 "Carriage control characters"
.X "carriage return, character"
.X "line feed, character"
.X "character, carriage return"
.X "character, line feed"
In the olden days, the standard computer terminal was a Teletype, a kind of
computer-controlled electric typewriter.  When the carriage, which contained the
print head, got to the end of a line, it required two mechanical operations to
move to the beginning of the next line: the \fIcarriage return\fP\/ control
character told it to move the carriage back to the beginning of the line, and
the \fIline feed\fP\/ character told it turn the platen to the next line.
.P
.X "new line, character"
.X "character, new line"
Generations of computer systems emulated this behaviour by putting both
characters at the end of each text line.  This makes it more difficult to
recognize the end of line, it uses up more storage space, and normally it
doesn't buy you much.  The implementors of UNIX decided instead to use a single
character, which it calls the \fInew line\fP\/ character.  For some reason, they
chose the line feed to represent new line, though the character generated by
\fBEnter\fP is a carriage return.  As we saw above, the C programming language
represents it as \f(CW\en\fP.
.P
This causes problems transferring data between FreeBSD and Microsoft, and also
when printing to printers that still expect both characters.  We'll look at the
file transfer issues on page
.Sref \*[Microsoft-staircase] \&
and the printer issues on page
.Sref \*[staircase] .
.H2 "The Emacs editor"
.Pn Emacs
Apart from the shell, your second most important tool is the \fIeditor\fP, a
program that creates and changes texts.  Another divergence of concept between
UNIX and Microsoft environments is that UNIX gives you a choice of editors in
just about anything you do.  Microsoft products frequently try to redefine the
whole environment, so if you change mailers, you may also have to change the
editor you use to write mail.  This has a profound effect on the way you work.
In particular, the Microsoft way makes it uninteresting to write a really good
editor, because you can't use it all the time.
.P
The standard BSD editor is
.Command vi ,
about which people speak with a mixture of admiration, awe and horror.
.Command vi
is one of the oldest parts of BSD.  It is a very powerful editor, but nobody
would say that it is easy to learn.  There are two reasons to use
.Command vi \/:
.Ls N
.LI
If you're already an experienced \fIvi\fP\/ hacker, you probably won't want to
change.
.LI
If you do a lot of work on different UNIX systems, you can rely on
.Command vi
being there.  It's about the only one on which you can rely.
.Le
If, on the other hand, you don't know
.Command vi ,
and you only work on systems whose software you can control, you probably
shouldn't use
.Command vi .
\fIEmacs\fP\/ is much easier to learn, and it is more powerful than
.Command vi .
.PIC "images/emacs.init.ps" 4i
.sp 1.5v
.Figure-heading "Emacs main menu"
.Fn emacs-init
When running under X, \fIEmacs\fP\/ displays its own window
.Command ( vi \&
uses an
.Command xterm
under these circumstances).  As a result, if you start \fIEmacs\fP\/ from an
.Command xterm ,
you should use the \f(CW&\fP character to start it in the background:
.Dx
$ \f(CBemacs &\fP
.De
Figure
.Sref \*[emacs-init] \&
shows the resulting display.  As you can see, the first thing that \fIEmacs\fP\/
offers you is a tutorial.  You should take it.  You'll also notice the menu bars
at the top.  Although they look primitive compared to graphics toolbars, they
offer all the functionality of graphics-oriented menus.  In addition, they will
tell you the keystrokes that you can use to invoke the same functions.  Figure
.Sref \*[emacs-files-menu] \&
gives an example of the \fIFiles\fP\/ menu.
.P
There is a lot of documentation for \fIEmacs\fP, much of it on line.  The
complete \fIEmacs\fP\/ handbook is available via the \fIinfo\fP\/ mode of
\fIEmacs\fP, which is described in the tutorial.  If that's not enough, read
\fILearning GNU Emacs\fP, by Debra Cameron, Bill Rosenblatt and Eric Raymond.
.PIC "images/emacs.menu.ps" 4i
.Figure-heading "Emacs files menu"
.Fn emacs-files-menu
.X "Files, menu"
.H2 "Stopping the system"
To stop X, press the key combination \fBCtrl\fP-\fBAlt\fP-\fBBackspace\fP, which
is deliberately chosen to resemble the key combination
\fBCtrl\fP-\fBAlt\fP-\fBDelete\fP used to reboot the machine.
\fBCtrl\fP-\fBAlt\fP-\fBBackspace\fP stops X and returns you to the virtual
terminal in which you started it.  If you run from
.Daemon xdm ,
it redisplays a login screen.
.P
To stop the system, use the
.Command shutdown
program.  To do so, you need to be a member of group \f(CWoperator\fP.
.P
By default, KDE uses the
.Command halt
program.  Only \f(CWroot\fP can use this program, so you should reconfigure KDE
to use
.Command shutdown .
After this, you can shut down from KDE with the keystroke combination
\fBCtrl-Alt-PageDown\fP.
