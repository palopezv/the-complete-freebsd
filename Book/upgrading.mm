.\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\"
.\" To do: add info about disabled devices
.\"
.\" $Id: upgrading.mm,v 4.14 2003/11/17 05:28:23 grog Exp grog $
.Chapter \*[nchupgrading] "Updating the system software"
.Pn make-world
.X "make world"
In the previous chapter, we looked at how to get an up-to-date FreeBSD source
tree.  Once you have the sources, you can build various components of the
system.  The main tool we use for this purpose is
.Command make ,
which we looked at on page
.Sref \*[make] .
The best way to think of upgrading the system is that everything is a matter of
changing files.  For the purposes of this discussion, you can divide the files
on your system into the following categories:
.Ls B
.LI
The \fIuserland\fP, that part of the system software that is not the kernel.
Unlike some other operating systems, FreeBSD expects to keep userland and kernel
at the same release level.  We'll look at the interaction between kernel and
userland below.
.LI
The kernel.  You may build a new kernel without updating the sources, of course,
if you want to add functionality to the kernel.  In this chapter we'll look at
upgrading the kernel in the context of a complete system upgrade.  We'll
consider building a custom kernel in the next chapter,
.Sref "\*[chbuild]" .
.LI
Support for booting the machine, which is currently performed as a separate
step.
.LI
Configuration files relating to your system.  Some of them, such as
.File /etc/fstab
and
.File /etc/rc.conf ,
overlap with the previous category.
.LI
The Ports Collection.  This doesn't have to be done at the same time as userland
and kernel, though if you upgrade to a significant new version of FreeBSD, it's
a good idea to upgrade the ports as well.  We looked at upgrading ports on page
.Sref "\*[portsutils]" .
.LI
Your own files.  They have nothing to do with a software upgrade.
.Le
.ne 5v
You can make upgrading less onerous by planning in advance.  Here are some
suggestions:
.Ls B
.LI
Keep system files and user files on different file systems.
.LI
Keep careful records of which configuration files you change, for example with
\fIRCS\fP, the \fIRevision Control System\fP.  This proves to be the most
complicated part of the entire upgrade process.
.Le
The only files that are upgraded are on the traditional root file system
and
.Directory /usr .
No others are affected by an upgrade.  Table
.Sref \*[dirowners] ,
an abridged version of Table
.Sref \*[hierarchy] \&
on page
.Sref  \*[hierarchy-page] ,
gives an overview of where the system files come from.
.ad l
.ne 10
.ds Section*title FreeBSD directory hierarchy
.Table-heading "FreeBSD directory hierarchy"
.Tn dirowners
.TS H
tab(#) ;
 lfI | lw34 | lw23  .
\fBdirectory
\fBname#\fBUsage#\fBPopulated by
_
.TH N
.X "directory, /bin"
/bin#T{
Executable programs of general use.
T}#T{
\fImake world\fP\/
T}
.X "directory, /boot"
/boot#T{
Files used when booting the system.
T}#T{
\fImake install\fP\/ in
.Directory /usr/src/sys .
T}
.X "directory, /dev"
.X "/dev, directory"
/dev#T{
Directory of device nodes.
T}#T{
.nh
System startup (\fIdevfs\fP\/)
.hy
T}
.X "directory, /etc"
/etc#T{
Configuration files used at system startup.
T}#T{
Install from CD-ROM only, \fImergemaster\fP, administrator
T}
.X "directory, /sbin"
\fI/sbin#T{
System executables needed at system startup time.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/X11R6"
/usr/X11R6#T{
The X11 windowing system.
T}#T{
X-based programs in the Ports Collection
T}
.X "directory, /usr/bin"
/usr/bin#T{
Standard executable programs that are not needed at system start.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/compat"
/usr/compat#T{
A directory containing code for emulated systems, such as Linux.
T}#T{
Ports Collection
T}
.X "directory, /usr/games"
/usr/games#T{
Games.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/include"
/usr/include#T{
Header files for programmers.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/lib"
/usr/lib#T{
Library files.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/libexec"
/usr/libexec#T{
Executable files that are not started directly by the user.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/libdata"
/usr/libdata#T{
Miscellaneous files used by system utilities.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/local"
/usr/local#T{
Additional programs that are not part of the operating system.
T}#T{
Ports collection
T}
.X "directory, /usr/obj"
/usr/obj#T{
Temporary object files created when building the system.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/ports"
/usr/ports#T{
The Ports Collection.
T}#T{
.Command sysinstall ,
\fIcvs\fP\/
T}
.X "directory, /usr/sbin"
/usr/sbin#T{
System administration programs that are not needed at system startup.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/share"
/usr/share#T{
Miscellaneous read-only files, mainly informative.
T}#T{
\fImake world\fP\/
T}
.X "directory, /usr/src"
/usr/src#T{
System source files.
T}#T{
.Command sysinstall ,
\fIcvs\fP\/
T}
_
.TE
.ad b
.H2 "Upgrading kernel and userland"
.Pn buildworld
The core part of a system upgrade consists of a synchronized update of both
kernel and userland.  It's relatively simple to do, but depending on the speed
of the machine, it may keep the computer busy for several hours.  In general,
you build and install the userland first, then you build and install the kernel.
.P
The traditional way to build the userland is:
.Dx
# \f(CBcd /usr/src\fP
# \f(CBmake world\fP
.De
This operation performs a number of functions, which can be influenced by
variables you pass to \fImake\fP.  Without any variables, \fImake world\fP\/
performs the following steps:
.Ls B
.LI
It removes the old build directories and creates new ones.  You can skip this
step by setting the \f(CWNOCLEAN\fP variable.  Don't set \f(CWNOCLEAN\fP unless
you know exactly why you are doing so, since it can cause inconsistencies that
come back to bite you later.  In particular, if you do have problems after
building the world in this manner, you should first go back and perform a
complete rebuild without \f(CWNOCLEAN\fP.
.LI
It rebuilds and installs build tools, including \fImake\fP, the C compiler and
the libraries.
.LI
It builds the rest of the system, with the exception of the kernel and the boot
tools.
.LI
It installs everything.  You can omit this stage by building the
\f(CWbuildworld\fP target instead of \f(CWworld\fP.
.Le
It does this by building a number of subtargets.  Occasionally, you might find
it useful to build them individually: \fImake world\fP\/ can pose a
chicken-and-egg problem.  It creates the userland, and \fImake kernel\fP\/ makes
the kernel.  Userland and kernel belong together, and if you upgrade the
userland first, you may find that the new userland takes advantage of
differences in the newer version of the kernel.  A typical situation is when a
new system call is added to the kernel.  In this case, you may find processes
exiting with a signal 12 (invalid system call).  If this happens, you may have
to perform the upgrade with the sequence:
.Dx
# \f(CBmake buildworld\fP
# \f(CBmake kernel\fP
(\fIreboot\fP\/)
# \f(CBmake installworld\fP
.De
.ne 3v
You'll find information about such requirements in the file
.File /usr/src/UPDATING .
Table \*[build-targets] gives an overview of the more useful targets to the
top-level \fIMakefile\fP.
.Table-heading "Targets for top-level \fIMakefile\fP\/"
.TS H
tab(#) ;
 lfCWp9 | lw55  .
\fBTarget#\fBPurpose
_
.TH
buildworld#Rebuild everything, including glue to help do upgrades.
.sp .4v
installworld#Install everything built by \f(CWbuildworld\fP.
.sp .4v
world#Perform \f(CW buildworld\fP and \f(CWinstallworld\fP.
.sp .4v
update#Update your source tree.
.sp .4v
most#Build user commands, no libraries or include files.
.sp .4v
installmost#Install user commands, but not libraries or include files.
.sp .4v
reinstall#T{
If you have a build server, you can NFS mount the source and object directories
and do a \f(CWmake reinstall\fP on the \fIclient\fP\/ to install new binaries
from the most recent build on the server.
T}
.sp .4v
buildkernel#T{
Build a kernel for your architecture.  By default, use the \f(CWGENERIC\fP
kernel configuration file.  You can select a different configuration file, say
\f(CWMYKERNEL\fP, with:
.Dx
# \f(CBmake buildkernel KERNCONF=MYKERNEL\fP
.De
By default, this target builds all the KLDs (Kernel Loadable Modules), which
significantly increases the time it takes.  If you know that your KLDs will not
change, or that you won't be using any, you can skip building them by specifying
the \f(CW-DNO_MODULES\fP flag.
T}
.sp .4v
installkernel#T{
Install a kernel you have built with \f(CWbuildkernel\fP.
T}
.sp .4v
reinstallkernel#T{
Install a kernel you have built with \f(CWbuildkernel\fP.  Don't rename the
previous kernel directory to
.Directory -n kernel.old .
Use this target when the previous kernel is not worth keeping.
T}
.sp .4v
kernel#T{
Build and install a kernel.
T}
.TE
.P
Another issue is that the system configuration might have changed.  For example,
in early 2002 the default configuration for \fIsendmail\fP\/ changed.  The
process added a daemon user and group, both called \f(CWsmmsp\fP.  To install
the userland, this user already needed to be present.
.P
.ne 10v
The solution to this issue is called \fImergemaster\fP, a script that helps you
to upgrade the configuration files.  We'll look at it in more detail below, but
at this point you should know that you need to run it with the \f(CW-p\fP
(\fIpre-build\fP\/) option:
.Dx 1
# \f(CBmergemaster -p\fP
.De
As we've seen in table
.Sref "\*[dirowners]" ,
the \f(CWinstallworld\fP target changes a number of directories.  Sometimes,
though, it leaves old binaries behind: it doesn't remove anything that it
doesn't replace.  The result can be that you end up using old programs that
have long passed their use-by date.  One solution to this problem is to look at
the last modification date of each program in the directories.  For example, if
you see:
.Dx
$ \f(CBls -lrt /usr/sbin\fP
-r-xr-xr-x  1 root  wheel       397 Jul 14 11:36 svr4
-r-xr-xr-x  1 root  wheel       422 Jul 14 11:29 linux
-r-xr-xr-x  1 root  wheel    142080 Jul 13 17:20 sshd
\&...
-r-xr-xr-x  1 root  wheel     68148 Jul 13 17:16 uuchk
-r-xr-xr-x  1 root  wheel      6840 Jan  5  2002 ispppcontrol
-r-xr-xr-x  1 root  wheel     27996 Apr 21  2001 k5stash
-r-xr-xr-x  1 root  wheel     45356 Apr 21  2001 ktutil
-r-xr-xr-x  1 root  wheel     11124 Apr 21  2001 kdb_util
-r-xr-xr-x  1 root  wheel      6768 Apr 21  2001 kdb_init
.De
It's fairly clear that the files dated April 2001 have not just been installed,
so they must be out of date.  You can use a number of techniques to delete them;
one might be:
.Dx
# \f(CBfind . -mtime +10 | xargs rm\fP
.De
This command removes all files in the current directory
.Directory -n ( . )
that are older than 10 days (\f(CW+10\fP).  Of course, this method will only
work if you haven't installed anything in these directories yourself.  You
shouldn't have done so; that's the purpose of the directory hierarchy
.Directory /usr/local ,
to ensure that you keep system files apart from ports and private files.
.P
Be careful with
.Directory /usr/lib \/:
a number of ports refer to libraries in this directory hierarchy, and if you
delete them, the ports will no longer work.  In general there's no problem with
old libraries in
.Directory /usr/lib ,
unless they take up too much space, so you're safer if you don't clean out this
directory hierarchy.
.P
.Tn build-targets
Note that you need to specify the \f(CWKERNCONF\fP parameter to all the targets
relating to kernel builds.
.H2 "Upgrading the kernel"
There are two reasons for building a new kernel: it might be part of the upgrade
process, which is what we'll look at here, or you may build a kernel from your
current sources to add functionality to the system.  We'll look at this aspect
in Chapter
.Sref \*[nchbuild] .
.P
.ne 3v
One point to notice is that if you're upgrading from an older custom
configuration file, you could have a lot of trouble.  We'll see a strategy for
minimizing the pain on page
.Sref "\*[kernel-upgrade]" .
In addition, when upgrading to FreeBSD Release 5 from an older release of
FreeBSD, you need to install a file
.File /boot/device.hints ,
which you can typically copy from
.File /usr/src/sys/i386/conf/GENERIC.hints \/:
.Dx
# \f(CBcp /usr/src/sys/i386/conf/GENERIC.hints /boot/device.hints\fP
.De
See page
.Sref "\*[IO-config]" \&
for more details.
.P
.ne 6v
When upgrading the kernel, you might get error messages like this one:
.Dx
# \f(CBconfig GENERIC \fP
config: GENERIC:71: devices with zero units are not likely to be correct
.De
Alternatively, you might get a clearer message:
.Dx
# \f(CBconfig GENERIC\fP
\&../../conf/files: coda/coda_fbsd.c must be optional, mandatory or standard
Your version of config(8) is out of sync with your kernel source.
.De
Apart from that, you might find that the kernel fails to link with lots of
undefined references.  This, too, could mean that the
.Command config
program is out of synchronization with the kernel modules.  In each case,
build and install the new version of
.Command config \/:
.Dx
# \f(CBcd /usr/src/usr.sbin/config\fP
# \f(CBmake depend all install clean\fP
.De
.X "make clean"
You need to \fImake clean\fP\/ at the end since this method will store the
object files in non-standard locations.
.H2 "Upgrading the boot files"
At the time of writing, it's still necessary to install the files in
.Directory /boot
separately.  It's possible that this requirement will go away in the future.
There are two steps: first you build and install the boot files in the
.Directory /boot
directory, then you install them on your boot disk.  Assuming your system disk
is the SCSI disk
.Device da0 ,
you would perform some of the following steps.
.Dx
# \f(CBcd /usr/src/sys\fP                       \fIbuild directory\fP\/
# \f(CBmake install\fP                          \fIbuild and install the bootstraps\fP\/
# \f(CBbsdlabel -B da0\fP                       \f(BIEither\fI, for a dedicated disk\f(CW
# \f(CBbsdlabel -B da0s1\fP                     \f(BIOr\fI, for a PC disk slice\f(CW
# \f(CBboot0cfg -B da0\fP                       \f(BIOr\fI, booteasy for a dedicated PC disk\f(CW
.De
If you have a dedicated disk, which is normal on a non-Intel platform, use the
first
.Command bsdlabel
invocation to install the bootstrap
.Command -n ( boot1 )
at the beginning of the disk.  Otherwise, install
.Command -n boot1
at the beginning of your FreeBSD slice and use
.Command boot0cfg
to install the  
.Command -n boot0
boot manager at the beginning of the disk.
.H2 "Upgrading the configuration files"
Currently, the system build procedure does not install the configuration files
in
.Directory /etc .
You need to do that separately.  There are two possible methods:
.Ls B
.LI
Do it manually:
.Ls
.LI
Backup the old configuration files.  They're not very big, so you can probably
make a copy on disk somewhere.
.LI
Install pristine new configuration files:
.Dx
# \f(CBcd /usr/src/etc/\fP
# \f(CBmake install\fP
.De
.sp -1v
.LI
Compare the files and update the new ones with information from your
configuration.
.Le
.LI
Use \fImergemaster\fP, a semi-automatic method of doing effectively the same
thing.
.Le
The simple method is: run
.Command mergemaster
with the options \f(CW-i\fP and \f(CW-a\fP, which tell it to run automatically
(in other words, not to stop and ask questions), and to install new files
automatically.  That doesn't mean intelligently: you may run into problems
anyway.
.P
.Command mergemaster
produces a lot of output, and some of it in the middle is important, so you
should save the output to disk with the
.Command tee
command.  The first time you try, you might see:
.Dx
# \f(CBmergemaster -ia 2>&1 | tee -a /var/tmp/merge\fP

*** Creating the temporary root environment in /var/tmp/temproot
 *** /var/tmp/temproot ready for use
 *** Creating and populating directory structure in /var/tmp/temproot

set - `grep "^[a-zA-Z]" /usr/src/etc/locale.deprecated`;  while [ $# -gt 0 ] ;  do
for dir in /usr/share/locale  /usr/share/nls  /usr/local/share/nls;  do  test -d /va
r/tmp/temproot/${dir} && cd /var/tmp/temproot/${dir};  test -L "$2" && rm -rf "$2";
 test \! -L "$1" && test -d "$1" && mv "$1" "$2";  done;  shift; shift;  done
mtree -deU  -f /usr/src/etc/mtree/BSD.root.dist -p /var/tmp/temproot/
\&./bin missing (created)
\&./boot missing (created)
\fI\&...\fP\/
\&./vm missing (created)
mtree -deU  -f /usr/src/etc/mtree/BSD.sendmail.dist -p /var/tmp/temproot/
mtree: line 10: unknown user smmsp
*** Error code 1

Stop in /usr/src/etc.

  *** FATAL ERROR: Cannot 'cd' to /usr/src/etc and install files to
      the temproot environment
.De
These messages are somewhat misleading.  First, the files that are created are
all in
.Directory /var/tmp/temproot .
In addition, the message \f(CWCannot 'cd' to /usr/src/etc\fP does not refer to
any problem with that directory; it's just an indication that it can't continue
with the installation due to the previous errors.
.P
The real issue here is that the user \f(CWsmmsp\fP doesn't exist.  As we saw
above, this user was added some time in 2002 to address some mail security
problems.  It's in the new
.File /etc/master.passwd
file, but it's not in the one on the system.  But how do you merge the two
files?  One way would to be to use
.Command mergemaster
with the \f(CW-p\fP option, but then
.Command mergemaster
prompts you for every single file that it finds to be different, usually about
300 of them.  In addition, the editing facilities are relatively basic.  It's
better to edit the file in advance with an editor.
.H3 "Merging the password file"
As we saw on page
.Sref "\*[adding-user]" ,
the password file is quite complicated.  Depending on how much work you want to
do, you have a couple of possibilities:
.Ls B
.LI
You can choose to completely replace the old
.File /etc/master.passwd
with the new one.  This will cause all added user names and passwords to
disappear, so unless this is just a test machine, it's unlikely you'll want to
follow this path.
.LI
You can take advantage of the fact that, with the exception of \f(CWroot\fP, the
distribution
.File /etc/master.passwd
contains no ``real'' users.  You can merge the entries for real users with the
entries in the distribution
.File /etc/master.passwd .
This works relatively well, but it removes the passwords of the system users, so
you have to set them again.  We'll look at how to do that below.
.Le
The distribution version of
.File /etc/master.passwd
looks something like this:
.Dx
# $FreeBSD: src/etc/master.passwd,v 1.33 2002/06/23 20:46:44 des Exp $
#
root::0:0::0:0:Charlie &:/root:/bin/csh
toor:*:0:0::0:0:Bourne-again Superuser:/root:
\fI\&...etc\fP\/
.De
The individual fields are separated by colons (\f(CW:\fP).  We'll look at only
the fields that interest us in the following expansion.  It's easier to look at
if they're separated by spaces; numerically, they're the first, second, eighth,
ninth and tenth fields.  For a description of the other fields, see the man page
\fImaster.passwd(4)\fP.
.Dx
.ft I
User        password        GECOS                                   home directory                             shell
.ft
root          Charlie &               /root                   /bin/csh
toor      *   Bourne-again Superuser  /root
daemon    *   Owner of many processes /root                   /sbin/nologin
operator  *   System &                /                       /sbin/nologin
bin       *   Binaries Commands       /                       /sbin/nologin
tty       *   Tty Sandbox             /                       /sbin/nologin
kmem      *   KMem Sandbox            /                       /sbin/nologin
games     *   Games pseudo-user       /usr/games              /sbin/nologin
news      *   News Subsystem          /                       /sbin/nologin
man       *   Mister Man Pages        /usr/share/man          /sbin/nologin
sshd      *   Secure Shell Daemon     /var/empty              /sbin/nologin
smmsp     *   Sendmail Submission     /var/spool/clientmqueue /sbin/nologin
mailnull  *   Sendmail Default User   /var/spool/mqueue       /sbin/nologin
bind      *   Bind Sandbox            /                       /sbin/nologin
xten      *   X-10 daemon             /usr/local/xten         /sbin/nologin
pop       *   Post Office Owner       /nonexistent            /sbin/nologin
www       *   World Wide Web Owner    /nonexistent            /sbin/nologin
nobody    *   Unprivileged user       /nonexistent            /sbin/nologin
.De
The first field is the name of the user.  In the course of time, a number of
pseudo-users have been added to reduce exposure to security issues.  The main
issue in merging the files is to add these users.  If you don't have the user in
your current
.File /etc/master.passwd ,
you can add the line from the distribution file.
.P
The second field contains the \fIpassword\fP.  In the distribution file, it's
usually \f(CW*\fP, which means it needs to be set before you can log in at all.
Only \f(CWroot\fP has no password; you need to be able to log in as root to set
passwords.  By contrast, in your installed
.File /etc/master.passwd ,
you will almost certainly have a password, and in general you will want to keep
it.
.P
The home directory entry has not changed much.  You'll notice directory names
like
.Directory /nonexistent
and
.Directory /var/empty .
The former is a fake, the latter a directory that can't be changed.  It's
possible that this entry will change from one release to another, and it's
important to get it correct.
.P
For many accounts, the \fIshell\fP\/ field contains the name
.Command /sbin/nologin ,
which prints the text ``This account is currently not available'' and exits.
Currently only \f(CWroot\fP has a real shell, but that could change.
.P
To update the
.File /etc/master.passwd ,
you can use the following method:
.Ls B
.LI
\f(BIMake a copy of your old /etc/master.passwd!\fP
.LI
Maintain a strict separation of the original lines from the distribution file
and your own entries.  This will help you with the next update.
.LI
Copy the entire distribution
.File /etc/master.passwd
to the top of your
.File /etc/master.passwd
file.  At this point you will have a number of duplicates.
.LI
Check the entries for \f(CWroot\fP.  You can probably remove the distribution
entry and leave your entry in the file, preserving the password and shell.  In
this case, you should make an exception to the separation between distribution
and local additions: due to the way the name lookups work, if you put user
\f(CWroot\fP below user \f(CWtoor\fP (``root'' spelt backwards, and the same
user with possibly a different shell), all files will appear to belong to
\f(CWtoor\fP instead of to \f(CWroot\fP.
.LI
Check what other entries you have for user ids under 1000.  You can probably
remove them all, but if you have installed ports that require their own user ID,
you will need to keep them.
.LI
You should be able to keep all the entries for users with IDs above and
including 1000, with the exception of user \f(CWnobody\fP (ID 65534).  Use the
entry from the distribution file for \f(CWnobody\fP.
.Le
Once you have merged the files, you need to run
.Command pwd_mkdb
to rebuild the password files
.File /etc/passwd ,
.File /etc/pwd.db
and
.File /etc/spwd.db .
.File /etc/passwd
is gradually going out of use, but you probably have one on your system, and
some ports use it, so it's preferable to recreate it.  Do this with the
\&\f(CW-p\fP  option to
.Command pwd_mkdb \/:
.Dx 1
# \f(CBpwd_mkdb -p /etc/master.passwd\fP
.De
.SPUP
.ne 10v
.H2 "Merging /etc/group"
In addition to
.File /etc/master.passwd ,
you will probably need to upgrade
.File /etc/group .
In this case, the main issue is to add users to the \f(CWwheel\fP group.  The
distribution
.File /etc/group
looks like this:
.Dx
# $FreeBSD: src/etc/group,v 1.27 2002/10/14 20:55:49 rwatson Exp $
#
wheel:*:0:root
daemon:*:1:
kmem:*:2:
sys:*:3:
tty:*:4:
operator:*:5:root
mail:*:6:
bin:*:7:
news:*:8:
man:*:9:
games:*:13:
staff:*:20:
sshd:*:22:
smmsp:*:25:
mailnull:*:26:
guest:*:31:
bind:*:53:
uucp:*:66:
xten:*:67:
dialer:*:68:
network:*:69:
www:*:80:
nogroup:*:65533:
nobody:*:65534:
.De
Again, new groups have appeared for security reasons.  Use a similar method to
the one you used for
.File /etc/master.passwd :
.Ls B
.LI
\f(BIMake a copy of your old /etc/group!\fP
.LI
Maintain a strict separation of the original lines from the distribution file
and your own entries.  This will help you with the next update.
.LI
Copy the entire distribution
.File /etc/group
to the top of your
.File /etc/group
file.  At this point you will have a number of duplicates.
.LI
Check the entries for \f(CWwheel\fP.  You can probably remove the distribution
entry and leave your entry in the file, preserving the users.
.LI
In addition, you may have some users in other groups.  For example, installing
.Daemon postfix \&
adds the user \f(CWpostfix\fP to group \f(CWmail\fP.  You need to preserve these
users.
.Le
You don't need to do anything special after updating
.File /etc/group .
You can now continue with
.Command mergemaster .
.H2 "Mergemaster, second time around"
Before running
.Command mergemaster
again, you should delete the contents of
.Directory /var/tmp/temproot .
Otherwise you might see something like:
.Dx
*** The directory specified for the temporary root environment,
    /var/tmp/temproot, exists.  This can be a security risk if untrusted
    users have access to the system.
.De
.Command mergemaster
does not delete the old directories: you should do so yourself.  If this file
already exists,
.Command mergemaster
ignores it and creates a new directory with a name like
.File -n /var/tmp/temproot.0917.02.18.06 .
The numbers are a representation of the date and time of creation.
.P
.Command mergemaster
doesn't make it easy to remove the
.Directory /var/tmp/temproot
directory.  You may see:
.Dx
# \f(CBrm -rf /var/tmp/temproot\fP
rm: /var/tmp/temproot/var/empty: Operation not permitted
rm: /var/tmp/temproot/var: Directory not empty
rm: /var/tmp/temproot: Directory not empty
.De
The problem here is that the directory
.Directory /var/empty
has been set \fIimmutable\fP.  Change that with the
.Command chflags
command and try again:
.Dx
# \f(CBfind /var/tmp/temproot|xargs chflags noschg\fP
# \f(CBrm -rf /var/tmp/temproot\fP
.De
Run
.Command mergemaster
in the same way as before, saving the output.  If you haven't deleted the old
.Directory /var/tmp/temproot
directory, you might see:
.Dx
# \f(CBmergemaster -ia 2>&1 | tee -a /var/tmp/merge\fP
*** Creating the temporary root environment in /var/tmp/temproot.1102.15.01.14
 *** /var/tmp/temproot.1102.15.01.14 ready for use
 *** Creating and populating directory structure in /var/tmp/temproot.1102.15.01.14

set - `grep "^[a-zA-Z]" /usr/src/etc/locale.deprecated`;  while [ $# -gt 0 ] ;  do
for dir in /usr/share/locale  /usr/share/nls  /usr/local/share/nls;  do  test -d /va
r/tmp/temproot.1102.15.01.14/${dir} && cd /var/tmp/temproot.1102.15.01.14/${dir};  t
est -L "$2" && rm -rf "$2";  test \! -L "$1" && test -d "$1" && mv "$1" "$2";  done;
  shift; shift;  done
mtree -deU  -f /usr/src/etc/mtree/BSD.root.dist -p /var/tmp/temproot.1102.15.01.14/
\&./bin missing (created)
\&./boot missing (created)
\&./boot/defaults missing (created)
\&./boot/kernel missing (created)
\&./boot/modules missing (created)
\&./
\fI\&...\fP\/
install -o root -g wheel -m 644 /dev/null  /var/tmp/temproot.1102.15.01.14/var/run/u
tmp
install -o root -g wheel -m 644 /usr/src/etc/minfree  /var/tmp/temproot.1102.15.01.1
4/var/crash
cd /usr/src/etc/..; install -o root -g wheel -m 444  COPYRIGHT /var/tmp/temproot.110
2.15.01.14/
cd /usr/src/etc/../share/man; make makedb
makewhatis /var/tmp/temproot.1102.15.01.14/usr/share/man

*** Beginning comparison

 *** Temp ./etc/defaults/rc.conf and installed have the same CVS Id, deleting
 *** Temp ./etc/defaults/pccard.conf and installed have the same CVS Id, deleting
   *** ./etc/defaults/periodic.conf will remain for your consideration
 *** Temp ./etc/gnats/freefall and installed have the same CVS Id, deleting
 *** Temp ./etc/isdn/answer and installed have the same CVS Id, deleting
 *** Temp ./etc/isdn/isdntel.sh and installed have the same CVS Id, deleting
\fI\&...\fP\/

*** Comparison complete

*** Files that remain for you to merge by hand:
/var/tmp/temproot.1102.15.01.14/etc/defaults/periodic.conf
/var/tmp/temproot.1102.15.01.14/etc/mail/freebsd.mc
/var/tmp/temproot.1102.15.01.14/etc/mail/freebsd.cf
/var/tmp/temproot.1102.15.01.14/etc/mail/sendmail.cf
/var/tmp/temproot.1102.15.01.14/etc/mail/freebsd.submit.cf
/var/tmp/temproot.1102.15.01.14/etc/mail/mailer.conf
/var/tmp/temproot.1102.15.01.14/etc/mtree/BSD.include.dist
/var/tmp/temproot.1102.15.01.14/etc/mtree/BSD.local.dist
/var/tmp/temproot.1102.15.01.14/etc/mtree/BSD.usr.dist
/var/tmp/temproot.1102.15.01.14/etc/mtree/BSD.var.dist
/var/tmp/temproot.1102.15.01.14/etc/pam.d/su
/var/tmp/temproot.1102.15.01.14/etc/periodic/security/100.chksetuid
/var/tmp/temproot.1102.15.01.14/etc/periodic/security/200.chkmounts
/var/tmp/temproot.1102.15.01.14/etc/periodic/security/500.ipfwdenied
/var/tmp/temproot.1102.15.01.14/etc/periodic/security/600.ip6fwdenied
/var/tmp/temproot.1102.15.01.14/etc/periodic/security/700.kernelmsg
/var/tmp/temproot.1102.15.01.14/etc/rc.d/local
/var/tmp/temproot.1102.15.01.14/etc/crontab
/var/tmp/temproot.1102.15.01.14/etc/inetd.conf
/var/tmp/temproot.1102.15.01.14/etc/motd
/var/tmp/temproot.1102.15.01.14/etc/syslog.conf


*** You chose the automatic install option for files that did not
    exist on your system.  The following were installed for you:
      /etc/periodic/security/510.ipfdenied
      /etc/periodic/security/security.functions
      /etc/mac.conf
.De
You're not done yet: there are 21 files above that need looking at.  There's a
good chance that you've never heard of some of them, let alone changed them.  If
you \fIknow\fP\/ for a fact that you have never changed them, for example if you
have religiously kept track of your changes with RCS, you don't need to bother:
.Command mergemaster
errs on the side of safety.  You may have changed others, though.  The most
obvious one above is
.File /etc/crontab ,
which contains system-wide commands to be executed by
.Daemon cron .
To compare them, use
.Command diff \/:
.Dx
$ \f(CBdiff -wu /etc/crontab /var/tmp/temproot.1102.15.01.14/etc/crontab\fP
--- /var/tmp/crontab    Sat Nov  2 16:27:02 2002
+++ /var/tmp/temproot.1102.15.01.14/etc/crontab Sat Nov  2 15:01:16 2002
@@ -1,6 +1,6 @@
 # /etc/crontab - root's crontab for FreeBSD
 #
-# $FreeBSD: src/etc/crontab,v 1.21 1999/12/15 17:58:29 obrien Exp $
+# $FreeBSD: src/etc/crontab,v 1.31 2001/02/19 02:47:41 peter Exp $
 #
 SHELL=/bin/sh
 PATH=/etc:/bin:/sbin:/usr/bin:/usr/sbin
@@ -10,19 +10,18 @@
 #
 */5    *       *       *       *       root    /usr/libexec/atrun
 #
+# save some entropy so that /dev/random can reseed on boot
+*/11   *       *       *       *       operator /usr/libexec/save-entropy
+#
 # rotate log files every hour, if necessary
 0      *       *       *       *       root    newsyslog
 #
 # do daily/weekly/monthly maintenance
-59     1       *       *       *       root    periodic daily
-30     3       *       *       6       root    periodic weekly
+1      3       *       *       *       root    periodic daily
+15     4       *       *       6       root    periodic weekly
 30     5       1       *       *       root    periodic monthly
 #
 # time zone change adjustment for wall cmos clock,
-# does nothing if you have UTC cmos clock.
+# does nothing, if you have UTC cmos clock.
 # See adjkerntz(8) for details.
-# 1,31 0-5     *       *       *       root    adjkerntz -a
+1,31   0-5     *       *       *       root    adjkerntz -a
-0,30   *       *       *       *       build   /home/build/build_farm/build_test 2>
 /home/build/cron.err
-0      21      *       *       *       root    /usr/local/bin/cleanup
-0      7       *       *       *       grog    /home/grog/bin/update-FreeBSD-cvs
-1       *       *       *       *       root    (cd /usr/local/etc/postfix; make) 2
>/dev/null >/dev/null
.De
The lines starting with \f(CW-\fP show lines only in the old file, which is
still in
.File /etc/crontab .
The lines starting with \f(CW+\fP show lines only in the new file, which is in
.File -n /var/tmp/temproot.1102.15.01.14/etc/crontab .
There are a number of changes here: the CVS ID (\f(CW$FreeBSD$\fP) has changed
from 1.21 to 1.31, and the times of the periodic maintenance have changed.  In
the meantime, though, you have added other tasks (the bottom four lines), and
you have also commented out the periodic invocation of
.Command adjkerntz .
These are the changes you need to make to the new
.File /etc/crontab
before you install it.
.P
There's a simpler possibility here, though: the only real change that would
then be left in
.File /etc/crontab
is the change in the starting times for the daily and weekly housekeeping.  Does
that matter?  If you want, you don't need to change anything: the old
.File /etc/crontab
is fine the way it is.
.P
There's a whole list of files that you're likely to change from the defaults.
Here are some more likely candidates:
.Ls B
.LI
You may find it necessary to change
.File /etc/syslog.conf .
If so, you may have to merge by hand, but it shouldn't be too difficult.
.LI
You will almost certainly change
.File /etc/fstab .
About the only reason why you might need to merge changes would be if the file
format changes, which it hasn't done for over 20 years.
.LI
.File /etc/motd
contains the login greeting.  There's never a reason to take the new version.
.LI
.File /etc/inetd.conf
can be a problem: as new services are introduced, it changes.  At the same time,
you may have added services via ports, or enabled services in the manner we will
see on page
.Sref "\*[inetd]" .
You definitely need to merge this one yourself.
.LI
If you're using
.Daemon postfix ,
don't install the distribution version of
.File /etc/mail/mailer.conf .
It will reenable
.Daemon sendmail ,
which can cause significant problems.
.LI
If you have changed anything in
.File /etc/sysctl.conf ,
you'll need to move the changes to the new file.
.Le
.ig
Alternative strategy;

 # mv /etc /ETC
 # mkdir /etc
 # cd /ETC; cp -p group passwd master.passwd pwd.db spwd.db /etc
 # mv /ETC/RCS /etc
 # mergemaster
 # rcsdiff -wu /etc/RCS/*
   (fix)
 # cd /ETC; cp -p resolv.conf ntp.conf ntp.drift localtime /etc

Files to remove from RCS:

  /etc/RCS:
  used 1 available 68
  drwxr-xr-x  2 root  wheel   1024 Jun 30 02:01 .
  drwxr-xr-x  5 root  wheel   1024 Jun 30 02:01 ..
  -r--r--r--  1 root  wheel    995 Jun 30 02:00 crontab,v
  -r--r--r--  1 root  wheel   3091 Jan 14 03:42 devd.conf,v
D -r--r--r--  1 root  wheel    897 Jun 30 02:00 dumpdates,v
  -r--r--r--  1 root  wheel   1525 Jun 30 01:56 fstab,v
  -r--r--r--  1 root  wheel    701 Jun 30 02:00 group,v
  -r--r--r--  1 root  wheel   5392 Jun 30 02:00 inetd.conf,v
  -r--------  1 root  wheel   2147 Jun 30 02:00 master.passwd,v
  -r--r--r--  1 root  wheel    497 Jun 30 02:00 ntp.conf,v
D -r--r--r--  1 root  wheel    185 Jun 30 02:00 ntp.drift,v
D -r--r--r--  1 root  wheel   1878 Jun 30 02:00 passwd,v
D -r--r--r--  1 root  wheel  41137 Jun 30 02:01 pwd.db,v
  -r--r--r--  1 root  wheel   2687 Jan 13 05:16 rc.conf,v
  -r--r--r--  1 root  wheel    381 Jan 23 00:22 rc.local,v
D -r--r--r--  1 root  wheel    246 Jun 30 02:01 resolv.conf,v (maybe: DHCP?)
  -r--r--r--  1 root  wheel    420 Jun 30 02:01 shells,v
D -r--------  1 root  wheel  41140 Jun 30 02:01 spwd.db,v
  -r--r--r--  1 root  wheel    434 Jun 30 02:01 start_if.wi0,v
D                                               localtime


Subdirectories:

namedb
ssh
mail

--------------------------------------

Date: 27 Jul 2003 17:08:05 -0400
From: "Karl Vogel" <vogelke@pobox.com> 
To: grog@FreeBSD.org
Cc: kirk@strauser.com
Subject: Maintaining config files under RCS

>> On Sat, 26 Jul 2003 09:57:20 +0930, 
>> Greg 'groggy' Lehey <grog@FreeBSD.org> said:

G> A much better approach, which I'll describe in the next edition of the
G> book, is to maintain all config files under RCS.  This gives you more
G> than one backup.

   http://www.dnaco.net/~vogelke/Software/Configuration_Management/System_Files
   describes our production setup for doing that.

Date: 28 Jul 2003 12:40:26 -0400
From: "Karl Vogel" <vogelke@pobox.com>
To: grog@FreeBSD.org
Subject: Re: Maintaining config files under RCS

>> In an earlier message, I said:

K> http://www.dnaco.net/~vogelke/Software/Configuration_Management/System_Files
K> describes our production setup for doing that.

>> On Mon, 28 Jul 2003 09:54:36 +0930, 
>> Greg 'groggy' Lehey <grog@FreeBSD.org> said:

G> Interesting.  I haven't had time to look at it in detail, but it looks
G> good.  Can we use it in the FreeBSD project?

   Certainly!

-- 
Karl Vogel                      I don't speak for the USAF or my company
vogelke at pobox dot com                   http://www.pobox.com/~vogelke



Date: Wed, 23 Jul 2003 14:26:17 -0700
From: Peter Wemm <peter@wemm.org>
To: src-committers@FreeBSD.org, cvs-src@FreeBSD.org,
        cvs-all@FreeBSD.org
Subject: Re: cvs commit: src/sys/conf kern.pre.mk 
X-Mailer: exmh version 2.5 07/13/2001 with nmh-1.0.4

Peter Wemm wrote:
> peter       2003/07/23 13:10:09 PDT
> 
>   FreeBSD src repository
> 
>   Modified files:
>     sys/conf             kern.pre.mk 
>   Log:
>   Turn -Werror back on.  
  
Note that if you run into something that doesn't compile cleanly yet, you
can use 'make WERROR=' when building the kernel.




..
