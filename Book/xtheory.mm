\" This file is in -*- nroff-fill -*- mode
.\" STATUS: 4th edition
.\" $Id: xtheory.mm,v 4.15 2003/08/19 03:34:24 grog Exp grog $
.\"
..if article
.ce 2
\s17Setting up the X Window System\s0
.sp
\s14Greg Lehey (\f(CWgrog@lemis.com)\fP\s0
.sp .4i
.2C
.\" .nr Fs .5 \" Footnote spacing
Most people know Microsoft's ``Windows'' series of ``Operating Systems.''  From
a UNIX point of view, they're not really operating systems at all: their main
function is display management.  UNIX systems use a separate software package
for display management, usually the X window system, often called simply
\fIX\fP.  Some people call it \fIX windows\fP, but the X Consortium, who are
responsible for X, frown on this term.  To quote the documentation:
.P
.in +.1i
.ll -.2i
The X Consortium requests that the following names be used when referring to
this software:
.sp
.ce 5
X
.br
X Window System
.br
X Version 11
.br
X Window System, Version 11
.br
X11
.P
\fI``X Window System''\fP\/ is a trademark of X Consortium, Inc.
.br
.in
.ll
.P
X is the most popular display manager, at least partially because it is free.
As a result, nearly every UNIX-like free operating system supplies an
implementation of X.  On the Intel platforms, the best-known implementation is
called \fIXFree86\fP.
.P
XFree86 goes back to a display board driver written by Thomas R�ll and Mark
Snitily, and called \fIX386\fP.  This driver was originally free, but Thomas
R�ll subsequently developed it into a commercial product and created a company,
\fIXInside\fP, to market it.  At about the same time, the XFree86 project was
created to maintain the old, free version.  At Thomas R�lls request, the changed
the name of the driver to \fIXFree86\fP.
.P
It's a sign of the times that XInside has also had to change its name.  They
published this press announcement at
\fIhttp://www.xig.com/ci/pr/970219.xigraphics.html\fP\/:
.P
.in +.1i
.ll -.2i
\fBLeading Software Company Changes Name to Avoid Confusion with Internet
Porn\fP Denver--A leading developer of high-performance graphical software has
changed its name from X Inside to Xi Graphics. The reason: Ongoing confusion
between the software developer and the increasing number of X-rated pornography
companies on the Internet.
.P
``We develop software that solves performance, compatability, and correcness
issues in the UNIX X Windows environment,'' explains Richard Van Dyke, vice
president of marketing. ``However, we do not address the performance issues of
the sexually adventurous.''
.P
The name X Inside brought a plethora of requests for pornographic material, and
even impacted the company principals in their dealings outside the computer
field. ``An aspiring model even went so far as to show me a portfolio of highly
suggestive photographs,'' explained Thomas R�ll, president and chief technology
designer.  ``We felt a name change was in order.''
.ll
.in
.P
Other X implementations are also available for the Intel 386 architecture, but
they're no longer very common.
.P
By comparison with Microsoft's combined operating environments, the combination
of UNIX and X offers much more flexibility.  Unfortunately, the flexibility
comes with a price: it can be difficult to set up X to run the way you want it.
.P
This article is the first of a series aimed to help you get the most out of X.
I have previously published some of the material in my book \fIThe Complete
FreeBSD\fP, but the material isn't specific to any particular operating system.
I'm planning the following articles:
.Ls B
.LI
How to set up XFree86 to run with your board and your monitor (this article).
.LI
How to install XFree86.
.LI
How to set up a \fIwindow manager\fP.  A window manager is a program that
controls the placement and the appearence of windows on the screen.  Several
different window managers are available for X, and they offer a bewildering
number of options.
.Le
In the course of time, there will probably be other articles as well.  What
subjects interest you?  Send mail to Free Systems Journal, or personally to me
(\f(CWgrog@lemis.com\fP) and let us know.
..else
.\" ******* Start of book chapter
.Chapter \*[nchxtheory] "XFree86 in depth"
The information in Chapter
.Sref \*[nchpostinstall] \&
should be enough to get X up and running.  There's a lot more to X than that,
however, enough to fill many books.  In this chapter we'll look at some of the
more interesting topics:
.Ls B
.LI
The next section describes the technical background of running X displays.
.LI
On page
.Sref \*[doxconfig] \&
we'll look at setting up the
.File XF86Config
file.
.LI
On page
.Sref \*[multihead] \&
we'll look at using more than one monitor with X.
.LI
On page
.Sref \*[network-X] \&
we'll look at using X in a network.
.Le
.SPUP
..if article
.H2 "The problem with boards and monitors"
PCs have arguably the highest performance/price ratio of any computer family.
Unfortunately, there are hardly two PCs with the same hardware configuration: in
the course of the PC's evolution, hundreds of different display boards have
appeared, each with their own peculiar quirks.  How come they can sell?
Initially, most people used DOS or a system derived from DOS, and the boards
have the necessary driver software for DOS on the board.  If you're running a
real operating system, you don't want to use these drivers: they run in 16 bit
mode, they don't interface well to X, and they're slow.  This problem has since
come back and bitten its creators.  Every modern display board now comes with
driver at least for Microsoft programming environments.  Almost none of them
have UNIX drivers, however: that's why XFree86 supplies its own drivers and
incorporates them in the X server.
.P
XFree86 drivers are a ``good news, bad news'' situation.  First, the bad news:
setup can be more difficult.  With Microsoft Windows, you install the board and
the software that comes with it, and it works (well, you get a recognizable
picture on the screen).  With XFree86, things might not be as easy.
.P
The good news is that the drivers are much faster and much more flexible.  In
particular, you can configure XFree86 to your exact combination of display board
and monitor, if you know how, but you need to tune it manually.
.P
..else
.H2 "X configuration: the theory"
Setting up your
.File XF86Config
file normally takes a few minutes, but sometimes you can run into problems that
make grown men cry.  In the rest of this
..if article
article,
..else
chapter,
..endif
we'll look at the technical background:
.Ls B
.LI
How display boards and monitors work.
.LI
How to set up XFree86 to work with your hardware.
.LI
How to tune your hardware for maximum display performance.
.LI
How to fry your monitor.
.Le
I mean the last point seriously: conventional wisdom says that you can't damage
hardware with a programming mistake, but in this case it is possible.  Read the
section on how monitors work, and don't start tuning until you understand the
dangers involved.
..endif
.H3 "How TVs and monitors work"
You don't have to be a computer expert to see the similarity between monitors
and TVs: current monitor technology is derived from TV technology, and many
older display boards have modes that can use TVs instead of monitors.  Those of
us who were on the microcomputer scene 20 to 25 years ago will remember the joy
of getting a computer display on a portable TV, a ``glass tty'' connected by a
serial line running at 300 or 1200 bps.
.P
There are at least two ways to create pictures on a cathode ray tube: one is
derived from oscilloscopes, where each individual character is scanned by the
electron beam, rather like writing in the sand with your finger.  Some early
terminals used this technology, but it has been obsolete for several decades.
.P
TVs and monitors display the picture by scanning equally spaced lines across the
entire screen.  Like in a book, the first line starts at the top left of the
screen and goes to the top right.  Each successive line starts slightly below
the previous line.  This continues until the screen is full.  The picture is
formed by altering the intensity of the electron beam as it scans the lines.
.P
.X "deflection unit"
.X "deflection, horizontal"
.X "deflection, vertical"
.X "deflection, line"
.X "deflection, frame"
To perform this scan, the TV has two \fIdeflection units\fP\/: one scans from left
to right, and the other scans, much more slowly, from top to bottom.  Not
surprisingly, these units are called the \fIhorizontal\fP\/ and \fIvertical\fP\/
deflection units.  You may also encounter the terms \fIline\fP\/ and \fIframe\fP\/
deflection.
.P\" *** This is where the figure starts
.ie n \{\
The printed version of this book includes diagrams that are impossible to
reproduce in ASCII.  Sorry about that\(emabout the only thing I can suggest is
to get hold of the book.
\}
.el \{\
..if article
The following figure
..else
Figure
.Sref \*[scan-pattern] \&
..endif
shows the resultant pattern.
.PS
[
..if article
TV: box wid 1.6i height 1.2i
..else
move right .5i
TV: box wid 3.2i height 2.4i
..endif
A: line from TV.nw+ (.1i,-.1i) to TV.ne + (-.1i,-.1i)
B: line from TV.nw+ (.1i,-.2i) to TV.ne + (-.1i,-.2i)
C: line from TV.nw+ (.1i,-.3i) to TV.ne + (-.1i,-.3i)
"\fIetc\fR" at C.w + (.1i,-.2i)

line dashed .04i from A.e to B.w
Fb2: line dashed .04i from B.e to C.w

..if article
"\s8First line" at A.e  + (.2i,0i) ljust
"\s8Second line" at B.e + (.2i,0i) ljust
..else
"First scan line" at A.e  + (.3i,0i) ljust
"Second scan line" at B.e + (.3i,0i) ljust
..endif

FB: "Flyback" at C.c + (0i,-.7i)

arrow dashed from FB.c +(0i,.1i) to Fb2.c
]
.PE
.sp
..if article
..else
.ce
.Figure-heading "Scanning pattern on the monitor"
.Fn scan-pattern
..endif
.\}
.\" *** The figure ends here
.X "flyback, horizontal"
.X "flyback, vertical"
.ne 2v
The tube can only move the electron beam at a finite speed.  When the electron
beam reaches the right hand side of the screen, it needs to be deflected back
again.  This part of the scan is called the \fIhorizontal flyback\fP, and it is
not used for displaying picture data.  The actual time that the hardware
requires for the flyback depends on the monitor, but it is in the order of 5% to
10% of the total line scan time.  Similarly, when the vertical deflection
reaches the bottom of the screen, it performs a \fIvertical flyback\fP, which is
also not used for display purposes.
.P
It's not enough to just deflect, of course: somehow you need to ensure that the
scanning is synchronized with the incoming signal, so that the scan is at the
top of the screen when the picture information for the top of the screen
arrives.  You've seen what happens when synchronization doesn't work: the
picture runs up and down the screen (incorrect vertical synchronization) or
tears away from the left of the screen (incorrect horizontal synchronization).
Synchronization is achieved by including synchronization pulses in the
horizontal and vertical flyback periods.  They have a voltage level outside the
normal picture data range to ensure that they are recognized as synchronization
pulses.
.P
.X "video, blanking"
.X "porch, front"
.X "porch, back"
As if that wasn't enough, the video amplifier, the part of the TV that alters
the intensity of the spot as it travels across the screen, needs time to ensure
that the flyback is invisible, so there are brief pauses between the end of the
line and the start of the sync pulse, and again between the end of the sync
pulse and the beginning of the data.  This process is called \fIblanking\fP, and
the delays are called the \fIfront porch\fP\/ (before the sync pulse) and the
\fIback porch\fP\/ (after the sync pulse).
.ie n \{\
In the printed version of this book there's another diagram here.
\}
.el \{\
..if article
The following figure
..else
Figure
.Sref \*[waveform-figure] \&
..endif
depicts a complete scan line.
.Pn waveform
..if article
.\".1C
..endif
.PS
.\" Dimensions
..if article
.ps 7
p3=.3i*.63
p02=.02i*.63
p1=.1i*.63
p15=.15i*.63
p05=.05i*.63
.\" In the magazine article, we need to make some .63" things .75" to look right
p100=.75i
p100a=.63i
p70=.7i*.63
p20=.2i*.63
p60=.6i*.63
p48=.48i*.63
p08=.08i*.63
p025=.025i*.63
p80=.8i*.63
p08=.08i*.63
p50=.5i*.63
p12=.12i*.63
p125=1.25i*.63
p170=1.7i*.63
p07=.07i*.63
move right p3
..else
p3=.3i
p02=.02i
p1=.1i
p15=.15i
p05=.05i
.\" For the sake of the article, two 1" variables
p100=1i
p100a=1i
p70=.7i
p20=.2i
p60=.6i
p48=.48i
p08=.08i
p025=.025i
p80=.8i
p08=.08i
p50=.5i
p12=.12i
p125=1.25i
p170=1.7i
p07=.07i
move right .5i
..endif
line dotted p02 right p1;                                       # end of previous line
A: line dotted p02 up p100 then right p15;                      # start of sync pulse
arc dotted p02 from A.n to A.n+(p05, p05)
line dotted p02 up p15;
Pulse1: line dotted p02 right p1;
B: line dotted p02 down p15;    # pulse
arc dotted p02 from B.s to B.s+(p05,-p05);
Rp: line dotted p02 right p15;                          # rear porch
Ps: line down p100                              # and picture data
line right p1
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
C: line up p100
line right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
line up p100 then right p1 then down p100 then right p1;
E: line up p100;
Fp: line right p15;                             # start of next sync pulse
arc from Fp.e to Fp.e+(p05, p05)
line up p15;
Pulse2: line right p1;
F: line down p15;                               # pulse
arc from F.s to F.s+(p05,-p05);
line right p15;                         # front porch
G: line down p100                                       # and picture data of next frame
   line dotted p02 right p1
   line dotted p02 up p70

move to Pulse1.c + (0,p20)
P1text: "Sync pulse" above;
arrow from P1text.c to Pulse1.c

Rpt: "Back porch" at Rp.c + (p60,p48)
arrow from Rpt.w + (-p3,-p08) to Rp.c + (-p07, 0i)
move to Fp

Fpt: "Front porch" at Fp.c + (-p60,p48)

arrow from Fpt.se + (p3,-p08) to Fp.c + (p07, 0i)
move to Fp

move to Pulse2.c + (0,p20)
P2text: "Sync pulse" above;
arrow from P2text.c to Pulse2.c

Cw: arrow from C.n + (0i,p1) to Ps.n + (0i,p1)
Ce: arrow from C.n + (0i,p1) to E.n + (0i,p1)

line dotted p025 from Cw.w to Ps.n
line dotted p025 from Ce.e to E.n

"Picture data" at C.n + (0i,p20)

Rd: "(Reference point)" at Ps.s + (0i,-p50)
     arrow from Rd + (0i,p1) to Ps.s + (0i,-p05)

HDE: "HDE" at E.s + (-p3,-p50)
     arrow from HDE + (0i,p1) to E.s + (0i,-p05)

SHR: "SHR" at Pulse2.w + (-p12,-p170)
     arrow from SHR + (p12,p1) to Pulse2.w + (0i,-p125)
     line dotted p025 from Pulse2.w + (0i,-p125+p100a-p100) to Pulse2.w

EHR: "EHR" at Pulse2.e + (p12,-p170)
     arrow from EHR + (-p12,p1) to Pulse2.e + (0i,-p125)
     line dotted p025 from Pulse2.e + (0i,-p125+p100a-p100) to Pulse2.e

HT: "HT" at G.s + (p3,-p50)
     arrow from HT + (0i,p1) to G.s + (0i,-p05)

"\fIRegisters:\fP" at HDE.w - (p80,0i)
.ps
.PE
.sp
..if article
.\" .2C
..else
.ce
.Figure-heading "Scan line and register values"
.Fn waveform-figure
..endif
.\}
.P
.X "interlacing"
The register information at the bottom of the picture refers to the video
controller registers.  We'll look at how to interpret them
..if article
below.
..else
on page
.Sref \*[setting-video-regs] .
..endif
.P
That, in a nutshell, is how horizontal deflection works.  Vertical deflection
works in almost the same way, just slower, with one minor exception.  This basic
display mechanism was developed for TVs in the 1930s, at a time when terms like
high-tech (or even electronics) hadn't even been invented, and even today we're
stuck with the low data rates that they decided upon in those days.  Depending
on the country, conventional TVs display only 25 or 30 frames (pages of display)
per second.  This would cause an unpleasant flicker in the display.  This
flicker is minimized with a trick called \fIinterlacing\fP\/: instead of
displaying the frame in one vertical scan, the odd and even lines are displayed
in two alternating half frames, which doubles the apparent vertical frequency.
.H3 "How monitors differ from TVs"
So how do we apply this to computer displays?  Let's look at the US standard
NTSC system\(emthe international PAL and SECAM systems are almost identical
except for the number of lines and a minor difference in the frequencies.  NTSC
specifies 525 lines, but that includes the vertical flyback time, and in fact
only about 480 lines are visible.  The aspect ratio of a normal TV is 4:3, in
other words the screen is one-third wider than it is high, so if we want square
pixels,\*F
.FS
A square pixel is one with the same height and width.  They don't have to be
that way, but it makes graphics software much simpler.
.FE
we need to have one-third more pixels per line.  This means that we can display
640 pixels per line on 480 lines.\*F
.FS
Does this look familiar?
.FE
This resolution is normally abbreviated to ``640x480.''  PAL and SECAM have
lower vertical frequencies, which allows a nominal 625 lines, of which about 600
are displayed.  Either way, these values have two huge disadvantages: first, the
resolution is barely acceptable for modern graphics displays, and secondly they
are interlaced displays.  Older PC display hardware, such as the CGA and some
EGA modes, was capable of generating these signal frequencies, but normal
graphic cards can no longer do it.  Instead, dedicated TV output cards are
available if that's what you want to do.
.P
The first problem is interlace: it works reasonably for TVs, but it's a pain for
computer displays\(emthere's still more flicker than a real 50 Hz or 60 Hz
display.  Modern display boards can still run in interlace mode, but don't even
think about doing so unless you're forced to\(emthe resultant picture looks out
of focus and is very tiring to read.
.P
The second problem is the resolution: nowadays, 1024x768 is a minimum
resolution, and some monitors display up to 2048x1536 pixels.  On the other
hand, even 60 Hz refresh rate is barely adequate: read any marketing literature
and you'll discover that 72 Hz is the point at which flicker suddenly
disappears.  To get high-resolution, high refresh rate displays, you need some
very high internal frequencies\(emwe'll look at that further down.
.H3 "How to fry your monitor"
.Pn fry-monitor
.X "line transformer"
Remember that a monitor is just a glorified TV?  Well, one of the design
constraints of real TVs is that they have only a single horizontal frequency and
only a single vertical frequency.  This simplifies the hardware design
considerably: the horizontal deflection uses a tuned circuit to create both the
deflection frequency and the high voltage required to run the tube.  This
circuit is comprised of a transformer (the \fIline transformer\fP\/) and a
condenser.  Run a line transformer even fractionally off its intended frequency
and it runs much less efficiently and use more current, which gets converted to
heat.  If you run a conventional monitor off spec for any length of time, it
will burn out the line transformer.
.P
You don't have to roll your own X configuration to burn out the monitor: 20
years ago, the standard display boards were CGAs and HDAs,\*F
.FS
Color Graphics Adapter and Hercules Display Adapter.
.FE
and they had different horizontal frequencies and thus required different
monitors.  Unfortunately, they both used the same data connector.  If you
connected an HDA (18.43 kHz horizontal frequency) to a CGA monitor (15.75 kHz,
the NTSC line frequency), you would soon see smoke signals.
.P
All modern PC monitors handle at least a range of horizontal frequencies.  This
doesn't mean that an out of spec signal can't damage them\(emyou might just burn
out something else, frequently the power supply.  Most better monitors recognize
out-of-spec signals and refuse to try to display them; instead, you get an error
display.  Unfortunately, there are plenty of other monitors, especially older or
cheaper models, which don't protect themselves against out of spec signals.  In
addition, just because the monitor displays correctly doesn't mean that it is
running in spec.  The moral of the story:
.Highlight
Never run your monitor out of spec.  If your display is messed up, there's a
good chance that the frequencies are out, so turn off the monitor.
.End-highlight
.P
Monitors aren't the only thing that you can burn out, of course.  If you try
hard, you can also burn out chips on some display boards by running them at
frequencies that are out of spec.  In practice, though, this doesn't happen
nearly as often.
.P
.X "video, composite"
.X "composite video"
Another difference between TVs and monitors is the kind of signal they take.  A
real TV includes a receiver, of course, so you have an antenna connection, but
modern TVs also have connections for inputs from VCRs, which are usually two
audio signals and a video signal.  The video signal contains five important
components: the \fIred\fP, \fIgreen\fP and \fIblue\fP\/ signals, and the
horizontal and vertical sync pulses.  This kind of signal is called \fIcomposite
video\fP.  By contrast, most modern monitors separate these signals onto
separate signal lines, and older boards, such as the EGA, even used several
lines per colour.  Unfortunately, there is no complete agreement about how these
signals should work: the polarity of the sync pulses can vary, and some boards
cheat and supply the sync pulses on the green signal line.  This is mainly of
historical interest, but occasionally you'll come across a real bargain 20"
monitor that only has three signal connections, and you may not be able to get
it to work\(emthis could be one of the reasons.
.H3 "The CRT controller"
.Pn setting-video-regs
The display controller, usually called a CRT (Cathode Ray Tube) controller, is
the part of the display board that creates the signals we've just been talking
about.  Early display controllers were designed to produce signals that were
compatible with TVs: they had to produce a signal with sync pulses, front and
back porches, and picture data in between.  Modern display controllers can do a
lot more, but the principles remain the same.
.P
.X "dot clock"
The first part of the display controller creates the framework we're looking
for: the horizontal and vertical sync pulses, blanking and picture information,
which is represented as a series of points or \fIdots\fP.  To count, we need a
pulse source, which also determines the duration of individual dots, so it is
normally called a \fIdot clock\fP.  For reasons lost in history, CRT controllers
start counting at the top left of the display, and not at the vertical sync
pulse, which is the real beginning of the display.  To define a line to the
horizontal deflection, we need to set four CRTC registers to tell
.ie n it.
.el \{\
it\(emsee the
..if article
scan line diagram above:
..else
diagram on page
.Sref \*[waveform] :
..endif
.\}
.Ls B
.LI
.X "registers, video card"
The \fIHorizontal Display End\fP\/ register (HDE) specifies how many dots we want
on each line.  After the CRTC has counted this many pixels, it stops outputting
picture data to the display.
.LI
The \fIStart Horizontal Retrace\fP\/
register (SHR) specifies how many dot clock
pulses occur before the sync pulse starts.  The difference between the contents
of this register and the contents of the HDE register defines the length of the
front porch.
.LI
The \fIEnd Horizontal Retrace\fP\/
register (EHR) defines the end of the sync
pulse.  The width of the sync pulse is the difference between the contents of
this register and the SHR register.
.LI
The \fIHorizontal Total\fP\/
register (HT) defines the total number of dot clocks
per line.  The width of the back porch is the difference between the contents of
this register and the EHR register.
.Le
In addition, the \fIStart Horizontal Blanking\fP\/ and \fIEnd Horizontal
Blanking\fP\/ registers (SHB and EHB) define when the video signals are turned off
and on.  The server sets these registers automatically, so we don't need to look
at them in more detail.
.P
The control of the vertical deflection is similar.  In this case, the registers
are \fIVertical Display End\fP\/ (VDE), \fIStart Vertical Retrace\fP\/ (SVR), \fIEnd
Vertical Retrace\fP\/ (EVR), \fIVertical Total\fP\/ (VT), \fIStart Vertical
Blanking\fP\/ (SVB), and \fIEnd Vertical Blanking\fP\/ (EVB).  The values in these
registers are counted in lines.
.P
VGA hardware evolved out of older 8 bit character-based display hardware, which
counted lines in characters, not dot clocks.  As a result, all of these
registers are 8 bits wide.  This is adequate for character displays, but it's a
problem when counting dots: the maximum value you can set in any of these
registers is 255.  The designers of the VGA resorted to a number of nasty
kludges to get around this problem: the horizontal registers count in groups of
8 dot clocks, so they can represent up to 2048 dot clocks. The vertical
registers overflow into an overflow register.  Even so, the standard VGA can't
count beyond 1024 lines.  Super VGAs vary in how they handle this problem, but
typically they add additional overflow bits.  To give you an idea of how clean
the VGA design is, consider the way the real Vertical Total (total number of
lines on the display) is defined on a standard VGA.  It's a 10 bit quantity, but
the first 8 bits are in the VT register, the 9th bit is in bit 0 of the overflow
register, and the 10th bit is in bit 5 of the overflow register.
.H3 "The XF86Config mode line"
.Pn ModeLine
One of the steps in setting up XFree86 is to define these register values.
Fortunately, you don't have to worry about which bits to set in the overflow
register: the mode lines count in dots, and it's up to the server to convert the
dot count into something that the display board can understand.  A typical Mode
line looks like:
.Dx
Modeline "640x480a" 28 640 680 728 776 480 480 482 494
.De
These ten values are required.  In addition, you may specify modifiers at the
end of the line.  The values are:
.Ls B
.LI
A label for the resolution line.  This must be enclosed in quotation marks, and
is used to refer to the line from other parts of the
.File XF86Config
file.  Traditionally, the label represents the resolution of the display mode,
but it doesn't have to.  In this example, the resolution really is 640x480, but
the \f(CWa\fP at the end of the label is a clue that it's an alternative value.
.LI
The clock frequency, 28 MHz in this example.
.LI
The Horizontal Display End, which goes into the HDE register.  This value and
all that follow are specified in dots.  The server mangles them as the display
board requires and puts them in the corresponding CRTC register.
.LI
The Start Horizontal Retrace (SHR) value.
.LI
The End Horizontal Retrace (EHR) value.
.LI
The Horizontal Total (HT) value.
.LI
The Vertical Display End (VDE) value.  This value and the three following are
specified in lines.
.LI
The Start Vertical Retrace (SVR) value.
.LI
The End Vertical Retrace (EVR) value.
.LI
The Vertical Total (VT) value.
.Le
This is pretty dry stuff.  To make it easier to understand, let's look at how we
would set a typical VGA display with 640x480 pixels.  Sure, you can find values
for this setup in any release of XFree86, but that doesn't mean that they're the
optimum for \fIyour system\fP.  We want a non-flicker display, which we'll take
to mean a vertical frequency of at least 72 Hz, and of course we don't want
interlace.  Our monitor can handle any horizontal frequency between 15 and 40
kHz: we want the least flicker, so we'll aim for 40 kHz.
.P
First, we need to create our lines.  They contain 640 pixels, two porches and a
sync pulse.  The only value we really know for sure is the number of pixels.
How long should the porches and the sync pulses be?  Good monitor documentation
should tell you, but most monitor manufacturers don't seem to believe in good
documentation.  The documented values vary significantly from monitor to
monitor, and even from mode to mode: they're not as critical as they look.  Here
are some typical values:
.P
Horizontal sync pulse: 1 to 4 \(*ms, front porch 0.18 to 2.1 \(*ms, back porch
1.25 to 3.56 \(*ms.
.P
As we'll see, the proof of these timing parameters is in the display.  If the
display looks good, the parameters are OK.  I don't know of any way to damage
the monitor purely by modifying these parameters, but there are other good
reasons to stick to this range.  As a rule of thumb, if you set each of the
three values to 2 \(*ms to start with, you won't go too far wrong.
Alternatively, you could start with the NTSC standard values: the standard
specifies that the horizontal sync pulse lasts for 4.2 to 5.1 \(*ms, the front
porch must be at least 1.27 \(*ms.  NTSC doesn't define the length of the back
porch\(eminstead it defines the total line blanking, which lasts for 8.06 to 10.3
\(*ms.  For our purposes, we can consider the back porch to be the length of the
total blanking minus the lengths of the front porch and the sync pulse.  If you
take values somewhere in the middle of the ranges, you get a front porch of 1.4
\(*ms, a sync pulse of 4.5 \(*ms, and total blanking 9 \(*ms, which implies a
back porch of 9 - 1.4 - 4.5 = 3.1 \(*ms.
.P
For our example, let's stick to 2 \(*ms per value.  We have a horizontal
frequency of 40 kHz, or 25 \(*ms per line.  After taking off our 6 \(*ms for
flyback control, we have only 19 \(*ms left for the display data.  To get 640
pixels in this time, we need one pixel every 19 \(di 640 \(*ms, or about 30 ns.
This corresponds to a frequency of 33.6 MHz.  This is our desired dot clock.
.P
The next question is: do we have a dot clock of this frequency?  Maybe.  This
should be in your display board documentation, but I'll take a bet that it's
not.  Never mind, the XFree86 server is clever enough to figure this out for
itself.  At the moment, let's assume that you do have a dot clock of 33 MHz.
.Aside
If you don't have a suitable clock, you'll have to take the next lower clock
frequency that you do have: you can't go any higher, since this example assumes
the highest possible horizontal frequency.
.End-aside
You now need to calculate four register values to define the horizontal lines:
.Ls B
.LI
The first value is the Horizontal Display End, the number of pixels on a line.
We know this one: it's 640.
.LI
You calculate SHR by adding the number of dot clocks that elapse during the
front porch to the value of HDE.  Recall that we decided on a front porch of 2
\(*ms.  In this time, a 33 MHz clock counts 66 cycles.  So we add 66, right?
Wrong.  Remember that the VGA registers count in increments of 8 pixels, so we
need to round the width of the front porch to a multiple of 8.  In this case, we
round it to 64, so we set SHR to 640 + 64 = 704.
.LI
The next value we need is EHR, which is SHR plus the width of the horizontal
retrace, again 64 dot clocks, so we set that to 704 + 64 = 768.
.LI
The final horizontal value is HT.  Again, we add the front porch\(em64 dot
clocks\(emto EHR and get 768 + 64 = 832.
.Le
At this point, our vestigial mode line looks like:
.Dx 1
Modeline "640x480"   28   640 704 768 832
.De
Next, we need another four values to define the vertical scan.  Again, of the
four values we need, we only know the number of lines.  How many lines do we use
for the porches and the vertical sync?  As we've seen, NTSC uses about 45 lines
for the three combined, but modern monitors can get by with much less.  Again
referring to the Multisync manual, we get a front porch of betwwen 0.014 and 1.2
ms, a sync pulse of between 0.06 and 0.113 ms, and a back porch of between 0.54
and 1.88 ms.  But how many lines is that?
.P
To figure that out, we need to know our \fIreal\fP\/ horizontal frequency.  We
were aiming at 40 kHz, but we made a couple of tradeoffs along the way.  The
real horizontal frequency is the dot clock divided by the horizontal total, in
this case 33 MHz \(di 832, which gives us 39.66 kHz\(emnot too bad.  At that
frequency, a line lasts 1\(di39660 seconds, or just over 25 \(*ms, so our front
porch can range between \(12 and 48 lines, our sync pulse between 2 and 5 lines,
and the back porch between 10 and 75 lines.  Do these timings make any sense?
No, they don't\(emthey're just values that the monitor can accept.
.P
To get the highest refresh rate, we can go for the lowest value in each case.
It's difficult to specify a value of \(12, so we'll take a single line front
porch.  We'll take two lines of sync pulse and 10 lines of back porch. This
gives us:
.Ls B
.LI
VDE is 480.
.LI
SVR is 481.
.LI
EVR is 483.
.LI
VT is 493.
.LE 1
Now  our mode line is complete:
.Dx
Modeline "640x480" 28  640 704 768 832  480 481 483 493
.De
Now we can calculate our vertical frequency, which is the horizontal frequency
divided by the Vertical Total, or 39.66 \(di 493 kHz, which is 80.4 Hz\(emthat's
not bad either.  By comparison, if you use the default value compiled into the
server, you get a horizontal frequency of 31.5 kHz and a vertical frequency of
only 60 Hz.
.P
If you know the technical details of your monitor and display board, it really
is that simple.  This method doesn't require much thought, and it creates
results that work.
.P
Note that the resultant mode line may not work on other monitors.  If you are
using a laptop that you want to connect to different monitors or overhead
display units, don't use this method.  Stick to the standard frequencies
supplied by the X server.  Many overhead projectors understand only a very small
number of frequencies, and the result of using a tweaked mode line is frequently
that you can't synchronize with the display, or that it cuts off a large part of
the image.
.H2 "XF86Config"
.Pn doxconfig
The main configuration file for XFree86 is called
.File XF86Config .
It has had a long and varied journey through the file system.  At the time of
writing, it's located at
.File /usr/X11R6/lib/X11/XF86Config ,
but previously it has been put in
.File /etc/X11/XF86Config ,
.File -n /etc/XF86Config
or
.File -n /usr/X11R6/etc/X11/XF86Config ,
and the server still looks for it in many of these places.  If you're upgrading
a system, you should ensure that you don't have old configuration files in one
of the alternative places.
.P
As we saw on page
.Sref \*[xf86cfg] ,
there are a couple of ways to automatically create an
.File XF86Config
file.  On that page we saw how to do it with
.Command xf86cfg .
An alternative way is to run the X server in configuration mode:
.Dx
# \f(CBX -configure\fP
XFree86 Version 4.2.0 / X Window System
(protocol Version 11, revision 0, vendor release 6600)
Release Date: 18 January 2002
        If the server is older than 6-12 months, or if your card is
        newer than the above date, look for a newer version before
        reporting problems.  (See http://www.XFree86.Org/)
Build Operating System: FreeBSD 5.0-CURRENT i386 [ELF]
Module Loader present
Markers: (--) probed, (**) from config file, (==) default setting,
         (++) from command line, (!!) notice, (II) informational,
         (WW) warning, (EE) error, (NI) not implemented, (??) unknown.
(==) Log file: "/var/log/XFree86.0.log", Time: Sat Apr  6 13:51:10 2002
List of video drivers:
        atimisc
\fI(the list is long, and will change; it's omitted here)\fP\/
(++) Using config file: "/root/XF86Config.new"


Your XF86Config file is /root/XF86Config.new

To test the server, run 'XFree86 -xf86config /root/XF86Config.new'
.De
Note that
.Command X
does not place the resultant configuration file in the default location.  The
intention is that you should test it first and then move it to the final
location when you're happy with it.  As generated above, it's good enough to run
XFree86, but you'll possibly want to change it.  For example, it only gives you
a single resolution, the highest it can find.  In this section we'll look at the
configuration file in more detail, and how to change it.
.P
.File XF86Config
is divided into several sections, as shown in Table
.Sref \*[Xconfig-sections] .
We'll look at them in the order they appear in the generated
.File XF86Config
file, which is not the same order as in the man page.
.br
.ne 10
.Table-heading "XF86Config sections"
.TS H
tab(#) ;
 l | lw58  .
\fBSection#\fBDescription
.TH N
_
ServerLayout#T{
Describes the overall layout of the X configuration.  X can handle more than one
display card and monitor.  This section is the key to the other sections
T}
.sp .4v
Files#T{
Sets the default font and RGB paths.
T}
.sp .4v
ServerFlags#T{
Set some global options.
T}
.sp .4v
Module#T{
Describes the software modules to load for the configuration.
T}
.sp .4v
InputDevice#T{
Sets up keyboards, mice and other input devices.
T}
.sp .4v
Monitor#T{
Describes your monitor to the server.
T}
.sp .4v
Device#T{
Describes your video hardware to the server.
T}
.sp .4v
Screen#T{
Describes how to use the monitor and video hardware.
T}
_
.TE
.Tn Xconfig-sections
.sp
.H3 "The server layout"
.Pn XF86Config-ServerLayout
The ServerLayout section describes the relationships between the individual
hardware components under the control of an X server.  For typical hardware,
\f(CWX -configure\fP might generate:
.Dx
Section "ServerLayout"
        Identifier     "XFree86 Configured"
        Screen      0  "Screen0" 0 0
        InputDevice    "Mouse0" "CorePointer"
        InputDevice    "Keyboard0" "CoreKeyboard"
EndSection
.De
This shows that the server has one screen and two input devices.  The names
\f(CWMouse0\fP and \f(CWKeyboard0\fP suggest that they're a mouse and a
keyboard, but any name is valid.  These entries are pointers to sections
elsewhere in the file, which must contain definitions for \f(CWScreen0\fP,
\f(CWMouse0\fP and \f(CWKeyboard0\fP.
.P
Normally you only have one screen, one mouse and one keyboard, so this section
might seem rather unnecessary.  As we will see when we look at multiple monitor
configurations, it's quite important to be able to describe these relationships.
.H3 "The Files section"
.Pn XF86Config-File-section
.X "XF86Config, Files section"
The \fIFiles\fP\/ section of the
.File XF86Config
file contains the path to the RGB database file, which should never need to be
changed, and the default font path.  You may want to add more font paths, and
some ports do so: the \f(CWFontPath\fP lines in your
.File XF86Config
are concatenated to form a search path.  Ensure that each directory listed
exists and is a valid font directory.
.P
.ne 3v
The standard \fIFiles\fP\/ section looks like:
.Dx
Section "Files"
        RgbPath      "/usr/X11R6/lib/X11/rgb"
        ModulePath   "/usr/X11R6/lib/modules"
        FontPath     "/usr/X11R6/lib/X11/fonts/misc/"
        FontPath     "/usr/X11R6/lib/X11/fonts/Speedo/"
        FontPath     "/usr/X11R6/lib/X11/fonts/Type1/"
        FontPath     "/usr/X11R6/lib/X11/fonts/CID/"
        FontPath     "/usr/X11R6/lib/X11/fonts/75dpi/"
        FontPath     "/usr/X11R6/lib/X11/fonts/100dpi/"
EndSection
.De
If you are running a high-resolution display, this sequence may be sub-optimal.
For example, a 21" monitor running at 1600x1200 pixels has a visible display of
approximately 16" wide and 12" high, exactly 100 \fIdpi\fP\/ (\fIdots per
inch\fP, really pixels per inch).  As a result, you'll probably be happier with
the 100 dpi fonts.  You can change this by swapping the last two lines in the
section:
.Dx
        FontPath     "/usr/X11R6/lib/X11/fonts/100dpi/"
        FontPath     "/usr/X11R6/lib/X11/fonts/75dpi/"
EndSection
.De
Don't just remove the 75 dpi fonts: some fonts may be available only in the 75
dpi directory.
.P
Sometimes the server complains:
.Dx
Can't open default font 'fixed'
.De
This is almost certainly the result of an invalid entry in your font path.  Try
running
.Command mkfontdir
in each directory if you are certain that each one is correct.  The
.File XF86Config
man page describes other parameters that may be in this section of the file.
.H3 "The ServerFlags section"
.X "XF86Config, ServerFlags section"
The ServerFlags section allows you to specify a number of global options.  By
default it is not present, and you will probably not find any reason to set it.
See the man page \fIXF86Config(5)\fP\/ for details of the options.
.H3 "The Module section"
.X "XF86Config, Module section"
The \f(CWModule\fP section describes binary modules that the server loads:
.Dx
Section "Module"
        Load  "extmod"
        Load  "xie"
        Load  "pex5"
        Load  "glx"
        Load  "GLcore"
        Load  "dbe"
        Load  "record"
        Load  "type1"
EndSection
.De
We won't look at modules in more detail; see the XFree86 documentation.
.Pn XF86Config-Module
.H3 "The InputDevice section"
.Pn XF86Config-InputDevice-section
.X "XF86Config, InputDevice section"
The \fIInputDevice\fP\/ section specifies each input device, typically mice and
keyboards.  Older versions of XFree86 had separate \f(CWMouse\fP and
\f(CWKeyboard\fP sections to describe these details.  The default
\f(CWXF86Config\fP looks something like this:
.Dx
Section "InputDevice"
        Identifier  "Keyboard0"
        Driver      "keyboard"
EndSection

Section "InputDevice"
        Identifier  "Mouse0"
        Driver      "mouse"
        Option      "Protocol" "auto"
        Option      "Device" "/dev/mouse"
EndSection
.De
There's not much to be said for the keyboard.  Previous versions of XFree86
allowed you to set things like \fINumLock\fP\/ handling and repeat rate, but the
former is no longer needed, and the latter is easier to handle with the
.Command xset
program.
.P
Mice are still not as standardized as keyboards, so you still need a
\f(CWProtocol\fP line and a device name.  The defaults shown here are correct
for most modern mice; the mouse driver can detect the mouse type correctly.  If
you're using the mouse daemon,
.Daemon moused ,
change this entry to the
.Daemon moused
device,
.Device sysmouse .
.P
If you're using a serial mouse or one with only two buttons, \fIand\fP\/ if
you're not using
.Daemon moused ,
you need to change the device entries and specify the \f(CWEmulate3Buttons\fP
option.  That's all described in the man page, but in general it's easier to use
.Daemon moused .
.H3 "The Monitor section"
.Pn XF86Config-Monitor-section
.X "XF86Config, Monitor section"
Next comes the description of the monitor.  Modern monitors can identify
themselves to the system.  In that case, you get a section that looks like
this:
.Dx
Section "Monitor"
        Identifier   "Monitor0"
        VendorName   "IBM"
        ModelName    "P260"
        HorizSync    30.0 - 121.0
        VertRefresh  48.0 - 160.0
.De
This tells the server that the monitor is an IBM P260, that it can handle
horizontal frequencies between 30 kHz and 121 kHz, and vertical frequencies
between 48 Hz and 160 Hz.  Less sophisticated monitors don't supply this
information, so you might end up with an entry like this:
.Dx
Section "Monitor"
        Identifier   "Monitor0"
        VendorName   "Monitor Vendor"
        ModelName    "Monitor Model"
EndSection
.De
This may seem like no information at all, but in fact it does give the
identifier.  Before you use it, you should add at least the horizontal and
vertical frequency range, otherwise the server assumes it's a standard (and
obsolete) VGA monitor capable of only 640x480 resolution.
.P
This is also the place where you can add mode lines.  For example, if you have
created a mode line as described in the first part of this chapter, you should
add it here:
.Dx
Section "Monitor"
        Identifier   "right"
        VendorName   "iiyama"
        ModelName    "8221T"
        HorizSync    24.8 - 94.0
        VertRefresh  50.0 - 160.0

ModeLine  "640x480"    73   640  672  768  864    480  488  494  530
# 62 Hz!
ModeLine  "800x600"   111   800  864  928 1088    600  604  610  640
# 143 Hz
ModeLine  "1024x768"  165  1024 1056 1248 1440    768  771  781  802
# 96 Hz
ModeLine  "1280x1024" 195  1280 1312 1440 1696   1024 1031 1046 1072 -hsync -vsync
# 76 Hz
ModeLine  "1600x1200" 195  1600 1616 1808 2080   1200 1204 1207 1244 +hsync +vsync
# 56 Hz!
ModeLine  "1920x1440" 200  1920 1947 2047 2396   1440 1441 1444 1483 -hsync +vsync
# 61 Hz
ModeLine  "1920x1440" 220  1920 1947 2047 2448   1440 1441 1444 1483 -hsync +vsync
        EndSection
.De
It's possible to have multiple mode lines for a single frequency, and this even
makes sense.  The examples for 1920x1440 above have different pixel clocks.  If
you use this monitor with a card with a pixel clock that only goes up to 200
MHz, the server chooses the first mode line.  If you use a card with up to 250
MHz pixel clock, it uses the second and gets a better page refresh rate.
.P
The X server has a number of built-in mode lines, so it's quite possible to have
a configuration file with no mode lines at all.  The names correspond to the
resolutions, and there can be multiple mode lines with the same name.  The
server chooses the mode line with the highest frequency compatible with the
hardware.
.H3 "The Device section"
.X "XF86Config, Device section"
.Pn XF86Config-Device-section
The \fIDevice\fP\/ section describes the video display board:
.Dx
Section "Device"
        ### Available Driver options are:-
        ### Values: <i>: integer, <f>: float, <bool>: "True"/"False",
        ### <string>: "String", <freq>: "<f> Hz/kHz/MHz"
        ### [arg]: arg optional
        #Option     "SWcursor"                  # [<bool>]
        #Option     "HWcursor"                  # [<bool>]
        #Option     "PciRetry"                  # [<bool>]
        #Option     "SyncOnGreen"               # [<bool>]
        #Option     "NoAccel"                   # [<bool>]
        #Option     "ShowCache"                 # [<bool>]
        #Option     "Overlay"                   # [<str>]
        #Option     "MGASDRAM"                  # [<bool>]
        #Option     "ShadowFB"                  # [<bool>]
        #Option     "UseFBDev"                  # [<bool>]
        #Option     "ColorKey"                  # <i>
        #Option     "SetMclk"                   # <freq>
        #Option     "OverclockMem"              # [<bool>]
        #Option     "VideoKey"                  # <i>
        #Option     "Rotate"                    # [<str>]
        #Option     "TexturedVideo"             # [<bool>]
        #Option     "Crtc2Half"                 # [<bool>]
        #Option     "Crtc2Ram"                  # <i>
        #Option     "Int10"                     # [<bool>]
        #Option     "AGPMode"                   # <i>
        #Option     "DigitalScreen"             # [<bool>]
        #Option     "TV"                        # [<bool>]
        #Option     "TVStandard"                # [<str>]
        #Option     "CableType"                 # [<str>]
        #Option     "NoHal"                     # [<bool>]
        #Option     "SwappedHead"               # [<bool>]
        #Option     "DRI"                       # [<bool>]
        Identifier  "Card0"
        Driver      "mga"
        VendorName  "Matrox"
        BoardName   "MGA G200 AGP"
        BusID       "PCI:1:0:0"
EndSection
.De
This example shows a Matrox G200 AGP display board.  It includes a number of
options that you can set by removing the comment character (\f(CW#\fP).  Many
of these options are board dependent, and none of them are required.  See the X
documentation for more details.
.P
Note particularly the last line, \f(CWBusID\fP.  This is a hardware-related
address that tells the X server where to find the display board.  If you move
the board to a different PCI slot, the address will probably change, and you
will need to re-run \fIX -configure\fP\/ to find the new bus ID.
.P
If your display board is older, much of this information will not be available,
and you'll have to add it yourself.  Unlike older monitors, it's hardly worth
worrying about older boards, though: modern boards have become extremely cheap,
and they're so much faster than older boards that it's not worth the trouble.
.H3 "The Screen section"
.Pn XF86Config-Screen-section
.X "XF86Config, Screen section"
The final section is the Screen section, which describes the display on a
monitor.  The default looks something like this:
.Dx
Section "Screen"
        Identifier "Screen0"
        Device     "Card0"
        Monitor    "Monitor0"
        SubSection "Display"
                Depth     1
        EndSubSection
        SubSection "Display"
                Depth     4
        EndSubSection
        SubSection "Display"
                Depth     8
        EndSubSection
        SubSection "Display"
                Depth     15
        EndSubSection
        SubSection "Display"
                Depth     16
        EndSubSection
        SubSection "Display"
                Depth     24
        EndSubSection
EndSection
.De
The first three lines describe the relationship between the screen display, the
video board that creates it, and the monitor on which it is displayed.  Next
come a number of subsections describing the possible bit depths that the screen
may have.
.Pn pixel-depth
For each display depth, you can specify which mode lines you wish to use.
Modern display hardware has plenty of memory, so you'll probably not want to
restrict the display depth.  On the other hand, you may want to have multiple
mode lines.  Your display card and monitor are good enough to display 2048x1536
at 24 bits per pixel, but occasionally you'll get images (in badly designed web
pages, for example) so miniscule that you'll want to zoom in, maybe going all
the way back to 640x480 in extreme cases.  You can toggle through the available
resolutions with the key combinations \fBCtrl\fP-\fBAlt\fP-\fBNumeric\ +\fP and
\fBCtrl\fP-\fBAlt\fP-\fBNumeric\ -\fP.  You're probably not interested in pixel
depths lower than 640x480, so your Screen section might look like:
.Dx
Section "Screen"
        Identifier "Screen0"
        Device     "Card0"
        Monitor    "Monitor0"
        DefaultDepth    24
        SubSection "Display"
                Depth     24
                Modes     "2048x1536" "1600x1200"  "1024x768"  "640x480"
        EndSubSection
EndSection
.De
This section includes a \f(CWDefaultDepth\fP entry for the sake of example.  In
this case, it's not strictly needed, because there's only one pixel depth.  If
there were more than one Display subsection, it would tell
.Command xinit
which depth to use by default.
..endif
.br
.ne 15v
.H2 "Multiple monitors and servers"
.Pn multihead
.X "X, multiple monitors"
We've seen above that X provides for more than one monitor per server.  If you
have multiple display cards and monitors, let the server generate the
.File XF86Config
file: it generates a file that supports all identified devices.  The resultant
server layout section might look like this:
.Dx
Section "ServerLayout"
        Identifier     "XFree86 Configured"
        Screen      0  "Screen0" 0 0
        Screen      1  "Screen1" RightOf "Screen0"
        Screen      2  "Screen2" RightOf "Screen1"
        InputDevice    "Mouse0" "CorePointer"
        InputDevice    "Keyboard0" "CoreKeyboard"
EndSection
.De
The file will also have multiple monitor, device and screen sections.  The
server can't know about the real physical layout of the screen, of course, so
you may have to change the ordering of the screens.  When you run the server
without any other specifications, it is assigned server number 0, so these
screens will be numbered \fI:0.0\fP, \fI:0.1\fP\/ and \fI:0.2\fP.
.H3 "Multiple servers"
.X "X, multiple servers"
It's also possible to run more than one X server on a single system, even if it
only has a single monitor.  There can be some good reasons for this: you may
share a system amongst your family members, so each of them can have their own
server.  Alternatively, you may have a laptop with a high-resolution display and
need to do a presentation on overhead projectors that can't handle more than
1024x768 pixels.  It's not practical to simply switch to a lower resolution,
because the overall screen size doesn't change, and it's difficult to avoid
sliding the image around when you move the cursor.
.P
For each server, you require one virtual terminal\(emsee page
.Sref \*[xdm] \&
for more details.  If you're using the same hardware, you can also use the same
.File XF86Config
file.  The only difference is in the way in which you start the server.  For
example, you could start three X servers, one with the \fIfvwm2\fP\/ window
manager, one with \fIKDE\fP\/ and one with \fIGNOME\fP, with the following
script:
.Dx
xinit &
xinit .xinitrc-kde -- :1 &
xinit .xinitrc-gnome -- :2 -xf86config XF86Config.1024x768 &
.De
Due to different command line options, you must use
.Command xinit
here, and not
.Command startx .
The first
.Command xinit
starts a server with the default options:  it reads its commands from
.File .xinitrc ,
it has the server number 0, and it reads its configuration from the default
.File XF86Config
file.
The second server reads its commands from
.File .xinitrc-kde ,
it has the server number 1, and it reads its configuration from the default
.File XF86Config
file.
The third server reads its commands from
.File .xinitrc-gnome ,
it has the server number 2, and the configuration file is
.File XF86Config.1024x768 .
Assuming that you reserve virtual terminals
.Device -n ttyv7 ,
.Device -n ttyv8
and
.Device -n ttyv9
for the servers, you can switch between them with the key combinations
\fBCtrl-Alt-F8\fP,  \fBCtrl-Alt-F9\fP and  \fBCtrl-Alt-F10\fP.
.H2 "X in the network"
.Pn network-X
.X "X, networking"
X is a network protocol.  So far we have looked at the server.  The clients are
the individual programs, such as
.Command xterm ,
.Command emacs
or a web browser, and they don't have to be on the same machine.  A special
notation exists to address X servers and screens:
.Ds
\fISystem name\fP\/\f(CB:\fP\fIserver number\fP\/\f(CB.\fP\fIscreen number\fP\/
.De
When looking at X client-server interaction, remember that the server is the
software component that manages the display.  This means that you're always
sitting at the server, not at the client.  For example, if you want to start an
.Command xterm
client on \fIfreebie\fP\/ and display it on \fIpresto\fP, you'll be sitting at
\fIpresto\fP.  To do this, you could type in, on \fIpresto\fP,
.Dx
$ \f(CBssh freebie xterm -ls -display presto:0 &\fP
.De
.X "~/.rhosts"
The flag \f(CW-ls\fP tells
.Command xterm
that this is a \fIlogin shell\fP, which causes it to read in the startup files.
.P
For this to work, you must tell the X server to allow the connection.  There are
two things to do:
.Ls B
.LI
Use
.Command xhost
to specify the names of the systems that have access:
.Dx
$ \f(CBxhost freebie presto bumble wait gw\fP
.De
This enables access from all the systems on our reference network, including the
one on which it is run.  You don't need to include your own system, which is
enabled by default, but if you do, you can use the same script on all systems on
the network.
.LI
.Command xhost
is not a very robust system, so by default
.Command startx
starts X with the option \f(CW-nolisten tcp\fP.  This completely blocks access
from other systems.  If you want to allow remote clients to access your X
server, modify
.X "startx, command"
.X "command, startx"
.Command -n /usr/X11R6/bin/startx ,
which contains the text:
.Dx
listen_tcp="-nolisten tcp"
.De
Change this line to read:
.Dx
listen_tcp=
.De
This enables remote connections the next time you start the server.
.Le
.ne 15v
.H3 "Multiple monitors across multiple servers"
.X "X, multiple monitors"
.X "X, multiple servers"
We saw above that a server can handle multiple monitors, and a system can handle
multiple servers.  One problem with multiple monitors is that most computers can
only handle a small number of display boards: a single AGP board and possibly a
number of PCI boards.  But PCI boards are difficult to find nowadays, and
they're slower and have less memory.
.P
If you have a number of machines located physically next to each other, you have
the alternative of running X on each of them and controlling everything from one
keyboard and mouse.  You do this with the
.Daemon x11/x2x
port.  For example: \fIfreebie\fP, \fIpresto\fP\/ and \fIbumble\fP\/ have
monitors next to each other, and \fIpresto\fP\/ has two monitors.  From left to
right they are \fIfreebie:0.0\fP, \fIpresto:0.0\fP, \fIpresto:0.1\fP\/ and
\fIbumble:0.0\fP.  The keyboard and mouse are connected to \fIpresto\fP.  To
incorporate \fIfreebie:0.0\fP\/ and \fIbumble:0.0\fP\/ in the group, enter these
commands on \fIpresto\fP\/:
.Dx
$ \f(CBDISPLAY=:0.0 x2x -west -to freebie:0 &\fP
$ \f(CBDISPLAY=:0.1 x2x -east -to bumble:0 &\fP
.De
After this, you can move to the other machines by moving the mouse in the
corresponding direction.  It's not possible to continue to a further machine,
but it is possible to connect in other directions (\fInorth\fP\/ and
\fIsouth\fP\/) from each monitor on \fIpresto\fP, which in this case would allow
connections to at least six other machines.  Before that limitation becomes a
problem, you need to find space for all the monitors.
.H3 "Stopping X"
.X "X, stopping"
To stop X, press the key combination \fBCtrl\fP-\fBAlt\fP-\fBBackspace\fP, which
is deliberately chosen to resemble the key combination
\fBCtrl\fP-\fBAlt\fP-\fBDelete\fP used to reboot the machine.
\fBCtrl\fP-\fBAlt\fP-\fBBackspace\fP stops X and returns you to the virtual
terminal in which you started it.  If you run from
.Daemon xdm ,
it redisplays a login screen.
.ig
Date: Sun, 17 Aug 2003 18:43:53 -0700
From: Rex Buddenberg <buddenr21@comcast.net>
To: grog@freebsd.org
Subject: book, fodder for next edition

Greg,

Watching all my office neighbors and their Windows machines crashing
reminds me why I stay current with Linux, Mac, FreeBSD and Solaris
....

My latest haul of books included your Complete FreeBSD, 4th edition.

Need to see: treatment of X and LCD screens.

Obsolescence.  I'm in ch 6, page 104.  You spend a lot of time
telling the reader how to figure out sync rates and such for CRT
monitors.  But none for configuring X with recalcitrant laptop
screens which happens to be my specific problem.
        Specific detail.  I have been keeping FreeBSD in a
one-generation-older laptop (running Linux in the current one).  The
installation went fine (4.8-stable) and I agree with your comments
about FreeBSD stability: progressive releases routinely pick up a
PCMCIA ethernet card that Linux occasionally balks at, depending on
version and I don't have the bus probe problems with FreeBSD that
Linux occasionally presents on installation.  But the X (4.3) only
works for 800x600 and 640x480 ... I KNOW this laptop will run
1024x768 -- it's done it before.  Granted the problem is in XFree86,
not the OS itself (and it's shown up in Linux distributions that
include X v4.3 as well).  Given the increasing ubiquity of LCDs on
desktops as well as laptops, this strikes me as a worthwhile
inclusion in the next edition.... assuming you have the patience to
write it.

Current uses.  I teach networking at Naval Postgraduate School.  DoD
has dictum from on high that all kinds of networks are supposed to
migrate to IPv6 in the next few years ... and there is precious
little deckplate experience in v6.  My students in this fall's class
don't know it yet, but they'll be getting some.
..
