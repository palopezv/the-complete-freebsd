/* Primitive preprocessor: read in a file and look for lines beginning
 * with ..if <tag> and ..endif.  Remove everything between ..if and ..endif
 * if it's not our tag.
 *
 * $Id: cleanup.c,v 1.3 2005/10/09 05:55:07 grog Exp grog $
 *
 * $Log: cleanup.c,v $
 * Revision 1.3  2005/10/09 05:55:07  grog
 * White space cleanup.
 * Remove unused variables.
 *
 * Revision 1.2  1996/01/13 15:27:33  grog
 * Checkpoint 21 January
 *
 * Revision 1.1  1996/01/13  13:57:06  grog
 * Initial revision
 *
 * Revision 1.3  1995/12/15  11:49:31  grog
 * Checkpoint 16 December
 *
 * Revision 1.2  1995/12/12  11:16:31  grog
 * Checkpoint 12 December
 *
 * Revision 1.1  1995/12/05  14:10:27  grog
 * Initial revision
 *
 *
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/errno.h>
#include <sys/param.h>

#define BIGBUF 1024

char inbuf [BIGBUF];
char filename [MAXPATHLEN];				    /* name of current file */
char lastfilename [MAXPATHLEN];				    /* name of current file */

int main (int argc, char *argv [])
{
  int line = 0;						    /* line number */
  int founddot = 0;					    /* set when we find a dot command */

  if (argc < 2)
    {
    printf ("Usage: %s <file>\n", argv [0]);
    exit (1);
    }
  while (fgets (inbuf, BIGBUF, stdin))
    {
    line++;
    inbuf [strlen (inbuf) - 1] = '\0';			    /* chop off this damn trailing \n */
    if (! memcmp (inbuf, ".lf ", strlen (".lf ")))	    /* got an lf, */
      {
      int newline;
      int count;					    /* of parms read */
      char command [4];					    /* the .lf command */
      founddot = 1;					    /* found a dot, by the way */
      if ((count = sscanf (inbuf, "%s %d %s", command, &newline, filename)) != 3) /* get the parameters */
	{
	fprintf (stderr, "Invalid .lf command, only %d parameters found:\n%s\n",
		 count,
		 inbuf );
	}
      else
	{
	line = newline;					    /* note the new line number */
	strcpy (lastfilename, filename);
	puts (inbuf);					    /* pass name */
	}
      }
    else if (*inbuf == '.' && inbuf [1] != '\\')	    /* found something */
      founddot = 1;					    /* found a dot */
    if (! memcmp (inbuf, ".SH NAME", strlen (".SH NAME")))  /* found a name line */
      {
      char *cp;
      puts (inbuf);					    /* output it */
      /* Now look at all names on the line up to the first - */

      /* This empirical algorithm says: If the line(s) immediately
       * after the .SH NAME line are comments, just output them.  If
       * the first non-comment line is a . line, skip the command.
       * Divide the line into chunks separated by commas or other
       * delimiters, and output an index hit for each.
       */
      do
	{
	if (! fgets (inbuf, BIGBUF, stdin))
	  {
	  printf ("Unexpected EOF on %s\n", argv [1]);
	  exit (1);
	  }
	inbuf [strlen (inbuf) - 1] = '\0';		    /* chop off this damn trailing \n */
	line++;
	}
      while (! memcmp (inbuf, ".\\", 2));		    /* comment */
      cp = inbuf;
      if (*cp == '.')					    /* command */
	while (*++cp != ' ');				    /* look for a blank */
      while (*cp && (*cp != '\n'))
	{
	printf (".tm ><INDEX:");
	while (*cp && (! ispunct (*cp)))
	  {
	  putchar (*cp);
	  cp++;						    /* care, could be a macro */
	  }
	printf (" \\n%%\n");
	while (ispunct (*cp))
	  cp++;						    /* skip punctuation */
	}
      printf (".lf %d %s\n", line, argv [1]);		    /* sync line number info */
      }
    if (founddot)
      puts (inbuf);
    }
  return 0;
  }
