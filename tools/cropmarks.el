; Set crop marks.  All values in points.
(defun cropmarks (xorigin		; new origin of page
		  yorigin 
		  height		; height of paper
		  width			; of paper
		  &optional istex	; set if it's TeX output
		  xorig			; x origin 
		  yorig			; and y
		  cropoffset		; offset from corner of paper to crop mark
		  insert-pos )		; regexp to find insert position
  (let ((croplength 25)			; length of crop mark
	(linewidth .4)			; width of crop mark
	(pageno 1) )			; running page number
    (if (null cropoffset)
	(setq cropoffset 36) )		; default to 1/2" offset
    (if (null insert-pos)
	(setq insert-pos "^%%EndPageSetup$") )
    (if (null xorig)
	(setq xorig 30) )		; original x
    (if (null yorig)
	(setq yorig 2) )		; and y offset of page
    (while (re-search-forward insert-pos nil t)
;;; put in crop marks and move the page down to accomodate them
      (insert "
")					; new line
      (insert (int-to-string linewidth) " LW ")	; set line width
; Get this right
;      (insert (int-to-string (+ xorigin cropoffset)) " " ; insert identification at top of page
;	      (int-to-string (- yorigin cropoffset)) " moveto ("
;	      buffer-file-truename " "
;	      (current-time-string) " Page "
;	      (int-to-string pageno) ")
;" )
      (setq pageno (+ 1 pageno))	; increment page number
;;; top left marks
      (insert (int-to-string (- xorigin cropoffset croplength)) " " ; top left, horizontal
	      (int-to-string (- yorigin cropoffset)) " " 
	      (int-to-string (- xorigin cropoffset)) " " 
	      (int-to-string (- yorigin cropoffset)) " DL ")
      (insert (int-to-string (- xorigin cropoffset)) " " ; top left, vertical
	      (int-to-string (- yorigin cropoffset croplength)) " " 
	      (int-to-string (- xorigin cropoffset)) " " 
	      (int-to-string (- yorigin cropoffset)) " DL 
")
;;; top right marks
      (insert (int-to-string (+ xorigin width cropoffset croplength)) " " ; horizontal
	      (int-to-string (- yorigin cropoffset)) " " 
	      (int-to-string (+ xorigin width cropoffset)) " " 
	      (int-to-string (- yorigin cropoffset)) " DL ")
      (insert (int-to-string (+ xorigin width cropoffset)) " " ; vertical
	      (int-to-string (- yorigin cropoffset croplength)) " " 
	      (int-to-string (+ xorigin width cropoffset)) " " 
	      (int-to-string (- yorigin cropoffset)) " DL 
")
;;; bottom left marks
      (insert (int-to-string (- xorigin cropoffset croplength)) " " ; bottom left, horizontal
	      (int-to-string (+ yorigin height cropoffset)) " " 
	      (int-to-string (- xorigin cropoffset)) " " 
	      (int-to-string (+ yorigin height cropoffset)) " DL ")
      (insert (int-to-string (- xorigin cropoffset)) " " ; bottom left, vertical
	      (int-to-string (+ yorigin height cropoffset croplength)) " " 
	      (int-to-string (- xorigin cropoffset)) " " 
	      (int-to-string (+ yorigin height cropoffset)) " DL 
" )
;;; bottom right marks
      (insert (int-to-string (+ xorigin width cropoffset croplength)) " " ; horizontal
	      (int-to-string (+ yorigin height cropoffset)) " " 
	      (int-to-string (+ xorigin width cropoffset)) " " 
	      (int-to-string (+ yorigin height cropoffset)) " DL ")
      (insert (int-to-string (+ xorigin width cropoffset)) " " ; vertical
	      (int-to-string (+ yorigin height cropoffset croplength)) " " 
	      (int-to-string (+ xorigin width cropoffset)) " " 
	      (int-to-string (+ yorigin height cropoffset)) " DL/F0
")
      (insert "10/Times-Roman@0 SF() 465.6 588 Q " 
	      (int-to-string (- xorigin xorig)) " "  
	      (int-to-string (- yorigin yorig)) " translate") ) ) )
    
; Draw a page frame around the text. All values in points.
(defun pageframe (xorigin		; new origin of page
		  yorigin 
		  height		; height of paper
		  width			; of paper
		  &optional cropoffset	; offset from corner of paper to crop mark
		  insert-pos )		; regexp to find insert position
  (let ((linewidth .4)			; width of crop mark
	(xorig 30)			; original x
	(yorig 2) 			; and y offset of page
	(pageno 1) )			; running page number
    (if (null cropoffset)
	(setq cropoffset 36) )		; default to 1/2" offset
    (if (null insert-pos)
	(setq insert-pos "^%%EndPageSetup$") )
    (while (re-search-forward insert-pos nil t)
;;; put in crop marks and move the page down to accomodate them
      (insert "
")					; new line
      (insert (int-to-string linewidth) " LW ")	; set line width
; Get this right
;      (insert (int-to-string (+ xorigin cropoffset)) " " ; insert identification at top of page
;	      (int-to-string (- yorigin cropoffset)) " moveto ("
;	      buffer-file-truename " "
;	      (current-time-string) " Page "
;	      (int-to-string pageno) ")
;" )
      (setq pageno (+ 1 pageno))	; increment page number
;;; top
      (insert (int-to-string xorigin) " " 
	      (int-to-string yorigin) " " 
	      (int-to-string (+ xorigin width)) " " 
	      (int-to-string yorigin) " DL
")
;;; right
      (insert (int-to-string (+ xorigin width)) " " ; horizontal
	      (int-to-string yorigin) " " 
	      (int-to-string (+ xorigin width)) " " 
	      (int-to-string (+ yorigin height)) " DL
")
;;; left
      (insert (int-to-string xorigin) " " ; horizontal
	      (int-to-string yorigin) " " 
	      (int-to-string xorigin) " " 
	      (int-to-string (+ yorigin height)) " DL
")
;;; bottom
      (insert (int-to-string xorigin ) " " 
	      (int-to-string (+ yorigin height)) " " 
	      (int-to-string (+ xorigin width)) " " 
	      (int-to-string (+ yorigin height)) " DL
") ) ) )

; For testing:
 (defun docrop ()
   (interactive)
    (pageframe 80 80 648 432 nil "^%%Page:.*$") )		;for testing only!

