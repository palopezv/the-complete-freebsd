; Despite appearances, this is not an emacs .el file.  It needs to
; be passed through sed first
; Set crop marks.  All values in points.
(defun cropmarks (xorigin		; new origin of page
		  yorigin 
		  height		; height of paper
		  width			; of paper
		  &optional cropoffset	; offset from corner of paper to crop mark
		  insert-pos )		; regexp to find insert position
  (let ((croplength 25)			; length of crop mark
	(linewidth .4)			; width of crop mark
	(xorig 30)			; original x
	(yorig 149)			; and y offset of page
	(pageno 1) )			; running page number
    (if (null cropoffset)
	(setq cropoffset 36) )		; default to 1/2" offset
    (if (null insert-pos)
	(setq insert-pos "^%%Page:.*$") )
    (while (re-search-forward insert-pos nil t)
;;; put in crop marks and move the page down to accomodate them
      (insert "
")					; new line
      (insert (int-to-string linewidth) " LW ")	; set line width
; Get this right
;      (insert (int-to-string (+ xorigin cropoffset)) " " ; insert identification at top of page
;	      (int-to-string (- yorigin cropoffset)) " moveto ("
;	      buffer-file-truename " "
;	      (current-time-string) " Page "
;	      (int-to-string pageno) ")
;" )
      (setq pageno (+ 1 pageno))	; increment page number
;;; bottom left marks
      (insert (int-to-string (- xorigin cropoffset croplength)) " " ; bottom left, horizontal
	      (int-to-string (- yorigin cropoffset)) " " 
	      (int-to-string (- xorigin cropoffset)) " " 
	      (int-to-string (- yorigin cropoffset)) " DL ")
      (insert (int-to-string (- xorigin cropoffset)) " " ; bottom left, vertical
	      (int-to-string (- yorigin cropoffset croplength)) " " 
	      (int-to-string (- xorigin cropoffset)) " " 
	      (int-to-string (- yorigin cropoffset)) " DL 
")
;;; bottom right marks
      (insert (int-to-string (+ xorigin width cropoffset croplength)) " " ; horizontal
	      (int-to-string (- yorigin cropoffset)) " " 
	      (int-to-string (+ xorigin width cropoffset)) " " 
	      (int-to-string (- yorigin cropoffset)) " DL ")
      (insert (int-to-string (+ xorigin width cropoffset)) " " ; vertical
	      (int-to-string (- yorigin cropoffset croplength)) " " 
	      (int-to-string (+ xorigin width cropoffset)) " " 
	      (int-to-string (- yorigin cropoffset)) " DL 
")
;;; top left marks
      (insert (int-to-string (- xorigin cropoffset croplength)) " " ; top left, horizontal
	      (int-to-string (+ yorigin height cropoffset)) " " 
	      (int-to-string (- xorigin cropoffset)) " " 
	      (int-to-string (+ yorigin height cropoffset)) " DL ")
      (insert (int-to-string (- xorigin cropoffset)) " " ; top left, vertical
	      (int-to-string (+ yorigin height cropoffset croplength)) " " 
	      (int-to-string (- xorigin cropoffset)) " " 
	      (int-to-string (+ yorigin height cropoffset)) " DL 
" )
;;; top right marks
      (insert (int-to-string (+ xorigin width cropoffset croplength)) " " ; horizontal
	      (int-to-string (+ yorigin height cropoffset)) " " 
	      (int-to-string (+ xorigin width cropoffset)) " " 
	      (int-to-string (+ yorigin height cropoffset)) " DL ")
      (insert (int-to-string (+ xorigin width cropoffset)) " " ; vertical
	      (int-to-string (+ yorigin height cropoffset croplength)) " " 
	      (int-to-string (+ xorigin width cropoffset)) " " 
	      (int-to-string (+ yorigin height cropoffset)) " DL/F0
")
      (insert "10/Times-Roman@0 SF() 465.6 588 Q " 
	      (int-to-string (- xorigin xorig)) " "  
	      (int-to-string (- yorigin yorig)) " translate") ) ) )
    
; Draw a page frame around the text. All values in points.
(defun pageframe (xorigin		; new origin of page
		  yorigin 
		  height		; height of paper
		  width			; of paper
		  &optional cropoffset	; offset from corner of paper to crop mark
		  insert-pos )		; regexp to find insert position
  (let ((linewidth .4)			; width of crop mark
	(xorig 30)			; original x
	(yorig 178)			; and y offset of page
	(pageno 1) )			; running page number
    (if (null insert-pos)
	(setq insert-pos "^%%Page:.*$") )
    (while (re-search-forward insert-pos nil t)
;;; put in crop marks and move the page down to accomodate them
      (insert "
")					; new line
      (insert (int-to-string linewidth) " LW ")	; set line width
      (setq pageno (+ 1 pageno))	; increment page number
;;; bottom
      (insert (int-to-string xorigin) " " 
	      (int-to-string yorigin) " " 
	      (int-to-string (+ xorigin width)) " " 
	      (int-to-string yorigin) " DL
")
;;; right
      (insert (int-to-string (+ xorigin width)) " " ; horizontal
	      (int-to-string yorigin) " " 
	      (int-to-string (+ xorigin width)) " " 
	      (int-to-string (+ yorigin height)) " DL
")
;;; left
      (insert (int-to-string xorigin) " " ; horizontal
	      (int-to-string yorigin) " " 
	      (int-to-string xorigin) " " 
	      (int-to-string (+ yorigin height)) " DL
")
;;; top
      (insert (int-to-string xorigin ) " " 
	      (int-to-string (+ yorigin height)) " " 
	      (int-to-string (+ xorigin width)) " " 
	      (int-to-string (+ yorigin height)) " DL
") ) ) )

; For testing:
 (defun docrop ()
   (interactive)
    (pageframe 80 80 @HEIGHT@ @WIDTH@ nil "^%%Page:.*$") )		;for testing only!

; Join the collection of PostScript files in the buffer to make a single file.
(defun massageps ()
  (interactive)
  (let ((searching t)
	(start)
	(pagenum 1) )
    (beginning-of-buffer)
    (while (and searching
		(re-search-forward "^%%Trailer" nil t) )
      (beginning-of-line)
      (setq start (point))
      (if (setq searching (re-search-forward "^%%Page:" nil t))
	  (progn (beginning-of-line)
		 (delete-region start (point)) ) ) )
    ;; Now go back and sort out the page numbers
    (beginning-of-buffer)
    (while (re-search-forward "^%%Page: [0-9]+ *" nil t)
      (kill-line)
      (insert (int-to-string pagenum))
      (setq pagenum (1+ pagenum)) )
    (beginning-of-buffer)			; now adjust page count
    (re-search-forward "^%%Pages: " nil t)
    (kill-line)
    (insert (int-to-string pagenum)) 
    (beginning-of-buffer)
    (re-search-forward "%%EndProlog" nil t) ;find somewhere to put our drawing definitions
    (beginning-of-line)
    (insert "/Q{moveto show}bind def
/SF{
findfont exch
[exch dup 0 exch 0 exch neg 0 0]makefont
dup setfont
[exch/setfont cvx]cvx bind def
}bind def
/LW/setlinewidth load def
/SN{
transform
.25 sub exch .25 sub exch
round .25 add exch round .25 add exch
itransform
}bind def
/DL{
SN
moveto
SN
lineto stroke
}bind def" )
    ;; now set crop marks on all the pages, and move the origin a little
    (beginning-of-buffer)
    (cropmarks 87 64 @HEIGHT@ @WIDTH@)
    )
  )

(defun exit ()
  (save-buffers-kill-emacs t) )

(defun makeframe ()
  (beginning-of-buffer)
  (pageframe 87 64 @HEIGHT@ @WIDTH@) )
