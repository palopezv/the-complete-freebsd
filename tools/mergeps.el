; Join the collection of PostScript files in the buffer to make a single file.
(defun mergeps ()
  (interactive)
  (let ((searching t)
	(start)
	(pagenum 1) )
    (beginning-of-buffer)
    (while (and searching
		(re-search-forward "^%%Trailer" nil t) )
      (beginning-of-line)
      (setq start (point))
      (if (setq searching (re-search-forward "^%%Page:" nil t))
	  (progn (beginning-of-line)
		 (delete-region start (point)) ) ) )
    ;; Now go back and count the pages
    (beginning-of-buffer)
    (while (re-search-forward "^%%Page: [0-9]+ *" nil t)
      (kill-line)
      (insert (int-to-string pagenum))
      (setq pagenum (1+ pagenum)) )
    (beginning-of-buffer)			; now adjust page count
    (re-search-forward "^%%Pages: " nil t)
    (kill-line)
    (insert (int-to-string pagenum)) ) )

(mergeps)
(save-buffers-kill-emacs t)