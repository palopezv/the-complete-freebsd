; Join the collection of PostScript files in the buffer to make a single file.
(defun massageps ()
  (interactive)
  (let ((searching t)
	(start)
	(pagenum 1) )
    (beginning-of-buffer)
    (pageframe 80 80 648 432)
    )
  )

(load-file "/home/book/FreeBSD/tools/cropmarks.el")
(massageps)
(save-buffers-kill-emacs t)

