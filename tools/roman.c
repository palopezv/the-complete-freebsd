/* Routines to convert to and from roman numbers
 *
 * Greg Lehey, LEMIS
 *
 * $Id: roman.c,v 1.1 1996/01/06 17:57:29 grog Exp grog $
 *
 * $Log: roman.c,v $
 * Revision 1.1  1996/01/06 17:57:29  grog
 * Initial revision
 *
 * Revision 1.2  1995/12/04  16:48:53  grog
 * First final draft
 *
 * Revision 1.1  1995/11/15  11:48:22  grog
 * Initial revision
 *
 */

#include <ctype.h>
#include <stdio.h>

/* Convert roman number to int */
int romannumber (char *text)
{
  int number = 0;
  char *p = text;
  while (*p)
    {
    char c = tolower (*p);
    p++;
    switch (c)				    /* p is pointing to next char in this switch */
      {
    case 'i':
      if (tolower (*p) == 'v')
	{
	number += 4;
	p++;
	}
      else if (tolower (*p) == 'x')
	{
	number += 9;
	p++;
	}
      else
	number++;
      break;
      
    case 'v':
      number += 5;
      break;
      
    case 'x':
      if (tolower (*p) == 'l')
	{
	number += 40;
	p++;
	}
      else if (tolower (*p) == 'c')
	{
	number += 90;
	p++;
	}
      else
	number += 10;
      break;
      
    case 'l':
      number += 50;
      break;
      
    case 'c':
      if (tolower (*p) == 'd')
	{
	number += 400;
	p++;
	}
      else if (tolower (*p) == 'm')
	{
	number += 900;
	p++;
	}
      else
	number += 100;
      break;
      
    case 'd':
      number += 500;
      break;
      
    case 'm':
      number += 1000;
      break;
      
    case '\n':						    /* got to the end */
      break;

    default:
      printf ("Invalid character '%c' in roman numeral %s\n", c, text);
      }
    }
  return number;
  }
      
/* Convert int to roman, return pointer to a static field */
char *toroman (int number)
{
  static char result [80];
  char *c = result;
  
  if (number > 4000)
    {
    sprintf (result, "Too big for roman: %d", number);
    return result;
    }
  while (number >= 1000)
    {
    *c++ = 'm';
    number -= 1000;
    }
  if (number >= 900)
    {
    *c++ = 'c';
    *c++ = 'm';
    number -= 900;
    }
  if (number >= 500)
    {
    *c++ = 'd';
    number -= 500;
    }
  if (number >= 400)
    {
    *c++ = 'c';
    *c++ = 'd';
    number -= 400;
    }
  while (number >= 100)
    {
    *c++ = 'c';
    number -= 100;
    }
  if (number >= 90)
    {
    *c++ = 'x';
    *c++ = 'c';
    number -= 90;
    }
  if (number >= 50)
    {
    *c++ = 'l';
    number -= 50;
    }
  if (number >= 40)
    {
    *c++ = 'x';
    *c++ = 'l';
    number -= 40;
    }
  while (number >= 10)
    {
    *c++ = 'x';
    number -= 10;
    }
  if (number >= 9)
    {
    *c++ = 'i';
    *c++ = 'x';
    number -= 9;
    }
  if (number >= 5)
    {
    *c++ = 'v';
    number -= 5;
    }
  if (number >= 4)
    {
    *c++ = 'i';
    *c++ = 'v';
    number -= 4;
    }
  while (number--)
    *c++ = 'i';
  *c = '\0';
  return result;
  }
