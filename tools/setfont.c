/* Filter to change some troff requests into the corresponding
 * HTML requests.
 * $Id: setfont.c,v 1.1 1999/07/05 22:53:36 grog Exp grog $
 *
 * $Log: setfont.c,v $
 * Revision 1.1  1999/07/05 22:53:36  grog
 * Initial revision
 *
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/errno.h>
#include <sys/param.h>

#define BIGBUF 1024
#define OUTPUT(x) { char *foo = x; while (*foo) *op++ = *foo++; }
char inbuf [BIGBUF];
char outbuf [BIGBUF];
char filename [MAXPATHLEN];				    /* name of current file */
char lastfilename [MAXPATHLEN];				    /* name of current file */

char *op;

struct xfont
  {
  char *trofffont;
  char *htmlfont;
  char *htmlofffont;
  }
fonts [] = 
{
  {"CW", "<tt>", "</tt>"},
  {"R", "", ""},
  {"B", "<b>", "</b>"},
  {"I", "<i>", "</i>"},
  {"CB", "<b><tt>", "</tt></b>"},
  {"CI", "<i><tt>", "</tt></i>"},
  {"BI", "<b><i>", "</i></b>"}
  };

struct escapees
{
  char c;
  char *replacement;
  }
escapee [] = 
{
  {'<', "&lt;"},
  {'>', "&gt;"},
  {'�', "&aelig;"},
  {'�', "&Aelig;"},
  {'�', "&eacute;"},
  {'�', "&Eacute;"},
  {'&', "&amp;"}};  

char *lastfont;
int lastfontsize = 0;					    /* previous troff font size */
int fontsize = 12;					    /* current font size */
int htmlfontsize;					    /* corresponding HTML size */
int centrecount = 0;					    /* number of lines yet to centre */

char *findspecial (char c)
{
  int i;
  
  for (i = 0; i < sizeof (escapee) / sizeof (struct escapees); i++)
    if (c == escapee [i].c)
      return escapee [i].replacement;
  return NULL;
  }

char * dofonts (char *cp)
{
  int i;

  while (isspace (*cp))					    /* skip spaces */
    cp++;
  if (*cp == '+')
    fontsize += atoi (&cp [1]);
  else if (*cp == '-')
    fontsize -= atoi (&cp [1]);
  else
    fontsize = atoi (&*cp);
  if (fontsize == 0)					    /* reset */
    {
    OUTPUT ("</font>");
    fontsize = lastfontsize;
    }
  else
    {
    char temp [64];
    
    if (fontsize >= 20)
      htmlfontsize = 7;
    else if (fontsize >= 17)
      htmlfontsize = 6;
    else if (fontsize >= 14)
      htmlfontsize = 5;
    else if (fontsize >= 12)
      htmlfontsize = 4;
    else if (fontsize >= 10)
      htmlfontsize = 3;
    else if (fontsize >= 8)
      htmlfontsize = 2;
    else
      htmlfontsize = 1;
    sprintf (temp, "<font size=%d>", htmlfontsize);
    OUTPUT (temp);
    }
  lastfontsize = fontsize;
  while (isdigit (*++cp));
  return cp;
  }

int main (int argc, char *argv [])
{
  int nestlevel = 0;					    /* number of nested ..if's */
  int line = 0;						    /* line number */
  FILE *infile;
  int founddot = 0;					    /* set when we find a dot command */
  int skipping = 0;					    /* set to 1 to skip output */
  int tabs = 0;						    /* set to 1 to output only tables and pics */
  char *cp;
  char *ep;
  char *ss;
  char *estring;					    /* escape string */

  if (argc > 1)
    {
    if ((argc > 2) || strcmp (argv [1], "-t"))
      {
      fprintf (stderr, "Usage:\n\n\t%s [-t]\n", argv [0]);
      exit (1);
      }
    tabs = 1;
    skipping = 1;
    }
  while (fgets (inbuf, BIGBUF, stdin))
    {
    line++;
    op = outbuf;
    cp = inbuf;
    ep = &inbuf [strlen (inbuf) - 1];
    *ep = '\0';						    /* chop off this damn trailing \n */
    
    if (centrecount)
      {
      if ((*inbuf != '\0')				    /* something there */
	  && (*inbuf != '.') )				    /* and not a command */
	{
	puts ("<br>");
	if (--centrecount == 0)				    /* finished */
	  puts ("</center>");
	}
      }
    if (skipping)
      {
      if (tabs)
	{
	if (! memcmp (inbuf, ".TS", 3)			    /* table start: turn output on again */
	    || (! memcmp (inbuf, ".PS", 3)) )
	  skipping = 0;					    /* stop skipping and output this line */
	else
	  continue;
	}
      else						    /* no tables, */
	{
	if (! memcmp (inbuf, ".TE", 3)			    /* table start: turn output on again */
	    || (! memcmp (inbuf, ".PE", 3)) )
	  skipping = 0;
	continue;					    /* don't output this line, though */
	}
      }
    else						    /* not skipping */
    /* Check if we should start skipping */
      {
#ifdef UNDERSTOOD
      /* What is this crap? */
      if (! memcmp (inbuf, ".ps", 3))		    /* found a size spec */
	{
	puts ("<br>");
	continue;
	}
#endif
      if (! memcmp (inbuf, ".ps", 3))		    /* found a size spec */
	{
	int i;
	char fontname [3];
	
	cp = dofonts (&inbuf [3]);
	}
      else if (! memcmp (inbuf, ".ce", 3))		    /* found a center thing */
	{
	cp = &inbuf [3];
	while (isspace (*cp))				    /* skip spaces */
	  cp++;
	centrecount = atoi (cp);
	centrecount += 1;				    /* default centre one line */
	puts ("<center>");
	continue;
	}
      else						    /* no tables, */
	{
	if (! memcmp (inbuf, ".TS", 3)			    /* table start: turn output on again */
	    || (! memcmp (inbuf, ".PS", 3)) )
	  {
	  skipping = 1;
	  continue;					    /* don't output this line, though */
	  }
	}
      }
    while (*cp)						    /* look for things */
      {
      if (! memcmp (cp, "\\f", 2))			    /* found a font */
	{
	int i;
	char fontname [3];
	
	if (cp [2] == '(')				    /* two-letter font */
	  {
	  memcpy (fontname, &cp [3], 2);
	  fontname [2] = '\0';
	  cp += 5;
	  }
	else if (cp [2] == '[')
	  {
	  int i = 0;
	  cp = &cp [3];					    /* look at it */
	  while ((*cp != ']')
		 && (i < sizeof (fontname) - 1) )
	    fontname [i++] = *cp++;
	  fontname [i] = '\0';
	  if (i = sizeof (fontname) - 1)
	    fprintf (stderr,
		     "Font name starting with %s is too long\n:%s\n",
		     fontname,
		     inbuf);
	  }
	else
	  {
	  fontname [0] = cp [2];
	  fontname [1] = '\0';
	  cp += 3;
	  }
	if (! strcmp (fontname, "P"))			    /* previous font? */
	  {
	  if (lastfont)
	    OUTPUT (lastfont);
	  lastfont = NULL;
	  }
	else
	  {
	  for (i = 0; i < sizeof (fonts) / sizeof (struct xfont); i++)
	    {
	    if (! (strcmp (fontname, fonts [i].trofffont))) /* found the font */
	      {
	      if (lastfont)				    /* turn off previous font */
		OUTPUT (lastfont);
	      lastfont = fonts [i].htmlofffont;		    /* note how to turn off this one */
	      OUTPUT (fonts [i].htmlfont);
	      break;
	      }
	    }
	  if (i ==  sizeof (fonts) / sizeof (struct xfont)) /* didn't find it */
	    fprintf (stderr, "can't find font %s:\n%s\n", fontname, inbuf);
	  }
	}
      else if (! memcmp (cp, "\\s", 2))			    /* found a size spec */
	cp = dofonts (&cp [2]);
      else if (estring = findspecial (*cp))
	{
	while (*estring)
	  *op++ = *estring++;
	cp++;
	}
      else						    /* nothing special, just copy */
	*op++ = *cp++;
      }
    *op = '\0';
    puts (outbuf);
    if ((tabs)
	&& (! memcmp (inbuf, ".TE", 3)			    /* table start: turn output on again */
	    || (! memcmp (inbuf, ".PE", 3)) ) )
      skipping = 1;					    /* stop skipping and output this line */
    }
  return 0;
  }

