; Mangle a PostScript file to set the offset correctly for a LaserJet 6MP
(defun setoffset ()
  (interactive)
  (while (re-search-forward "^%%EndPageSetup" nil t)
    (insert "
-80 -55 translate") ) )

(setoffset)
(save-buffers-kill-emacs t)