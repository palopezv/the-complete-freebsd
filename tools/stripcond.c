/*
 * Primitive preprocessor: read in a file and look for lines beginning
 * with ..if <tag> and ..endif.  Remove everything between ..if and
 * ..endif if it's not our tag.
 *
 * Greg Lehey, 5 December 1995
 *
 * $Id: stripcond.c,v 1.9 2005/08/23 05:24:59 grog Exp grog $
 *
 */
#ifndef DOTS
#define DOTS ".."					    /* leadin sequence for our commands */
#endif

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/param.h>

#define BIGBUF 1024

char inbuf [BIGBUF];
char filename [MAXPATHLEN];				    /* name of current file */
char lastfilename [MAXPATHLEN];				    /* name of current file */
char picspacing [64];					    /* spacing for pic, if specified */

int main (int argc, char *argv [])
{
  char **tag = argv;					    /* tags to search for */
  int tags = argc - 1;					    /* and number of tags */
  int nestlevel = 0;					    /* number of nested ..if's */
  int line = 0;						    /* line number */
  int skipping = 0;					    /* nest level at which we started skipping */

  tag++;						    /* point to argv [1] */
  while (fgets (inbuf, BIGBUF, stdin))
    {
    line++;
    inbuf [strlen (inbuf) - 1] = '\0';			    /* chop off this damn trailing \n */
    if (! memcmp (inbuf, ".lf ", strlen (".lf ")))	    /* got an lf, */
      {
      int newline;
      int count;					    /* of parms read */
      char command [4];					    /* the .lf command */
      if ((count = sscanf (inbuf, "%s %d %s", command, &newline, filename)) != 3) /* get the parameters */
	{
	fprintf (stderr, "Invalid .lf command, only %d parameters found:\n%s\n",
		 count,
		 inbuf );
	}
      else
	{
	line = newline;					    /* note the new line number */
	if (skipping && strcmp (filename, lastfilename))    /* didn't finish a skip before end of file */
	  fprintf (stderr, "Changed file from %s to %s while skipping input\n", lastfilename, filename);
	strcpy (lastfilename, filename);
	if (! skipping)
	  puts (inbuf);					    /* pass name */
	}
      }
    else if (! memcmp (inbuf, DOTS"if ", strlen (DOTS"if "))) /* got an if, */
      {
      nestlevel++;					    /* one more nest level */
      if (! skipping)					    /* not currently skipping */
	{
	int i;
	char *rest = &inbuf [strlen (DOTS"if ")];	    /* rest of line */
	while (*rest == ' ')
	  rest++;					    /* find something */
	for (i = 0; i < tags; i++)
	  if (! strcmp (rest, tag [i]))			    /* found our tag */
	    break;
	if (i == tags)					    /* didn't find anything, */
	  skipping = nestlevel;				    /* skip */
	}
      }
    else if (! memcmp (inbuf, DOTS"else", strlen (DOTS"else"))) /* got an else, */
      {
      if (nestlevel == 0)
	fprintf (stderr, "Line %d: unpaired ..else\n", line);
      else if (skipping >= nestlevel)			    /* is this where we started skipping? */
	{
	printf (".lf %d %s\n", line, lastfilename);	    /* put new line number info in */
	skipping = 0;					    /* yes, stop now */
	}
      else if (! skipping)				    /* not skipping? */
	skipping = nestlevel;				    /* yes, start now */
      }
    else if (! memcmp (inbuf, DOTS"endif", strlen (DOTS"endif"))) /* got an endif, */
      {
      if (nestlevel == 0)
	fprintf (stderr, "%s:%d: unpaired ..endif\n", filename, line);
      else
	nestlevel--;
      if (skipping > nestlevel)				    /* is this where we started skipping? */
	{
	printf (".lf %d %s\n", line, lastfilename);	    /* put new line number info in */
	skipping = 0;					    /* yes, stop now */
	}
      }
    else if ((! skipping)
	     && (! memcmp (inbuf, DOTS"sp", strlen (DOTS"sp"))) ) /* got a spacing line for pixc, */
      strcpy (picspacing, &inbuf [4]);			    /* save it */
    else if ((! skipping)
	     && (! memcmp (inbuf, DOTS"#", strlen (DOTS"#"))) ) /* got a pic line, */
      {
      char *c = &inbuf [3];				    /* start after the ..# */
      char *e;

      while (1)
	{
	e = strchr (c, '#');				    /* find a tab character */
	if (e)						    /* got one */
	  {
	  *e++ = '\0';					    /* nuke it */
	  printf ("T{\n%s\nT}#", c);			    /* print this part of the line */
	  c = e;
	  }
	else						    /* end of the line */
	  {
	  printf ("T{\n%s\nT}\n", c);			    /* print this part of the line */
	  break;
	  }
	}
      if (*picspacing)					    /* we have special spacing */
	printf (".sp%s\n", picspacing);
      }
    else if (! skipping)
      puts (inbuf);					    /* just pass it through */
    }
  if (skipping)
    fprintf (stderr, "%s:%d: end of file at nest level %d\n", filename, line, skipping);
  return 0;
  }
