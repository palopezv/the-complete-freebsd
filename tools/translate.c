#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#define BUFSIZE 65536

int started = 0;
char inbuf [BUFSIZE];

int main (int argc, char *argv [])
{
  while (1)
    {
    if (fgets (inbuf, BUFSIZE, stdin) == NULL)
      return 0;
    printf ("%s", inbuf);
    if (strcmp (inbuf, "%%EndPageSetup\n") == 0)
      printf ("-60 360 translate\n");
    }
  }
