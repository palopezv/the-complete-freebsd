/* xchar: turn a numeric value into a character.
 *
 * For convenience, extract the number from between '' if it's embedded
 * in other junk.
 * $Id: xchar.c,v 1.1 1997/09/07 07:42:29 grog Exp grog $
 *
 * $Log: xchar.c,v $
 * Revision 1.1  1997/09/07 07:42:29  grog
 * Initial revision
 *
 *
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/errno.h>
#include <sys/param.h>

int main (int argc, char *argv [])
{
  char *quote;						    /* find a quote in argv [1] */
  char *digit;						    /* point to first digit */
  int value = 0;					    /* value to return */

  if (argc < 2)
    {
    printf ("Usage: %s <file>\n", argv [0]);
    exit (1);
    }
  if (quote = strchr (argv [1], '\''))
    digit = &quote [1];					    /* expect a digit here  */
  else
    digit = argv [1];					    /* start at the beginning */
  value = atoi ((u_char *) digit);			    /* convert it */
  if (isdigit (*digit)					    /* it's really a digit */
      && (value < 256) )				    /* and the number's small enough */
    {
    printf ("%c\n", value);
    return 0;						    /* all's well */
    }
  else
    return 1;						    /* no go */
  }

