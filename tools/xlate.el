; Functions to manipulate PostScript numbers.  mirror moves a coordinate to the
; same distance on the other side of the value of the global variable mirrors, 
; xlate adds the value xoffset to the coordinate.  In each case, the coordinate 
; is the value around point.
;
; Greg Lehey, 1 May 1996
;
; $Id: xlate.el,v 1.1 1996/05/01 11:52:14 grog Exp grog $

(defun mirror ()
  (interactive)
  (let ((startpoint)
	(number)
	(offset (* mirrors 2)) )
    (re-search-backward "[^0-9]" nil t)
    (forward-char)
    (setq startpoint (point))
    (re-search-forward "[^0-9]")
    (forward-char -1)
    (kill-region startpoint (point))
    (setq number (string-to-int (car kill-ring)))
    (insert (int-to-string (- offset number))) ) )

(defun xlate ()
  (interactive)
  (let ((startpoint)
	(number) )
    (re-search-backward "[^0-9]" nil t)
    (forward-char)
    (setq startpoint (point))
    (re-search-forward "[^0-9]")
    (forward-char -1)
    (kill-region startpoint (point))
    (setq number (string-to-int (car kill-ring)))
    (insert (int-to-string (+ xoffset number))) ) )

