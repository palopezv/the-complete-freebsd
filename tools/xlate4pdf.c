/*
 * xlate4pdf.c: Massage PostScript.
 *
 * $Id: xlate4pdf.c,v 1.2 2005/10/09 06:16:48 grog Exp $
 *
 * Greg Lehey, 9 October 2005
 *
 * This used to be done by some functions in massageps.el, but Emacs
 * gets upset with files more than about 200 MB in size.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#define BUFSIZE 65536

int started = 0;
char inbuf [BUFSIZE];

int main (int argc, char *argv [])
{
  while (1)
    {
    if (fgets (inbuf, BUFSIZE, stdin) == NULL)
      return 0;
    if (memcmp (inbuf, "%%BeginFeature: *PageSize", strlen ("%%BeginFeature: *PageSize")) == 0)
      while (memcmp (inbuf, "%%EndFeature", strlen ("%%EndFeature")))
	{
	if (fgets (inbuf, BUFSIZE, stdin) == NULL)
	  return 0;
	}
    else
      printf ("%s", inbuf);
    if (strcmp (inbuf, "%%EndPageSetup\n") == 0)
      printf ("-60 360 translate\n");
    }
  }
